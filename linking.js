const config = {
    screens: {
        Home:{
            path: "home",
        },
        Profile:{
            path: "profile",
        },
        Notification:{
            path: "notification/:id",
            parse: {
                id: (id) => `${id}`,
            }
        }
    }
};

const linking = {
    prefixes: ["vfa://app"],
    config,
};

export default linking;