import React, { useState, useEffect } from 'react';
import { useColorScheme, Text, Platform, StatusBar, NativeModules} from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { BottomTabNavigator } from "./src/routes/bottomTabNavigatorPlayer";
import { createStackNavigator } from "@react-navigation/stack";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { setFCMTOKEN } from "./src/storage/user/dataUser";
import linking from './linking';
import { MenuProvider } from 'react-native-popup-menu';
import I18n from 'react-native-i18n';

// Vistas iniciales
import NotificationCoach from './src/screens/coach/global/notification';
import NotificationPlayer from './src/screens/player/global/notification';
import Splash from "./src/screens/splash";
import Login from "./src/screens/login";
import validationSesion from "./src/routes/validationSesion";
import Register from './src/screens/register';
import WelcomeView from './src/screens/welcome';
import RegistreSelect from './src/screens/registerSelect';
import WelcomeCoach from './src/screens/welcome/welcomeCoach';
import WelcomePlayer from './src/screens/welcome/welcomePlayer';
import RegisterCoach from './src/screens/registerCoach';
import RegisterPlayer from './src/screens/registerPlayer';
import PayAppCoach from './src/screens/payCoach';
import PayAppPlayer from './src/screens/payPlayer';
import PayAppPlayerDiscount from './src/screens/payPlayerDiscount';
import PayAppCoachDiscount from './src/screens/payCoachDiscount';



//// View Welcome player
import WelcomePlayerH from './src/screens/player/home/welcome';

/// Traduccion global
import en from './src/translation/en';
import es from './src/translation/es';
import zh from './src/translation/ch';

import { SafeAreaView } from 'react-native-safe-area-context';


I18n.fallbacks = true;

I18n.translations = {
  en,
  'en-US':en,
  es,
  'es-US':es,
  zh,
  'zh-CN':zh,
};


//Instanciando el stact navigator
const Stack = createStackNavigator();

// Configuracion del tema global de la app
const Theme = {
  // dark: true,
  colors: {
    blanco: 'rgb(255, 255, 255)',
    rojo: 'red'
  },
  options: {




    bottomTab: {
      fontFamily: 'DINRoundPro'
    },
    topBar: {
      title: {
        fontFamily: 'DINRoundPro'
      },
      subtitle: {
        fontFamily: 'helvetica'
      },
      backButton: {
        fontFamily: 'helvetica'
      }
    }
  }
};



//  // Função responsável por adquirir o idioma utilizado no device
//  var  getLanguageByDevice = () => {
//   return Platform.OS === 'ios'
//   ? NativeModules.SettingsManager.settings.AppleLocale // Adquire o idioma no device iOS
//   : NativeModules.I18nManager.localeIdentifier // Adquire o idioma no device Android
//   }

  
//   var language = getLanguageByDevice()

//   console.log(language);

const App = () => { // Componente inicial de la app



  




//  FIN DE LA PRUEBA DEL API GOOGLE PAY

// PRUEBA DEL API DE GOOGLE PAY 

     const [purchased, setPurchased] = useState(false);
     const [products, setProducts] = useState({});

        const [user, setUser] = useState({})
        useEffect(async() => {
          const user = await AsyncStorage.getItem('@user');
          const userJson = JSON.parse(user);
          setUser(userJson)
        }, [])




 

  // Prueba de los pagos  

  // const plataforma  = Plaform.OS ==

  return (
    <MenuProvider>

      <StatusBar backgroundColor="#f3f3f3" barStyle="dark-content" />
      <NavigationContainer theme={Theme} >
        <Stack.Navigator initialRouteName="Splash">
          <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
          <Stack.Screen name="Welcome1" component={WelcomeView} options={{ headerShown: false }} />
          <Stack.Screen name="Pay App Coach" component={PayAppCoach} options={{ headerShown: false }} />
          <Stack.Screen name="Pay App Player" component={PayAppPlayer} options={{ headerShown: false }} />
          <Stack.Screen name="Pay Discount" component={PayAppPlayerDiscount} options={{ headerShown: false }} />
          <Stack.Screen name="Pay Discount coach" component={PayAppCoachDiscount} options={{ headerShown: false }} />
          

          <Stack.Screen name="Registre Select" component={RegistreSelect} />
          <Stack.Screen name="Welcome Coach" component={WelcomeCoach} />
          <Stack.Screen name="Welcome Player" component={WelcomePlayer} />
          <Stack.Screen name="Registry" component={Register} />
          <Stack.Screen name="Register Coach" component={RegisterCoach} />
          <Stack.Screen name="Register Player" component={RegisterPlayer} />
          <Stack.Screen name="WP" component={WelcomePlayerH} options={{ headerShown: false }} />
          <Stack.Screen name="validationSesion" component={validationSesion} options={{ headerShown: false }} />
          <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }} />
          {user?
            <Stack.Screen name="Notification" component={user.type === 'coach'?NotificationCoach:NotificationPlayer} />
          :null}
        </Stack.Navigator>

      </NavigationContainer>
    </MenuProvider>
  );

}

export default App;