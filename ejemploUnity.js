import React, {Component} from 'react';
import { StyleSheet, Image, View, Dimensions, Text, TouchableOpacity, Modal } from 'react-native';
import UnityView, { MessageHandler, UnityModule } from '@asmadsen/react-native-unity-view';
import SmsListener from 'react-native-android-sms-listener'

export default class App extends Component {

    constructor(props){
      super(props);
      this.state = {
        modal:false,
        url:null,
      }
    }

  componentDidMount(){
    SmsListener.addListener(message => {
      console.warn(message)
    })
  }

  skill(){
    if (this.unity) {
      let data = {
        id: 20,
        token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiYjA0YThlZTBkZjI3NTc2OWVmZTcyOWU4MTBhZjFmYjg0ZjJiZmIxYjc1ODRjYTY4MTYwZGU0YTUzZjVjNDYzMGNmYzc0ZDJmMTUzMTEzNzUiLCJpYXQiOjE2MjQyMjE3NTMuMjQ2MzkxLCJuYmYiOjE2MjQyMjE3NTMuMjQ2Mzk4LCJleHAiOjE2NTU3NTc3NTMuMjA5ODYzLCJzdWIiOiIxMyIsInNjb3BlcyI6W119.f_jlQKV4VKd9q5LY9AbV273IxnwWyH-F76_SFJk1Z63GKXK0bSBXGPrR7xkRJFOKSyFQ8-da0_UljNwEoNAFZcqBPDdNbDwqWZ_OLSbCFMu3JJO_72lJV8MZxNlJ_SdXsnJAW8FEIYMc3OC8a6qh_U3CtgW10QuIn4tKgM8110x96iHhd6_-xKTBNyecTigcAeHOHwVSJhGK0mlvI4lWKcBEM7yZf4iSqk7JK3HkQfFgzL1pN4r3ZiaJ3TC0DbZjkBWPip7C_XqzwJq0eUApjfc9ErpAiEb7viGh6dox1GXz74_3WM0q26-pP9svJjhKsl8L9u-t9sy6ra4MmerRYGuOTFdLHqOKOliZcSyUB3YD0BiQXaB0eiM9ky4jq7DJVI1KS6cx1_XM7wIp58a5R-hZhRGdo1VMDtOEsiqzGXvdnCVnUT0crrNhNbdr3GJTv6GDFVw9_HOiCj49aeprNTn4yMF8QJ3AhmtyzZvxauwzubECnnygKCxxY1XzRU5iCeCfzD1uj7GSHIspsXAEX8lHSIYJJJS7qjZXpyfnXx6wt-AL7jMrfu99kTdqHz9VgKQRlV9Ps_N4_xx2uADKlUlDjhbmQrjYv-RDTMh22ZM94O4P1phrosKxUZofvB4Pj0clFTJI6v8ZSc-yOotQQqrWWl3Rb8cjBbbWYnw843E',
        type: 2,
      };
      var d = JSON.stringify(data)
      UnityModule.postMessage('ReactManager', 'ChangeSkill', d)
      // UnityModule.postMessage('WebController', 'ChangeSkill', 'hola')
      console.log(d)
      
    }
  }

  onMessage(event) {
    console.log('OnUnityMessage: ' + event); 
    this.setState({modal:true, url:event.data});   // OnUnityMessage: click
  }

    render() {
    return (
      <View style={{width:'100%', height:'100%'}}>
        <UnityView 
          style={{ width:'100%', height:'100%', }} 
          onMessage={this.onMessage.bind(this)}
          ref={(ref) => this.unity = ref}
        /> 
        <View style={{width:'100%', height:'10%', justifyContent:'center', alignItems:'flex-end', position:'absolute', bottom:800}}>
          <TouchableOpacity style={{width:'30%', height:'80%', backgroundColor: 'red', borderRadius:10, justifyContent:'center', alignItems:'center'}} onPress={this.skill.bind(this)}>
            <Text style={{color:'#fff'}}>Cargar 3D</Text>
          </TouchableOpacity>
        </View>

       
      </View>
    );
  }
}