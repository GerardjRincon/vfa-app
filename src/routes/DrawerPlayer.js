//import liraries
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Linking } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import AsyncStorage from '@react-native-async-storage/async-storage';
import Snackbar from 'react-native-snackbar';
import { useNavigation } from '@react-navigation/native';
import { url } from '../storage/config';
import { LogoutUser } from '../storage/user/dataUser';
// import Pusher from 'pusher-js/react-native'
import { setTeam, setCoach, setUser, setClub } from '../storage/user/dataUser';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import { pusher, canal } from './validationSesion';

import {signal, canal} from '../storage/pusher';
import I18n from 'i18n-js';



// create a component
const DrawerComponentPlayer = (props) => {

   
  

    const navigation = useNavigation();
    const [team, setT] = useState({})
    const [coach, setC] = useState({})
    const [token, setToken] = useState(null);
    const [club, setCLUB] = useState({});
    // const [user, setU] = useState(props.user)
    const [user, setU] = useState(props.user)
    const [Cambio, setCambios] = useState(false)

    const imagenUser = 'https://go.vfa.app'+user.photo;

    useEffect(async() => {


        const canal = signal.subscribe("VFA");


        const tokenAsync = await AsyncStorage.getItem('@token');
        const team = await AsyncStorage.getItem('@team');
        const coach = await AsyncStorage.getItem('@coach');
        const club = await AsyncStorage.getItem('@club');
        const coachJson = JSON.parse(coach);
        const teamJson = JSON.parse(team);
        const clubJson = JSON.parse(club);
        const tokenJson = JSON.parse(tokenAsync);
        setCLUB(clubJson);
        setToken(tokenJson);
        setT(teamJson);
        setC(coachJson);
        // getUser(tokenJson);

        AsyncStorage.getItem('@token')
        .then(res => {
            let datos = JSON.parse(res);
            getUser(datos);
        })
     
        
        // console.log(token);


        // canal.bind("Notificationxx", (data) => {
        //     console.log(signal,'---- Activo pusher ---');
        //     console.log(canal,'---- Activo pusher  canal---');
        //     console.log("Llego validation drawer ----");
        //     // console.log(data)
        //  })




        console.log(canal, 'canal puhser drawer');
        canal.bind("NotificationReplyPlayerUpdate-"+user.id,(data) =>{
            // getUser(tokenJson);
            AsyncStorage.getItem('@token')
            .then(res => {
                let datos = JSON.parse(res);
                getUser(datos);
                // setCambios(true)
            })
            
        })

        // canal.bind("NotificationUpdatePhoto-"+user.id, (data) => {
        //     setU(data.noti)
        // });

        return ()=>{
            // signal.disconnect();
            // canal.unsubscribe("VFA");
        }

    }, [])

    async function LogoutUser(){ // Se desloguea el user
        try {
            logoutBackend()
        } catch (error) {
        //   console.error(error);
          return null;
        }
      }

    async function getUser(token) {

        console.log("Se cumple la primera lista ------")

        await fetch(url + 'api/user', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
        }).then(res => res.json())
        .then(async(dat) => {
            console.log(dat, ' ----- datos')
            try {
                if (dat.message == 'Unauthenticated.') {
                    LogoutUser()
                    setTimeout(() => {
                        navigation.replace('Login')
                    }, 1000)
                }else{
                    setUser(dat)

                    setTeam(dat.myteamplayer)
                    setT(dat.myteamplayer)
                    setCoach(dat.mycoach)
                    setC(dat.mycoach)
                    setClub(dat.club)
                    setCLUB(dat.club)


                    const datosuser = await AsyncStorage.getItem('@user');
                    const jsonuser = JSON.parse(datosuser);
                    setU(jsonuser)

                }

            } catch (error) {
                Snackbar.show({
                    text: 'Failed to logout',
                    duration: Snackbar.LENGTH_LONG,
                });
            }
        })
    }

    async function logoutBackend(){
        await fetch(url + 'api/logout', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
        }).then(res => res.json())
        .then(async(dat) => {
            console.warn(dat)
            try {
                if (dat.ok) {
                    const keys = ['@user', '@token', '@skills', '@reviews', '@team', '@coach']
                    
                    // signal.disconnect();
                    signal.unsubscribe("VFA");
                    await AsyncStorage.multiRemove(keys)
                    navigation.replace('Login');
                }
            } catch (error) {
                Snackbar.show({
                    text: 'Failed to logout',
                    duration: Snackbar.LENGTH_LONG,
                });
            }
        })
    }



    const EnviarCoppa = async () => {
        try {
            await Linking.openURL('https://vfa.app/children-policy');
          } catch (e) {
            console.warn(e)
          }
    }



    const policy = async () => {
        try {
          await Linking.openURL('https://www.websitepolicies.com/policies/view/0NueZaX0');
        } catch (e) {
          console.warn(e)
        }
      };


      const contacts = async () => {
        try {
          await Linking.openURL('https://vfa.app/contact-us/');
        } catch (e) {
          console.warn(e)
        }
      };

      const Faq = async () => {
        try {
            await Linking.openURL('https://vfa.app/faq-player');
          } catch (e) {
          }
      }
     

    return (
        <View style={{flex:1, backgroundColor: '#F3F3F3',}}>
                 <Image source={require('../assets/pelota.png')} style={{
                    height: '100%',
                    width: '100%', 
                    position: "absolute",
                }} />
                <View style={{flex:1}}>
                    <View style={{width:'100%',justifyContent:'center', alignItems:'center', paddingTop:20, paddingBottom:20, borderBottomWidth:1, borderColor:'#d6d6d6'}}>
                        {user.photo?
                            <Image source={{uri:imagenUser}} resizeMode="cover" style={{width:hp('13%'), height:hp('13%'), backgroundColor: "#FFF", borderRadius:hp('13%'), justifyContent:'center', alignItems:'center'}}/>
                        :
                            <View style={{width:hp('13%'), height:hp('13%'), backgroundColor: "#FFF", borderRadius:hp('13%'), justifyContent:'center', alignItems:'center'}}>
                                <Text style={{fontSize:hp('4%'), fontWeight:'bold'}}>{user.name.charAt(0)}</Text>
                            </View>
                        }
                        <Text style={{fontSize:hp('2%'), paddingTop:10}}>{user.name}</Text>
                        <Text style={{fontSize:hp('1.7%')}}>{user.email}</Text>
                        {club?
                            <View style={{marginTop:10, justifyContent:'center', alignItems:'center', flexDirection:'row',}}>
                                <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center', flex:1}}>
                                    <Icon2 name="shield-account" size={hp('2%')} style={{marginRight:5}}/>
                                    <Text>{club.name_club}</Text>
                                </View>
                            </View>
                        :null}

                        <View style={{width:'100%', height:hp('6.5%'), flexDirection:'row', justifyContent:'space-around', alignItems:'center', paddingTop:10}}>
                            <View style={{flex:1, justifyContent:'center', alignItems:'center', flexDirection:'row', paddingHorizontal:5}}>
                                {user.mycoach?
                                    <View style={{justifyContent:'space-evenly', alignItems:'center', flex:1}}>
                                        <Icon2 name="account-tie" size={hp('4%')}/>
                                        <Text numberOfLines={1} style={{fontSize:hp('1.5%')}}>{user.mycoach.name}</Text>
                                    </View>
                                :
                                    <View style={{flexDirection:'row', justifyContent:'space-evenly', alignItems:'center', flex:1, paddingHorizontal:10,}}>
                                        <Text numberOfLines={1} style={{fontSize:hp('2%'), opacity:0.5}}> {I18n.t('drawer.text7')}</Text>
                                    </View>
                                }
                            </View>
                            <View style={{height:'90%', width:1, backgroundColor: '#d6d6d6',}}/>
                            <View style={{flex:1, justifyContent:'center', alignItems:'center', flexDirection:'row', paddingHorizontal:5}}>
                                {user.myteamplayer?
                                    <View style={{justifyContent:'space-evenly', alignItems:'center', flex:1,}}>
                                        <Icon2 name="account-group-outline" size={hp('4%')}/>
                                        <Text numberOfLines={1} style={{fontSize:hp('1.5%')}}>{user.myteamplayer.name}</Text>
                                    </View>
                                :
                                    <View style={{flexDirection:'row', justifyContent:'space-evenly', alignItems:'center', flex:1, paddingHorizontal:10,}}>
                                        <Text numberOfLines={1} style={{fontSize:hp('2%'), opacity:0.5}}>{I18n.t('drawer.text6')}</Text>
                                    </View>
                                }
                            </View>
                        </View>
                    </View>
                    <View style={{flex:1,paddingTop:10}}>
                        {/* <TouchableOpacity onPress={()=>
                            {Snackbar.show({
                                text: 'Developing',
                                duration: Snackbar.LENGTH_LONG,
                            });}
                        } style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center',marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon2 name="cast-education" size={hp('2.4%')}/>
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text>Tutorial</Text>
                            </View>
                            <View style={{flex:1, alignItems:'center'}}>
                                <Icon2 name="chevron-right" size={hp('2.4%')}/>
                            </View>
                        </TouchableOpacity> */}
                        {/* <TouchableOpacity onPress={()=>
                            {Snackbar.show({
                                text: 'Developing',
                                duration: Snackbar.LENGTH_LONG,
                            });}
                        } style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center', marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon2 name="card-account-details-outline" size={hp('2.4%')}/>
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text>Account</Text>
                            </View>
                            <View style={{flex:1, alignItems:'center'}}>
                                <Icon2 name="chevron-right" size={hp('2.4%')}/>
                            </View>
                        </TouchableOpacity> */}
                        {/* <View style={{width:'100%', height:1, marginTop:10, marginBottom:10, backgroundColor: "#d6d6d6",}}/> */}
                        

                         {/* FAQ  */}

                        <TouchableOpacity onPress={()=> {Faq()}} style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center', marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon name="info-circle" size={hp('2.4%')} style={styles.colorIcon}/>
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text style={{fontSize:hp('2%')}}>{I18n.t('drawer.text5')}</Text>
                            </View>
                            
                        </TouchableOpacity>


{/* 
                        <TouchableOpacity onPress={()=>{console.log('Pagos')}} style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center', marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon name="credit-card" size={hp('2.4%')} style={styles.colorIcon}/>
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text style={{fontSize:hp('2%')}}>Payment</Text>
                            </View>
                           
                        </TouchableOpacity> */}



                        <TouchableOpacity onPress={()=>policy()} style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center', marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon name="lock" size={hp('2.4%')} style={styles.colorIcon} />
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text style={{fontSize:hp('2%')}}>{I18n.t('drawer.text4')}</Text>
                            </View>
                           
                        </TouchableOpacity>


                        <TouchableOpacity onPress={()=>EnviarCoppa()} style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center', marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon name="lock" size={hp('2.4%')} style={styles.colorIcon} />
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text style={{fontSize:hp('2%')}}>{I18n.t('drawer.text1')}</Text>
                            </View>
                            
                        </TouchableOpacity>




                        <TouchableOpacity onPress={()=>contacts()} style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center', marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon2 name="card-account-phone" size={hp('2.4%')} style={styles.colorIcon} />
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text style={{fontSize:hp('2%')}}>{I18n.t('drawer.text2')}</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={()=>logoutBackend()} style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center', marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon2 name="logout" size={hp('2.4%')} style={styles.colorIcon} />
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text style={{fontSize:hp('2%')}}> {I18n.t('drawer.text3')}</Text>
                            </View>
                            <View style={{flex:1, alignItems:'center'}}>
                                <Icon2 name="chevron-right" size={hp('2.4%')}/>
                            </View>
                        </TouchableOpacity>





                    </View>
                    <View style={{flex:0.1, paddingLeft:20, flexDirection:'row', justifyContent:'space-around', alignItems:'center'}}>
                        <View>
                            <Text style={{fontSize:hp('1.2%')}}>V1.0</Text>
                        </View>
                        <Image source={require('../assets/Logo.png')} style={{width:45, height:20}}/>
                    </View>
                </View>
            </View>
    );
};

//make this component available to the app
export default DrawerComponentPlayer;


const styles = StyleSheet.create({
    colorIcon:{
        color:'#8d8d8d'
    }
})