import React, {useState, useEffect} from 'react'
import { View, TouchableOpacity, Text, Image, Vibration, Modal, KeyboardAvoidingView, Platform} from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import Icon from 'react-native-vector-icons/FontAwesome';
import { useTheme, useNavigation } from '@react-navigation/native';
import { url } from '../storage/config';
// import { channel, pusher } from './validationSesion';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { setNotificationChat, setNotificationGlobal } from '../storage/user/dataUser';
import Snackbar from 'react-native-snackbar';
const Sound = require('react-native-sound');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import HomePlayer from "../screens/player/home/home";
import Previa from '../screens/player/home/previa';
import PreviaVideo from '../screens/player/home/previaVideo';
import VideoRe from '../screens/player/home/videoPreviaRe';

import ChatPlayer from "../screens/player/chat/chat";
import MyTeam from '../screens/player/myteam/myteam';
import Ranking from '../screens/player/ranking/ranking';
import ViewPlayer from '../screens/player/ranking/viewPlayer';
import ProfilePublic from '../screens/player/ranking/profilePublic';
import VideoPlayer from '../screens/player/profile/videoPlayer';
import Training from '../screens/player/training/training';
import ProfilePlayer from '../screens/player/profile/profile';
import Skill3DPlayer from '../screens/player/home/3D';
import NotificationPlayer from '../screens/player/global/notification';
import PreviaNotification from '../screens/player/global/PreviaNotification';
import Skill3DTRAINING from '../screens/player/training/3Dtrainig';
import Video from '../screens/player/profile/video';
import Seguridad from '../screens/player/profile/seguridad';

import {signal, canal}  from '../storage/pusher'
import I18n from 'i18n-js';

const Stack = createStackNavigator();


const screenOptionStyle = {
  headerStyle: {
    backgroundColor: "#F3F3F3",
  },
  headerTintColor: "black",
  headerBackTitle: "Back",
};


const Logo = (props) =>{
  const navigation = useNavigation()
  return(
    <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.toggleDrawer()} style={{flex:1,justifyContent: 'center', paddingLeft:10}}>
            <Image source={require('../assets/Logo.png')} style={{width:70, height:30}}/>
    </TouchableOpacity>
  );
}

const Notificacion = (props) =>{
  const [noti, setNoti] = useState(0)
  const [user, setUser] = useState({});
  const [barBadge, setBarBadge] = useState(0);
  const [visible, setVisible] = useState(false);
  const [coach, setCoach] = useState(false);
  const navigation = useNavigation();

  useEffect(async() => {
    const user = await AsyncStorage.getItem('@user');
    const token = await AsyncStorage.getItem('@token');
    const coach = await AsyncStorage.getItem('@coach');
    const coachJson = JSON.parse(coach);
    const userJson = JSON.parse(user);
    const tokenJson = JSON.parse(token);
    setCoach(coachJson)
    getList(tokenJson)
    setUser(userJson);
    // getMessage(tokenJson)
    const unsubscribe = navigation.addListener('focus', async() => {
      const notification = await AsyncStorage.getItem('@notificationGlobal');
      const notificationChat = await AsyncStorage.getItem('@notificationChat');
      const c = JSON.parse(notificationChat);
      const n = JSON.parse(notification);
      setNoti(n);
      setBarBadge(c);
    })

    const canal = signal.subscribe("VFA");
    
    canal.bind("Notification-"+userJson.id, (data) => {
    
      if (data.tag === 'NewMessage') {
        setBarBadge(barBadge => barBadge + 1);
        setNotificationChat(barBadge + 1);
        // Vibration.vibrate(130);
        var whoosh = new Sound('gota.mp3', Sound.MAIN_BUNDLE, (error) => {
          // whoosh.setCurrentTime(1);
          whoosh.play();
        });
      }else if (data.tag === 'ReplyPlayerUpdate'){
        getUser(tokenJson);
      }
      if (data.tag === 'ReplyPlayerYes'){
        return;
      }else{
        if (data.tag === 'NewMessage') {
          return;
        }else{
          if (data.tag === 'UpdatePhoto') {
            return;
          }else{
            setNoti(noti => noti + 1);
            setNotificationGlobal(noti + 1);
            // Vibration.vibrate(130);
            var whoosh = new Sound('gota.mp3', Sound.MAIN_BUNDLE, (error) => {
              // whoosh.setCurrentTime(1);
              whoosh.play();
            });
          }

        }
      }
    });
    return ()=>{
      signal.disconnect();
      // unsubscribe;
    }
  }, [navigation])

  async function getUser(token) {
    await fetch(url + 'api/user', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
    }).then(res => res.json())
    .then(async(dat) => {
        console.warn(dat)
        try {
            if (dat.message == 'Unauthenticated.') {
                LogoutUser()
                setTimeout(() => {
                    navigation.replace('Login')
                }, 1000)
            }else{
                setCoach(dat.mycoach)
            }

        } catch (error) {
            Snackbar.show({
                text: 'Failed to logout',
                duration: Snackbar.LENGTH_LONG,
            });
        }
    })
}

  async function getList(t){
    try {
        await fetch(url+'api/notification/list/notview',{
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '+t
        },
    }).then(res => res.json())
    .then((dat) =>{
        try {
            if (dat.ok) {
                setNoti(dat.data)
            }
        } catch (error) {
            console.warn('error:'+error)
        }
    })
        
    } catch (error) {
        console.warn(error)
    }
  }

  // async function getMessage(t) {
  //   try {
  //     await fetch(url + 'api/chat/message/notview', {
  //       method: 'POST',
  //       headers: {
  //         'Accept': 'application/json',
  //         'Content-Type': 'application/json',
  //         'Authorization': 'Bearer ' + t
  //       },
  //     }).then(res => res.json())
  //       .then((dat) => {
  //         try {
  //           if (dat.ok) {
  //             setBarBadge(dat.data)
  //           }
  //         } catch (error) {
  //           console.warn('error:' + error)
  //         }
  //       })

  //   } catch (error) {
  //     console.warn(error)
  //   }
  // }
  return(
    <View style={{flexDirection:'row', width:100, height:'100%',}}>
      <TouchableOpacity activeOpacity={0.8} onPress={()=>{setNotificationGlobal(0);setNoti(0);navigation.navigate('Notification')}} style={{flex:1, justifyContent: 'center', alignItems:'flex-end',right:20}}>
            <Icon name="bell" size={hp('2.4%')} color="#646469"/>
            {noti>0?
              <View style={{backgroundColor: 'red', width:15, height:15, borderRadius:50, position: 'absolute',right:0, top:10, justifyContent: 'center', alignItems:'center'}}>
                <Text style={{fontSize:hp('1.2%'), color:'#FFF'}}>
                  {noti}
                </Text>
              </View>
            :null}
      </TouchableOpacity>
      {user.coach_id?
        <TouchableOpacity activeOpacity={0.8} onPress={() => { if(coach){setNotificationChat(0); setVisible(true); setBarBadge(0)}else{
          Snackbar.show({
            text: 'You have no assigned coach',
            duration: Snackbar.LENGTH_LONG,
          });
        } }} style={{ justifyContent: 'center', flex: 1, alignItems: 'center', }}>
          <Icon name="comments" size={hp('2.4%')} color="#646469" />
          {barBadge > 0 ?
            <View style={{ backgroundColor: 'red', width: 15, height: 15, borderRadius: 50, position: 'absolute', right: 5, top: 10, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: hp('1.1%'), color: '#FFF' }}>
                {barBadge}
              </Text>
            </View>
            : null}
        </TouchableOpacity>
      :null}
      <Modal 
            animationType="fade"
            onRequestClose={()=>setVisible(false)}
            visible={visible}>
                    <ChatPlayer close={()=>setVisible(false)} style={{flex:1}}/>
            </Modal>
    </View>
  )
}

const Home = () => {
  const { colors } = useTheme();
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Home" component={HomePlayer} options={{
          headerLeft: (props) => {
            return (
                <Logo {...props}/>
            );
          },
          headerTitle:(props) =>{
            return(
              <Text style={{marginLeft:30, fontSize:hp('2.1')}}>
              {I18n.t('tex1')}
              </Text>
            )
          },
          headerRight: (props) =>{
            return(
              <Notificacion {...props} />
            )
          }
      }} />
      <Stack.Screen name="Previa" component={Previa}/>
      <Stack.Screen name="PreviaVideo" component={PreviaVideo}/>
      <Stack.Screen name="3D" component={Skill3DPlayer}/>
      <Stack.Screen name="Notification" component={NotificationPlayer}/>
      <Stack.Screen name="previaNotification" component={PreviaNotification}/>
      <Stack.Screen name="Chat" component={ChatPlayer} />
      <Stack.Screen name="VideoRe" component={VideoRe}/>
    </Stack.Navigator>
  );
}

const MyTeams = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="MyTeamPlayer" component={MyTeam} options={{
          headerLeft: (props) => {
            return (
                <Logo {...props}/>
            );
          },
          headerTitle:(props) =>{
            return(
              <Text style={{marginLeft:30, fontSize:hp('2.1')}}>
                 {I18n.t('tex2')}
              </Text>
            )
          },
          headerRight: (props) =>{
            return(
              <Notificacion {...props} />
            )
          }
      }} />
      <Stack.Screen name="ProfilePublic" component={ProfilePublic}/>
      <Stack.Screen name="Video Player" component={VideoPlayer}/>
      <Stack.Screen name="Video" component={Video}/>
      <Stack.Screen name="Notification" component={NotificationPlayer}/>
      <Stack.Screen name="previaNotification" component={PreviaNotification}/>
      <Stack.Screen name="3D" component={Skill3DPlayer}/>
      
     
    </Stack.Navigator>
  );
}

const Rankings = () => {
    return(
        <Stack.Navigator screenOptions={screenOptionStyle}>
            <Stack.Screen name="Ranking" component={Ranking} options={{
                headerLeft: (props) => {
                  return (
                      <Logo {...props}/>
                  );
                },
                headerTitle:(props) =>{
                  return(
                    <Text style={{marginLeft:30, fontSize:hp('2.1')}}>
                      {I18n.t('tex3')}
                    </Text>
                  )
                },
                headerRight: (props) =>{
                  return(
                    <Notificacion {...props} />
                  )
                }
            }} />
            <Stack.Screen name="ProfilePublic" component={ProfilePublic}/>
            <Stack.Screen name="Video Player" component={VideoPlayer}/>
            <Stack.Screen name="Video" component={Video}/>
            <Stack.Screen name="Featured Players" component={ViewPlayer}/>
            <Stack.Screen name="Notification" component={NotificationPlayer}/>
            <Stack.Screen name="previaNotification" component={PreviaNotification}/>
            <Stack.Screen name="3D" component={Skill3DPlayer}/>
        </Stack.Navigator>
    );
}

const Trainings = () => {
    return(
        <Stack.Navigator screenOptions={screenOptionStyle}>
            <Stack.Screen name="Training" component={Training} options={{
                headerLeft: (props) => {
                  return (
                      <Logo {...props}/>
                  );
                },
                headerTitle:(props) =>{
                  return(
                    <Text style={{marginLeft:30, fontSize:hp('2.1')}}>
                      {I18n.t('tex4')}
                    </Text>
                  )
                },
                headerRight: (props) =>{
                  return(
                    <Notificacion {...props} />
                  )
                }
            }} />
            <Stack.Screen name="3DTraining" component={Skill3DTRAINING}/>
            <Stack.Screen name="Notification" component={NotificationPlayer}/>
            <Stack.Screen name="previaNotification" component={PreviaNotification}/>
            <Stack.Screen name="3D" component={Skill3DPlayer}/>
        </Stack.Navigator>
    );
}

const Profile = () => {
    return(
        <Stack.Navigator screenOptions={screenOptionStyle}>
            <Stack.Screen name="Profile" component={ProfilePlayer} options={{
                headerLeft: (props) => {
                  return (
                      <Logo {...props}/>
                  );
                },
                headerTitle:(props) =>{
                  return(
                    <Text style={{marginLeft:30, fontSize:hp('2.1')}}>
                      {I18n.t('tex5')}
                    </Text>
                  )
                },
                headerRight: (props) =>{
                  return(
                    <Notificacion {...props} />
                  )
                }
            }} />
            <Stack.Screen name="Video Profile" component={VideoPlayer}/>
            <Stack.Screen name="Video" component={Video} />
            <Stack.Screen name="Account security" component={Seguridad} />
            <Stack.Screen name="Notification" component={NotificationPlayer}/>
            <Stack.Screen name="previaNotification" component={PreviaNotification}/>
            <Stack.Screen name="3D" component={Skill3DPlayer}/>

        </Stack.Navigator>
    );
}

export { Home, Rankings, Trainings, Profile, MyTeams };