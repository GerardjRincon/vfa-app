import React, { useState, useEffect } from 'react'
import { View, TouchableOpacity, Text, Image, Animated, Dimensions, TextInput, Vibration } from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import Icon from 'react-native-vector-icons/FontAwesome';
import { useTheme, useNavigation } from '@react-navigation/native';
const { width, height } = Dimensions.get('screen');
import AsyncStorage from '@react-native-async-storage/async-storage';
import { setNotificationGlobal, setNotificationChat } from '../storage/user/dataUser';
import { url } from '../storage/config';
// import { channel } from './DrawerCoach';
const Sound = require('react-native-sound');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import {signal, canal} from '../storage/pusher';

///// View Global
import Notification from '../screens/coach/global/notification';
import VideoQualification from '../screens/coach/global/notification/videoQualification';
import Chat from '../screens/coach/global/chat';
import ChatItem from '../screens/coach/global/chatItem';
///// Finish view global

///// View from Home
import Home from "../screens/coach/home/home"; // index
import DetailsSkill from "../screens/coach/home/detailsSkill";
import Skill3D from "../screens/coach/home/skill3d";
import DetailsExercises from "../screens/coach/home/detailsExercises";
import Exercise3D from "../screens/coach/home/exercise3d";
///// Finish view from Home


///// View from training
import Training from '../screens/coach/training/training'; // index
///// Finish view from training


///// View from Reviews
import Reviews from '../screens/coach/reviews/review';
import Video from '../screens/coach/reviews/videoPlayer';
///// Finish view from Reviews


///// View from My Team
import MyTeam from '../screens/coach/myteam/myteam';
import Perfil from '../screens/coach/myteam/perfil';
///// Finish view My Team


///// View from Stats
import Stats from '../screens/coach/stats/stats';
///// Finish view Stats
import I18n from 'i18n-js';


const Stack = createStackNavigator();


const screenOptionStyle = {
  headerStyle: {
    backgroundColor: "#FFF",
  },
  headerTintColor: "black",
  headerBackTitle: "Back",
};

const Header = (props) => {
  const { colors } = useTheme();
  return (
    <View style={{ height: 56, backgroundColor: '#F3F3F3', justifyContent: 'center', flexDirection: 'row', borderBottomWidth: 1, borderColor: "#D6D6D6", elevation: 3, top: 10, position: "absolute", }}>
      <TouchableOpacity style={{ flex: 1, justifyContent: 'center', paddingLeft: 10 }}>
        <Image source={require('../assets/Logo.png')} style={{ width: 75, height: 30 }} />
      </TouchableOpacity>
      <View style={{ flex: 3, justifyContent: 'center', }}>
        <Text style={{ color: colors.text, fontSize: 17 }}>Home</Text>
      </View>
      <TouchableOpacity style={{ justifyContent: 'center', width: 70, alignItems: 'center', }}>
        <Icon name="bell" size={24} color="#646469" />
      </TouchableOpacity>
    </View>
  );
}

const Logo = (props) => {
  const navigation = useNavigation()
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.toggleDrawer()} style={{ flex: 1, justifyContent: 'center', paddingLeft: 10 }}>
        <Image source={require('../assets/Logo.png')} style={{width:70, height:30}}/>
    </TouchableOpacity>
  );
}

const Notificacion = (props) => {
  const [noti, setNoti] = useState(0);
  const [chat, setChat] = useState(0);
  const navigation = useNavigation();

  useEffect(async () => {
    const user = await AsyncStorage.getItem('@user');
    const token = await AsyncStorage.getItem('@token');
    const userJson = JSON.parse(user);
    const tokenJson = JSON.parse(token);
    getNotification(tokenJson)
    // getMessage(tokenJson)
    const unsubscribe = navigation.addListener('focus', async() => {
      const notification = await AsyncStorage.getItem('@notificationGlobal');
      const notificationChat = await AsyncStorage.getItem('@notificationChat');
      const c = JSON.parse(notificationChat);
      const n = JSON.parse(notification);
      setNoti(n);
      setChat(c);
    });
    canal.bind("Notification-" + userJson.id, (data) => {
      // console.warn(data)
      if (data.tag === 'SkillAssignedCoach') {
        return;
      }else{
        if (data.tag == 'UpdatePhoto') {
          return;
        }else{

          if (data.tag == 'CalendarAssigned') {
            return;
          }else{
            if (data.tag === 'NewMessage') {
              setChat(chat => chat + 1);
              setNotificationChat(chat + 1);
            } else {
              setNoti(noti => noti + 1);
              setNotificationGlobal(noti + 1);
            }
            // Vibration.vibrate(130);
            var whoosh = new Sound('gota.mp3', Sound.MAIN_BUNDLE, (error) => {
              // whoosh.setCurrentTime(1);
              whoosh.play();
            });
          }
        }
      }
    });
    // return ()=>{
    //   pusher.disconnect();
    //   unsubscribe;
    // }
  }, [])

  async function getNotification(t) {
    try {
      await fetch(url + 'api/notification/list/notview', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + t
        },
      }).then(res => res.json())
        .then((dat) => {
          try {
            if (dat.ok) {
              setNoti(dat.data)
            }
          } catch (error) {
            console.warn('error:' + error)
          }
        })

    } catch (error) {
      console.warn(error)
    }
  }

  // async function getMessage(t) {
  //   try {
  //     await fetch(url + 'api/chat/message/notview', {
  //       method: 'POST',
  //       headers: {
  //         'Accept': 'application/json',
  //         'Content-Type': 'application/json',
  //         'Authorization': 'Bearer ' + t
  //       },
  //     }).then(res => res.json())
  //       .then((dat) => {
  //         try {
  //           if (dat.ok) {
  //             setChat(dat.data)
  //           }
  //         } catch (error) {
  //           console.warn('error:' + error)
  //         }
  //       })

  //   } catch (error) {
  //     console.warn(error)
  //   }
  // }

  return (
    <View style={{ flexDirection: 'row', width: 100, height: '100%' }}>
      <TouchableOpacity activeOpacity={0.8} onPress={() => { setNotificationGlobal(0); setNoti(0); navigation.navigate('Notification'); }} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
        <Icon name="bell" size={hp('2.4%')} color="#646469" />
        {noti > 0 ?
          <View style={{ backgroundColor: 'red', width: 15, height: 15, borderRadius: 50, position: 'absolute', right: 5, top: 10, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: hp('1.1%'), color: '#FFF' }}>
              {noti}
            </Text>
          </View>
          : null}
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={0.8} onPress={() => {setNotificationChat(0); setChat(0); navigation.navigate('Chat'); }} style={{ justifyContent: 'center', flex: 1, alignItems: 'center', }}>
        <Icon name="comments" size={hp('2.4%')} color="#646469" />
        {chat > 0 ?
          <View style={{ backgroundColor: 'red', width: 15, height: 15, borderRadius: 50, position: 'absolute', right: 5, top: 10, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: hp('1.1%'), color: '#FFF' }}>
              {chat}
            </Text>
          </View>
          : null}
      </TouchableOpacity>
    </View>
  )
}


const MainStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Home" component={Home} options={{
        headerLeft: (props) => {
          return (
            <Logo {...props} />
          );
        },
        headerTitle: (props) => {
          return (
            <Text style={{ marginLeft: 30, fontSize: hp('2.1') }}>
              {I18n.t('tex1')}
            </Text>
          )
        },
        headerRight: (props) => {
          return (
            <Notificacion {...props} />
          )
        }

      }} />
      <Stack.Screen name="Details skill" component={DetailsSkill} />
      <Stack.Screen name="Skill 3D" component={Skill3D} />
      <Stack.Screen name="Details exercises" component={DetailsExercises} />
      <Stack.Screen name="Exercise 3D" component={Exercise3D} />
      <Stack.Screen name="Notification" component={Notification} />
      <Stack.Screen name="Chat" component={Chat} />
      <Stack.Screen name="QualificationVideo" component={VideoQualification} />
      <Stack.Screen name="ChatItem" component={ChatItem} />
      <Stack.Screen name="Video player" component={Video} />
    </Stack.Navigator>
  );
}

const Stat = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Stats" component={Stats} options={{
        headerLeft: (props) => {
          return (
            <Logo {...props} />
          );
        },
        headerTitle: (props) => {
          return (
            <Text style={{ marginLeft: 30, fontSize: hp('2.1') }}>
              {I18n.t('tex10')}
            </Text>
          )
        },
        headerRight: (props) => {
          return (
            <Notificacion {...props} />
          )
        }
      }} />

      <Stack.Screen name="Notification" component={Notification} />
      <Stack.Screen name="Chat" component={Chat} />
      <Stack.Screen name="Video player" component={Video} />
      <Stack.Screen name="QualificationVideo" component={VideoQualification} />
      


    </Stack.Navigator>
  );
}

const Review = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Review" component={Reviews} options={{
        headerLeft: (props) => {
          return (
            <Logo {...props} />
          );
        },
        headerTitle: (props) => {
          return (
            <Text style={{ marginLeft: 30, fontSize: hp('2.1') }}>
              {I18n.t('tex11')}
            </Text>
          )
        },
        headerRight: (props) => {
          return (
            <Notificacion {...props} />
          )
        }
      }} />
      <Stack.Screen name="Notification" component={Notification} />
      <Stack.Screen name="Chat" component={Chat} />
      <Stack.Screen name="Review player" component={Video} />
      <Stack.Screen name="Video player" component={Video} />
      <Stack.Screen name="QualificationVideo" component={VideoQualification} />
    </Stack.Navigator>
  );
}

const Trainings = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Training" component={Training} options={{
        headerLeft: (props) => {
          return (
            <Logo {...props} />
          );
        },
        headerTitle: (props) => {
          return (
            <Text style={{ marginLeft: 30, fontSize: hp('2.1') }}>
              
              {I18n.t('tex4')}

              </Text>
          )
        },
        headerRight: (props) => {
          return (
            <Notificacion {...props} />
          )
        }

      }} />
      <Stack.Screen name="Notification" component={Notification} />
      <Stack.Screen name="Chat" component={Chat} />
      <Stack.Screen name="3D Skill Training" component={Skill3D} />
      <Stack.Screen name="3D Exercises Training" component={Exercise3D} />
      <Stack.Screen name="Video player" component={Video} />
      <Stack.Screen name="QualificationVideo" component={VideoQualification} />
    </Stack.Navigator>
  );
}

const MyTeams = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="My Team" component={MyTeam} options={{
        headerLeft: (props) => {
          return (
            <Logo {...props} />
          );
        },
        headerTitle: (props) => {
          return (
            <Text style={{ marginLeft: 30, fontSize: hp('2.1') }}>
              {I18n.t('tex2')}
            </Text>
          )
        },
        headerRight: (props) => {
          return (
            <Notificacion {...props} />
          )
        }
      }} />
      <Stack.Screen name="Notification" component={Notification} />
      <Stack.Screen name="Chat" component={Chat} />
      <Stack.Screen name="Player Profile" component={Perfil} />
      <Stack.Screen name="Review player MyTeam" component={Video} />
      <Stack.Screen name="Video player" component={Video} />
      <Stack.Screen name="QualificationVideo" component={VideoQualification} />
    </Stack.Navigator>
  );
}

export { MainStackNavigator, Stat, Review, Trainings, MyTeams };