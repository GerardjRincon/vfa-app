//import liraries
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Linking} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import Pusher from 'pusher-js/react-native'
import { url } from '../storage/config';
import { LogoutUser } from '../storage/user/dataUser';
import CheckBox from '@react-native-community/checkbox';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { setTutorialHome, setTutorial3D, setTeam, setCoach, setUser, setClub } from '../storage/user/dataUser';


import {signal, canal} from '../storage/pusher'
import I18n from 'i18n-js';

// export const pusher = new Pusher('fd03f070445c44912dc6', {
//     cluster: 'us2',
//   });
// export const channel = pusher.subscribe("VFA");


// create a component
const DrawerComponent = (props) => {
    const navigation = useNavigation();
    const [token, setToken] = useState(null);
    const [toggleCheckBox, setToggleCheckBox] = useState(false);
    const [toggleCheckBox3D, setToggleCheckBox3D] = useState(false);
    const [team, setT] = useState({});
    const [coach, setC] = useState({});
    const [club, setCLUB] = useState({});
    const [countPlayer, setCountPlayer] = useState(0);
    const [countTeams, setCountTeams] = useState(0);
    const [user, setU] = useState(props.user);
    const imagenUser = 'https://go.vfa.app/'+user.photo;

    useEffect(async() => {
        const unsub = navigation.addListener('focus', async() => {
            const tutorial = await AsyncStorage.getItem('@tutorialHome');
            const tutorial3D = await AsyncStorage.getItem('@tutorial3D');
            const tuto3D = JSON.parse(tutorial3D);
            const tutorialHome = JSON.parse(tutorial);

            setToggleCheckBox3D(tuto3D);
            setToggleCheckBox(tutorialHome);
        });
        const token = await AsyncStorage.getItem('@token');
        const user = await AsyncStorage.getItem('@user');
        const userJson = JSON.parse(user);
        const tokenJson = JSON.parse(token);
        setToken(tokenJson);
        getUser(tokenJson)

        const canal = signal.subscribe("VFA");

        
        canal.bind("NotificationPlayerOutTeam-"+userJson.id, (data) => {
            getUser(tokenJson);
            console.log("se actualizo el coach menu");
        });

        canal.bind("NotificationUpdateData-"+userJson.id, (data) => {
            getUser(tokenJson);
            console.log("se actualizo el coach menu");
        });


        canal.bind("Notification-"+userJson.id, (data) => {
            if (data.tag == 'ReplyPlayerUpdate' || data.tag == 'UpdatePhoto' ) {
                if (data.tag == 'UpdatePhoto') {
                    setU(data.noti)
                }else{
                    getUser(tokenJson);
                }
            }
        });
        return ()=>{
            // canal.disconnect();
            // canal.unsubscribe("VFA");
            // unsub;
        }
    }, [navigation])



      async function logoutBackend(){
        await fetch(url + 'api/logout', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
        }).then(res => res.json())
        .then(async(dat) => {
            console.warn(dat)
            try {
                if (dat.ok) {
                    const keys = ['@user', '@token', '@skills', '@reviews', '@team',]
                    signal.unsubscribe("VFA");




                    await AsyncStorage.multiRemove(keys)
                    navigation.replace('Login');
                }
                if (dat.message == 'Unauthenticated.') {
                    LogoutUser()
                    setTimeout(() => {
                        navigation.replace('Login')
                    }, 1000)
                }
            } catch (error) {
                {Snackbar.show({
                    text: 'Failed to logout',
                    duration: Snackbar.LENGTH_LONG,
                });}
            }
        })
    }

    async function getUser(token) {
        await fetch(url + 'api/user', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
        }).then(res => res.json())
        .then(async(dat) => {
            console.log(dat)
            try {
                if (dat.message == 'Unauthenticated.') {
                    LogoutUser()
                    setTimeout(() => {
                        navigation.replace('Login')
                    }, 1000)
                }else{
                    setUser(dat);
                    setTeam(dat.myteam);
                    setClub(dat.club);
                    setCLUB(dat.club)
                    setCountPlayer(dat.playerscount);
                    setCountTeams(dat.teamcount);
                }

            } catch (error) {
                Snackbar.show({
                    text: 'Failed to logout',
                    duration: Snackbar.LENGTH_LONG,
                });
            }
        })
    }


    const policy = async () => {
        try {
          await Linking.openURL('https://www.websitepolicies.com/policies/view/0NueZaX0');
        } catch (e) {
          console.warn(e)
        }
      };


      const contacts = async () => {
        try {
          await Linking.openURL('https://vfa.app/contact-us/');
        } catch (e) {
          console.warn(e)
        }
      };

      const Faq = async () => {
        try {
            await Linking.openURL('https://vfa.app/faq-coach');
          } catch (e) {
          }
      }

      const CoachGuide = async () => {
        try {
            await Linking.openURL('https://go.vfa.app/vfa-coach-guide.pdf');
          } catch (e) {
          }
      }
     

    return (
        <View style={{flex:1, backgroundColor: '#F3F3F3',}}>
                 <Image source={require('../assets/pelota.png')} style={{
                    height: '100%',
                    width: '100%', 
                    position: "absolute",
                }} />
                <View style={{flex:1}}>
                    <View style={{width:'100%',justifyContent:'center', alignItems:'center', paddingTop:20, paddingBottom:20, borderBottomWidth:1, borderColor:'#d6d6d6'}}>
                        {user.photo?
                            <Image source={{uri:imagenUser}} resizeMode="cover" style={{width:hp('13%'), height:hp('13%'), backgroundColor: "#FFF", borderRadius:hp('13%'), justifyContent:'center', alignItems:'center'}}/>
                            :
                            <View style={{width:hp('13%'), height:hp('13%'), backgroundColor: "#FFF", borderRadius:hp('13%'), justifyContent:'center', alignItems:'center'}}>
                                <Text style={{fontSize:hp('4%'), fontWeight:'bold'}}>{user.name.charAt(0)}</Text>
                            </View>
                        }
                        <Text style={{fontSize:hp('1.7%'), paddingTop:10}}>{user.name}</Text>
                        <Text>{user.email}</Text>
                        <View style={{width:'100%', height:club?hp('12%'):hp('6.5%')}}>
                            {club?
                                <View style={{flex:1, marginTop:10, justifyContent:'center', alignItems:'center', flexDirection:'row',}}>
                                    <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center', flex:1}}>
                                        <Icon2 name="shield-account" size={hp('2%')} style={{marginRight:5}}/>
                                        <Text>{club.name_club}</Text>
                                    </View>
                                </View>
                            :null}
                            <View style={{width:'100%', height:hp('6.5%'), flexDirection:'row', justifyContent:'space-around', alignItems:'center', paddingTop:10}}>
                                <View style={{flex:1, justifyContent:'center', alignItems:'center', flexDirection:'row',}}>
                                    <View style={{flexDirection:'row', justifyContent:'space-evenly', alignItems:'center', flex:1}}>
                                        <Icon2 name="soccer" size={hp('4%')}/>
                                        <Text>{I18n.t('drawer.text8')}</Text>
                                        <Text>{countPlayer}</Text>
                                    </View>
                                </View>
                                <View style={{height:'90%', width:1, backgroundColor: '#d6d6d6',}}/>
                                <View style={{flex:1, justifyContent:'center', alignItems:'center', flexDirection:'row',}}>
                                    <View style={{flexDirection:'row', justifyContent:'space-evenly', alignItems:'center', flex:1}}>
                                        <Icon2 name="account-group-outline" size={hp('4%')}/>
                                        <Text> {I18n.t('drawer.text9')}</Text>
                                        <Text>{countTeams}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{flex:1,paddingTop:10}}>

                    <TouchableOpacity onPress={()=>navigation.navigate('ProfileController')} style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center', marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon2 name="account-cog" size={hp('2.4%')}/>
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text> {I18n.t('drawer.text10')}</Text>
                            </View>
                            <View style={{flex:1, alignItems:'center'}}>
                                <Icon2 name="chevron-right" size={hp('2.4%')}/>
                            </View>
                        </TouchableOpacity>


                        <TouchableOpacity onPress={()=>navigation.navigate('TeamsController')} style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center',marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon2 name="account-group-outline" size={hp('2.4%')}/>
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text> {I18n.t('drawer.text11')}</Text>
                            </View>
                            <View style={{flex:1, alignItems:'center'}}>
                                <Icon2 name="chevron-right" size={hp('2.4%')}/>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={()=>CoachGuide()} style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center', marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon2 name="file" size={hp('2.4%')} style={styles.colorIcon} />
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text>{I18n.t('drawer.text12')}</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={()=>contacts()} style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center', marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon2 name="card-account-phone" size={hp('2.4%')} style={styles.colorIcon} />
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text >{I18n.t('drawer.text2')}</Text>
                            </View>
                        </TouchableOpacity>


                        <TouchableOpacity onPress={()=> {Faq()}} style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center', marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon name="info-circle" size={hp('2.4%')} style={styles.colorIcon}/>
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text>{I18n.t('drawer.text5')}</Text>
                            </View>
                        </TouchableOpacity>


                        <View  style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center',marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon2 name="cast-education" size={hp('2.4%')}/>
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text> {I18n.t('drawer.text13')}</Text>
                            </View>
                            <View style={{flex:1, alignItems:'center'}}>
                                <CheckBox
                                    disabled={false}
                                    value={toggleCheckBox}
                                    onValueChange={(newValue) => {
                                        console.warn(newValue)
                                        setToggleCheckBox(newValue)
                                        setTutorialHome(newValue)
                                    }}
                                />
                            </View>
                        </View>
                        <View  style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center',marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon2 name="rotate-3d" size={hp('2.4%')}/>
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text>{I18n.t('drawer.text14')}</Text>
                            </View>
                            <View style={{flex:1, alignItems:'center'}}>
                                <CheckBox
                                    disabled={false}
                                    value={toggleCheckBox3D}
                                    onValueChange={(item) => {
                                        console.warn(item)
                                        setToggleCheckBox3D(item)
                                        setTutorial3D(item)
                                    }}
                                />
                            </View>
                        </View>
                        
                        <View style={{width:'100%', height:1, marginTop:10, marginBottom:10, backgroundColor: "#d6d6d6",}}/>
                        <TouchableOpacity onPress={()=>logoutBackend()} style={{width:'100%', height:hp('5%'), flexDirection:'row', justifyContent:'flex-start', alignItems:'center', marginBottom:10}}>
                            <View style={{width:'15%', alignItems:'center'}}>
                                <Icon2 name="logout" size={hp('2.4%')}/>
                            </View>
                            <View style={{flex:2, flexDirection:'row', alignItems:'center'}}>
                                <Text>{I18n.t('drawer.text15')}</Text>
                            </View>
                            <View style={{flex:1, alignItems:'center'}}>
                                <Icon2 name="chevron-right" size={hp('2.4%')}/>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:0.1, paddingLeft:20, flexDirection:'row', justifyContent:'space-around', alignItems:'center'}}>
                        <View>
                            <Text style={{fontSize:hp('1.2%')}}>V1.0</Text>
                        </View>
                        <Image source={require('../assets/Logo.png')} style={{width:45, height:20}}/>
                    </View>
                </View>
            </View>
    );
};



const styles = StyleSheet.create({
    colorIcon:{
        color:'#8d8d8d'
    }
})
//make this component available to the app
export default DrawerComponent;
