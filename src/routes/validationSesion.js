import React, { useState, useEffect } from 'react'
import { Text, View, Button, StyleSheet, FlatList } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { BottomTabNavigatorPlayer } from "./bottomTabNavigatorPlayer";
import { BottomTabNavigatorCoach } from './bottomTabNavigatorCoach';
import { createDrawerNavigator } from "@react-navigation/drawer";
///// Drawer menu Coach
import DrawerComponent from './DrawerCoach';
import DrawerComponentPlayer from './DrawerPlayer';

//// View Coach Menu
import { TeamsController } from '../screens/coach/global/drawer/teamsController';
import { ProfileController } from '../screens/coach/global/drawer/profileController';

//// View Welcome player
import WelcomePlayerH from '../screens/player/home/welcome';

import PayAppCoach from '../screens/payCoach';
import PayAppPlayer from '../screens/payPlayer';
import PayAppPlayerDiscount from '../screens/payPlayerDiscount';
import PayAppCoachDiscount from '../screens/payCoachDiscount';

import Pusher from 'pusher-js/react-native'

// Import de la raiz del pusher 

import {signal, canal } from '../storage/pusher';



// export const pusher = new Pusher('fd03f070445c44912dc6', {
//     cluster: 'us2',
//   });
  
// export const channel = pusher.subscribe("VFA");


const Drawer = createDrawerNavigator();

const validationSesion = ({ navigation, route }) => {
    const { user } = route.params;
    const [sesion, setSesion] = useState(null);
    const [visibleWelcome, setVisibleWelcome] = useState(false);

    const [validatePusher, setValidatePusher] = useState(false);

    useEffect(async() => {

       
    //     ACTIVAR PUSHER PARA LA PRODUCCION  
       

        
        console.log(signal,'---- Activo pusher 1---');
        console.log(canal,'---- Activo pusher  canal 1---');
     
        const canal = signal.subscribe("VFA");


        canal.bind("Notificationxx-155", (data) => {
            console.log(signal,'---- Activo pusher 155 ---');
            console.log(canal,'---- Activo pusher  canal---');
            console.log("Llego del validation sesion");
            // console.log(data)
         })


         canal.bind("Notificationxx-20", (data) => {
            console.log(signal,'---- Activo pusher 20 ---');
            console.log(canal,'---- Activo pusher  canal---');
            console.log("Llego del validation sesion");
            // console.log(data)
         })




         signal.connection.bind('state_change', function(states) {
           console.log('----', states,'---- del pucher ')
          });


          let instancia = signal.connection.bind('disconnected');
          console.log(instancia, '--- mi instancia');

    


        let isMount = true;
        if (user.type) {
            if (isMount){
                const WpH = await AsyncStorage.getItem('@welcomePlayerHome');
                const welcomePlayerHomeConst = JSON.parse(WpH);
                if (!welcomePlayerHomeConst) {
                    setVisibleWelcome(true);
                    console.warn(visibleWelcome)
                }
                setSesion(user.type);

            }
        }
        return () => { isMount = false };


        // const unsubscribe = navigation.addListener('focus', async() => {
        // })
        // return unsubscribe;
        // return ()=>{
        //     signal.disconnect();
        //     signal.unsubscribe();
        //   }


    }, [])
    
    

    return (

        sesion === 'player' ?
            (
                <Drawer.Navigator
                    initialRouteName={user.pay?"Home":
                    user.state_cupon?
                    "Pay Discount"
                    :"Pay app player"
                    }
                    drawerStyle={{ width: '65%' }}
                    drawerContent={(props) => <DrawerComponentPlayer user={user} {...props} />}>
                        
                    <Drawer.Screen name="Pay app player" component={PayAppPlayer} />
                    <Drawer.Screen name="Pay Discount" component={PayAppPlayerDiscount} />
                    <Drawer.Screen name="Home" component={BottomTabNavigatorPlayer} />

                </Drawer.Navigator>

            )
            :sesion === 'coach'?
            (
                <Drawer.Navigator
                    initialRouteName={user.pay?"Home":
                    user.state_cupon?
                    "Pay Discount coach"
                    :"Pay app coach"
                    }
                    
                    drawerStyle={{ width: '65%' }}
                    drawerContent={(props) => <DrawerComponent user={user} {...props} />}
                >
                    <Drawer.Screen name="Pay app coach" component={PayAppCoach} />
                    <Drawer.Screen name="Pay Discount coach" component={PayAppCoachDiscount} />
                    <Drawer.Screen name="Home" component={BottomTabNavigatorCoach} />
                    <Drawer.Screen name="TeamsController" component={TeamsController} />
                    <Drawer.Screen name="ProfileController" component={ProfileController} />
                </Drawer.Navigator>
            )
            :null
    );
};

export default validationSesion;