import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity, Vibration } from 'react-native';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Home, Rankings, Trainings, Profile, MyTeams } from "./stackNavigatorPlayer";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useTheme, useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';
import I18n from 'react-native-i18n';
import { setNotificationChat, setUser } from '../storage/user/dataUser';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import UnityView, { MessageHandler, UnityModule } from '@asmadsen/react-native-unity-view';

// import { canal } from './validationSesion';


import {signal, canal} from '../storage/pusher';

const Tab = createBottomTabNavigator();

const screenOptionStyle = {
  paddingBottom: 10,
};






  function MyTabBar({ state, descriptors, navigation }) {
  const theme = useTheme();
  const [barBadge, setBarBadge] = useState(0);
  const [user, setU] = useState({});
  // const [userCoach, setUserCoach] = useState(false)
  const [status, setStatus] = useState(false)



  

  useEffect(async() => {


      let isMount = true;
      if (isMount) {
        const userasync = await AsyncStorage.getItem('@user');
        const userJson = JSON.parse(userasync);
        setU(userJson)
      
      }

    return ()=>{
      isMount = false;
    }



  }, [])


  return (
    <View style={{ flexDirection: 'row', backgroundColor:'white', borderTopColor:"#cfcfcf", borderTopWidth:1, height: hp('7%'), justifyContent: 'space-around', alignItems: 'center' }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
              ? options.title
              : route.name;

        const icon = options.icono;
        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }

        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{ justifyContent: 'center', alignItems: 'center', }}
            key={index}
          >
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Icon name={icon} color={isFocused ? 'green' : '#787878'} size={hp('2.5')} />
                <Text style={{ color: isFocused ? 'green' : '#787878',fontSize:hp('1.2') }}>
                  {label}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const BottomTabNavigatorPlayer = () => {





  function back() {
    UnityModule.postMessage('ReactManager', 'GetReactMessage', 'HideAR')
    UnityModule.postMessage('ReactManager', 'GetReactMessage', 'Clear3D');
  }
  
  const [user, setUserTab] = useState({});


  useEffect(async() => {

    const canal = signal.subscribe("VFA");


  //   canal.bind("Notificationxx", (data) => {
  //     console.log("se activop un evento en el botton");
  //     console.log(data)
  // })



      //     ACTIVAR PUSHER PARA LA PRODUCCION  
       
 


    // let isMount = true;
    // if (isMount) {
      const userasync = await AsyncStorage.getItem('@user');
      const userJson = JSON.parse(userasync);

      setUserTab(userJson);
      
      // AsyncStorage.getItem('@user').
      // then(res => {
      //   const datos = JSON.parse(res);
      //   setUserTab(datos);
      // })

      // // setUserTab(userJson)

      // console.log(user, '---user 1')
         console.log(userJson, '---user 2222 ----')

    // }
        console.log(canal, 'canal puhser button');
        console.log("Entre al componente")

        canal.bind("NotificationReplyPlayerUpdate-"+userJson.id, (data) =>{
        console.log(data.noti, '---- lo que llega');
        setUserTab(data.noti);
        console.log('Estos son los datos del data', user);

      //  setTimeout(() => {
      //    AsyncStorage.getItem('@user')
      //    .then(res => {
      //      let datos = JSON.parse(res);
      //      setUserTab(datos);
      //      console.log('Estos son los datos del data', datos);
      //    })
      //  }, 700)


        // const userasync = await AsyncStorage.getItem('@user');
        // const userJson = JSON.parse(userasync);
        // setUserTab(userJson)

     
     })


    
    // return ()=>{
    //   isMount = false;
    // }


    return ()=>{
      signal.disconnect();
      // pusher.unsubscribe("VFA");
  }


  }, [])

  return (
    <Tab.Navigator  tabBar={props => <MyTabBar {...props} />} initialRouteName="Home" tabBarOptions={{
      activeTintColor: '#e91e63',
    }}>
      <Tab.Screen
        name="Home"
        component={Home}
        listeners={{
          tabPress: (e) => {
            back();
          },
        }}
        options={{
          unmountOnBlur: true,
          tabBarLabel: I18n.t('bottomMenu.Home'),
          icono: 'home',
        }}
        initialParams={{ user:user }}
      />
      {/* <Tab.Screen
        name="Chat"
        component={Chat}
        tabPress={() => console.warn('presionado')}
        options={{
          tabBarLabel: 'Chat',
          icono: 'comments',
        }}
      /> */}
      <Tab.Screen
        name="Ranking"
        component={Rankings}
        listeners={{
          tabPress: (e) => {
            back();
          },
        }}
        options={{
          tabBarLabel: I18n.t('bottomMenu.Ranking'),
          icono: 'trophy'
        }}
      />
      {user.coach_id?
        <Tab.Screen
          name="My Team"
          component={MyTeams}
          listeners={{
            tabPress: (e) => {
              back();
            },
          }}
          options={{
            tabBarLabel: I18n.t('bottomMenu.My_Team'),
            icono: 'users',
          }}
        />
      :null}

      {user.coach_id?
        <Tab.Screen
          name="Training"
          component={Trainings}
          listeners={{
            tabPress: (e) => {
              back();
            },
          }}
          options={{
            tabBarLabel: I18n.t('bottomMenu.Training'),
            icono: 'calendar'
          }}
        />
      :null}
      <Tab.Screen
      
        name="Profile"
        component={Profile}
        listeners={{
          tabPress: (e) => {
            back();
          },
        }}
        options={{
          // unmountOnBlur: true,
          tabBarLabel: I18n.t('bottomMenu.Profile'),
          icono: 'user-circle',
        }}
      />
    </Tab.Navigator>
  );
};

export { BottomTabNavigatorPlayer };