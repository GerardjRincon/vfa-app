import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MainStackNavigator, Stat, Review, Trainings, MyTeams } from "./stackNavigatorCoach";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useTheme } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import I18n from 'react-native-i18n';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import UnityView, { MessageHandler, UnityModule } from '@asmadsen/react-native-unity-view';
const Tab = createBottomTabNavigator();

const screenOptionStyle = {
  paddingBottom: 10,
};

function MyTabBar({ state, descriptors, navigation }) {
  const theme = useTheme();
  return (
    <View style={{ flexDirection: 'row', backgroundColor: '#FFF', height: hp('7%'), justifyContent: 'space-around', alignItems: 'center', borderTopWidth: 1, borderColor: '#F3F3F3', elevation: 5 }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
              ? options.title
              : route.name;

        const icon = options.icono;
        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{ justifyContent: 'center', alignItems: 'center' }}
            key={index}
          >
            {label == I18n.t('bottomMenu.Stats')?
              <Icon2 name={icon} color={isFocused ? 'green' : '#787878'} size={hp('2.5')} />
            :
              label == I18n.t('bottomMenu.Reviews')?
              <Icon2 name={icon} color={isFocused ? 'green' : '#787878'} size={hp('2.5')} />
              :
              <Icon name={icon} color={isFocused ? 'green' : '#787878'} size={hp('2.5')} />
            }
            <Text style={{ color: isFocused ? 'green' : '#787878', fontSize:hp('1.2')}}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const BottomTabNavigatorCoach = () => {

  function back() {
    UnityModule.postMessage('ReactManager', 'GetReactMessage', 'HideAR')
    UnityModule.postMessage('ReactManager', 'GetReactMessage', 'Clear3D');
  }

  return (
    <Tab.Navigator tabBar={props => <MyTabBar {...props} />} initialRouteName="Home" tabBarOptions={{
      activeTintColor: '#e91e63',
    }}>
      <Tab.Screen
        name="Home"
        component={MainStackNavigator}
        listeners={{
          tabPress: (e) => {
            back();
          },
        }}
        options={{
          unmountOnBlur: true,
          tabBarLabel: I18n.t('bottomMenu.Home'),
          icono: 'home',
        }}
      />
      <Tab.Screen
        name="Reviews"
        component={Review}
        listeners={{
          tabPress: (e) => {
            back();
          },
        }}
        options={{
          tabBarLabel: I18n.t('bottomMenu.Reviews'),
          icono: 'account-check',
        }}
      />
      <Tab.Screen
        name="My Team"
        component={MyTeams}
        listeners={{
          tabPress: (e) => {
            back();
          },
        }}
        options={{
          tabBarLabel: I18n.t('bottomMenu.My_Team'),
          icono: 'users',
        }}
      />
      <Tab.Screen
        name="Training"
        component={Trainings}
        listeners={{
          tabPress: (e) => {
            back();
          },
        }}
        options={{
          tabBarLabel: I18n.t('bottomMenu.Training'),
          icono: 'calendar',
        }}
      />
      <Tab.Screen
        name="Stats"
        component={Stat}
        listeners={{
          tabPress: (e) => {
            back();
          },
        }}
        options={{
          tabBarLabel: I18n.t('bottomMenu.Stats'),
          icono: 'poll',
        }}
      />
    </Tab.Navigator>
  );
};

export { BottomTabNavigatorCoach };