import React, { useState, useEffect } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage';

///////////////////////////////////////////////////// GUARDAR DATOS DEL USUARIO

export async function setUser(data) { // Se guarda el objeto del usuario
    try {
        data = JSON.stringify(data);
        await AsyncStorage.setItem('@user', data);
    } catch (error) {
        return false;
    }
}

export async function setModalprimary(data) { // Se guarda el objeto del usuario
  try {
      data = JSON.stringify(data);
      await AsyncStorage.setItem('@modalprimary', data);
  } catch (error) {
      return false;
  }
}


export async function setToken(data) { // Se guarda el token de sesion
    try {
        data = JSON.stringify(data);
        await AsyncStorage.setItem('@token', data);
    } catch (error) {
        return false;
    }
}


export async function setSkill(data) { // Se guarda el token de sesion
    try {
        data = JSON.stringify(data);
        await AsyncStorage.setItem('@skills', data);
    } catch (error) {
        return false;
    }
}

export async function setMethod(data) { // Se guarda el token de sesion
  try {
      data = JSON.stringify(data);
      await AsyncStorage.setItem('@method', data);
  } catch (error) {
      return false;
  }
}

export async function setExercises(data) { // Se guarda el token de sesion
  try {
      data = JSON.stringify(data);
      await AsyncStorage.setItem('@exercises', data);
  } catch (error) {
      return false;
  }
}


export async function setFCMTOKEN(data){ // Se guarda el token de pushy para las notificaciones
    try {
        data = JSON.stringify(data);
        await AsyncStorage.setItem('@fcmToken', data);
    } catch (error) {
        return false;
    }
}

export async function setReview(data){ // Se guarda el token de pushy para las notificaciones
  try {
      data = JSON.stringify(data);
      await AsyncStorage.setItem('@reviews', data);
  } catch (error) {
      return false;
  }
}

export async function setTeam(data){
  try {
    data = JSON.stringify(data);
    await AsyncStorage.setItem('@team',data);
  } catch (error) {
    return false;
  }
}

export async function setClub(data){
  try {
    data = JSON.stringify(data);
    await AsyncStorage.setItem('@club',data);
  } catch (error) {
    return false;
  }
}

export async function setCoach(data){
  try {
    data = JSON.stringify(data);
    await AsyncStorage.setItem('@coach',data);
  } catch (error) {
    return false;
  }
}

export async function setNotificationChat(data){
  try {
    data = JSON.stringify(data);
    await AsyncStorage.setItem('@notificationChat',data);
  } catch (error) {
    return false;
  }
}

export async function setNotificationGlobal(data){
  try {
    data = JSON.stringify(data);
    await AsyncStorage.setItem('@notificationGlobal',data);
  } catch (error) {
    return false;
  }
}

export async function setTutorialHome(data){
  try {
    data = JSON.stringify(data);
    await AsyncStorage.setItem('@tutorialHome',data);
  } catch (error) {
    return false;
  }
}

export async function setTutorial3D(data){
  try {
    data = JSON.stringify(data);
    await AsyncStorage.setItem('@tutorial3D',data);
  } catch (error) {
    return false;
  }
}

export async function setTutorialSkillDetails(data){
  try {
    data = JSON.stringify(data);
    await AsyncStorage.setItem('@tutorialSkillDetails',data);
  } catch (error) {
    return false;
  }
}

export async function setWelcome1(data){
  try {
    data = JSON.stringify(data);
    await AsyncStorage.setItem('@welcome1',data);
  } catch (error) {
    return false;
  }
}

export async function setWelcomeCoach(data){
  try {
    data = JSON.stringify(data);
    await AsyncStorage.setItem('@welcomeCoach',data);
  } catch (error) {
    return false;
  }
}

export async function setWelcomePlayer(data){
  try {
    data = JSON.stringify(data);
    await AsyncStorage.setItem('@welcomePlayer',data);
  } catch (error) {
    return false;
  }
}



///////////////////////////////////////////////////// OBTENER DATOS DEL USUARIO

export async function GetUser(){ // Se obtiene el objeto del usuario
    try {
      const user = await AsyncStorage.getItem('@user');
      if(!user) return null;
      const userJson = JSON.parse(user);
      return userJson;
    } catch (error) {
      console.error(error);
      return null;
    }
  }


  export async function GetToken(){ // Se obtiene el token de la sesion
    try {
      const token = await AsyncStorage.getItem('@token');
      if(!token) return null;
      const tokenJson = JSON.parse(token);
      return tokenJson;
    } catch (error) {
      console.error(error);
      return null;
    }
  }


  export async function GetFCMTOKEN(){ // Se obtiene el token de pushy para las notificaciones
    try {
      const token = await AsyncStorage.getItem('@fcmToken');
      if(!token) return null;
      const tokenJson = JSON.parse(token);
      return tokenJson;
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  export async function GetReview (){ // Se obtiene el token de pushy para las notificaciones
    try {
      const review = await AsyncStorage.getItem('@reviews');
      // if(!review) return null;
      const reviewJson = JSON.parse(review);
      return reviewJson;
    } catch (error) {
      console.error(error);
      return null;
    }
  }



  //////////////////////////////  Logout sesion


  export async function LogoutUser(){ // Se desloguea el user
    const keys = ['@user', '@token', '@skills', '@reviews', '@team']
    try {
      await AsyncStorage.multiRemove(keys)
      return true;
    } catch (error) {
      console.error(error);
      return null;
    }
  }
