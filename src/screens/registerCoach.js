//import liraries
import React, { useState, useEffect, useLayoutEffect, useRef, useCallback } from 'react';
import { View, Text, StyleSheet,
     TouchableOpacity, Image, Dimensions, TextInput,  KeyboardAvoidingView,
    Keyboard,
    TouchableWithoutFeedback, Platform, ScrollView, Linking } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import DatePicker from 'react-native-datepicker'
import AsyncStorage from '@react-native-async-storage/async-storage';
import md5 from 'md5';
import Snackbar from 'react-native-snackbar';
import Loader from './coach/global/loader';
import { Block, Checkbox, Input, Button as GaButton, theme } from 'galio-framework'
const { width, height } = Dimensions.get('screen');
import { setUser, setToken, setTeam, setCoach, setTutorialHome, setTutorial3D } from "../storage/user/dataUser";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'react-native-i18n';
import moment from "moment";

import CuponModal from './modalCupon';

// create a component
const RegisterCoach = ({navigation, route}) => {
    // variables del coach
    const [fullNameCoach, setFullNameCoach] = useState(null);
    const [emailCoach, setEmailCoach] = useState(null);
    const [passwordCoach, setPasswordCoach] = useState(null);

    const [cuponModal, setCuponModal] = useState(false);
    const [code, setCode] = useState('');
    const [errorcode, setErrorCode] = useState(false);

    // variables error coach 
    const [nameErrorCoach, setNameErrorCoach] = useState(false);
    const [emailErrorCoach, setEmailErrorCoach] = useState(false);
    const [passwordErrorCoach, setPasswordErrorCoach] = useState(false);
    const [passwordErrorPlayer, setPasswordErrorPlayer] = useState(false);

    // Terminos y condiciones
    const [politicasCoach, setPoliticasCoach] = useState(false);
    const [loader, setLoader] = useState(false);

    // Variable token
    const [token, setTok] = useState(null); // token fcm


    const [passwordStatus, setPasswordStatus] = useState(false);
    const [passValid, setpassValid] = useState(false);
    const [locatePais, setLocatePais] = useState('');


    useEffect(async() => {
        const fcmToken = await AsyncStorage.getItem('@fcmToken');
        const fcmTokenJson = JSON.parse(fcmToken);
        setTok(fcmTokenJson);

        Pais();

    }, [])

    useLayoutEffect(() => {
        navigation.setOptions({
            title:I18n.t('registerCoach.titlePage'),
        });
    }, [])




    async function validarcode(){
        if(!code){
            setErrorCode(false);
        }else{
        
        await fetch('https://go.vfa.app/api/validarcode', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
             codigo:code,
             pais_code:locatePais
            }),
        }).then(data => data.json())
        .then((res) => {
            if(res.data == false){
                setErrorCode(true);
            }
            else {
                setErrorCode(false);
            }
        })
        .catch((e) => {

        })
    }

    }


    async function Pais(){
        let response = await fetch("https://ipwhois.app/json/")
        .then((data) => data.json())
        .then((res) => {
            setLocatePais(res.country_code);
        })
    }



    function ValidateEnvio(){

        // Validacion de la edad 
        // const edad = moment(datePlayer).format("YYYY");
        // const fecha = moment().format("YYYY");
        // let calculo = fecha - edad;

        let regex = /^.*(?=.{8,})(?=.*\d)(?=.*[a-záéíóúüñ]).*[A-ZÁÉÍÓÚÜÑ]/;
        if (regex.test(passwordCoach)) {
            setPasswordStatus(false)
            //Validamos que la contraseña cumpla con el minimo de validacion
            setCuponModal(true);
                                   
            } else {
            setPasswordStatus(true)
             }
        
    }

    const SubmitCoach = async (e) => {
        if(errorcode != true || code == ''){
        try {
            if (!fullNameCoach) {
                setNameErrorCoach(true)
                return;
            } else {
                setNameErrorCoach(false)
            }
            if (!emailCoach) {
                setEmailErrorCoach(true)
                return;
            } else {
                setEmailErrorCoach(false)
            }
            if (!passwordCoach) {
                setPasswordErrorCoach(true)
                return;
            } else {
                setPasswordErrorCoach(false)
            }
            setLoader(true);


            await fetch('https://go.vfa.app/api/signup', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: fullNameCoach,
                    email: emailCoach,
                    password: md5(passwordCoach),
                    type: 'coach',
                    codigo:e,
                    onesignal_id: token,
                }),
            }).then(res => res.json())
                .then((dat) => {
                    console.log(dat)
                    try {
                        if (dat.ok) {
                            if (dat.data.team) {
                                setTeam(dat.data.team);
                            }
                            if (dat.data.coach) {
                                setCoach(dat.data.coach);
                            }
                            setUser(dat.data.user);
                            setToken(dat.data.token);
                            setTutorialHome(true);
                            setTutorial3D(true);
                            setLoader(false);

                            if (dat.data.user.pay == true && dat.data.user.invite == true) {
                                navigation.replace('validationSesion', { user: dat.data.user })
                            }else{

                                if (dat.data.user.state_cupon) {
                                    navigation.replace('Pay Discount coach', {user:dat.data.user, token:dat.data.token})
                                }
                                else if(!dat.data.user.state_cupon){
                                    navigation.replace('Pay App Coach', {user:dat.data.user, token:dat.data.token})
                                }

                            }
                           


                            
                            // navigation.replace('validationSesion', { user: dat.data.user })
                        } else {
                            setLoader(false);
                            alert(I18n.t('registerCoach.alert.alertEmailalready'))
                            // Snackbar.show({
                            //     text: 'Hello world',
                            //     duration: Snackbar.LENGTH_SHORT,
                            //   });

                            // setLoader(false);
                            // Snackbar.show({
                            //     text: 'The email has already been taken.',
                            //     duration: Snackbar.LENGTH_LONG,
                            // });
                        }

                    } catch (error) {
                        setLoader(false);
                        Snackbar.show({
                            text: I18n.t('registerCoach.alert.errorMessage'),
                            duration: Snackbar.LENGTH_LONG,
                        });

                    }
                })
        } catch (error) {
            Snackbar.show({
                text: I18n.t('registerCoach.alert.errorMessage'),
                duration: Snackbar.LENGTH_LONG,
            });
        }
    } else {
        Snackbar.show({
            text:I18n.t('texto4'),
            duration: Snackbar.LENGTH_LONG,
        });
    }
    }

    const policy = async () => {
        try {
          await Linking.openURL('https://www.websitepolicies.com/policies/view/0NueZaX0');
        } catch (e) {
          console.warn(e)
        }
      };

    const terminos = async () => {
    try {
        await Linking.openURL('https://www.websitepolicies.com/policies/view/Qmp6RQvE');
    } catch (e) {
        console.warn(e)
    }
    };

    return (



        <View style={{ flex: 1, paddingTop: 30, height: height, backgroundColor:'white' }}>
            {loader ? <Loader /> : null}


            {/* Modal para levantar el cupon  */}
                {cuponModal?
                <CuponModal 
                modalCloce={() => { setCuponModal(false)}}
                enviarFormulario={(e)=> {SubmitCoach(e)}}
                visibleModal={cuponModal} />
                :null}




            <Image source={require('../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'), position: "absolute",
            }} />
            <View style={{ flex: 1, height: height, justifyContent: 'center', alignItems: 'center' }}>
                <Image source={require('../assets/Logo.png')} style={styles.logo} />
                <Text style={{ fontSize: hp('2%'), fontWeight: 'bold' }}>{I18n.t('registerCoach.coach.title')}</Text>
                <Text style={{ fontSize: hp('1.5%'), opacity: 0.8 }}>{I18n.t('registerCoach.coach.descripcion')}</Text>
            </View>
            <View style={{ flex: 4, }}>
            <KeyboardAvoidingView
                                behavior={Platform.OS === "ios" ? "padding" : 'padding'}
                                style={{ padding: 14,
                                    flex: 1,
                                    justifyContent: "space-around"}}
                            >
                                 <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

                    <View style={styles.contentInputs}>
                        <Input
                            left
                            icon="user"
                            family="font-awesome"
                            style={styles.inputs}
                            iconSize={hp('1.4%')}
                            placeholder={I18n.t('registerCoach.coach.name')}
                            help={nameErrorCoach ? I18n.t('registerCoach.coach.helpName') : null}
                            bottomHelp
                            onChangeText={(e) => setFullNameCoach(e)}
                            placeholderTextColor="#818181"

                        />
                        <Input
                            left
                            icon="envelope"
                            family="font-awesome"
                            style={styles.inputs}
                            iconSize={hp('1.4%')}
                            placeholder={I18n.t('registerCoach.coach.email')}
                            help={emailErrorCoach ? I18n.t('registerCoach.coach.helpEmail') : null}
                            bottomHelp
                            autoCapitalize='none'
                            onChangeText={(e) => setEmailCoach(e)}
                            placeholderTextColor="#818181"

                        />
                        <Input
                            left
                            icon="key"
                            family="font-awesome"
                            style={styles.inputs}
                            iconSize={hp('1.4%')}
                            placeholder={I18n.t('registerCoach.coach.password')}
                            help={passwordErrorCoach ? I18n.t('registerCoach.coach.helpPass') : null}
                            bottomHelp
                            password
                            viewPass
                            onChangeText={(e) => setPasswordCoach(e)}
                            placeholderTextColor="#818181"

                        />


                             {
                             passwordStatus?
                            <Text style={{color:'red', fontWeight:'bold'}}>{ I18n.t('registerCoach.alert.errorPasswordValidate') }</Text>:null
                              }

{/* 

<Text>
                                       {I18n.t('textos.text9')}
                                    </Text>

                                    <Input
                                        left
                                        icon="ticket"
                                        family="font-awesome"
                                        style={styles.inputs}
                                        iconSize={hp('1.4%')}
                                        placeholder={I18n.t('textos.text10')}
                                        bottomHelp
                                        autoCapitalize={"characters"}
                                        onBlur={() => {validarcode()}}
                                        onChangeText={(e) => setCode(e)}
                                        placeholderTextColor="#818181"

                                    />

                                    {errorcode? 
                                    <Text style={{color:'red', fontWeight:'bold'}}>{I18n.t('texto4')}</Text>
                                    :null
                                    } */}







                        <Block
                            style={{ marginVertical: theme.SIZES.BASE, marginLeft: 15, flexDirection: 'row', }}
                            row
                            width={width * 0.75}
                        >
                            <Checkbox
                                checkboxStyle={{
                                    borderWidth: 1,
                                    borderRadius: 2,
                                    borderColor: '#818181',
                                    backgroundColor: '#818181',
                                }}
                                color='#818181'
                                labelStyle={{
                                    color: '#818181',
                                }}
                                value={politicasCoach}
                                onChange={(val) => {setPoliticasCoach(val),  validarcode()}}
                                label={null}
                            />

<View style={{flexDirection: 'row', marginLeft:10}}>
                                <Text style={{fontSize:13}}>
                                    {I18n.t('registerPlayer.player.text1')}
                                </Text>
                                <TouchableOpacity onPress={()=>policy()} style={{marginLeft:5,}}>
                                    <Text style={{color:'green',fontSize:13}}>
                                        {I18n.t('registerPlayer.player.text2')}
                                    </Text>
                                </TouchableOpacity>
                                <Text style={{marginLeft:5, marginRight:5, fontSize:13}}>
                                    {I18n.t('registerPlayer.player.text3')}
                                </Text>
                                <TouchableOpacity onPress={()=>terminos()}>
                                    <Text style={{color:'green', fontSize:13}}>
                                       {I18n.t('registerPlayer.player.text4')}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            
                            {/* <View style={{flexDirection: 'row', marginLeft:10}}>
                                <Text>
                                    {I18n.t('registerCoach.coach.text1')}
                                </Text>
                                <TouchableOpacity onPress={()=>policy()} style={{marginLeft:5,}}>
                                    <Text style={{color:'green'}}>
                                       {I18n.t('registerCoach.coach.text2')}
                                    </Text>
                                </TouchableOpacity>
                                <Text style={{marginLeft:5, marginRight:5}}>
                                    {I18n.t('registerCoach.coach.text3')}
                                </Text>
                                <TouchableOpacity onPress={()=>terminos()}>
                                    <Text style={{color:'green'}}>
                                        {I18n.t('registerCoach.coach.text4')}
                                    </Text>
                                </TouchableOpacity>
                            </View> */}
                        </Block>
                        <Block center style={{ marginTop: 20 }}>
                            <Block center>
                                <GaButton style={styles.createButton} size="small" color="success" onPress={() => {
                                    if (politicasCoach) {
                                        ValidateEnvio()
                                    } else {
                                        Snackbar.show({
                                            text: I18n.t('registerCoach.alert.alertSubmit'),
                                            duration: Snackbar.LENGTH_LONG,
                                        });
                                    }
                                }}>
                                    <Text style={{fontWeight:'bold', color:'white', textTransform:'uppercase'}}>
                                    {I18n.t('registerCoach.coach.submit')}
                                    </Text>
                                   
                                </GaButton>
                            </Block>
                        </Block>
                    </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    logo: {
        height: hp('7%'),
        width: wp('30%'),
    },
    inputs: {
        borderWidth: 1,
        height: 55,
        borderColor: '#E3E3E3',
        borderRadius: 10,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
    },
    contentInputs: {
        paddingHorizontal: 20,
        flex:6
    },
    createButton: {
        width: width * 0.8,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#11A10F',
        backgroundColor: '#11A10F',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderRadius:10,
        elevation: 5,
        justifyContent:'center',
        alignItems:'center',
        height:hp('6%'),
    },
});

//make this component available to the app
export default RegisterCoach;
