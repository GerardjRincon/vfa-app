import  React, {useState, useEffect} from 'react';
import {Modal, View, Text, StyleSheet,Button, TouchableOpacity, Image, Dimensions, TextInput,  KeyboardAvoidingView,
    Keyboard,
    Pressable,
    TouchableWithoutFeedback, Platform, ScrollView, Linking } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import Icon from 'react-native-vector-icons/FontAwesome';
import I18n from 'i18n-js';
import Snackbar from 'react-native-snackbar';
import { Block, Checkbox, Input, Button as GaButton, theme } from 'galio-framework'

const CuponModal = (props) => {

    
    
    const [data, setData] = useState({});
    const [code, setCode] = useState('');
    const [errorcode, setErrorCode] = useState(false);
    const [locatePais, setLocatePais] = useState('');


    useEffect(async() =>{
        Pais();
    }, [])



    async function Pais(){
        let response = await fetch("https://ipwhois.app/json/")
        .then((data) => data.json())
        .then((res) => {
            setLocatePais(res.country_code);
        })
    }


    async function validarcode(){
        if(!code){
             props.enviarFormulario();
        }else{
      


       const datos = {
        codigo:code,
        pais_code:locatePais
       }
        

       console.log(datos);

        await fetch('https://go.vfa.app/api/validarcode', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(datos),
        }).then(data => data.json())
        .then((res) => {
            console.log(res);
            if(res.data == false){
                setErrorCode(true);
                    Snackbar.show({
                        text:I18n.t('texto4'),
                        duration: Snackbar.LENGTH_LONG,
                    });
            }
            else {
                props.enviarFormulario(code);
            }
        })
        .catch((e) => {

        })
    }

    }


   

    return(
   
    <Modal
        animationType="slide"
        transparent={true}
        visible={props.visibleModal}
        onRequestClose={() => {
      
        }}
      >
       
        <View style={styles.centeredView}>
        <ScrollView>
          <View style={styles.modalView}>

          <Pressable onPress={()=> {props.modalCloce()}} style={{position:'absolute', zIndex:1000, width:40, height:40, top:20, left:15, color:'#3d3d3d' }}>
        <Icon2 name="close" size={30} ></Icon2>
        </Pressable>

             
              
          <Image source={require('../assets/confeti.png')} style={{
                height: '50%',
                width:wp('100%'),
                position:'absolute',
                top:0
            }} />



              <View>
                  <Text style={{fontSize:25, textAlign:'center', marginTop:70, fontWeight:'bold', marginBottom:20}}>
                      {I18n.t('tex18')}
                  </Text>
              </View>

                                    {/* <Text>
                                       {I18n.t('textos.text9')}
                                    </Text> */}

                                    <Input
                                        left
                                        icon="ticket"
                                        family="font-awesome"
                                        style={{borderWidth:2, borderColor:"#eee", height:hp('7%')}}
                                        iconSize={hp('1.4%')}
                                        placeholder={I18n.t('textos.text10')}
                                        bottomHelp
                                        onBlur={() => {validarcode()}}
                                        autoCapitalize={"characters"}
                                        onChangeText={(e) => setCode(e)}
                                        placeholderTextColor="#818181"

                                    />

                                    {errorcode? 
                                    <Text style={{color:'red', fontWeight:'bold'}}>{I18n.t('texto4')}</Text>
                                    :null
                                    }


                  <Pressable
                    style={[styles.button, {justifyContent:'center', alignItems:'center',marginTop:20 }]}
                    onPress={() => validarcode()}
                  >
                    <Text style={styles.textStyle}>{I18n.t('TutorialView.buttom')}</Text>
                    <Icon name="chevron-right" style={{position:'absolute',right:30, color:'white' }}></Icon>
                  </Pressable>


                   <Pressable onPress={() => validarcode()}>
                    <Text style={{fontWeight:'bold', fontSize:19, textDecorationLine: 'underline'}}>
                      {I18n.t('tex20')}
                    </Text>
                  </Pressable>
          </View>
          </ScrollView>
        </View>
      
      </Modal>

    )
}

export default CuponModal;

const styles = StyleSheet.create({
    splah_happy:{
        width:wp('55%'),
        height:hp('28%'),
      },
    
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 0,
    backgroundColor:'#0000002e',
    height:hp('100%')
    
  },
  modalView: {
    // margin: 20,
    backgroundColor: "white",
    // borderRadius: 20,
    width:wp('100%'),
    height:hp('100%'),
    padding: 35,
    alignItems: "center",
    justifyContent:'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
        borderRadius:5,
        width:wp('81%'),
        height:hp('6%'),
        marginTop: 25,
        marginBottom: 40,
        backgroundColor:'#11A10F',
        flexDirection:'row',
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#00c900",
    textTransform:'uppercase',
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    textTransform:'uppercase',
  },
  modalText: {
    marginBottom: 10,
    textAlign: "center"
  },


  modalTextTiTle: {
    marginBottom: 10,
    textAlign: "center",
    fontSize:wp('9%'),
    fontWeight:'normal',
    color:'#333'
  },

  subTitle:{
    fontWeight:'bold',
    fontSize:wp('6%'),
    // textAlign: "center",
    textTransform:'uppercase',
    color:'#404040',
    


  },
  areaText:{
    marginTop:22,
    marginBottom:10,
    // fontWeight:'700',
    opacity:0.9,
    fontSize:15,
    // textAlign: 'center',
    // lineHeight:22

  },

  areaTextTow:{
    marginTop:0,
    marginBottom:22,
    // fontWeight:'bold',
    // opacity:0.9,
    fontSize:15,
    // textAlign: "center",
    // lineHeight:22

  },



  textPuntos:{
    backgroundColor:'#eee',
    padding:2,
    paddingLeft:10, 
    paddingRight:10,
    color:'#404040',
    borderRadius:5,
    marginTop:10,
    fontSize:wp('4%'),
    
  },






})