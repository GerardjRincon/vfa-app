import React, { useState, useEffect, useRef } from 'react'
import {
    StyleSheet,
    Dimensions,
    Image,
    View,
    StatusBar,
    SafeAreaView,
    ActivityIndicator,
    KeyboardAvoidingView,
    Keyboard,
    TouchableWithoutFeedback,
    Platform, 
    Text,
    DeviceEventEmitter,
    TouchableOpacity,
    Linking,
} from 'react-native';
import md5 from 'md5';
import { nowTheme } from '../assets/fonts';
import { Icon } from 'react-native-elements'
import Loader from './coach/global/loader';
import { Block, Checkbox, Input, Text as T, Button as GaButton, theme } from 'galio-framework'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Row, Col } from 'react-native-responsive-grid-system';
import { useNavigation, useTheme, useRoute } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Snackbar from 'react-native-snackbar';
const { width, height } = Dimensions.get('screen');
import OneSignal from 'react-native-onesignal';
import { SvgUri } from 'react-native-svg';
import I18n from 'react-native-i18n';

// I18n.defaultLocale = 'es'

// // traducciones
// import en from '../translation/login/en';
// import es from '../translation/login/es';
// import ch from '../translation/login/ch';

import { setUser, setToken, setTeam, setCoach, setFCMTOKEN } from "../storage/user/dataUser";


const Login = (props) => {
    const navigation = useNavigation();
    const [active, setActive] = useState(true)
    const [Useremail, setUseremail] = useState(null);
    const [pass, setPass] = useState(null);
    const [remember, setRemember] = useState(false);
    const [emailError, setEmailError] = useState(false);
    const [passError, setPassError] = useState(false);
    const [loader, setLoader] = useState(false);
    const [fcm, setFcm] = useState(null);
    const ReactTheme = useTheme();
    const [emailErrorMSJ,setEmailErrorMSJ] = useState(false);

    useEffect(async () => {

        setLoader(true);

        const fcmToken = await AsyncStorage.getItem('@fcmToken');
        const fcmTokenJson = JSON.parse(fcmToken);
        if (fcmTokenJson) {
            setLoader(false);
            setFcm(fcmTokenJson);
        } else {
            OneSignal.addSubscriptionObserver(() => {
                OneSignal.getDeviceState().then((deviceState) => {
                    if (deviceState.userId) {
                        setFCMTOKEN(deviceState.userId)
                        setFcm(deviceState.userId);
                        setLoader(false);
                    }
                });
            });
        }
        
    }, [])


    function ValidateFormulario(){
        if(Useremail && pass){
            const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (reg.test(Useremail) === true){
                Logger();
                setEmailErrorMSJ(false)

            }
            else{
               setEmailErrorMSJ(true)
            }
        }else {
           Snackbar.show({
            text:I18n.t('login.validateInputs'),
            duration: Snackbar.LENGTH_LONG,
          });

        }
       
    }

    const Logger = async () => {
        
        if (!Useremail) {
            setEmailError(true)
            return;
        } else {
            setEmailError(false)
        }
        if (!pass) {
            setPassError(true)
            return;
        } else {
            setPassError(false)
        }

        setLoader(true)

        await fetch('https://go.vfa.app/api/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: Useremail,
                password: md5(pass),
                remember_me: remember,
                onesignal_id: fcm,
            }),
        }).then(res => res.json())
            .then(async (dat) => {
                console.log(dat)
                try {
                    if (dat.ok) {
                        setTeam(dat.data.team);
                        console.warn(dat)
                        setUser(dat.data.user);
                        setToken(dat.data.token);
                        setCoach(dat.data.coach);
                        navigation.replace('validationSesion', { user: dat.data.user });
                    } else {
                        console.warn(dat.data.login)
                        setTimeout(() => {
                            Snackbar.show({
                                text: dat.data.login[0],
                                duration: Snackbar.LENGTH_LONG,
                            });
                        }, 100)
                    }
                    setLoader(false)
                } catch (error) {
                    setLoader(false)
                    console.log(error)
                }
            })
    }


    return (
    //     <KeyboardAvoidingView
    //   behavior={Platform.OS === "ios" ? "padding" : 'padding'}
    //   style={styles.container}
    // >






    //   <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

     
        <View style={{ width: wp('100%'),  backgroundColor: 'white',}}>
           <StatusBar backgroundColor="#f3f3f3" barStyle="dark-content" />
            {loader ?
                <Loader />
                : null}

            <Image source={require('../assets/pelota.png')} style={{
                height: '70%',
                width: wp('75%'), 
                bottom:0,
                right:0,
                position: "absolute",
                
            }} />

            <View style={{ height: height, justifyContent:'center' }}>
                <Block flex middle>
                    <Block style={styles.registerContainer}>
                        <Block flex>
                            <Block flex={0.4} middle style={styles.socialConnect}>
                                <Block flex={0.5} middle>
                                    <Image source={require('../assets/Logo.png')} style={styles.logo} />
                                    <Text style={{ fontSize: hp('2.3%'), fontWeight: 'bold', textTransform:'uppercase', color:'#3d3d3d', marginTop:10}}>
                                        {I18n.t('login.Bienvenida')}
                                        </Text>
                                </Block>
                            </Block>

                            
                            <KeyboardAvoidingView
                                behavior={Platform.OS === "ios" ? "padding" : 'padding'}
                                style={{ padding: 14,
                                    flex: 1,
                                    justifyContent: "space-around"}}
                            >
                                 <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                                <View style={{ flex:1,justifyContent: "space-around"}}>
                                    <Block center>
                                        <Block flex>
                                            <Block >
                                                {/* <Block width={width * 0.8} style={{ marginBottom: 5 }}> */}
                                                <Block  style={[styles.contentInput, { marginBottom: 5 }]}>
                                                    <Input
                                                        left
                                                        icon="envelope"
                                                        family="font-awesome"
                                                        style={styles.inputs}
                                                        iconSize={hp('1.5%')}
                                                        placeholder={I18n.t('login.email')}
                                                        help={emailError ? I18n.t('login.helpEmail') : null}
                                                        bottomHelp
                                                        autoCapitalize='none'
                                                        onChangeText={(e) => setUseremail(e)}
                                                        placeholderTextColor="#818181"

                                                    />
                                                </Block>
                                                {emailErrorMSJ?
                                                <Text style={{marginTop:hp('-1%'), color:'red', fontWeight:'bold', marginLeft:wp('10%')}}>
                                                   {I18n.t('login.validateInputEmail')}
                                                </Text>:null}
                                                
                                                <Block  style={[styles.contentInput, { marginBottom: 5 }]}>
                                                    <Input
                                                        left
                                                        icon="lock"
                                                        family="font-awesome"
                                                        style={styles.inputs}
                                                        iconSize={hp('1.8%')}
                                                        placeholder={I18n.t('login.password')}
                                                        help={passError ? I18n.t('login.helpPass') : null}
                                                        bottomHelp
                                                        onChangeText={(val) => setPass(val)}
                                                        placeholderTextColor="#818181"
                                                        password
                                                        viewPass
                                                    />
                                                </Block>
                                                {/* <Block
                                                    style={{ marginVertical: theme.SIZES.BASE, marginLeft: 15 }}
                                                    row
                                                    width={width * 0.75}
                                                >
                                                    <Checkbox
                                                        checkboxStyle={{
                                                            borderWidth: 1,
                                                            borderRadius: 2,
                                                            borderColor: '#E3E3E3'
                                                        }}
                                                        color={nowTheme.COLORS.PRIMARY}
                                                        labelStyle={{
                                                            color: nowTheme.COLORS.HEADER,
                                                        }}
                                                        value={remember}
                                                        onChange={(val)=>setRemember(val)}
                                                        label="Remember access."
                                                    />
                                                </Block> */}
                                                <Block >


                                                    <Block center>
                                                        <GaButton style={styles.createButton} onPress={() => ValidateFormulario()}>
                                                             <Text style={{fontWeight:'bold', color:'white'}}>
                                                                 {I18n.t('login.login')}
                                                                 </Text> 
                                                            </GaButton>
                                                    </Block>


                                                    <View style={{justifyContent:'center', alignItems:'center'}}>
                                                    <View>
                                                    <TouchableOpacity 
                                                        activeOpacity={0.9} style={{justifyContent:'center', alignItems:'center', paddingTop:hp('0%'), paddingBottom:hp('3%'), width:wp('40%')}}
                                                         onPress={async() => await Linking.openURL('https://go.vfa.app/password/3/reset')}>
                                                        <Text style={{fontWeight:'bold',fontSize: hp('2%'), color:'#11A10F', opacity:0.7 }}>{I18n.t('login.recover')}
                                                        </Text>
                                                    </TouchableOpacity>
                                                    </View>

                                                     <View
                                                        style={{
                                                                width:wp('90%'),
                                                                // backgroundColor:'red',
                                                                borderBottomColor:'#d1d1d1',
                                                                borderBottomWidth: 1,
                                                                margin:10,
                                                        }}
                                                        >
                                                            {/* <Text>
                                                                Hola
                                                            </Text> */}
                                                        </View>

                                                  
                                                      <View style={{alignItems:'center'}}>

                                                                <T style={{ fontSize: hp('1.9%'), }}>
                                                                    {I18n.t('login.account')}
                                                                </T>
                                                    <TouchableOpacity activeOpacity={0.9} style={{paddingTop:hp('1%'), width:wp('50%'), alignItems:'center'}} 
                                                       onPress={() => navigation.navigate('Registre Select')}>
                                                        <Text style={{ fontWeight:'bold', fontSize: hp('2.4%'), color: '#11A10F',opacity:0.7, alignItems:'center'}}>
                                                            {I18n.t('login.signUp')}
                                                             </Text>
                                                    </TouchableOpacity>

                                                    </View>


                                                    </View>



                                                   


                                                </Block>

                                            </Block>
                                        </Block>
                                    </Block>
                                </View>
                                </TouchableWithoutFeedback>
                            </KeyboardAvoidingView>
                        </Block>
                    </Block>

                </Block>
            </View>
        </View>
        // </TouchableWithoutFeedback>
        // </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    registerButton: {
        width: 50,
        height: 50, backgroundColor: 'white', paddingTop: 10, borderRadius: 50,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
        marginTop: 10,
        marginLeft: 30,
        position: 'absolute',
        flex: 1,
        justifyContent: 'center',
        top: 0

    },
    textalign: {
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 0,
    },
    buttomRegister: {
        marginTop: 30
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    fondo: {
        flex: 1,
        backgroundColor: "#fefefe",
        height: hp('100%'),
        width: wp('100%')
    },
    logo: {
        height: 60,
        width: 140,
    },
    imageBackgroundContainer: {
        width: width,
        height: height,
        padding: 0,
        zIndex: 1
    },
    registerContainer: {
        marginTop: 55,
        width: width * 0.9,
        // height: height < 812 ? height * 0.8 : height * 0.8,
        height:hp('100%'),
        // justifyContent:'center',
        // alignItems:'center',
        backgroundColor: "transparent",
        elevation: 1,
        overflow: 'hidden'
    },

    contentInput:{
       width:wp('100%'),
       justifyContent:'center',
       alignItems:'center'
       
    },

    inputs: {
        // borderWidth: 1,
        height: 55,
        borderColor: '#E3E3E3',
        borderRadius: 10,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
        width:wp('80%'),
    },
    passwordCheck: {
        paddingLeft: 2,
        paddingTop: 6,
        paddingBottom: 15
    },
    createButton: {
        marginTop: 25,
        marginBottom: 20,
        shadowColor: '#11A10F',
        backgroundColor: '#11A10F',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderRadius:10,
        elevation: 5,
        // justifyContent:'center',
        // alignItems:'center',
        height:hp('6%'),
        fontWeight:'bold',
          width:wp('80%'),
    },
    social: {
        width: theme.SIZES.BASE * 3.5,
        height: theme.SIZES.BASE * 3.5,
        borderRadius: theme.SIZES.BASE * 1.75,
        justifyContent: 'center',
        marginHorizontal: 10
    }
});

// I18n.fallbacks = true;

// I18n.translations = {
//   en,
//   'en-US':en,
//   es,
//   'es-US':es,
//   ch,
// };

export default Login;

