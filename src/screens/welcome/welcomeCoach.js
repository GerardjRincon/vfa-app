//import liraries
import React, { useState, useEffect, useLayoutEffect, useRef } from 'react'
import {
    StyleSheet,
    Dimensions,
    Image,
    View,
    StatusBar,
    SafeAreaView,
    ActivityIndicator,
    KeyboardAvoidingView,
    Platform, 
    Text,
    DeviceEventEmitter,
    TouchableOpacity
} from 'react-native';
import Swiper from 'react-native-swiper';
import I18n from 'react-native-i18n';
import { Block, Checkbox, Input, Text as T, Button as GaButton, theme } from 'galio-framework'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { setWelcomeCoach } from '../../storage/user/dataUser';

// constantes de vista
const { width, height } = Dimensions.get('screen');

// create a component
const WelcomeCoach = ({navigation,route}) => {

    useLayoutEffect(() => {
        navigation.setOptions({
            title:I18n.t('welcomeCoach.titleView'),
        });
    }, [])

    return (
        <SafeAreaView style={styles.container}>
            <Image source={require('../../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'), position: "absolute",
            }} />
            <View style={{flex:1}}>
                <Swiper
                    loop={false} 
                    style={{height: '100%',justifyContent:'center', alignItems:'center'}}
                    scrollEnabled={true}
                    showsButtons={false}
                    dot={<View style={{width: hp('2%'),height: hp('2%'),borderRadius: wp('2%'),marginHorizontal:wp('2%'), marginTop:hp('2%'),backgroundColor: '#646469',opacity: 0.3,}}/>}
                    activeDot={<View style={{width: hp('2%'),height: hp('2%'),borderRadius: wp('2%'),marginHorizontal:wp('2%'),marginTop:hp('2%'),backgroundColor: '#11A10F',}}/>}>
                        <View style={{flex:1,justifyContent:'center', alignItems:'center'}}>
                            <View style={{flex:1, justifyContent:'center', alignItems:'center', paddingHorizontal:20}}>
                                <Image source={require('../../assets/welcome/coach/welcome1.png')} resizeMode="contain" style={{width:hp('30%'), height:hp('30%')}}/>
                                <Text style={{fontSize:hp('3%'), color:'#000', textAlign:'center', paddingBottom:hp('3%'), paddingHorizontal:25, paddingTop:hp('3%')}}>{I18n.t('welcomeCoach.carrouselOneTitle')}</Text>
                                <Text style={{fontSize:hp('2.5%'), color:'#000', textAlign:'center',}}>{I18n.t('welcomeCoach.carrouselOneDescription')}</Text>
                            </View>
                        </View>
                        <View style={{flex:1,justifyContent:'center', alignItems:'center'}}>
                            <View style={{flex:1, justifyContent:'center', alignItems:'center', paddingHorizontal:15}}>
                                <Image source={require('../../assets/welcome/coach/welcome2.png')} resizeMode="contain" style={{width:hp('30%'), height:hp('30%')}}/>
                                <Text style={{fontSize:hp('3%'), color:'#000', textAlign:'center', paddingBottom:hp('3%'), paddingTop:hp('3%')}}>{I18n.t('welcomeCoach.carrouselTwoTitle')}</Text>
                                <Text style={{fontSize:hp('2.5%'), color:'#000', textAlign:'center'}}>{I18n.t('welcomeCoach.carrouselTwoDescription')}</Text>
                            </View>
                        </View>
                        <View style={{flex:1,justifyContent:'center', alignItems:'center'}}>
                            <View style={{flex:1, justifyContent:'center', alignItems:'center', paddingHorizontal:20}}>
                                <Image source={require('../../assets/welcome/coach/welcome3.png')} resizeMode="contain" style={{width:hp('30%'), height:hp('30%')}}/>
                                <Text style={{fontSize:hp('3%'), color:'#000', textAlign:'center', paddingBottom:hp('3%'), paddingTop:hp('3%')}}>{I18n.t('welcomeCoach.carrouseThreeTitle')}</Text>
                                <Text style={{fontSize:hp('2.5%'), color:'#000', textAlign:'center'}}>{I18n.t('welcomeCoach.carrouselThreeDescription')}</Text>
                            </View>
                        </View>
                </Swiper>
                <View>
                    <Block center>
                        <TouchableOpacity activeOpacity={0.9} onPress={() => {
                            // setWelcomeCoach(true);
                            navigation.navigate('Register Coach')
                            }} style={styles.createButton}>
                            <Text style={{fontWeight:'bold', color:'#FFF', fontSize:hp('1.7%')}}>
                                {I18n.t('welcomeCoach.bottom')}
                            </Text>
                        </TouchableOpacity>
                        {/* <GaButton style={styles.createButton} size="small" color="success" onPress={() => {setWelcomeCoach(true), setTimeout(()=>{navigation.navigate('Register Coach')},10)}}>{I18n.t('welcomeCoach.bottom')}</GaButton> */}
                    </Block>
                </View>
            </View>
        </SafeAreaView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    createButton: {
        width: width * 0.8,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#11A10F',
        backgroundColor: '#11A10F',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderRadius:10,
        elevation: 5,
        justifyContent:'center',
        alignItems:'center',
        height:hp('6%'),
    },
});

//make this component available to the app
export default WelcomeCoach;
