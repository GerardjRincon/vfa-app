//import liraries
import React, { useState, useEffect, useRef } from 'react'
import {
    StyleSheet,
    Dimensions,
    Image,
    View,
    StatusBar,
    SafeAreaView,
    ActivityIndicator,
    KeyboardAvoidingView,
    Platform, 
    Text,
    DeviceEventEmitter,
    TouchableOpacity
} from 'react-native';
import Swiper from 'react-native-swiper';
import I18n from 'react-native-i18n';
import { Block, Checkbox, Input, Text as T, Button as GaButton, theme } from 'galio-framework'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { setWelcome1 } from '../../storage/user/dataUser';

// constantes de vista
const { width, height } = Dimensions.get('screen');


// TEST PRUEBA API GOOGLE PAY

// import IAP from 'react-native-iap';

// const items = Platform.select({
//     ios:[],
//     android:['coach_19_annual']
// });
 // FIN DE LA PRUEBA DEL API GOOGLE PAY



// create a component
const WelcomeView = ({navigation,route}) => {
    
    // PRUEBA DEL API DE GOOGLE PAY 

     // const [purchased, setPurchased] = useState(false);
     // const [products, setProducts] = useState({});

     // useEffect(() => {
     //   console.log(`estamos dentro`);

     //   IAP.initConnection().catch(() => {
     //    console.log(`error dentro de la conexion`);
     //   })
     //   .then(() => {
     //      IAP.getSubscriptions(items).catch(() => {
     //        console.log("error al traer items")
     //      })
     //     }).then((res) => {
     //        console.log(`datos ${res}`);
     //   })

     // }, []);


    // FIN DE LA PRUEBA DEL API GOOGLE PAY

    return (
        <SafeAreaView style={styles.container}>
            <Image source={require('../../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'), position: "absolute",
            }} />
            <View style={{flex:1}}>


                <Swiper
                    loop={false} 
                    style={{height: '100%',justifyContent:'center', alignItems:'center'}}
                    scrollEnabled={true}
                    showsButtons={false}
                    dot={<View style={{width: hp('2%'),height: hp('2%'),borderRadius: wp('2%'),marginHorizontal:wp('2%'), marginTop:hp('2%'),backgroundColor: '#646469',opacity: 0.3,}}/>}
                    activeDot={<View style={{width: hp('2%'),height: hp('2%'),borderRadius: wp('2%'),marginHorizontal:wp('2%'),marginTop:hp('2%'),backgroundColor: '#11A10F',}}/>}>
                        <View style={{flex:1,justifyContent:'center', alignItems:'center'}}>
                            <View style={{flex:1, justifyContent:'center', alignItems:'center', paddingHorizontal:15}}>
                                <Image source={require('../../assets/welcome/welcome1.png')} resizeMode="contain" style={{width:hp('30%'), height:hp('30%')}}/>
                                <Text style={{fontSize:hp('3%'), color:'#000', textAlign:'center', paddingBottom:hp('3%'), paddingTop:hp('3%')}}>{I18n.t('welcome.title1')}</Text>
                                <Text style={{fontSize:hp('2.5%'), color:'#000', textAlign:'center',}}>{I18n.t('welcome.descripcion1')}</Text>
                            </View>
                        </View>
                        <View style={{flex:1,justifyContent:'center', alignItems:'center'}}>
                            <View style={{flex:1, justifyContent:'center', alignItems:'center', paddingHorizontal:15}}>
                                <Image source={require('../../assets/welcome/welcome2.png')} resizeMode="contain" style={{width:hp('30%'), height:hp('30%')}}/>
                                <Text style={{fontSize:hp('3%'), color:'#000', textAlign:'center', paddingBottom:hp('3%'), paddingTop:hp('3%')}}>{I18n.t('welcome.title2')}</Text>
                                <Text style={{fontSize:hp('2.5%'), color:'#000', textAlign:'center'}}>{I18n.t('welcome.descripcion2')}</Text>
                            </View>
                        </View>
                        <View style={{flex:1,justifyContent:'center', alignItems:'center'}}>
                            <View style={{flex:1, justifyContent:'center', alignItems:'center', paddingHorizontal:15}}>
                                <Image source={require('../../assets/welcome/welcome3.png')} resizeMode="contain" style={{width:hp('30%'), height:hp('30%')}}/>
                                <Text style={{fontSize:hp('3%'), color:'#000', textAlign:'center', paddingBottom:hp('3%'), paddingTop:hp('3%')}}>{I18n.t('welcome.title3')}</Text>
                                <Text style={{fontSize:hp('2.5%'), color:'#000', textAlign:'center'}}>{I18n.t('welcome.descripcion3')}</Text>
                            </View>
                        </View>
                </Swiper>
                <View>
                    <Block center>
                        <TouchableOpacity activeOpacity={0.9} onPress={() => {setWelcome1(true); setTimeout(()=>{navigation.replace('Login')},10)}} style={styles.createButton}>
                            <Text style={{fontWeight:'bold', color:'#FFF', fontSize:hp('1.7%')}}>
                                {I18n.t('welcome.Buton')}
                            </Text>
                        </TouchableOpacity>
                        {/* <GaButton style={styles.createButton} size="small" color="success" onPress={() => {setWelcome1(true); setTimeout(()=>{navigation.replace('Login')},10)}}>{I18n.t('welcome.Buton')}</GaButton> */}
                    </Block>
                </View>
            </View>
        </SafeAreaView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    createButton: {
        width: width * 0.8,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#11A10F',
        backgroundColor: '#11A10F',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderRadius:10,
        elevation: 5,
        justifyContent:'center',
        alignItems:'center',
        height:hp('6%'),
    },
});


//make this component available to the app
export default WelcomeView;
