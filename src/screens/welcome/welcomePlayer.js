//import liraries
import React, { useState, useEffect, useLayoutEffect, useRef } from 'react'
import {
    StyleSheet,
    Dimensions,
    Image,
    View,
    StatusBar,
    SafeAreaView,
    ActivityIndicator,
    KeyboardAvoidingView,
    Platform, 
    Text,
    DeviceEventEmitter,
    TouchableOpacity
} from 'react-native';
import Swiper from 'react-native-swiper';
import I18n from 'react-native-i18n';
import { Block, Checkbox, Input, Text as T, Button as GaButton, theme } from 'galio-framework'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon  from 'react-native-vector-icons/MaterialCommunityIcons';
import { setWelcomePlayer } from '../../storage/user/dataUser';

// constantes de vista
const { width, height } = Dimensions.get('screen');

// create a component
const WelcomePlayer = ({navigation,route}) => {

    useLayoutEffect(() => {
        navigation.setOptions({
            title:I18n.t('welcomePlayer.titleView'),
        });
    }, [])

    return (
        <SafeAreaView style={styles.container}>

            <Image source={require('../../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'), position: "absolute",
            }} />
            <View style={{flex:1}}>
                <Swiper
                    loop={false} 
                    style={{height: '100%',justifyContent:'center', alignItems:'center'}}
                    scrollEnabled={true}
                    showsButtons={false}
                    dot={<View style={{width: hp('2%'),height: hp('2%'),borderRadius: wp('2%'),marginHorizontal:wp('2%'), marginTop:hp('2%'),backgroundColor: '#646469',opacity: 0.3,}}/>}
                    activeDot={<View style={{width: hp('2%'),height: hp('2%'),borderRadius: wp('2%'),marginHorizontal:wp('2%'),marginTop:hp('2%'),backgroundColor: '#11A10F',}}/>}>
                        <View style={{flex:1,justifyContent:'center', alignItems:'center'}}>
                            <View style={{flex:1, justifyContent:'center', alignItems:'center', paddingHorizontal:20}}>
                                <Image source={require('../../assets/welcome/player/welcome1.png')} resizeMode="contain" style={{width:hp('30%'), height:hp('30%')}}/>
                                <Text style={{fontSize:hp('3%'), color:'#000', paddingBottom:hp('3%'),  paddingHorizontal:25, textAlign:'center', paddingTop:hp('3%')}}>{I18n.t('welcomePlayer.carrouselOneTitle')}</Text>
                                <Text style={{fontSize:hp('2.5%'), color:'#000', textAlign:'center',}}>{I18n.t('welcomePlayer.carrouselOneDescription')}</Text>
                            </View>
                        </View>
                        <View style={{flex:1,justifyContent:'center', alignItems:'center'}}>
                            <View style={{flex:1, justifyContent:'center', alignItems:'center', paddingHorizontal:15}}>
                                <Image source={require('../../assets/welcome/player/welcome2.png')} resizeMode="contain" style={{width:hp('30%'), height:hp('30%')}}/>
                                <Text style={{fontSize:hp('3%'), color:'#000', paddingBottom:hp('3%'), textAlign:'center', paddingTop:hp('3%')}}>{I18n.t('welcomePlayer.carrouselTwoTitle')}</Text>
                                <Text style={{fontSize:hp('2.5%'), color:'#000', textAlign:'center'}}>{I18n.t('welcomePlayer.carrouselTwoDescription')}</Text>
                            </View>
                        </View>

                        {/* cada vista  */}
                        <View style={{flex:1,justifyContent:'center', alignItems:'center'}}>
                       
                            <View style={{flex:1, justifyContent:'center', alignItems:'center', paddingHorizontal:15}}>
                                <Image source={require('../../assets/welcome/player/welcome3.png')} resizeMode="contain" style={{width:hp('30%'), height:hp('30%')}}/>
                                <Text style={{fontSize:hp('3%'), color:'#000', paddingBottom:hp('3%'), textAlign:'center', paddingTop:hp('3%')}}>{I18n.t('welcomePlayer.carrouseThreeTitle')}</Text>
                                <Text style={{fontSize:hp('2.5%'), color:'#000', textAlign:'center'}}>{I18n.t('welcomePlayer.carrouselThreeDescription')}</Text>
                          

        {/* ICONOS DE L VISTA WELCOME PLAYER  */}
          <View style={{justifyContent:'center', alignItems:'center', marginTop:hp('2%')}}>

                <View>
                <Text>{I18n.t('video3D.sendVideo.text')}</Text>
                </View>
                <View style={{width:wp('100%'), height:hp('5%'), flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                <View style={{ marginHorizontal:5, justifyContent:'center', alignItems:'center'}}>
                    <Image source={require('../../assets/imgs/3D/wechat.png')} resizeMode="contain" style={{width:hp('3%'), height:hp('3%')}}/>
                </View>
                <View style={{ marginHorizontal:5, justifyContent:'center', alignItems:'center'}}>
                    <Image source={require('../../assets/imgs/3D/telegrama.png')} resizeMode="contain" style={{width:hp('3%'), height:hp('3%')}}/>
                </View>
                <View style={{ marginHorizontal:5, justifyContent:'center', alignItems:'center'}}>
                    <Image source={require('../../assets/imgs/3D/005-whatsapp.png')} resizeMode="contain" style={{width:hp('3%'), height:hp('3%')}}/>
                </View>
                <View style={{ marginHorizontal:5, justifyContent:'center', alignItems:'center'}}>
                    <Image source={require('../../assets/imgs/3D/instagram.png')} resizeMode="contain" style={{width:hp('3%'), height:hp('3%')}}/>
                </View>
                <View style={{ marginHorizontal:5, justifyContent:'center', alignItems:'center'}}>
                    <Image source={require('../../assets/imgs/3D/tik-tok.png')} resizeMode="contain" style={{width:hp('3%'), height:hp('3%')}}/>
                </View>
                </View>
            </View>

            
     </View>



                        </View>
                </Swiper>
                <View>
                    <Block center>
                        <TouchableOpacity activeOpacity={0.9} onPress={() => { 
                            // setWelcomePlayer(true); 
                            navigation.navigate('Register Player')
                            }} style={[styles.createButton, {flexDirection:'row'}]}>
                            <Text style={{fontWeight:'bold', color:'#FFF', fontSize:hp('1.7%')}}>
                                {I18n.t('welcomePlayer.bottom')}
                            </Text>
                            <Icon name="arrow-right" size={wp('4%')} style={{marginLeft:10, color:'white'}}></Icon>
                        </TouchableOpacity>
                        {/* <GaButton style={styles.createButton} size="small" color="success" onPress={() => { setWelcomePlayer(true); setTimeout(()=>{navigation.navigate('Register Player')},10)}}>{I18n.t('welcomePlayer.bottom')}</GaButton> */}
                    </Block>
                </View>
            </View>
        </SafeAreaView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    createButton: {
        width: width * 0.8,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#11A10F',
        backgroundColor: '#11A10F',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderRadius:10,
        elevation: 5,
        justifyContent:'center',
        alignItems:'center',
        height:hp('6%'),
    },
});


//make this component available to the app
export default WelcomePlayer;
