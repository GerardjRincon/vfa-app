//import liraries
import React, { useState, useEffect, useLayoutEffect, useRef } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, Dimensions, TextInput, KeyboardAvoidingView, Platform } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Snackbar from 'react-native-snackbar';
import Loader from './coach/global/loader';
import AsyncStorage from '@react-native-async-storage/async-storage';
const { width, height } = Dimensions.get('screen');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'react-native-i18n';

import RegisterPlayer from './registerPlayer';
import RegisterCoach from './registerCoach';

// create a component
const Register = ({ navigation, route }) => {

    // variables de vistas
    const [fcm, setFcm] = useState(null);
    const [loader, setLoader] = useState(false);
    const [index, setIndex] = useState(0); //// Indice del tabView
    const [routes] = useState([ //// Rutas del tabView
        { key: 'player', title: I18n.t('playerTab') },
        { key: 'coach', title: I18n.t('coachTab') },
    ]);


    console.warn("estamos aqui");

    useEffect(async () => {
        const isEnabled = true;
        const fcmToken = await AsyncStorage.getItem('@fcmToken');
        const fcmTokenJson = JSON.parse(fcmToken);
        setFcm(fcmTokenJson);
        return () => isEnabled = false;
    }, [])

    useLayoutEffect(() => {
        navigation.setOptions({
            title:I18n.t('titlePage'),
            headerLeft: (props) => {
                return (
                    <BottonBack {...props} />
                )
            },
        });
    }, [])

    const BottonBack = (props) => {
        return (
            <View style={{ width: 100, height: '100%' }}>
                <TouchableOpacity activeOpacity={0.5} onPress={() => back()} activeOpacity={0.8} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginLeft: -50 }}>
                    <Icon2 name="arrow-left" size={hp('2.4%')} color="#000" />
                </TouchableOpacity>
            </View>
        )
    }

    function back() {
        navigation.goBack()
    }

    function aviso(v) {
        setTimeout(() => {
            Snackbar.show({
                text: v,
                duration: Snackbar.LENGTH_LONG,
            });
        }, 1)
    }

    const Player = () => {
        return (
            <RegisterPlayer fcm={fcm} navigation={(v) => navigation.replace('validationSesion', { user: v })} aviso={(v) => { aviso(v) }} />
        )
    }

    const Coach = () => {
        return (
            <RegisterCoach fcm={fcm} navigation={(v) => navigation.replace('validationSesion', { user: v })} aviso={(v) => { aviso(v) }} />
        )
    }


    return (
        <View style={styles.container}>
            {loader ?
                <Loader />
                : null}
            <Image source={require('../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'), position: "absolute",

            }} />
            <TabView
                navigationState={{ index: index, routes }}
                renderScene={SceneMap({
                    player: Player,
                    coach: Coach,
                })}
                renderTabBar={(props) => <TabBar {...props}
                    indicatorStyle={{ backgroundColor: 'green' }}
                    style={{ backgroundColor: '#F3F3F3' }}
                    renderLabel={({ route, focused, color }) => (
                        <Text style={{ color: focused ? 'green' : '#646469', opacity: focused ? 1 : 0.5, }}>{route.title}</Text>
                    )}
                />}
                onIndexChange={(index) => setIndex(index)}
                initialLayout={{ width: Dimensions.get('screen').width }}
            />
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
    logo: {
        height: hp('7%'),
        width: wp('30%'),
    },
    inputs: {
        borderWidth: 1,
        height: 55,
        borderColor: '#E3E3E3',
        borderRadius: 10,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
    },
    contentInputs: {
        paddingHorizontal: 20,
    },
    createButton: {
        width: width * 0.5,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
    },
});



//make this component available to the app
export default Register;
