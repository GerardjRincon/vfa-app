//import liraries
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Modal, Image, Dimensions, SafeAreaView, Platform } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import IconF from 'react-native-vector-icons/FontAwesome';
import IconM  from 'react-native-vector-icons/MaterialCommunityIcons';
import {Button as GaButton, Block } from 'galio-framework'
const { width, height } = Dimensions.get('screen');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// create a component
const BottomTour = (props) => {
    const [snapToItem, setSnapToItem] = useState(0);
    const [h, setH] = useState(height - hp('8.9%'));
    const [entries, setEntries] = useState([
        {
            title:"You start by establishing or choosing the right program for your class, team and students, this will depend on the technical skill level of your students and players."
        },
        {
            title:"After you have established the students and players skill level you then work on an individual basis through the levels."
        }
    ])

    const RenderItem = ({ item, index }) => {
        return (
            <View style={{ width: wp('90%'), justifyContent: 'center', alignItems: 'center', }}>
                <Text style={{ fontSize: hp('2.6%') }}>{item.title}</Text>
            </View>
        );
    }


    return (
        <SafeAreaView style={{height:h, width:wp('100%'), justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)', paddingHorizontal:10, borderBottomWidth:1, borderColor:'green' }}>
                    
            <View style={{flex:1, justifyContent:'center', alignItems:'center', paddingHorizontal:10}}>
                <Text style={{fontSize:hp('5%'), fontWeight:'bold', color:'#FFF'}}>
                    {I18n.t('tutorial.text1')}
                </Text>
            </View>
                    
            <Block center style={{marginBottom:hp('10%'),}}>
                <View style={{justifyContent:'center', alignItems:'center', flexDirection: 'row',}}>
                    <GaButton round size="small" color="gray" style={{color:'#000'}} onPress={() => props.previous()}>Previous</GaButton>
                    <GaButton round size="small" color="success" onPress={() => props.next()}>Next</GaButton>
                </View>
                <GaButton round size="small" color="gray" style={{color:'#000'}} onPress={() => props.skip()}>Skip tour</GaButton>
            </Block>

            <View>
                    <IconM name="arrow-down" size={hp('10%')} color="#FFF"/>
            </View>
        </SafeAreaView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
    createButton: {
        width: wp('100%') * 0.5,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
    },
});

//make this component available to the app
export default BottomTour;
