//import liraries
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Modal, Image } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import {Button as GaButton, Block } from 'galio-framework'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'react-native-i18n';
// create a component
const Welcome = (props) => {
    const [snapToItem, setSnapToItem] = useState(0);
    const [entries, setEntries] = useState([
        {
            title:"You start by establishing or choosing the right program for your class, team and students, this will depend on the technical skill level of your students and players."
        },
        {
            title:"After you have established the students and players skill level you then work on an individual basis through the levels."
        }
    ])

    const RenderItem = ({ item, index }) => {
        return (
            <View style={{ width: wp('90%'), justifyContent: 'center', alignItems: 'center', }}>
                <Text style={{ fontSize:hp('4.5%'), fontWeight:'bold', color:'#FFF' }}>{item.title}</Text>
            </View>
        );
    }


    return (
        <View style={{flex:1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)', paddingHorizontal:10}}>
                    <View style={{paddingTop:hp('5%'), paddingBottom:hp('5%')}}>
                        <Image source={require('../../../assets/Logo.png')} resizeMode="cover" style={{width:320, height:150}}/>
                    </View>
                    <Carousel
                        // ref={(c) => { this._carousel = c; }}
                        data={entries}
                        index={snapToItem}
                        renderItem={RenderItem}
                        sliderWidth={wp('90%')}
                        itemWidth={wp('90%')}
                        onSnapToItem={(i)=>setSnapToItem(i)}
                        style={{justifyContent: 'center', alignItems: 'center', flex:1, backgroundColor: 'red', width:wp('100%')}}
                    />
                    <Pagination
                        style={{}}
                        dotsLength={entries.length}
                        activeDotIndex={snapToItem}
                        // containerStyle={{ }}
                        dotStyle={{
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            marginHorizontal: 8,
                            backgroundColor: 'green'
                        }}
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                    />
                    <Block center style={{marginBottom:hp('10%'), flexDirection: 'row',}}>
                        <GaButton round size="small" color="gray" style={{color:'#000'}} onPress={() => props.skip()}>{I18n.t("tutorial.text6")}</GaButton>
                        <GaButton round size="small" color="success" onPress={() => props.next()}>{I18n.t("tutorial.text5")}</GaButton>
                    </Block>
                </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
    createButton: {
        width: wp('100%') * 0.5,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
    },
});

//make this component available to the app
export default Welcome;
