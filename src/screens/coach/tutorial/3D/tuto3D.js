//import liraries
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Modal, Image } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import {Button as GaButton, Block } from 'galio-framework'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// create a component
const Tuto3D = (props) => {
    const [snapToItem, setSnapToItem] = useState(0);
    const [entries, setEntries] = useState([
        {
            title:I18n.t('tutorial.text22'),
            icon:require('../../../../assets/iconos/iconpie.png')
        },
        {
            title:I18n.t('tutorial.text23'),
            icon:require('../../../../assets/iconos/iconaudio.png')
        },
        {
            title:I18n.t('tutorial.text24'),
            icon:require('../../../../assets/iconos/iconidioma.png')
        },
        {
            title:I18n.t('tutorial.text25'),
            icon:require('../../../../assets/iconos/iconplay.png')
        },
        {
            title:I18n.t('tutorial.text26'),
            icon:require('../../../../assets/iconos/iconbar.png')
        },
        {
            title:I18n.t('tutorial.text27'),
            icon:require('../../../../assets/next.png')
        },
        {
            title:I18n.t('tutorial.text28'),
            icon:'icon',
            type:'AR'
        },
        {
            title:I18n.t('tutorial.text29'),
            icon:'icon',
            type:'3D'
        }

    ])

    const RenderItem = ({ item, index }) => {
        return (
            <View style={{ width: wp('90%'), height:hp('100%'), justifyContent: 'center', alignItems: 'center',}}>
                {item.icon === 'icon'?
                    item.type == 'AR'?
                    <Icon2 name="cube-scan" size={hp('20%')} color="#FFF" />
                    :
                    <Icon2 name="rotate-3d" size={hp('20%')} color="#FFF" />
                :
                    <Image source={item.icon} resizeMode="cover" style={{width:hp('20%'), height:hp('20%')}}/>
                }
                <Text style={{ fontSize:hp('4%'), fontWeight:'bold', color:'#FFF' }}>{item.title}</Text>
            </View>
        );
    }


    return (
        <View style={{flex:1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)', paddingHorizontal:10}}>
                    <Carousel
                        // ref={(c) => { this._carousel = c; }}
                        data={entries}
                        index={snapToItem}
                        renderItem={RenderItem}
                        sliderWidth={wp('90%')}
                        itemWidth={wp('90%')}
                        onSnapToItem={(i)=>setSnapToItem(i)}
                        style={{justifyContent: 'center', alignItems: 'center', flex:1, backgroundColor: 'red', width:wp('100%')}}
                    />
                    <Pagination
                        style={{}}
                        dotsLength={entries.length}
                        activeDotIndex={snapToItem}
                        // containerStyle={{ }}
                        dotStyle={{
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            marginHorizontal: 8,
                            backgroundColor: 'green'
                        }}
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                    />
                    <Block center style={{marginBottom:hp('10%'), flexDirection: 'row',}}>
                        <GaButton round size="small" color="success" onPress={() => props.skip()}> 
                          {I18n.t('tutorial.text9')}
                        </GaButton>
                    </Block>
                </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
    createButton: {
        width: wp('100%') * 0.5,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
    },
});

//make this component available to the app
export default Tuto3D;
