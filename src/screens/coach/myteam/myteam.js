//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Modal, TextInput, Image, KeyboardAvoidingView, Platform, Dimensions} from 'react-native';
import { Block,Button as GaButton } from 'galio-framework'
import { LogoutUser } from "../../../storage/user/dataUser";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { url } from '../../../storage/config';
// import { channel, pusher } from '../../../routes/DrawerCoach';
import Icon from 'react-native-vector-icons/FontAwesome';
import Select2 from "react-select2-native";
import Snackbar from 'react-native-snackbar';
import Loader from '../global/loader';
const { width, height } = Dimensions.get('screen');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'react-native-i18n';

import {signal, canal} from '../../../storage/pusher'
import { block } from 'react-native-reanimated';
const dataLevel = [
    {id:1, name: I18n.t('teams.levels.1')},
    {id:2, name: I18n.t('teams.levels.2')},
    {id:3, name: I18n.t('teams.levels.3')},
    {id:4, name:I18n.t('teams.levels.4')},
    {id:5, name:I18n.t('teams.levels.5')},
    {id:6, name:I18n.t('teams.levels.6')},
    {id:7, name:I18n.t('teams.levels.7')},
    {id:8, name:I18n.t('teams.levels.8')},
    {id:9, name:I18n.t('teams.levels.9')},
    {id:10, name:I18n.t('teams.levels.10')},
    {id:11, name:I18n.t('teams.levels.11')},
    {id:12, name:I18n.t('teams.levels.12')},
    {id:13, name:I18n.t('teams.levels.13')},
    {id:14, name:I18n.t('teams.levels.14')},
    {id:15, name:I18n.t('teams.levels.15')},
    {id:16, name:I18n.t('teams.levels.16')},
    {id:17, name:I18n.t('teams.levels.17')},
    {id:18, name:I18n.t('teams.levels.18')},
    {id:19, name:I18n.t('teams.levels.19')},
    {id:20, name:I18n.t('teams.levels.20')},
    {id:21, name:I18n.t('teams.levels.21')},
    {id:22, name:I18n.t('teams.levels.22')},
    {id:23, name:I18n.t('teams.levels.23')},
    {id:24, name:I18n.t('teams.levels.24')},
    {id:25, name:I18n.t('teams.levels.25')},
];

const dataCategory=[
    {id:1,name:I18n.t('teams.category.1')},
    {id:2,name:I18n.t('teams.category.2')},
    {id:3,name:I18n.t('teams.category.3')},
    {id:4,name:I18n.t('teams.category.4')},
    {id:5,name:I18n.t('teams.category.5')},
    {id:6,name:I18n.t('teams.category.6')},
    {id:7,name:I18n.t('teams.category.7')},
    {id:8,name:I18n.t('teams.category.8')},
    {id:9,name:I18n.t('teams.category.9')},
    {id:10,name:I18n.t('teams.category.10')},
    {id:11,name:I18n.t('teams.category.11')},
    {id:12,name:I18n.t('teams.category.2')},
    {id:13,name:I18n.t('teams.category.13')},

]

const dataCourse=[
    {id:1,name:I18n.t('teams.course.1')},
    {id:2,name:I18n.t('teams.course.2')},
    {id:3,name:I18n.t('teams.course.3')},
    {id:4,name:I18n.t('teams.course.4')},
    {id:5,name:I18n.t('teams.course.5')},

]
// create a component
const MyTeam = ({ navigation, route }) => {
    /////// Estados globales 
    const [visible, setVisible] = useState(false);
    const [loader, setLoader] = useState(false);
    const [data, setData] = useState([]);
    const [team, setTeam] = useState({});
    const [token, setToken] = useState(null);
    const [email, setEmail] = useState('');
    const [working, setWorking] = useState('');
    const [refresh, setRefresh] = useState(false)
    const [user, setUser] = useState({})
    const [workingLevel] = useState([
        {id:'level',name:'By Levels'},
        {id:'category',name:'By Categories'},
        {id:'course',name:'By Course'}
    ]);
    const [Levels] = useState(dataLevel);
    const [Categories] = useState(dataCategory);
    const [Courses] = useState(dataCourse);

    const [valiWorking,setValiWorking] = useState('');
    const [workingValue, setWorkingValue] = useState('');

    // const [team, setTeam] = useState({name:'My Teams'});


    useEffect(async() => {

        const token = await AsyncStorage.getItem('@token');
        const team = await AsyncStorage.getItem('@team');
        const user = await AsyncStorage.getItem('@user');
        const userJson = JSON.parse(user);
        const teamJson = JSON.parse(team);
        const tokenJson = JSON.parse(token);
        if (tokenJson) {
            setToken(tokenJson)
            setUser(userJson)
            setTeam(teamJson)
            getMyTeam(tokenJson,teamJson.id)

            const canal = signal.subscribe("VFA");
            

            canal.bind("Notification-" + userJson.id, (data) => {
                if (data.tag === 'ReplyPlayerYes') {
                    getMyTeam(tokenJson,teamJson.id)
                }
            });
        }



        


                canal.bind("NotificationUpdateData-"+userJson.id, (data) => {
                    console.log("se actualizo menu team");
                    getMyTeam(tokenJson,teamJson.id)
                    
                });


        const unsubscribe = navigation.addListener('focus', async() => {
            
        });
      
        return ()=>{
            signal.disconnect();
            unsubscribe;
        }

    }, [navigation])


    ////// Funciones de my team

    async function getMyTeam(t,id){
        setRefresh(true)
        setLoader(true)
        try {
            await fetch(url+'api/team/player/all',{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+t
            },
            body: JSON.stringify({
                team_id: id,
            }),
        }).then(res => res.json())
        .then((dat) =>{
            try {
                console.warn(dat.data)
                setData(dat.data)
                // setTeam(dat.team)
                setRefresh(false)
                setLoader(false)
                // if (dat.message == 'Unauthenticated.') {
                //     LogoutUser()
                //     setTimeout(()=>{
                //         navigation.replace('Login')
                //     },1000)
                // }
            } catch (error) {
                setRefresh(false)
                setLoader(false)
                Snackbar.show({
                    text: error,
                    duration: Snackbar.LENGTH_LONG,
                });
            }
        })
            
        } catch (error) {
            setRefresh(false)
            setLoader(false)
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
            });
        }
    }

    function clean(){
        setEmail(null);
        setVisible(false)
    }

    function Message(message){
        // console.warn(message)
        setTimeout(()=>{
            Snackbar.show({
                text: message,
                duration: Snackbar.LENGTH_LONG,
            });
        },1000);
        setVisible(false)
    }

    async function SendInvitation(){

        setLoader(true)

        if(!email){
            Snackbar.show({
                text: I18n.t('teams.alert.text1'),
                duration: Snackbar.LENGTH_LONG,
              });
              setLoader(false)
            return;
        }
        if(!valiWorking){
            Snackbar.show({
                text: I18n.t('teams.alert.text2'),
                duration: Snackbar.LENGTH_LONG,
              });
              setLoader(false)
            return;
        }
        if(!workingValue){
            Snackbar.show({
                text:I18n.t('teams.alert.text3'),
                duration: Snackbar.LENGTH_LONG,
              });
              setLoader(false)
            return;
        }
        try {
            setLoader(true)
            await fetch(url+'api/team/new/player',{
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
                },
                body: JSON.stringify({
                    email: email,
                    name_method: valiWorking,
                    value_method:workingValue
                }),
            }).then(res => res.json())
            .then((dat) =>{
                try {
                    setLoader(false)
                    Message(dat.data)
                    setEmail('')
                    setValiWorking('')
                    setWorkingValue('')
                    
                    // if (dat.message == 'Unauthenticated.') {
                    //     LogoutUser()
                    //     Snackbar.show({
                    //         text: dat.message,
                    //         duration: Snackbar.LENGTH_LONG,
                    //     });
                    //     setTimeout(()=>{
                    //         navigation.replace('Login')
                    //     },1000)
                    // }
                } catch (error) {
                    setLoader(false)
                    Snackbar.show({
                        text: error,
                        duration: Snackbar.LENGTH_LONG,
                    });
                }
            })
        } catch (error) {
            setLoader(false)
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
            });
        }
    }


    
    ////// Fin de funciones My team

    const RenderItem=({item, index, separators})=>{
        const imageMyTeam = 'https://go.vfa.app'+item.photo;
        return(
            <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('Player Profile', {player:item,token:token, team:team, update:(e)=>{getMyTeam(token,team.id)}})} style={{width:wp('100%'), height:hp('10%'),  flexDirection:'row',backgroundColor: "#FFF",}}>
                <View style={{width:wp('100%'), height:hp('10%'), flexDirection:'row',backgroundColor: "#fff",  borderBottomWidth:1, borderColor:'#f4f4f4'  }}>
                   
                   
                    <View style={{flex:1, marginLeft:20, justifyContent:'center',}}>
                        {item.photo?
                            <Image source={{uri:imageMyTeam}} resizeMode="cover" style={{width:hp('8%'),height: hp('8%'), borderRadius:wp('50%')}} />
                        :
                            <View style={{backgroundColor:'#eee', width:hp('8%'),height: hp('8%'), borderRadius:wp('50%'), justifyContent:'center', alignItems:'center'}}>
                                <Text style={{fontSize:hp('3%'), fontWeight:'bold', textTransform:'uppercase' }}>{item.name.charAt(0)}</Text>
                            </View>
                        }
                    </View>



                    <View style={{flex:4, flexDirection:'row',  justifyContent:'center', alignItems:'center'}}>
                        <View style={{flex:3}}>
                            <Text style={{fontSize:hp('1.7%'), fontWeight:'bold', textTransform:'capitalize', color:'#3d3d3d'}}>{item.name}</Text>
                            <Text style={{fontSize:hp('1.5%'),opacity:0.4, textTransform:'capitalize', color:'#3d3d3d', opacity:0.8 }}>{item.name_method == 'level'?item.item_name:item.name_method+': '}{item.name_method !== 'level'? item.item_name: null}</Text>
                            <Text style={{fontSize:hp('1.7%'), fontWeight:'bold', textTransform:'capitalize', color:'#3d3d3d'}}>{item.skill_name}</Text>
                        
                        </View>

                        <View style={{flex:2,height:15,backgroundColor: '#eee',marginRight:30, borderRadius:5, justifyContent:'center', alignItems:'center'}}>
                                <Text style={{position: 'absolute', color:'#000', fontWeight:'bold', zIndex: 10000, fontSize:hp('1.3%')}}>{item.media_skills}%</Text>
                                <View style={{width:'100%', height:'100%', borderRadius:10}}>
                                    <View style={{backgroundColor: 'green', height:'100%', width:'1%', borderRadius:2}}/>
                                </View>
                        </View>
                        {/* <View style={{marginRight: 20,}}>
                            <Icon name="chevron-right" size={hp('1.7%')} />
                        </View> */}
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    const Empty = ()=>{
        return(
            <View style={{justifyContent:'center', alignItems:'center', flex:1, marginTop:'50%',paddingRight: 40, paddingLeft: 40,}}>
                <Icon name="users" size={hp('15%')} color="#000" style={{ opacity: 0.3, paddingBottom: 20 }} />
                <Text style={{fontSize:hp('3%'),fontWeight:'bold', color:'#000', opacity:0.5}}>{I18n.t('teams.textInfo')}</Text>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            {loader?<Loader/>:null}
           
            <FlatList
                data={data}
                renderItem={RenderItem}
                keyExtractor={(item, index) => index}
                style={{ flex: 1 }}
                ListEmptyComponent={Empty}
                onRefresh={() => getMyTeam(token,user.team_id)}
                refreshing={refresh}
            />
            <TouchableOpacity onPress={()=>setVisible(true)} style={{position:'absolute', bottom:30, right:20, width:65, height:65, backgroundColor: '#11A10F', borderRadius:50, elevation:3, justifyContent: 'center', alignItems:'center'}}>
                <Icon name="plus" size={hp('2%')} color="white"/>
            </TouchableOpacity>

            <Modal 
            animationType="slide"
            transparent={true}
            visible={visible}>
                 <KeyboardAvoidingView  behavior={Platform.OS === "ios"? "padding":"null"} enabled style={{ flex: 1 ,flexDirection: 'column', justifyContent: 'flex-end',backgroundColor:'#0000002e',}}>
                    <View style={{ height: "100%", borderTopColor:'#eee',  borderTopWidth:2, width: '100%', backgroundColor:"#FFF", justifyContent:"center", borderTopRightRadius:10, borderTopLeftRadius:10}}>
                    
                    <Image source={require('../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    opacity:0.4,
                    position: "absolute",
            }} />

                     <TouchableOpacity onPress={()=>clean()} style={{flex:0.1,width:'100%', height:'4%', justifyContent:'center', alignItems:'center'}}>
                        <Icon name="chevron-down" size={hp('2%')} color="#333"/>
                     </TouchableOpacity>


                        <View style={{flex:1, borderTopRightRadius:10, borderTopLeftRadius:10}}>
                            
                            {/* <View style={{width:wp('100%'), paddingLeft:20}}>
                                <Text style={{fontSize:19, fontWeight:'bold', marginBottom:hp('-10%'), marginTop:20}}>{I18n.t('tex19')}</Text>
                            </View>
 */}

                    
                            <View style={{flex:2, justifyContent:'center'}}>
           
                            <Text style={{fontSize:19, paddingLeft:20, fontWeight:'bold',   marginTop:20}}>{I18n.t('tex19')}</Text>
                            <View style={{justifyContent:'center', alignItems:'center',}}>

                     
                                    <View style={{width:wp('90%'), paddingVertical: 10,}}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Email"
                                            placeholderTextColor="#848383"

                                            value={email}
                                            maxLenth={255}
                                            onChangeText={(v) => setEmail(v)}
                                        />
                                    </View>
                                    <View style={{width:wp('90%'), paddingVertical: 10,}}>
                                        <Select2
                                            isSelectSingle
                                            style={{ borderRadius: 5, backgroundColor: '#FFF', borderWidth:2,
                                            borderColor:"#eee", height:hp('7%'),fontWeight:'bold' }}
                                            colorTheme="blue"
                                            showSearchBox={false}
                                            popupTitle="Select the working method"
                                            title="Select the working method"
                                            data={workingLevel}
                                            onSelect={(d) => {
                                                setValiWorking(d[0]);
                                            }}
                                        />
                                    </View>
                                    <View style={{width:wp('90%'), paddingVertical: 10,}}>
                                        <Select2
                                            isSelectSingle
                                            showSearchBox={false}
                                            style={{ borderRadius: 5, backgroundColor: '#FFF', borderWidth:2,
                                            borderColor:"#eee", height:hp('7%'), fontWeight:'bold' }}
                                            colorTheme="blue"
                                            popupTitle={"Choose a "+valiWorking}
                                            title={"Choose a "+valiWorking}
                                            data={valiWorking=='level'?Levels:valiWorking=='category'?Categories:valiWorking=='course'?Courses:[]}
                                            onSelect={(d) => {
                                                setWorkingValue(d[0]);
                                            }}
                                        />
                                    </View>

                                    <Block center>
                                        <GaButton style={styles.createButton} round size="small" color="success" onPress={()=>SendInvitation()}>
                                            <Text style={{color:'white', fontWeight:'bold', textTransform:'uppercase'}}> Send Invitation</Text>
                                            <Icon name="send" style={{color:'white', marginLeft:10}}></Icon>
                                            </GaButton>
                                    </Block>
                            </View>
                            </View>
                            
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </Modal>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    input: {
        height: hp('7%'),
        borderWidth:2,
        borderColor:"#eee",
        borderRadius:5,
        paddingLeft:20,
        color:"#000",
        backgroundColor: "#FFF",
        
    },
    createButton: {
        borderRadius:5,
        width:wp('90%'),
        marginTop: 25,
        marginBottom: 40,
        backgroundColor:'#11A10F',
        flexDirection:'row',
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
    },
});

//make this component available to the app
export default MyTeam;
