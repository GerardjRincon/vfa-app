import React, { useState, useEffect } from 'react'
import { Text, View, Button, StyleSheet, FlatList, TouchableOpacity, Image, Dimensions, Platform, InteractionManager, Modal, SafeAreaView, NativeModules} from 'react-native'
import { url } from '../../../storage/config';
// import { channel, pusher } from '../../../routes/DrawerCoach';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { setSkill, LogoutUser, setExercises, setTutorialHome } from "../../../storage/user/dataUser";
import Icon from 'react-native-vector-icons/FontAwesome';
import SelectPicker from 'react-native-form-select-picker';
import ContentLoader, { Rect, Circle } from 'react-content-loader/native'
import { useNavigation } from '@react-navigation/native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
const { width, height } = Dimensions.get('screen');
import Loader from '../global/loader';
// import { useChannel, useEvent } from "@harelpls/use-pusher";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import Welcome from '../tutorial/welcome';
import BottomTour from '../tutorial/bottomTour';
import HomeTour from '../tutorial/homeTour';
import ReviewTour from '../tutorial/reviewTour';
import TeamsTour from '../tutorial/teamsTour';
import TrainingTour from '../tutorial/trainingTour';
import StatsTour from '../tutorial/statsTour';
import NotificationsTour from '../tutorial/notificationsTour';
import ChatTour from '../tutorial/chatTour';
import DrawerTour from '../tutorial/drawerTour';

import {signal, canal} from '../../../storage/pusher';


// import del api de pagos  
import RNIap, {
    InAppPurchase,
    PurchaseError,
    SubscriptionPurchase,
    acknowledgePurchaseAndroid,
    consumePurchaseAndroid,
    finishTransaction,
    finishTransactionIOS,
    purchaseErrorListener,
    purchaseUpdatedListener,
  } from 'react-native-iap';
import I18n from 'i18n-js';
  

  



const Home = ({navigation}) => {
    /////////////////////////////////////////// Estados globales
    const [welcome, setWelcome] = useState(false);
    const [bottomVisible, setBottomVisible] = useState(false);
    const [homeVisible, setHomeVisible] = useState(false);
    const [reviewVisible, setReviewVisible] = useState(false)
    const [teamsVisible, setTeamsVisible] = useState(false)
    const [trainingVisible, setTrainingVisible] = useState(false)
    const [statsVisible, setStatsVisible] = useState(false)
    const [notificationsVisible, setNotificationsVisible] = useState(false)
    const [chatVisible, setChatVisible] = useState(false)
    const [drawerVisible, setDrawerVisible] = useState(false)
    const [visibleDataCourse, setVisibleDataCourse] = useState(false)
    const [courseValue, setCourseValue] = useState('BASIC COURSE')
    // const navigation = useNavigation();  ///// Props de navegacion
    const [user, setUser] = useState({}); /// Objeto de usuario logueado
    const [token, setToken] = useState(null); //// Token de sesion
    const [loader, setLoader] = useState(false);
    const [index, setIndex] = useState(0); //// Indice del tabView
    const [snapToItem, setSnapToItem] = useState(0);
    const [lenguaje, setLenguaje] = useState('en');


    const [miuser, setMiuser] = useState(null);

    const [routes] = useState([ //// Rutas del tabView
        { key: 'skill', title: I18n.t('textos.text1') },
        { key: 'exercises', title: I18n.t('textos.text2') },
    ]);
    const [entries, setEntries] = useState([
        {
            title: "You start by establishing or choosing the right program for your class, team and students, this will depend on the technical skill level of your students and players."
        },
        {
            title: "After you have established the students and players skill level you then work on an individual basis through the levels."
        }
    ])

    // const channel = useChannel("VFA");

    /////////////////////////////////////////// Fin de estados globales

    /////////////////////////////////////////// Estados de Skill

    const [list, setList] = useState([]);
    const [method, setMethod] = useState('level');
    const [listSelect, setListSelect] = useState([{ name: I18n.t('textos.text3'), value: 'level',key:1 }, { name:  I18n.t('textos.text4'), value: "category", key:2 }, { name:  I18n.t('textos.text5'), value: "course", key:3 }])
    const [course, setCourse] = useState('BASIC COURSE');
    const [dataCourseValue, setdataCourseValue] = useState([
        {id:I18n.t('teams.course.'+1),name:I18n.t('teams.course.'+1),value:'BASIC COURSE'},
        {id:I18n.t('teams.course.'+2),name:I18n.t('teams.course.'+2),value:'MASTER THE BALL COURSE'},
        {id:I18n.t('teams.course.'+3),name:I18n.t('teams.course.'+3),value:'MASTER THE OPPONENT COURSE'},
        {id:I18n.t('teams.course.'+4),name:I18n.t('teams.course.'+4),value:'BEAT THE OPPONENT COURSE'},
        {id:I18n.t('teams.course.'+5),name:I18n.t('teams.course.'+5),value:'PERFECTION COURSE'},
    
    ]);

    /////////////////////////////////////////// Fin de estados de Skill

    /////////////////////////////////////////// Estados de Ejercicios

    const [listExercises, setListExercises] = useState([]);
    const [filterExercises, setFilterExercises] = useState(2);
    const [listSelectExcercises, setListSelectExcercises] = useState([{ title: I18n.t('textos.text6'), value: 2 }, { title: I18n.t('textos.text7'), value: 3 }]);

    /////////////////////////////////////////// Fin de estados de Ejercicios

    
    /////////////////////////////////////////// Funcion que se ejecuta antes de renderizar el DOM
    useEffect(async () => {
        // setLoader(true)


        // Função responsável por adquirir o idioma utilizado no device
        const getLanguageByDevice = () => {
        return Platform.OS === 'ios'
        ? NativeModules.SettingsManager.settings.AppleLocale // Adquire o idioma no device iOS
        : NativeModules.I18nManager.localeIdentifier // Adquire o idioma no device Android
        }
   
        
        const language = getLanguageByDevice()
        let idioma = language.substr(0,2);
   
         if(idioma == 'es'){
           setLenguaje('es')
         }
         else if(idioma == 'zh'){
           setLenguaje('ch')
         }
         else{
           setLenguaje('en')
         }
   
         console.log(lenguaje, 'este es el idioma que pasara para el 3D')




        await RNIap.initConnection(); // iniciando la aplicacion de pagos 


            const user = await AsyncStorage.getItem('@user');
            const token = await AsyncStorage.getItem('@token');
            const skills = await AsyncStorage.getItem('@skills');
            const exercises = await AsyncStorage.getItem('@exercises');
            const skillsJson = JSON.parse(skills);
            const exercisesJson = JSON.parse(exercises)
            const userJson = JSON.parse(user);


            setMiuser(userJson);
            const tokenJson = JSON.parse(token);
            // const tutorial = await AsyncStorage.getItem('@tutorialHome');
            // const tutorialHome = JSON.parse(tutorial);
            // if (!tutorialHome) {
            //     setWelcome(true);
            // }


        // validacion test 
        if(userJson && userJson.email != 'aaaa@aaaa.es'){
            if(userJson.invite != true) {
                console.log(userJson.invite, 'invite')
                console.log("no cumplio la validacion del email qwqwrqwr")
                // RNIap.endConnection();
                ValidacionSuscription(tokenJson, userJson); // llamamos a la funcion de verificar si tiene una suscripcion 
            }else {
                console.log("nowedwed cumplio la validacion del email qwqwrqwr")
                RNIap.endConnection();
            }
            
        }else {
            console.log("nowedwed cumplio la validacion del email qwqwrqwr")
            RNIap.endConnection();
        }


                // const tutorial = await AsyncStorage.getItem('@tutorialHome');
                // const tutorialHome = JSON.parse(tutorial);
                // if (tutorialHome) {
                //     setWelcome(true);
                // }

                AsyncStorage.getItem('@tutorialHome')
                .then(res => {
                    const tutorialHome = JSON.parse(res);
                    if (tutorialHome) {
                        setWelcome(true);
                    }
                })



                if (userJson) {
                    if (userJson.id) {
                        // console.warn(userJson)
                        if (skillsJson) {
                            setList(skillsJson);
                            setExercises(exercisesJson);
                        }
                        if (exercisesJson) {
                            setExercises(exercisesJson);
                        }
                        setUser(userJson)
                        getListExercises(tokenJson, filterExercises);
                        setToken(tokenJson)
                        getList(tokenJson, method);
                        setLoader(false)



                        const canal = signal.subscribe("VFA");


                        canal.bind("Notification-" + userJson.id, (data) => {
                            if (data.tag == 'SkillAssigned' || data.tag == 'SkillReviewed') {
                                getList(tokenJson, method);
                            }
                        });
                    }
                } else {
                    navigation.replace('Login');
                }
           
        return () => {
            signal.disconnect(canal);
        }

    }, [])

    /////////////////////////////////////////// Funciones anexas

   //   Validar suscripcion 
   async function ValidacionSuscription(tokenJson, userJson){
    try {
      console.info(
         'verificamos si tiene o no planes consumibles'
      );
      const purchases = await RNIap.getAvailablePurchases();
      console.info('Available purchases :: ', purchases);
      if (purchases && purchases.length > 0) {
        RNIap.endConnection();
         console.log("suscripcion activa");
       }else{
         console.log("no tiene suscripciones");
         ResetPaymentAccount(tokenJson, userJson);
      }
      } catch (err) {
       console.warn(err.code, err.message);
       alert(err.message);
    }
  };

  async function  ResetPaymentAccount(tokenJson, userJson){

    try {

        const datos =  {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenJson
            },
            body: JSON.stringify({
                id:userJson.id,
                pay:false
            }),
        }

        console.log(datos, 'enviando datos function ResetPaymentAccount');

      await fetch('https://go.vfa.app/api/play/unpay',datos).then(response => response.json())
      .then((res) => {


        console.log(res, 'respuesta de la consola logout')
            
        // si se cumple que el user se deslogue 
        alert(I18n.t('homecoach.alert.nosuscription'))
        logoutBackend(tokenJson);
       
      }).catch(error=>{
          console.log("aqui");
          console.warn(error)
      });
  } catch (error) {
      console.warn(error)
  }

  }


  async function logoutBackend(tokenJson){
    await fetch(url + 'api/logout', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenJson
        },
    }).then(res => res.json())
    .then(async(dat) => {
        console.warn(dat)
        try {
            if (dat.ok) {
                const keys = ['@user', '@token', '@skills', '@reviews', '@team', '@coach']
                
                // signal.disconnect();
                signal.unsubscribe("VFA");
                await AsyncStorage.multiRemove(keys)
                navigation.replace('Login');
            }
        } catch (error) {
            Snackbar.show({
                text:I18n.t('homecoach.alert.fileLogout'),
                duration: Snackbar.LENGTH_LONG,
            });
        }
    })
}







    function LoadSkill(value) {
        setMethod(value);
        getList(token, value);
    }

    function LoadCourse(value) {
        setCourse(value)
        setMethod('course');
        setCourseValue(value);
        getListCourse(token, 'course', value);
        // setVisibleDataCourse(false);
    }

    async function getListCourse(token, met, val) {
        setLoader(true)
        try {
            await fetch(url + 'api/filter/method/' + met, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    curso: val,
                }),
            }).then(res => res.json())
                .then((dat) => {
                    try {
                        if (dat.data) {
                            console.warn(dat)
                            setList(dat.data);
                            setSkill(dat.data);
                            setLoader(false)
                        }
                        // if (dat.message == 'Unauthenticated.') {
                        //     LogoutUser()
                        //     setTimeout(() => {
                        //         navigation.replace('Login')
                        //     }, 1000)
                        // }
                    } catch (error) {
                        setLoader(false)
                        // console.warn('error:'+error)
                    }
                })

        } catch (error) {
            setLoader(false)
            // console.warn(error)
        }
    }

    async function getList(token, met) {
        setLoader(true)
        try {
            await fetch(url + 'api/filter/method/' + met, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    curso: course,
                }),
            }).then(res => res.json())
                .then((dat) => {
                    try {
                        if (dat.data) {
                            console.warn(dat)
                            setList(dat.data);
                            setSkill(dat.data);
                            setLoader(false)
                        }
                        // if (dat.message == 'Unauthenticated.') {
                        //     LogoutUser()
                        //     setTimeout(() => {
                        //         navigation.replace('Login')
                        //     }, 1000)
                        // }
                    } catch (error) {
                        setLoader(false)
                        // console.warn('error:'+error)
                    }
                })

        } catch (error) {
            setLoader(false)
            // console.warn(error)
        }
    }

    async function getListExercises(token, v) {
        try {
            // console.warn(filterExercises)
            await fetch(url + 'api/exercises/list', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    players: v,
                }),
            }).then(res => res.json())
                .then((dat) => {
                    // console.warn(dat)
                    
                    try {
                        if (dat.data) {
                            setListExercises(dat.data);
                            setExercises(dat.data);
                        }
                        if (dat.message == I18n.t('Permissions.text7')) {
                            LogoutUser()
                            setTimeout(() => {
                                navigation.replace('Login')
                            }, 1000)
                        }
                    } catch (error) {
                        // console.warn('error:'+error)
                    }
                })

        } catch (error) {
            // console.warn(error)
        }
    }


    function updateListExercises(v) {
        // console.warn(v)
        setFilterExercises(v);
        getListExercises(token,v);
    }

    function visibleCourse(){
        setVisibleDataCourse(true);
        setCourse(courseValue)
        setMethod('course');
        setCourseValue(courseValue);
        getListCourse(token, 'course', courseValue);
    }
    /////////////////////////////////////////// Fin de Funciones anexas


    /////////////////////////////////////////// Todo lo relacionado a la vista de Skill

    const Skill = () => {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flexDirection: 'row', width: '100%', height: 50, alignItems: 'flex-end', marginTop: 5, marginBottom: 5 }}>
                    <View style={{ flex: 1, height: '100%', marginLeft: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFF', marginRight: 10, borderRadius: 10, elevation: 3 }}>
                        <View style={{flex:1, justifyContent: 'center', alignItems: 'center',}}>
                            <SelectPicker
                                onValueChange={(value) => {

                                    console.log("----- Value");
                                    if (value=='course') {
                                        visibleCourse()
                                    }else{
                                        setVisibleDataCourse(false);
                                        LoadSkill(value)
                                    }
                                }}
                                style={{ color: '#000' }}
                                selected={method}
                            >

                                {Object.values(listSelect).map((val, i) => (
                                    val.name?
                                        <SelectPicker.Item label={val.name} value={val.value}  key={i}/>
                                    :null
                                ))}

                            </SelectPicker>

                            {/* Comente aqui 1111 */}
                        </View>
                    </View>
                    {/* <View style={{flex:1,justifyContent: 'center', alignItems:'flex-end', marginBottom:10}}>
                        <Text style={{fontSize:hp('1.7%'), marginRight:20, elevation:2}}>SKILLS BY {method}</Text>
                    </View> */}
                </View>
                {visibleDataCourse?
                <View style={{ flexDirection: 'row', width: '100%', height: 50, alignItems: 'flex-end', marginTop: 5, marginBottom: 5 }}>
                    <View style={{ flex: 1, height: '100%', marginLeft: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFF', marginRight: 10, borderRadius: 10, elevation: 3 }}>
                        <SelectPicker
                            onValueChange={(value) => {
                                LoadCourse(value)
                            }}
                            style={{ color: '#000' }}
                            selected={courseValue}
                        >

                            {Object.values(dataCourseValue).map((val, i) => (
                                val.name?
                                    <SelectPicker.Item  label={val.name} value={val.value} key={i} />
                                :null
                            ))}

                        </SelectPicker>
                    </View>
                </View>
                :null}
                <FlatList
                    data={list}
                    renderItem={method == 'course' ? dataCourse : dataList}
                    keyExtractor={(item, i) => i}
                    style={{ width: '100%', height: '100%', }}
                />
            </View>
        )
    }


    
    const dataList = ({ item, index, separators }) => {

        console.log(item);
        return (
            <View key={item} style={{ width: '100%', height: 400, }}>
                <View style={{ paddingBottom: 10, paddingTop: 10, height: 60 }}>
                    {loader ?
                        <ContentLoader
                            height={30}
                            speed={0.2}
                            backgroundColor={'#F0F0F0'}
                            foregroundColor={'#E8E8E8'}
                            viewBox="0 0 400 20"
                        >
                            <Rect x="10" y="0" rx="4" ry="4" width="300" height="20" />
                        </ContentLoader>
                        :
                        method == 'level'?
                            <Text style={{ color: '#000', fontSize: hp('3%'), fontWeight:'bold', marginBottom: 5, marginLeft: 10 }}>{I18n.t('teams.levels.' + item.number_level)}</Text>
                        :
                            <Text style={{ color: '#000', fontSize: hp('2%'), fontWeight:'bold', marginBottom: 5, marginLeft: 10 }}>  {I18n.t('teams.category.'+item.id)}</Text>
                    }
                    <View style={{ backgroundColor: 'green', height: 5, width: '96%', borderRadius: 10, marginLeft: 10 }} />
                </View>
                <FlatList
                    data={item.skill}
                    renderItem={dataSkill}
                    keyExtractor={(item, i) => i}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    style={{ width: '100%', height: '100%', }}
                />
            </View>
        )
    }

    const dataSkill = ({ item, index, separators }) => {
        var urlImage = 'https://go.vfa.app/preview/' + item.img_preview;
        return (
            <View key={item} style={{ width: wp('40%'), height: '85%', backgroundColor: '#FFF', marginHorizontal: 10, borderRadius: 10, marginTop: 10, elevation: 3, }}>
                {loader ?
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <ContentLoader
                            height={hp('30%')}
                            speed={0.2}
                            backgroundColor={'#F0F0F0'}
                            foregroundColor={'#E8E8E8'}
                            viewBox="0 0 330 550"
                        >
                            <Rect x="10" y="0" rx="4" ry="4" width="300" height="300" />
                            <Rect x="10" y="330" rx="4" ry="4" width="300" height="13" />
                            <Rect x="10" y="380" rx="4" ry="4" width="200" height="13" />
                            <Rect x="10" y="420" rx="4" ry="4" width="200" height="13" />
                        </ContentLoader>
                    </View>
                    :
                    <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate('Details skill', { skill: item,token: token,lenguaje:lenguaje, user })} style={{ flex: 1 }}>
                        <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: wp('40%'), height: 200, borderRadius: 10 }} />
                        <View style={{ position: 'absolute', width: 30, height: 30, backgroundColor: '#FFF', borderRadius: 50, right: 2, top: 2, justifyContent: 'center', alignItems: 'center' }}>
                            {method == "level" || method == "course" ?
                                <Text style={{ fontSize: hp('1.5%'), fontWeight: 'bold' }}>{index + 1}</Text>
                                :
                                <Text style={{ fontSize: hp('1.5%'), fontWeight: 'bold' }}>{item.number}</Text>
                            }
                        </View>
                        <View style={{ flex: 1, padding: 10 }}>
                            <Text numberOfLines={1} style={{ fontSize: hp('1.7%'), fontWeight: 'bold', color: '#000' }}> {I18n.t('skills.'+item.id)}</Text>
                        </View>
                        <View style={{ flex: 1, padding: 10, justifyContent: 'center', flexDirection: 'row' }}>
                            <View style={{ width: wp('30%'), }}>
                                <Text numberOfLines={1} style={{ fontSize: hp('1.2%'), marginTop: 6, color: '#000', fontWeight: 'bold', }}>
                                {I18n.t('teams.category.'+item.category_id) }</Text>
                            </View>
                            <View style={{ width: wp('5%'), justifyContent: 'center', alignItems: 'center', }}>
                                <Icon name="user" size={hp('1.2%')} color="green" />
                                <Text style={{ fontSize: hp('1.2%') }}>{item.approved}/{item.all}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                }
            </View>
        );
    }

    const dataCourse = ({ item, index, separators }) => {
        return (
            <View key={item} style={{ width: '100%', height: 2070, }}>
                {/* <View style={{ paddingBottom: 10, paddingTop: 10 }}>
                    <Text style={{ color: '#000', fontSize: hp('4%'), fontWeight:'bold', marginBottom: 5, marginLeft: 10 }}>{item.title}</Text>
                </View> */}
                <FlatList
                    data={item.level}
                    renderItem={dataList}
                    keyExtractor={(item, i) => i}
                    ListFooterComponent={<View style={{height:30}}/>}
                    style={{ width: '100%', height: '100%' }}
                />
            </View>
        )
    }

    /////////////////////////////////////////// Fin de todo lo relacionado con la vista de skill


    /////////////////////////////////////////// Todo lo relacionado con la vista de ejercicios
    const Exercises = () => {
        return (
            <View style={{ flex: 1}}>
                <View style={{ flexDirection: 'row', width: '100%', height: 50, alignItems: 'flex-end', marginTop: 5, marginBottom: 5 }}>
                    <View style={{ flex: 1, height: '100%', marginLeft: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFF', marginRight: 10, borderRadius: 10, elevation: 3 }}>
                        
                        <SelectPicker
                            onValueChange={(value) => {
                                updateListExercises(value)
                            }}
                            style={{ color: '#000' }}
                            selected={filterExercises}
                        >

                            {Object.values(listSelectExcercises).map((val, i) => (
                                <SelectPicker.Item  label={val.title} value={val.value} key={i}/>
                            ))}

                        </SelectPicker>
                        {/* comente aqui  */}

                    </View>
                </View>
                <View style={{ flex: 1 }}>
                    <FlatList
                        data={listExercises}
                        key={(item) => item}
                        renderItem={dataListExcercises}
                        keyExtractor={(item, i) => i}
                        style={{ width: '100%', height: '100%', }}
                    />
                </View>
            </View>
        )
    }

    const dataListExcercises = ({ item, index, separators }) => {
        return (
            item.exercises.length > 0 ?
                <View key={item} style={{ flex: 1 }}>
                    <View style={{ paddingBottom: 10, paddingTop: 10 }}>
                        {loader ?
                            <ContentLoader
                                height={30}
                                speed={0.2}
                                backgroundColor={'#F0F0F0'}
                                foregroundColor={'#E8E8E8'}
                                viewBox="0 0 400 20"
                            >
                                <Rect x="10" y="0" rx="4" ry="4" width="300" height="20" />
                            </ContentLoader>
                            :
                            <Text style={{ color: '#000', fontSize: hp('2%'), marginBottom: 5, marginLeft: 10 }}>{I18n.t('teams.exercises_categories.'+item.id)}</Text>
                        }
                        <View style={{ backgroundColor: 'green', height: 5, width: '96%', borderRadius: 10, marginLeft: 10 }} />
                    </View>
                    <FlatList
                        data={item.exercises}
                        renderItem={dataExcercises}
                        keyExtractor={(item, i) => i}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        style={{ width: '100%', height: '100%' }}
                    />
                </View>
                : null
        )
    }

    const dataExcercises = ({ item, index, separators }) => {
        var urlImage = 'https://go.vfa.app/exercises/' + item.file + '.jpeg';
        return (
            <View key={item} style={{ width: wp('40%'), height: '95%', backgroundColor: '#FFF', marginHorizontal: 10, borderRadius: 10, marginTop: 10, elevation: 3 }}>
                {loader ?
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <ContentLoader
                            height={hp('20%')}
                            speed={0.2}
                            style={{ borderRadius: 10 }}
                            backgroundColor={'#F0F0F0'}
                            foregroundColor={'#E8E8E8'}
                            viewBox="0 0 330 300"
                        >
                            <Rect x="15" y="10" rx="10" ry="10" width="300" height="270" />
                        </ContentLoader>
                    </View>
                    :
                    <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate('Details exercises', { exercise: item, token: token, lenguaje:lenguaje, user })} style={{ flex: 1, }}>
                        <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: wp('40%'), height: 200, borderRadius: 10 }} />
                        <View style={{ position: 'absolute', width: 30, height: 30, backgroundColor: '#FFF', borderRadius: 50, right: 2, top: 2, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: hp('1.5%'), fontWeight: 'bold' }}>{index + 1}</Text>
                        </View>
                    </TouchableOpacity>
                }
            </View>
        )
    }

    /////////////////////////////////////////// Fin de todo lo relacionado con la vista de ejercicios

    return (
        <View style={styles.center}>
            {loader?<Loader/>:null}
            <Image source={require('../../../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'),
                position: "absolute",
            }} />


            <TabView
                navigationState={{ index: index, routes }}
                renderScene={SceneMap({
                    skill: Skill,
                    exercises: Exercises,
                })}
                renderTabBar={(props) => <TabBar {...props}
                     pressColor={'transparent'}
                    indicatorStyle={{ backgroundColor: 'green' }}
                    style={{ backgroundColor: '#F3F3F3' }}
                    renderLabel={({ route, focused, color }) => (
                        <Text style={{ color: focused ? 'green' : '#646469', opacity: focused ? 1 : 0.5, }}>{route.title}</Text>
                    )}
                />}
                onIndexChange={(index) => setIndex(index)}
                initialLayout={{ width: Dimensions.get('screen').width }}
               />


               

                <Modal
                    animationType="fade"
                    // onRequestClose={() => setWelcome(false)}
                    visible={welcome}
                    transparent
                >
                    <Welcome next={()=>{
                        setWelcome(false);
                        setBottomVisible(true);
                    }}
                    skip={()=>{
                        setTutorialHome(false)
                        setWelcome(false);
                    }}/>
                </Modal>
                <Modal
                    animationType="fade"
                    // onRequestClose={() => setBottomVisible(false)}
                    visible={bottomVisible}
                    transparent
                >
                    <BottomTour next={()=>{
                        setBottomVisible(false);
                        setHomeVisible(true);
                    }}
                    previous={()=>{
                        setBottomVisible(false);
                        setWelcome(true);
                    }}
                    skip={()=>{
                        setTutorialHome(false)
                        setBottomVisible(false);
                    }}/>
                </Modal>
                <Modal
                    animationType="fade"
                    // onRequestClose={() => setBottomVisible(false)}
                    visible={homeVisible}
                    transparent
                >
                    <HomeTour next={()=>{
                        setHomeVisible(false)
                        setReviewVisible(true)
                    }}
                    previous={()=>{
                        setBottomVisible(true);
                        setHomeVisible(false);
                    }}
                    skip={()=>{
                        setTutorialHome(false)
                        setHomeVisible(false)
                    }}/>
                </Modal>
                <Modal
                    animationType="fade"
                    // onRequestClose={() => setBottomVisible(false)}
                    visible={reviewVisible}
                    transparent
                >
                    <ReviewTour next={()=>{
                        setReviewVisible(false)
                        setTeamsVisible(true)
                    }}
                    previous={()=>{
                        setHomeVisible(true);
                        setReviewVisible(false);
                    }}
                    skip={()=>{
                        setTutorialHome(false)
                        setReviewVisible(false)
                    }}/>
                </Modal>
                <Modal
                    animationType="fade"
                    // onRequestClose={() => setBottomVisible(false)}
                    visible={teamsVisible}
                    transparent
                >
                    <TeamsTour next={()=>{
                        setTeamsVisible(false)
                        setTrainingVisible(true)
                    }}
                    previous={()=>{
                        setTeamsVisible(false)
                        setReviewVisible(true);
                    }}
                    skip={()=>{
                        setTutorialHome(false)
                        setTeamsVisible(false)
                    }}/>
                </Modal>
                <Modal
                    animationType="fade"
                    // onRequestClose={() => setBottomVisible(false)}
                    visible={trainingVisible}
                    transparent
                >
                    <TrainingTour next={()=>{
                        setTrainingVisible(false)
                        setStatsVisible(true)
                    }}
                    previous={()=>{
                        setTrainingVisible(false)
                        setTeamsVisible(true);
                    }}
                    skip={()=>{
                        setTutorialHome(false)
                        setTrainingVisible(false)
                    }}/>
                </Modal>
                <Modal
                    animationType="fade"
                    // onRequestClose={() => setBottomVisible(false)}
                    visible={statsVisible}
                    transparent
                >
                    <StatsTour next={()=>{
                        setStatsVisible(false)
                        setChatVisible(true)
                    }}
                    previous={()=>{
                        setTrainingVisible(true)
                        setStatsVisible(false);
                    }}
                    skip={()=>{
                        setTutorialHome(false)
                        setStatsVisible(false)
                    }}/>
                </Modal>
                <Modal
                    animationType="fade"
                    // onRequestClose={() => setBottomVisible(false)}
                    visible={chatVisible}
                    transparent
                >
                    <ChatTour next={()=>{
                        setChatVisible(false)
                        setNotificationsVisible(true)
                    }}
                    previous={()=>{
                        setStatsVisible(true)
                        setChatVisible(false);
                    }}
                    skip={()=>{
                        setTutorialHome(false)
                        setChatVisible(false)
                    }}/>
                </Modal>
                <Modal
                    animationType="fade"
                    // onRequestClose={() => setBottomVisible(false)}
                    visible={notificationsVisible}
                    transparent
                >
                    <NotificationsTour next={()=>{
                        setNotificationsVisible(false)
                        setDrawerVisible(true)
                    }}
                    previous={()=>{
                        setNotificationsVisible(false)
                        setChatVisible(true);
                    }}
                    skip={()=>{
                        setTutorialHome(false)
                        setNotificationsVisible(false)
                    }}/>
                </Modal>
                <Modal

                    animationType="fade"
                    // onRequestClose={() => setBottomVisible(false)}
                    visible={drawerVisible}
                    transparent
                >
                    <DrawerTour next={()=>{
                        setDrawerVisible(false)
                        setTutorialHome(true)
                    }}
                    previous={()=>{
                        setNotificationsVisible(true)
                        setDrawerVisible(false);
                    }}
                    skip={()=>{
                        setTutorialHome(false)
                        setDrawerVisible(false)
                    }}/>
                </Modal>
                {/* <Modal
                animationType="fade"
                // onRequestClose={() => setBottomVisible(false)}
                visible={visibleDataCourse}
                transparent>
                    <SafeAreaView style={{flex:1, width:wp('100%'), justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)', paddingHorizontal:10, }}>
                        <SelectPicker
                            onValueChange={(value) => {
                                LoadCourse(value)
                            }}
                            style={{ color: '#FFF' }}
                            selected={courseValue}
                        >

                            {Object.values(dataCourse).map((val, i) => (
                                <SelectPicker.Item key={i} label={val.name} value={val.name} />
                            ))}

                        </SelectPicker>

                    </SafeAreaView>
                </Modal> */}
        </View>
    );
};

const styles = StyleSheet.create({
    center: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    createButton: {
        width: wp('100%') * 0.5,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
    },
});

export default Home;