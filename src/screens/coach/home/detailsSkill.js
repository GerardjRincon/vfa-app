import React, { useState, useEffect, useLayoutEffect, useRef } from 'react'
import { Divider } from 'react-native-elements';
import { Text, View, StyleSheet, Image, TouchableOpacity, FlatList, Modal, Alert, Animated, TextInput, Dimensions, KeyboardAvoidingView, Platform, SafeAreaView } from 'react-native'
import { useNavigation, useRoute, useTheme } from '@react-navigation/native';
import { url } from '../../../storage/config';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { setSkill, setUser, setTutorialSkillDetails } from "../../../storage/user/dataUser";
import { Block, Button as GaButton } from 'galio-framework'
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import DatePicker from 'react-native-datepicker'
const { width, height } = Dimensions.get('screen');
import Loader from '../global/loader';
import Video2 from 'react-native-video';
// import { copilot, walkthroughable, CopilotStep } from "react-native-copilot";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'i18n-js';


const DetailsSkill = (props) => {
    const route = useRoute();
    const { skill, token,lenguaje, user } = route.params;
    const navigation = useNavigation();
    // const CopilotText = walkthroughable(Text);
    // const CopilotView = walkthroughable(View);
    const [loader, setLoader] = useState(false);
    const [playersData, setPlayersData] = useState([]);
    const [videosData, setVideosData] = useState([]);
    const [favoritesData, setFavoritesData] = useState([]);
    const [skillDetails, setSkillDetails] = useState(skill);
    const [visible, setVisible] = useState(false);
    const [listPlayer, setListPlayer] = useState([]);
    const [multiSelected, setMultiSelected] = useState(false);
    const [index, setIndex] = useState(0);
    const [date, setDate] = useState(null);
    const [description, setDescription] = useState(null);
    const [visibleEvent, setVisibleEvent] = useState(false);
    const [myteam, setMyteam] = useState({});
    const opacity = useRef(new Animated.Value(0)).current;
    const menu = useRef(new Animated.Value(56)).current;
    const [refresh, setRefresh] = useState(false);
    const [refreshPlayer, setRefreshPlayer] = useState(false)
    const urlImage = 'https://go.vfa.app/preview/' + skill.img_preview;

    const [routes, setRoutes] = useState([ // Esta es una constante de rutas
        { key: 'players', title: 'Player(s)' },
        { key: 'videos', title: 'Videos' },
        { key: 'favorites', title: 'Favorite' },
    ]);
    const [visibleVideo, setVisibleVideo] = useState(false);
    const [urlVideo, setUrlVideo] = useState(null);
    const [playerVideo, setPlayerVideo] = useState(null);

    useEffect(async () => {

        
        const unsubscribe = navigation.addListener('focus', async() => {
            const team = await AsyncStorage.getItem('@team');
            const tutorial = await AsyncStorage.getItem('@tutorialSkillDetails');
            const teamJson = JSON.parse(team);
            const tutorialSkillDetails = JSON.parse(tutorial);
            // if (!tutorialSkillDetails) {
            //     props.start();
            //     props.copilotEvents.on("stop", () => {
            //         setTutorialSkillDetails(true)
            //     });
            // }
            setMyteam(teamJson)
            getViewPlayer()
            getPlayer()
            console.warn(skill)
            
        });
        return unsubscribe;
    }, [navigation])

    useLayoutEffect(() => {
        navigation.setOptions({
            title: I18n.t('navigations.text1')
        });
    }, [])

    async function getViewPlayer() {
        console.warn('entro')
        setLoader(true)
        setRefresh(true)
        try {
            await fetch(url + 'api/skill/players/assigned/' + skill.id, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
            }).then(res => res.json())
                .then((dat) => {
                    try {
                        console.warn(dat.players_video)
                        setPlayersData(dat.data);
                        setVideosData(dat.players_video);
                        setFavoritesData(dat.players_favorite);
                        setLoader(false)
                        setRefresh(false)
                    } catch (error) {
                        console.warn(error)
                        Snackbar.show({
                            text: error,
                            duration: Snackbar.LENGTH_LONG,
                        });
                        setLoader(false)
                        setRefresh(false)
                    }
                })
            console.warn('termino')

        } catch (error) {
            console.warn(error)
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
            });
            setLoader(false)
            setRefresh(false)
        }
    }

    async function getPlayer() {
        // setLoader(true)
        setRefreshPlayer(true)
        try {
            await fetch(url + 'api/skill/players/noassigned/' + skill.id, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
            }).then(res => res.json())
                .then((dat) => {
                    console.warn(dat)
                    try {
                        // console.warn(dat)
                        setListPlayer(dat.data)

                        console.log(dat.data, 'lista de kenneth');

                        setRefreshPlayer(false)
                        // setLoader(false)
                    } catch (error) {
                        setRefreshPlayer(false)
                        Snackbar.show({
                            text: error,
                            duration: Snackbar.LENGTH_LONG,
                        });
                    }
                })
        } catch (error) {
            setRefreshPlayer(false)
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
            });
        }
    }

    const sendInvite = async () => {
        setVisible(false)
        setLoader(true)
        try {
            let player = listPlayer.filter(item => item.isSelected);
            let ids = [];
            player.forEach((v) => {
                ids.push(v.id);
            });
            await fetch(url + 'api/skill/assigned', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    name_method: myteam.working,
                    players_id: ids.toString(),
                    skill_id: skill.id,
                    value_method: skill.level_id,
                }),
            }).then(res => res.json())
                .then((dat) => {
                    try {
                        setVisible(false)
                        getViewPlayer()
                        cleanIsSelect()
                        setLoader(false)
                        setTimeout(() => {
                            Snackbar.show({
                                text: dat.data,
                                duration: Snackbar.LENGTH_LONG,
                            });
                        }, 500);
                        // setLoader(false)
                    } catch (error) {
                        setLoader(false)
                        Snackbar.show({
                            text: error,
                            duration: Snackbar.LENGTH_LONG,
                        });
                    }
                })
        } catch (error) {
            setLoader(false)
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
            });
        }
    }

    const createEvent = async () => {
        setLoader(true)
        try {
            if (!date) {
                setLoader(true)
                Snackbar.show({
                    text: I18n.t('skillData.alert.text1'),
                    duration: Snackbar.LENGTH_LONG,
                });
                return;
            }
            await fetch(url + 'api/calendar/create', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    date: date,
                    description: description,
                    skill_id: skill.id,
                }),
            }).then(res => res.json())
                .then((dat) => {
                    try {
                        setLoader(false)
                        cleanCreateEvent()
                        Snackbar.show({
                            text: dat.data,
                            duration: Snackbar.LENGTH_LONG,
                        });
                        // setLoader(false)
                    } catch (error) {
                        setLoader(false)
                        Snackbar.show({
                            text: error,
                            duration: Snackbar.LENGTH_LONG,
                        });
                    }
                })
        } catch (error) {
            setLoader(false)
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
            });
        }
    }

    function cleanIsSelect() {
        const list = [...listPlayer]
        list.forEach(v => {
            v.isSelected = false
        });
        setListPlayer(list);
        cerrarAnimacion()
        setTimeout(() => setVisible(false), 1)
    }

    function animacion(type) {
        Animated.timing(opacity, {
            toValue: type,
            duration: 500,
            useNativeDriver: true,
        }).start();
    }

    function cerrarAnimacion() {
        Animated.timing(menu, {
            toValue: 56,
            duration: 400,
            useNativeDriver: true,
        }).start();
    }

    const selectItem = (index) => {
        //invierto la seleccion del item (selecciono o desselecciono)
        //actualizo el state para que se renderice
        const array = [...listPlayer]
        array[index].isSelected = !array[index].isSelected;
        setListPlayer(array)
        setMultiSelected(true)

        animacion(1)
        Animated.timing(menu, {
            toValue: 0,
            duration: 400,
            useNativeDriver: true,
        }).start();

        let very = listPlayer.filter(item => item.isSelected).length;
        if (very < 1) {
            setMultiSelected(false)
            animacion(0)
            Animated.timing(menu, {
                toValue: 56,
                duration: 400,
                useNativeDriver: true,
            }).start();
        }
    }

    function cleanCreateEvent() {
        setVisibleEvent(false)
        setDescription(false)
        setDate(null)
    }




    const CardVideos = ({ item, index, separators }) => {
        const imagePlayer = 'https://go.vfa.app/' + item.photo;
        const imageVideo = item.file_captura;
        return (
            <TouchableOpacity activeOpacity={0.8} onPress={() =>{navigation.navigate('Video player',{player:item, token:token, view:'skill'})}}  style={{ width: wp('100%'), height: hp('10%'), flexDirection: 'row', borderBottomWidth: 1, borderColor: '#D6D6D6', backgroundColor: '#fff', }}>
                <View style={{ flex: 1, marginLeft: 20, justifyContent: 'center' }}>
                    {item.photo ?
                        <Image source={{ uri: imagePlayer }} resizeMode="cover" style={{ width: hp('8%'), height: hp('8%'), borderRadius: 50 }} />
                        :
                        <View style={{ backgroundColor: '#D6D6D6', width: hp('8%'), height: hp('8%'), borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: hp('2%'), fontWeight: 'bold', }}>{item.name.charAt(0)}</Text>
                        </View>
                    }
                </View>
                <View style={{ flex: 4, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderBottomWidth: 1, borderColor: '#D6D6D6' }}>
                    <Text style={{ fontSize: hp('1.7%'), fontWeight: 'bold' }}>{item.name}</Text>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 20 }}>
                        {item.file_captura ?
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={{ uri: imageVideo }} resizeMode="cover" style={{ width: hp('8%'), height: hp('8%'), borderRadius: 10 }} />
                                <View style={{ position: 'absolute', width: hp('5%'), height: hp('5%'), backgroundColor: 'rgba(0,0,0,0.3)', borderRadius: 50, borderColor: '#D6D6D6', borderWidth: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon name="play" size={hp('2')} color="#FFF" style={{ marginLeft: 5 }} />
                                </View>
                            </View>
                            :
                            <Icon name="film" size={hp('7%')} color="#D6D6D6" />
                        }
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    const CardPlayer = ({ item, index, separators }) => {
        const imagePlayer = 'https://go.vfa.app/' + item.photo;
        return (
            <View style={{ width: wp('100%'), height: hp('10%'), flexDirection: 'row', borderBottomWidth: 1, borderColor: '#D6D6D6', backgroundColor: '#fff', }}>
                <View style={{ flex: 1, marginLeft: 20, justifyContent: 'center' }}>
                    {item.photo ?
                        <Image source={{ uri: imagePlayer }} resizeMode="cover" style={{ width: hp('8%'), height: hp('8%'), borderRadius: 50 }} />
                        :
                        <View style={{ backgroundColor: '#D6D6D6', width: hp('8%'), height: hp('8%'), borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: hp('2%'), fontWeight: 'bold', }}>{item.name.charAt(0)}</Text>
                        </View>
                    }
                </View>
                <View style={{ flex: 4, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                    <Text style={{ fontSize: hp('1.7%'), fontWeight: 'bold' }}>{item.name}</Text>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 20 }}>
                        {item.skill_qualification?
                            <View style={{ flexDirection:'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name="trophy" size={hp('1.8%')} color="#eab900" />
                                <Text style={{ fontSize: hp('1.5'), marginLeft:5, fontWeight: 'bold', }}>{item.skill_qualification}</Text>
                            </View>
                        :
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Icon2 name="account-clock" size={hp('4%')} color="#FFA200" />
                            </View>
                        }
                    </View>
                </View>
            </View>
        );
    }
    
    const CardFavorite = ({ item, index, separators }) => {
        var imagePlayer = 'https://go.vfa.app/' + item.photo;
        var imageVideo = item.file_captura;
        return (
            <TouchableOpacity onPress={() =>{navigation.navigate('Video player',{player:item, token:token, view:'skill'})}} style={{ width: wp('100%'), height: hp('10%'), flexDirection: 'row', borderBottomWidth: 1, borderColor: '#D6D6D6', backgroundColor: '#fff', }}>
                <View style={{ flex: 1, marginLeft: 20, justifyContent: 'center', marginRight: 10 }}>
                    {item.file_captura ?
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={{ uri: imageVideo }} resizeMode="cover" style={{ width: hp('8%'), height: hp('8%'), borderRadius: 10 }} />
                            <View style={{ position: 'absolute', width: hp('5%'), height: hp('5%'), backgroundColor: 'rgba(0,0,0,0.3)', borderRadius: 50, borderColor: '#D6D6D6', borderWidth: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name="play" size={hp('2')} color="#FFF" style={{ marginLeft: 5 }} />
                            </View>
                        </View>
                        :
                        <Icon name="film" size={hp('7%')} color="#D6D6D6" />
                    }
                </View>
                <View style={{ flex: 4, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', borderBottomWidth: 1, borderColor: '#D6D6D6' }}>
                    <Text style={{ fontSize: hp('1.7%'), fontWeight: 'bold', }}>{item.name}</Text>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 20 }}>
                        <Icon2 name="star" size={hp('2%')} color="#F6D24C" />
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    var CardListPlayer = ({ item, index, separators }) => {
        const imagenItemPlayer = 'https://go.vfa.app/' + item.photo;
        return (
            <TouchableOpacity activeOpacity={item.course_noassigned ? 1 : 0.8} onPress={() => {
                if(item.course_noassigned == false){
                    selectItem(index)
                }else{
                    Snackbar.show({
                        text: "This player is doing a course. Level not accepted.",
                        duration: Snackbar.LENGTH_LONG,
                    });
                }
            }} style={{ width: wp('100%'), height: hp('10%'), flexDirection: 'row', backgroundColor: item.course_noassigned ? '#e3e4e5':'#FFF', borderBottomWidth: 1, borderColor: '#D6D6D6' }}>
                <View style={{ flex: 1, marginLeft: 20, justifyContent: 'center', }}>
                    {item.photo ?
                        <Image source={{ uri: imagenItemPlayer }} resizeMode="cover" style={{ width: hp('8%'), height: hp('8%'), borderRadius:wp('50%') }} />
                        :
                        <View style={{ backgroundColor: '#f5f5f5', width: hp('8%'), height: hp('8%'), borderRadius:wp('50%'), justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: hp('4%'), fontWeight: 'bold', textTransform:'uppercase'}}>{item.name.charAt(0)}</Text>
                        </View>
                    }
                    {item.isSelected ?
                        <Animated.View key={index} style={{ opacity, position: 'absolute', right: 8, bottom: 8, backgroundColor: 'green', borderRadius: 50, padding: 3, elevation: 3 }}>
                            <Icon name="check" size={hp('2%')} color="#fff" />
                        </Animated.View>
                        : null}
                </View>
                <View style={{ flex: 4, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                    <View style={{ flex: 3 }}>
                        <Text style={{ fontSize: hp('1.9%'), fontWeight: 'bold', textTransform:'capitalize' }}>{item.name}</Text>
                        <Text style={{fontSize:hp('1.6%'), textTransform:'capitalize' }}>{item.name_method == 'level'?item.namemethod:item.name_method+': '}{item.name_method !== 'level'? item.namemethod: null}</Text>
                        <Text style={{fontSize:hp('1.5%'), fontWeight:'bold', textTransform:'uppercase' }}>{item.skill_name}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    const Players = () => {
        return (
            <View style={{ flex: 1,}}>
                {playersData.length > 0 ?
                    <FlatList
                        data={playersData}
                        renderItem={CardPlayer}
                        keyExtractor={(item, index) => index}
                        style={{ flex: 1 }}
                        onRefresh={() => getViewPlayer()}
                        refreshing={refresh}
                    />
                    :
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingRight: 40, paddingLeft: 40, }}>
                        <Icon name="users" size={hp('15%')} color="#000" style={{ opacity: 0.3, paddingBottom: 20 }} />
                        <Text style={{ fontSize: hp('3%'), fontWeight: 'bold', color: '#000', opacity: 0.3 }}>{I18n.t('skillData.notassigned')}</Text>
                    </View>
                }
            </View>
        );
    }

    const Videos = () => {
        return (
            <View style={{ flex: 1 }}>
                {videosData.length > 0 ?
                    <FlatList
                        data={videosData}
                        renderItem={CardVideos}
                        keyExtractor={(item, index) => index}
                        style={{ flex: 1 }}
                        onRefresh={() => getViewPlayer()}
                        refreshing={refresh}
                    />
                    :
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name="film" size={hp('15%')} color="#000" style={{ opacity: 0.3 }} />
                        <Text style={{ fontSize: hp('3%'), fontWeight: 'bold', color: '#000', opacity: 0.3 }}>{I18n.t('profile.listEmpty')}</Text>
                    </View>
                }
            </View>
        );
    }

    const Favorites = () => {
        return (
            <View style={{ flex: 1 }}>
                {favoritesData.length > 0 ?
                    <FlatList
                        data={favoritesData}
                        renderItem={CardFavorite}
                        keyExtractor={(item, index) => index}
                        style={{ flex: 1 }}
                        onRefresh={() => getViewPlayer()}
                        refreshing={refresh}
                    />
                    :
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name="star" size={hp('15%')} color="#000" style={{ opacity: 0.3 }} />
                        <Text style={{ fontSize: hp('3%'), fontWeight: 'bold', color: '#000', opacity: 0.3 }}>{I18n.t('skillData.videofavorite')}</Text>
                    </View>
                }
            </View>
        );
    }


    console.log("esta es la skill", skill);
    return (
        <View style={styles.center}>
            {loader ? <Loader /> : null}
            <Image source={require('../../../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'),
                position: "absolute",
            }} />
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate('Skill 3D', { skill: skill,lenguaje, token: token, user })} style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                    <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: hp('16%'), height: hp('16%'), borderRadius: 10, }} />
                    <View style={{ position: 'absolute', width: hp('10%'), height: hp('10%'), backgroundColor: 'rgba(0,0,0,0.3)', borderRadius: 50, borderColor: '#D6D6D6', borderWidth: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name="play" size={hp('4')} color="#FFF" style={{ marginLeft: 10 }} />
                    </View>
                </TouchableOpacity>
                <View style={{ flex: 1, justifyContent: 'center', marginTop: 10 }}>
                    <Text style={{ fontSize: hp('1.4%'), color: '#646469', marginBottom: 20 }}>{I18n.t('teams.category.'+skill.category_id) }</Text>
                    <Text style={{ fontSize: hp('2%'), color: '#000' }}>{skill.name}</Text>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View  style={{ width: wp('20%'),  justifyContent: 'center', alignItems: 'center', height: hp('4%'),}}>
                            {/* <CopilotStep
                                text="Choose the player(s), the player(s) will be notified, If the coach already assigned homework and it is not completed, the coach can not assign new homework.!"
                                order={4}
                                name="SEND THE SKILL AS HOMEWORK"
                                >
                                <CopilotView style={{ width: wp('20%'), height:hp('10%') }}>
                                        
                                </CopilotView>
                            </CopilotStep> */}
                            <TouchableOpacity onPress={() => setVisible(true)} activeOpacity={0.8} style={{ width: wp('20%'), flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderWidth: 2, borderColor: 'green', height: hp('4%'), borderRadius: 20, }}>
                                <Icon name="user" size={hp('2.4%')} color="green" />
                            </TouchableOpacity>
                        </View>
                        <View  style={{ width: wp('20%'), justifyContent: 'center', alignItems: 'center', height: hp('4%'), marginRight:wp('4%')}}>
                            {/* <CopilotStep
                                text="Coach can choose the day, enter a description and accept, the player(s) will be notified on their Phone and App!"
                                order={5}
                                name="ADD TO THE TRAINING PLAN"
                                >
                                <CopilotView style={{ width: wp('20%'), height:hp('10%') }}>
                                    
                                </CopilotView>
                            </CopilotStep> */}
                            <TouchableOpacity activeOpacity={0.8} onPress={() => setVisibleEvent(true)} style={{ width: wp('20%'), marginRight: 20, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderWidth: 2, borderColor: 'green', height: hp('4%'), borderRadius: 20 }}>
                                <Icon name="calendar" size={hp('2.4%')} color="green" />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
            <View style={{ flex: 4 }}>
                <TabView
                    navigationState={{ index: index, routes }}
                    renderScene={SceneMap({
                        players: Players,
                        videos: Videos,
                        favorites: Favorites
                    })}
                    renderTabBar={(props) => <TabBar {...props}
                        indicatorStyle={{ backgroundColor: 'green', }}
                        style={{ backgroundColor: '#FFF', height:56 }}
                        renderLabel={({ route, focused, color })=>(
                            route.key == 'players' ?
                            //    <View style={{height:56,}}>
                            //         <CopilotStep
                            //             text="The player(s) that the coach have assigned the skill!"
                            //             order={1}
                            //             name="PLAYERS"
                            //         >
                            //             <CopilotView style={{height:56, width:'100%'}}>
                            //                 <Text style={{color:focused ? 'green' : '#D6D6D6', fontSize:hp('1.7%'), paddingLeft:20, paddingRight:20}}>
                            //                     PLAYERS
                            //                 </Text>
                            //             </CopilotView>
                            //         </CopilotStep>
                            //     </View>
                                    <Text style={{color:focused ? 'green' : '#D6D6D6', fontSize:hp('1.7%'), paddingLeft:20, paddingRight:20}}>
                                        {I18n.t('skillData.text2')} 
                                    </Text>
                            : route.key == 'videos' ?
                                // <View style={{height:56,}}>
                                //     <CopilotStep
                                //         text="The AR the player(s) have sent to coach!"
                                //         order={2}
                                //         name="VIDEOS"
                                //     >
                                //         <CopilotView style={{height:56, width:'100%'}}>
                                //             <Text style={{color:focused ? 'green' : '#D6D6D6', fontSize:hp('1.7%'), paddingLeft:20, paddingRight:20}}>
                                //                 VIDEOS
                                //             </Text>
                                //         </CopilotView>
                                //     </CopilotStep>
                                // </View>
                                <Text style={{color:focused ? 'green' : '#D6D6D6', fontSize:hp('1.7%'), paddingLeft:20, paddingRight:20}}>
                                    
                                    {I18n.t('skillData.text1')}
                                </Text>
                            : route.key == 'favorites' ?
                                // <View style={{height:56,}}>
                                //     <CopilotStep
                                //         text="The AR the coach liked!"
                                //         order={3}
                                //         name="FAVORITES"
                                //     >
                                //         <CopilotView style={{height:56, width:'100%'}}>
                                //             <Text style={{color:focused ? 'green' : '#D6D6D6', fontSize:hp('1.7%'), paddingLeft:20, paddingRight:20}}>
                                //                 FAVORITES
                                //             </Text>
                                //         </CopilotView>
                                //     </CopilotStep>
                                // </View>
                                <Text style={{color:focused ? 'green' : '#D6D6D6', fontSize:hp('1.7%'), paddingLeft:20, paddingRight:20}}>
                                    
                                    {I18n.t('skillData.text3')}
                                </Text>
                            : null
                        )}
                    />}

                    onIndexChange={(index) => setIndex(index)}
                    initialLayout={{ width: wp('100%') }}
                />
            </View>

            <Modal
                animationType="fade"
                transparent={true}
                visible={visible}
                onRequestClose={() => {
                    setVisible(false);
                }}
            >
                <SafeAreaView style={{ flex: 1, backgroundColor: '#F3F3F3', }}>
                    <Image source={require('../../../assets/pelota.png')} style={{
                        height: '100%',
                        width: wp('100%'),
                        position: "absolute",
                    }} />
                    <View style={{ width: hp('100%'), height: hp('5.6%'), backgroundColor: '#FFF', flexDirection: 'row', elevation: 5, borderBottomWidth: 1, borderColor: '#FFF' }}>
                        <TouchableOpacity activeOpacity={0.8} onPress={() => { cleanIsSelect() }} style={{ height: '100%', width: wp('10%'), justifyContent: 'center', alignItems: 'center' }}>
                            <Icon2 name="arrow-left" size={hp('2.4%')} />
                        </TouchableOpacity>
                        <View style={{ justifyContent: 'center', alignItems: 'center', paddingLeft: 25 }}>
                            <Text style={{ fontSize: hp('2%'), fontWeight: 'bold' }}>{I18n.t('skillData.text4')}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, }}>
                        <FlatList
                            data={listPlayer}
                            renderItem={CardListPlayer}
                            extraData={listPlayer}
                            keyExtractor={(item) => item.id}
                            style={{ flex: 1 }}
                            onRefresh={() => getPlayer()}
                            refreshing={refreshPlayer}
                        />
                    </View>
                    <Animated.View style={{ backgroundColor: '#FFF', height: 56, width: wp('100%'), justifyContent: 'center', alignItems: 'center', transform: [{ translateY: menu }] }}>
                        <TouchableOpacity activeOpacity={0.8} onPress={() => sendInvite()} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name="send" size={hp('2%')} style={{color:'green'}} />
                            <Text>{I18n.t('skillData.send')}</Text>
                        </TouchableOpacity>
                    </Animated.View>
                </SafeAreaView>
            </Modal>

            <Modal
                animationType="slide"
                transparent={true}
                visible={visibleEvent}>
                <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "null"} enabled style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-end' }}>
                    <View style={{ height: "50%", width: '100%', backgroundColor: "#F3F3F3", borderTopRightRadius: 10, borderTopLeftRadius: 10 }}>
                        {/* <View style={{ height: "40%" ,width: '100%', backgroundColor:"#F3F3F3", justifyContent:"center", borderTopRightRadius:10, borderTopLeftRadius:10}}> */}
                        <TouchableOpacity onPress={() => cleanCreateEvent()} style={{ flex: 0.1, width: '100%', height: '6%', justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name="chevron-down" size={hp('2.3%')} color="#000" />
                        </TouchableOpacity>
                        <View style={{ flex: 1, borderTopRightRadius: 10, borderTopLeftRadius: 10, }}>
                            <View style={{ flex: 2, justifyContent: 'space-evenly', alignItems: 'center' }}>
                                <Block width={wp('90')} style={{ marginBottom: 5, borderRadius: 10 }}>
                                    <DatePicker
                                        style={{ width: '100%', borderRadius: 10, borderWidth: 0, backgroundColor: '#FFF', }}
                                        date={date}
                                        mode="date"
                                        placeholder="Select date"
                                        format="YYYY-MM-DD"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        showIcon={false}
                                        onDateChange={(v) => setDate(v)}
                                        customStyles={{
                                            dateInput: {
                                                borderRadius: 5,
                                                alignItems: 'flex-start',
                                                paddingLeft: 20,
                                            },
                                            // ... You can check the source to find the other keys.
                                        }}
                                    />
                                </Block>
                                <Block width={wp('90')} style={{ marginBottom: 5 }}>
                                    <TextInput
                                        style={styles.textArea}
                                        placeholder="Description"
                                        placeholderTextColor="#E1E1E1"
                                        numberOfLines={10}
                                        multiline={true}
                                        value={description}
                                        maxLenth={255}
                                        onChangeText={(v) => setDescription(v)}
                                    />
                                </Block>
                            </View>
                            <View style={{ flex: 1, marginBottom: 30 }}>
                                <Block center>

                                    <GaButton style={styles.createButton} round size="small" color="success" onPress={() => 
                                       {
                                        if(date){
                                            createEvent();
                                        }else{
                                            alert(I18n.t('teams.alert.text1'))
                                        }
                                        }
                                        }>{I18n.t('skillData.text5')}</GaButton>
                                </Block>
                            </View>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </Modal>

            <Modal
                animationType="slide"
                transparent={true}
                visible={visibleVideo}
                onRequestClose={()=>setVisibleVideo(false)}>
                <View style={{ flex: 1, backgroundColor: '#F3F3F3' }}>
                    <View style={{width:wp('100%'), height:55, backgroundColor: '#FFF', flexDirection:'row', alignItems:'center', zIndex: 1,}}>
                        <TouchableOpacity activeOpacity={1} onPress={()=>setVisibleVideo(false)} style={{height:'100%', width:'10.5%', justifyContent:'center', alignItems:'center'}}>
                            <Icon2 name="arrow-left" size={hp('2.5%')} color="#000"/>
                        </TouchableOpacity>
                        <View style={{height:'100%', justifyContent:'center',paddingLeft:20}}>
                            <Text style={{fontSize:hp('2.1%')}}>
                                {playerVideo}
                            </Text>
                        </View>
                    </View>
                    <View style={{flex:1}}>
                        {loader ? <Loader title={"Loading video..."} /> : null}
                        <Video2 source={{ uri: urlVideo }}
                            controls={true}
                            resizeMode="cover"
                            onLoad={() => setLoader(false)}
                            style={styles.backgroundVideo} />
                    </View>
                </View>
            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    center: {
        flex: 1,
        textAlign: "center",
        backgroundColor: '#F3F3F3',
    },
    createButton: {
        width: width * 0.5,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
    },
    textArea: {
        height: hp('7%'),
        justifyContent: "flex-start",
        textAlignVertical: "top",
        borderWidth: 1,
        borderColor: "#BFBEBE",
        borderRadius: 5,
        paddingLeft: 20,
        color: "#000",
        backgroundColor: "#FFF",
        fontSize: hp('1.7%')
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
});

export default DetailsSkill;
//  copilot({
//     overlay: "svg", // or 'view'
//     animated: true // or false
//   })(DetailsSkill);