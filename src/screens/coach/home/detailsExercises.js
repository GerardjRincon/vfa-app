//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, FlatList, Modal, Dimensions, TextInput, KeyboardAvoidingView, Platform} from 'react-native'
import {url} from '../../../storage/config';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TabView, SceneMap,TabBar } from 'react-native-tab-view';
import { Block, Checkbox, Input, Button as GaButton, theme } from 'galio-framework'
const { width, height } = Dimensions.get('screen');
import DatePicker from 'react-native-datepicker'
import Snackbar from 'react-native-snackbar';
import Loader from '../global/loader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'react-native-i18n';
// create a component
const DetailsExercises = ({ navigation, route }) => {

    ///////////////// Estados globales
    const { exercise, token,lenguaje, user } = route.params;

    console.log("lenguaje", lenguaje)
    const [loader, setLoader] = useState(false)
    const [index, setIndex] = useState(0);
    const [routes, setRoutes] = useState([ // Esta es una constante de rutas
        { key: 'left', title: I18n.t('calendarCoach.text6') },
        { key: 'other', title: I18n.t('calendarCoach.text7')},
        { key: 'right', title: I18n.t('calendarCoach.text5')},
    ]);
    const [createEvent, setCreateEvent]=useState(false);
    const [date, setDate] = useState(null);
    const [description, setDescription] = useState(null);

    useEffect(() => {
    
        console.warn(user)
        getExercise()
    }, [])


    useLayoutEffect(() => {
        navigation.setOptions({
          title: I18n.t('calendarCoach.text66'),
        });
    })
    ///////////////// Fin de estado globales

    ///////////////// Estado de Views

    const [dataLeft, setDataLeft] = useState([]);
    const [dataOther, setDataOther] = useState([]);
    const [dataRight, setDataRight] = useState([]);

    ///////////////// Fin de estados de Views


    ///////////////// Constantes anexas

    const urlExercises = 'https://go.vfa.app/exercises/'+exercise.file+'.jpeg';

    ///////////////// Fin de constantes anexas


    ///////////////// Funciones de exercise

    async function getExercise(){
        setLoader(true)
        try {
            await fetch(url+'api/exercises/view',{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+token
            },
            body: JSON.stringify({
                id: exercise.file,
            }),
        }).then(res => res.json())
        .then((dat) =>{
            try {
                // console.warn(dat)
                setDataLeft(dat.data.left_foot)
                setDataOther(dat.data.other)
                setDataRight(dat.data.right_foot)
                setLoader(false)
            } catch (error) {
                // console.warn(error)
                Snackbar.show({
                    text: error,
                    duration: Snackbar.LENGTH_LONG,
                  });
                  setLoader(false)
            }
        })
            
        } catch (error) {
            // console.warn(error)
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
              });
              setLoader(false)
        }
    }

    function clean(){
        setCreateEvent(false)
    }

   async function SendEvent(){
        setLoader(true)
        if(!date){
            Snackbar.show({
                text: 'The fields are required',
                duration: Snackbar.LENGTH_LONG,
              });
            return;
        }
        try {
            await fetch(url+'api/calendar/create',{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+token
            },
            body: JSON.stringify({
                date: date,
                description:description,
                exercises_id: exercise.file,
            }),
        }).then(res => res.json())
        .then((dat) =>{
            try {
                clean()
                setLoader(false)
                setTimeout(()=>{
                    Snackbar.show({
                        text: dat.data,
                        duration: Snackbar.LENGTH_LONG,
                      });
                },500)
            } catch (error) {
                setLoader(false)
                Snackbar.show({
                    text: error,
                    duration: Snackbar.LENGTH_LONG,
                  });
            }
        })
            
        } catch (error) {
            setLoader(false)
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
              });
        }
    }

    ///////////////// Fin de Funciones de exercise


    /////// Componente de item list 

    const Data=({item, index, separators})=>{


        const imageExercises = 'https://go.vfa.app/preview/'+item.img_preview;


        return(
            <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('Skill 3D', {skill:item,token:token,lenguaje, user:user})} style={{width:wp('100%'), height:hp('10%'), flexDirection:'row', borderBottomWidth: 1, borderColor: '#D6D6D6', backgroundColor: '#fff',}}>
                <View style={{flex:1, marginLeft:20, justifyContent:'center'}}>
                    {item.img_preview?
                        <Image source={{uri:imageExercises}} resizeMode="cover" style={{width:hp('8%'),height: hp('8%'), borderRadius:10}} />
                    :
                        <View style={{backgroundColor:'#D6D6D6', width:hp('8%'),height: hp('8%'), borderRadius:50, justifyContent:'center', alignItems:'center'}}>
                            <Text style={{fontSize:hp('2%'), fontWeight:'bold', }}>{item.name.charAt(0)}</Text>
                        </View>
                    }
                </View>
                <View style={{flex:4, flexDirection:'row', justifyContent:'center', alignItems:'center',}}>
                    <View style={{flex:3}}>
                        <Text style={{fontSize:hp('1.7%'), fontWeight:'bold'}}>{I18n.t('skills.' + item.id)}</Text>
                        <Text style={{fontSize:hp('1.4%'),opacity:0.4}}>{I18n.t('teams.category.' + item.category_id)}</Text>
                    </View>
                    <View style={{width:'20%',marginRight:20, backgroundColor: 'green', borderRadius:10, bottom:10, right:5, position: 'absolute',justifyContent:'center', alignItems:'center'}}>
                        <View style={{justifyContent:'center', alignItems:'flex-end',}}>
                            <Text style={{fontSize:hp('1.5'), color:'#FFF', fontWeight:'bold',}}>{I18n.t('teams.levels.' + item.level)}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    ////// Fin de componente item list


    ///////////////// Vista de Left Foot

    const Left =()=>{
        return(
            
            <View style={{flex:1}}>

                <Text style={{marginTop:10,
                    marginBottom:10, fontWeight:'bold', marginLeft:20, fontSize:15, color:'#3d3d3d'}}>
                        
                        {I18n.t('calendarCoach.text4')}
                        </Text>

                <FlatList
                    data={dataLeft}
                    renderItem={Data}
                    keyExtractor={(item, index) => index}
                    style={{flex:1}}
                />
            </View>
        )
    }

    ///////////////// Fin de vista Left Foot

    ///////////////// Vista de Other
    
    const Other =()=>{
        return(
            <View style={{flex:1}}>
                 <Text style={{marginTop:10,
                    marginBottom:10, fontWeight:'bold', marginLeft:20, fontSize:15, color:'#3d3d3d'}}>
                        
                        {I18n.t('calendarCoach.text4')}
                        </Text>

                <FlatList
                    data={dataOther}
                    renderItem={Data}
                    keyExtractor={(item, index) => index}
                    style={{flex:1}}
                />
            </View>
        )
    }

    ///////////////// Fin de vista Other

    ///////////////// Vista de Right Foot
    
    const Right =()=>{
        return(
            <View style={{flex:1}}>
                 <Text style={{marginTop:10,
                    marginBottom:10, fontWeight:'bold', marginLeft:20, fontSize:15, color:'#3d3d3d'}}>
                        
                        {I18n.t('calendarCoach.text4')}
                        </Text>

                <FlatList
                    data={dataRight}
                    renderItem={Data}
                    keyExtractor={(item, index) => index}
                    style={{flex:1}}
                />
            </View>
        )
    }

    ///////////////// Fin de vista Right Foot
    

    return (
        <View style={styles.container}>
            {loader?<Loader/>:null}
            <Image source={require('../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    position: "absolute",
            }} />
            <View style={{flex:1, flexDirection:'row'}}>
              <TouchableOpacity activeOpacity={0.8}  onPress={()=>navigation.navigate('Exercise 3D', {exercise:exercise,token:token,lenguaje, user})} style={{flex:1,alignItems:'center', justifyContent:'center',}}>
                <Image source={{uri: urlExercises}} resizeMode="cover" style={{width:hp('16%'),height: hp('16%'), borderRadius:10, }}/>
                <View style={{position:'absolute', width:hp('10%'),height: hp('10%'), backgroundColor: 'rgba(0,0,0,0.3)', borderRadius:50, borderColor:'#D6D6D6', borderWidth:1, justifyContent:'center', alignItems:'center'}}>
                    <Icon name="play" size={hp('4')} color="#FFF" style={{marginLeft:10}}/>
                </View>
              </TouchableOpacity>
              <View style={{flex:1,justifyContent:'center',marginTop:10, marginRight:20 }}>
                <Text numberOfLines={2} style={{fontSize:hp('2%'), color:'#000', marginTop:10}}>{I18n.t('teams.exercises_categories.' + exercise.category_id)} - {I18n.t('textos.text8')} {exercise.number}</Text>
                <Text numberOfLines={2}  style={{fontSize:hp('1.6%'), color:'#646469',}}>{I18n.t('teams.exercises_description.' + exercise.id)} </Text>
                <View style={{flex:1, flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                    <TouchableOpacity activeOpacity={0.8} onPress={()=>setCreateEvent(true)}  style={{width:wp('20%'),flexDirection:'row', justifyContent:'center', alignItems:'center', borderWidth:2, borderColor:'green', height:hp('4%'), borderRadius:20}}>
                        <Icon name="calendar" size={hp('2.4%')} color="green"/>
                    </TouchableOpacity>
                    {/* <TouchableOpacity activeOpacity={0.8}  style={{width:hp('4%'), height:hp('4%'), marginRight:wp('15%'),  flexDirection:'row', justifyContent:'center', alignItems:'center', borderWidth:2, borderColor:'green',borderRadius:50, }}>
                        <Icon name="info-circle" size={hp('2.4%')} color="green"/>
                    </TouchableOpacity> */}
                </View>
              </View>
          </View>
          <View style={{flex:4,}}>
            <TabView
                navigationState={{ index:index, routes }}
                renderScene={SceneMap({
                left: Left,
                other: Other,
                right: Right
                })}
                renderTabBar={(props) =><TabBar { ... props } 
                indicatorStyle = {{  backgroundColor : 'green' }} 
                style = {{ backgroundColor : '#FFF'  }} 
                renderLabel={({ route, focused, color }) => (
                    <Text style={{color:focused?'green':'#646469', opacity:focused?1:0.5,}}>{route.title}</Text>
                )}
                /> } 
        
                onIndexChange={(index)=>setIndex(index)}
                initialLayout={{ width: wp('100%') }}
                />
          </View>

          <Modal 
            animationType="slide"
            transparent={true}
            visible={createEvent}>
                 <KeyboardAvoidingView behavior={Platform.OS === "ios"? "padding":"null"} enabled style={{ flex: 1 ,flexDirection: 'column', justifyContent: 'flex-end'}}>
                    <View style={{ height: "40%" ,width: '100%', backgroundColor:"#F3F3F3", justifyContent:"center", borderTopRightRadius:10, borderTopLeftRadius:10}}>
                    {/* <View style={{ height: "40%" ,width: '100%', backgroundColor:"#F3F3F3", justifyContent:"center", borderTopRightRadius:10, borderTopLeftRadius:10}}> */}
                     <TouchableOpacity onPress={()=>clean()} style={{flex:0.1 ,width:'100%', height:'6%', justifyContent:'center', alignItems:'center'}}>
                        <Icon name="chevron-down" size={hp('2.3%')} color="#000"/>
                     </TouchableOpacity>
                        <View style={{flex:1, borderTopRightRadius:10, borderTopLeftRadius:10,}}>
                            <View style={{flex:2,justifyContent:'space-evenly', alignItems:'center' }}>
                                <Block  width={wp('90')} style={{marginBottom: 5, borderRadius:10 }}>
                                    <DatePicker
                                        style={{width:'100%',borderRadius:10, borderWidth:0, backgroundColor: '#FFF',}}
                                        date={date}
                                        mode="date"
                                        placeholder={I18n.t('calendarCoach.text2')}
                                        format="YYYY-MM-DD"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        showIcon={false}
                                        onDateChange={(v) => setDate(v)}
                                        customStyles={{
                                            dateInput: {
                                              borderRadius:5,
                                              alignItems:'flex-start',
                                              paddingLeft:20,
                                            },
                                          }}
                                    />
                                </Block>
                                <Block width={wp('90')} style={{ marginBottom: 5 }}>
                                    <TextInput
                                        style={styles.textArea}
                                        placeholder={I18n.t('calendarCoach.text1')}
                                        placeholderTextColor="#E1E1E1"
                                        numberOfLines={10}
                                        multiline={true}
                                        value={description}
                                        maxLenth={255}
                                        onChangeText={(v) => setDescription(v)}
                                    />
                                </Block>
                            </View>
                            <View style={{flex:1, marginBottom:30}}>
                                    <Block center>
                                        <GaButton style={styles.createButton} round size="small" color="success" onPress={() => 
                                       {
                                        if(date){
                                            SendEvent();
                                        }else{
                                            alert(I18n.t('teams.alert.text1'))
                                        }
                                        }
                                        }
                                        >
                                            {I18n.t('calendarCoach.text3')}
                                            </GaButton>
                                    </Block>
                            </View>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </Modal>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
    createButton: {
        width: width * 0.5,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
    },
    textArea: {
        height: hp('7%'),
        justifyContent: "flex-start",
        textAlignVertical: "top",
        borderWidth:1,
        borderColor:"#BFBEBE",
        borderRadius:5,
        paddingLeft:20,
        color:"#000",
        fontSize:hp('1.7%'),
        backgroundColor: "#FFF",
        
      },
});

//make this component available to the app
export default DetailsExercises;
