import React, { useState, useEffect, useLayoutEffect } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Image, Modal, Platform, PermissionsAndroid } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import UnityView, { MessageHandler, UnityModule } from '@asmadsen/react-native-unity-view';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Snackbar from 'react-native-snackbar';
import Share from 'react-native-share';
import { setTutorial3D } from '../../../storage/user/dataUser';
import CameraRoll from "@react-native-community/cameraroll";
import { request, requestMultiple, PERMISSIONS } from 'react-native-permissions';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import Tuto3D from '../tutorial/3D/tuto3D';
import I18n from 'i18n-js';

const Skill3D = ({ navigation, route }) => {
  const { skill, token,lenguaje, user } = route.params;

  console.log(route.params);

  const [us, setUs] = useState({});
  const [ar, setAr] = useState(false);
  const [video, setVideo] = useState(null);
  const [visibleMenu, setVisibleMenu] = useState(false);
  const [visibleTuto, setVisibleTuto] = useState(false)
  const [data, setData] = useState({
    id: skill.id,
    token: token,
    type: 1,
    username: user.username.replace('@', ''),
    height:960,
    width:540,
    Language:lenguaje,
    CanRecord:true,
    usertype:'Coach',
  })

  // parseInt(hp('80%'))
  useLayoutEffect(() => {
    navigation.setOptions({
      title:I18n.t('skills.' + skill.id),
      headerLeft: (props) => {
        return (
          <BottonBack {...props} />
        )
      },
      headerRight: (props) => {
        return (
          <BottonAR {...props} />
        )
      }
    });
  }, [ar])

  function cambiar(type) {
    if (type) {
      


      if (Platform.OS === 'android') {
        // async function requestCameraPermission() {
        //   try {
        //     const granted = await PermissionsAndroid.request(
        //       PermissionsAndroid.PERMISSIONS.CAMERA,
        //       PermissionsAndroid.PERMISSIONS.RECORD_VIDEO, {
        //         'title': 'Permiso Camara',
        //         'message': 'Tamyda necesita acceso a la camara'
        //       }
        //     )
        //     if (granted === PermissionsAndroid.RESULTS.GRANTED) {

        setAr(true)
        UnityModule.postMessage('ReactManager', 'GetReactMessage', 'ShowAR')

        //     } else {
        //       // alert("Permiso denegado");
        //       Snackbar.show({
        //         text: 'Permiso denegado.',
        //         duration: Snackbar.LENGTH_LONG,
        //       });
        //     }
        //   } catch (err) {
        //     // alert("Error:", err);
        //     Snackbar.show({
        //       text: 'Ha ocurrido un error.',
        //       duration: Snackbar.LENGTH_LONG,
        //     });
        //   }
        // }
        // requestCameraPermission();
      } else {
        // request(PERMISSIONS.IOS.RECORD_VIDEO).then((result) => {
        //   if (result === 'granted'|'limited') {

        setAr(true)
        UnityModule.postMessage('ReactManager', 'GetReactMessage', 'ShowAR')

        //   }else{
        //     Snackbar.show({
        //       text: 'No se han concedido los permisos necesarios para acceder a la camara.',
        //       duration: Snackbar.LENGTH_LONG,
        //     });
        //   }
        // });
      }
    } else {
      setAr(false)
      UnityModule.postMessage('ReactManager', 'GetReactMessage', 'HideAR')
    }
  }

  const BottonAR = () => {
    return (
      ar ?
        <TouchableOpacity activeOpacity={0.5} onPress={() => cambiar(false)} style={{ width: wp('10%'), height: '100%', justifyContent: 'center', alignItems: 'center', paddingRight: 20 }}>
          {/* <Image source={require('../../../assets/iconos/mobile.png')} style={{width:40, height:40}} /> */}
          <Icon2 name="rotate-3d" size={hp('3%')} />
        </TouchableOpacity>
        :
        <TouchableOpacity activeOpacity={0.5} onPress={() => cambiar(true)} style={{ width: wp('10%'), height: '100%', justifyContent: 'center', alignItems: 'center', paddingRight: 20 }}>
          {/* <Image source={require('../../../assets/iconos/mobile.png')} style={{width:40, height:40}} /> */}
          <Icon2 name="cube-scan" size={hp('3%')} />
        </TouchableOpacity>
    )
  }

  const BottonBack = (props) => {
    return (
      <View style={{ width: 100, height: '100%' }}>
        <TouchableOpacity activeOpacity={0.5} onPress={() => back()} activeOpacity={0.8} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginLeft: -50 }}>
          <Icon name="arrow-left" size={hp('2.4%')} color="#646469" />
        </TouchableOpacity>
      </View>
    )
  }

  function back() {
    UnityModule.postMessage('ReactManager', 'GetReactMessage', 'HideAR')
    UnityModule.postMessage('ReactManager', 'GetReactMessage', 'Reload');
    navigation.goBack()
  }

  useEffect(async () => {

    UnityModule.postMessage('ReactManager', 'GetReactMessage', 'Clear3D');
    
    // console.warn(user)
    const user = await AsyncStorage.getItem('@user');
    // const tuto = await AsyncStorage.getItem('@tutorial3D');

    AsyncStorage.getItem('@tutorial3D')
    .then(res => {
      const tutorial = JSON.parse(res);
      if (tutorial) {
        setVisibleTuto(true)
      }
    })

   
    setUs(JSON.parse(user));
    var d = JSON.stringify(data)
    // UnityModule.postMessage('ReactManager', 'UnityMessage', d)
    setTimeout(() => {
      UnityModule.postMessage('ReactManager', 'GetReactMessage', d);
    }, 2000)
    // console.warn('pantalla alto: '+hp('100%'))
    // console.warn('pantalla ancho: '+wp('100%'))


    return () => {
      // console.warn('se desmonto')
      UnityModule.postMessage('ReactManager', 'GetReactMessage', 'Reload');
    }

  }, [])

  const onMessage = (event) => {
    var data = JSON.parse(event)
    setVisibleMenu(true)
    setVideo(data)
  }

  function ShareVideo() {
    Share.open({
      url: "file://" + video.data,
    })
  }

  function SaveVideo() {
    if (Platform.OS === 'android') {
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
            'title': 'Permiso Camara',
            'message': 'Tamyda necesita acceso a la camara'
          }
          )
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {


            CameraRoll.save("file://" + video.data)
            setVisibleMenu(false)

            setTimeout(() => {
              Snackbar.show({
                text: I18n.t('Permissions.text3'),
                duration: Snackbar.LENGTH_LONG,
              });
            }, 1)

            setTimeout(() =>{
              navigation.replace('Home')
            },5)

          } else {
            // alert("Permiso denegado");
            Snackbar.show({
              text: I18n.t('Permissions.text4'),
              duration: Snackbar.LENGTH_LONG,
            });
          }
        } catch (err) {
          // alert("Error:", err);
          Snackbar.show({
            text: I18n.t('Permissions.text5'),
            duration: Snackbar.LENGTH_LONG,
          });
        }
      }
      requestCameraPermission();
    } else {
      request(PERMISSIONS.IOS.PHOTO_LIBRARY).then((result) => {
        if (result === 'granted' | 'limited') {

          CameraRoll.save("file://" + video.data)
          setVisibleMenu(false)
          setTimeout(() => {
            Snackbar.show({
              text: I18n.t('Permissions.text3'),
              duration: Snackbar.LENGTH_LONG,
            });
          }, 1);

          setTimeout(() =>{
            navigation.replace('Home')
          },5);

        } else {
          Snackbar.show({
            text: I18n.t('Permissions.text6'),
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
    }
  }

  return (
    <View style={styles.container}>
      <UnityView
        style={{ width: '100%', height: '100%', }}
        onMessage={onMessage.bind(this)}
      />

      <Modal
        animationType="slide"
        transparent={true}
        visible={visibleMenu}>
        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-end' }}>
          <View style={{ height: "40%", width: '100%', backgroundColor: "#F3F3F3", justifyContent: "center", borderTopRightRadius: 10, borderTopLeftRadius: 10 }}>
            <TouchableOpacity onPress={() => setVisibleMenu(false)} style={{ flex: 0.1, width: '100%', height: '6%', justifyContent: 'center', alignItems: 'center' }}>
              <Icon name="chevron-down" size={hp('2.3%')} color="#000" />
            </TouchableOpacity>
            <View style={{ flex: 1, borderTopRightRadius: 10, borderTopLeftRadius: 10, }}>
              <View style={{ flex: 1, justifyContent: 'space-evenly', alignItems: 'center', }}>
                <View style={{ flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                  <TouchableOpacity activeOpacity={0.9} onPress={() => ShareVideo()} style={{ flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                    <Icon name="share" size={hp('2%')} />
                    <Text style={{ fontSize: hp('2%'), marginLeft: 10 }}>Share</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center', borderTopWidth: 1, borderColor: '#D6D6D6', borderBottomWidth: 1 }}>
                  <TouchableOpacity activeOpacity={0.9} onPress={() => SaveVideo()} style={{ flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                    <Icon2 name="download" size={hp('2%')} />
                    <Text style={{ fontSize: hp('2%'), marginLeft: 10 }}>Save</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                  <TouchableOpacity activeOpacity={0.9} onPress={() => setVisibleMenu(false)} style={{ flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                    <Icon2 name="reload" size={hp('2%')} />
                    <Text style={{ fontSize: hp('2%'), marginLeft: 10 }}>Discard</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
      </Modal>

      <Modal
        animationType="fade"
        transparent={true}
        visible={visibleTuto}>
          <Tuto3D skip={()=>{
            setVisibleTuto(false);
            setTutorial3D(false);
          }}/>
        </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F3F3F3',
  },
});

export default Skill3D;