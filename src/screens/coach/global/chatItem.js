//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, Image, FlatList, TextInput, TouchableOpacity, Platform, KeyboardAvoidingView, SafeAreaView } from 'react-native';
import { url } from '../../../storage/config';
// import { channel, pusher } from '../../../routes/DrawerCoach';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { GiftedChat } from 'react-native-gifted-chat'
import moment from 'moment';


import {signal, canal} from '../../../storage/pusher';


require('moment/locale/es.js');
// create a component
const ChatItem = (props) => {

    // variable de route
    const {player} = props.player;

    // Variables del chat
    const [token, setToken] = useState(null); // Token de sesion
    const [user, setUser] = useState({});
    const [sms, setSms] = useState([]); // Lista de mensajes
    const [message, setMessage] = useState(''); // Variable de mensaje

    // useLayoutEffect(() => {
    //     navigation.setOptions({
    //         title: player.name,
    //     });
    // }, [])


    // Funciones que cargan al montar la vista
    useEffect(async() => {
        const user = await AsyncStorage.getItem('@user');
        const token = await AsyncStorage.getItem('@token');
        const userJson = JSON.parse(user);
        const tokenJson = JSON.parse(token);
        setToken(tokenJson);
        getList(tokenJson);
        setUser(userJson);



        const canal = signal.subscribe("VFA");

        canal.bind("Notification-"+userJson.id, (data) => {
            if (data.tag === 'NewMessage') {
                setSms(sms => sms.concat(data.mensaje));
            }
        });
        return () => {
            signal.disconnect(canal)
        }
    },[])


    // Funciones del chat

    async function getList(token){
        // setLoader(true)
        try {
            await fetch(url+'api/chat/coach/list',{
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer '+token
            },
            body:JSON.stringify({
                player_id:props.player.id
            })
        }).then(res => res.json())
        .then((dat) =>{
            try {
                console.warn(dat)
                setSms(dat.data)
            } catch (error) {
                console.warn('error:'+error)
            }
        })
            
        } catch (error) {
            console.warn(error)
        }
    }

    async function Send(messages){
        try {
            await fetch(url+'api/chat/coach/new',{
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': 'Bearer '+token
                },
                body:JSON.stringify({
                    message: messages[0].text,
                    player_id:props.player.id
                })
            }).then(res => res.json())
            .then((dat) =>{
                try {
                    const list = [...sms];
                    list.push(dat.data);
                    setSms(list);
                } catch (error) {
                    console.warn(error)
                }
            })
        } catch (error) {
            console.warn(error)
        }
    }


    const renderAva=(props)=>{
        return(
            props.currentMessage.user.photo?
                <View style={{justifyContent:'center', alignItems:'center', backgroundColor: '#D6D6D6', height:38, width:38, borderRadius:50}}>
                    <Image source={{uri:props.currentMessage.user.avatar}} resizeMode="cover" style={{height:38, width:38,borderRadius:50}}/>
                </View>
            :
                <View style={{justifyContent:'center', alignItems:'center', backgroundColor: '#D6D6D6', height:38, width:38, borderRadius:50}}>
                    <Text>{props.currentMessage.user.name.charAt(0)}</Text>
                </View>
            
        )
    }

    const renderFooter=(props)=>{
        return(
            <View style={{width: '100%',height: 10}} /> 
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={{width:wp('100%'), height:55, backgroundColor: '#FFF', flexDirection:'row', alignItems:'center', zIndex: 1,}}>
                <TouchableOpacity activeOpacity={1} onPress={()=>props.close()} style={{height:'100%', width:'10.5%', justifyContent:'center', alignItems:'center'}}>
                    <Icon2 name="arrow-left" size={hp('2.5%')} color="#000"/>
                </TouchableOpacity>
                <View style={{height:'100%', justifyContent:'center',paddingLeft:20}}>
                    <Text style={{fontSize:hp('2.1%')}}>
                        {props.player.name}
                    </Text>
                </View>
            </View>
            <Image source={require('../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    position: "absolute",
            }} />
               <View style={{flex:1,}}>
                    <GiftedChat
                        messages={sms}
                        placeholder={'Write a message'}
                        inverted={false}
                        renderAvatar={renderAva}
                        renderFooter={renderFooter}
                        onLongPress={(context, message)=>console.warn(message)}
                        onSend={messages => Send(messages)}
                        user={{
                            _id: user.id,
                        }}
                    />
                </View>
            
        </SafeAreaView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
});

//make this component available to the app
export default ChatItem;
