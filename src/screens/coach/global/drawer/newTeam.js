import React, { useState, useEffect, useRef } from 'react';
import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity, Modal, TextInput, Platform, PermissionsAndroid, KeyboardAvoidingView, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import { url } from '../../../../storage/config';
// import { channel, pusher } from '../../../../routes/DrawerCoach';
import Snackbar from 'react-native-snackbar';
import Select2 from "react-select2-native";
import ImagePicker from 'react-native-image-crop-picker';
import { Block,Button as GaButton } from 'galio-framework'
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { setTeam } from '../../../../storage/user/dataUser';
import {request, requestMultiple, PERMISSIONS} from 'react-native-permissions';
import Dialog from "react-native-dialog";
import Loader from '../loader';

import {signal, canal} from '../../../../storage/pusher';
import I18n from 'i18n-js';

// Datos de select
const dataLevel = [
    {id:'Level 1', name:'Level 1'},
    {id:'Level 2', name:'Level 2'},
    {id:'Level 3', name:'Level 3'},
    {id:'Level 4', name:'Level 4'},
    {id:'Level 5', name:'Level 5'},
    {id:'Level 6', name:'Level 6'},
    {id:'Level 7', name:'Level 7'},
    {id:'Level 8', name:'Level 8'},
    {id:'Level 9', name:'Level 9'},
    {id:'Level 10', name:'Level 10'},
    {id:'Level 11', name:'Level 11'},
    {id:'Level 12', name:'Level 12'},
    {id:'Level 13', name:'Level 13'},
    {id:'Level 14', name:'Level 14'},
    {id:'Level 15', name:'Level 15'},
    {id:'Level 16', name:'Level 16'},
    {id:'Level 17', name:'Level 17'},
    {id:'Level 18', name:'Level 18'},
    {id:'Level 19', name:'Level 19'},
    {id:'Level 20', name:'Level 20'},
    {id:'Level 21', name:'Level 21'},
    {id:'Level 22', name:'Level 22'},
    {id:'Level 23', name:'Level 23'},
    {id:'Level 24', name:'Level 24'},
    {id:'Level 25', name:'Level 25'},
];

const dataCategory=[
    {id:'SPEED OF FOOTWORK - WARMING UP',name:'SPEED OF FOOTWORK - WARMING UP'},
    {id:'BALL CONTROL',name:'BALL CONTROL'},
    {id:'KICKING TECHNIQUE / PASSING',name:'KICKING TECHNIQUE / PASSING'},
    {id:'CUTBACK',name:'CUTBACK'},
    {id:'TURN AWAY',name:'TURN AWAY'},
    {id:'RECEIVE AND PROCEED',name:'RECEIVE AND PROCEED'},
    {id:'THROW IN',name:'THROW IN'},
    {id:'HEADING',name:'HEADING'},
    {id:'SLIDING TACKLE',name:'SLIDING TACKLE'},
    {id:'SKILLS TO BEAT AN OPPONENT',name:'SKILLS TO BEAT AN OPPONENT'},
    {id:'DOUBLE SKILLS TO BEAT AN OPPONENT',name:'DOUBLE SKILLS TO BEAT AN OPPONENT'},
    {id:'SKILLS TO BEAT AN OPPONENT IN YOUR BACK',name:'SKILLS TO BEAT AN OPPONENT IN YOUR BACK'},

];

const dataCourse=[
    {id:'BASIC COURSE',name:'BASIC COURSE'},
    {id:'MASTER THE BALL COURSE',name:'MASTER THE BALL COURSE'},
    {id:'MASTER THE OPPONENT COURSE',name:'MASTER THE OPPONENT COURSE'},
    {id:'BEAT THE OPPONENT COURSE',name:'BEAT THE OPPONENT COURSE'},
    {id:'PERFECTION COURSE',name:'PERFECTION COURSE'},

];


// create a component
const newTeam = ({navigation, route}) => {
    const {team} = route.params ?? {};
    const select1 = useRef();
    const [teams, setTeams] = useState([]);
    const [visible, setVisible] = useState(false);
    const [token, setToken] = useState(null);
    const [edit, setEdit] = useState(team?team:null);
    const [editabled, setEditable] = useState(team?true:false);
    const [name, setName] = useState(team?team.name:null);

    const [visibleModalImagen, setVisibleModalImagen] = useState(false);
    const [imagen, setImagen] = useState(null);
    const [imagenEdit, setImagenEdit] = useState(team?'https://go.vfa.app'+team.photo:null);

    // filtro de seleccion
    const [workingLevel] = useState([
        {id:'level',name:'By Levels', value:'level'},
        {id:'category',name:'By Category', value:'category'},
        {id:'course',name:'By Course', value:'course'}
    ]);


    // variables de trabajo
    const [Levels] = useState(dataLevel);
    const [Categories] = useState(dataCategory);
    const [Courses] = useState(dataCourse);
    const [valiWorking,setValiWorking] = useState(team?team.working:'Select the working method');
    const [valiValue, setValiValue] = useState(team?team.working:null);
    const [workingValue, setWorkingValue] = useState(null);
    const [workingValueEdit, setWorkingValueEdit] = useState(team?team.start_working:null);
    const [workingValueLabel, setWorkingValueLabel] = useState(team?team.start_working:'Choose By ');
    const [refresh, setRefresh] = useState(false);
    const [loader, setLoader] = useState(false);

    useEffect(async() => {
        const user = await AsyncStorage.getItem('@user');
        const token = await AsyncStorage.getItem('@token');
        const userJson = JSON.parse(user);
        const tokenJson = JSON.parse(token);
        setToken(tokenJson)
    }, [])


    async function selectCamera() {
        if (Platform.OS === 'android') {
          async function requestCameraPermission() {
            try {
              const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
                  'title': 'Permiso Camara',
                  'message': 'Tamyda necesita acceso a la camara'
                }
              )
              if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                ImagePicker.openCamera({
                    width: 256,
                    height: 356,
                    cropping: true,
                    includeBase64:true
                  }).then(image => {
                    var base64Icon = 'data:image/png;base64,'+image.data;
                    image.base64 = base64Icon;
                    setImagen(image);
                    setVisible(true);
                    setVisibleModalImagen(false);
                  });
    
              } else {
                // alert("Permiso denegado");
                Snackbar.show({
                  text: 'Permiso denegado.',
                  duration: Snackbar.LENGTH_LONG,
                });
              }
            } catch (err) {
              // alert("Error:", err);
              Snackbar.show({
                text: 'Ha ocurrido un error.',
                duration: Snackbar.LENGTH_LONG,
              });
            }
          }
          requestCameraPermission();
        }else{
          request(PERMISSIONS.IOS.CAMERA).then((result) => {
            if (result === 'granted'|'limited') {
                ImagePicker.openCamera({
                    width: 256,
                    height: 356,
                    cropping: true,
                    includeBase64:true
                  }).then(image => {
                    var base64Icon = 'data:image/png;base64,'+image.data;
                    image.base64 = base64Icon;
                    setImagen(image);
                    setVisible(true);
                    setVisibleModalImagen(false);
                  });
    
            }else{
              Snackbar.show({
                text: I18n.t('coach.alert.text1'),
                duration: Snackbar.LENGTH_LONG,
              });
            }
          });
        }   
    }

    async function selectGaleria(){
        if (Platform.OS === 'android') {
          async function requestCameraPermission() {
            try {
              const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, {
                  'title': I18n.t('coach.alert.text2'),
                  'message': I18n.t('coach.alert.text3'),
                }
              )
              if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                // let options = {
                //     mediaType: 'photo',
                //     quality: 0.5,
                //     includeBase64:true,
                //   };
                  
                //   launchImageLibrary(options, (response) => {
              
                //       if (response.didCancel) {
                //           setVisibleModalImagen(false)
                //         return;
                //       } else if (response.errorCode == 'camera_unavailable') {
                //           setVisibleModalImagen(false)
                //         return;
                //       } else if (response.errorCode == 'permission') {
                //           setVisibleModalImagen(false)
                //         return;
                //       } else if (response.errorCode == 'others') {
                //           setVisibleModalImagen(false)
                //         return;
                //       }
  
                //       setImagen({uri:response.assets[0].uri,data:response.assets[0].base64});
                //       setVisibleModalImagen(false)
                //     console.log(response.assets[0])
                //     });
  
                  ImagePicker.openPicker({
                    width: 256,
                    height: 356,
                    cropping: true,
                    includeBase64:true,
                    mediaType: 'photo',
                  }).then(image => {
                    var base64Icon = 'data:image/jpg;base64,'+image.data;
                    image.base64 = base64Icon;
                    setImagen(image);
                    setVisible(true);
                    setVisibleModalImagen(false);
                  });
    
              } else {
                Snackbar.show({
                  text: I18n.t('coach.alert.text4'),
                  duration: Snackbar.LENGTH_LONG,
                });
              }
            } catch (err) {
              // alert("Error:", err);
              Snackbar.show({
                text: err,
                duration: Snackbar.LENGTH_LONG,
              });
            }
          }
          requestCameraPermission();
        }else{
          request(PERMISSIONS.IOS.PHOTO_LIBRARY).then((result) => {
            console.warn(result);
            if (result === 'granted'|'limited') {
                ImagePicker.openPicker({
                    width: 256,
                    height: 356,
                    cropping: true,
                    includeBase64:true,
                    mediaType: 'photo',
                  }).then(image => {
                    var base64Icon = 'data:image/jpg;base64,'+image.data;
                    image.base64 = base64Icon;
                    setImagen(image);
                    setVisible(true);
                    setVisibleModalImagen(false);
                  });
            }else{
              Snackbar.show({
                text: I18n.t('coach.alert.text5'),
                duration: Snackbar.LENGTH_LONG,
              });
            }
          });
        }
    }

    async function createTeam(){
        try {
            if (!name) {
                Snackbar.show({
                    text: I18n.t('coach.alert.text6'),
                    duration: Snackbar.LENGTH_LONG,
                });
                return;
            }
            if (valiWorking == 'Select the working method') {
                Snackbar.show({
                    text: I18n.t('coach.alert.text8'),
                    duration: Snackbar.LENGTH_LONG,
                });
                return;
            }
            if (workingValueLabel === 'Choose By' || workingValueLabel === 'Choose By level' || workingValueLabel === 'Choose By category' || workingValueLabel === 'Choose By course') {
                Snackbar.show({
                    text: 'Choose the value of the working method',
                    duration: Snackbar.LENGTH_LONG,
                });
                return;
            }

            if (imagen) {
                const data = new FormData();
                data.append('photo', {
                    uri: imagen.path,
                    name: 'image.jpg',
                    type: 'image/jpg',
                    data: imagen.data,
                });
                data.append('name',name);
                data.append('working',valiWorking);
                data.append('start_working',workingValueLabel);
                await fetch(url+'api/team/create',{
                    method: 'POST',
                    headers: {
                      'Content-Type': 'multipart/form-data',
                      'Authorization': 'Bearer '+token
                    },
                    body:data
                    }).then(res => res.json())
                    .then((dat) =>{
                        console.warn(dat)
                        if (dat.ok) {
                            setVisible(false)
                            setName(null);
                            setImagen(null);
                            setLoader(false);
                            navigation.goBack();
                            Snackbar.show({
                                text: 'Team successfully created',
                                duration: Snackbar.LENGTH_LONG,
                            });
                        } else {
                            setLoader(false);
                            Snackbar.show({
                                text: dat.data,
                                duration: Snackbar.LENGTH_LONG,
                            });
                        }
                    });
            }else{
                await fetch(url+'api/team/create',{
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer '+token
                    },
                    body: JSON.stringify({
                        name:name,
                        working:valiWorking,
                        start_working:workingValueLabel,
                    }),
                    }).then(res => res.json())
                    .then((dat) =>{
                        console.warn(dat)
                        if (dat.ok) {
                            setVisible(false)
                            setName(null);
                            setImagen(null);
                            setLoader(false);
                            navigation.goBack();
                            Snackbar.show({
                                text: 'Team successfully created',
                                duration: Snackbar.LENGTH_LONG,
                            });
                        } else {
                            setLoader(false);
                            console.warn(dat)
                            Snackbar.show({
                                text: dat.data,
                                duration: Snackbar.LENGTH_LONG,
                            });
                        }
                    });
            }
        } catch (error) {
            setLoader(false);
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
              });
        }
    }

    async function updateTeam(){
        try {
            if (!name) {
                Snackbar.show({
                    text: 'Name is required',
                    duration: Snackbar.LENGTH_LONG,
                });
                return;
            }
            if (valiWorking == 'Select the working method') {
                Snackbar.show({
                    text: 'Working is required',
                    duration: Snackbar.LENGTH_LONG,
                });
                return;
            }
            if (workingValue == 'Choose By ' || workingValue == 'Choose By level' || workingValue == 'Choose By category' || workingValue == 'Choose By course') {
                Snackbar.show({
                    text: 'Choose the value of the working method',
                    duration: Snackbar.LENGTH_LONG,
                });
                return;
            }

            if (imagen) {
                const data = new FormData();
                data.append('photo', {
                    uri: imagen.path,
                    name: 'image.jpg',
                    type: 'image/jpg',
                    data: imagen.data,
                });
                data.append('name',name);
                data.append('working',valiWorking);
                data.append('start_working',workingValueEdit);
                await fetch(url+'api/team/update/'+edit.id,{
                    method: 'POST',
                    headers: {
                      'Content-Type': 'multipart/form-data',
                      'Authorization': 'Bearer '+token
                    },
                    body:data
                    }).then(res => res.json())
                    .then((dat) =>{
                        if (dat.ok) {
                            setVisible(false)
                            setName(null);
                            setImagen(null);
                            navigation.goBack();
                            Snackbar.show({
                                text: 'Team successfully created',
                                duration: Snackbar.LENGTH_LONG,
                            });
                        } else {
                            Snackbar.show({
                                text: dat.data,
                                duration: Snackbar.LENGTH_LONG,
                            });
                        }
                    });
            }else{
                // console.warn(JSON.stringify({
                //     name:name,
                //     working:valiWorking,
                //     start_working:workingValueEdit,
                // }))
                await fetch(url+'api/team/update/'+edit.id,{
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer '+token
                    },
                    body: JSON.stringify({
                        name:name,
                        working:valiWorking,
                        start_working:workingValueEdit,
                    }),
                    }).then(res => res.json())
                    .then((dat) =>{
                        if (dat.ok) {
                            setVisible(false)
                            setName(null);
                            setImagen(null);
                            navigation.goBack();
                            Snackbar.show({
                                text: 'Team successfully created',
                                duration: Snackbar.LENGTH_LONG,
                            });
                        } else {
                            Snackbar.show({
                                text: dat.data,
                                duration: Snackbar.LENGTH_LONG,
                            });
                        }
                    });
            }
        } catch (error) {
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
              });
        }
    }

    function changeLabel(working,value){
        var data= "Choose By "+working;
        if (value) {
            switch (working) {
                case 'level':
                    const l = [...dataLevel];
                    const vl = l.filter(v=>v.id == value);
                    data=vl[0].name;
                    break;
                
                case 'category':
                    const c = [...dataCategory];
                    const vc = c.filter(v=>v.id == value);
                    data=vc[0].name;
                    break;
    
                case 'course':
                    const cour = [...dataCourse];
                    const vcour = cour.filter(v=>v.id == value);
                    data=vcour[0].name;
                    break;
            }
        }
        setWorkingValueLabel(data)
        setWorkingValueEdit(data)
    }

    return (
        <View style={styles.container}>
            {loader?<Loader/>:null}
            <Image source={require('../../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    position: "absolute",
            }} />
            <KeyboardAvoidingView  behavior={Platform.OS === "ios"? "padding":"null"} enabled style={{ flex: 1 ,flexDirection: 'column',}}>
                <View style={{ flex:1 ,width: '100%', }}>
                    <View style={{flex:1, borderTopRightRadius:10, borderTopLeftRadius:10,}}>
                        <View style={{flex:1,justifyContent:'center', alignItems:'center',}}>
                                <TouchableOpacity activeOpacity={0.8} onPress={()=>{
                                    if (Platform.OS == 'ios') {
                                        setVisible(false);
                                        setVisibleModalImagen(true);
                                    }else{
                                        setVisibleModalImagen(true);
                                    }
                                }} style={{flex:1, width:'100%', justifyContent:'center', alignItems:'center'}}>
                                    {edit?
                                        edit.photo?
                                        <View style={{width:hp('20%'), height:hp('20%'), backgroundColor: '#FFF', borderWidth:1, borderColor:'#D6D6D6', borderRadius:10,}}>
                                            <Image source={{uri:imagenEdit}} style={{width:'100%', height:'100%', borderRadius:10}}/>
                                        </View>
                                        :
                                            imagen?
                                            <View style={{width:hp('20%'), height:hp('20%'),backgroundColor: '#FFF', borderWidth:1, borderColor:'#D6D6D6', borderRadius:10,}}>
                                                <Image source={{uri:imagen.base64}} style={{width:'100%', height:'100%', borderRadius:10}}/>
                                            </View>
                                            :
                                            <View style={{width:hp('20%'), height:hp('20%'), backgroundColor: '#FFF', borderWidth:1, borderColor:'#D6D6D6', borderRadius:10, justifyContent:'center', alignItems:'center'}}>
                                                <Icon name="image" size={hp('5%')} style={{opacity:0.5}}/>
                                                <Text style={{fontSize:hp('1.5%'), paddingHorizontal:20, opacity:0.6, textAlign:'center'}}>Select an image or capture a photo</Text>
                                            </View>
                                    :
                                    imagen?
                                        <View style={{width:hp('20%'), height:hp('20%'), backgroundColor: '#FFF', borderWidth:1, borderColor:'#D6D6D6', borderRadius:10,}}>
                                            <Image source={{uri:imagen.base64}} style={{width:'100%', height:'100%', borderRadius:10}}/>
                                        </View>
                                        :
                                        <View style={{width:hp('20%'), height:hp('20%'), backgroundColor: '#FFF', borderWidth:1, borderColor:'#D6D6D6', borderRadius:10, justifyContent:'center', alignItems:'center'}}>
                                            <Icon name="image" size={hp('5%')} style={{opacity:0.5}}/>
                                            <Text style={{fontSize:hp('1.5%'), paddingHorizontal:20, opacity:0.6, textAlign:'center'}}>Select an image or capture a photo</Text>
                                        </View>
                                    }
                                </TouchableOpacity>
                                <View style={{width:wp('90%'), paddingVertical: 10,}}>
                                    <TextInput
                                        style={styles.input}
                                        placeholder="Enter the team name"
                                        placeholderTextColor="#898989"
                                        value={name}
                                        maxLenth={255}
                                        onChangeText={(v) => setName(v)}
                                    />
                                </View>
                                <View style={{width:wp('100%'),}}>
                                    <Text style={{fontSize:hp('1.5%'),opacity:0.6, paddingLeft:30}}>
                                        {I18n.t('coach.newTeam')}
                                    </Text>
                                </View>
                                <View style={{width:wp('90%'), paddingVertical: 10,}}>
                                    
                                        <Select2
                                            ref={select1}
                                            isSelectSingle
                                            style={{ borderRadius: 5, backgroundColor: '#FFF', }}
                                            colorTheme="blue"
                                            showSearchBox={false}
                                            popupTitle={valiWorking}
                                            title={valiWorking}
                                            data={workingLevel}
                                            onSelect={(d) => {
                                                if (d[0]) {
                                                    setValiWorking(d[0])
                                                    setValiValue(d[0])
                                                    setWorkingValueLabel("Choose By "+d[0])
                                                }else{
                                                    setValiValue('')
                                                    setValiWorking('Select the working method')
                                                    setWorkingValueLabel("Choose By ")
                                                }
                                            }}
                                        />
                                    
                                </View>
                                <View style={{width:wp('90%'), paddingVertical: 10,}}>
                                    <Select2
                                        isSelectSingle
                                        showSearchBox={false}
                                        style={{ borderRadius: 5, backgroundColor: '#FFF', }}
                                        colorTheme="blue"
                                        title={workingValueLabel}
                                        data={valiValue=='level'?Levels:valiValue=='category'?Categories:valiValue=='course'?Courses:[]}
                                        onSelect={(d) => {
                                            if (d[0]) {
                                                setWorkingValue(d[0]);
                                                changeLabel(valiWorking,d[0]);
                                            }else{
                                                setWorkingValue(null);
                                                changeLabel(valiWorking,null);
                                            }
                                        }}
                                    />
                                </View>
                        </View>
                        <View style={{flex:0.8, justifyContent:'center', alignContent:'center', }}>
                                <Block center>
                                    <GaButton style={styles.createButton} round size="small" color="success" onPress={()=>{if(!editabled){createTeam()}else{updateTeam()}}}>{I18n.t('coach.buttom')}</GaButton>
                                </Block>
                        </View>
                    </View>
                </View>
            </KeyboardAvoidingView>


            {/* <Modal
            animationType="fade"
            transparent={true}
            onRequestClose={()=>{
                if (Platform.OS == 'ios') {
                    setVisibleModalImagen(false)
                    setVisible(true);
                }else{
                    setVisibleModalImagen(false);
                }
            }}
            visible={visibleModalImagen}>
                <SafeAreaView style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                    <View style={{justifyContent:'space-between', backgroundColor: '#E2E2E2', borderRadius:10, height:hp('20%'), width:wp('90%')}}>
                            <View style={{ justifyContent:'center',alignItems:'center', paddingTop:10,}}>
                                <Text style={{fontSize:hp('1.7'), fontWeight:'bold', textTransform:'capitalize', opacity:0.8}}>Team image</Text>
                            </View>
                            <View style={{justifyContent:'center',alignItems:'center',}}>
                                <Text style={{fontSize:hp('1.7'), opacity:0.7}}>Select an image or capture a photo</Text>
                            </View>
                            <View style={{width:'100%', height:'30%',flexDirection: 'row', borderTopWidth:1, borderColor:'#D6D6D6'}}>
                                <TouchableOpacity activeOpacity={0.8} onPress={()=>{
                                    if (Platform.OS == 'ios') {
                                        setVisibleModalImagen(false)
                                        setVisible(true);
                                    }else{
                                        setVisibleModalImagen(false);
                                    }
                                }} style={{width:'33.3%', height:'80%', justifyContent:'center', alignItems:'center', borderRightWidth:1, borderColor:'#D6D6D6'}}>
                                        <Text style={{fontSize:hp('1.7%'), color:'red'}}>Cancel</Text>
                                </TouchableOpacity>
                                <TouchableOpacity activeOpacity={0.8} onPress={()=>{selectCamera()}} style={{width:'33.3%', height:'80%', justifyContent:'center', alignItems:'center', borderRightWidth:1, borderColor:'#D6D6D6'}}>
                                    <Text style={{fontSize:hp('1.7%')}}>Camera</Text>
                                </TouchableOpacity>
                                <TouchableOpacity activeOpacity={0.8} onPress={()=>{selectGaleria()}} style={{width:'33.3%',height:'80%', justifyContent:'center', alignItems:'center'}}>
                                    <Text style={{fontSize:hp('1.7%'),}}>Gallery</Text>
                                </TouchableOpacity>
                            </View>
                    </View>
                </SafeAreaView>
                
            </Modal> */}

            <Dialog.Container visible={visibleModalImagen} onRequestClose={()=>{
                    setVisibleModalImagen(false);
                }} onPress={()=>{
                    setVisibleModalImagen(false);
                }}>
                <Dialog.Title>Team image</Dialog.Title>
                    <Dialog.Description>
                        {I18n.t('coach.selectImg')}
                    </Dialog.Description>
                    <Dialog.Button label="Cancel" style={{color:'red'}} onPress={()=>{
                        setVisibleModalImagen(false);
                    }} />
                <Dialog.Button label="Camera" onPress={()=>{selectCamera()}} />
                <Dialog.Button label="Gallery" onPress={()=>{selectGaleria()}} />
            </Dialog.Container>


        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
    input: {
        height: 45,
        borderWidth:1,
        borderColor:"#BFBEBE",
        borderRadius:5,
        paddingLeft:20,
        color:"#000",
        backgroundColor: "#FFF",
        
    },
});

//make this component available to the app
export default newTeam;
