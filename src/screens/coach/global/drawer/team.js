//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Modal, TextInput, Image, Dimensions, SafeAreaView, KeyboardAvoidingView, Platform } from 'react-native';
import { Block,Button as GaButton } from 'galio-framework'
import { LogoutUser } from "../../../../storage/user/dataUser";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { url } from '../../../../storage/config';
import Icon from 'react-native-vector-icons/FontAwesome';
import Select2 from "react-select2-native";
import Snackbar from 'react-native-snackbar';
const { width, height } = Dimensions.get('screen');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'i18n-js';

const dataLevel = [
    {id:1, name:'Level 1'},
    {id:2, name:'Level 2'},
    {id:3, name:'Level 3'},
    {id:4, name:'Level 4'},
    {id:5, name:'Level 5'},
    {id:6, name:'Level 6'},
    {id:7, name:'Level 7'},
    {id:8, name:'Level 8'},
    {id:9, name:'Level 9'},
    {id:10, name:'Level 10'},
    {id:11, name:'Level 11'},
    {id:12, name:'Level 12'},
    {id:13, name:'Level 13'},
    {id:14, name:'Level 14'},
    {id:15, name:'Level 15'},
    {id:16, name:'Level 16'},
    {id:17, name:'Level 17'},
    {id:18, name:'Level 18'},
    {id:19, name:'Level 19'},
    {id:20, name:'Level 20'},
    {id:21, name:'Level 21'},
    {id:22, name:'Level 22'},
    {id:23, name:'Level 23'},
    {id:24, name:'Level 24'},
    {id:25, name:'Level 25'},
];

const dataCategory=[
    {id:1,name:'SPEED OF FOOTWORK - WARMING UP'},
    {id:2,name:'BALL CONTROL'},
    {id:3,name:'KICKING TECHNIQUE / PASSING'},
    {id:4,name:'CUTBACK'},
    {id:5,name:'TURN AWAY'},
    {id:6,name:'RECEIVE AND PROCEED'},
    {id:7,name:'THROW IN'},
    {id:8,name:'HEADING'},
    {id:9,name:'SLIDING TACKLE'},
    {id:10,name:'SKILLS TO BEAT AN OPPONENT'},
    {id:11,name:'DOUBLE SKILLS TO BEAT AN OPPONENT'},
    {id:12,name:'SKILLS TO BEAT AN OPPONENT IN YOUR BACK'},

]

const dataCourse=[
    {id:1,name:'BASIC COURSE'},
    {id:2,name:'MASTER THE BALL COURSE'},
    {id:3,name:'MASTER THE OPPONENT COURSE'},
    {id:4,name:'BEAT THE OPPONENT COURSE'},
    {id:5,name:'PERFECTION COURSE'},

]
// create a component
const Team = ({ navigation, route }) => {
    /////// Estados globales 
    const {team} = route.params;
    const [visible, setVisible] = useState(false);
    const [data, setData] = useState([]);
    const [token, setToken] = useState(null);
    const [email, setEmail] = useState('');
    const [working, setWorking] = useState('');
    const [refresh, setRefresh] = useState(false);
    const [user, setUser] = useState({})
    const [workingLevel] = useState([
        {id:'level',name:'Level'},
        {id:'category',name:'Category'},
        {id:'course',name:'Course'}
    ]);
    const [Levels] = useState(dataLevel);
    const [Categories] = useState(dataCategory);
    const [Courses] = useState(dataCourse);

    const [valiWorking,setValiWorking] = useState('');
    const [workingValue, setWorkingValue] = useState('');



    // const [team, setTeam] = useState({name:'My Teams'});


    useEffect(async() => {
        const token = await AsyncStorage.getItem('@token');
        const user = await AsyncStorage.getItem('@user');
        const userJson = JSON.parse(user);
        const tokenJson = JSON.parse(token);
        if (tokenJson) {
            setToken(tokenJson)
            setUser(userJson)
            getMyTeam(tokenJson,team.id)

        }
    }, [])

    useLayoutEffect(() => {
        navigation.setOptions({
            title: team.name,
          });
    }, [])


    ////// Funciones de my team

    async function getMyTeam(t,id){
        setRefresh(true)
        try {
            await fetch(url+'api/team/player/all',{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+t
            },
            body: JSON.stringify({
                team_id: id,
            }),
        }).then(res => res.json())
        .then((dat) =>{
            try {
                console.warn(dat.data)
                setRefresh(false)
                setData(dat.data)
                // if (dat.message == 'Unauthenticated.') {
                //     LogoutUser()
                //     setTimeout(()=>{
                //         navigation.replace('Login')
                //     },1000)
                // }
            } catch (error) {
                setRefresh(false)
                Snackbar.show({
                    text: error,
                    duration: Snackbar.LENGTH_LONG,
                });
            }
        })
            
        } catch (error) {
            setRefresh(false)
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
            });
        }
    }

    function clean(){
        setVisible(false)
        setEmail('');
    }

    function Message(message){
        // console.warn(message)
        setTimeout(()=>{
            Snackbar.show({
                text: message,
                duration: Snackbar.LENGTH_LONG,
            });
        },1000);
        setVisible(false)
    }

    async function SendInvitation(){
        if(!email){
            Snackbar.show({
                text: I18n.t('coach.alert.text25'),
                duration: Snackbar.LENGTH_LONG,
              });
            return;
        }
        try {
            await fetch(url+'api/team/new/player',{
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
                },
                body: JSON.stringify({
                    email: email,
                    name_method: valiWorking,
                    value_method:workingValue
                }),
            }).then(res => res.json())
            .then((dat) =>{
                try {
                    Message(dat.data)
                    // if (dat.message == 'Unauthenticated.') {
                    //     LogoutUser()
                    //     Snackbar.show({
                    //         text: dat.message,
                    //         duration: Snackbar.LENGTH_LONG,
                    //     });
                    //     setTimeout(()=>{
                    //         navigation.replace('Login')
                    //     },1000)
                    // }
                } catch (error) {
                    Snackbar.show({
                        text: error,
                        duration: Snackbar.LENGTH_LONG,
                    });
                }
            })
        } catch (error) {
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
            });
        }
    }

    ////// Fin de funciones My team

    const RenderItem=({item, index, separators})=>{
        const imageMyTeam = 'https://go.vfa.app'+item.photo;
        return(
            <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('Profile Player', {player:item,token:token, team:team})} style={{width:wp('100%'), height:hp('10%'), flexDirection:'row',backgroundColor: "#FFF",borderBottomWidth:1, borderColor:'#D6D6D6' }}>
                <View style={{flex:1, marginLeft:20, justifyContent:'center',}}>
                    {item.photo?
                        <Image source={{uri:imageMyTeam}} resizeMode="cover" style={{width:hp('8%'),height: hp('8%'), borderRadius:10}} />
                    :
                        <View style={{backgroundColor:'#D6D6D6', width:hp('8%'),height: hp('8%'), borderRadius:50, justifyContent:'center', alignItems:'center'}}>
                            <Text style={{fontSize:hp('2%'), fontWeight:'bold', }}>{item.name.charAt(0)}</Text>
                        </View>
                    }
                </View>
                <View style={{flex:4, flexDirection:'row',  justifyContent:'center', alignItems:'center',}}>
                    <View style={{flex:3}}>
                        <Text style={{fontSize:hp('1.7%'), fontWeight:'bold', textTransform:'capitalize'}}>{item.name}</Text>
                        <Text style={{fontSize:hp('1.4%'),opacity:0.4, textTransform:'capitalize'}}>{item.name_method == 'level'?item.item_name:item.name_method+':'} {item.name_method !== 'level'?item.item_name:null}</Text>
                    </View>
                    <View style={{justifyContent:'center', alignItems:'center', flexDirection:'row', marginRight:20, width:hp('5%'),height:hp('5%'), borderRadius:50,}}>
                        <Icon name="trophy" size={hp('1.5%')} color="#eab900" />
                        <Text style={{fontSize:hp('1.5'), marginLeft:10, fontWeight:'bold',color:"#FFF"}}>{item.media?item.media:"0"}</Text>
                    </View>
                    <View style={{marginRight: 20,}}>
                        <Icon name="chevron-right" size={hp('1.7%')} />
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    return (
        <View style={styles.container}>
            <Image source={require('../../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    position: "absolute",
            }} />
            <FlatList
                data={data}
                renderItem={RenderItem}
                keyExtractor={(item, index) => index}
                style={{ flex: 1 }}
                onRefresh={() => getMyTeam(token,user.team_id)}
                refreshing={refresh}
            />
            <TouchableOpacity activeOpacity={0.8} onPress={()=>setVisible(true)} style={{position:'absolute', bottom:30, right:20, width:60, height:60, backgroundColor: 'green', borderRadius:50, elevation:3, justifyContent: 'center', alignItems:'center'}}>
                <Icon name="plus" size={hp('1.7%')} color="white"/>
            </TouchableOpacity>

            <Modal 
            animationType="slide"
            transparent={true}
            visible={visible}>
                 <SafeAreaView style={{ flex: 1 ,flexDirection: 'column', justifyContent: 'flex-end'}}>
                 <KeyboardAvoidingView  behavior={Platform.OS === "ios"? "padding":"null"} enabled style={{ flex: 1 ,flexDirection: 'column', justifyContent: 'flex-end'}}>
                    <View style={{ height: hp('80%') ,width: '100%', backgroundColor:"#FFF", justifyContent:"center", borderTopRightRadius:10, borderTopLeftRadius:10}}>
                     <TouchableOpacity onPress={()=>clean()} style={{flex:0.1 ,width:'100%', height:'6%', justifyContent:'center', alignItems:'center'}}>
                        <Icon name="chevron-down" size={hp('2.3%')} color="#000"/>
                     </TouchableOpacity>
                        <View style={{flex:1, borderTopRightRadius:10, borderTopLeftRadius:10, justifyContent:'center', alignItems:'center'}}>
                            <View style={{width:wp('100%'), justifyContent:'center', alignItems:'center'}}>
                                <Text style={{fontSize:hp('1.7%'),opacity:0.5, color:'#000'}}>Invite a new player to join</Text>
                            </View>
                            <View style={{flex:2,justifyContent:'center', alignItems:'center',}}>
                                    <View style={{width:wp('90%'), paddingVertical: 20,}}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Email"
                                            placeholderTextColor="#898989"
                                            numberOfLines={10}
                                            multiline={true}
                                            value={email}
                                            maxLenth={255}
                                            onChangeText={(v) => setEmail(v)}
                                        />
                                    </View>
                                    <View style={{width:wp('90%'), paddingVertical: 20,}}>
                                        <Select2
                                            isSelectSingle
                                            style={{ borderRadius: 5, backgroundColor: '#FFF', }}
                                            colorTheme="blue"
                                            showSearchBox={false}
                                            popupTitle="Select the working method"
                                            title="Select the working method"
                                            data={workingLevel}
                                            onSelect={(d) => {
                                                setValiWorking(d[0]);
                                            }}
                                        />
                                    </View>
                                    <View style={{width:wp('90%'), paddingVertical: 20,}}>
                                        <Select2
                                            isSelectSingle
                                            showSearchBox={false}
                                            style={{ borderRadius: 5, backgroundColor: '#FFF', }}
                                            colorTheme="blue"
                                            popupTitle={"Choose By "+valiWorking}
                                            title={"Choose By "+valiWorking}
                                            data={valiWorking=='level'?Levels:valiWorking=='category'?Categories:valiWorking=='course'?Courses:[]}
                                            onSelect={(d) => {
                                                setWorkingValue(d[0]);
                                            }}
                                        />
                                    </View>
                            </View>
                            <View style={{flex:0.8, justifyContent:'center', alignContent:'center', }}>
                                    <Block center>
                                        <GaButton style={styles.createButton} round size="small" color="success" onPress={()=>SendInvitation()}>{I18n.t('coach.sendButton')}</GaButton>
                                    </Block>
                            </View>
                        </View>
                    </View>
                 </KeyboardAvoidingView>
                </SafeAreaView>
            </Modal>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
    input: {
        height: 45,
        borderWidth:1,
        borderColor:"#BFBEBE",
        borderRadius:5,
        paddingLeft:20,
        color:"#000",
        backgroundColor: "#FFF",
        
    },
    createButton: {
        width: width * 0.5,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
    },
});

//make this component available to the app
export default Team;
