import React, { useState, useEffect, useLayoutEffect, useRef } from 'react';
import { View,Alert, Text, StyleSheet, TouchableOpacity, Image, Modal, Button, FlatList, Platform, PermissionsAndroid, TextInput, KeyboardAvoidingView, Animated, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Snackbar from 'react-native-snackbar';
import { LogoutUser, setTeam, setUser } from '../../../../storage/user/dataUser';
import { url } from '../../../../storage/config';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import * as Progress from 'react-native-progress';
import Dialog from "react-native-dialog";
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {request, requestMultiple, PERMISSIONS} from 'react-native-permissions';
import { Block, Checkbox, Input, Button as GaButton, theme } from 'galio-framework'
import DatePicker from 'react-native-datepicker'
import ImagePicker from 'react-native-image-crop-picker';
import md5 from 'md5';
// import { channel, pusher } from '../../../../routes/DrawerCoach';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Loader from '../loader';
import LinearGradient from 'react-native-linear-gradient';

import {signal, canal} from '../../../../storage/pusher';
import I18n from 'i18n-js';


// create a component
const ProfileCoach = ({ navigation, route }) => {
    // constantes principales
    const [loader, setLoader] = useState(false);
    const [token, setToken] = useState(null);
    const [user, setU] = useState({});
    const [name, setName] = useState(null);
    const [email, setEmail] = useState(user.email);
    const [username, setUsername] = useState(null);
    const [passwork, setPasswork] = useState(null);
    const [visible, setVisible] = useState(false);
    const img = useRef(new Animated.Value(0)).current;
    const containerImg = useRef(new Animated.Value(0)).current;
    const [modalConfirmation, setModalConfirmation] = useState(false);


    // constantes actualizacion de imagen de perfil
    const [visibleModalImagen, setVisibleModalImagen] = useState(false);



    useEffect(async() => {
        const user = await AsyncStorage.getItem('@user');
        const token = await AsyncStorage.getItem('@token');
        const userJson = JSON.parse(user);
        const tokenJson = JSON.parse(token);
        setU(userJson);
        setName(userJson.name);
        setEmail(userJson.email);
        setUsername(userJson.username);
        setToken(tokenJson);
        Animated.spring(containerImg,{
            toValue:100,
            // useNativeDriver:true,
          }).start()
        Animated.spring(img,{
            toValue:100,
            // useNativeDriver:true,
        }).start()
        const unsub = navigation.addListener('focus', async() => {
            Animated.spring(containerImg,{
                toValue:100,
                // useNativeDriver:true,
              }).start()
            Animated.spring(img,{
                toValue:100,
                // useNativeDriver:true,
            }).start() 
        });
        return ()=>{
            unsub;
        }
    }, [navigation])

    async function UpdatePhoto(img){
        setLoader(true);
        try {
            const data = new FormData();
            data.append('photo', {
                uri: img.path,
                name: 'image.jpg',
                type: 'image/jpg',
                data: img.data,
            });
            await fetch(url+'api/update/profile/player',{
                method: 'POST',
                headers: {
                  'Content-Type': 'multipart/form-data',
                  'Authorization': 'Bearer '+token
                },
                body:data
                }).then(res => res.json())
                .then((dat) =>{
                    console.warn(dat)
                    if (dat.ok) {
                        Snackbar.show({
                            text: I18n.t('coach.alert.text9'),
                            duration: Snackbar.LENGTH_LONG,
                        });
                        setUser(dat.data);
                        setU(dat.data);
                        setLoader(false);
                    } else {
                        Snackbar.show({
                            text: I18n.t('coach.alert.text10'),
                            duration: Snackbar.LENGTH_LONG,
                        });
                        setLoader(false);
                    }
                });
            
        } catch (error) {
            Snackbar.show({
                text: I18n.t('coach.alert.text11'),
                duration: Snackbar.LENGTH_LONG,
            });
            setLoader(false);
        }
    }

    async function selectCamera() {
        if (Platform.OS === 'android') {
          async function requestCameraPermission() {
            try {
              const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
                  'title': 'Permiso Camara',
                  'message': 'Tamyda necesita acceso a la camara'
                }
              )
              if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                
                ImagePicker.openCamera({
                  width: 256,
                  height: 356,
                  cropping: true,
                  includeBase64:true
                }).then(image => {
                    setVisible(false);
                  UpdatePhoto(image);
                });
    
              } else {
                // alert("Permiso denegado");
                Snackbar.show({
                  text: I18n.t('coach.alert.text12'),
                  duration: Snackbar.LENGTH_LONG,
                });
              }
            } catch (err) {
              alert("Error:", err);
              console.warn(err)
              Snackbar.show({
                text: I18n.t('coach.alert.text13'),
                duration: Snackbar.LENGTH_LONG,
              });
            }
          }
          requestCameraPermission();
        }else{
          request(PERMISSIONS.IOS.CAMERA).then((result) => {
            if (result === 'granted'|'limited') {
              ImagePicker.openCamera({
                width: 256,
                height: 356,
                cropping: true,
                includeBase64:true
              }).then(image => {
                setVisible(false);
                UpdatePhoto(image);
              });
    
            }else{
              Snackbar.show({
                text: I18n.t('coach.alert.text14'),
                duration: Snackbar.LENGTH_LONG,
              });
            }
          });
        }   
    }

    async function selectGaleria(){
        if (Platform.OS === 'android') {
          async function requestCameraPermission() {
            try {
              const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, {
                  'title': I18n.t('coach.alert.text15'),
                  'message': I18n.t('coach.alert.text16'),
                }
              )
              if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                ImagePicker.openPicker({
                  width: 256,
                  height: 356,
                  cropping: true,
                  includeBase64:true
                }).then(image => {
                  setVisible(false)
                  UpdatePhoto(image);
                });
    
              } else {
                Snackbar.show({
                  text: 'Permiso denegado.',
                  duration: Snackbar.LENGTH_LONG,
                });
              }
            } catch (err) {
              // alert("Error:", err);
              Snackbar.show({
                text: err,
                duration: Snackbar.LENGTH_LONG,
              });
            }
          }
          requestCameraPermission();
        }else{
          request(PERMISSIONS.IOS.PHOTO_LIBRARY).then((result) => {
            console.warn(result);
            if (result === 'granted'|'limited') {
              ImagePicker.openPicker({
                width: 256,
                height: 356,
                cropping: true,
                includeBase64:true
              }).then(image => {
                setVisible(false)
                UpdatePhoto(image);
              });
            }else{
              Snackbar.show({
                text: 'No se han otorgado los permisos necesarios para acceder a tu almacenamiento.',
                duration: Snackbar.LENGTH_LONG,
              });
            }
          });
        }
    }


     // Estilos del boton coppa register 
     function AlertTwo(){
      Alert.alert(
          I18n.t('coach.alert.text17'),
          I18n.t('coach.alert.text18'),
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "OK", onPress: () => DeleteCuenta() }
          ]
        )
     
  }

  async function DeleteCuenta(){

    let apiurl = url+'api/account/delete/'+ user.id;
    const datos = {
        method: 'DELETE',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+token
        }
      }
     try{
        await fetch(apiurl, datos)
        .then(response => response.json())
        .then((res) => {
            setLoader(true);
            console.log(res);
            const keys = ['@user', '@token', '@skills', '@reviews', '@team', '@coach']
             AsyncStorage.multiRemove(keys)
            navigation.replace('Login');
            logoutBackend();

        })
        .catch(e => {
            console.log("Error en la consulta")
        })

     }
     catch(e){
       console.log("Error")
     }

}


async function logoutBackend(){
await fetch(url + 'api/logout', {
    method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    },
}).then(res => res.json())
.then(async(dat) => {
    console.warn(dat)
    try {
        if (dat.ok) {
            const keys = ['@user', '@token', '@skills', '@reviews', '@team', '@coach']
            await AsyncStorage.multiRemove(keys)
            navigation.replace('Login');
        }
    } catch (error) {
        Snackbar.show({
            text: I18n.t('coach.alert.text19'),
            duration: Snackbar.LENGTH_LONG,
        });
    }
})
}






    async function UpdateInfo(){
        setLoader(true);
        try {
            const data = new FormData();
            if (name) {
                data.append('name', name);
            }
            if (passwork) {
                data.append('password_app', md5(passwork));
            }

            if (name || passwork) {
                await fetch(url+'api/update/profile/coach',{
                    method: 'POST',
                    headers: {
                      'Content-Type': 'multipart/form-data',
                      'Authorization': 'Bearer '+token
                    },
                    body:data
                    }).then(res => res.json())
                    .then((dat) =>{
                        console.warn(dat)
                        if (dat.ok) {

                            alert(I18n.t('coach.alert.text20'));
                            Snackbar.show({
                                text: I18n.t('coach.alert.text21'),
                                duration: Snackbar.LENGTH_LONG,
                            });
                            setUser(dat.data);
                            setU(dat.data);
                            setLoader(false);
                        } else {
                            Snackbar.show({
                                text: I18n.t('coach.alert.text22'),
                                duration: Snackbar.LENGTH_LONG,
                            });
                            setLoader(false);
                        }
                    });
            }else{
                Snackbar.show({
                    text: I18n.t('coach.alert.text23'),
                    duration: Snackbar.LENGTH_LONG,
                });
            }

            
        } catch (error) {
            Snackbar.show({
                text: I18n.t('coach.alert.text24'),
                duration: Snackbar.LENGTH_LONG,
            });
            setLoader(false);
        }
    }

    const imagePlayer = 'https://go.vfa.app'+user.photo;
    return (
        <View style={styles.container}>



<Modal
        animationType="slide"
        transparent={true}
        visible={modalConfirmation}
        
      >


        <View style={styles.centeredView }>

          <View style={[styles.modalView]}>

               <View>
               <Text style={[styles.modalTextTiTle, {textTransform:'uppercase'}]}>
                {I18n.t('coach.deleteMessage')}
                </Text>

                <Text style={[styles.subTitle, {marginBottom:10}]}>
                 {I18n.t('coach.messageInfodelete')}
                </Text>
               
              </View>
           


              <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
              <GaButton style={[styles.createButtonAlertCancel,  {backgroundColor:'#eee', color:'#3d3d3d'}]}  onPress={() => {
                                          setModalConfirmation(false)
                                }}>
                            <Text style={{fontWeight:'bold', color:'#3d3d3d'}}>
                            {I18n.t('coach.buttonCancel')}
                            
                            </Text>
                                   
                </GaButton>


                <GaButton style={[styles.createButtonAlert, {backgroundColor: 'red',color:'#fff',} ]} color="success" onPress={() => {
                                        AlertTwo();
                                }}>
                            <Text style={{fontWeight:'bold', color:'#fff'}}>
                            {I18n.t('coach.buttonNext')}
                            </Text>
                                   
                </GaButton>

                </View>


          </View>
        </View>
      </Modal>




            {loader?<Loader/>:null}
            <Image source={require('../../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    opacity:0.3,
                    position: "absolute",
            }} />
            <ScrollView style={{flex:1}}>

               <View style={{padding:30}}>
                 <Text style={{fontWeight:'bold', fontSize:18, color:'#3d3d3d'}}>
                 </Text>
               </View>
                <Animated.View style={{height:160, width:wp('100%'),flexDirection:'row', justifyContent:'center', transform:[{scale:containerImg.interpolate({inputRange: [-100, 0],outputRange: [-1, 0]})}]}}>
                        {/* <LinearGradient colors={['#98DE4A', '#30EC16', '#00D665']} style={{height:280, width:wp('100%'), elevation:5, backgroundColor: '#8860B1',borderBottomRightRadius:hp('30%'), borderBottomLeftRadius:hp('30%'),}}> */}
                            <Animated.View style={{justifyContent:'flex-start', alignItems:'center', marginTop:hp('-4%'), transform:[{scale:img.interpolate({inputRange: [-100, 0],outputRange: [-1, 0]})}]}}>
                                <TouchableOpacity activeOpacity={0.8} onPress={()=>setVisible(true)} style={{width:hp('4%'), height:hp('4%'), position:'absolute', top:0, right:0, borderWidth:1, zIndex: 100, borderColor:'#D6D6D6', backgroundColor: "#FFF", borderRadius:hp('3%'), justifyContent:'center', alignItems:'center'}}>
                                    <Icon2 name="update" size={hp('2.5%')} style={{opacity:0.5}}/>
                                </TouchableOpacity>
                                {user.photo?
                                    <Image source={{uri:imagePlayer}} resizeMode="cover" style={{width:hp('14%'),height:hp('14%'), borderRadius:hp('15%')}} />
                                :
                                    <View style={{backgroundColor:'#f3f3f3', width:hp('14%'),height: hp('14%'), borderRadius:hp('15%'), justifyContent:'center', alignItems:'center', elevation:3}}>
                                        <Text style={{fontSize:hp('5%'), fontWeight:'bold', }}>{((user.name || "").charAt(0) || "").toUpperCase()}</Text>
                                    </View>
                                }
                                <Text style={{fontSize:hp('1.7%'), marginTop:10, fontWeight:'bold'}}>{user.username}</Text>
                            </Animated.View>
                        {/* </LinearGradient> */}
            </Animated.View>
            <View style={{flex:1, width:wp('100%'), paddingHorizontal:20, }}>
                    <KeyboardAvoidingView
                        behavior={Platform.OS === "ios" ? "padding" : null}
                        style={{ flex: 1 }}
                    >
                        <View style={styles.contentInputs}>
                            <Input
                                left
                                icon="user"
                                family="font-awesome"
                                style={styles.inputs}
                                iconSize={hp('1.4%')}
                                placeholder={I18n.t('coach.formulario.name')}
                                value={name}
                                onChangeText={(e) => setName(e)}
                                placeholderTextColor="#818181"

                            />
                            <Input
                                left
                                icon="lock"
                                family="font-awesome"
                                style={styles.inputs}
                                iconSize={hp('1.4%')}
                                placeholder={I18n.t('coach.formulario.password')}
                                value={passwork}
                                onChangeText={(e) => setPasswork(e)}
                                placeholderTextColor="#818181"
                                password
                                viewPass

                            />
                            <Block center style={{ marginTop: 20 }}>
                                <Block center>
                                    <GaButton style={{borderRadius:5, backgroundColor:'#11A10F', width:wp('90%')}} round size="small" color="success" onPress={() => {
                                            UpdateInfo()
                                    }}>
                                      <Text style={{fontWeight:'bold', textTransform:'uppercase', color:'white'}}> {I18n.t('coach.formulario.button')} </Text>
                                      </GaButton>
                                </Block>
                            </Block>
                        </View>
                    </KeyboardAvoidingView>
            </View>
              

              <View style={{borderBottomWidth:2, borderBottomColor:'#f3f3f3', width:wp('100%'), justifyContent:'center', alignItems:'center'}}></View>
         


               <View style={{padding:wp('6%')}}>
                <Text style={styles.title}>Account control </Text>
                  <View style={{flexDirection:'row',  marginTop:hp('4%')}}>
                       <Icon name="trash" style={styles.iconos}></Icon>
                       <Text style={styles.fuente}>{I18n.t('coach.deleteMessage')}</Text>
                  </View>

                  <View>
                      <Text style={styles.textSmall}>
                      {I18n.t('coach.text1')}
                      </Text>
                  </View>

                  <View style={{ marginTop:20}}>
                     {/* <Button  style={styles.buttonCreate}
                         onPress={() => { 
                             setModalConfirmation(true);
                         }}
                         >
                        <Text style={styles.textButton}>
                        Delete account</Text>  
                         </Button> */}

<GaButton style={{borderRadius:5, backgroundColor:'gray', width:wp('30%')}} round size="small" color="success" onPress={() => {
                                           setModalConfirmation(true);
                                    }}>
                                      <Text style={{fontWeight:'bold', textTransform:'uppercase', color:'white'}}>  {I18n.t('coach.text2')} </Text>
                                      </GaButton>


                  </View>
                
              </View>




            </ScrollView>
            <Dialog.Container visible={visible} onRequestClose={()=>setVisible(false)} onPress={()=>setVisible(false)}>
                <Dialog.Title>Profile image</Dialog.Title>
                    <Dialog.Description>
                       {I18n.t('coach.selectImg')}
                    </Dialog.Description>
                    <Dialog.Button label="Cancel" style={{color:'red'}} onPress={()=>{setVisible(false)}} />
                <Dialog.Button label="Camera" onPress={()=>{selectCamera()}} />
                <Dialog.Button label="Gallery" onPress={()=>{selectGaleria()}} />
            </Dialog.Container>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({

  title:{
    fontWeight:'bold',
    fontSize:wp('4.5%'),
    color:'#3d3d3d'
},

textSmall:{
  color:"#3d3d3d",
  fontSize:wp('4%'),
  marginLeft:wp('7.5%'),
  fontWeight:'normal'
 }, 

    container: {
        flex: 1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: '#fff',
    },
    inputs:{
      borderWidth:2,
      borderRadius:5,
      borderColor:'#f4f4f4',
      height:55
    },

   
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 0,
      backgroundColor:'#0000002e',
    },
    modalView: {
      margin: 20,
      backgroundColor: "white",
      borderRadius: 20,
      width:wp('85%'),
      padding: 35,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },
    button: {
      borderRadius: 5,
      padding: 10,
      elevation: 2,
      width:wp('30%'),
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonClose: {
      backgroundColor: "#00c900",
      textTransform:'uppercase',
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textTransform:'uppercase',
    },
    modalText: {
      marginBottom: 5,
    },
  
  
    modalTextTiTle: {
      marginBottom: 5,
      fontSize:wp('5%'),
      fontWeight:'normal',
      color:'red',
      fontWeight:'bold',
    },
  
    subTitle:{
      fontWeight:'normal',
      fontSize:wp('4%'),
      color:'#404040',
      marginTop:10,
      
  
  
    },
    areaText:{
      fontWeight:'normal',
      fontSize:wp('4%'),
      color:'#3d3d3d'
  
    },

    margin5:{
       marginTop:5,
    },
    margin10:{
      marginTop:10,
   },
  
    textPuntos:{
      backgroundColor:'#eee',
      padding:2,
      paddingLeft:10, 
      paddingRight:10,
      color:'#404040',
      borderRadius:5,
      marginTop:10,
      fontSize:wp('4%'),
      
    },

    createButtonAlertCancel: {
      width: wp('30%'),
      marginTop: 25,
      marginBottom: 0,
      shadowColor: '#3d3d3d',
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.8,
      borderRadius:10,
      elevation: 5,
      justifyContent:'center',
      alignItems:'center',
      height:hp('6%'),
  },


    createButtonAlert: {
      width: wp('30%'),
      marginTop: 25,
      marginBottom: 0,
      shadowColor: 'red',
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.8,
      borderRadius:10,
      elevation: 5,
      justifyContent:'center',
      alignItems:'center',
      height:hp('6%'),
  },

  


  buttonCreate:{
      borderRadius:5, 
      backgroundColor:'#11A10F', 
      padding:9, 
      // width:wp('70%'), 
      textAlign:'center', 
      alignItems:'center', 
      // height:hp('5%'),  
      flexDirection:'row',
      justifyContent:'center'
  },
  textButton: {
      color:'white',
      fontWeight:'bold',
      textTransform:'uppercase'
  },

 
  textSmall:{
   color:"#3d3d3d",
   fontSize:wp('4%'),
   marginLeft:wp('7.5%'),
   fontWeight:'normal'
  }, 
  container: {
      flex: 1,
      backgroundColor: '#fff',
  },

  title:{
      fontWeight:'bold',
      fontSize:wp('4.5%'),
      color:'#3d3d3d'
  },
  fuente:{
      fontSize:wp('4.8%')
  },

  iconos:{
     fontSize:wp('6%'),
     color:'#a5a5a5',
     marginRight:10
  }

});



//make this component available to the app
export default ProfileCoach;
