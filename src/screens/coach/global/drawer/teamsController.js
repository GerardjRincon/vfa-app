//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import { useNavigation } from '@react-navigation/native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import Teams from './teams';
import Team from './team';
import Perfil from '../../myteam/perfil';
import Video from '../../reviews/videoPlayer';
import newTeam from './newTeam';
import I18n from 'react-native-i18n';
const Stack = createStackNavigator();
const screenOptionStyle = {
    headerStyle: {
      backgroundColor: "#FFF",
    },
    headerTintColor: "black",
    headerBackTitle: "Back",
  };

const Back =(props)=>{
    const navigation = useNavigation();
    return(
      <View style={{height:hp('5.7%'), width:wp('10%'), justifyContent: 'center', alignItems:'center', flexDirection:'row',backgroundColor: '#FFF',}}>
          <TouchableOpacity onPress={()=>navigation.goBack()} style={{width:'100%', marginLeft:10, height:'100%', justifyContent: 'center', alignItems:'center', backgroundColor: '#FFF', }}>
              <Icon2 name="arrow-left" size={hp('2.5%')}/>
          </TouchableOpacity>
      </View>
    );
}
// create a component
const TeamsController = () => {
    return (
        <Stack.Navigator screenOptions={screenOptionStyle} initialRouteName="Teams">
            <Stack.Screen name="Teams" component={Teams} options={{
                headerLeft: (props) => {
                    return (
                        <Back {...props}/>
                    );
                }
              }}/>
              <Stack.Screen name="Team" component={Team}/>
              <Stack.Screen name="Profile Player" component={Perfil}/>
              <Stack.Screen name="Review player MyTeam" component={Video}/>
              <Stack.Screen name="New Team" component={newTeam}/>
        </Stack.Navigator>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F3F3F3',
    },
});

//make this component available to the app
export  {TeamsController};
