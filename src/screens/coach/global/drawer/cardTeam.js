//import liraries
import React, { useState, useEffect, useRef } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'react-native-i18n';
// create a component
const CardTeam =(props)=>{
    const item = props.item;
    const imageMyTeam = 'https://go.vfa.app'+item.photo;
    const menu = useRef();
    const hideMenu = () => menu.current.hide();
    const showMenu = () => menu.current.show();

    const Button = (props) => {
        return (
            <View style={{flex:1, flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                <Icon name={props.icon} size={hp('1.5%')}/>
                <Text style={{fontSize:hp('1.7%'), marginLeft:10}}>{props.title}</Text>
            </View>
        )
    }

    return(
        <TouchableOpacity activeOpacity={1} onPress={()=>props.team(item)} style={{width:wp('100%'), height:hp('10%'), flexDirection:'row',backgroundColor: "#FFF", borderBottomWidth:1, borderColor:'#D6D6D6'}}>
            <View style={{flex:1, marginLeft:20, justifyContent:'center',}}>
                {item.photo?
                    <Image source={{uri:imageMyTeam}} resizeMode="cover" style={{width:hp('8%'),height: hp('8%'), borderRadius:10}} />
                :
                    <View style={{backgroundColor:'#D6D6D6', width:hp('8%'),height: hp('8%'), borderRadius:10, justifyContent:'center', alignItems:'center'}}>
                        <Text style={{fontSize:hp('2%'), fontWeight:'bold', }}>{item.name.charAt(0)}</Text>
                    </View>
                }
                {item.active?
                    <Icon name="circle" size={hp('2%')} color="green" style={{position:'absolute', top:10, left:0}}/>
                :null}
            </View>
            <View style={{flex:4, flexDirection:'row',  justifyContent:'center', alignItems:'center',}}>
                <View style={{flex:3}}>
                    <Text style={{fontSize:hp('1.7%'), fontWeight:'bold', textTransform:'capitalize'}}>{item.name}</Text>
                    <Text style={{fontSize:hp('1.4%'),opacity:0.4, textTransform:'capitalize'}}>{item.working == 'level'?item.start_working:item.working+':'} {item.working !== 'level'?item.start_working:null}</Text>
                    <View style={{alignItems:'center', flexDirection:'row', width:wp('10%'),}}>
                        <Icon name="users" size={hp('1.5%')} color="#333" />
                        <Text style={{fontSize:hp('1.5'), fontWeight:'bold',color:"#333"}}> / {item.player}</Text>
                    </View>
                </View>
                <View style={{marginRight: 20, height:'100%', width:wp('15%'), justifyContent:'center', alignItems:'center'}}>
                <Menu
                    ref={menu}
                    onPress={showMenu} 
                    >
                        <MenuItem onPress={()=>{props.activate(item),hideMenu()}} children={<Button style={{paddingLeft: 10,}} title="Activate" icon="check" />}></MenuItem>
                        <MenuItem onPress={()=>{props.editable(item),hideMenu()}} children={<Button style={{paddingLeft: 10,}} title="Edit" icon="pencil" />}></MenuItem>
                        <MenuItem onPress={()=>{props.delete(item),hideMenu()}} children={<Button style={{paddingLeft: 10,}} title="Delete" icon="trash" />}></MenuItem>

                </Menu>
                    <TouchableOpacity activeOpacity={1} onPress={showMenu} style={{justifyContent:'center', alignItems:'center', height:'100%', width:'100%',}}>
                        <Icon name="ellipsis-h" size={hp('2%')} />
                    </TouchableOpacity>
                </View>
            </View>
        </TouchableOpacity>
    );
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default CardTeam;
