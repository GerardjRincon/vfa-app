//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ActivityIndicator, Modal } from 'react-native';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

// create a component
const Loader = (props) => {
    return (
        <Modal style={styles.container} transparent>
            <View style={{flex:1, backgroundColor: 'rgba(0,0,0,0.5)', justifyContent:'center', alignItems:'center'}}>
                <ActivityIndicator size='large' color='green'/>
                {props.title?
                    <Text style={{color:'#FFF'}}>{props.title}</Text>
                :null}
            </View>
        </Modal>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        top:0,
        bottom:0,
        right:0,
        left:0,
        zIndex: 100000,
        // backgroundColor: 'rgba(0,0,0,0.5)',
    },
});

//make this component available to the app
export default Loader;
