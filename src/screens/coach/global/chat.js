//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity, Modal, KeyboardAvoidingView, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { url } from '../../../storage/config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ChatItem from './chatItem';
import I18n from 'i18n-js';
// create a component
const Chat = ({navigation, route}) => {
    const [players, setPlayers] = useState([]);
    const [user, setUser] = useState({});
    const [token, setToken] = useState(null);
    const [visible, setVisible] = useState(false);
    const [player, setPlayer] = useState({})

    useEffect(async() => {

        const user = await AsyncStorage.getItem('@user');
        const token = await AsyncStorage.getItem('@token');
        const userJson = JSON.parse(user);
        const tokenJson = JSON.parse(token);
        setToken(tokenJson);
        getList(tokenJson);
        setUser(userJson);
    }, [])


    useLayoutEffect(() => {
        navigation.setOptions({
            title: I18n.t('tex16'),
        });
    }, [])




    async function getList(token){
        // setLoader(true)
        try {
            await fetch(url+'api/team/myteam',{
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer '+token
            },
        }).then(res => res.json())
        .then((dat) =>{
            try {
                console.warn(dat)
                setPlayers(dat.data.players)
            } catch (error) {
                // console.warn('error:'+error)
            }
        })
            
        } catch (error) {
            // console.warn(error)
        }
    }

    const RenderItem=({item, index, separators})=>{
        const imageMyTeam = 'https://go.vfa.app/'+item.photo;
        return(
            <TouchableOpacity activeOpacity={0.8} onPress={()=>{setPlayer(item); setVisible(true)}} style={{width:wp('100%'), height:hp('10%'), borderBottomWidth:1, borderColor:'#D6D6D6', flexDirection:'row',backgroundColor: "#FFF", }}>
                <View style={{flex:1, marginLeft:20, justifyContent:'center',}}>
                    {item.photo?
                        <Image source={{uri:imageMyTeam}} resizeMode="cover" style={{width:hp('8%'),height: hp('8%'), borderRadius:10}} />
                    :
                        <View style={{backgroundColor:'#D6D6D6', width:hp('8%'),height: hp('8%'), borderRadius:10, justifyContent:'center', alignItems:'center'}}>
                            <Text style={{fontSize:hp('2%'), fontWeight:'bold', }}>{item.name.charAt(0)}</Text>
                        </View>
                    }
                </View>
                <View style={{flex:4, flexDirection:'row',  justifyContent:'center', alignItems:'center'}}>
                    <View style={{flex:3}}>
                        <Text style={{fontSize:hp('1.7%'), fontWeight:'bold'}}>{item.name}</Text>
                        <Text style={{fontSize:hp('1.5%'),opacity:0.4, textTransform:'capitalize' }}>{item.name_method == 'level'?item.namemethod:item.name_method+': '}{item.name_method !== 'level'? item.namemethod: null}</Text>
                    </View>
                    {item.messagespendient>0?
                        <View style={{justifyContent:'center', alignItems:'center', marginRight:20, width:hp('5%'),height:hp('5%'), borderRadius:50, backgroundColor: `${item.media<6?'#EF3529':item.media==6?'#F17011':item.media>6?'green':null}`,}}>
                            <Text style={{fontSize:hp('1.5'), fontWeight:'bold',color:"#FFF", position:'absolute', zIndex:1}}>{item.messagespendient}</Text>
                            <Icon name="circle" size={hp('3.5%')} color="green" />
                        </View>
                    :null}
                    <View style={{marginRight: 20,}}>
                        <Icon name="chevron-right" size={hp('1.7%')} />
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    return (
        <View style={styles.container}>
            <Image source={require('../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    position: "absolute",
            }} />
            <FlatList
                data={players}
                renderItem={RenderItem}
                keyExtractor={(item, index) => index}
                style={{ flex: 1 }}
            />
            <Modal 
            animationType="fade"
            onRequestClose={()=>{setVisible(false)}}
            visible={visible}>
                    <ChatItem close={()=>{setVisible(false); getList(token)}} player={player} style={{flex:1}}/>
            </Modal>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F3F3F3',
    },
});

//make this component available to the app
export default Chat;
