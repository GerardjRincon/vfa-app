//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, FlatList, Modal,NativeModules  } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Snackbar from 'react-native-snackbar';
import Loader from '../../coach/global/loader';
import { LogoutUser } from '../../../storage/user/dataUser';
import { url } from '../../../storage/config';
import Dialog from "react-native-dialog";
// import { channel, pusher } from '../../../routes/DrawerCoach';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// create a component

import {signal, canal} from '../../../storage/pusher';

// import moment from 'moment';
import moment from "moment/min/moment-with-locales";
// require('moment/locale/es.js');
const Notification = ({navigation, route}) => {
    const {id} = route.params?route.params:0;
    const [list, setList] = useState([]);
    const [user, setUser] = useState({});
    const [token, setToken] = useState(null);
    const [itemId, setItemId] = useState(null);
    const [visible, setVisible] = useState(false);
    const [loader, setLoader] = useState(false);
    const [refresh, setRefresh] = useState(false);
    const [idioma, setIdioma] = useState('en');

    useEffect(async() => {
        const token = await AsyncStorage.getItem('@token');
        const user = await AsyncStorage.getItem('@user');
        const userJson = JSON.parse(user);
        const tokenJson = JSON.parse(token);
        setToken(tokenJson);
        setUser(userJson);
        getList(tokenJson);



         // Função responsável por adquirir o idioma utilizado no device
         const getLanguageByDevice = () => {
            return Platform.OS === 'ios'
            ? NativeModules.SettingsManager.settings.AppleLocale // Adquire o idioma no device iOS
            : NativeModules.I18nManager.localeIdentifier // Adquire o idioma no device Android
            }
       
            
            const language = getLanguageByDevice()
            let idiomas = language.substr(0,2);
       
             if(idiomas == 'es'){
                setIdioma('es')
             }
             else if(idiomas == 'cn'){
               setIdioma('cn')
             }
             else{
                setIdioma('en')
             }
    
    
    
             console.log("este es el idioma", idioma)
    
         
             




        const canal = signal.subscribe("VFA");
        
        canal.bind("Notification-" + userJson.id, (data) => {
            console.warn(data)
            if (data.tag === 'SkillAssignedCoach') {
                return;
            }else{
                console.warn(tokenJson) 
                getList(tokenJson);
            }
          });
        return ()=>{
            signal.disconnect();
        }
    }, [navigation])

    useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Notifications',
        });
    }, [navigation])

    async function getList(token){
        setLoader(true)
        try {
            await fetch(url+'api/notification/list',{
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer '+token
            },
        }).then(res => res.json())
        .then((dat) =>{
            console.warn(dat)
            try {
                if (dat.ok) {
                    const solicitud = dat.solicitudes;
                    const noti = dat.data;
                    var list = solicitud.concat(noti);
                    setList(list);
                    setLoader(false)
                }
                // if (dat.message == 'Unauthenticated.') {
                //     LogoutUser()
                //     setTimeout(()=>{
                //         navigation.replace('Login')
                //     },1000)
                // }
            } catch (error) {
                setLoader(false)
                console.warn('error:'+error)
            }
        })
            
        } catch (error) {
            console.warn(error)
            setLoader(false)
        }
    }


    // Função responsável por adquirir o idioma utilizado no device
    // const getLanguageByDevice = () => {
    //     return Platform.OS === 'ios'
    //     ? NativeModules.SettingsManager.settings.AppleLocale // Adquire o idioma no device iOS
    //     : NativeModules.I18nManager.localeIdentifier // Adquire o idioma no device Android
    //     }
   



    // function Tiempo(fecha){

    //     const language = getLanguageByDevice()
    //     let idioma = language.substr(0,2);



    //     if (fecha == null) {
    //       return '';
    //   } else {
    //       var formato = new Date(fecha)
    //       var fechaCreacion = formato.getTime();
    //         // console.warn(i)
    //         moment.locale(idioma); //set your locale (eg: fr)
    //         return moment(fechaCreacion).startOf('segund').fromNow();
    //       }
    // }


    function Tiempo(fecha){
        var formato = new Date(fecha)
        var fechaCreacion = formato.getTime();
        // console.warn(i)
        moment.locale(idioma);
        return moment(fechaCreacion).startOf('segund').fromNow();
      }




    async function reply(id, type, index) {
        setLoader(true)
        try {
            await fetch(url + 'api/team/reply', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    noti_id: id,
                    reply: type,
                    type_user:'coach'
                })
            }).then(res => res.json())
                .then((dat) => {
                    // console.warn(dat)
                    try {
                        if (dat.ok) {
                            setLoader(false)
                            setList(list => list.filter((item, i) => i !== index));
                            Snackbar.show({
                                text: 'Reply sent',
                                duration: Snackbar.LENGTH_LONG,
                            });
                        }
                        // if (dat.message == 'Unauthenticated.') {
                        //     setLoader(false)
                        //     LogoutUser()
                        //     setTimeout(() => {
                        //         navigation.replace('Login')
                        //     }, 1000)
                        // }
                    } catch (error) {
                        setLoader(false)
                        console.warn('error:' + error)
                    }
                })

        } catch (error) {
            console.warn(error)
            setLoader(false)
        }
    }

    async function delet() {
        setLoader(true)
        try {
            await fetch(url + 'api/notification/delete/'+itemId, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
            }).then(res => res.json())
                .then((dat) => {
                    console.warn(dat)
                    try {
                        if (dat.ok) {
                            setLoader(false)
                            getList(token) 
                            setVisible(false)
                            setItemId(null)
                            setRefresh(false)
                        }
                        // if (dat.message == 'Unauthenticated.') {
                        //     LogoutUser()
                        //     setTimeout(() => {
                        //         navigation.replace('Login')
                        //     }, 1000)
                        // }
                    } catch (error) {
                        setLoader(false)
                        setRefresh(false)
                        console.warn('error:' + error)
                    }
                })
        } catch (error) {
            
        }
    }

    const renderItem=({item, index, separators})=>{
        var urlProfile = 'https://go.vfa.app/'+item.user_photo;
        var color = '#FFF';
        if (id) {
            if (item.id == id) {
                color = '#88C4A0';
            }
        }else{
            if (!item.status) {
                color= '#D7E8D3';
            }
        }
        return(
            item.status !== 'pending' ?
                <TouchableOpacity activeOpacity={0.8} onPress={()=>{
                    if (item.skill_user_id) {
                        navigation.navigate('QualificationVideo',{id:item.user_id, token, skill_user_id:item.skill_user_id})
                    }

                    }} 
                    onLongPress={()=>
                        {
                            setItemId(item.id);
                            setVisible(true);
                        }
                     } style={{width:wp('100%'), height:hp('13%'), flexDirection:'row', justifyContent:'center', alignItems:'center', backgroundColor: color,  borderBottomWidth:1, borderColor:'#D5D5D5', paddingHorizontal:15, }}>
                    <View style={{width:70, height:'100%', justifyContent:'center', alignItems:'center',}}>
                        {item.user_photo?
                            <Image source={{uri:urlProfile}} esizeMode="cover" style={{width:60, height:60, borderRadius:50, marginRight:10}} />
                        :
                            <View style={{backgroundColor:'#D6D6D6',width:60, height:60, borderRadius:50, justifyContent:'center', alignItems:'center'}}>
                                <Text style={{color:'#FFF', fontSize:hp('1.7%'), fontWeight:'bold'}}>{item.user_name.charAt(0)}</Text>
                            </View>
                        }
                    </View>
                    <View style={{flex:1,}}>
                            <Text numberOfLines={1} style={{fontSize:hp('2%'), fontWeight:'bold'}}>
                                {item.title}
                            </Text>
                            <Text numberOfLines={2} style={{fontSize:hp('1.7%'), opacity:0.8, width:'90%',}}>
                                {item.body}
                            </Text>
                            <Text style={{fontSize:hp('1.3%'), position:'absolute', bottom:-20, opacity:0.5}}>
                                {Tiempo(item.created_at)}
                            </Text>
                    </View>
                    <View>
                        <Icon name="chevron-right" size={hp('1.5%')} color="#000"/>
                    </View>
                </TouchableOpacity>
            :
                <TouchableOpacity activeOpacity={0.8} onLogPress={()=>{
                    setItemId(item.id);
                    setVisible(true);
                }} style={{ width: wp('100%'), height: hp('20%'), justifyContent: 'center', alignItems: 'center', backgroundColor: color, borderBottomWidth: 1, borderColor: '#D5D5D5', paddingHorizontal: 15, paddingTop: 10, paddingBottom: 10, }}>
                        <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'center', alignItems: 'center', paddingLeft: 10 }}>
                            <View style={{ flex: 1, }}>
                                <Text numberOfLines={2} style={{ fontSize: hp('1.7%'), fontWeight: 'bold' }}>
                                    {item.title}
                                </Text>
                                <Text numberOfLines={2} style={{ fontSize: hp('1.5%'), opacity: 0.8 }}>
                                    {item.body}
                                </Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => reply(item.id, 'yes', index)} activeOpacity={0.8} style={{ width: '70%', borderRadius: 10, backgroundColor: 'green', padding: 5, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontSize: hp('1.5%'), color: '#FFF', fontWeight: 'bold' }}>Accept</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => reply(item.id, 'no', index)} activeOpacity={0.8} style={{ width: '70%', borderRadius: 10, backgroundColor: 'red', padding: 5, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontSize: hp('1.5%'), color: '#FFF', fontWeight: 'bold' }}>Decline</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableOpacity>
            
        )
    }


    return (
        <View style={styles.container}>
            {loader?<Loader/>:null}
            <Image source={require('../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    position: "absolute",
            }} />
            {list.length<1?
                <View style={{justifyContent:'center', alignItems:'center'}}>
                    <Icon name="bell-slash" size={hp('15%')} style={{opacity:0.3}}/>
                    <Text style={{fontSize:hp('2%'), paddingTop:20}}>No notifications available</Text>
                </View>
            :
                <FlatList
                    data={list}
                    renderItem={renderItem}
                    keyExtractor={(item, i) => i}
                    style={{width:'100%',height:'100%',}}
                    onRefresh={() => getList(token)}
                    refreshing={refresh}
                />
            }
            <Dialog.Container visible={visible} onRequestClose={()=>setVisible(false)} onPress={()=>setVisible(false)}>
                <Dialog.Title>Delete notification</Dialog.Title>
                    <Dialog.Description>
                        You sure want to delete the notification?
                    </Dialog.Description>
                    <Dialog.Button label="Cancel" style={{color:'red'}} onPress={()=>{setVisible(false)}} />
                <Dialog.Button label="Delete" onPress={()=>{delet()}} />
            </Dialog.Container>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F3F3F3',
    },
});

//make this component available to the app
export default Notification;
