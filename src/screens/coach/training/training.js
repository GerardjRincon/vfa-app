import React, { useState, useEffect, useLayoutEffect } from 'react'
import { Alert, StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import { url} from '../../../storage/config';
// import { channel, pusher } from '../../../routes/DrawerCoach';
import {signal, canal} from '../../../storage/pusher';

import AsyncStorage from '@react-native-async-storage/async-storage';
import { Agenda } from 'react-native-calendars';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Dialog from "react-native-dialog";
import Loader from '../global/loader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'i18n-js';


const Training = ({ route }) => {

  const navigation = useNavigation();
  const [onDateChanged, setOnDateChange] = useState('2017-09-07')
  const [items, setItems] = useState({});
  const [token, setToken] = useState(null);
  const [refresh, setRefresh] = useState(false)
  const [visible, setVisible] = useState(false);
  const [loader, setLoader] = useState(false);
  const [itemDele, setItemDele] = useState(null);
  const [user, setUser] = useState(null);

  async function getList2(token) {
    // setLoader(true)
    setRefresh(true)
    try {
      await fetch(url + 'api/calendar/app/list/coach', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      }).then(res => res.json())
        .then((dat) => {
          try {
            setItems(dat.data)
            console.warn(dat)
            setRefresh(false)
          } catch (error) {
            // console.warn('error:'+error)
            setRefresh(false)
          }
        })

    } catch (error) {
      // console.warn(error)
      setRefresh(false)
    }
  }

  useEffect(async () => {
    const user = await AsyncStorage.getItem('@user');
    const token = await AsyncStorage.getItem('@token');
    const userJson = JSON.parse(user);
    const tokenJson = JSON.parse(token);
    setToken(tokenJson);
    getList2(tokenJson);
    setUser(userJson);

    const canal = signal.subscribe("VFA");

    canal.bind("NotificationCalendarAssigned-" + userJson.id, async (data) => {
          getList2(tokenJson);
    });


    return ()=>{
      signal.disconnect();
  }
    
  }, [])



  useLayoutEffect(() => {
    navigation.setOptions({
      title: I18n.t('training.text5'),
  })
  }, [])

  async function delet() {
    setLoader(true)
    setVisible(false)
    try {
        await fetch(url + 'api/calendar/destroy', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body:JSON.stringify({
              calendar_id:itemDele.calendar_id
            })
        }).then(res => res.json())
            .then((dat) => {
                console.warn(dat)
                try {
                    if (dat.ok) {
                        setLoader(false)
                        getList2(token) 
                    }
                    // if (dat.message == 'Unauthenticated.') {
                    //     LogoutUser()
                    //     setTimeout(() => {
                    //         navigation.replace('Login')
                    //     }, 1000)
                    // }
                } catch (error) {
                    setLoader(false)
                    console.warn('error:' + error)
                }
            })
    } catch (error) {
        
    }
}

  const renderItem = (item) => {
    var urlImage = 'https://go.vfa.app/preview/' + item.img;
    var urlExercises = 'https://go.vfa.app/exercises/' + item.img;
    item.id = item.skill_id;
    return (
      <TouchableOpacity
        testID={'item'}
        activeOpacity={0.8}
        style={[styles.item, { height: 80, elevation: 4 }]}
        onPress={() => {
          if (item.type == 1) {
            navigation.navigate('3D Skill Training', { skill: item, token: token, user })
          } else {
            navigation.navigate('3D Exercises Training', { exercise: item, token: token, user, view:'training' })
          }
        }}
        onLongPress={()=>{
          setVisible(true);
          setItemDele(item);
        }}>
        <View style={{ flex: 1, flexDirection: 'row', }}>
          <View style={{ width: hp('7%'), height: '100%', justifyContent: 'center', alignItems: 'center' }}>
            <Image source={{ uri: item.type == 1 ? urlImage : urlExercises }} resizeMode="cover" style={{ width: hp('6%'), height: hp('6%'), borderRadius: 10 }} />
          </View>
          <View style={{ marginLeft: 10, flex: 1, }}>
            <Text style={{ fontSize: hp('1.2%'), position: 'absolute', right: 0, color: '#929292' }}>{item.type == 1 ? "Skill" : "Exercises"}</Text>
            <View style={{ width: '100%', height: '100%', justifyContent: 'center', }}>
              <Text numberOfLines={2} style={{ fontSize: hp('1.7%'), opacity: 0.7, width: '85%' }}>{item.name}</Text>
              {item.description ?
                <Text numberOfLines={2} style={{ fontSize: hp('1.4%'), opacity: 0.5 }}>{item.description}</Text>
                : null}
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  const renderEmptyDate = () => {
    return (
      <View
        style={{ height: 80, elevation: 4, justifyContent:'center', alignItems:'center', backgroundColor: '#FFF', borderRadius: 5,
        padding: 10,
        marginLeft:10,
        marginRight: 10,
        marginTop: 17,
        marginBottom: 4, }}>
        <View style={{flexDirection: 'row', justifyContent:'center', alignItems:'center' }}>
          <View style={{ width: hp('7%'), height: '100%', justifyContent: 'center', alignItems: 'center' }}>
            {/* <Image source={{ uri: item.type == 1 ? urlImage : urlExercises }} resizeMode="cover" style={{ width: hp('6%'), height: hp('6%'), borderRadius: 10 }} /> */}
            <Icon2 name="soccer" size={hp('4%')} />
          </View>
          <View style={{ marginLeft: 10, flex: 1, justifyContent:'center', alignItems:'center'}}>
            {/* <Text style={{ fontSize: hp('1.2%'), position: 'absolute', right: 0, color: '#929292' }}>{item.type == 1 ? "Skill" : "Exercises"}</Text> */}
            <View style={{ width: '100%', height: '100%', justifyContent: 'center', }}>
              <Text numberOfLines={2} style={{ fontSize: hp('1.7%'), opacity: 0.7, width: '85%' }}> {I18n.t('training.text4')}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }

  function timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }

  return (
    <View style={{ flex: 1 }}>
      {loader?<Loader/>:null}
      <Agenda
        testID={'agenda'}
        items={items}
        renderItem={renderItem}
        renderEmptyData = {renderEmptyDate}
        // showClosingKnob={true}
        onRefresh={() => getList2(token)}
        refreshing={refresh}
      />
      {/* <TouchableOpacity style={{position:'absolute', bottom:30, right:20, width:60, height:60, backgroundColor: '#54BCE5', borderRadius:50, elevation:3, justifyContent: 'center', alignItems:'center'}}>
                <Icon name="plus" size={hp('1.7%')} color="white"/>
            </TouchableOpacity> */}
      <Dialog.Container visible={visible} onRequestClose={()=>setVisible(false)} onPress={()=>setVisible(false)}>
        <Dialog.Title>{I18n.t('training.text3')}</Dialog.Title>
            <Dialog.Description>
              {I18n.t('training.text2')}
            </Dialog.Description>
            <Dialog.Button label="Cancel" style={{color:'red'}} onPress={()=>{setVisible(false)}} />
        <Dialog.Button label="Delete" onPress={()=>{delet()}} />
      </Dialog.Container>
    </View>
  )
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#FFF',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
    marginBottom: 4,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30,
    backgroundColor: 'red',
  }
});

export default Training
