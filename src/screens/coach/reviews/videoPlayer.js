import React, { useState, useEffect, useLayoutEffect, useRef } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Modal, Dimensions, KeyboardAvoidingView, Platform } from 'react-native';
import { Block, Checkbox, Input, Button as GaButton, theme } from 'galio-framework'
import Snackbar from 'react-native-snackbar';
import Video2 from 'react-native-video';
import { url } from '../../../storage/config';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
const { width, height } = Dimensions.get('screen');
import Loader from '../global/loader';

import CameraRoll from "@react-native-community/cameraroll";
import { request, requestMultiple, PERMISSIONS } from 'react-native-permissions';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'react-native-i18n';


const Video = ({ navigation, route }) => {
    const { player, token, update, video, view } = route.params;
    const [visible, setVisible] = useState(false);
    const [index, setIndex] = useState(0);
    const [note, setNote] = useState(null);
    const [loader, setLoader] = useState(false);
    const [qualification1] = useState([
        { label: '1', value: 1 }, { label: '2', value: 2 }, { label: '3', value: 3 }, { label: '4', value: 4 },
        { label: '5', value: 5 }, { label: '6', value: 6 }, { label: '7', value: 7 }, { label: '8', value: 8 }, { label: '9', value: 9 },
        { label: '10', value: 10 }
    ])
    const [qualify, setQualify] = useState(1);
    const [status, setStatus] = useState(false);

    var urlVideo = '';
    if (view) {
        if (view == 'perfil') {
            urlVideo = video.file_video;
        }else{
            urlVideo = player.skill_file_video;
        }
    }else{
        if (video) {
            urlVideo = video.file_video;
        } else {
            urlVideo =  player.file_video;
        }
    }

    useEffect(() => {
        setLoader(true)
        if (view=='skill') {
            setStatus(player.favorite)
        }else{
            if (view == 'perfil') {
              setStatus(video.favorite)
            }
        }
        console.log(player)


        console.log("entro aqui");
    }, [])

    useLayoutEffect(() => {
        {
            view !== 'skill' ?
                view == 'perfil'?
                    navigation.setOptions({
                        title: player.name,
                        headerRight: (props) => {
                            return (
                                <View style={{width: 100, height: '100%'}}>   
                                    <TouchableOpacity activeOpacity={0.5} onPress={() =>Favorite()} activeOpacity={0.8} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                                        <Icon2 name={status?"star":"star"} size={hp('4%')} color={status?"#F6D24C":"#d3d3d3"} />
                                    </TouchableOpacity>
                                </View>
                            )
                        }
                    })
                :
                    video ?
                        navigation.setOptions({
                            title: player.player_name,
                        })
                    :
                        navigation.setOptions({
                            title: player.player_name,
                            headerRight: (props) => {
                                return (
                                    <Qualify {...props} />
                                )
                            }
                        })
                : 
                navigation.setOptions({
                    title: player.name,
                    headerRight: (props) => {
                        return (
                            <View style={{width: 100, height: '100%'}}>   
                                <TouchableOpacity activeOpacity={0.5} onPress={() =>Favorite()} activeOpacity={0.8} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                                    <Icon2 name={status?"star":"star"} size={hp('4%')} color={status?"#F6D24C":"#d3d3d3"} />
                                </TouchableOpacity>
                            </View>
                        )
                    }
                })
        }
    }, [status])


    function DescargarVideo(){

    }


    function SaveVideo() {
        if (Platform.OS === 'android') {
          async function requestCameraPermission() {
            try {
              const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
                'title': I18n.t('ar.alert.permitcamera'),
                'message':I18n.t('ar.alert.msgpermitcamera'),
              }
              )
              if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    
    
                CameraRoll.save("file://" + video.data)
                setVisibleMenu(false)
    
                setTimeout(() => {
                  Snackbar.show({
                    text:I18n.t('ar.alert.textsuccessfully'),
                    duration: Snackbar.LENGTH_LONG,
                  });
                }, 1)
    
              } else {
                // alert("Permiso denegado");
                Snackbar.show({
                  text:I18n.t('ar.alert.textdenied'),
                  duration: Snackbar.LENGTH_LONG,
                });
              }
            } catch (err) {
              // alert("Error:", err);
              Snackbar.show({
                text: I18n.t('ar.alert.hasoccurred'),
                duration: Snackbar.LENGTH_LONG,
              });
            }
          }
          requestCameraPermission();
        } else {
          request(PERMISSIONS.IOS.PHOTO_LIBRARY).then((result) => {
            if (result === 'granted' | 'limited') {
    
              CameraRoll.save("file://" + video.data)
            //   setVisibleMenu(false)
              setTimeout(() => {
                Snackbar.show({
                  text:I18n.t('ar.alert.textsuccessfully'),
                  duration: Snackbar.LENGTH_LONG,
                });
              }, 1)
            } else {
              Snackbar.show({
                text:I18n.t('ar.alert.necessarypermissions'),
                duration: Snackbar.LENGTH_LONG,
              });
            }
          });
        }
      }

      


    const Qualify = (props) => {
        return (
            <View style={{ width: 100, height: '100%' }}>
                <TouchableOpacity activeOpacity={0.5} onPress={() => setVisible(true)} activeOpacity={0.8} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                    <Icon name="check" size={hp('2.4%')} color="#646469" />
                    <Text>Rate</Text>
                </TouchableOpacity>
            </View>
        )
    }

    function clean() {
        setVisible(false)
        setIndex(0)
        setNote(null)
    }

    const SendQualify = async () => {
        setLoader(true)
        setVisible(false)
        try {
            await fetch(url + 'api/skill/qualification', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    comment: note,
                    qualification: qualify,
                    skill_user_id: player.id,
                }),
            }).then(res => res.json())
                .then((dat) => {
                    try {
                        if (dat.ok) {
                            setLoader(false)
                            update(dat.data);
                            navigation.goBack();
                        } else {
                            setLoader(false)
                            Snackbar.show({
                                text: 'Error sending the grade',
                                duration: Snackbar.LENGTH_LONG,
                            });
                        }
                    } catch (error) {
                        setLoader(false)
                        Snackbar.show({
                            text: 'An unexpected error has occurred',
                            duration: Snackbar.LENGTH_LONG,
                        });
                    }
                })

        } catch (error) {
            setLoader(false)
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
            });
        }
    }

    const Favorite = async () => {
        try {
            const datos = {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    id: view == 'perfil'?video.id:player.skill_id,
                    status: !status,
                }),
            }

            console.log(datos);

            await fetch(url + 'api/skill/favorite', datos).then(res => res.json())
                .then((dat) => {
                    console.warn(dat)
                    try {
                        if (dat.ok) {
                            setStatus(!status)
                        } else {
                            Snackbar.show({
                                text: 'Error sending the grade',
                                duration: Snackbar.LENGTH_LONG,
                            });
                        }
                    } catch (error) {
                        Snackbar.show({
                            text: 'An unexpected error has occurred',
                            duration: Snackbar.LENGTH_LONG,
                        });
                    }
                })
        } catch (error) {
            
        }
    }

    console.log('video', video);

    return (
        <View style={{ width: '100%', height: '100%' }}>
            {loader ? <Loader title={'Loading video...'} /> : null}
            <Video2 source={{ uri: urlVideo }}
                controls={true}
                resizeMode="cover"
                onLoad={() => setLoader(false)}
                style={styles.backgroundVideo} />
            <Modal
                animationType="slide"
                transparent={true}
                visible={visible}>
                <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "null"} enabled style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-end' }}>
                    <View style={{ height: "50%", width: '100%', backgroundColor: "#F3F3F3", justifyContent: "center", borderTopRightRadius: 10, borderTopLeftRadius: 10 }}>
                        {/* <View style={{ height: "50%" ,width: '100%', backgroundColor:"#F3F3F3", justifyContent:"center", borderTopRightRadius:10, borderTopLeftRadius:10}}> */}
                        <TouchableOpacity onPress={() => clean()} style={{ flex: 0.1, width: '100%', height: '6%', justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name="chevron-down" size={hp('2.3%')} color="#FFF" />
                        </TouchableOpacity>
                        <View style={{ flex: 1, borderTopRightRadius: 10, borderTopLeftRadius: 10, }}>
                            <View style={{ flex: 1, marginTop: 10, justifyContent: 'space-between', paddingHorizontal: 20 }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    {
                                        qualification1.map((obj, i) => (
                                            i < 5 ?
                                                <View>
                                                    <TouchableOpacity key={i} onPress={() => { setIndex(i), setQualify(obj.value) }} style={{ width: hp('5%'), height: hp('5%'), borderRadius: 50, borderWidth: 2, borderColor: i < 6 ? '#EF3529' : i == 6 ? "#F17011" : i > 6 ? 'green' : null, justifyContent: 'center', alignItems: 'center' }}>
                                                        <View style={{ width: hp('4%'), height: hp('4%'), borderRadius: 50, backgroundColor: index == i ? `${i < 6 ? '#EF3529' : i == 6 ? '#F17011' : i > 6 ? 'green' : null}` : null, justifyContent: 'center', alignItems: 'center' }}>
                                                            <Text style={{ fontSize: hp('1.5%'), fontWeight: 'bold' }}>{obj.value}</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                                : null
                                        ))
                                    }
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                    {
                                        qualification1.map((obj, i) => (
                                            i > 4 ?
                                                <View>
                                                    <TouchableOpacity key={i} onPress={() => { setIndex(i), setQualify(obj.value) }} style={{ width: hp('5%'), height: hp('5%'), borderRadius: 50, borderWidth: 2, borderColor: i < 6 ? '#EF3529' : i == 6 ? "#F17011" : i > 6 ? 'green' : null, justifyContent: 'center', alignItems: 'center' }}>
                                                        <View style={{ width: hp('4%'), height: hp('4%'), borderRadius: 50, backgroundColor: index == i ? `${i < 6 ? '#EF3529' : i == 6 ? '#F17011' : i > 6 ? 'green' : null}` : null, justifyContent: 'center', alignItems: 'center' }}>
                                                            <Text style={{ fontSize: hp('1.5%'), fontWeight: 'bold' }}>{obj.value}</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                                : null
                                        ))
                                    }
                                </View>
                            </View>
                            <View style={{ flex: 1.5, justifyContent: 'center', alignItems: 'center' }}>
                                <Block width={wp('90')} style={{ marginBottom: 5 }}>
                                    <Input
                                        left
                                        icon="comment"
                                        family="font-awesome"
                                        style={styles.inputs}
                                        iconSize={hp('1.4%')}
                                        placeholder="Note"
                                        help={"Write a note for the player"}
                                        bottomHelp
                                        onChangeText={(e) => setNote(e)}
                                        placeholderTextColor="#818181"

                                    />
                                </Block>
                            </View>
                            <View style={{ flex: 1, marginBottom: 40 }}>
                                <Block center>
                                    <GaButton style={styles.createButton} round size="small" color="success" onPress={() => SendQualify()}>Accept</GaButton>
                                </Block>
                            </View>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </Modal>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F3F3F3',
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    createButton: {
        width: width * 0.5,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
    },
});

//make this component available to the app
export default Video;
