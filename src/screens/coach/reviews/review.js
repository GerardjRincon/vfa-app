//import liraries
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Image as Imagen2, Dimensions } from 'react-native';
import { url } from '../../../storage/config';
// import { channel, pusher } from '../../../routes/DrawerCoach';
import {signal, canal} from '../../../storage/pusher';

import Snackbar from 'react-native-snackbar';
import { setReview } from "../../../storage/user/dataUser";
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TabView, SceneMap,TabBar } from 'react-native-tab-view';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// create a component
const Review = ({navigation}) => {
    const [list, setList] = useState([]);
    const [pending, setPending] = useState([]);
    const [index, setIndex] = useState(0);
    const [token, setToken] = useState(null);
    const [refresh, setRefresh] = useState(false)
    const [routes] = useState([ // Esta es una constante de rutas
        { key: 'inbox', title: I18n.t('tex12') },
        { key: 'pending', title: I18n.t('tex13')  },
      ]);

    useEffect(async() => {
        const token = await AsyncStorage.getItem('@token');
        const reviews = await AsyncStorage.getItem('@reviews');
        const user = await AsyncStorage.getItem('@user');
        const userJson = JSON.parse(user);
        const reviewsJson = JSON.parse(reviews);
        const tokenJson = JSON.parse(token);
        if(reviewsJson){
            setList(reviewsJson.data)
            setPending(reviewsJson.skillsnotVideo)
        }
        setToken(tokenJson)
        getList2(tokenJson)

        const canal = signal.subscribe("VFA");

        canal.bind("Notification-" + userJson.id, (data) => {
            // console.warn(data.tag);
            if (data.tag === "SkillAssignedCoach") {
                getList2(tokenJson);
            }
            if(data.tag === "SkilReview"){
                getList2(tokenJson);
            }
          });
        // const unsubscribe = navigation.addListener('focus', async() => {
        // })
        // return unsubscribe;
        return ()=>{
            signal.disconnect();
          }
    }, [])

    ////// Funciones anexas

    async function getList2(token){ /// Funcion para cargar todo la data de review
        // console.warn(token)
        setRefresh(true)
        try {
            await fetch(url+'api/filter/pending/review',{
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer '+token
            },
        }).then(res => res.json())
        .then((dat) =>{
            console.warn(dat)
            try {
                if(dat.ok){
                    setList(dat.data)
                    setReview(dat)
                    setPending(dat.skillsnotVideo)
                    setRefresh(false)
                }else if(dat.message){
                    Snackbar.show({
                        text: dat.message,
                        duration: Snackbar.LENGTH_LONG,
                      });
                      setRefresh(false)
                }else{
                    Snackbar.show({
                        text: dat.data,
                        duration: Snackbar.LENGTH_LONG,
                      });
                      setRefresh(false)
                }
            } catch (error) {
                console.warn(erro)
                Snackbar.show({
                    text: error,
                    duration: Snackbar.LENGTH_LONG,
                  });
                  setRefresh(false)
            }
        })
        } catch (error) {
            console.warn(error)
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
              });
              setRefresh(false)
        }
    }

    async function deletePendingTask(id){
        try {
            await fetch(url+'api/skill/delete/'+id,
            {
            method: 'DELETE',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer '+token
            },
        }).then(res => res.json())
        .then((dat) =>{
            try {
                if(dat.ok){
                    getList2(token)
                    Snackbar.show({
                        text: dat.data,
                        duration: Snackbar.LENGTH_LONG,
                      });
                }else if(dat.message){
                    Snackbar.show({
                        text: dat.message,
                        duration: Snackbar.LENGTH_LONG,
                      });
                }else{
                    Snackbar.show({
                        text: dat.data,
                        duration: Snackbar.LENGTH_LONG,
                      });
                }
            } catch (error) {
                Snackbar.show({
                    text: I18n.t('reviews.text1'),
                    duration: Snackbar.LENGTH_LONG,
                  });
            }
        })
        } catch (error) {
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
            });
        }
    }



    function updateReview(message){ //// Funcion para actualizar desde la vista del video del player
        getList2(token)
        Snackbar.show({
            text: message,
            duration: Snackbar.LENGTH_LONG,
          });
    }

    ///// Fin de funciones anexas

    ///// Vista de inbox y todo lo relacionado con ella
    const Inbox = () => {
        return(
            <View style={{width:'100%', height:'100%'}}>
                <FlatList
                    data={list}
                    renderItem={dataList}
                    keyExtractor={(item, index) => index}
                    style={{width:'100%',height:'100%',}}
                    ListEmptyComponent={<EmptyInbox/>}
                    onRefresh={() => getList2(token)}
                    refreshing={refresh}
                />
            </View>
        )
    }

    const EmptyInbox=()=>{
        return(
            <View style={{flex:1, height:hp('80%'), justifyContent:'center', alignItems:'center'}}>
                <Icon name="list-alt" size={hp('15%')} color="#000" style={{opacity:0.3}}/>
                <Text style={{fontSize:hp('3%'),fontWeight:'bold', color:'#000', opacity:0.3}}>{I18n.t('reviews.text2')} </Text>
            </View>
        )
    }

    const dataList=({item, index, separators})=>{
        return(
            <View style={{width:'100%',height:400,}}>
                <View style={{paddingBottom:10, paddingTop:10}}>
                    <View style={{marginLeft:10,}}>
                        <Text style={{color:'#000', fontSize:hp('2%'), fontWeight:'bold'}}>{item.name}</Text>
                    </View>
                    <View style={{backgroundColor:'#D6D6D6', height:2, width:'96%', borderRadius:10, marginLeft:10, marginTop:5}}/>
                </View>
                <FlatList
                data={item.practices}
                renderItem={dataInbox}
                keyExtractor={(item, index) => index}
                horizontal
                showsHorizontalScrollIndicator={false}
                style={{width:'100%', height:'100%'}}
                />
            </View>
        )
    }

    const dataInbox=({item, index, separators})=>{
        var urlImage = item.file_captura;
        var urlProfile = 'https://go.vfa.app/'+item.player_photo;
        return(
            <View style={{width:wp('40%'), height:'95%', backgroundColor:'#FFF', marginHorizontal:10, borderRadius:10, marginTop:10, elevation:3}}>
                 <TouchableOpacity activeOpacity={0.8} onPress={()=> navigation.navigate('Review player',{player:item,token:token,update:(message)=>{updateReview(message)}})}  style={{flex:1}}>
                     <Imagen2 source={{uri:urlImage}} resizeMode="cover" style={{width:wp('40%') ,height: 200, borderRadius:10}} />
                    {/* <View style={{position:'absolute', width:hp('2.5%'), height:hp('2.5'), backgroundColor: 'red', borderRadius:50, right:3, top:3, justifyContent: 'center', alignItems:'center'}}>
                        <Icon name="times" size={hp('1.7%')} />
                    </View> */}
                    <View style={{flex:2, padding:10, flexDirection:'row'}}>
                            <View style={{width:'100%', alignItems:'center', flexDirection:'row'}}>
                                {item.player_photo?
                                    <Imagen2 source={{uri:urlProfile}} resizeMode="cover" style={{width:40, height:40, borderRadius:10}} />
                                :
                                    <View style={{backgroundColor:'rgb(238, 238, 238)', width:40, height:40, borderRadius:10, justifyContent:'center', alignItems:'center' }}>
                                        <Text>{item.player_name.charAt(0)}</Text>
                                    </View>
                                }
                                <Text numberOfLines={1} style={{fontSize:hp('1.5%'), fontWeight:'bold', color:'#000', marginLeft:5}}>{item.player_name}</Text>
                            </View>
                    </View>
                    <View style={{flex:1,padding:10, justifyContent: 'center',flexDirection:'row'}}>
                        <View style={{width:wp('30%'), marginTop:10}}>
                           <View style={{width:wp('15%'), backgroundColor: 'green', elevation:2, justifyContent:'center', alignItems:'center', borderRadius:5}}>
                                <Text numberOfLines={1} style={{fontSize:hp('1.2%'),  color:'#FFF',fontWeight:'bold',}}>{item.name_method} {item.value_method}</Text>
                           </View>
                        </View>
                        <View style={{width:wp('5%'), justifyContent: 'center', alignItems:'center',}}>
                            <Icon name="trophy" size={hp('1.4%')} color="green"/>
                            <Text style={{fontSize:hp('1.4%')}}>{item.qualification}</Text>
                        </View>
                    </View>
                 </TouchableOpacity>
            </View>
        );
    }

    //////// Fin de vista de inbox y todo lo relacionado con ella


    ////// Vista de tareas pendientes y todo lo relacionado con ella

    const Pending = () => {
        return(
            <View style={{width:'100%', height:'100%'}}>
                 <FlatList
                    data={pending}
                    renderItem={dataPending}
                    keyExtractor={(item, index) => index}
                    style={{width:'100%',height:'100%',}}
                    onRefresh={() => getList2(token)}
                    ListEmptyComponent={<EmptyPending/>}
                    refreshing={refresh}
                    />
            </View>
        )
    }

    const EmptyPending=()=>{
        return(
            <View style={{flex:1, height:hp('80%'), justifyContent:'center', alignItems:'center'}}>
                <Icon name="list-alt" size={hp('15%')} color="#000" style={{opacity:0.3}}/>
                <Text style={{fontSize:hp('3%'),fontWeight:'bold', color:'#000', opacity:0.5}}>No pending tasks</Text>
            </View>
        )
    }

    const dataPending=({item, index, separators})=>{
        var urlSkill = 'https://go.vfa.app/preview/'+item.skillone[0].img_preview;
        var imgPlayer = 'https://go.vfa.app/'+item.photo;
        return(
            <View style={{width:'96%',height:hp('13%'),marginHorizontal:10, flexDirection:'row'}}>
                <View style={{flex:1, flexDirection:'row', paddingBottom:10, marginTop:10, backgroundColor: '#FFF', borderRadius:10, justifyContent:'center', alignItems:'center', marginBottom:5, elevation:4}}>
                    <View style={{flex:1, alignItems:'center', flexDirection:'row', paddingLeft:10}}>
                        {item.photo?
                            <Imagen2 source={{uri:imgPlayer}} resizeMode="cover" style={{width:hp('5%'), height:hp('5%'), borderRadius:10}} />
                        :
                            <View style={{backgroundColor:'rgb(238, 238, 238)', width:hp('5%'), height:hp('5%'), borderRadius:10, justifyContent:'center', alignItems:'center' }}>
                                <Text style={{fontSize:hp('1.7%'), fontWeight:'bold'}}>{item.name.charAt(0)}</Text>
                            </View>
                        }
                        <Text numberOfLines={1} style={{fontSize:hp('1.5%'), fontWeight:'bold', color:'#000', marginLeft:10}}>{item.name}</Text>
                    </View>
                    <View style={{backgroundColor:'#D6D6D6', width:'0.2%', height:hp('8%') }}/>
                    <View style={{flex:1, flexDirection:'row', paddingLeft:10, alignItems:'center'}}>
                        <Imagen2 source={{uri:urlSkill}} resizeMode="cover" style={{width:hp('5%'), height:hp('5%'), borderRadius:10}} />
                        <View style={{marginLeft:10,}}>
                            <Text style={{fontSize:hp('1.5%'), fontWeight:'bold'}}>{item.skillone[0].name}</Text>
                            <View style={{backgroundColor:'green', width:wp('15%'), justifyContent:'center', alignItems:'center', borderRadius:5}}>
                                <Text style={{color:'#FFF', fontSize:hp('1.2%')}}>{item.name_method} {item.skillone[0].level_id}</Text>
                            </View>
                        </View>
                    </View>
                    <TouchableOpacity activeOpacity={0.5} onPress={()=>deletePendingTask(item.skillid)} style={{position:'absolute', backgroundColor: 'red', width:30, height:30, borderRadius:50, justifyContent:'center', alignItems:'center', top:-5, right:-5}}>
                        <Icon name="times" size={hp('1.7%')} color="#FFF"/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    //////// Fin de vista de tareas pendientes y todo lo relacionado con ella


    ///// retornando vista de reviews
    return (
        <View style={styles.container}>
            <Imagen2 source={require('../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    position: "absolute",
            }} />
            <TabView
            navigationState={{ index:index, routes }}
            renderScene={SceneMap({
              inbox: Inbox,
              pending: Pending,
            })}
            renderTabBar={(props) =><TabBar { ... props } 
            indicatorStyle = {{  backgroundColor : 'green' }} 
            style = {{ backgroundColor : '#F3F3F3'  }}
            renderLabel={({ route, focused, color }) => (
              <Text style={{color:focused?'green':'#646469', opacity:focused?1:0.5,}}>{route.title}</Text>
            )}
             /> } 
            onIndexChange={(index)=>setIndex(index)}
            initialLayout={{ width: Dimensions.get('screen').width }}
          />
        </View>
    );
};

//// Estilos de la vista
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
});

export default Review;
