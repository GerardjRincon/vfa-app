//import liraries
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Snackbar from 'react-native-snackbar';
import * as Progress from 'react-native-progress';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { LogoutUser, setTeam, setUser } from "../../../storage/user/dataUser";
import { url } from '../../../storage/config';
import Loader from '../global/loader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'react-native-i18n';
// create a component
const Stats = ({navigation}) => {
    const [loader, setLoader] = useState(false)
    const [token, setToken] = useState(null);
    const [califications, setCalifications] = useState(false)
    const [object, setObject] = useState({})
    // stats variables
    const [calificationsTotal, setCalificationsTotal] = useState(0);
    const [calificationsMedia, setCalificationsMedia] = useState(0);
    const [percentage, setPercentage] = useState(0);
    const [percentageTotal, setPercentageTotal] = useState(0);
    const [approved, setApproved] = useState(0);
    const [approvedTotal, setApprovedTotal] = useState(0);
    const [assigned, setAssigned] = useState(0);
    const [assignedTotal, setAssignedTotal] = useState(0);
    useEffect(async() => {
        const unsubscribe = navigation.addListener('focus', async() => {
            const token = await AsyncStorage.getItem('@token');
            const tokenJson = JSON.parse(token);
            console.warn(tokenJson)
            setToken(tokenJson);
            getStats(tokenJson);
        })
        return unsubscribe;
    }, [navigation])

    function suma(number) {
        var real = parseInt(number);
        var length = real.toString().length;
        var cero = "0"; /* String de cero */  
        
        if (number >= 100) {
            return "1"; 
        } else {
            return (cero.repeat(1) +"."+(cero.repeat(2-length))+real); 
            // return length
        }
    }

    async function getStats(tokenJson){
          try {
              await fetch(url+'api/stats',{
              method: 'POST',
              headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer '+tokenJson
              },
          }).then(res => res.json())
          .then((dat) =>{
              console.warn(dat)
              try {
                  if (dat) {
                    setCalifications(true)
                    setObject(dat)
                    // calificadas total
                    var calificadas_total = dat.calificadas_total
                    setCalificationsTotal(calificadas_total);
                    // calificadas media 
                    var calificadas_media = dat.calificadas_media;
                    var cm = suma(calificadas_media)
                    setCalificationsMedia(cm);
                    // porentaje
                    var percentage = dat.porcentaje;
                    var p = suma(percentage);
                    setPercentage(p);
                    // porentaje total
                    var percentage_total = dat.porcetaje_total;
                    setPercentageTotal(percentage_total);
                    // Aprobados
                    var aprobados = dat.aprobados;
                    // var division = dat.aprobados/dat.aprobados_total;
                    // var porc = division*100;
                    // var a = suma(porc);
                    setApproved(aprobados);
                    // Aprobados total
                    var aprobados_total = dat.aprobados_total;
                    setApprovedTotal(aprobados_total);
                    // Asignadas
                    var asignadas = dat.asignadas;
                    setAssigned(asignadas);
                    // Asignadas total
                    var asignadas_total = dat.asignadas_total;
                    setAssignedTotal(asignadas_total);
                  }
                //   if (dat.message == 'Unauthenticated.') {
                //       LogoutUser()
                //       setTimeout(()=>{
                //           navigation.replace('Login')
                //       },1)
                //   }
              } catch (error) {
                  Snackbar.show({
                      text: error,
                      duration: Snackbar.LENGTH_LONG,
                  });
              }
          })
              
          } catch (error) {
              Snackbar.show({
                  text: error,
                  duration: Snackbar.LENGTH_LONG,
              });
          }
      }

    return (
        <View style={styles.container}>
            {loader?<Loader/>:null}
            <Image source={require('../../../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'),
                position: "absolute",
            }} />
            {califications?
            <ScrollView style={{paddingTop:20,}}>
                <View style={{flex:1, height:hp('15%')}}>
                    <Image source={require('../../../assets/iconos/001-ftbol-americano.png')} resizeMode="cover" style={{width:40, height:40, marginLeft:10, marginBottom:5}}/>
                    <Text style={{fontSize:hp('1.7%'), paddingBottom:10, paddingLeft:10}}>Team average score</Text>
                    <View style={{justifyContent:'center', alignItems:'center'}}>
                        <Text style={{position:'absolute', zIndex: 10, fontSize:hp('1.5'), fontWeight:'bold'}}>{object.calificadas_media}pts</Text>
                        <Progress.Bar progress={calificationsMedia} width={wp('95%')} height={hp('3%')} color="green"/>
                    </View>
                    <View style={{flexDirection:'row', width:wp('100%'), justifyContent:'space-around', marginTop:5}}>
                        <Text style={{fontSize:hp('1.5%')}}>0</Text>
                        <Text style={{fontSize:hp('1.5%')}}>PROGRESS</Text>
                        <Text style={{fontSize:hp('1.5%')}}>{calificationsTotal}pts</Text>
                    </View>
                </View>
                <View style={{flex:1, height:hp('15%'), marginBottom:10, }}>
                    <Image source={require('../../../assets/iconos/003-zapatillas-de-deporte.png')} resizeMode="cover" style={{width:40, height:40, marginLeft:10, marginBottom:5}}/>
                    <Text style={{fontSize:hp('1.7%'), paddingBottom:10,  paddingLeft:10}}>Team average percentage</Text>
                    <View style={{justifyContent:'center', alignItems:'center'}}>
                        <Text style={{position:'absolute', zIndex: 10, fontSize:hp('1.5'), fontWeight:'bold'}}>{object.porcentaje}%</Text>
                        <Progress.Bar progress={percentage} width={wp('95%')} height={hp('3%')} color="green"/>
                    </View>
                    <View style={{flexDirection:'row', width:wp('100%'), justifyContent:'space-around', marginTop:5}}>
                        <Text style={{fontSize:hp('1.5%')}}>0</Text>
                        <Text style={{fontSize:hp('1.5%')}}>PROGRESS</Text>
                        <Text style={{fontSize:hp('1.5%')}}>{percentageTotal}%</Text>
                    </View>
                </View>
                <View style={{flex:1, height:hp('15%'), marginTop:10, paddingHorizontal:20,}}>
                    <Image source={require('../../../assets/iconos/002-copa-de-ftbol.png')} resizeMode="cover" style={{width:40, height:40, marginBottom:5}}/>
                    <View style={{flex:1, flexDirection:'row', justifyContent:'space-between'}}>
                        <Text style={{fontSize:hp('1.7%'), paddingBottom:10,}}>Team mastered skills</Text>
                        <Text style={{fontSize:hp('2%'), fontWeight:'bold', color:'green'}}>
                            {assigned}/{assignedTotal}
                        </Text>
                    </View>
                </View>
                <View style={{flex:1, height:hp('15%'), paddingHorizontal:20, }}>
                    <Image source={require('../../../assets/iconos/004-jugador-de-ftbol.png')} resizeMode="cover" style={{width:40, height:40, marginBottom:5}}/>
                    <View style={{flex:1, flexDirection:'row', justifyContent:'space-between'}}>
                        <Text style={{fontSize:hp('1.7%'), paddingBottom:10,}}>Team mastered skills</Text>
                        <Text style={{fontSize:hp('2%'), fontWeight:'bold', color:'green'}}>
                            {approved}/{approvedTotal}
                        </Text>
                    </View>
                </View>
            </ScrollView>
            :
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Icon name="poll" size={hp('15%')} color="#000" style={{ opacity: 0.3 }} />
                <Text style={{ fontSize: hp('3%'), fontWeight: 'bold', color: '#000', opacity: 0.3 }}>No statistics</Text>
            </View>
            }
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F3F3F3',
    },
});

//make this component available to the app
export default Stats;
