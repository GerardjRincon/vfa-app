//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity } from 'react-native';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { url } from '../../../storage/config';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


// create a component
const ViewPlayer = ({navigation, route}) => {
    const { skill, team } = route.params;
    const [user, setUser] = useState({});
    const [token, setToken] = useState(null);
    const [refresh, setRefresh] = useState(false);
    const [data, setData] = useState([]);

    useEffect(async() => {
        const user = await AsyncStorage.getItem('@user');
        const token = await AsyncStorage.getItem('@token');
        const userJson = JSON.parse(user);
        const tokenJson = JSON.parse(token);
        setUser(userJson);
        setToken(tokenJson);
        players(tokenJson);
    }, [])

    useLayoutEffect(() => {
        navigation.setOptions({
            title: skill.name,
        });
    }, [])

    async function players(tokenJson) {
        setRefresh(true)
        try {
          await fetch(url+'api/skills_destacados_view',{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+tokenJson
            },
            body: JSON.stringify({
                id: skill.id,
              }),
            }).then(res => res.json())
            .then((dat) =>{
                console.warn(dat.players);
              if (dat) {
                setData(dat.players) 
                setRefresh(false)
              }else{
                setData([]) 
                setRefresh(false)
              }
            }).catch(err=>{
              setRefresh(false)
            });
        } catch (error) {
          setRefresh(false)
        }
    }

    const RenderItem=({item, index, separators})=>{
    const imageMyTeam = 'https://go.vfa.app'+item.photo;
    return(
            <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('ProfilePublic', {player:item,token:token, team})} style={{width:wp('100%'), height:hp('10%'), flexDirection:'row',backgroundColor: "#FFF",  borderBottomWidth:1, borderColor:'#D6D6D6' }}>
                <View style={{flex:1, marginLeft:20, justifyContent:'center',}}>
                    {item.photo?
                        <Image source={{uri:imageMyTeam}} resizeMode="cover" style={{width:hp('8%'),height: hp('8%'), borderRadius:10}} />
                    :
                        <View style={{backgroundColor:'#D6D6D6', width:hp('8%'),height: hp('8%'), borderRadius:10, justifyContent:'center', alignItems:'center'}}>
                            <Text style={{fontSize:hp('2%'), fontWeight:'bold', }}>{((item.name || "").charAt(0) || "").toUpperCase()}</Text>
                        </View>
                    }
                </View>
                <View style={{flex:4, flexDirection:'row',  justifyContent:'center', alignItems:'center',}}>
                    <View style={{flex:3}}>
                        <Text style={{fontSize:hp('1.7%'), fontWeight:'bold', textTransform:'capitalize'}}>{item.name}</Text>
                        <Text style={{fontSize:hp('1.4%'),opacity:0.4, textTransform:'capitalize'}}>{item.username}</Text>
                    </View>
                    <View style={{flex:1, justifyContent:'center', flexDirection:'row', alignItems:'center', marginRight:20, width:hp('5%'),height:hp('5%'),}}>
                    <View style={{width:hp('2%'), marginRight:8, height:hp('2%'), backgroundColor: '#D8D8D8', borderRadius:hp('2%'), justifyContent:'center', alignItems:'center'}}>
                        <Icon2 name="flash" size={hp('1.5%')} color="#FFAF20" />
                    </View>
                    <Text style={{fontSize:hp('1.7%'), fontWeight:'bold', color:'#000'}}>{item.points} pts</Text>
                    </View>
                </View>
            </TouchableOpacity>
    );
    }

    return (
        <View style={styles.container}>
            <Image source={require('../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    position: "absolute",
            }} />
            <View style={{flex:1}}>
                <FlatList
                    data={data}
                    renderItem={RenderItem}
                    keyExtractor={(item, index) => index}
                    style={{ flex: 1,}}
                    onRefresh={() => players(token)}
                    refreshing={refresh}
                />
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
});

//make this component available to the app
export default ViewPlayer;
