import React, {useState, useEffect, useLayoutEffect} from 'react'
import { Text, View, StyleSheet, FlatList, Image, TouchableOpacity } from 'react-native'
import { LogoutUser, setTeam } from "../../../storage/user/dataUser";
import { url } from '../../../storage/config';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import { SvgUri } from 'react-native-svg';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'i18n-js';

const Ranking = ({ navigation }) => {

    const [data, setData] = useState([]);
    const [user, setUser] = useState({});
    const [token, setToken] = useState(null);
    const [team, setTeam] = useState({});
    const [coach, setCoach] = useState({})
    const [refresh, setRefresh] = useState(false);
    const [svg, setSvg] = useState(null);
    const [textEmptyList, setTextEmptyList] = useState(I18n.t('tex6'))
    const [filtro, setFiltro] = useState(0); // 0 defaul "Global ranking" - 1 "Best of pais" - 2 "Top skills"



    useEffect(async() => {
     
          const unsubscribe = navigation.addListener('focus', async() => {
            const user = await AsyncStorage.getItem('@user');
            const token = await AsyncStorage.getItem('@token');
            const team = await AsyncStorage.getItem('@team');
            const coach = await AsyncStorage.getItem('@coach');
            const userJson = JSON.parse(user);
            const tokenJson = JSON.parse(token);
            const teamJson = JSON.parse(team);
            const coachJson = JSON.parse(coach);
            setCoach(coachJson);
            setUser(userJson);
            setToken(tokenJson);
            console.warn(userJson.pais)
            setTeam(teamJson);
            setFiltro(0);
            if (userJson) {
              setSvg("https://cdn.ipwhois.io//flags//"+userJson.pais+".svg");
            }
            if (tokenJson) {
              // getMyInfo(tokenJson);
              globalRanking(tokenJson);
            }
          })

          
        return () => { 
          unsubscribe;
        }


    }, [navigation])



    async function getMyInfo(tokenJson){
        setRefresh(true)
      try {
          await fetch(url+'api/team/ranking/players',{
          method: 'POST',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '+tokenJson
          },
          }).then(res => res.json())
          .then((dat) =>{
            console.log(dat)
              try {
                  if (dat.ok) {
                    setData(dat.data) 
                    setRefresh(false)
                  }
                //   if (dat.message == 'Unauthenticated.') {
                //       LogoutUser()
                //       setTimeout(()=>{
                //           navigation.replace('Login')
                //       },1)
                //   }
              } catch (error) {
                setRefresh(false)
                  Snackbar.show({
                      text: error,
                      duration: Snackbar.LENGTH_LONG,
                  });
              }
          })
              
          } catch (error) {
            setRefresh(false)
              Snackbar.show({
                  text: error,
                  duration: Snackbar.LENGTH_LONG,
              });
          }
    }

    async function globalRanking(tokenJson) {
      setRefresh(true)
      try {
        await fetch(url+'api/jugadores_destacados/all',{
          method: 'POST',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '+tokenJson
          },
          }).then(res => res.json())
          .then((dat) =>{
            if (dat) {
              console.warn(dat);
              const array = dat;
              setData(array) 
              setRefresh(false)
            }else{
              setTextEmptyList(I18n.t('tex6'))
              setData(dat) 
              setRefresh(false)
            }
          }).catch(err=>{
            setRefresh(false)
          });
      } catch (error) {
        setRefresh(false)
      }
    }

    async function rankingPaises(tokenJson) {
      setRefresh(true)
      try {
        await fetch(url+'api/jugadores_destacados_paises',{
          method: 'POST',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '+tokenJson
          },
          body: JSON.stringify({
            countryCode: user.pais.toUpperCase(),
          }),
          }).then(res => res.json())
          .then((dat) =>{
            // console.warn(dat)
            if (dat) {
              const array = dat;
              setData(array) 
              setRefresh(false)
            }else{
              setData(dat);
              setTextEmptyList('There are no players from your country')
              setRefresh(false);
            }
          }).catch(err=>{
            setRefresh(false);
          });
      } catch (error) {
        setRefresh(false);
      }
    }

    async function rankingSkill(tokenJson) {
      setRefresh(true)
      try {
        await fetch(url+'api/skills_destacados',{
          method: 'POST',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '+tokenJson
          },
          }).then(res => res.json())
          .then((dat) =>{
            console.warn(dat)
            if (dat) {
              const array = ordenar(dat, 'players')
              setData(array) 
              setRefresh(false)
            }else{
              setData(dat);
              setRefresh(false);
            }
          }).catch(err=>{
            setRefresh(false);
          });
      } catch (error) {
        setRefresh(false);
      }
    }






  function setNumber(num) {
    num = num.toString().replace(/[^0-9.]/g, '');
    if (num < 1000) {
       return num;
    }
    let si = [{
          v: 1E3,
          s: "K"
       },
       {
          v: 1E6,
          s: "M"
       },
       {
          v: 1E9,
          s: "B"
       },
       {
          v: 1E12,
          s: "T"
       },
       {
          v: 1E15,
          s: "P"
       },
       {
          v: 1E18,
          s: "E"
       }
    ];
    let index;
    for (index = si.length - 1; index > 0; index--) {
       if (num >= si[index].v) {
          break;
       }
    }
    return (num / si[index].v).toFixed(2).replace(/\.0+$|(\.[0-9]*[1-9])0+$/, "$1") + si[index].s;
 }


  // alert("aqui")

 

    function ordenar(array,param){ //ordenar alfabeticamente
      array.sort( (a, b) => {
        let asingle= a[param].toString().replace(/[Áá]/gi,"a").replace(/[Éé]/gi,"e").replace(/[Íí]/gi,"i")
        .replace(/[Óó]/gi,"o").replace(/[Úú]/gi,"u").replace(/[Ññ]/gi,"nzz").toLowerCase();
        let bsingle = b[param].toString().replace(/[Áá]/gi,"a").replace(/[Éé]/gi,"e").replace(/[Íí]/gi,"i")
        .replace(/[Óó]/gi,"o").replace(/[Úú]/gi,"u").replace(/[Ññ]/gi,"nzz").toLowerCase();
        if (asingle > bsingle) return -1;
        if (asingle < bsingle) return 1;
        return 0;
      });
      return array;
    }

    const EmptyList = () => { // Render que se muestra cuando la lista esta vacia 
      return(
          <View style={{flex:1,height:hp('70%'),justifyContent:'center', alignItems:'center'}}>
              <Icon2 name="account-alert" size={hp('20%')} color="#000" style={{opacity:0.4}}/>
              <Text style={{fontSize:hp('2%'), opacity:0.7}}>{textEmptyList}</Text>
          </View>
      )
    }

    const RenderItem=({item, index, separators})=>{
      const imageMyTeam = 'https://go.vfa.app'+item.photo;
      const urlImage = 'https://go.vfa.app/preview/' + item.img_preview;
      return(
          filtro==0 || filtro == 1?
            <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('ProfilePublic', {player:item,token:token, team:team})} style={{width:wp('100%'), height:hp('10%'), flexDirection:'row',backgroundColor: "#FFF",  borderBottomWidth:1, borderColor:'#D6D6D6' }}>
                <View style={{flex:1, marginLeft:20, justifyContent:'center',}}>
                    {item.photo?
                        <Image source={{uri:imageMyTeam}} resizeMode="cover" style={{width:hp('8%'),height: hp('8%'), borderRadius:50}} />
                    :
                        <View style={{backgroundColor:'#D6D6D6', width:hp('8%'),height: hp('8%'), borderRadius:50, justifyContent:'center', alignItems:'center'}}>
                            <Text style={{fontSize:hp('3%'), fontWeight:'bold', }}>{((item.name || "").charAt(0) || "").toUpperCase()}</Text>
                        </View>
                    }
                </View>
                <View style={{flex:4, flexDirection:'row',  justifyContent:'center', alignItems:'center',}}>
                    <View style={{flex:3}}>
                        <Text style={{fontSize:hp('1.8%'), fontWeight:'bold', textTransform:'capitalize'}}>{item.name}</Text>
                        <Text style={{fontSize:hp('1.4%'),opacity:0.6,}}>{item.username}</Text>
                        <Text style={{color:"#3d3d3d", fontWeight:'bold',opacity:0.6}}>{item.name_method} {item.value_method}</Text>
                    </View>
                    <View style={{flex:1, justifyContent:'center', flexDirection:'row', alignItems:'center', marginRight:20, width:hp('5%'),height:hp('5%'),}}>
                      <View style={{width:hp('2%'), marginRight:8, height:hp('2%'), backgroundColor: '#D8D8D8', borderRadius:hp('5%'), justifyContent:'center', alignItems:'center'}}>
                          <Icon2 name="flash" size={hp('1.5%')} color="#FFAF20" />
                      </View>
                      <Text style={{fontSize:hp('1.7%'), fontWeight:'bold', color:'#000'}}>{setNumber(parseInt(item.points))} pts</Text>
                    </View>
                </View>
            </TouchableOpacity>
          :
            <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('Featured Players', {skill:item, team:team})} style={{width:wp('100%'), height:hp('10%'), flexDirection:'row',backgroundColor: "#FFF",  borderBottomWidth:1, borderColor:'#D6D6D6' }}>
                <View style={{flex:1, marginLeft:20, justifyContent:'center',}}>
                    {item.img_preview?
                        <Image source={{uri:urlImage}} resizeMode="cover" style={{width:hp('8%'),height: hp('8%'), borderRadius:50}} />
                    :
                        <View style={{backgroundColor:'#D6D6D6', width:hp('8%'),height: hp('8%'), borderRadius:10, justifyContent:'center', alignItems:'center'}}>
                            <Text style={{fontSize:hp('3%'), fontWeight:'bold', }}>{((item.name || "").charAt(0) || "").toUpperCase()}</Text>
                        </View>
                    }
                </View>
                <View style={{flex:4, flexDirection:'row',  justifyContent:'center', alignItems:'center',}}>
                    <View style={{flex:3}}>
                        <Text style={{fontSize:hp('1.7%'), fontWeight:'bold', textTransform:'capitalize'}}>{item.name}</Text>
                        <Text style={{fontSize:hp('1.4%'),opacity:0.4, textTransform:'capitalize'}}>{item.name_category}</Text>
                    </View>
                    <View style={{flex:1, justifyContent:'center', flexDirection:'row', alignItems:'center', marginRight:20, width:hp('5%'),height:hp('5%'),}}>
                      <View style={{width:hp('4%'), marginRight:8, height:hp('4%'), borderRadius:hp('5%'), justifyContent:'center', alignItems:'center'}}>
                          <Icon2 name="account-group" size={hp('3%')} color="#FFAF20" />
                      </View>
                      <Text style={{fontSize:hp('1.7%'), fontWeight:'bold', color:'#000'}}>{item.players}</Text>
                    </View>
                </View>
            </TouchableOpacity>
      );
    }

    return (
      <View style={styles.center}>
        <Image source={require('../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    position: "absolute",
            }} />
            <View style={{flexDirection:'row', width:wp('100%'), height:hp('10%'), backgroundColor: '#FFF', borderBottomWidth:1, borderColor:'#D6D6D6', justifyContent:'space-evenly', alignItems:'center'}}>
                <TouchableOpacity activeOpacity={0.9} onPress={()=>{setFiltro(0); globalRanking(token)}} style={{flexDirection:'row', backgroundColor: '#FFF', elevation:3, width:'30%', height:'50%', borderRadius:10, justifyContent:'space-evenly', alignItems:'center'}}>
                    <View style={{justifyContent:'center', alignItems:'center', width:hp('2%'), height:hp('2%'), borderRadius:hp('2%'), borderWidth:1, borderColor:'#9A9A9A',}}>
                      <Icon name="circle" size={hp('1.5%')} color={filtro == 0 ?"#11A10F":"#FFF"}/>
                    </View>
                    <View>
                      <Text>
                         {I18n.t('ranking.text1')}
                      </Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.9} onPress={()=>{setFiltro(1); rankingPaises(token)}} style={{flexDirection:'row', backgroundColor: '#FFF', elevation:3, width:'30%', height:'50%', borderRadius:10, justifyContent:'space-evenly', alignItems:'center'}}>
                    <View style={{justifyContent:'center', alignItems:'center', width:hp('2%'), height:hp('2%'), borderRadius:hp('2%'), borderWidth:1, borderColor:'#9A9A9A',}}>
                      <Icon name="circle" size={hp('1.5%')} color={filtro == 1?"#11A10F":"#FFF"}/>
                    </View>
                    <View style={{flexDirection:'row', justifyContent:'space-evenly', alignItems:'center'}}>
                      <Text style={{paddingRight:10}}>
                      {I18n.t('ranking.text2')}
                      </Text>
                      <SvgUri
                        width="25"
                        height="25"
                        fill="#000"
                        viewBox={user.pais == 've'? "0 0 130 100" : "0 0 640 480"}
                        uri={svg}
                      />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.9} onPress={()=>{setFiltro(2); rankingSkill(token)}} style={{flexDirection:'row', backgroundColor: '#FFF', elevation:3, width:'30%', height:'50%', borderRadius:10, justifyContent:'space-evenly', alignItems:'center'}}>
                    <View style={{justifyContent:'center', alignItems:'center', width:hp('2%'), height:hp('2%'), borderRadius:hp('2%'), borderWidth:1, borderColor:'#9A9A9A',}}>
                      <Icon name="circle" size={hp('1.5%')} color={filtro == 2 ?"#11A10F":"#FFF"}/>
                    </View>
                    <View>
                      <Text>
                         {I18n.t('ranking.text3')}
                      </Text>
                    </View>
                </TouchableOpacity>
            </View>
            <FlatList
            data={data}
            renderItem={RenderItem}
            keyExtractor={(item, index) => index}
            style={{ flex: 1,}}
            onRefresh={() => {
              if (filtro==0) {
                globalRanking(token)
              }
              if (filtro==1) {
                rankingPaises(token)
              }
              if (filtro==2) {
                rankingSkill(token)
              }}}
            refreshing={refresh}
            ListEmptyComponent={EmptyList}
            />
        
      </View>
    );
  };

  const styles = StyleSheet.create({
    center: {
      flex: 1,
      backgroundColor: '#F3F3F3',
    },
  });
  
  export default Ranking;