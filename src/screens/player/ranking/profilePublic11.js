//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Snackbar from 'react-native-snackbar';
import { LogoutUser } from "../../../storage/user/dataUser";
import { url } from '../../../storage/config';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import { SvgUri } from 'react-native-svg';
import * as Progress from 'react-native-progress';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

// create a component
const ProfilePublic = ({ navigation, route }) => {
    const { token, team } = route.params;
    const [player, setPlayer] = useState(route.params.player);
    const [svg, setSvg] = useState(null);
    const imagePlayer = 'https://go.vfa.app' + player.photo;
    const [data, setData] = useState([]);
    const [refresh, setRefresh] = useState(false)
    const [porcentaje, setPorcentaje] = useState(0)

    useEffect(() => {
        getMyVideos()
    }, [])

    useLayoutEffect(() => {
        navigation.setOptions({
            title: player.name,
        });
    }, [])

    function suma(number) {
        var real = parseInt(number);
        var length = real.toString().length;
        var cero = "0"; /* String de cero */  
        
        if (number >= 100) {
            return "1"; 
        } else {
            return (cero.repeat(1) +"."+(cero.repeat(2-length))+real); 
            // return length
        }
      }

    async function getMyVideos() {
        setRefresh(true)
        try {
            await fetch(url + 'api/player/find/public', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    id: player.id,
                }),
            }).then(res => res.json())
                .then((dat) => {
                    console.log(dat.data)
                    try {
                        setRefresh(false)
                        // console.warn(dat.data.videos)
                        setPlayer(dat.data);
                        setSvg('https://go.vfa.app/'+dat.data.avatar_svg);
                        setData(dat.data.videos)
                        var num = suma(dat.data.videos.length<1?0:dat.data.videos.length)
                        setPorcentaje(num)
                        // if (dat.message == 'Unauthenticated.') {
                        //     LogoutUser()
                        //     setTimeout(()=>{
                        //         navigation.replace('Login')
                        //     },1000)
                        // }
                    } catch (error) {
                        setRefresh(false)
                        Snackbar.show({
                            text: error,
                            duration: Snackbar.LENGTH_LONG,
                        });
                    }
                })

        } catch (error) {
            setRefresh(false)
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
            });
        }
    }

    function mensaje(mensaje) {
        Snackbar.show({
            text: mensaje,
            duration: Snackbar.LENGTH_LONG,
        });
    }


    const RenderItem = ({ item, index, separators }) => {
        var urlImage = item.file_captura;
        return (
            <View style={{ width:wp('29%'), height: '95%', backgroundColor: '#FFF', marginHorizontal: 10, borderRadius: 10, marginTop: 10, elevation: 3 }}>
                <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate('Video Player', { player: player, token: token, update: (message) => { mensaje(message) }, video: item })} style={{ flex: 1 }}>
                    <View style={{ alignItems: 'center', justifyContent: 'center', }}>
                        <Image source={{ uri: urlImage }} resizeMode="stretch" style={{width:wp('29%') ,height: hp('25%'),}} />
                        <View style={{ position: 'absolute', width: hp('10%'), height: hp('10%'), backgroundColor: 'rgba(0,0,0,0.3)', borderRadius: 50, borderColor: '#D6D6D6', borderWidth: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name="play" size={hp('4')} color="#FFF" style={{ marginLeft: 10 }} />
                        </View>
                    </View>
                    {/* <View style={{ flex: 1, padding: 10, flexDirection: 'row', }}>
                        <View style={{ width: '100%', alignItems: 'flex-start'}}>
                            <Text numberOfLines={1} style={{ fontSize: hp('1.5%'), fontWeight: 'bold', color: '#000', marginLeft: 5 }}>{item.skill_name}</Text>
                            <Text numberOfLines={1} style={{ fontSize: hp('1.3%'), opacity:0.5,  color: '#000', marginLeft: 5 }}>{item.category}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, padding: 10, justifyContent: 'center', flexDirection: 'row' }}>
                        <View style={{ width: wp('30%'), marginTop: 10 }}>
                            <View style={{ width: wp('15%'), backgroundColor: 'green', elevation: 2, justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                                <Text numberOfLines={1} style={{ fontSize: hp('1.2%'), color: '#FFF', fontWeight: 'bold', }}>{item.level}</Text>
                            </View>
                        </View>
                        {item.qualification?
                        <View style={{ width: wp('5%'), justifyContent: 'center', alignItems: 'center', flexDirection:'row'}}>
                            <Icon name="trophy" size={hp('1.4%')} color="#EAB900" />
                            <Text style={{ fontSize: hp('1.4%'), marginLeft:5, color: "#000" }}>{item.qualification}</Text>
                        </View>
                        :null}
                    </View> */}
                </TouchableOpacity>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            {/* <Image source={require('../../../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'),
                position: "absolute",
            }} /> */}
            <View style={{ flex: 1, flexDirection: 'row', justifyContent:'center', alignItems:'center' }}>
                <View style={{marginLeft: 20, marginRight:20, justifyContent: 'center', }}>
                    {player.photo ?
                        <Image source={{ uri: imagePlayer }} resizeMode="cover" style={{ width: hp('14%'), height: hp('14%'), borderRadius: 50 }} />
                        :
                        <View style={{ backgroundColor: '#D6D6D6', width: hp('14%'), height: hp('14%'), borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: hp('4%'), fontWeight: 'bold', }}>{player.name.charAt(0)}</Text>
                        </View>
                    }
                </View>
                <View style={{ flex: 1, height:'60%', justifyContent: 'flex-start', alignItems:'flex-start', marginLeft:20, marginRight: 20, }}>
                    <View style={{justifyContent:'flex-start', alignItems:'flex-start'}}>
                        <Text numberOfLines={2} style={{ fontSize: hp('2%'), color: '#000', marginTop: 10, textTransform:'capitalize' }}>{player.name}</Text>
                        <Text numberOfLines={2} style={{ fontSize: hp('1.6%'), color: '#000', marginBottom:10, textTransform:'capitalize' }}>{player.username}</Text>
                        {/* <Text numberOfLines={2} style={{ fontSize: hp('1.6%'), color: '#000', opacity: 0.5, marginTop: 5 }}>{team.name}</Text> */}
                        {/* <View style={{width:player.name_method === 'course'?wp('40%'):wp('20%'), paddingHorizontal:20, paddingVertical:5, backgroundColor: 'green', marginTop: 10, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }}>
                        </View> */}
                        {/* <Text numberOfLines={1} style={{ fontSize: hp('1.7%'), color:'#000', textTransform:'capitalize' }}>{player.name_method}: {player.value_method}</Text> */}
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        {svg?
                            <SvgUri
                                width="25"
                                height="25"
                                viewBox="0 0 1200 1200"
                                uri={svg}
                            />
                        :null}
                        <Text style={{paddingLeft:10}}>{player.rango}</Text>
                    </View>
                    <View style={{position:'absolute', bottom:0, right:10}}>
                        {player.pais?
                            <SvgUri
                                width="40"
                                height="40"
                                viewBox={player.pais == 've'? "0 0 130 100" : "0 0 640 480"}
                                uri={"https://cdn.ipwhois.io//flags//"+player.pais+".svg"}
                            />
                        :null}
                    </View>
                </View>
            </View>
            <View style={{ height:hp('3%') }}>
                <View style={{justifyContent:'center', alignItems:'center'}}>
                      <Text style={{position:'absolute', zIndex: 10, fontSize:hp('1.5'), fontWeight:'bold'}}>{data.length<1?"0":data.length}%</Text>
                      <Progress.Bar progress={porcentaje} width={wp('90%')} height={hp('2%')} borderRadius={10} color="green"/>
                </View>
                {/* <View style={{flexDirection:'row', width:wp('100%'), justifyContent:'space-around', marginTop:5}}>
                    <Text style={{fontSize:hp('1.5%')}}>1</Text>
                    <Text style={{fontSize:hp('1.5%')}}>PROGRESS</Text>
                    <Text style={{fontSize:hp('1.5%')}}>100%</Text>
                </View> */}
            </View>
            <View style={{ flex: 3, }}>
            <View style={{flexDirection:'row', justifyContent:'space-between', width:wp('100%'), paddingRight:20, marginTop:10, marginBottom:10, paddingLeft:20, height:hp('4%'), alignItems:'center'}}>
                    {/* {team?
                        <Text style={{fontSize:hp('1.7%')}}>My Videos</Text>
                    :
                        <Text style={{fontSize:hp('1.5%'), opacity:0.8}}>You do not have any assigned work method</Text>
                    } */}
                    {/* <TouchableOpacity style={{width:wp('10%'), height:'100%', justifyContent:'center', alignItems:'center'}}>
                        <Icon name="ellipsis-h" size={hp('3%')}/>
                    </TouchableOpacity> */}
                    <View style={{flex:1, flexDirection:'row',  alignItems:'center'}}>
                      <View style={{width:hp('4%'), height:hp('4%'), marginRight:5, borderRadius:hp('4%'), justifyContent:'center', alignItems:'center', backgroundColor: "#ABABAB",}}> 
                        <Icon name="star" color="#535353" size={hp('3%')}/>
                      </View>
                      <Text style={{color:'#535353', fontSize:hp('2%'), fontWeight:'bold'}}>{player.starts}</Text>
                    </View>
                    <View style={{flex:1, flexDirection:'row',  alignItems:'center'}}>
                      <View style={{width:hp('4%'), height:hp('4%'), marginRight:5, borderRadius:hp('4%'), justifyContent:'center', alignItems:'center', backgroundColor: "#ABABAB",}}> 
                        <Icon2 name="video" color="#535353" size={hp('3%')}/>
                      </View>
                      <Text style={{color:'#535353', fontSize:hp('2%'), fontWeight:'bold'}}>{data.length} Videos</Text>
                    </View>
                    <View style={{flex:1, flexDirection:'row',  alignItems:'center'}}>
                      <View style={{width:hp('4%'), height:hp('4%'), marginRight:5, borderRadius:hp('4%'), justifyContent:'center', alignItems:'center', backgroundColor: "#ABABAB",}}> 
                        <Icon2 name="flash" color="#535353" size={hp('3%')}/>
                      </View>
                      <Text style={{color:'#535353', fontSize:hp('2%'), fontWeight:'bold'}}>{player.points} points</Text>
                    </View>
              </View>
                <FlatList
                    data={data}
                    renderItem={RenderItem}
                    keyExtractor={(item, index) => index}
                    numColumns={3}
                    style={{width: wp('100%'), flex: 1 ,}}
                    onRefresh={() => getMyVideos()}
                    refreshing={refresh}
                />
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
});

//make this component available to the app
export default ProfilePublic;
