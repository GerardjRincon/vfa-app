//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View,Alert, useWindowDimensions, Text, StyleSheet, TouchableOpacity, Image, Modal, FlatList, Platform, PermissionsAndroid, Dimensions, TextInput, KeyboardAvoidingView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Block,Button as GaButton } from 'galio-framework'
import Snackbar from 'react-native-snackbar';
import { LogoutUser } from "../../../storage/user/dataUser";
import { url } from '../../../storage/config';
import Select2 from "react-select2-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import * as Progress from 'react-native-progress';
import I18n from 'react-native-i18n';
import Share from 'react-native-share';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
  } from 'react-native-popup-menu';
  import { SvgUri } from 'react-native-svg';


import { TabView, SceneMap,TabBar} from 'react-native-tab-view';




  
const dataLevel = [
    {id:1, name:'Level 1'},
    {id:2, name:'Level 2'},
    {id:3, name:'Level 3'},
    {id:4, name:'Level 4'},
    {id:5, name:'Level 5'},
    {id:6, name:'Level 6'},
    {id:7, name:'Level 7'},
    {id:8, name:'Level 8'},
    {id:9, name:'Level 9'},
    {id:10, name:'Level 10'},
    {id:11, name:'Level 11'},
    {id:12, name:'Level 12'},
    {id:13, name:'Level 13'},
    {id:14, name:'Level 14'},
    {id:15, name:'Level 15'},
    {id:16, name:'Level 16'},
    {id:17, name:'Level 17'},
    {id:18, name:'Level 18'},
    {id:19, name:'Level 19'},
    {id:20, name:'Level 20'},
    {id:21, name:'Level 21'},
    {id:22, name:'Level 22'},
    {id:23, name:'Level 23'},
    {id:24, name:'Level 24'},
    {id:25, name:'Level 25'},
];

const dataCategory=[
    {id:1,name:'SPEED OF FOOTWORK - WARMING UP'},
    {id:2,name:'BALL CONTROL'},
    {id:3,name:'KICKING TECHNIQUE / PASSING'},
    {id:4,name:'CUTBACK'},
    {id:5,name:'TURN AWAY'},
    {id:6,name:'RECEIVE AND PROCEED'},
    {id:7,name:'THROW IN'},
    {id:8,name:'HEADING'},
    {id:9,name:'SLIDING TACKLE'},
    {id:10,name:'SKILLS TO BEAT AN OPPONENT'},
    {id:11,name:'DOUBLE SKILLS TO BEAT AN OPPONENT'},
    {id:12,name:'SKILLS TO BEAT AN OPPONENT IN YOUR BACK'},

]

const dataCourse=[
    {id:1,name:'BASIC COURSE'},
    {id:2,name:'MASTER THE BALL COURSE'},
    {id:3,name:'MASTER THE OPPONENT COURSE'},
    {id:4,name:'BEAT THE OPPONENT COURSE'},
    {id:5,name:'PERFECTION COURSE'},

]
// create a component


  const ProfilePublic = ({ navigation, route }) => {



    const [index, setIndex] = useState(0);

    const [routes] = useState([ //// Rutas del tabView
        { key: 'videos', title: 'WALL' },
      { key: 'homework', title: 'HOMEWORK' },
      
  ]);



    const { player, token, team } = route.params;
    const imagePlayer = 'https://go.vfa.app' + player.photo;
    const [data, setData] = useState([]);
    const [refresh, setRefresh] = useState(false);
    const [visible, setVisible] = useState(false);
    const [workingLevel] = useState([
        {id:'level',name:'By Levels'},
        {id:'category',name:'By Categories'},
        {id:'course',name:'By Course'}
    ]);
    const [Levels] = useState(dataLevel);
    const [Categories] = useState(dataCategory);
    const [Courses] = useState(dataCourse);
    const [valiWorking,setValiWorking] = useState('');
    const [workingValue, setWorkingValue] = useState('');
    const [porcentajeHomework, setPorcentajeHomework] = useState(0)
    const [porcentaje, setPorcentaje] = useState(0)
    const [user, setUser] = useState(player);
    const [dataHomework, setDataHomework] = useState([]);

    const [svg, setSvg] = useState(url+'/Rangos/rango.png');


    useEffect(() => {
        // getMyVideos()
        // getVideosHomework();
        getVideosHomework(token, user);
    }, [])


    useLayoutEffect(() => {
        navigation.setOptions({
            title: player.name,
        });
    }, [])



    async function getVideosHomework(tokenJson, user){

        try{
          const datos = {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+tokenJson
            },
            body: JSON.stringify({
              id: user.id,
            })
          }
  
          await fetch(url+'api/player/find/videos', datos)
          .then(response => response.json())
          .then((res) => {
           
          // VIDEOS PUBLICOS 
            // setData(res.videosPublic) 
            // var num = suma(res.videosPublic.length)
            // setPorcentaje(num)
  
  
            // // VIDEOS PRIVADOS 
            // setDataHomework(res.videos);
            // var num2 = suma(res.videos.length)
            // setPorcentajeHomework(num2)


            setData(res.videosPublic) 
            var num = suma(res.porcentaje)
            setPorcentaje(num)

            console.log('porcentaje 1', porcentaje)


          // VIDEOS PRIVADOS 
           setDataHomework(res.videos);
          // var num2 = res.porcentaje;
           setPorcentajeHomework(res.porcentaje)

          console.log('porcentaje 2', porcentajeHomework)


  
  
          })
          .catch(e => {console.log(e)})
        }
        catch(error) {
           console.log(error);
        }
      }
  

      


    async function getMyVideos() {
        try {
            await fetch(url + 'api/player/find/public', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    id: player.id,
                }),
            }).then(res => res.json())
                .then((dat) => {
                    try {
                        console.warn(dat)
                        setData(dat.data.videos)
                        setUser(dat.data)
                        var num = suma(dat.data.videos.length<1?0:dat.data.videos.length)
                        setPorcentaje(num)


                         // VIDEOS PUBLICOS 
          


          
                        // if (dat.message == 'Unauthenticated.') {
                        //     LogoutUser()
                        //     setTimeout(()=>{
                        //         navigation.replace('Login')
                        //     },1000)
                        // }
                    } catch (error) {
                        Snackbar.show({
                            text: error,
                            duration: Snackbar.LENGTH_LONG,
                        });
                    }
                })

        } catch (error) {
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
            });
        }
    }

    function mensaje(mensaje) {
        Snackbar.show({
            text: mensaje,
            duration: Snackbar.LENGTH_LONG,
        });
    }


    console.log(user);



    function setNumber(num) {
      num = num.toString().replace(/[^0-9.]/g, '');
      if (num < 1000) {
         return num;
      }
      let si = [{
            v: 1E3,
            s: "K"
         },
         {
            v: 1E6,
            s: "M"
         },
         {
            v: 1E9,
            s: "B"
         },
         {
            v: 1E12,
            s: "T"
         },
         {
            v: 1E15,
            s: "P"
         },
         {
            v: 1E18,
            s: "E"
         }
      ];
      let index;
      for (index = si.length - 1; index > 0; index--) {
         if (num >= si[index].v) {
            break;
         }
      }
      return (num / si[index].v).toFixed(2).replace(/\.0+$|(\.[0-9]*[1-9])0+$/, "$1") + si[index].s;
   }

   
   


    // const RenderItem = ({ item, index, separators }) => {
    //     var urlImage =  item.file_captura;
    //     return (
    //         <View style={{ width: wp('40%'),  height: '95%', backgroundColor: '#FFF', marginHorizontal: 25, borderRadius: 10, marginTop: 10, elevation: 3 }}>
    //             <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate('Review player MyTeam', { player: player, token: token, update: (message) => { mensaje(message) }, video: item, view:'perfil' })} style={{ flex: 1 }}>
    //                 <View style={{ alignItems: 'center', justifyContent: 'center', }}>
    //                     <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: wp('40%'), height: 200, borderRadius: 10 }} />
    //                     <View style={{ position: 'absolute', width: hp('10%'), height: hp('10%'), backgroundColor: 'rgba(0,0,0,0.3)', borderRadius: 50, borderColor: '#D6D6D6', borderWidth: 1, justifyContent: 'center', alignItems: 'center' }}>
    //                         <Icon name="play" size={hp('4')} color="#FFF" style={{ marginLeft: 10 }} />
    //                     </View>
    //                 </View>
    //                 <View style={{ flex: 2, padding: 10, flexDirection: 'row' }}>
    //                     <View style={{ width: '100%', alignItems: 'center', flexDirection: 'row' }}>
    //                         <Text numberOfLines={1} style={{ fontSize: hp('1.5%'), fontWeight: 'bold', color: '#000', marginLeft: 5 }}>{item.skill_name}</Text>
    //                     </View>
    //                 </View>
    //                 <View style={{ flex: 1, padding: 10, justifyContent: 'center', flexDirection: 'row' }}>
    //                     <View style={{ width: wp('30%'), marginTop: 10 }}>
    //                         <View style={{ width: wp('15%'), backgroundColor: 'green', elevation: 2, justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
    //                             <Text numberOfLines={1} style={{ fontSize: hp('1.2%'), color: '#FFF', fontWeight: 'bold', }}>{item.level}</Text>
    //                         </View>
    //                     </View>
    //                     <View style={{ width: wp('5%'), justifyContent: 'center', alignItems: 'center', }}>
    //                         <Icon name="trophy" size={hp('1.4%')} color="green" />
    //                         <Text style={{ fontSize: hp('1.4%'), color: "#000" }}>{item.qualification}</Text>
    //                     </View>
    //                 </View>
    //             </TouchableOpacity>
    //         </View>
    //     );
    // }




    // FUNCIONES DE RENDERZADO 

    // SESSION DE LOS VIDEOS 
    const RenderItem=({item, index, separators})=>{
        var urlImage = item.file_captura;
        return(
            <View style={{width:wp('33%'), height: '95%', marginHorizontal:2, marginRight:0, marginBottom: 2, elevation: 3,}}>
                 <TouchableOpacity activeOpacity={0.8} 
                 onPress={()=>{navigation.navigate('Video',{player, video:item})}}
                 style={{ flex: 1 }}>
                     <View style={{alignItems:'center', justifyContent:'center',}}>

                     <View style={{backgroundColor:'#333', position:'absolute', opacity:0.7, left:5, zIndex:100, top:5, padding:3, borderRadius:5}}>
                         <Text style={{color:"#fff", fontWeight:'bold'}}>{item.level}</Text>
                      </View>

                        <Image source={{uri:urlImage}} resizeMode='stretch' style={{width:wp('33%') ,height: hp('21%'),}} />
                        <View style={{position:'absolute', width:hp('10%'),height: hp('10%'), justifyContent:'center', alignItems:'center'}}>
                            <Icon name="play" size={hp('4')} color="#FFF" style={{marginLeft:10}}/>
                        </View>

                        <View style={{backgroundColor:'#333', position:'absolute', opacity:0.7, left:5, zIndex:100, bottom:5, padding:3, borderRadius:5}}>
                         <Text style={{color:"#fff", fontWeight:'bold'}}>{item.skill_name.substr(0,15)}..</Text>
                      </View>
                      

                     </View>
                 </TouchableOpacity>
  
                 
            </View>
        );
    }
    // FIN DE LA SESSION VIDEO  
    const Videos = () => {
      return(
        <View style={{ flex: 1,}}>
  
                {/* RENDER DE LOS VIDEOS  */}
                <FlatList
                      data={data}
                      renderItem={RenderItem}
                      keyExtractor={(item, index) => index}
                      numColumns={3}
                      initialNumToRender={5}
                      // style={{width: wp('100%'), flex: 1, marginTop:15}}
                      // onRefresh={() => getMyInfo(token,user)}
                      refreshing={false}
                      onEndReachedThreshold={0.7}
                      removeClippedSubviews={true}
                      ListEmptyComponent={EmptyList}
                  />
                  {/* RENDER DE LOS VIDEOS  */}
        </View>
      )
    }
  
   
  




    // SESSION DE LOS VIDEOS HOMEWORK
    const RenderItemHomework=({item, index, separators})=>{
        var urlImage = item.file_captura;
        return(
            <View style={{width:wp('33%'), height: '95%', marginHorizontal:2, marginRight:0, marginBottom: 2, elevation: 3,}}>
                 <TouchableOpacity activeOpacity={0.8} onPress={()=>{navigation.navigate('Video',{player, video:item})}} style={{ flex: 1 }}>
                     <View style={{alignItems:'center', justifyContent:'center',}}>

                     <View style={{backgroundColor:'#333', position:'absolute', opacity:0.7, left:5, zIndex:100, top:5, padding:3, borderRadius:5}}>
                         <Text style={{color:"#fff", fontWeight:'bold'}}>{item.level}</Text>
                      </View>

                        <Image source={{uri:urlImage}} resizeMode='stretch' style={{width:wp('33%') ,height: hp('21%'),}} />
                        <View style={{position:'absolute', width:hp('10%'),height: hp('10%'), justifyContent:'center', alignItems:'center'}}>
                            <Icon name="play" size={hp('4')} color="#FFF" style={{marginLeft:10}}/>
                        </View>

                        <View style={{backgroundColor:'#333', position:'absolute', opacity:0.7, left:5, zIndex:100, bottom:5, padding:3, borderRadius:5}}>
                         <Text style={{color:"#fff", fontWeight:'bold'}}>{item.skill_name.substr(0,15)}..</Text>
                      </View>
                      
                     </View>
                 </TouchableOpacity>
  
                 
            </View>
        );
    }
    // FIN DE LA SESSION VIDEO  
    const VideosHomework = () => {
      return(
        <View style={{ flex: 1,}}>
  
                {/* RENDER DE LOS VIDEOS  */}
                <FlatList
                      data={dataHomework}
                      renderItem={RenderItemHomework}
                      keyExtractor={(item, index) => index}
                      numColumns={3}
                      initialNumToRender={5}
                      // style={{width: wp('100%'), flex: 1, marginTop:15}}
                      // onRefresh={() => getMyInfo(token,user)}
                      refreshing={false}
                      onEndReachedThreshold={0.7}
                      removeClippedSubviews={true}
                      ListEmptyComponent={EmptyList}
                  />
                  {/* RENDER DE LOS VIDEOS  */}
        </View>
      )
    }

    

    const EmptyList = () => { // Render que se muestra cuando la lista esta vacia 
        return(
            <View style={{flex:1,height:hp('70%'),justifyContent:'center', alignItems:'center'}}>
                <Icon2 name="video" size={hp('10%')} color="#b4b4b4" style={{opacity:0.4}}/>
                <Text style={{fontSize:hp('2%'), opacity:0.7, color:'#b4b4b4', fontWeight:'bold'}}>
                {I18n.t('profile.listEmpty')}
                </Text>
            </View>
        )
      }
    
    
    


    

    
     // SESSION DE LOS VIDEOS HOMEWORK
    //  const RenderItem=({item, index, separators})=>{
    //     var urlImage = item.file_captura;
    //     return(
    //         <View style={{width:wp('33%'), height: '95%', marginHorizontal:2, marginRight:0, marginBottom: 2, elevation: 3,}}>
    //              <TouchableOpacity activeOpacity={0.8} onPress={()=> navigation.navigate('Video Profile',{player:user, i:index, token:token,update:(message)=>{mensaje(message)}, video:item})}  style={{flex:1}}>
    //                  <View style={{alignItems:'center', justifyContent:'center',}}>
    //                     <Image source={{uri:urlImage}} resizeMode='stretch' style={{width:wp('33%') ,height: hp('21%'),}} />
    //                     <View style={{position:'absolute', width:hp('10%'),height: hp('10%'), justifyContent:'center', alignItems:'center'}}>
    //                         <Icon name="play" size={hp('4')} color="#FFF" style={{marginLeft:10}}/>
    //                     </View>
    //                  </View>
    //              </TouchableOpacity>
  
                 
    //         </View>
    //     );
    // }



    const Prueba = ({ item, index, separators }) => {
        return (
            <View>
                <Text>{item.skill_name}</Text>
            </View>
        )
    }

    const Change = async()=>{
        try {
            await fetch(url + 'api/team/change/player', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    name_method: valiWorking,
                    value_method: workingValue,
                    player_id: player.id,
                }),
            }).then(res => res.json())
                .then((dat) => {
                    console.warn(dat)
                    try {
                        if (dat.ok) {
                            Snackbar.show({
                                text: dat.data,
                                duration: Snackbar.LENGTH_LONG,
                            });
                            getMyVideos()
                            setVisible(false)
                        }
                        // if (dat.message == 'Unauthenticated.') {
                        //     LogoutUser()
                        //     setTimeout(()=>{
                        //         navigation.replace('Login')
                        //     },1000)
                        // }
                    } catch (error) {
                        Snackbar.show({
                            text: error,
                            duration: Snackbar.LENGTH_LONG,
                        });
                    }
                })
        } catch (error) {
            
        }
    }

    function ButtonDelete (id){
        Alert.alert(
          "Remove",
          "Are you sure you want to remove this player?",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "OK", onPress: () => RemovePlayer() }
          ]
        )
    };
    

    const RemovePlayer = async()=>{

        try{

            
            const datos = {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: null
            }

            let ruta = url+'api/player/out/team/'+ player.id;

            console.log(ruta);
            await fetch(ruta, datos)
            .then(res => res.json())
            .then((data) => {
               console.log(data);
               navigation.goBack();
            })
            .catch(e => {
                console.log(e)
            })

        }catch(error){

        }

    }

console.log(user, 'user -----')

    function suma(number) {
        var real = parseInt(number);
        var length = real.toString().length;
        var cero = "0"; /* String de cero */  
        
        if (number >= 100) {
            return "1"; 
        } else {
            return (cero.repeat(1) +"."+(cero.repeat(2-length))+real); 
            // return length
        }
      }
    return (
        <View style={styles.container}>
            {/* <Image source={require('../../../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'),
                position: "absolute",
            }} /> */}





            <View style={{flex:1, flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                <View style={{marginLeft:20, justifyContent:'center',}}>
                    {/* <TouchableOpacity activeOpacity={0.8} onPress={()=>setVisibleModalImagen(true)} style={{width:hp('3%'), height:hp('3%'), position:'absolute', top:-5, right:-5, borderWidth:1, zIndex: 100, borderColor:'#D6D6D6', backgroundColor: "#FFF", borderRadius:hp('3%'), justifyContent:'center', alignItems:'center'}}>
                        <Icon2 name="update" size={hp('2%')} style={{opacity:0.5}}/>
                    </TouchableOpacity> */}
                    {user.photo?
                        <Image source={{uri:imagePlayer}} resizeMode="cover" style={{width:wp('25.7%'),height: hp('12.5%'), borderRadius:hp('50%')}} />
                    :
                        <View style={{backgroundColor:'#D6D6D6', width:hp('13%'),height: hp('13%'), borderRadius:hp('50%'), justifyContent:'center', alignItems:'center'}}>
                            <Text style={{fontSize:hp('6%'), color:'#3d3d3d', fontWeight:'bold', }}>{((user.name || "").charAt(0) || "").toUpperCase()}</Text>
                        </View>
                       
                    }
                </View>
                <View style={{flex:1, height:'60%',  justifyContent:'flex-start', marginLeft:20, marginRight:20, }}>
                  
                    <Text numberOfLines={1} style={{fontSize:hp('2.5%'), color:'#000',textTransform:'capitalize', marginTop:10, fontWeight:'bold'}}>
                      {user.name}
                      </Text>
                    <Text numberOfLines={1} style={{fontSize:hp('1.6%'), opacity:0.8, color:'#000', }}>{user.username}</Text>
                <View style={{flexDirection:'row', alignItems:'center', marginTop:5}}>
                    {svg?
                    //   <SvgUri
                    //     width="25"
                    //     height="25"
                    //     viewBox="0 0 1200 1200"
                    //     uri={svg}
                    //   />
                      <Image source={{uri:svg}} resizeMode="cover" style={{width:wp('10.7%'),height: hp('5.9%'), borderRadius:hp('50%')}} />
                    :null}
                  <Text style={{paddingLeft:10}}>{user.rango_state}</Text>
                </View>
                <View style={{position:'absolute', bottom:0, right:10, }}>
                    {user.pais?
                      <SvgUri
                        width="40"
                        height="40"
                        fill="#000"
                        style={{borderRadius:5}}
                        viewBox={user.pais == 've'? "0 0 130 100" : "0 0 640 480"}
                        uri={"https://cdn.ipwhois.io//flags//"+user.pais+".svg"}
                      />
                    :null}
                </View>
              </View>
          </View>


            {/* <View style={{ flex: 1, flexDirection: 'row', justifyContent:'center', alignItems:'center' }}>
                <View style={{marginLeft: 20, marginRight:20, justifyContent: 'center', }}>
                    {user.photo ?
                        <Image source={{ uri: imagePlayer }} resizeMode="cover" style={{ width: hp('14%'), height: hp('14%'), borderRadius: 10 }} />
                        :
                        <View style={{ backgroundColor: '#D6D6D6', width: hp('14%'), height: hp('14%'), borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: hp('4%'), fontWeight: 'bold', }}>{user.name.charAt(0)}</Text>
                        </View>
                    }
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-start', marginRight: 20, }}>
                    <Text numberOfLines={2} style={{ fontSize: hp('2%'), color: '#000', marginTop: 10 }}>{user.name}</Text>
                    <Text numberOfLines={1} style={{ fontSize: hp('1.3%'), color: '#000', opacity:0.5 }}>{user.email}</Text>
                    <Text numberOfLines={2} style={{ fontSize: hp('1.6%'), color: '#000', opacity: 0.5, marginTop: 5 }}>{user.team_name}</Text>
                    <Text numberOfLines={1} style={{ fontSize: hp('1.7%'), color:'#000', textTransform:'capitalize' }}>{user.name_method == 'level'?user.namemethod:user.name_method+': '}{user.name_method !== 'level'? user.namemethod: null}</Text>
                </View>
            </View> */}



            {/* <View style={{ flex: 0.2, }}>
                <View style={{justifyContent:'center', alignItems:'center'}}>
                    <Text style={{position:'absolute', zIndex: 10, fontSize:hp('1.5'), fontWeight:'bold'}}>{data.length<1?"0":data.length}%</Text>
                    <Progress.Bar progress={porcentaje} width={wp('90%')} height={hp('2%')} color="green"/>
                </View>
             
            </View> */}


            <View style={{height:hp('3%')}}>
                <TouchableOpacity activeOpacity={1} style={{justifyContent:'center', alignItems:'center'}}>
                      <Text style={{position:'absolute', zIndex: 10, fontSize:hp('1.5'), fontWeight:'bold'}}>{porcentajeHomework}%</Text>
                      <Progress.Bar progress={Number(porcentaje)} width={wp('90%')} height={hp('1.6%')} color="green" borderColor="white" backgroundColor="white" borderRadius={5}/>
                </TouchableOpacity>
                {/* <View style={{flexDirection:'row', width:wp('100%'), justifyContent:'space-around', marginTop:5}}>
                    <Text style={{fontSize:hp('1.5%')}}>1</Text>
                    <Text style={{fontSize:hp('1.5%')}}>PROGRESS</Text>
                    <Text style={{fontSize:hp('1.5%')}}>100%</Text>
                </View> */}
          </View>

          
            <View style={{ flex: 3, }}>
                
                {/* <FlatList
                    data={data}
                    renderItem={RenderItem}
                    keyExtractor={(item, index) => index}
                    numColumns={3}
                    style={{width: wp('100%'), flex: 1 }}
                    onRefresh={() => getMyVideos()}
                    refreshing={refresh}
                /> */}


                <TabView
                navigationState={{ index, routes }}
                onIndexChange={(i) => console.log(i)} 
                renderScene={SceneMap({
                    videos: Videos,
                    homework: VideosHomework,
                    
                })}
                renderTabBar={(props) => <TabBar {...props}
                    pressColor={'transparent'}
                    indicatorStyle={{ backgroundColor: '#868686' }}
                    style={{ backgroundColor: '#f3f3f3' }}
                    renderLabel={({ route, focused, color }) => (
                        <Text 
                        style={{fontWeight:'bold', color: focused ? '#3d3d3d' : '#646469', opacity: focused ? 1 : 0.5, }}>
                          {route.title}
                        </Text>
                    )}
                />}
                // onIndexChange={(index) => console.log(index)}
                initialLayout={{ width: Dimensions.get('screen').width }}
                
               />





            </View>
            <Modal 
            animationType="slide"
            transparent={true}
            visible={visible}>
                 <KeyboardAvoidingView  behavior={Platform.OS === "ios"? "padding":"null"} enabled style={{ flex: 1 ,flexDirection: 'column', justifyContent: 'flex-end'}}>
                    <View style={{ height: "80%" ,width: '100%', backgroundColor:"#FFF", justifyContent:"center", borderTopRightRadius:10, borderTopLeftRadius:10}}>
                     <TouchableOpacity onPress={()=>setVisible(false)} style={{flex:0.1 ,width:'100%', height:'6%', justifyContent:'center', alignItems:'center'}}>
                        <Icon name="chevron-down" size={hp('2.3%')} color="#000"/>
                     </TouchableOpacity>
                        <View style={{flex:1, borderTopRightRadius:10, borderTopLeftRadius:10, justifyContent:'center', alignItems:'center'}}>
                            <View style={{width:wp('100%'), justifyContent:'center', alignItems:'center'}}>
                                <Text style={{fontSize:hp('1.7%'),opacity:0.5}}>Change working method</Text>
                            </View>
                            <View style={{flex:2,justifyContent:'center', alignItems:'center',}}>
                                    <View style={{width:wp('90%'), paddingVertical: 20,}}>
                                        <Select2
                                            isSelectSingle
                                            style={{ borderRadius: 5, backgroundColor: '#FFF', }}
                                            colorTheme="blue"
                                            showSearchBox={false}
                                            popupTitle="Select the working method"
                                            title="Select the working method"
                                            data={workingLevel}
                                            onSelect={(d) => {
                                                setValiWorking(d[0]);
                                            }}
                                        />
                                    </View>
                                    <View style={{width:wp('90%'), paddingVertical: 20,}}>
                                        <Select2
                                            isSelectSingle
                                            showSearchBox={false}
                                            style={{ borderRadius: 5, backgroundColor: '#FFF', }}
                                            colorTheme="blue"
                                            popupTitle={"Choose a "+valiWorking}
                                            title={"Choose a "+valiWorking}
                                            data={valiWorking=='level'?Levels:valiWorking=='category'?Categories:valiWorking=='course'?Courses:[]}
                                            onSelect={(d) => {
                                                setWorkingValue(d[0]);
                                            }}
                                        />
                                    </View>
                            </View>
                            <View style={{flex:0.8, justifyContent:'center', alignContent:'center', }}>
                                    <Block center>
                                        <GaButton style={{borderRadius:5}} round size="small" color="success" onPress={()=>Change()}>Accept</GaButton>
                                    </Block>
                                    {/* <TouchableOpacity style={{}} activeOpacity={0.8} onPress={()=>Change()}>
                                            <Text>Accept</Text>
                                    </TouchableOpacity> */}
                            </View>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </Modal>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
});


const optionStyles = {
    optionTouchable: {
      underlayColor: 'red',
      activeOpacity: 40,
    },
    optionWrapper: {
      margin: 5,
    },
    optionText: {
      color: 'black',
    },
  };

//make this component available to the app
export default ProfilePublic;
