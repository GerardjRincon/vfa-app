import React, { useState, useEffect, useLayoutEffect } from 'react'
import { Alert, StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import { url } from '../../../storage/config';
// import { channel, pusher } from '../../../routes/validationSesion';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Agenda } from 'react-native-calendars';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Pusher from 'pusher-js/react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {signal, canal} from '../../../storage/pusher';
import I18n from 'i18n-js';

const Training = ({ route }) => {

  const navigation = useNavigation();
  const [onDateChanged, setOnDateChange] = useState('2017-09-07')
  const [items, setItems] = useState({});
  const [token, setToken] = useState(null);
  const [coach, setCoach] = useState({})
  const [user, setUser] = useState({})
  const [refresh, setRefresh] = useState(false)

  async function getList2(token) {
    console.log("refresco 1www")
    // setLoader(true)
    // setRefresh(true)
    try {
      await fetch(url + 'api/calendar/app/list/player', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      }).then(res => res.json())
        .then((dat) => {

          console.log(dat);
          try {
            console.log("aqui 444")
            setItems(dat.data)
            console.warn(dat)
            setRefresh(false)
          } catch (error) {
            console.log("wefwefwef")
            setRefresh(false)
            // console.warn('error:'+error)
          }
        })

    } catch (error) {
      setRefresh(false)
      // console.warn(error)
    }
  }

  useEffect(async () => {
    const users = await AsyncStorage.getItem('@user');
    const token = await AsyncStorage.getItem('@token');
    const coach = await AsyncStorage.getItem('@coach');
    const coachJson = JSON.parse(coach);
    const userJson = JSON.parse(users);

    const tokenJson = JSON.parse(token);
    setCoach(coachJson);
    setToken(tokenJson);
    getList2(tokenJson);
    setUser(userJson);

    const canal = signal.subscribe("VFA");


    canal.bind("NotificationCalendarTeam-"+userJson.myteamplayer.id, async(data) => {
          console.log("signal update calendar");
          getList2(tokenJson);
    });


    return ()=>{
      canal.disconnect();
      // unsub;
      }
  }, [])

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Training Plan',
  })
  }, [])


  const renderEmptyDate = () => {
    return (
      <View
        style={{ height: 80, elevation: 4, justifyContent:'center', alignItems:'center', backgroundColor: '#FFF', borderRadius: 5,
        padding: 10,
        marginLeft:10,
        marginRight: 10,
        marginTop: 17,
        marginBottom: 4, }}>
        <View style={{flexDirection: 'row', justifyContent:'center', alignItems:'center' }}>
          <View style={{ width: hp('7%'), height: '100%', justifyContent: 'center', alignItems: 'center' }}>
            {/* <Image source={{ uri: item.type == 1 ? urlImage : urlExercises }} resizeMode="cover" style={{ width: hp('6%'), height: hp('6%'), borderRadius: 10 }} /> */}
            <Icon2 name="soccer" size={hp('4%')} />
          </View>
          <View style={{ marginLeft: 10, flex: 1, justifyContent:'center', alignItems:'center'}}>
            {/* <Text style={{ fontSize: hp('1.2%'), position: 'absolute', right: 0, color: '#929292' }}>{item.type == 1 ? "Skill" : "Exercises"}</Text> */}
            <View style={{ width: '100%', height: '100%', justifyContent: 'center', }}>
              <Text numberOfLines={2} style={{ fontSize: hp('1.7%'), opacity: 0.7, width: '85%' }}>

                {I18n.t('tex7')}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }

  const renderItem = (item) => {
    var urlImage = 'https://go.vfa.app/preview/' + item.img;
    var urlExercises = 'https://go.vfa.app/exercises/' + item.img;
    return (
      <TouchableOpacity
        testID={'item'}
        style={[styles.item, { height: 80, elevation: 4 }]}
        onPress={() => {
          if (item.type == 1) {
            navigation.navigate('3DTraining', { skill: item, token: token, user, type:1 })
          } else {
            navigation.navigate('3DTraining', { skill: item, token: token, user, type:2 })
          }
        }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ width: hp('7%'), height: '100%', justifyContent: 'center', alignItems: 'center' }}>
            <Image source={{ uri: item.type == 1 ? urlImage : urlExercises }} resizeMode="cover" style={{ width: hp('6%'), height: hp('6%'), borderRadius: 10 }} />
          </View>
          <View style={{ marginLeft: 10, flex: 1, }}>
            <Text style={{ fontSize: hp('1.2%'), position: 'absolute', right: 0, color: '#929292' }}>{item.type == 1 ? "Skill" : "Exercises"}</Text>
            <View style={{ width: '100%', height: '100%', justifyContent: 'center', }}>
              <Text numberOfLines={2} style={{ fontSize: hp('1.7%'), opacity: 0.7, width: '85%' }}>{item.name}</Text>
              {item.description ?
                <Text numberOfLines={2} style={{ fontSize: hp('1.4%'), opacity: 0.5 }}>{item.description}</Text>
                : null}
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  function timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }



  return (
    <View style={{ flex: 1, backgroundColor: '#F3F3F3', }}>
      <Image source={require('../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    position: "absolute",
            }} />
      {!coach?
        <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
            <Icon name="calendar" size={hp('15%')} style={{opacity:0.3}}/>
            <Text style={{fontSize:hp('2%'), paddingTop:20}}>{I18n.t('training.text1')}</Text>
        </View>
    :
      <Agenda
        testID={'agenda'}
        items={items}
        renderItem={renderItem}
        renderEmptyData = {renderEmptyDate}
        // showClosingKnob={true}
        // onRefresh={() => getList2(token)}
        // refreshing={refresh}
      />
    }
      {/* <TouchableOpacity style={{position:'absolute', bottom:30, right:20, width:60, height:60, backgroundColor: '#54BCE5', borderRadius:50, elevation:3, justifyContent: 'center', alignItems:'center'}}>
                <Icon name="plus" size={hp('1.7%')} color="white"/>
            </TouchableOpacity> */}
    </View>
  )
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#FFF',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
    marginBottom: 4,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30
  }
});

export default Training
