//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Modal, TextInput, Image, KeyboardAvoidingView, Platform, Dimensions} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { url } from '../../../storage/config';
import Icon from 'react-native-vector-icons/FontAwesome';
import Snackbar from 'react-native-snackbar';
import Loader from '../../coach/global/loader';
const { width, height } = Dimensions.get('screen');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// create a component
const MyTeam = ({ navigation, route }) => {
    /////// Estados globales 
    const [visible, setVisible] = useState(false);
    const [loader, setLoader] = useState(false);
    const [data, setData] = useState([]);
    const [team, setTeam] = useState({});
    const [token, setToken] = useState(null);
    const [refresh, setRefresh] = useState(false)
    const [user, setU] = useState({})

    // const [team, setTeam] = useState({name:'My Teams'});


    useEffect(async() => {
        const unsubscribe = navigation.addListener('focus', async() => {
            const token = await AsyncStorage.getItem('@token');
            const team = await AsyncStorage.getItem('@team');
            const userasync = await AsyncStorage.getItem('@user');
            const userJson = JSON.parse(userasync);
            const teamJson = JSON.parse(team);
            const tokenJson = JSON.parse(token);
            if (tokenJson) {
                setToken(tokenJson)
                setU(userJson)
                setTeam(teamJson)
                getMyTeam(tokenJson,teamJson.id)
                
            }
        });
      
        return unsubscribe;
    }, [navigation])


    ////// Funciones de my team

    async function getMyTeam(t,id){

        console.log(t, id);

        setLoader(true)
        try {
            await fetch(url+'api/team/player/all',{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+t
            },
            body: JSON.stringify({
                team_id: id,
            }),
        }).then(res => res.json())
        .then((dat) =>{
            try {
                console.warn(dat.data)
                const array = ordenar(dat.data, 'points')
                setData(array)
                // setTeam(dat.team)
                setRefresh(false)
                setLoader(false)
                // if (dat.message == 'Unauthenticated.') {
                //     LogoutUser()
                //     setTimeout(()=>{
                //         navigation.replace('Login')
                //     },1000)
                // }
            } catch (error) {
                setRefresh(false)
                setLoader(false)
                Snackbar.show({
                    text: error,
                    duration: Snackbar.LENGTH_LONG,
                });
            }
        })
            
        } catch (error) {
            setRefresh(false)
            setLoader(false)
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
            });
        }
    }

    function ordenar(array,param){ //ordenar alfabeticamente
        array.sort( (a, b) => {
          let asingle= a[param].toString().replace(/[Áá]/gi,"a").replace(/[Éé]/gi,"e").replace(/[Íí]/gi,"i")
          .replace(/[Óó]/gi,"o").replace(/[Úú]/gi,"u").replace(/[Ññ]/gi,"nzz").toLowerCase();
          let bsingle = b[param].toString().replace(/[Áá]/gi,"a").replace(/[Éé]/gi,"e").replace(/[Íí]/gi,"i")
          .replace(/[Óó]/gi,"o").replace(/[Úú]/gi,"u").replace(/[Ññ]/gi,"nzz").toLowerCase();
          if (asingle > bsingle) return -1;
          if (asingle < bsingle) return 1;
          return 0;
        });
        return array;
      }

      console.log(user, '--- dtaa  111');

    ////// Fin de funciones My team

    const RenderItem=({item, index, separators})=>{
        const imageMyTeam = 'https://go.vfa.app'+item.photo;
        return(
            <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('ProfilePublic', {player:item,token:token, team:team, update:(e)=>{getMyTeam(token,team.id)}})} style={{width:wp('100%'), height:hp('10%'), marginTop:5, flexDirection:'row',backgroundColor: "#FFF",}}>
                <View style={{width:wp('100%'), height:hp('10%'), marginTop:5, flexDirection:'row',backgroundColor: "#FFF",  borderBottomWidth:1, borderColor:'#D6D6D6'  }}>
                    <View style={{flex:1, marginLeft:20, justifyContent:'center',}}>
                        {item.photo?
                            <Image source={{uri:imageMyTeam}} resizeMode="cover" style={{width:hp('8%'),height: hp('8%'), borderRadius:50}} />
                        :
                            <View style={{backgroundColor:'#D6D6D6', width:hp('8%'),height: hp('8%'), borderRadius:50, justifyContent:'center', alignItems:'center'}}>
                                <Text style={{fontSize:hp('3%'), fontWeight:'bold', textTransform:'capitalize' }}>{item.name.charAt(0)}</Text>
                            </View>
                        }
                    </View>
                    <View style={{flex:4, flexDirection:'row',  justifyContent:'center', alignItems:'center'}}>
                        <View style={{flex:3}}>
                            <Text style={{fontSize:hp('1.8%'), fontWeight:'bold', textTransform:'capitalize'}}>{item.name}</Text>
                            <Text style={{fontSize:hp('1.5%'),opacity:0.6, }}>{item.username}</Text>
                            <Text style={{color:"#3d3d3d", fontWeight:'bold',opacity:0.6}}>{item.name_method} {item.value_method}</Text>
                        </View>
                        <View style={{flex:1, justifyContent:'center', flexDirection:'row', alignItems:'center', marginRight:20, width:hp('5%'),height:hp('5%'),}}>
                            {/* <View style={{width:hp('2%'), marginRight:8, height:hp('2%'), backgroundColor: '#D8D8D8', borderRadius:50, justifyContent:'center', alignItems:'center'}}>
                                <Icon name="flash" size={hp('1.5%')} color="#FFAF20" />
                            </View> */}
                            {/* <Text style={{fontSize:hp('1.7%'), fontWeight:'bold', color:'#000'}}>{item.points} pts</Text> */}
                        </View>
                        <View style={{marginRight: 20,}}>
                            <Icon name="chevron-right" size={hp('1.7%')} />
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    const Empty = ()=>{
        return(
            <View style={{justifyContent:'center', alignItems:'center', flex:1, marginTop:'50%',paddingRight: 40, paddingLeft: 40,}}>
                <Icon name="users" size={hp('15%')} color="#000" style={{ opacity: 0.3, paddingBottom: 20 }} />
                <Text style={{fontSize:hp('3%'),fontWeight:'bold', color:'#000', opacity:0.5}}>
                       {I18n.t('myTeam.menssagePage')}
                    </Text>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            {loader?<Loader/>:null}
            <Image source={require('../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    position: "absolute",
            }} />
            <FlatList
                data={data}
                renderItem={RenderItem}
                keyExtractor={(item, index) => index}
                style={{ flex: 1 }}
                ListEmptyComponent={Empty}
                onRefresh={() => getMyTeam(token,user.myteamplayer.id)}
                refreshing={refresh}
            />
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
    input: {
        height: hp('4.5%'),
        borderWidth:1,
        borderColor:"#BFBEBE",
        borderRadius:5,
        paddingLeft:20,
        color:"#000",
        backgroundColor: "#FFF",
        
    },
    createButton: {
        width: width * 0.5,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
    },
});

//make this component available to the app
export default MyTeam;
