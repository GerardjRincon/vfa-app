import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, StyleSheet, Dimensions, Image, Text, Modal, TouchableOpacity, Alert, Platform, LogBox } from 'react-native';
import Video2 from 'react-native-video';
import AsyncStorage from '@react-native-async-storage/async-storage';
const { width, height } = Dimensions.get('screen');
import Loader from '../../coach/global/loader';
import { BlurView } from "@react-native-community/blur";
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import Clipboard from '@react-native-clipboard/clipboard';
import Share from 'react-native-share';
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
  } from 'react-native-popup-menu';
import I18n from 'react-native-i18n';
import Snackbar from 'react-native-snackbar';
import { url } from '../../../storage/config';
import * as RNFS from 'react-native-fs';
import { request, requestMultiple, PERMISSIONS } from 'react-native-permissions';
import RNFetchBlob from 'rn-fetch-blob'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


const VideoPlayer = ({ navigation, route }) => {
    const { player, i, token, update, video } = route.params;


    console.log(video, 'video ----');

    const indx = i + 1;
    const [index, setIndex] = useState(0);
    const [user, setUser] = useState({});
    const [loader, setLoader] = useState(false);
    const [textLoader, setTextLoader] = useState('Loading...');
    const [visible, setVisible] = useState(false);
    // const [token, setToken] = useState(null);

    var urlVideo = '';
    if (video) {
        urlVideo = video.file_video;
    } else {
        urlVideo = player.file_video;
    }

    useEffect(async() => {
          const user = await AsyncStorage.getItem('@user');
          const userJson = JSON.parse(user);
          setUser(userJson);
    }, [])

    useLayoutEffect(() => {
        navigation.setOptions({
            title: player.name,
        })
    }, [])


    // Area de las funciones  

    function ButtonDelete (id){
        Alert.alert(
          "Are you sure?",
          "If you continue you will lost the points!",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "OK", onPress: () => DeleteVideo(id, token) }
          ]
        )
    };
    
    async function DeleteVideo(id,token) {
        // setRefresh(true)
        try {
            await fetch(url+'api/borrar/video',{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+token
            },
            body: JSON.stringify({
              id: id,
            }),
            }).then(res => res.json())
            .then((dat) =>{
                // console.warn(dat);
              if (dat) {
                console.warn(dat);
                navigation.goBack();
                // const array = ordenar(dat, 'points')
                // setData(array) 
                // setRefresh(false)
              }else{
                // setTextEmptyList('No players')
                // setData(dat) 
                // setRefresh(false)
              }
            }).catch(err=>{
              console.warn(err);
            //   setRefresh(false)
            });
        } catch (error) {
          console.warn(error);
        //   setRefresh(false)
        }
      }
      ////



      async function CompartirDOSIos(){

        console.log(user);
    
        // console.log(`${RNFS.DocumentDirectoryPath}/video.mp4`);
        var ubicacion =  `${RNFS.DocumentDirectoryPath}/video.mp4`;
    
        RNFS.downloadFile({
            fromUrl:video.file_video,
            toFile: ubicacion,
        }).promise.then(res =>{
            console.log(res);
            console.log('file://' + ubicacion);
    
    
    const url = "file://" + ubicacion;

    var title= '';
    var message= '';



    const options = Platform.select({
      ios: {
        activityItemSources: [
          {
            // For sharing url with custom title.
            placeholderItem: { type: 'url', content: url },
            item: {
              default: { type: 'url', content: url },
            },
            subject: {
              default: title,
            },
            linkMetadata: { originalUrl: url, url, title },
          },
          {
            // For sharing text.
            placeholderItem: { type: 'text', content: message },
            item: {
              default: { type: 'text', content: message },
              message: null, // Specify no text to share via Messages app.
            },
            linkMetadata: {
              // For showing app icon on share preview.
              title: message,
            },
          },
        ],
      },
      default: {
        title,
        subject: title,
        message: `${message} ${url}`,
      },
    });
    
    Share.open(options);
    
    
    
            // Share.open({
            //     title: "Rate my performance on VFA.app using this link!",
            //     message: "Rate my performance on VFA.app using this link! "+ 'https://go.vfa.app/'+userJson.username,
            //     url: "file://" + ubicacion,
           
            //   });
          })
        }
   async function CompartirIos(){

    console.log(user);

    // console.log(`${RNFS.DocumentDirectoryPath}/video.mp4`);
    var ubicacion =  `${RNFS.DocumentDirectoryPath}/video.mp4`;

    RNFS.downloadFile({
        fromUrl:video.file_video,
        toFile: ubicacion,
    }).promise.then(res =>{
        console.log(res);
        console.log('file://' + ubicacion);


const url = "file://" + ubicacion;
// let title = I18n.t('ar.shareSocial.text1') +' '+ player.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+player.username+'/'+video.url_public;
// // let title = 'Rate my performance on VFA.app using this link!'+ ' https://go.vfa.app/'+player.username+'/'+video.url_public;
// // let message = 'Rate my performance on VFA.app using this link!' + ' https://go.vfa.app/'+player.username+'/'+video.url_public;
// let message = I18n.t('ar.shareSocial.text1') +' '+ player.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+player.username+'/'+video.url_public;

var title= I18n.t('ar.shareSocial.text1') +' '+ player.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+player.username+'/'+video.url_public;
var message= I18n.t('ar.shareSocial.text1') +' '+ player.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+player.username+'/'+video.url_public;


const options = Platform.select({
  ios: {
    activityItemSources: [
      {
        // For sharing url with custom title.
        placeholderItem: { type: 'url', content: url },
        item: {
          default: { type: 'url', content: url },
        },
        subject: {
          default: title,
        },
        linkMetadata: { originalUrl: url, url, title },
      },
      {
        // For sharing text.
        placeholderItem: { type: 'text', content: message },
        item: {
          default: { type: 'text', content: message },
          message: null, // Specify no text to share via Messages app.
        },
        linkMetadata: {
          // For showing app icon on share preview.
          title: message,
        },
      },
    ],
  },
  default: {
    title,
    subject: title,
    message: `${message} ${url}`,
  },
});

Share.open(options);



        // Share.open({
        //     title: "Rate my performance on VFA.app using this link!",
        //     message: "Rate my performance on VFA.app using this link! "+ 'https://go.vfa.app/'+userJson.username,
        //     url: "file://" + ubicacion,
       
        //   });
      })
    }
    async function CompartirDOS() {
      setLoader(true)
      setTextLoader('Download video...');
      setTimeout(()=>{
        setTextLoader('Wait please...');
      }, 5000);
      // try {
        // if (Platform.OS == 'androd') {
          RNFS.downloadFile({
            fromUrl: video.file_video,
            toFile: `${RNFS.DocumentDirectoryPath}/video.mp4`
        }).promise.then((r) => {
          // console.warn(r)
            setLoader(false)
            setTextLoader('Loading...');
            Share.open({
              title: "",
              message: "",
              url: "file://" + `${RNFS.DocumentDirectoryPath}/video.mp4`,
              // type: 'video/mp4',
              // subject: "Skill " + skill.name,
            });
            // RNFS.unlink(`${RNFS.DocumentDirectoryPath}/video.mp4`)
            // .then(() => {
            //   console.warn('FILE DELETED');
            // }).catch((err) => {
            //   console.warn(err.message);
            // });
        });
        // }
      // } catch (error) {
      //   setLoader(false)
      //   setTextLoader('Loading...');
      //   console.warn(error)
      // }
    }



    async function Compartir() {
      setLoader(true)
      setTextLoader('Download video...');
      setTimeout(()=>{
        setTextLoader('Wait please...');
      }, 5000);
      // try {
        // if (Platform.OS == 'androd') {
          RNFS.downloadFile({
            fromUrl: video.file_video,
            toFile: `${RNFS.DocumentDirectoryPath}/video.mp4`
        }).promise.then((r) => {
          // console.warn(r)
            setLoader(false)
            setTextLoader('Loading...');
            Share.open({

              title: I18n.t('ar.shareSocial.text1') +' '+ player.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+player.username+'/'+video.url_public,
              message: I18n.t('ar.shareSocial.text1') +' '+ player.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+player.username+'/'+video.url_public,
              url: "file://" + `${RNFS.DocumentDirectoryPath}/video.mp4`,
              // type: 'video/mp4',
              // subject: "Skill " + skill.name,
            });
            // RNFS.unlink(`${RNFS.DocumentDirectoryPath}/video.mp4`)
            // .then(() => {
            //   console.warn('FILE DELETED');
            // }).catch((err) => {
            //   console.warn(err.message);
            // });
        });
        // }
      // } catch (error) {
      //   setLoader(false)
      //   setTextLoader('Loading...');
      //   console.warn(error)
      // }
    }

console.log(video);
    return (
        <View style={{ width: '100%', height: '100%', backgroundColor:'white'}}>
            {loader?<Loader title={textLoader}/>:null}
            <Image source={require('../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'),
                    position: "absolute",
                }} />
            <View style={{flex:1, justifyContent:'center', alignItems:'center', backgroundColor:'#000'}}>
                <Image source={{uri:video.file_captura}} style={{
                    height: '100%',
                    width: wp('70%'),
                    position: "absolute",
                }} />
                {/* <BlurView
                    style={{
                        height: '100%',
                        width: wp('100%'),
                        position: "absolute",
                    }}
                    blurType="light"
                    blurAmount={2}
                    reducedTransparencyFallbackColor="white"
                /> */}
                <TouchableOpacity activeOpacity={0.9} onPress={()=>{navigation.navigate('Video',{player, video})}} style={{position:'absolute', width:hp('15%'),height: hp('15%'), justifyContent:'center', alignItems:'center'}}>
                    <Icon2 name="play" size={hp('10%')} color="#FFF" style={{marginLeft:0}}/>
                </TouchableOpacity>
            </View>
            <View style={{height:hp('38%'),paddingTop:10}}>

                <View style={{flexDirection:'row', width:wp('100%'), paddingRight:20, marginTop:10, marginBottom:10, paddingLeft:15, height:hp('4%'), alignItems:'center'}}>

                     {/* En caso de tener coach  */}

                     {video.video_status == 1?
                    <View style={{flexDirection:'row',  alignItems:'center'}}>
                      <View style={{width:hp('4%'), height:hp('4%'), marginRight:5, borderRadius:hp('4%'), justifyContent:'center', alignItems:'center', backgroundColor: "#D8D8D8",}}> 
                        <Icon name="trophy" color="#eab900" size={hp('2.3%')}/>
                      </View>
                      <Text style={{color:'#535353', fontSize:hp('2%'), fontWeight:'bold'}}>{video.qualification}</Text>
                    </View>
                    :
                    <View style={{flexDirection:'row', width:wp('100%'), paddingRight:20, marginTop:10, marginBottom:10, paddingLeft:15, height:hp('4%'), alignItems:'center'}}>
{/* En caso de no tener coach  */}

                    <View style={{flexDirection:'row',  alignItems:'center'}}>
                      <View style={{width:hp('4%'), height:hp('4%'), marginRight:5, borderRadius:hp('4%'), justifyContent:'center', alignItems:'center', backgroundColor: "#D8D8D8",}}> 
                        <Icon name="star" color="#535353" size={hp('2.3%')}/>
                      </View>
                      <Text style={{color:'#535353', fontSize:hp('2%'), fontWeight:'bold'}}>{video.starts}</Text>
                    </View>
                    <View style={{paddingLeft:25, flexDirection:'row',  alignItems:'center'}}>
                      <View style={{width:hp('4%'), height:hp('4%'), marginRight:5, borderRadius:hp('4%'), justifyContent:'center', alignItems:'center', backgroundColor: "#D8D8D8",}}> 
                        <Icon2 name="eye" color="#535353" size={hp('2.3%')}/>
                      </View>
                      <Text style={{color:'#535353', fontSize:hp('2%'), fontWeight:'bold'}}>{video.visits}</Text>
                    </View>
                    <View style={{paddingLeft:25, flexDirection:'row',  alignItems:'center'}}>
                      <View style={{width:hp('4%'), height:hp('4%'), marginRight:5, borderRadius:hp('4%'), justifyContent:'center', alignItems:'center', backgroundColor: "#D8D8D8",}}> 
                        <Icon2 name="flash" color="#535353" size={hp('2.3%')}/>
                      </View>
                      <Text style={{color:'#535353', fontSize:hp('2%'), fontWeight:'bold'}}>{video.puntos}  {I18n.t('tex15')}</Text>
                    </View>

                    {/*  */}
                    </View>
                    }



                    
                  
                  
                    <View style={{height:'100%', width:'15%', position:'absolute', right:10, top:10, }}>
                        <Menu style={{width:'30%', height:'100%', marginRight:wp('20%'), justifyContent:'center', alignItems:'center'}}>

                        
                        
                            <MenuTrigger style={{width:wp('10%'),height:hp('8%'), justifyContent: 'center',alignItems: 'center', borderRadius: hp('2%'),}}>
                                    <Icon2 name="dots-horizontal" size={hp('3%')} style={{opacity:0.7}}/>
                            </MenuTrigger>
                            <MenuOptions customStyles={optionStyles}>

                               {video.video_status != 1?
                               <View>
                                <MenuOption onSelect={() => {
                                    Clipboard.setString('https://go.vfa.app/'+player.username+'/'+video.url_public);
                                    Snackbar.show({
                                    text: 'Copy link',
                                    duration: Snackbar.LENGTH_LONG,
                                    });
                                }} >
                                <View style={{marginLeft:wp('2%')}}>
                                    <Text style={{fontSize:wp('3%')}}>{I18n.t('profile.previaVideo.option1')}</Text>
                                </View>
                                </MenuOption>


                                <MenuOption onSelect={() => {
                                    Share.open({
                                      // title: "Rate my performance on VFA.app using this link!",
                                      message: I18n.t('ar.shareSocial.text1') +' '+ player.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+player.username+'/'+video.url_public,
                                      // url: "file://" + `${RNFS.DocumentDirectoryPath}/video.mp4`,
                                      // type: 'video/mp4',
                                      // subject: "Skill " + skill.name,
                                    });
                                    Snackbar.show({
                                    text: I18n.t('profile.menuProvider.option3'),
                                    duration: Snackbar.LENGTH_LONG,
                                    });
                                }} >
                                <View style={{marginLeft:wp('2%')}}>
                                    <Text style={{fontSize:wp('3%')}}>{I18n.t('profile.menuProvider.option3')}</Text>
                                </View>
                                </MenuOption>


                                {Platform.OS === 'android'?
                                  <MenuOption onSelect={() => {
                                      Platform.OS == 'android'?
                                       Compartir()
                                       : 
                                       CompartirIos()
                                      Snackbar.show({
                                      text: I18n.t('profile.previaVideo.option2'),
                                      duration: Snackbar.LENGTH_LONG,
                                      });
                                     }} >
                                    <View style={{marginLeft:wp('2%')}}>
                                        <Text style={{fontSize:wp('3%')}}>{I18n.t('profile.previaVideo.option2')}</Text>
                                    </View>
                                  </MenuOption>
                                :null} 

                                 </View>
                                 :null}
                               
                                {user.id === player.id?
                                <View>
                                  <MenuOption onSelect={() =>  ButtonDelete(video.id)}>
                                    <View style={{marginLeft:wp('2%')}}>
                                        <Text style={{fontSize:wp('3%')}}>{I18n.t('profile.previaVideo.option3')}</Text>
                                    </View>
                                  </MenuOption>

                                  <MenuOption onSelect={() =>  {
                                     Platform.OS == 'android'?
                                            CompartirDOS()
                                            : 
                                            CompartirDOSIos()
                                        }}>
                                  <View style={{marginLeft:wp('2%')}}>
                                      <Text style={{fontSize:wp('3%')}}>{I18n.t('profile.previaVideo.option2')}</Text>
                                  </View>
                                  </MenuOption>
                                  </View>

                                :null}
                            </MenuOptions>
                        </Menu>
                    </View>
                </View>

                <View style={{flexDirection:'row', paddingTop:10, paddingHorizontal:15,}}>

                    <View style={{ padding:4,  borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'gray',}}>
                        <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>{I18n.t('teams.levels.' + video.level_id)}</Text>
                    </View>
                    

                   
                          {video.video_status == 1?
                            video.qualification >= 7?
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'green',}}>
                                <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Approved</Text>
                            </View>
                            :video.qualification != null && video.qualification <= 6 ?
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'red',}}>
                            <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Reproved</Text>
                            </View>
                            :video.qualification == null?
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'orange',}}>
                            <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Pending</Text>
                            </View>
                            :null
                            :null
                           }


                            {/* VALIDAMOS EL ESTADO DEL VIDEO  */}
                {video.video_status == 0?
                  video.public_status == 1?
                    <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'green',}}>
                        <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>
                        {I18n.t('videoPlayer.text1')}
                        </Text>
                    </View>
                    :video.public_status == 2?
                      <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'red',}}>
                      <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>
                      {I18n.t('videoPlayer.text2')}
                      </Text>
                      </View>
                      :
                      <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'orange',}}>
                      <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>
                      {I18n.t('videoPlayer.text3')}
                      </Text>
                    </View> 
                       :null
                    } 

                      

                             

                    {/* FIN DE LA VALIDACION DEL ESTADO DEL VIDEO  */}

                </View>
                

                <View style={{paddingTop:10, paddingHorizontal:15, marginBottom:20}}>
                    <View>
                        <Text style={{fontSize:hp('2.5%'), paddingTop:5, opacity:0.8}}>{I18n.t('skills.'+video.skill_id)}</Text>
                        <Text style={{fontSize:hp('1.7%'), paddingTop:5, opacity:0.6}}>{I18n.t('teams.category.'+video.category_id)}</Text>
                    </View>
                </View>


                {video.video_status != 1? 

                <View style={{justifyContent:'center', alignItems:'center',}}>
                <TouchableOpacity  activeOpacity={0.9} 
                onPress={() => {
                  Platform.OS == 'android'?
                   Compartir()
                   : 
                   CompartirIos()
                 }}
                  style={styles.buttonShare}>
                                <Text style={{fontWeight:'bold', fontSize:wp('4%'),  textTransform:'uppercase', color:'white'}}>{I18n.t('profile.previaVideo.option2')}
                                 </Text>
                                 <Icon2 name="send" style={{marginLeft:10, fontSize:wp('4%'), color:'white'}}></Icon2>
                </TouchableOpacity>  
              </View>
              :null
              // <View style={{justifyContent:'center', alignItems:'center',}}>
              //   <TouchableOpacity  activeOpacity={0.9} 
              //   onPress={() => {
              //     Platform.OS == 'android'?
              //      CompartirDOS()
              //      : 
              //      CompartirDOSIos()
              //    }}
              //     style={styles.buttonShare}>
              //                   <Text style={{fontWeight:'bold', fontSize:wp('4%'),  textTransform:'uppercase', color:'white'}}>Share video  ...
              //                    </Text>
              //                    <Icon2 name="send" style={{marginLeft:10, fontSize:wp('4%'), color:'white'}}></Icon2>
              //   </TouchableOpacity>  
              // </View>
              }



            </View>
             
              

        </View>
    );
};

// define your styles
const styles = StyleSheet.create({

  buttonShare: {
    borderRadius:5, 
    backgroundColor:'#11A10F', 
    padding:9, 
    width:wp('70%'), 
    textAlign:'center', 
    alignItems:'center', 
    height:hp('5%'),  
    flexDirection:'row',
    justifyContent:'center'
  },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F3F3F3',
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    createButton: {
        width: width * 0.5,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#171717',
        shadowOffset: {width: -2, height: 4},
        shadowOpacity: 0.2,
        shadowRadius: 3,
    },
});

const optionStyles = {
    optionTouchable: {
      underlayColor: 'red',
      activeOpacity: 40,
    },
    optionWrapper: {
      margin: 5,
    },
    optionText: {
      color: 'black',
    },
  };
//make this component available to the app
export default VideoPlayer;
