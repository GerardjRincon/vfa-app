//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet,Switch, TouchableOpacity, Image, Modal, FlatList, Platform, PermissionsAndroid, Dimensions, TextInput, KeyboardAvoidingView, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Snackbar from 'react-native-snackbar';
import Loader from '../../coach/global/loader';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Toast, Button } from 'galio-framework';
import { white } from 'react-native-paper/lib/typescript/styles/colors';
import { url } from '../../../storage/config';
import I18n from 'i18n-js';



const Seguridad = ({navigation, route}) => {
const {player, token, update } = route.params;

// Properties State 

const [loader, setLoader] = useState(false);
const [stateCuenta, setStateCuenta] = useState({});
const [modalConfirmation, setModalConfirmation] = useState(false);
const [statePrivada, setStatePrivada] = useState(false);
const toggleSwitch = () => {
      setStatePrivada(previousState => !previousState)
     
};
const [isShow, setShow] = useState(false);


console.log(route.params); 


    useEffect(async() => {
        // Mounted 
        // listamos los datos del user 
        getUser();
    }, [])

    useLayoutEffect(() => {
        // Nombre de la router 
        navigation.setOptions({
            title: ' Parental control',
        })
    }, [])

    async function getUser(){
        try{
            const datos = {
               method: 'GET',
               headers: {
               'Accept': 'application/json',
               'Content-Type': 'application/json',
               'Authorization': 'Bearer '+token
               }
             }
   
             await fetch(url+'api/user', datos)
             .then(response => response.json())
             .then((res) => {
              if(res.state_profile == 1){
                setStatePrivada(true);
              }
              else if (res.state_profile == 0){
                setStatePrivada(false);
              }
              else {
                setStatePrivada(!res.state_profile);
              }
             })
             .catch(e => {
                 console.log(e, 'error server');
             })
   
          }
          catch(e){
           console.log(e, 'error try');
          }
   
    }


    // FUNCION PARA CAMBIAR EL ESTADO DE LA CUENTA 
    async function cambiarState(){

       try{
         const datos = {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+token
            },
            body: JSON.stringify({
                estado: !statePrivada,
            }),
          }

          await fetch(url+'api/state_profile', datos)
          .then(response => response.json())
          .then((res) => {
           console.log(res, "se cumple la funcition");
        //    setShow(!isShow);
        //    getUser();
          })
          .catch(e => {
              console.log(e, 'error server');
          })

       }
       catch(e){
        console.log(e, 'error try');
       }

    }

    // Estilos del boton coppa register 
    function AlertTwo(){
        Alert.alert(
            I18n.t('seguridad.alert.text1'),
            I18n.t('seguridad.alert.text2'),
            [
              {
                text:  I18n.t('seguridad.alert.buttonCancel'),
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: I18n.t('seguridad.alert.buttonOk'), onPress: () => DeleteCuenta() }
            ]
          )
       
    }

    async function DeleteCuenta(){

            let apiurl = url+'api/account/delete/'+ player.id;
            const datos = {
                method: 'DELETE',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
                }
              }
             try{
                await fetch(apiurl, datos)
                .then(response => response.json())
                .then((res) => {
                    setLoader(true);
                    console.log(res);
                    const keys = ['@user', '@token', '@skills', '@reviews', '@team', '@coach']
                     AsyncStorage.multiRemove(keys)
                    navigation.replace('Login');
                    logoutBackend();

                })
                .catch(e => {
                    console.log("Error en la consulta")
                })

             }
             catch(e){
               console.log("Error")
             }
    
    }


    async function logoutBackend(){
        await fetch(url + 'api/logout', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
        }).then(res => res.json())
        .then(async(dat) => {
            console.warn(dat)
            try {
                if (dat.ok) {
                    const keys = ['@user', '@token', '@skills', '@reviews', '@team', '@coach']
                    await AsyncStorage.multiRemove(keys)
                    navigation.replace('Login');
                }
            } catch (error) {
                Snackbar.show({
                    text: 'Failed to logout',
                    duration: Snackbar.LENGTH_LONG,
                });
            }
        })
    }









    return (
        <View style={styles.container}>
            {loader ? <Loader title={"In process, please wait"}/> : null}
            

           {/* MODAL COPPA  */}
           <Modal
        animationType="slide"
        transparent={true}
        visible={modalConfirmation}
        
      >


        <View style={styles.centeredView }>

          <View style={[styles.modalView]}>

               <View>
               <Text style={[styles.modalTextTiTle, {textTransform:'uppercase'}]}>
               {I18n.t('seguridad.text1')}
                </Text>

                <Text style={[styles.subTitle, {marginBottom:10}]}>
                {I18n.t('seguridad.text2')}
                </Text>
               
              </View>
           


              <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
              <Button style={[styles.createButtonAlertCancel,  {backgroundColor:'#eee', color:'#3d3d3d'}]}  onPress={() => {
                                          setModalConfirmation(false)
                                }}>
                            <Text style={{fontWeight:'bold', color:'#3d3d3d'}}>
                            {/* {I18n.t('registerPlayer.player.submit')} */}
                            
                            {I18n.t('seguridad.button1')}
                            
                            </Text>
                                   
                </Button>


                <Button style={[styles.createButtonAlert, {backgroundColor: 'red',color:'#fff',} ]} color="success" onPress={() => {
                                        AlertTwo();
                                }}>
                            <Text style={{fontWeight:'bold', color:'#fff'}}>
                       
                            {I18n.t('seguridad.button2')}
                            </Text>
                                   
                </Button>

                </View>


          </View>
        </View>
      </Modal>

      {/* FIN MODAL COPPA */}
            {/* <Toast isShow={isShow} positionIndicator="top" color="primary">This is a top positioned toast</Toast> */}
            <View style={{padding:wp('6%')}}>
                <Text style={styles.title}>Account privacy</Text>

                {/* Switch del cambio de privaciodad  */}
                   
                  <View style={{flexDirection:'row',  marginTop:hp('4%')}}>
                       
                       <Icon name="lock" style={styles.iconos}></Icon>
                       <Text style={styles.fuente}>Private account</Text>

                       <Switch
                        style={{position:'absolute', right:wp('2%'), width:50, top:hp('-0.3%')}}
                        trackColor={{ false: "#b4b4b4", true: "#2ebb29" }}
                        thumbColor={!statePrivada ? "#eee" : "#eee"}
                        ios_backgroundColor="#eee"
                        onValueChange={toggleSwitch}
                        onChange={()=> cambiarState()}
                        value={statePrivada}
                    />
                       
                  </View>
                {/* Fin Switch del cambio de privaciodad  */}
              </View>


                   <View
                    style={{
                        borderWidth: 0.2,
                        borderColor:'#c7c7c7',
                    }}
                    />




        <View style={{padding:wp('6%')}}>
                <Text style={styles.title}>Account control </Text>
                  <View style={{flexDirection:'row',  marginTop:hp('4%')}}>
                       <Icon name="trash" style={styles.iconos}></Icon>
                       <Text style={styles.fuente}>Delete my account</Text>
                  </View>

                  <View>
                      <Text style={styles.textSmall}>
                         {I18n.t('seguridad.text3')}
                      </Text>
                  </View>

                  <View style={{justifyContent:'center', alignItems:'center', marginTop:20}}>
                     <Button  style={styles.buttonCreate}
                         onPress={() => { 
                             setModalConfirmation(true);
                         }}
                         >
                        <Text style={styles.textButton}>
                        {I18n.t('seguridad.text4')}
                        </Text>  
                         </Button>
                  </View>
                
              </View>

              



           
        </View>
    );



}



const styles = StyleSheet.create({

   
      centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0,
        backgroundColor:'#0000002e',
      },
      modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        width:wp('85%'),
        padding: 35,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
      },
      button: {
        borderRadius: 5,
        padding: 10,
        elevation: 2,
        width:wp('30%'),
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        backgroundColor: "#00c900",
        textTransform:'uppercase',
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textTransform:'uppercase',
      },
      modalText: {
        marginBottom: 5,
      },
    
    
      modalTextTiTle: {
        marginBottom: 5,
        fontSize:wp('5%'),
        fontWeight:'normal',
        color:'red',
        fontWeight:'bold',
      },
    
      subTitle:{
        fontWeight:'normal',
        fontSize:wp('4%'),
        color:'#404040',
        marginTop:10,
        
    
    
      },
      areaText:{
        fontWeight:'normal',
        fontSize:wp('4%'),
        color:'#3d3d3d'
    
      },

      margin5:{
         marginTop:5,
      },
      margin10:{
        marginTop:10,
     },
    
      textPuntos:{
        backgroundColor:'#eee',
        padding:2,
        paddingLeft:10, 
        paddingRight:10,
        color:'#404040',
        borderRadius:5,
        marginTop:10,
        fontSize:wp('4%'),
        
      },

      createButtonAlertCancel: {
        width: wp('30%'),
        marginTop: 25,
        marginBottom: 0,
        shadowColor: '#3d3d3d',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderRadius:10,
        elevation: 5,
        justifyContent:'center',
        alignItems:'center',
        height:hp('6%'),
    },


      createButtonAlert: {
        width: wp('30%'),
        marginTop: 25,
        marginBottom: 0,
        shadowColor: 'red',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderRadius:10,
        elevation: 5,
        justifyContent:'center',
        alignItems:'center',
        height:hp('6%'),
    },

    


    buttonCreate:{
        borderRadius:5, 
        backgroundColor:'#11A10F', 
        padding:9, 
        // width:wp('70%'), 
        textAlign:'center', 
        alignItems:'center', 
        // height:hp('5%'),  
        flexDirection:'row',
        justifyContent:'center'
    },
    textButton: {
        color:'white',
        fontWeight:'bold',
        textTransform:'uppercase'
    },

   
    textSmall:{
     color:"#3d3d3d",
     fontSize:wp('4%'),
     marginLeft:wp('7.5%'),
     fontWeight:'normal'
    }, 
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

    title:{
        fontWeight:'bold',
        fontSize:wp('4.5%'),
        color:'#3d3d3d'
    },
    fuente:{
        fontSize:wp('4.8%')
    },

    iconos:{
       fontSize:wp('6%'),
       color:'#a5a5a5',
       marginRight:10
    }


})



export default Seguridad