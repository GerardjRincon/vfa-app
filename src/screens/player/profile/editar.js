import  React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Modal, Pressable, Image, KeyboardAvoidingView,
    TouchableOpacity,
    Keyboard,
    Linking,
    TouchableWithoutFeedback, } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScrollView } from 'react-native-gesture-handler';
import { Block, Input,Button as GaButton } from 'galio-framework'
import DatePicker from 'react-native-date-picker'
import moment from "moment";
import { url } from '../../../storage/config';
import Snackbar from 'react-native-snackbar';
import { LogoutUser, setTeam, setUser } from "../../../storage/user/dataUser";
import I18n from 'i18n-js';


const editar = (props) => {

    const {user, modalEditProfile, token } = props;

    const [name, setName] = useState(user.name);
    const [username, setUsername] = useState(user.username.replace('@', ''));
    const [email, setEmail] = useState(user.email);
    const [pass, setPass] = useState(null);

    const [date, setDate] = useState(new Date())
    const [open, setOpen] = useState(false)
    const [campo, setCampo] = useState(null);
    const [age, setAge] = useState(user.age);

    useEffect(async() =>{
        // setData(props.user)

    }, [])




    async function UpdateInfo(){
       props.openLoader();
        try {
            const data = new FormData();
            if (name) {
                data.append('name', name);
            }
            if (username) {
                data.append('username', '@'+username);
            }
            if (age) {
                data.append('age', age);
            }

            if (name || age || username) {

                const datos = {
                    method: 'POST',
                    headers: {
                      'Content-Type': 'multipart/form-data',
                      'Authorization': 'Bearer '+token
                    },
                    body:data
                    }

                await fetch(url+'api/update/profile/player',datos).then(res => res.json())
                    .then((dat) =>{
                        console.warn(dat)
                        if (dat.ok) {
                            Snackbar.show({
                                text: I18n.t('profile.update'),
                                duration: Snackbar.LENGTH_LONG,
                            });

                            props.updateUser(dat.data);  // setU(dat.data);
                            props.closeModal()
                            // Pasamos una que actualice el user 
                            // Otra que carge el loader 
                        } else {
                            alert(dat.data.username);
                            Snackbar.show({
                                text: I18n.t('profile.errorUpdate'),
                                duration: Snackbar.LENGTH_LONG,
                            });
                            // setLoader(false);
                            props.closeLoader();
                        }
                    });
            }else{
                Snackbar.show({
                    text: I18n.t('profile.updateInfo'),
                    duration: Snackbar.LENGTH_LONG,
                });
            }

            
        } catch (error) {

            console.log(error);
            Snackbar.show({
                text: I18n.t('profile.required'),
                duration: Snackbar.LENGTH_LONG,
            });
            // setLoader(false);
            props.closeLoader();
        }
    }




    return(

    <Modal
    animationType="slide"
    style={styles.modalContent}
    transparent={true}
    backdropColor = {'white'}
    backdropOpacity = {1}
    visible={modalEditProfile}
    
  >
    <View style={styles.centeredView}>

    <View style={{paddingLeft:wp('10%'), marginTop:20, flexDirection:'row'}}>
     <Icon name="user-circle-o" size={20} style={{marginRight:10}}></Icon>
    <Text style={{textAlign:'left', fontWeight:'bold', color:'#3d3d3d'}}>
      {I18n.t('profile.textInfo')}
     </Text>
     </View>



       <Image source={require('../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    opacity:0.4,
                    position: "absolute",
            }} /> 
            
      
        <Pressable onPress={()=> {props.botonClick()}} style={{position:'absolute', top:20, left:15, color:'#3d3d3d' }}>
        <Icon2 name="close" size={30} ></Icon2>
        </Pressable>
        <Text style={{position:'absolute', top:25, left:60, color:'#3d3d3d', fontWeight:'bold', fontSize:19}}>
        {I18n.t('profile.menuProvider.option1')}
        </Text>


    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>


     
      <View style={styles.modalView}>

             {/* <Text style={{fontWeight:'bold'}}>
             Update your account information
             </Text> */}

                                       <Input
                                            left
                                            icon="user"
                                            family="font-awesome"
                                            style={styles.inputs}
                                            iconSize={hp('1.8%')}
                                            placeholder="Enter your name"
                                            value={name}
                                            bottomHelp
                                            onChangeText={(e)=>setName(e)}
                                            placeholderTextColor="#818181"
                                            
                                        />


                                        <Input
                                            left
                                            icon="id-card"
                                            family="font-awesome"
                                            style={styles.inputs}
                                            iconSize={hp('1.8%')}
                                            placeholder="Enter your username"
                                            value={username}
                                            autoCapitalize='none'
                                            bottomHelp
                                            onChangeText={(e)=> {setUsername(e.replace(' ', ''))}}
                                            placeholderTextColor="#818181"
                                            disabled
                                            
                                        />

                                     <TouchableOpacity disabled={true}>
                                         <Input
                                            left
                                            disabled={true}
                                            icon="envelope"
                                            family="font-awesome"
                                            style={[styles.inputs,{backgroundColor:'#f4f4f4'}]}
                                            iconSize={hp('1.8%')}
                                            autoCapitalize='none'
                                            // placeholder="Enter your email"
                                            value={email}
                                            // flat={disabled}
                                            contextMenuHidden={true} 
                                            // bottomHelp
                                            // onChangeText={(e)=>setEmail(e)}
                                            placeholderTextColor="#818181"
                                        />
                                        </TouchableOpacity>

                                    <DatePicker
                                        modal
                                        mode="date"
                                        open={open}
                                        maximumDate={new Date()}
                                        date={date}
                                        onConfirm={(date) => {
                                        setOpen(false)
                                        setDate(date)

                                        const edad = moment(date).format("YYYY-MM-DD");
                                        setAge(edad);
                                    
                                        }}
                                        onCancel={() => {
                                        setOpen(false)
                                        }}
                                    />

                                    <Input
                                        onPressIn={() =>{setOpen(true)}}
                                        // icon="envelope"
                                        // placeholder={I18n.t('registerPlayer.player.date')}
                                        icon="calendar"
                                        family="font-awesome"
                                        style={styles.inputs}
                                        iconSize={hp('1.8%')}
                                        bottomHelp
                                        value={age}
                                        onChangeText={(e) => setAge(e)}
                                        placeholderTextColor="#818181"
                                    
                                    />

              <Pressable
                style={styles.buttonShare}
                onPress={() => UpdateInfo()}
              >
                <Text style={styles.textStyle}>{I18n.t('profile.buttomSave')}</Text>
              </Pressable>
      </View>
      
    </TouchableWithoutFeedback>
   </KeyboardAvoidingView>


   {/* linea  */}
   <View style={{paddingLeft:wp('10%'), marginTop:20, flexDirection:'row'}}>
   <Pressable
                style={{flexDirection:'row'}}
                onPress={async() => { 
                    try {
                        await Linking.openURL('https://go.vfa.app');
                      } catch (e) {
                        console.warn(e)
                      }
                }}
              >
    <Icon name="lock" size={20} style={{marginRight:10}}></Icon>
    <Text style={{textAlign:'left', fontWeight:'bold', color:'#5605fb', opacity:0.8}}>
     {I18n.t('profile.textUpdatePass')}
     </Text>
     </Pressable>
     </View>

    </View>
  </Modal>
    )
}

export default editar;


const styles = StyleSheet.create({
    buttonShare: {
        borderRadius:5, 
        backgroundColor:'#11A10F', 
        padding:9, 
        marginTop:20,
        width:wp('80%'),
        textAlign:'center', 
        alignItems:'center', 
        height:hp('6.4%'),  
        flexDirection:'row',
        justifyContent:'center'
      },
    modalContent: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        margin: 0,
        
      },

    centeredView: {
        flex: 1,
        height:hp('100%'),
        width:wp('100%'),
        backgroundColor:'white',
        justifyContent: "center",
        // alignItems: "center",
        // marginTop: 0,
        // backgroundColor:'#0000002e',
      },
      modalView: {
        // margin: 20,
        // height:hp('100%'),
        // backgroundColor: "white",
          width:wp('100%'),
         alignItems: "center",
        // padding: 35,
        // alignItems: "center",
        
      },
      button: {
        borderRadius: 5,
        padding: 10,
        elevation: 2,
        width:wp('50%'),
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        backgroundColor: "#00c900",
        textTransform:'uppercase',
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
        textTransform:'uppercase',
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center"
      },
    
    
      modalTextTiTle: {
        marginBottom: 15,
        textAlign: "center",
        fontSize:wp('9%'),
        fontWeight:'normal',
        color:'#333'
      },
    
      subTitle:{
        fontWeight:'bold',
        fontSize:wp('6%'),
        textAlign: "center",
        textTransform:'uppercase',
        color:'#404040',
        
    
    
      },
      areaText:{
        marginTop:22,
        marginBottom:22,
        fontWeight:'normal',
        fontSize:wp('4%'),
        textAlign: "center",
    
      },
    
      textPuntos:{
        backgroundColor:'#eee',
        padding:2,
        paddingLeft:10, 
        paddingRight:10,
        color:'#404040',
        borderRadius:5,
        marginTop:10,
        fontSize:wp('4%'),
        
      },

      inputs: {
        height: hp('6%'),
        width:wp('80%'),
        borderWidth:1,
        borderColor:"#BFBEBE",
        borderRadius:5,
        paddingLeft:13,
        color:"#FFF",
        backgroundColor: "#FFF",
        
    },


    
})

