//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, useWindowDimensions, Text, StyleSheet, TouchableOpacity, Image, Modal, FlatList, Platform, PermissionsAndroid, Dimensions, TextInput, KeyboardAvoidingView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Snackbar from 'react-native-snackbar';
import { LogoutUser, setTeam, setUser } from "../../../storage/user/dataUser";
import { url } from '../../../storage/config';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import * as Progress from 'react-native-progress';
import Dialog from "react-native-dialog";
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {request, requestMultiple, PERMISSIONS} from 'react-native-permissions';
import { Block, Input,Button as GaButton } from 'galio-framework'
import DatePicker from 'react-native-datepicker'
import ImagePicker from 'react-native-image-crop-picker';
import md5 from 'md5';
import { SvgUri } from 'react-native-svg';
// import { channel, pusher } from '../../../routes/validationSesion';
import Clipboard from '@react-native-clipboard/clipboard';
import Share from 'react-native-share';

import {signal, canal} from '../../../storage/pusher';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
import I18n from 'react-native-i18n';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Loader from '../../coach/global/loader';
import Editar from './editar';

import { TabView, SceneMap,TabBar} from 'react-native-tab-view';



// constantes de vista
const { width, height } = Dimensions.get('screen');

// create a component
const Profile = ({ navigation }) => {
    const [data, setData] = useState([]);
    const [dataHomework, setDataHomework] = useState([]);
    const [user, setU] = useState({});
    const [token, setToken] = useState(null);
    const [team, setTeam] = useState({});
    const [visibleModalImagen, setVisibleModalImagen] = useState(false);
    const [visible, setVisible] = useState(false);
    const [loader, setLoader] = useState(false);
    const [porcentaje, setPorcentaje] = useState(0);
    const [porcentajeHomework, setPorcentajeHomework] = useState(0);
    const [svg, setSvg] = useState(null);

    // variables de info del perfil
    const [name, setName] = useState(null);
    const [username, setUsername] = useState(null);
    const [email, setEmail] = useState(null);
    const [age, setAge] = useState(null);
    const [pass, setPass] = useState(null);
    const [refresh, setRefresh] = useState(false)

    const [index, setIndex] = useState(0);

    const [routes] = useState([ //// Rutas del tabView
      { key: 'videos', title: I18n.t('tex8') },
      { key: 'homework', title: I18n.t('tex9') },
  ]);


    useEffect(async() => {
      const usersy = await AsyncStorage.getItem('@user');
      const token = await AsyncStorage.getItem('@token');
      const team = await AsyncStorage.getItem('@team');
      const userJson = JSON.parse(usersy);
      const tokenJson = JSON.parse(token);
      const teamJson = JSON.parse(team);
         
            setName(userJson.name);
            setEmail(userJson.email);
            setAge(userJson.age);
            setUsername(userJson.username);
            setToken(tokenJson);
            setTeam(teamJson);
            setU(userJson);
            
            // Entrardo por primera vez 
            if (userJson) {
                getMyInfo(tokenJson, userJson);
                getVideosHomework(tokenJson, userJson);
            }
             // Entrardo por segunda vez 
            const unsubscribe = navigation.addListener('focus', () => {
              // getMyInfo(tokenJson, userJson);
              getVideosHomework(tokenJson, userJson);
            });

            canal.bind("Notification-" + userJson.id, (data) => {
              if (data.tag === 'ReplyPlayerUpdate') {
                  setTeam(data.noti);
              }
            });

            canal.bind("NotificationX-" + userJson.id, (data) => {
              console.log('cargando el console que subio de nivel');
              getMyInfo(tokenJson, data.user);
            });


            // return unsubscribe;


            // const unsubscribe = navigation.addListener('focus', async() => {
            // canal.bind("Notification-" + userJson.id, (data) => {
            //   if (data.tag === 'ReplyPlayerUpdate') {
            //       setTeam(data.noti);
            //   }
            // });
            //  })

            return () => {
              signal.disconnect();
              // unsubscribe;
              unsubscribe
            }

    },[navigation])

    function suma(number) {
      var real = parseInt(number);
      var length = real.toString().length;
      var cero = "0"; /* String de cero */  
      
      if (number >= 100) {
          return "1"; 
      } else {
          return (cero.repeat(1) +"."+(cero.repeat(2-length))+real); 
          // return length
      }
    }
    

    async function getVideosHomework(tokenJson, user){

      console.log("entre")
      try{
        const datos = {
          method: 'POST',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '+tokenJson
          },
          body: JSON.stringify({
            id: user.id,
          })
        }

        await fetch(url+'api/player/find/videos', datos)
        .then(response => response.json())
        .then((res) => {
         
        // VIDEOS PUBLICOS 
          setData(res.videosPublic) 
          var num = suma(res.porcentaje)
          setPorcentaje(num)

          console.log(res.videosPublic);


          // VIDEOS PRIVADOS 
          setDataHomework(res.videos);

          console.log(res.videos);

          // var num2 = res.porcentaje;
          setPorcentajeHomework(res.porcentaje)


        })
        .catch(e => {console.log(e)})
      }
      catch(error) {
         console.log(error);
      }
    }




    async function getMyInfo(tokenJson,user){
      console.log("estamos aqui 23re3r");
        try {
            await fetch(url+'api/player/find/public',{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+tokenJson
            },
            body: JSON.stringify({
              id: user.id,
            }),
        }).then(res => res.json())
        .then((dat) =>{
            try {
                if (dat.ok) {
                  // setRefresh(false)
                  // setData(dat.data.videos) 
                  // console.log("wedwedw wewewedwedwed");
                  setU(dat.data)
                  setSvg('https://go.vfa.app/'+dat.data.avatar_svg);
                  // var num = suma(dat.data.videos.length)
                  // setPorcentaje(num)
                }
                if (dat.message == 'Unauthenticated.') {
                    LogoutUser()
                    setTimeout(()=>{
                        navigation.replace('Login')
                    },1)
                }
            } catch (error) {
              // setRefresh(false)
                Snackbar.show({
                    text: error,
                    duration: Snackbar.LENGTH_LONG,
                });
            }
        })
            
        } catch (error) {
          setRefresh(false)
            Snackbar.show({
                text: error,
                duration: Snackbar.LENGTH_LONG,
            });
        }
    }

    async function UpdatePhoto(img){
        setLoader(true);
        try {
            const data = new FormData();
            data.append('photo', {
                uri: img.path,
                name: 'image.jpg',
                type: 'image/jpg',
                data: img.data,
            });
            await fetch(url+'api/update/profile/player',{
                method: 'POST',
                headers: {
                  'Content-Type': 'multipart/form-data',
                  'Authorization': 'Bearer '+token
                },
                body:data
                }).then(res => res.json())
                .then((dat) =>{
                    console.warn(dat)
                    if (dat.ok) {
                        Snackbar.show({
                            text: I18n.t('profile.updatePhoto'),
                            duration: Snackbar.LENGTH_LONG,
                        });
                       var da =  setUser(dat.data);

                       console.log(da, '---- dta actualizacion');


                        // setU(dat.data);
                        getMyInfo(token,user)
                        setLoader(false);
                    } else {
                        Snackbar.show({
                            text: I18n.t('profile.errorUpdate'),
                            duration: Snackbar.LENGTH_LONG,
                        });
                        setLoader(false);
                    }
                });
            
        } catch (error) {
            Snackbar.show({
                text: I18n.t('profile.required'),
                duration: Snackbar.LENGTH_LONG,
            });
            setLoader(false);
        }
    }


    async function UpdateInfo(){
        setLoader(true);
        try {
            const data = new FormData();
            if (name) {
                data.append('name', name);
            }
            if (username) {
                data.append('username', username);
            }
            if (age) {
                data.append('age', age);
            }
            if (pass) {
                data.append('password_app', md5(pass));
            }

            if (name || age || pass) {
                await fetch(url+'api/update/profile/player',{
                    method: 'POST',
                    headers: {
                      'Content-Type': 'multipart/form-data',
                      'Authorization': 'Bearer '+token
                    },
                    body:data
                    }).then(res => res.json())
                    .then((dat) =>{
                        console.warn(dat)
                        if (dat.ok) {
                            Snackbar.show({
                                text: 'Your profile info was updated',
                                duration: Snackbar.LENGTH_LONG,
                            });
                            setUser(dat.data);
                            setU(dat.data);
                            setVisible(false);
                            setLoader(false);
                        } else {
                            Snackbar.show({
                                text: 'An error occurred while updating',
                                duration: Snackbar.LENGTH_LONG,
                            });
                            setLoader(false);
                        }
                    });
            }else{
                Snackbar.show({
                    text: 'You must update at least one piece of information',
                    duration: Snackbar.LENGTH_LONG,
                });
            }

            
        } catch (error) {
            Snackbar.show({
                text: 'The request could not be processed',
                duration: Snackbar.LENGTH_LONG,
            });
            setLoader(false);
        }
    }

    function mensaje(mensaje){
        Snackbar.show({
            text: mensaje,
            duration: Snackbar.LENGTH_LONG,
        });
    }




    

    async function selectCamera() {
        if (Platform.OS === 'android') {
          async function requestCameraPermission() {
            try {
              const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
                  'title': 'Permiso Camara',
                  'message': 'Tamyda necesita acceso a la camara'
                }
              )
              if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                
                ImagePicker.openCamera({
                  width: 256,
                  height: 356,
                  cropping: true,
                  includeBase64:true
                }).then(image => {
                  setVisibleModalImagen(false);
                  UpdatePhoto(image);
                });
    
              } else {
                // alert("Permiso denegado");
                Snackbar.show({
                  text: 'Permiso denegado.',
                  duration: Snackbar.LENGTH_LONG,
                });
              }
            } catch (err) {
              alert("Error:", err);
              console.warn(err)
              Snackbar.show({
                text: 'Ha ocurrido un error.',
                duration: Snackbar.LENGTH_LONG,
              });
            }
          }
          requestCameraPermission();
        }else{
          request(PERMISSIONS.IOS.CAMERA).then((result) => {
            if (result === 'granted'|'limited') {
              ImagePicker.openCamera({
                width: 256,
                height: 356,
                cropping: true,
                includeBase64:true
              }).then(image => {
                setVisibleModalImagen(false);
                UpdatePhoto(image);
              });
    
            }else{
              Snackbar.show({
                text: 'No se han concedido los permisos necesarios para acceder a la camara.',
                duration: Snackbar.LENGTH_LONG,
              });
            }
          });
        }   
    }

    async function selectGaleria(){
        if (Platform.OS === 'android') {
          async function requestCameraPermission() {
            try {
              const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, {
                  'title': 'Permiso Galeria',
                  'message': 'Tamyda necesita acceso a la galeria'
                }
              )
              if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                ImagePicker.openPicker({
                  width: 256,
                  height: 356,
                  cropping: true,
                  includeBase64:true
                }).then(image => {
                  setVisibleModalImagen(false)
                  UpdatePhoto(image);
                });
    
              } else {
                Snackbar.show({
                  text: 'Permiso denegado.',
                  duration: Snackbar.LENGTH_LONG,
                });
              }
            } catch (err) {
              // alert("Error:", err);
              Snackbar.show({
                text: err,
                duration: Snackbar.LENGTH_LONG,
              });
            }
          }
          requestCameraPermission();
        }else{
          request(PERMISSIONS.IOS.PHOTO_LIBRARY).then((result) => {
            console.warn(result);
            if (result === 'granted'|'limited') {
              ImagePicker.openPicker({
                width: 256,
                height: 356,
                cropping: true,
                includeBase64:true
              }).then(image => {
                setVisibleModalImagen(false)
                UpdatePhoto(image);
              });
            }else{
              Snackbar.show({
                text: 'No se han otorgado los permisos necesarios para acceder a tu almacenamiento.',
                duration: Snackbar.LENGTH_LONG,
              });
            }
          });
        }
    }

    function calcularEdad(fecha) {
      var hoy = new Date();
      var cumpleanos = new Date(fecha);
      var edad = hoy.getFullYear() - cumpleanos.getFullYear();
      var m = hoy.getMonth() - cumpleanos.getMonth();
  
      if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
          edad--;
      }
  
      return edad;
    }






  function setNumber(num) {
    num = num.toString().replace(/[^0-9.]/g, '');
    if (num < 1000) {
       return num;
    }
    let si = [{
          v: 1E3,
          s: "K"
       },
       {
          v: 1E6,
          s: "M"
       },
       {
          v: 1E9,
          s: "B"
       },
       {
          v: 1E12,
          s: "T"
       },
       {
          v: 1E15,
          s: "P"
       },
       {
          v: 1E18,
          s: "E"
       }
    ];
    let index;
    for (index = si.length - 1; index > 0; index--) {
       if (num >= si[index].v) {
          break;
       }
    }
    return (num / si[index].v).toFixed(2).replace(/\.0+$|(\.[0-9]*[1-9])0+$/, "$1") + si[index].s;
 }




    
    


  // FUNCIONES DE RENDERZADO 

    // SESSION DE LOS VIDEOS 
    const RenderItem=({item, index, separators})=>{
      var urlImage = item.file_captura;

      console.log(item);
      return(
          <View style={{width:wp('33%'), height: '95%', marginHorizontal:2, marginRight:0, marginBottom: 2, elevation: 3,}}>
               <TouchableOpacity activeOpacity={0.8} onPress={()=> navigation.navigate('Video Profile',{player:user, i:index, token:token,update:(message)=>{mensaje(message)}, video:item})}  style={{flex:1}}>
                   <View style={{alignItems:'center', justifyContent:'center',}}>
                      
                      <View style={{backgroundColor:'#333', position:'absolute', opacity:0.7, left:5, zIndex:100, top:5, padding:3, borderRadius:5}}>
                         <Text style={{color:"#fff", fontWeight:'bold'}}>{I18n.t('teams.levels.' + item.level_id)}</Text>
                      </View>

                      <Image source={{uri:urlImage}} resizeMode='stretch' style={{width:wp('33%') ,height: hp('21%'),}} />
                      <View style={{position:'absolute', width:hp('10%'),height: hp('10%'), justifyContent:'center', alignItems:'center'}}>
                          <Icon name="play" size={hp('4')} color="#FFF" style={{marginLeft:10}}/>
                      </View>

                      <View style={{backgroundColor:'#333', position:'absolute', opacity:0.7, left:5, zIndex:100, bottom:5, padding:3, borderRadius:5}}>
                         <Text style={{color:"#fff", fontWeight:'bold'}}>{I18n.t('skills.'+item.skill_id).substr(0,15)} ..</Text>
                      </View>


                   </View>
               </TouchableOpacity>

               
          </View>
      );
  }
  // FIN DE LA SESSION VIDEO  
  const Videos = () => {
    return(
      <View style={{ flex: 1,}}>

              {/* RENDER DE LOS VIDEOS  */}
              <FlatList
                    data={data}
                    renderItem={RenderItem}
                    keyExtractor={(item, index) => index}
                    numColumns={3}
                    initialNumToRender={5}
                    // style={{width: wp('100%'), flex: 1, marginTop:15}}
                    // onRefresh={() => getVideosHomework(token,user)}
                    refreshing={false}
                    onEndReachedThreshold={0.7}
                    removeClippedSubviews={true}
                    ListEmptyComponent={EmptyList}
                />
                {/* RENDER DE LOS VIDEOS  */}
      </View>
    )
  }

 



    // SESSION DE LOS VIDEOS HOMEWORK
    const RenderItemHomework=({item, index, separators})=>{
      var urlImage = item.file_captura;
      return(
          <View style={{width:wp('33%'), height: '95%', marginHorizontal:2, marginRight:0, marginBottom: 2, elevation: 3,}}>
               <TouchableOpacity activeOpacity={0.8} onPress={()=> navigation.navigate('Video Profile',{player:user, i:index, token:token,update:(message)=>{mensaje(message)}, video:item})}  style={{flex:1}}>
                   <View style={{alignItems:'center', justifyContent:'center',}}>

                   <View style={{backgroundColor:'#333', position:'absolute', opacity:0.7, left:5, zIndex:100, top:5, padding:3, borderRadius:5}}>
                         <Text style={{color:"#fff", fontWeight:'bold'}}>{I18n.t('teams.levels.' + item.level_id)}</Text>
                      </View>


                      <Image source={{uri:urlImage}} resizeMode='stretch' style={{width:wp('33%') ,height: hp('21%'),}} />
                      <View style={{position:'absolute', width:hp('10%'),height: hp('10%'), justifyContent:'center', alignItems:'center'}}>
                          <Icon name="play" size={hp('4')} color="#FFF" style={{marginLeft:10}}/>
                      </View>

                      <View style={{backgroundColor:'#333', position:'absolute', opacity:0.7, left:5, zIndex:100, bottom:5, padding:3, borderRadius:5}}>
                         <Text style={{color:"#fff", fontWeight:'bold'}}>{I18n.t('skills.'+item.skill_id).substr(0,15)} ..</Text>
                      </View>
                      
                   </View>
               </TouchableOpacity>

               
          </View>
      );
  }
  // FIN DE LA SESSION VIDEO  
  const VideosHomework = () => {
    return(
      <View style={{ flex: 1,}}>

              {/* RENDER DE LOS VIDEOS  */}
              <FlatList
                    data={dataHomework}
                    renderItem={RenderItemHomework}
                    keyExtractor={(item, index) => index}
                    numColumns={3}
                    initialNumToRender={5}
                    // style={{width: wp('100%'), flex: 1, marginTop:15}}
                    // onRefresh={() => getVideosHomework(token,user)}
                    refreshing={false}
                    onEndReachedThreshold={0.7}
                    removeClippedSubviews={true}
                    ListEmptyComponent={EmptyListTwo}
                />
                {/* RENDER DE LOS VIDEOS  */}
      </View>
    )
  }



  const EmptyList = () => { // Render que se muestra cuando la lista esta vacia 
    return(
        <View style={{flex:1,height:hp('70%'),justifyContent:'center', alignItems:'center'}}>
            <Icon2 name="video" size={hp('10%')} color="#b4b4b4" style={{opacity:0.4}}/>
            <Text style={{fontSize:hp('2%'), opacity:0.7, color:'#b4b4b4', fontWeight:'bold'}}>
            {I18n.t('profile.listEmpty')}
            </Text>
        </View>
    )
  }



  const EmptyListTwo = () => { // Render que se muestra cuando la lista esta vacia 
    return(
        <View style={{flex:1,height:hp('70%'),justifyContent:'center', alignItems:'center'}}>
            <Icon2 name="video" size={hp('10%')} color="#b4b4b4" style={{opacity:0.4}}/>
            <Text style={{fontSize:hp('2%'), width:wp('60%'), textAlign:'center', opacity:0.7, color:'#b4b4b4', fontWeight:'bold'}}>
            {I18n.t('profile.listEmpty2')}
            </Text>
        </View>
    )
  }



  

  // FIN DE LAS FUNCIONES DE RENDERIZADO 

    

    const imagePlayer = 'https://go.vfa.app'+user.photo;
    return (
        <View style={styles.container}>


        

            {loader?<Loader/>:null}
            {/* <Image source={require('../../../assets/pelota.png')} style={{
                    height: '100%',
                    width: wp('100%'), 
                    position: "absolute",
            }} /> */}

          {visible?
           <Editar user={user} 
           modalEditProfile={visible} 
           token={token} 
           updateUser={(data) => {getMyInfo(token, data)}}
           closeModal={() => { setVisible(false); setLoader(false)}}
           openLoader={() =>{ setLoader(true)}}
           closeLoader={() =>{ setLoader(false)}}
           botonClick={() =>{setVisible(false)}}/>
           :null
           }

            <View style={{flex:1, flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                <View style={{marginLeft:20, justifyContent:'center',}}>
                    {/* <TouchableOpacity activeOpacity={0.8} onPress={()=>setVisibleModalImagen(true)} style={{width:hp('3%'), height:hp('3%'), position:'absolute', top:-5, right:-5, borderWidth:1, zIndex: 100, borderColor:'#D6D6D6', backgroundColor: "#FFF", borderRadius:hp('3%'), justifyContent:'center', alignItems:'center'}}>
                        <Icon2 name="update" size={hp('2%')} style={{opacity:0.5}}/>
                    </TouchableOpacity> */}
                    {user.photo?
                        <Image source={{uri:imagePlayer}} resizeMode="cover" style={{width:wp('25.7%'),height: hp('12.5%'), borderRadius:hp('50%')}} />
                    :
                        <View style={{backgroundColor:'#D6D6D6', width:hp('13%'),height: hp('13%'), borderRadius:hp('50%'), justifyContent:'center', alignItems:'center'}}>
                            <Text style={{fontSize:hp('6%'), color:'#3d3d3d', fontWeight:'bold', }}>{((user.name || "").charAt(0) || "").toUpperCase()}</Text>
                        </View>
                       
                    }
                </View>
                <View style={{flex:1, height:'60%',  justifyContent:'flex-start', marginLeft:20, marginRight:20, }}>
                    {/* <TouchableOpacity activeOpacity={0.8}  onPress={()=>setVisible(true)} style={{width:30, height:30, position:'absolute', top:5, right:5, borderWidth:1, borderColor:'#D6D6D6', backgroundColor: '#FFF', zIndex: 1, borderRadius:hp('3%'), justifyContent:'center', alignItems:'center'}}>
                        <Icon2 name="pencil" size={hp('2%')} style={{opacity:0.5}}/>
                    </TouchableOpacity> */}
                    <Menu style={{width:40, height:40, position:'absolute', top:0, right:5,  zIndex: 1, borderRadius:hp('3%'), justifyContent:'center', alignItems:'center'}}>
                        <MenuTrigger style={{width:wp('10%'),height:hp('8%'), justifyContent: 'center',alignItems: 'center',}}>
                                <Icon2 name="cog" size={hp('3%')} style={{opacity:0.7}}/>
                        </MenuTrigger>
                        <MenuOptions customStyles={optionStyles}>
                        {/* primero  */}
                           <MenuOption onSelect={() => setVisibleModalImagen(true)} style={{marginBottom:0}} >
                            <View style={{marginLeft:wp('2%'), padding:5}}>
                            <Text style={{fontSize:wp('4%'), fontWeight:'bold', color:'#3d3d3d'}}>
                             {I18n.t('profile.menuProvider.option4')}</Text>
                            </View>
                            </MenuOption>
                          {/* segundo */}
                            <MenuOption onSelect={() => setVisible(true)} style={{marginBottom:0}}>
                            <View style={{marginLeft:wp('2%'), padding:5}}>
                            <Text style={{fontSize:wp('4%'), fontWeight:'bold', color:'#3d3d3d'}}>
                              {I18n.t('profile.menuProvider.option1')}</Text>
                            </View>
                            </MenuOption>

                            {/* tercero  */}
                            <MenuOption
                              style={{marginBottom:0}}  onSelect={() => {
                              Clipboard.setString('https://go.vfa.app/'+user.username);
                              Snackbar.show({
                                text: I18n.t('profile.menuProvider.option2'),
                                // text:'Copy profile URL',
                                duration: Snackbar.LENGTH_LONG,
                              });
                            }} >
                            <View style={{marginLeft:wp('2%'), padding:5}}>
                            <Text style={{fontSize:wp('4%'), fontWeight:'bold', color:'#3d3d3d'}}>
                                  {/* {I18n.t('profile.menuProvider.option2')}*/}
                                  {I18n.t('profile.menuProvider.option5')} 
                                  </Text>
                            </View>
                            </MenuOption>
                            <MenuOption 
                            style={{marginBottom:0}}
                            onSelect={() => {
                              Share.open({
                                // title: "Rate my performance on VFA.app using this link!",
                                message: "Rate my performance on VFA.app using this link! "+ 'https://go.vfa.app/'+user.username,
                                // url: "file://" + video.data,
                                // type: 'video/mp4',
                                // subject: "videos " + videos.name,
                              })
                              Snackbar.show({
                                text: I18n.t('profile.menuProvider.option3'),
                                duration: Snackbar.LENGTH_LONG,
                              });
                            }} >
                            <View style={{marginLeft:wp('2%'), padding:5}}>
                                  <Text style={{fontSize:wp('4%'), fontWeight:'bold', color:'#3d3d3d'}}>
                                  {/* {I18n.t('profile.menuProvider.option3')} */}
                                  {I18n.t('profile.menuProvider.option6')} 
                                  </Text>
                            </View>
                            </MenuOption>

                            <MenuOption 
                            style={{marginBottom:5}}
                            onSelect={() => {
                              navigation.navigate('Account security',{player:user,  token:token,update:(message)=>{mensaje(message)}})
                             }} >
                            <View style={{marginLeft:wp('2%'), padding:5}}>
                            <Text style={{fontSize:wp('4%'), fontWeight:'bold', color:'#3d3d3d'}}>
                                  {/* {I18n.t('profile.menuProvider.option2')}*/}
                                  {I18n.t('profile.menuProvider.option7')} 
                                  </Text>
                            </View>
                            </MenuOption>


                        </MenuOptions>
                    </Menu>
                    <Text numberOfLines={1} style={{fontSize:hp('2.5%'), color:'#000',textTransform:'capitalize', marginTop:10, fontWeight:'bold'}}>
                      {user.name}
                      </Text>
                    <Text numberOfLines={1} style={{fontSize:hp('1.6%'), opacity:0.8, color:'#000', }}>{user.username}</Text>
                <View style={{flexDirection:'row', alignItems:'center', marginTop:5}}>
                    {svg?
                      <SvgUri
                        width="25"
                        height="25"
                       
                        viewBox="0 0 1200 1200"
                        uri={svg}
                      />
                    :null}
                  <Text style={{paddingLeft:10}}>
                    {user.rango}
                    </Text>
                </View>
                <View style={{position:'absolute', bottom:0, right:10, }}>
                    {user.pais?
                      <SvgUri
                        width="40"
                        height="40"
                        fill="#000"
                        style={{borderRadius:5}}
                        viewBox={user.pais == 've'? "0 0 130 100" : "0 0 640 480"}
                        uri={"https://cdn.ipwhois.io/flags/"+user.pais+".svg"}
                      />
                    :null}
                </View>
              </View>
          </View>
          <View style={{height:hp('3%')}}>
                <TouchableOpacity activeOpacity={1} style={{justifyContent:'center', alignItems:'center'}}>
                      <Text style={{position:'absolute', zIndex: 10, fontSize:hp('1.5'), fontWeight:'bold'}}>{porcentajeHomework}%</Text>
                      <Progress.Bar progress={Number(porcentaje)} width={wp('90%')} height={hp('1.6%')} color="green" borderColor="white" backgroundColor="white" borderRadius={5}/>
                </TouchableOpacity>
                {/* <View style={{flexDirection:'row', width:wp('100%'), justifyContent:'space-around', marginTop:5}}>
                    <Text style={{fontSize:hp('1.5%')}}>1</Text>
                    <Text style={{fontSize:hp('1.5%')}}>PROGRESS</Text>
                    <Text style={{fontSize:hp('1.5%')}}>100%</Text>
                </View> */}
          </View>
          <View style={{flex:3,}}>

        
              <View style={{flexDirection:'row', justifyContent:'space-between', width:wp('100%'), paddingRight:20, marginTop:10, marginBottom:10, paddingLeft:20, height:hp('4%'), alignItems:'center'}}>
                    {/* {team?
                        <Text style={{fontSize:hp('1.7%')}}>My Videos</Text>
                    :
                        <Text style={{fontSize:hp('1.5%'), opacity:0.8}}>You do not have any assigned work method</Text>
                    } */}
                    {/* <TouchableOpacity style={{width:wp('10%'), height:'100%', justifyContent:'center', alignItems:'center'}}>
                        <Icon name="ellipsis-h" size={hp('3%')}/>
                    </TouchableOpacity> */}
                    <View style={{flex:1, flexDirection:'row',  alignItems:'center'}}>
                      <View style={{width:hp('4%'), height:hp('4%'), marginRight:5, borderRadius:hp('4%'), justifyContent:'center', alignItems:'center', backgroundColor: "#D8D8D8",}}> 
                        <Icon name="star" color="#535353" size={hp('2.3%')}/>
                      </View>
                      <Text style={{color:'#535353', fontSize:hp('2%'), fontWeight:'bold'}}>
                        {setNumber(parseInt(user.starts))}
                        </Text>
                    </View>
                    <View style={{flex:1, flexDirection:'row',  alignItems:'center'}}>
                      <View style={{width:hp('4%'), height:hp('4%'), marginRight:5, borderRadius:hp('4%'), justifyContent:'center', alignItems:'center', backgroundColor: "#D8D8D8",}}> 
                        <Icon2 name="video" color="#535353" size={hp('2.3%')}/>
                      </View>
                      <Text style={{color:'#535353', fontSize:hp('2%'), fontWeight:'bold'}}>{setNumber(parseInt(data.length))} {I18n.t('tex14')}</Text>
                    </View>
                    <View style={{flex:1, flexDirection:'row',  alignItems:'center'}}>
                      <View style={{width:hp('4%'), height:hp('4%'), marginRight:5, borderRadius:hp('4%'), justifyContent:'center', alignItems:'center', backgroundColor: "#D8D8D8",}}> 
                        <Icon2 name="flash" color="#535353" size={hp('2.3%')}/>
                      </View>
                      <Text style={{color:'#535353', fontSize:hp('2%'), fontWeight:'bold'}}>{setNumber(parseInt(user.points))} {I18n.t('tex15')} </Text>
                    </View>
              </View>

            <TabView
                navigationState={{ index, routes }}
                onIndexChange={(i) => console.log(i)} 
                renderScene={SceneMap({
                    videos: Videos,
                    homework: VideosHomework,
                })}
                renderTabBar={(props) => <TabBar {...props}
                    pressColor={'transparent'}
                    indicatorStyle={{ backgroundColor: '#868686' }}
                    style={{ backgroundColor: '#f3f3f3' }}
                    renderLabel={({ route, focused, color }) => (
                        <Text 
                        style={{fontWeight:'bold', color: focused ? '#3d3d3d' : '#646469', opacity: focused ? 1 : 0.5, }}>
                          {route.title}
                        </Text>
                    )}
                />}
                // onIndexChange={(index) => console.log(index)}
                initialLayout={{ width: Dimensions.get('screen').width }}
                
               />





                {/* RENDER DE LOS VIDEOS  */}
          </View>

   {/* modal para seleccionar la imagen de perfil  */}
            <Dialog.Container visible={visibleModalImagen} onRequestClose={()=>setVisibleModalImagen(false)} onPress={()=>setVisibleModalImagen(false)}>
                <Dialog.Title>  {I18n.t('profile.messagePhoto')}</Dialog.Title>
                    <Dialog.Description>
                      {I18n.t('profile.messajeModal')}
                    </Dialog.Description>
                    <Dialog.Button label="Cancel" style={{color:'red'}} onPress={()=>{setVisibleModalImagen(false)}} />
                <Dialog.Button label="Camera" onPress={()=>{selectCamera()}} />
                <Dialog.Button label="Gallery" onPress={()=>{selectGaleria()}} />
            </Dialog.Container>
   {/* fin modal para seleccionar la imagen de perfil  */}


            {/* <Modal 
            animationType="slide"
            transparent={true}
            visible={false}
            // visible={visible}
            onRequestClose={()=>setVisible(false)}>
                 <KeyboardAvoidingView  behavior={Platform.OS === "ios"? "padding":"null"} enabled style={{ flex: 1 ,flexDirection: 'column', justifyContent: 'flex-end'}}>
                    <View style={{ height: hp('80%') ,width: '100%', backgroundColor:"#FFF", justifyContent:"center", borderTopRightRadius:10, borderTopLeftRadius:10}}>
                     <TouchableOpacity onPress={()=>setVisible(false)} style={{flex:0.1 ,width:'100%', height:'6%', justifyContent:'center', alignItems:'center'}}>
                        <Icon name="chevron-down" size={hp('2.3%')} color="#000"/>
                     </TouchableOpacity>
                        <View style={{flex:1, borderTopRightRadius:10, borderTopLeftRadius:10,}}>
                            <View style={{flex:1, paddingHorizontal:20, marginBottom:20}}>
                                    <View style={{width:wp('90%'), paddingVertical: 10,}}>
                                        <Input
                                            left
                                            icon="user"
                                            family="font-awesome"
                                            style={styles.inputs}
                                            iconSize={hp('1.4%')}
                                            placeholder="Enter your name"
                                            value={name}
                                            bottomHelp
                                            onChangeText={(e)=>setName(e)}
                                            placeholderTextColor="#818181"
                                            
                                        />
                                    </View>
                                    <View style={{width:wp('90%'), paddingVertical: 10,}}>
                                        <Input
                                            left
                                            icon="id-card"
                                            family="font-awesome"
                                            style={styles.inputs}
                                            iconSize={hp('1.4%')}
                                            placeholder="Enter your username"
                                            value={username}
                                            bottomHelp
                                            onChangeText={(e)=>setUsername(e)}
                                            placeholderTextColor="#818181"
                                            disabled
                                            
                                        />
                                    </View>
                                    <View style={{width:wp('90%'), paddingVertical: 10,}}>
                                        <Input
                                            left
                                            icon="envelope"
                                            family="font-awesome"
                                            style={styles.inputs}
                                            iconSize={hp('1.4%')}
                                            placeholder="Enter your email"
                                            value={email}
                                            bottomHelp
                                            onChangeText={(e)=>setEmail(e)}
                                            placeholderTextColor="#818181"
                                            
                                        />
                                    </View>
                                    <View style={{width:wp('90%'), paddingVertical: 10,}}>
                                        <Input
                                            left
                                            icon="key"
                                            family="font-awesome"
                                            style={styles.inputs}
                                            iconSize={hp('1.4%')}
                                            placeholder="Enter your password"
                                            value={pass}
                                            bottomHelp
                                            password 
                                            viewPass
                                            onChangeText={(e)=>setPass(e)}
                                            placeholderTextColor="#818181"
                                            
                                        />
                                    </View>
                                    <View style={{width:wp('90%'), marginTop:10}}>
                                        <Icon name="calendar" size={hp('1.4%')} color="#818181" style={{position:'absolute', zIndex: 1, top:20, left:17}} />
                                        <DatePicker
                                            style={{width:'100%', height:45, borderRadius:10, borderWidth:0, backgroundColor: '#FFF', color:'#818181'}}
                                            date={age}
                                            mode="date"
                                            placeholder="Date of birth"
                                            format="YYYY-MM-DD"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            showIcon={false}
                                            onDateChange={(v) => setAge(v)}
                                            customStyles={{
                                                dateInput: {
                                                    borderRadius:8,
                                                    alignItems:'flex-start',
                                                    paddingLeft:38,
                                                    height:45,
                                                    marginTop:15,
                                                    borderColor:'#9E9E9E',
                                                    color:'#818181',
                                                },
                                                placeholderText: {
                                                    color:'#818181'
                                                }
                                                }}
                                        />
                                    </View>
                            </View>
                            <View style={{flex:1, justifyContent:'center', alignItems:'center',}}>
                                    <TouchableOpacity activeOpacity={0.9} onPress={() => {
                                            UpdateInfo()
                                        }} style={styles.Button}>
                                                <Text style={{fontWeight:'bold', color:'#FFF', fontSize:hp('1.7%')}}>
                                                    SAVE CHANGE
                                                </Text>
                                    </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </Modal> */}
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
    input: {
        height: hp('4.5%'),
        borderWidth:1,
        borderColor:"#BFBEBE",
        borderRadius:5,
        paddingLeft:20,
        color:"#FFF",
        backgroundColor: "#FFF",
        
    },
    Button: {
        width: width * 0.8,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#11A10F',
        backgroundColor: '#11A10F',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderRadius:10,
        elevation: 5,
        justifyContent:'center',
        alignItems:'center',
        height:hp('6%'),
    },
});

const optionStyles = {
  optionTouchable: {
    underlayColor: 'red',
    activeOpacity: 40,
  },
  optionWrapper: {
    margin: 5,
  },
  optionText: {
    color: 'black',
  },
};

//make this component available to the app
export default Profile;
