//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Video2 from 'react-native-video';
import Loader from '../../coach/global/loader';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

// create a component
const Video = ({navigation, route}) => {
    const { player, video } = route.params;
    const [loader, setLoader] = useState(false);
    var urlVideo = '';
    if (video) {
        urlVideo = video.file_video;
    } else {
        urlVideo = player.file_video;
    }

    useEffect(() => {
        setLoader(true);
    }, []);

    useLayoutEffect(() => {
        navigation.setOptions({
            title: player.name,
        })
    }, [])

    return (
        <View style={styles.container}>

           
            {loader ? <Loader title={"loading video..."}/> : null}
            <Video2 
                source={{ uri: urlVideo }}
                controls={true}
                resizeMode="cover"
                onLoad={() => setLoader(false)}
                style={styles.backgroundVideo} 
            />
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
});

//make this component available to the app
export default Video;
