//import liraries
import React, { useState, useEffect, useLayoutEffect, useRef } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, FlatList, Modal,Platform, Animated, NativeModules } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Snackbar from 'react-native-snackbar';
import Loader from '../../coach/global/loader';
import { LogoutUser, setTeam } from '../../../storage/user/dataUser';
import { url } from '../../../storage/config';
// import { canal, pusher } from '../../../routes/bottomTabNavigatorPlayer';
import {signal, canal } from '../../../storage/pusher';
// import moment from 'moment';
import moment from "moment/min/moment-with-locales";

import Dialog from "react-native-dialog";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'i18n-js';
// create a component
const NotificationPlayer = ({ navigation, route }) => {
    const {id} = route.params?route.params:0;
    const [list, setList] = useState([]);
    const [user, setUser] = useState({});
    const [token, setToken] = useState(null);
    const [loader, setLoader] = useState(false);
    const [refresh, setRefresh] = useState(false);
    const [itemId, setItemId] = useState(null);
    const [visible, setVisible] = useState(false);
    const [multiSelected, setMultiSelected] = useState(false);
    const menu = useRef(new Animated.Value(0)).current;
    const opacity = useRef(new Animated.Value(0)).current;
    const [idioma, setIdioma] = useState('en');


    useEffect(async() => {
        const token = await AsyncStorage.getItem('@token');
        const user = await AsyncStorage.getItem('@user');
        const userJson = JSON.parse(user);
        const tokenJson = JSON.parse(token);
        setToken(tokenJson);
        setUser(userJson);
        getList(tokenJson);




        // Função responsável por adquirir o idioma utilizado no device
        const getLanguageByDevice = () => {
        return Platform.OS === 'ios'
        ? NativeModules.SettingsManager.settings.AppleLocale // Adquire o idioma no device iOS
        : NativeModules.I18nManager.localeIdentifier // Adquire o idioma no device Android
        }
   
        
        const language = getLanguageByDevice()
        let idiomas = language.substr(0,2);
   
         if(idiomas == 'es'){
            setIdioma('es')
         }
         else if(idiomas == 'cn'){
           setIdioma('cn')
         }
         else{
            setIdioma('en')
         }



         console.log("este es el idioma", idioma)

         





        const canal = signal.subscribe("VFA");
        console.log("view notifications ------")

        canal.bind("Notification-" + userJson.id, (data) => {
            if (data.tag === 'NewMessage' || data.tag === 'ReplyPlayerYes'){
                return;
            }else{
                if(data.tag === 'ReplyPlayerUpdate') {
                    setTeam(data.noti)
                    console.warn('ReplyPlayerUpdate')
                }else{
                    getList(tokenJson);
                    console.warn('NewMessage')
                }
            }
          });
        return ()=>{
            signal.disconnect();
        }
    }, [])

    useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Notifications',
        });
    }, [])

    function cerrarAnimacion() {
        Animated.timing(menu, {
            toValue: 0,
            duration: 400,
            useNativeDriver: true,
        }).start();
    }

    const selectItem = (index) => {
        //invierto la seleccion del item (selecciono o desselecciono)
        //actualizo el state para que se renderice
        const array = [...list]
        array[index].isSelected = !array[index].isSelected;
        setList(array)
        setMultiSelected(true)

        animacion(1)
        abrirAnimacion()

        let very = list.filter(item => item.isSelected).length;
        if (very < 1) {
            setMultiSelected(false)
            animacion(0)
            cerrarAnimacion()
        }
    }

    const cancelSelect =()=>{
        const array = [...list]
        array.forEach((item,index)=>{
            array[index].isSelected=false;
        })
        setList(array)
        cerrarAnimacion()
    }
    function abrirAnimacion() {
        Animated.timing(menu, {
            toValue: -56,
            duration: 400,
            useNativeDriver: true,
        }).start();
    }

    function animacion(type) {
        Animated.timing(opacity, {
            toValue: type,
            duration: 200,
            useNativeDriver: true,
        }).start();
    }

  
    function Tiempo(fecha){
            var formato = new Date(fecha)
            var fechaCreacion = formato.getTime();
            // console.warn(i)
            moment.locale(idioma);
            return moment(fechaCreacion).startOf('segund').fromNow();
    }

    async function getList(t) {
        // console.warn(t)
        setLoader(true)
        setRefresh(true)
        try {
            await fetch(url + 'api/notification/list', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + t
                },
            }).then(res => res.json())
                .then((dat) => {
                    console.warn(dat, '----- qqui')
                    try {
                        if (dat.ok) {
                            console.log("listando aqui")
                            const solicitud = dat.solicitudes;
                            const noti = dat.data;
                            var list = solicitud.concat(noti);

                            
                            setList(list);
                            setLoader(false)
                            setRefresh(false)
                        }
                        // if (dat.message == 'Unauthenticated.') {
                        //     LogoutUser()
                        //     setTimeout(() => {
                        //         navigation.replace('Login')
                        //     }, 1000)
                        // }
                    } catch (error) {
                        setLoader(false)
                        setRefresh(false)
                        console.warn('error:' + error)
                    }
                })

        } catch (error) {
            console.warn(error)
            setLoader(false)
            setRefresh(false)
        }
    }

    async function delet() {
        setLoader(true)
        try {
            await fetch(url + 'api/notification/delete/'+itemId, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
            }).then(res => res.json())
                .then((dat) => {
                    console.warn(dat)
                    try {
                        if (dat.ok) {
                            setLoader(false)
                            getList(token) 
                            setVisible(false)
                            setItemId(null)
                            setRefresh(false)
                        }
                        // if (dat.message == 'Unauthenticated.') {
                        //     LogoutUser()
                        //     setTimeout(() => {
                        //         navigation.replace('Login')
                        //     }, 1000)
                        // }
                    } catch (error) {
                        setLoader(false)
                        setRefresh(false)
                        console.warn('error:' + error)
                    }
                })
        } catch (error) {
            
        }
    }

    async function reply(id, type, index) {
        setLoader(true)
        try {
            await fetch(url + 'api/team/reply', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({
                    noti_id: id,
                    reply: type,
                    type_user:'player'
                })
            }).then(res => res.json())
                .then((dat) => {
                    try {
                        if (dat.ok) {
                            console.log("listando aqui 2")
                            setLoader(false)
                            setList(list => list.filter((item, i) => i !== index));
                            Snackbar.show({
                                text: 'Reply sent',
                                duration: Snackbar.LENGTH_LONG,
                            });
                        }
                        // if (dat.message == 'Unauthenticated.') {
                        //     LogoutUser()
                        //     setTimeout(() => {
                        //         navigation.replace('Login')
                        //     }, 1000)
                        // }
                    } catch (error) {
                        setLoader(false)
                        console.warn('error:' + error)
                    }
                })

        } catch (error) {
            console.warn(error)
            setLoader(false)
        }
    }

    const renderItem = ({ item, index, separators }) => {


        var urlProfile = user.coach_id? 'https://go.vfa.app/'+item.user_photo : 'https://go.vfa.app/preview/' + item.skill_photo;
        var color = '#FFF';
        if (id) {
            if (item.id == id) {
                color = '#88C4A0';
            }
        }else{
            if (!item.status) {
                color= '#D7E8D3';
            }
        }
        
        console.log(item.skill_photo);

        return (
            item.status !== 'pending' ?
                <TouchableOpacity activeOpacity={0.8} onPress={() => {
                    if (item.skill_user_id) {
                        console.log(item, 'item');
                        navigation.navigate('previaNotification',{skill:item, token})
                    }
                 }}
                 onLongPress={()=>
                    {
                        setItemId(item.id);
                        setVisible(true);
                    }
                 } style={{ width: wp('100%'), height: hp('13%'), flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: color, borderBottomWidth: 1, borderColor: '#D5D5D5', paddingHorizontal: 15, }}>
                    <View style={{width:70, height:'100%', justifyContent:'center', alignItems:'center',}}>
                              
                               
                            {
                             item.title == 'Congratulations!'? 
                             <Image source={require('../../../assets/home/exito.png')} resizeMode="cover" style={{width:50, height:50, borderRadius:50, marginRight:10, justifyContent:'center', alignItems:'center'}} />
                             :item.title == 'Level approved'? 
                             <Image source={require('../../../assets/home/welcome.png')} resizeMode="cover" style={{width:50, height:50, borderRadius:50, marginRight:10, justifyContent:'center', alignItems:'center'}} />
                             :item.title == 'Skill approved'?
                             <Image source={require('../../../assets/home/aprobado.png')} resizeMode="cover" style={{width:50, height:50, borderRadius:50, marginRight:10, justifyContent:'center', alignItems:'center'}} />
                             :<Image source={{uri:'https://go.vfa.app/preview/' + item.skill_photo}} esizeMode="cover" style={{width:60, height:60, borderRadius:50, marginRight:10}} />
                             }


                        
                        {item.isSelected ?
                            <Animated.View key={index} style={{ opacity, position: 'absolute', right: 8, bottom: 8, backgroundColor: 'green', borderRadius: 50, padding: 3, elevation: 3 }}>
                                <Icon name="check" size={hp('2%')} color="#000" />
                            </Animated.View>
                        : null}
                    </View>
                    <View style={{ flex: 1, }}>
                        <Text numberOfLines={1} style={{ fontSize: hp('2%'), fontWeight: 'bold' }}>
                            {item.title} 
                        </Text>
                        <Text numberOfLines={2} style={{ fontSize: hp('1.7%'), opacity: 0.8, width:'90%'}}>
                            {item.body}
                        </Text>
                        <Text style={{fontSize:hp('1.3%'), position:'absolute', bottom:-20, opacity:0.5}}>
                            {Tiempo(item.created_at)}
                        </Text>
                    </View>
                    <View>
                        <Icon name="chevron-right" size={hp('1.5%')} color="#000" />
                    </View>
                </TouchableOpacity>
                :
                <TouchableOpacity activeOpacity={0.8} onLogPress={()=>{
                    setItemId(item.id);
                    setVisible(true);
                }} style={{ width: wp('100%'), height: hp('15%'), justifyContent: 'center', alignItems: 'center', backgroundColor: color, borderBottomWidth: 1, borderColor: '#D5D5D5', paddingHorizontal: 15, paddingTop: 10, paddingBottom: 10, }}>
                    <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'center', alignItems: 'center', paddingLeft: 10 }}>
                        <View style={{ flex: 1, }}>
                            <Text numberOfLines={2} style={{ fontSize: hp('1.9%'), fontWeight: 'bold' }}>
                                {item.title}
                            </Text>
                            <Text numberOfLines={2} style={{ fontSize: hp('1.7%'), opacity: 0.8 }}>
                                {item.body}
                            </Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', flex: 1, width: '100%',}}>

                    <View style={{ flex: 1, paddingLeft:10}}>
                            <TouchableOpacity activeOpacity={0.8} disabled={loader} onPress={() => reply(item.id, 'no', index)} activeOpacity={0.8} style={{ width: '70%', borderRadius: 5, backgroundColor: '#3d3d3d', padding: 5, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{textTransform:'uppercase', fontSize: hp('1.8%'), color: '#FFF', fontWeight: 'bold' }}>Decline</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ flex: 1,}}>
                            <TouchableOpacity activeOpacity={0.8} disabled={loader} onPress={() => reply(item.id, 'yes', index)} activeOpacity={0.8} style={{ width: '70%', borderRadius: 5, backgroundColor: 'green', padding: 5, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{textTransform:'uppercase', fontSize: hp('1.8%'), color: '#FFF', fontWeight: 'bold' }}>Accept</Text>
                            </TouchableOpacity>
                        </View>
                        
                    </View>
                </TouchableOpacity>

        )
    }

    const Empty = ()=>{
        return (
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop:hp('40%')}}>
                    <Icon name="bell-slash" size={hp('15%')} style={{ opacity: 0.3 }} />
                    <Text style={{ fontSize: hp('2%'), paddingTop: 20 }}>{I18n.t('notifications.messageInfo')}</Text>
                </View>
        )
    }

    return (
        <View style={styles.container}>
            {loader ? <Loader /> : null}
            <Image source={require('../../../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'),
                position: "absolute",
            }} />
                <FlatList
                    data={list}
                    renderItem={renderItem}
                    keyExtractor={(item, i) => i}
                    style={{ width: '100%', height: '100%', }}
                    ListEmptyComponent={Empty}
                    onRefresh={() => getList(token)}
                    refreshing={refresh}
                />
            {/* <Animated.View style={{height: 56, position:'absolute', width: wp('100%'), bottom:-56, transform: [{ translateY: menu }]}}>
                <View style={{ backgroundColor: '#FFF', height: 56, width: wp('100%'), justifyContent: 'center', alignItems: 'center', }}>
                    <TouchableOpacity activeOpacity={0.8} onPress={() => console.warn('no')} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name="trash" size={hp('2%')} />
                        <Text>Delete</Text>
                    </TouchableOpacity>
                </View>
            </Animated.View> */}
            <Dialog.Container visible={visible} onRequestClose={()=>setVisible(false)} onPress={()=>setVisible(false)}>
                <Dialog.Title>Delete notification</Dialog.Title>
                    <Dialog.Description>
                        {I18n.t('notifications.alerMessage')}
                    </Dialog.Description>
                    <Dialog.Button label="Cancel" style={{color:'red'}} onPress={()=>{setVisible(false)}} />
                <Dialog.Button label="Delete" onPress={()=>{delet()}} />
            </Dialog.Container>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F3F3F3',
    },
});

//make this component available to the app
export default NotificationPlayer;
