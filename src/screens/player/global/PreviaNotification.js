//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Platform, I18nManager } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Snackbar from 'react-native-snackbar';
import Video2 from 'react-native-video';
import Loader from '../../coach/global/loader';
import { url } from '../../../storage/config';
import { LogoutUser } from '../../../storage/user/dataUser';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as RNFS from 'react-native-fs';
import Share from 'react-native-share';
import I18n from 'i18n-js';
// create a component
const PreviaNotification = ({ navigation, route }) => {
    const { skill, token, } = route.params;
    const [loader, setLoader] = useState(false);
    const [loaderShare, setLoaderShare] = useState(false);
    const [coach, setCoach] = useState({});
    const [user, setUser] = useState({});
    const [video, setVideo] = useState(false);
    const [SkillPlayer, setSkillPlayer] = useState({})
    const [urlVideo, setUrlVideo] = useState(null);
    const [urlImageVideo, setUrlImageVideo] = useState(null)
    const [textLoader, setTextLoader] = useState('Loading...');
    var urlImage = 'https://go.vfa.app/preview/' + SkillPlayer.skill_photo;

    let miskill = {...skill, CanRecord:true}
   

    useEffect(async() => {
        const coach = await AsyncStorage.getItem('@coach');
        const user = await AsyncStorage.getItem('@user');
        const userJson = JSON.parse(user);
        const coachJson = JSON.parse(coach);
        setCoach(coachJson);
        setUser(userJson);
        getSkill(token)
        console.warn(skill)

    }, [])

    useLayoutEffect(() => {
        navigation.setOptions({
            title: SkillPlayer.skill_name,
        });
    }, [SkillPlayer])

    async function getSkill(t) {

        console.log(skill.skill_user_id, '---');

        setLoader(true)
        try {
            await fetch(url + 'api/skill/view/user/'+skill.skill_user_id, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + t
                },
            }).then(res => res.json())
                .then((dat) => {
                    console.warn(dat)
                    try {
                        if (dat.ok) {
                            setLoader(false)
                            // console.warn(dat)

                            let datos = {...dat.data, record:true};
                            setSkillPlayer(datos)

                            console.log(datos, 'los datos recibidos');


                            setUrlVideo(dat.data.file_video)
                            setUrlImageVideo(dat.data.file_captura)
                            if (dat.data.file_video) {
                                setVideo(true)
                            }
                        }
                        // if (dat.message == 'Unauthenticated.') {
                        //     LogoutUser()
                        //     setTimeout(() => {
                        //         navigation.replace('Login')
                        //     }, 1000)
                        // }
                    } catch (error) {
                        setLoader(false)
                        console.warn('error:' + error)
                    }
                })

        } catch (error) {
            console.warn(error)
            setLoader(false)
        }
    }




   async function CompartirIos(){

    console.log(user);

    // console.log(`${RNFS.DocumentDirectoryPath}/video.mp4`);
    var ubicacion =  `${RNFS.DocumentDirectoryPath}/video.mp4`;

    RNFS.downloadFile({
        fromUrl:skill.file_video,
        toFile: ubicacion,
    }).promise.then(res =>{
        console.log(res);
        console.log('file://' + ubicacion);


const url = "file://" + ubicacion;

if(SkillPlayer.video_status == 0){
    var title = I18n.t('ar.shareSocial.text1') +' '+ user.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+user.username+'/'+SkillPlayer.url_public;
    var message = I18n.t('ar.shareSocial.text1') +' '+ user.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+user.username+'/'+SkillPlayer.url_public;
}
else {
    var title = '';
    var message = '';
}

const options = Platform.select({
  ios: { 
    activityItemSources: [
      {
        // For sharing url with custom title.
        placeholderItem: { type: 'url', content: url },
        item: {
          default: { type: 'url', content: url },
        },
        subject: {
          default: title,
        },
        linkMetadata: { originalUrl: url, url, title },
      },
      {
        // For sharing text.
        placeholderItem: { type: 'text', content: message },
        item: {
          default: { type: 'text', content: message },
          message: null, // Specify no text to share via Messages app.
        },
        linkMetadata: {
          // For showing app icon on share preview.
          title: message,
        },
      },
    ],
  },
  default: {
    title,
    subject: title,
    message: `${message} ${url}`,
  },
});

Share.open(options);



        // Share.open({
        //     title: "Rate my performance on VFA.app using this link!",
        //     message: "Rate my performance on VFA.app using this link! "+ 'https://go.vfa.app/'+userJson.username,
        //     url: "file://" + ubicacion,
       
        //   });
      })
    }



    async function Compartir(){

        console.log("compartiendo en android");
        setLoaderShare(true)

      setTextLoader('Download video...');
      setTimeout(()=>{
        setTextLoader('Wait please...');
      }, 5000);

      
          RNFS.downloadFile({
            fromUrl:SkillPlayer.file_video,          // URL to download file from
            toFile: `${RNFS.DocumentDirectoryPath}/video.mp4`,           // Local filesystem path to save the file to
            cacheable: true,      // Whether the download can be stored in the shared NSURLCache (iOS only, defaults to true)
            
        }).promise.then((r) => {
            console.log(r);
            setLoaderShare(false)
            setTextLoader('Loading...');
            
            if(SkillPlayer.video_status == 0){
                var datos = {
                    title: I18n.t('ar.shareSocial.text1') +' '+ user.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+user.username+'/'+SkillPlayer.url_public,
                    message: I18n.t('ar.shareSocial.text1') +' '+ user.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+user.username+'/'+SkillPlayer.url_public,
                }
            }
            else {
                 var datos = {
                title: '',
                message: ''
            }
            }
           
            
            Share.open({
              title: datos.title,
              message: datos.message,
              url: "file://" + `${RNFS.DocumentDirectoryPath}/video.mp4`,
         
            });
          
        });
      

    }



    // async function Compartir(){
    //     setLoaderShare(true)

    //   setTextLoader('Download video...');
    //   setTimeout(()=>{
    //     setTextLoader('Wait please...');
    //   }, 5000);
    //       RNFS.downloadFile({
    //         fromUrl:skill.file_video,
    //         toFile: `${RNFS.DocumentDirectoryPath}/video.mp4`
    //     }).promise.then((r) => {
    //         setLoaderShare(false)
    //         setTextLoader('Loading...');

            


    //         Share.open({
    //           title:I18n.t('ar.shareSocial.text1') +' '+ user.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+user.username+'/'+skill.url_public,
    //           message: I18n.t('ar.shareSocial.text1') +' '+ user.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+user.username+'/'+skill.url_public,
    //           url: "file://" + `${RNFS.DocumentDirectoryPath}/video.mp4`,
         
    //         });
          
    //     });
      

    // }


    console.log(SkillPlayer, 'lista -------');
    console.log('urldel video', urlVideo)

    return (
        <View style={styles.container}>

            {loaderShare?<Loader title={textLoader}/>:null} 
            {loader ? <Loader /> : null}
            <Image source={require('../../../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'),
                position: "absolute",
            }} />
            <View style={{ flex: 1,}}>

{/*                

                <TouchableOpacity activeOpacity={0.9} onPress={()=> navigation.navigate('3D', { SkillPlayer, token, user })} style={{ height: hp('10%'), width:wp('100%'), position:'absolute', elevation:2, marginTop:20, justifyContent:'center', alignItems:'center', zIndex: 1000, flexDirection: 'row', paddingHorizontal:20 }}>
                    <View style={{backgroundColor: 'white', height:'100%', width:'90%', borderRadius:10}}>
                        <View style={{flexDirection: 'row',}}>
                            <View style={{ width: wp('20%'), height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('3D', { SkillPlayer, token, user })} style={{ width: wp('20%'), height: '90%', justifyContent:'center', alignItems:'center', elevation:3 }}>
                                    <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: hp('7%'), height: hp('7%'), borderRadius: hp('7%') }} />
                                </TouchableOpacity>
                            </View>
                            {user.coach_id?
                                <View style={{position:'absolute', right:5, top:5, backgroundColor: '#11A10F', padding:5, borderRadius:10}}>
                                    <Text numberOfLines={1} style={{color:'#FFF', fontSize:hp('1.5%')}}>{SkillPlayer.name_method == 'level'? "Level "+SkillPlayer.value_method:SkillPlayer.name_method }</Text>
                                </View>
                            :
                                <View style={{position:'absolute', right:5, top:5, backgroundColor: '#11A10F', padding:5, borderRadius:10}}>
                                    <Text numberOfLines={1} style={{color:'#FFF', fontSize:hp('1.5%')}}>Level {SkillPlayer.value_method}</Text>
                                </View>
                            }
                            <View style={{ padding: 10, paddingTop:20 }}>
                                <Text style={{ fontSize: hp('1.7%') }}>{SkillPlayer.skill_name}</Text>
                                <Text numberOfLines={1} style={{ fontSize: hp('1.4%') }}>{SkillPlayer.method_name}</Text>
                                {SkillPlayer.puntos ?
                                    <View style={{flexDirection:'row', alignItems:'center'}}>
                                        <View style={{width:hp('2.5%'), height:hp('2.5%'), backgroundColor: '#D8D8D8', borderRadius:hp('2%'), justifyContent:'center', alignItems:'center'}}>
                                            <Icon2 name="flash" size={hp('1.7%')} color="#535353" />
                                        </View>
                                        <Text style={{paddingLeft:5, opacity:0.7}}>+ {SkillPlayer.puntos} pts</Text>
                                    </View>
                                : null}
                            </View>
                        </View>
                    </View>
                </TouchableOpacity> */}



{SkillPlayer.file_video ?
                    <View style={{ width: '100%', height: '90%'}}>
                        <View style={{flex:1, alignItems:'center', backgroundColor:'#000'}}>
                            <Image source={{ uri: urlImageVideo }} style={{ 
                                height: '100%',
                                width: wp('70%'),
                                position: "absolute",
                             }} />
                            <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate('PreviaVideo', { skill:SkillPlayer, urlVideo })} style={{ position: 'absolute', width: hp('20%'), height: hp('20%'), justifyContent: 'center', alignItems: 'center',top:hp('10%') }}>
                                <Icon name="play" size={hp('6')} color="#FFF" style={{ marginLeft: 10 }} />
                            </TouchableOpacity>
{/* 
                            <TouchableOpacity
                                style={{position:'absolute', bottom:hp('6%'), borderRadius:5, backgroundColor:'#11A10F', padding:7, width:wp('70%'), textAlign:'center', alignItems:'center', height:hp('5%'),  shadowColor: "black", elevation:3}}
                                onPress={() => Compartir()}
                            >
                                <Text style={{fontWeight:'bold', fontSize:wp('5%'),  textTransform:'uppercase', color:'white'}}>Share video</Text>
                            </TouchableOpacity> */}
                        </View>

                        


                <View style={{flexDirection:'row', width:wp('100%'), paddingRight:20, marginTop:10, marginBottom:10, paddingLeft:15, height:hp('4%'), alignItems:'center'}}>
                    <View style={{flexDirection:'row',  alignItems:'center'}}>
                      <View style={{width:hp('4%'), height:hp('4%'), marginRight:5, borderRadius:hp('4%'), justifyContent:'center', alignItems:'center', backgroundColor: "#D8D8D8",}}> 
                        <Icon2 name="thumb-up" color="#3d3d3d" size={hp('2.3%')}/>
                      </View>
                      <Text style={{color:'#3d3d3d', fontSize:hp('2%'), fontWeight:'bold'}}>{SkillPlayer.votos}</Text>
                    </View>
                </View>




                        <View style={{flexDirection:'row', paddingTop:10, paddingHorizontal:15}}>

                            <View style={{ padding:4,  borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'gray',}}>
                                <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Level {SkillPlayer.level_id}</Text>
                            </View>


                            {/* VALIDAMOS EL ESTADO DEL VIDEO  */}
                            {/* {SkillPlayer.public_status == 1?
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'green',}}>
                                <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>
                                Approved
                                </Text>
                            </View>
                            :SkillPlayer.public_status == 2?
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'red',}}>
                            <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>
                            Reproved
                            </Text>
                            </View>
                            :
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'orange',}}>
                            <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>
                            Pending
                            </Text>
                            </View>
                            }  */}

                            {/* FIN DE LA VALIDACION DEL ESTADO DEL VIDEO  */}



                            {SkillPlayer.video_status == 0?
                            SkillPlayer.public_status == 1?
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'green',}}>
                                <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Approved</Text>
                            </View>
                            :SkillPlayer.public_status == 2?
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'red',}}>
                            <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Reproved</Text>
                            </View>
                            :
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'orange',}}>
                            <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Pending</Text>
                            </View>
                            :
                            SkillPlayer.qualification >= 7?
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'green',}}>
                                <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Approved</Text>
                            </View>
                            :SkillPlayer.qualification > 0 && SkillPlayer.qualification <= 6?
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'red',}}>
                            <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Reproved {SkillPlayer.public_status}</Text>
                            </View>
                            :
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'orange',}}>
                            <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Pending</Text>
                            </View>

                            } 



                            </View>

                         



                            <TouchableOpacity onPress={()=> navigation.navigate('3D', { skill:SkillPlayer, token, user })} style={{flexDirection:'row', paddingTop:10, paddingHorizontal:15, marginBottom:20, width:wp('80%')}}>

                            <View >
                            {/* <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('3D', { SkillPlayer, token, user })} style={{ width: wp('20%'),  justifyContent:'center', alignItems:'center', paddingTop:10 }}> */}
                                    <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: hp('6%'), height: hp('6%'), borderRadius: hp('7%') }} />
                            {/* </TouchableOpacity> */}
                            </View>  

                            <View style={{marginLeft:10, justifyContent:'center'}}>
                                <Text style={{fontSize:hp('2.5%'), paddingTop:5, opacity:0.8, fontWeight:'bold', color:'#333'}}>{SkillPlayer.name}</Text>
                                <Text style={{fontSize:hp('1.7%'), paddingTop:5, opacity:0.6}}>{SkillPlayer.method_name}</Text>
                            </View>
                            </TouchableOpacity>

                            <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center', marginTop:30, marginBottom:30}}>
                           
                           {SkillPlayer.video_status == 0?
                            <TouchableOpacity  activeOpacity={0.9} onPress={() => {
                                 Platform.OS == 'android'?
                                  Compartir()
                                  : 
                                  CompartirIos()
                                }}
                                 style={styles.buttonShare}>
                                        <Text style={{fontWeight:'bold', fontSize:wp('4%'),  textTransform:'uppercase', color:'white'}}>Share video 
                                        </Text>
                                        <Icon2 name="send" style={{marginLeft:10, fontSize:wp('4%'), color:'white'}}></Icon2>
                            </TouchableOpacity>  
                            :null}

                            <TouchableOpacity  activeOpacity={0.9} onPress={() => navigation.navigate('3D', { skill:SkillPlayer, token, user, view:'notification' })} style={styles.buttonRepet}>
                                        <Text style={{fontWeight:'bold', fontSize:wp('4%'),  textTransform:'uppercase', color:'#3d3d3d'}}>{I18n.t('previaNotifications.record')}
                                        </Text>
                                        <Icon2 name="backup-restore" style={{marginLeft:10, fontSize:wp('4%'), color:'#3d3d3d'}}></Icon2>
                            </TouchableOpacity> 

                            </View> 


                            {SkillPlayer.video_status == 0?
                            !SkillPlayer.public_status?
                             SkillPlayer.votospositivos < 3?
                            <View style={{justifyContent:'center', alignItems:'center'}}>
                                <Text style={{fontWeight:'bold', fontSize:wp('5%'), color:'#3d3d3d'}}>
                                {I18n.t('previaNotifications.text1')}  { 3 - SkillPlayer.votospositivos}  {I18n.t('previaNotifications.text2')}

                                </Text>
                            </View>
                            :null
                            :null
                        :null}


{/* 
                        {SkillPlayer.comment ?
                            <View style={{ bottom: 50, left: 0, position: "absolute", width: wp('100%'), }}>
                                <View style={{ marginLeft: 25, zIndex: 10, flexDirection: 'row' }}>
                                    {coach.photo ?
                                        <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: hp('6%'), height: hp('6%'), borderRadius: 50, }} />
                                        :
                                        <View style={{ width: hp('6%'), height: hp('6%'), borderRadius: hp('6%'), zIndex: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#D6D6D6', }}>
                                            <Text style={{ fontSize: hp('1.7%'), fontWeight: 'bold' }}>{((coach.name || "").charAt(0) || "").toUpperCase()}</Text>
                                        </View>
                                    }
                                    <View style={{ backgroundColor: '#FFF', height: 25, marginTop: 15, borderBottomRightRadius: 10, borderTopRightRadius: 10, marginLeft: -10, paddingLeft: 20, paddingRight: 10, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontSize: hp('1.5%') }}>{coach.name}</Text>
                                    </View>
                                </View>
                                <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', }}>
                                    <View style={{ paddingLeft: 10, width: wp('75%'), backgroundColor: '#FFF', padding: 10, marginTop: -10, borderTopEndRadius: 10, borderBottomRightRadius: 10, borderBottomLeftRadius: 10 }}>
                                        <Text style={{ fontSize: hp('1.7%') }}>{SkillPlayer.comment}</Text>
                                    </View>
                                </View>
                            </View>
                            : null} */}
                    </View>
                    :
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: '100%', height: '100%', }} />
                        <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate('3D', { skill:SkillPlayer, token, user, view:'notification' })} style={{ position: 'absolute', width: hp('20%'), height: hp('20%'), backgroundColor: 'rgba(0,0,0,0.3)', borderRadius: hp('20%'), borderColor: '#D6D6D6', borderWidth: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name="play" size={hp('6')} color="#FFF" style={{ marginLeft: 10 }} />
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate('3D', { skill:SkillPlayer, token, user, view:'notification' })} style={{ bottom: 20, width: wp('50%'), height: hp('4%'), elevation: 3, position: 'absolute', backgroundColor: '#FFF', justifyContent: 'center', alignItems: 'center', borderRadius: 10 }}>
                            <Text style={{ fontSize: hp('1.7%'), fontWeight: 'bold' }}>
                                {I18n.t('previaNotifications.buttonRecord')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                }
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    buttonRepet: {
        borderRadius:5, 
        backgroundColor:'white', 
        padding:9, 
        marginLeft:10,
        width:wp('40%'), 
        textAlign:'center', 
        alignItems:'center', 
        height:hp('5%'),  
        shadowColor: '#eee',
        shadowOffset: { width: 5, height: 4 },
        shadowOpacity: 0.7,
        flexDirection:'row',
        justifyContent:'center',
        elevation:3,
      },



    buttonShare: {
        borderRadius:5, 
        backgroundColor:'#11A10F', 
        padding:9, 
        width:wp('40%'), 
        textAlign:'center', 
        alignItems:'center', 
        height:hp('5%'),  
        shadowColor: '#eee',
        shadowOffset: { width: 5, height: 4 },
        shadowOpacity: 0.7,
        flexDirection:'row',
        justifyContent:'center',
        elevation:3,
      },

    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
});

//make this component available to the app
export default PreviaNotification;
