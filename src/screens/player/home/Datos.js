import  React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Modal, Pressable, Image } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScrollView } from 'react-native-gesture-handler';
import I18n from 'i18n-js';
const Datos = (props) => {
    // console.log(props, '____WEDwdwde')

    
    const [data, setData] = useState({});
    const [level, Level] = useState(null);
    useEffect(async() =>{
        setData(props.user)
        Level(props.level)

    }, [])
   

    return(
        <View>
           {/* MODAL NEXT NIVEL  */}
    <Modal
        animationType="slide"
        transparent={true}
        visible={props.visibleModal}
        onRequestClose={() => {
        //   Alert.alert("Modal has been closed.");
        //   setModalVisible(!modalVisible);
        }}
      >
       
        <View style={styles.centeredView}>
        <ScrollView>
          <View style={styles.modalView}>

            {/* <Text style={styles.modalTextTiTle}>{I18n.t('TutorialView.niveltitle')} 1!!</Text> */}

                <Image
                  style={styles.splah_happy}
                  source={require('../../../assets/playertutorial.png')}
                />


                  {level == '1'?

                  <View>
                  <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 1</Text>
                  <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level1.text1')}
                  </Text>
                  <Text style={{marginBottom:10}}> 
                     {I18n.t('TutorialView.level1.text2')}
                  </Text>
                 
                  <Text style={styles.areaTextTow}>
                  {I18n.t('TutorialView.level1.text3')}
                  </Text>
                  </View>


                  :level == '2'?
                  <View>
                  <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 2</Text>
                  <Text style={styles.areaText}>
                  {I18n.t('TutorialView.level2.text1')}
                  </Text>
                  <Text style={{marginBottom:10}}>
                  {I18n.t('TutorialView.level2.text2')}
                  
                    </Text>
                  <Text style={styles.areaTextTow}>
                  {I18n.t('TutorialView.level2.text3')}
                 
                  </Text>
                  </View>

                  : level == '3'?
                  <View>
                  <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 3</Text>
                  <Text style={styles.areaText}>
                  {I18n.t('TutorialView.level3.text1')}
                  
                  </Text>
                  <Text style={{marginBottom:10}}>
                  {I18n.t('TutorialView.level3.text2')}
                  </Text>

                  </View>

                  :level == '4'?
                  <View>
                  <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 4</Text>
                  <Text style={styles.areaText}>
                  {I18n.t('TutorialView.level4.text1')}
                  
                  </Text>

                  <Text style={{marginBottom:10}}>
                  {I18n.t('TutorialView.level4.text2')}
                  </Text>


                  </View>

                  :level == '5'?
                  <View>
                  <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 5</Text>
                  <Text style={styles.areaText}>
                  {I18n.t('TutorialView.level5.text1')}
                  
                  </Text>
                  <Text style={{marginBottom:10}}> 
                  {I18n.t('TutorialView.level5.text2')}
                  </Text>

                  </View>
                  :level == '6'?
                  <View>
                  <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 6</Text>
                  <Text style={styles.areaText}>
                  {I18n.t('TutorialView.level6.text1')}
                  </Text>

                  <Text style={{marginBottom:10}}>
                  {I18n.t('TutorialView.level6.text2')}
                 </Text>

                  </View>

                  :level == '7'?
                  <View>
                  <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 7</Text>
                  <Text style={styles.areaText}>
                  {I18n.t('TutorialView.level7.text1')}
                  
                  </Text>

                  <Text style={{marginBottom:10}}>
                  {I18n.t('TutorialView.level7.text2')}
                   </Text>

                  </View>


                    :level == '8'?
                    <View>
                    <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 8</Text>
                    <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level8.text1')}
                    
                    </Text>
                    </View>

                     :level == '9'?
                     <View>
                     <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 9</Text>
                     <Text style={styles.areaText}>
                     {I18n.t('TutorialView.level9.text1')}
                     
                     </Text>
                     </View>

                     :level == '10'?
                     <View>
                     <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 10</Text>
                     <Text style={styles.areaText}>
                     {I18n.t('TutorialView.level10.text1')}
                     
                     </Text>
                     </View>

                   

                    :level == '11'?
                    <View>
                    <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 11</Text>
                    <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level11.text1')}
                    
                    </Text>
                    </View>


                    :level == '12'?
                    <View>
                    <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 12</Text>
                    <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level12.text1')}
                    </Text>
                    </View>

                    :level == '13'?
                    <View>
                    <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 13</Text>
                    <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level13.text1')}
                    
                    </Text>

                  <Text style={{marginBottom:10}}> You will also learn another Turn Away skill and how to control the ball with your chest, this one looks easier than it is. What is important is not to let the ball bounce away from you but keep the ball in reach right in front of you.</Text>

                    </View>

                    :level == '14'?
                    <View>
                    <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 14</Text>
                    <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level14.text1')}
                    
                    </Text>

                  <Text style={{marginBottom:10}}> 
                  {I18n.t('TutorialView.level14.text2')}
                  </Text>



                    </View>

                    :level == '15'?
                    <View>
                    <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 15</Text>
                    <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level15.text1')}
                    </Text>

                  <Text style={{marginBottom:10}}>
                  {I18n.t('TutorialView.level15.text2')}
                  </Text>

                    </View>

                    :level == '16'?
                    <View>
                    <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 16</Text>
                    <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level16.text1')}
                     
                    </Text>

                  <Text style={{marginBottom:10}}>
                  {I18n.t('TutorialView.level16.text2')}
                  </Text>


                    </View>

                    :level == '17'?
                    <View>
                    <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 17</Text>
                    <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level17.text1')}
                    
                    </Text>
                    </View>


                    :level == '18'?
                    <View>
                    <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 18</Text>
                    <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level18.text1')}
                    
                    </Text>
                    </View>

                    :level == '19'?
                    <View>
                    <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 19</Text>
                    <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level19.text1')}
                    
                    </Text>

                  <Text style={{marginBottom:10}}>
                  {I18n.t('TutorialView.level19.text2')} </Text>


                    </View>

                    :level == '20'?
                    <View>
                    <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 20</Text>
                    <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level20.text1')}
                    </Text>
                    </View>

                    :level == '21'?
                    <View>
                    <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 21</Text>
                    <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level21.text1')}
                    </Text>
                    </View>

                    :level == '22'?
                    <View>
                    <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 22</Text>
                    <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level22.text1')}
                    
                    </Text>
                    </View>

                    :level == '23'?
                    <View>
                    <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 23</Text>
                    <Text style={styles.areaText}>
                    {I18n.t('TutorialView.level23.text1')}
                    
                    </Text>
                    </View>


                    :level == '24'?
                                        <View>
                                        <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 24</Text>
                                        <Text style={styles.areaText}>
                                        {I18n.t('TutorialView.level24.text1')}
                                        
                                        </Text>
                                        </View>


                    :level == '25'?
                                        <View>
                                        <Text style={styles.subTitle}>{I18n.t('TutorialView.niveltitle')} 25</Text>
                                        <Text style={styles.areaText}>
                                        {I18n.t('TutorialView.level25.text1')}
                                        </Text>
                                        </View>
                  :null

                }



                  <Pressable
                    style={[styles.button, styles.buttonClose, {justifyContent:'center', alignItems:'center',marginTop:20 }]}
                    onPress={() => props.botonClick()}
                  >
                    <Text style={styles.textStyle}>{I18n.t('TutorialView.buttom')}</Text>
                    <Icon name="chevron-right" style={{position:'absolute',right:30, color:'white' }}></Icon>
                  </Pressable>
          </View>
          </ScrollView>
        </View>
      
      </Modal>

      {/* FIN MODAL NEXT LEVEL */}
        </View>
    )
}

export default Datos;

const styles = StyleSheet.create({
    splah_happy:{
        width:wp('55%'),
        height:hp('28%'),
      },
    
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 0,
    backgroundColor:'#0000002e',
    height:hp('100%')
    
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    width:wp('85%'),
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 5,
    padding: 10,
    elevation: 2,
    width:wp('50%'),
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#00c900",
    textTransform:'uppercase',
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    textTransform:'uppercase',
  },
  modalText: {
    marginBottom: 10,
    textAlign: "center"
  },


  modalTextTiTle: {
    marginBottom: 10,
    textAlign: "center",
    fontSize:wp('9%'),
    fontWeight:'normal',
    color:'#333'
  },

  subTitle:{
    fontWeight:'bold',
    fontSize:wp('6%'),
    // textAlign: "center",
    textTransform:'uppercase',
    color:'#404040',
    


  },
  areaText:{
    marginTop:22,
    marginBottom:10,
    // fontWeight:'700',
    opacity:0.9,
    fontSize:15,
    // textAlign: 'center',
    // lineHeight:22

  },

  areaTextTow:{
    marginTop:0,
    marginBottom:22,
    // fontWeight:'bold',
    // opacity:0.9,
    fontSize:15,
    // textAlign: "center",
    // lineHeight:22

  },



  textPuntos:{
    backgroundColor:'#eee',
    padding:2,
    paddingLeft:10, 
    paddingRight:10,
    color:'#404040',
    borderRadius:5,
    marginTop:10,
    fontSize:wp('4%'),
    
  },






})