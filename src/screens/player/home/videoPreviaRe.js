//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet,TouchableOpacity } from 'react-native';
import Video2 from 'react-native-video';
import Loader from '../../coach/global/loader';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

// create a component
const VideoRe = ({navigation, route}) => {
    const {video } = route.params;
    const [loader, setLoader] = useState(false);
    var videoplay = video;


    console.log("video de la vista", videoplay);
   

    useEffect(() => {
        setLoader(true);
    }, []);

    // useLayoutEffect(() => {
    //     navigation.setOptions({
    //         title: player.name,
    //     })
    // }, [])

    return (
        <View style={styles.container}>

            {loader ? <Loader title={"loading video..."}/> : null}
            <Video2 
                source={{ uri: route.params.video.data }}
                controls={true}
                resizeMode="cover"
                onLoad={() => setLoader(false)}
                style={styles.backgroundVideo} 
            />


        <TouchableOpacity activeOpacity={0.5} onPress={() =>{}} style={{ width: wp('10%'), height: '100%', justifyContent: 'center', alignItems: 'center', paddingRight: 20 }}>
          {/* <Image source={require('../../../assets/iconos/mobile.png')} style={{width:40, height:40}} /> */}
          <Icon2 name="rotate-3d" size={hp('3%')} />
          <Text>Enviar</Text>
        </TouchableOpacity>



           



        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
});

//make this component available to the app
export default VideoRe;
