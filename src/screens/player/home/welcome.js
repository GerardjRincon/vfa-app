//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native';
import { Block } from 'galio-framework'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

// constantes de vista
const { width, height } = Dimensions.get('screen');

// create a component
const WelcomePlayerH = ({navigation, route}) => {
    const { user } = route.params;

    useLayoutEffect(() => {
        navigation.setOptions({
          title: '',
        });
    }, [])

    function redirec() {
        navigation.replace('validationSesion', { user })
    }

    return (


    


        <View style={styles.container}>
            
            <Image source={require('../../../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'), position: "absolute",
            }} />
            <View style={{flex:1}}>
               
                <View style={{width:wp('100%'), justifyContent:'center', alignItems:'center'}}>
                <Text style={{fontWeight:'bold', fontSize:hp('3%'), paddingTop:hp('5%')}}>WELCOME TO VFA</Text>
                </View>
                <View style={{flex:1, justifyContent:'center', paddingHorizontal:20}}>
                    <View style={{width:'100%', alignItems:'center', paddingBottom:hp('5%')}}>
                        <Image source={require('../../../assets/home/welcome.png')} resizeMode='contain' style={{width:hp('20%'), height:hp('20%')}} />
                    </View>
                    <Text style={{fontSize:hp('2%'), fontWeight:'bold', textAlign:'justify'}}>Go through VFA training program, show your technical talent to perform the 100 skills in AR, post your videos and get rated, conquer the VFA rankings:</Text>
                    <Text style={{fontSize:hp('1.7%'),paddingTop:hp('2%')}}>By skill, be the best performing each single skill.</Text>
                    <Text style={{fontSize:hp('1.7%'),paddingTop:hp('2%')}}>By level,  break the charts by doing each 4 skills from level 1 until 25</Text>
                    <Text style={{fontSize:hp('1.7%'),paddingTop:hp('2%')}}>Global ranking, complete the entire 25 levels and get on the top of the VFA world as a LEGEND!</Text>
                </View>
                <View>
                    <Block center>
                        <TouchableOpacity activeOpacity={0.9} onPress={() => {
                                redirec()
                            }} style={styles.createButton}>
                            <Text style={{fontWeight:'bold', color:'#FFF', fontSize:hp('1.7%')}}>
                                CONTINUE
                            </Text>
                        </TouchableOpacity>
                    </Block>
                </View>
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: 'white',
    },
    createButton: {
        width: width * 0.8,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#11A10F',
        backgroundColor: '#11A10F',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderRadius:10,
        elevation: 5,
        justifyContent:'center',
        alignItems:'center',
        height:hp('6%'),
      },
});

//make this component available to the app
export default WelcomePlayerH;
