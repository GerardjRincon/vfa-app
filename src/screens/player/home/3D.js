//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { Text, View, Alert, StyleSheet, TouchableOpacity, Image, Modal, Platform, PermissionsAndroid, Dimensions, StatusBar } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import UnityView, { MessageHandler, UnityModule } from '@asmadsen/react-native-unity-view';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Snackbar from 'react-native-snackbar';
import Share from 'react-native-share';
import Video2 from 'react-native-video';
import Loader from '../../coach/global/loader';
import { url } from '../../../storage/config';
import CameraRoll from "@react-native-community/cameraroll";
import { request, requestMultiple, PERMISSIONS } from 'react-native-permissions';
import I18n from 'react-native-i18n';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

// constantes de vista
const { width, height } = Dimensions.get('screen');

// create a component
const Skill3D = ({ navigation, route }) => {
  const { skill, token, lenguaje, user, view } = route.params;

  console.log(route.params, ' ----------------- Aqui estamos ');

  console.log(lenguaje, 'lenguaje');
  console.log("esta skil");


  const [ar, setAr] = useState(false);
  const [video, setVideo] = useState(null);
  // const [video, setVideo] = useState('/storage/emulated/0/Android/data/com.go.vfa.virtual/files/recording_2021_12_27_18_10_13_143.mp4');
  const [visibleMenu, setVisibleMenu] = useState(false);
  const [visiblePostVideo, setVisiblePostVideo] = useState(false);
  const [link, setLink] = useState(null);
  const [loader, setLoader] = useState(false);
  const [textLoader, setTextLoader] = useState('Loading...');
  const [dataSkill ,setDataSkill] = useState({});

  const [selectVideo, setSelectVideo] = useState(false);
  const [stateRecord, setStateRecord] = useState(false);
  const [statePrevia, setStatePrevia] = useState(false);

  const [data, setData] = useState({
    id: view === 'notification'? skill.skill_id : skill.id,
    token: token,
    type: 1,
    username: user.username.replace('@', ''),
    height:960,
    width:540,
    CanRecord:skill.record,
    Language:lenguaje,
    usertype:'Player'
  })

  useLayoutEffect(() => {
    navigation.setOptions({
      title: view === 'notification'? I18n.t('skills.' + skill.id) : I18n.t('skills.' + skill.id),
      headerLeft: (props) => {
        return (
          <BottonBack {...props} />
        )
      },
      headerRight: (props) => {
        return (
          <BottonAR {...props} />
        )
      }
    });
  }, [])

  useEffect(async() => {

    UnityModule.postMessage('ReactManager', 'GetReactMessage', 'Clear3D');

    // // validamos si tiene los permisos para grabar  
    // if(skill.skill_user_id || user.value_method <=3){
    //   setStateRecord(true);
    //   record : true
    //   console.log("se cumple 1")
    // }
    // else if(user.value_method >= skill.level_id){
    //   setStateRecord(true);
    //   console.log("se cumple 2")
    //   record: true
 
    // }
    // else {
    //   setStateRecord(false);
    //   console.log("se cumple 3")
    //   record:false

    // }


    // console.warn(user)
    var d = JSON.stringify(data)
    // UnityModule.postMessage('ReactManager', 'UnityMessage', d)
    setTimeout(() => {
      UnityModule.postMessage('ReactManager', 'GetReactMessage', d);
    }, 2000)

    return () => {
      // console.warn('se desmonto')
      UnityModule.postMessage('ReactManager', 'GetReactMessage', 'Reload');
    }

  }, [])

  const BottonAR = () => {
    return (
      ar ?
        <TouchableOpacity activeOpacity={0.5} onPress={() => cambiar(false)} style={{ width: wp('10%'), height: '100%', justifyContent: 'center', alignItems: 'center', paddingRight: 20 }}>
          {/* <Image source={require('../../../assets/iconos/mobile.png')} style={{width:40, height:40}} /> */}
          <Icon2 name="rotate-3d" size={hp('3%')} />
        </TouchableOpacity>
        :
        <TouchableOpacity activeOpacity={0.5} onPress={() => cambiar(true)} style={{ width: wp('10%'), height: '100%', justifyContent: 'center', alignItems: 'center', paddingRight: 20 }}>
          {/* <Image source={require('../../../assets/iconos/mobile.png')} style={{width:40, height:40}} /> */}
          <Icon2 name="cube-scan" size={hp('3%')} />
        </TouchableOpacity>
    )
  }

  const BottonBack = (props) => {
    return (
      <View style={{ width: 100, height: '100%' }}>
        <TouchableOpacity activeOpacity={0.5} onPress={() => back()} activeOpacity={0.8} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginLeft: -50 }}>
          <Icon2 name="arrow-left" size={hp('2.4%')} color="#000" />
        </TouchableOpacity>
      </View>
    )
  }
  function Rolback(){
    // linpiar la vista 
    UnityModule.postMessage('ReactManager', 'GetReactMessage', 'HideAR')
    UnityModule.postMessage('ReactManager', 'GetReactMessage', 'Reload');
    setStatePrevia(false)
  }

  function back() {
    setStatePrevia(false)
    UnityModule.postMessage('ReactManager', 'GetReactMessage', 'HideAR')
    UnityModule.postMessage('ReactManager', 'GetReactMessage', 'Reload');
    navigation.goBack()
  }

  function cambiar(type) {
    if (type) {


      if (Platform.OS === 'android') {
        // async function requestCameraPermission() {
        //   try {
        //     const granted = await PermissionsAndroid.request(
        //       PermissionsAndroid.PERMISSIONS.CAMERA,
        //       PermissionsAndroid.PERMISSIONS.RECORD_VIDEO, {
        //         'title': 'Permiso Camara',
        //         'message': 'Tamyda necesita acceso a la camara'
        //       }
        //     )
        //     if (granted === PermissionsAndroid.RESULTS.GRANTED) {

        setAr(true)
        UnityModule.postMessage('ReactManager', 'GetReactMessage', 'ShowAR')

        //     } else {
        //       // alert("Permiso denegado");
        //       Snackbar.show({
        //         text: 'Permiso denegado.',
        //         duration: Snackbar.LENGTH_LONG,
        //       });
        //     }
        //   } catch (err) {
        //     // alert("Error:", err);
        //     Snackbar.show({
        //       text: 'Ha ocurrido un error.',
        //       duration: Snackbar.LENGTH_LONG,
        //     });
        //   }
        // }
        // requestCameraPermission();
      } else {
        // request(PERMISSIONS.IOS.CAMERA).then((result) => {
        //   if (result === 'granted'|'limited') {

        setAr(true)
        UnityModule.postMessage('ReactManager', 'GetReactMessage', 'ShowAR')

        //   }else{
        //     Snackbar.show({
        //       text: 'No se han concedido los permisos necesarios para acceder a la camara.',
        //       duration: Snackbar.LENGTH_LONG,
        //     });
        //   }
        // });
      }
    } else {
      setAr(false)
      UnityModule.postMessage('ReactManager', 'GetReactMessage', 'HideAR')
    }
  }

  function ShareVideo() {
    console.log(video.data);
    Share.open({
      // title: "Skill " + skill.name,
      // message: "I share this video with you",
      url: "file://" + video.data,
      // type: 'video/mp4',
      // subject: "Skill " + skill.name,
    })
  }

  function SaveVideo() {
    if (Platform.OS === 'android') {
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
            'title': I18n.t('ar.alert.permitcamera'),
            'message':I18n.t('ar.alert.msgpermitcamera'),
          }
          )
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {


            CameraRoll.save("file://" + video.data)
            setVisibleMenu(false)

            // Donde podemos pasar a la otra ruta 

           



            setTimeout(() => {
              Snackbar.show({
                text:I18n.t('ar.alert.textsuccessfully'),
                duration: Snackbar.LENGTH_LONG,
              });
            }, 1)

          } else {
            // alert("Permiso denegado");
            Snackbar.show({
              text:I18n.t('ar.alert.textdenied'),
              duration: Snackbar.LENGTH_LONG,
            });
          }
        } catch (err) {
          // alert("Error:", err);
          Snackbar.show({
            text: I18n.t('ar.alert.hasoccurred'),
            duration: Snackbar.LENGTH_LONG,
          });
        }
      }
      requestCameraPermission();
    } else {
      request(PERMISSIONS.IOS.PHOTO_LIBRARY).then((result) => {
        if (result === 'granted' | 'limited') {

          CameraRoll.save("file://" + video.data)
          setVisibleMenu(false)
          setTimeout(() => {
            Snackbar.show({
              text:I18n.t('ar.alert.textsuccessfully'),
              duration: Snackbar.LENGTH_LONG,
            });
          }, 1)
        } else {
          Snackbar.show({
            text:I18n.t('ar.alert.necessarypermissions'),
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
    }
  }


  async function videoEnviarCoach(){
    
    if(skill.skill_user_id){
    setTextLoader(I18n.t('ar.alert.uploading'))
    setVisibleMenu(false)
    setLoader(true);
    try {
      const data = new FormData();
      data.append('video', {
        uri: "file://" + video.data,
        name: 'video.mp4',
        type: 'video/mp4',
      });
      
      data.append('skill_user_id', skill.skill_user_id);
      data.append('skill_id', skill.id);
      data.append('video_status', 1); 
      data.append('user_id', user.id);
      data.append('name_method', user.name_method);
      data.append('value_method', user.value_method);

      console.log(data);


      await fetch(url + 'api/skill/file', {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer ' + token
        },
        body: data
      }).then(res => res.json())
        .then((dat) => {
          console.log(dat)
          if (dat.ok) {
            
            setStatePrevia(false)

            Snackbar.show({
              text: I18n.t('video3D.sendVideo.message2'),
              duration: Snackbar.LENGTH_LONG,
            });
            cambiar(false)
            setLoader(false);
            setLink(dat.url);
            setDataSkill(dat);
            setVisiblePostVideo(true);
          } else {
            // alert(dat.message);
            Snackbar.show({
              text: dat.message,
              duration: Snackbar.LENGTH_LONG,
            });
            setLoader(false);
          }
          setTextLoader('Loading...')
        });

        } catch (error) {
          console.log(error);
          Snackbar.show({
            text:I18n.t('ar.alert.uploading'),
            duration: Snackbar.LENGTH_LONG,
          });
          setLoader(false);
          setTextLoader('Loading...')
        }

    }else {

      Alert.alert(
        I18n.t('ar.alert.notassigned'),
        I18n.t('ar.alert.desnotassigned'),
        [
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
      );
    }
  

    }
   



  // You need to finish the previous level to be able to publish your video. 
 
  async function videoEnviarProfile(){

    console.log(user, '---user')
    console.log(skill, '---skill')

    
   
    if(Number(user.value_method) >= skill.level_id || skill.level_id <= 3){
    setTextLoader(I18n.t('ar.alert.uploading'))
    setVisibleMenu(false)
    setLoader(true);
    try {
      const data = new FormData();

      data.append('video', {
        uri: "file://" + video.data,
        name: 'video.mp4',
        type: 'video/mp4',
      });
      
      // data.append('skill_user_id',  null);
      data.append('skill_id', skill.id);
      data.append('video_status', 0); 
      data.append('user_id', user.id);
      data.append('name_method', user.name_method);
      data.append('value_method', user.value_method);

      
      await fetch(url + 'api/skill/file', {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer ' + token,
           'Accept': "application/json"
          
        },
        body: data
      }).then(res => res.json())
        .then((dat) => {
          console.log(dat, 'trae esto')
          if (dat.ok) {

            setStatePrevia(false)
            
            Snackbar.show({
              text: I18n.t('video3D.sendVideo.message'),
              duration: Snackbar.LENGTH_LONG,
            });
            cambiar(false)
            setLoader(false);
            setLink(dat.url);
            setDataSkill(dat);
            setVisiblePostVideo(true);
          } else {
            // alert(dat.message);
            Snackbar.show({
              text: dat.message,
              duration: Snackbar.LENGTH_LONG,
            });
            setLoader(false);
          }
          setTextLoader('Loading...')
        });

    } catch (error) {
      console.log(error);
      Snackbar.show({
        text:I18n.t('ar.alert.erroruploading'),
        duration: Snackbar.LENGTH_LONG,
      });
      setLoader(false);
      setTextLoader('Loading...')
    }

    }else {

      Alert.alert(
        I18n.t('ar.alert.allowedpost'),
        I18n.t('ar.alert.needaprove'),
        [
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
      );
    }
  

    }
   
  // async function SendAndSave() {

  
  // }

  const onMessage = (event) => {
    var data = JSON.parse(event)
    // setVisibleMenu(true)
    setVideo(data)
    setStatePrevia(true);

    

    console.log("video de la vista que pasamos ", data);
    // navigation.navigate('VideoRe', {video:data})

    // video que pasamos 
    
  }





async function CompartirIos(){

const url = "file://" + video.data;
let title = I18n.t('ar.shareSocial.text1') +' '+ user.name +' '+ I18n.t('ar.shareSocial.text2') + link;
let message =  I18n.t('ar.shareSocial.text1') +' '+ user.name +' '+ I18n.t('ar.shareSocial.text2') + link;
const options = Platform.select({
  ios: {
    activityItemSources: [
      {
        // For sharing url with custom title.
        placeholderItem: { type: 'url', content: url },
        item: {
          default: { type: 'url', content: url },
        },
        subject: {
          default: title,
        },
        linkMetadata: { originalUrl: url, url, title },
      },
      {
        // For sharing text.
        placeholderItem: { type: 'text', content: message },
        item: {
          default: { type: 'text', content: message },
          message: null, // Specify no text to share via Messages app.
        },
        linkMetadata: {
          // For showing app icon on share preview.
          title: message,
        },
      },
    ],
  },
  default: {
    title,
    subject: title,
    message: `${message} ${url}`,
  },
});

Share.open(options);


    }



    async function Compartir() {
            Share.open({
              title: I18n.t('ar.shareSocial.text1') +' '+ user.name +' '+ I18n.t('ar.shareSocial.text2') + link,
              message: I18n.t('ar.shareSocial.text1') +' '+ user.name +' '+ I18n.t('ar.shareSocial.text2') + link,
              url: "file://" + video.data,
            });
    }


console.log(link, '--- skill data')
console.log(video, 'video ---');
console.log(user, 'user ---');
  return (
    <View style={styles.container}>

      {loader ? <Loader title={textLoader}/> : null}
      <UnityView
        onLayout={(event) => {
          console.warn(event.nativeEvent.layout.height)
        }}
        style={{ width: '100%', height: '100%', }}
        onMessage={onMessage.bind(this)}
      />


    {/* MODAL PARA ENVIAR EL VIDEO  */}
      <Modal
        animationType="slide"
        transparent={true}
         visible={visibleMenu}>
         {/* visible={true}>    */}
        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-end' }}>
          <View style={{ height: "40%", width: '100%', backgroundColor: "#F3F3F3",  borderTopRightRadius: 10, borderTopLeftRadius: 10 }}>
            {/* <TouchableOpacity onPress={() => setVisibleMenu(false)} style={{ flex: 0.1, width: '100%', height: '6%', justifyContent: 'center', alignItems: 'center' }}>
              <Icon name="chevron-down" size={hp('2.3%')} color="#000" />
            </TouchableOpacity> */}
            <View style={{ flex: 1, borderTopRightRadius: 10, borderTopLeftRadius: 10, }}>
              <View style={{ flex: 1,}}>

                {user.coach_id?   //cuando el carajito tiene coach
                    skill.skill_user_id?
                    <View style={{flex:1, }}>
                      <View style={styles.alignMenu}>
                        <TouchableOpacity activeOpacity={0.5} onPress={() =>{
                          if (!loader) {
                            setSelectVideo(true)
                            videoEnviarCoach();
                          }else{
                            Snackbar.show({
                              text:I18n.t('ar.alert.thevideocoach'),
                              duration: Snackbar.LENGTH_LONG,
                            });
                          }
                        }} style={styles.menuItems}>
                          <Icon name="send" size={hp('2%')} />
                          <Text style={styles.alignTextMenu}>{I18n.t('video3D.option1ConCoach')}</Text>
                        </TouchableOpacity>
                        
                      </View>

                      {/* <View style={styles.alignMenu}>
                    <TouchableOpacity activeOpacity={0.9} onPress={() =>{
                      if (!loader) {
                         setSelectVideo(false)
                         videoEnviarProfile();
                      }else{
                        Snackbar.show({
                          text:I18n.t('ar.alert.thevideocoach'),
                          duration: Snackbar.LENGTH_LONG,
                        });
                      }
                    }} style={styles.menuItems}>
                      <Icon2 name="table-settings" size={hp('2%')} />
                      <Text style={styles.alignTextMenu}>
                       
                        {I18n.t('ar.alert.postvideo')} 
                         </Text>
                    </TouchableOpacity>
                  </View> 
                   */}

                      </View>
                      
                     : null
                   : // else por si no tiene coach
                  null
                }




                {/* BOTON DE PUBLICAR EN PERFIL EN CASO DE QUE TENGA COACH  */}
                {/* {!skill.skill_user_id?  */}
                 <View style={styles.alignMenu}>
                 <TouchableOpacity activeOpacity={0.9} onPress={() =>{
                   if (!loader) {
                    setSelectVideo(false)
                    videoEnviarProfile();
                   }else{
                     Snackbar.show({
                      text: I18n.t('ar.alert.thevideocoach'),
                       duration: Snackbar.LENGTH_LONG,
                     });
                   }
                   }} style={styles.menuItems}>
                     <Icon2 name="table-settings" size={hp('2%')} />
                     <Text style={styles.alignTextMenu}>
                       {/* {I18n.t('video3D.option1SinCoach')} */}
                       {I18n.t('ar.alert.postvideo')}  
                       </Text>
                   </TouchableOpacity>
                 </View>
                {/* :null} */}

                  {/* FIN  BOTON DE PUBLICAR EN PERFIL EN CASO DE QUE TENGA COACH  */}


                
                <View style={styles.alignMenu}>
                  <TouchableOpacity activeOpacity={0.9} onPress={() => ShareVideo()} style={styles.menuItems}>
                    <Icon2 name="share" size={hp('2%')} />
                    <Text style={[{ fontSize: hp('2%'), marginLeft: 10 }], styles.alignTextMenu}>{I18n.t('video3D.option2')}</Text>
                  </TouchableOpacity>
                </View>

                {/* /////    */}
                <View style={styles.alignMenu}>
                  <TouchableOpacity activeOpacity={0.9} onPress={() => SaveVideo()} style={styles.menuItems}>
                    <Icon2 name="download" size={hp('2%')} />
                    <Text style={styles.alignTextMenu}>{I18n.t('video3D.option3')}</Text>
                  </TouchableOpacity>
                </View>
                {/*  */}

                {/* //////// */}
                <View style={styles.alignMenu}>
                  <TouchableOpacity activeOpacity={0.9} onPress={() => {setVisibleMenu(false), setStatePrevia(false)}} style={styles.menuItems}>
                    <Icon2 name="reload" size={hp('2%')} />
                    <Text style={styles.alignTextMenu}>
                      {I18n.t('video3D.option4')}
                      </Text>
                  </TouchableOpacity>
                </View>
                {/*  */}


                {/* //////// */}
                <View style={styles.alignMenu}>
                  <TouchableOpacity activeOpacity={0.9} onPress={() => {setVisibleMenu(false), setStatePrevia(false)}} style={styles.menuItems}>
                    <Icon name="times" size={hp('2%')} />
                    <Text style={styles.alignTextMenu}>
                       {I18n.t('ar.alert.cancel')}
                      </Text>
                  </TouchableOpacity>
                </View>
                {/*  */}




              </View>
            </View>
          </View>
        </View>
      </Modal>



      {/* MODAL LUEGO QUE ENVIO EL VIDEO  */}

      <Modal
        animationType="slide"
        transparent={true}
        // visible={true}>
         visible={visiblePostVideo}> 
        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
          <View style={{ height: "100%", width: '100%', backgroundColor: "#FFF", justifyContent: "center", borderTopRightRadius: 10, borderTopLeftRadius: 10 }}>
            <View style={{ flex: 1, borderTopRightRadius: 10, borderTopLeftRadius: 10, justifyContent:'center', alignItems:'center' }}>
              <View style={{width:wp('100%'),  justifyContent:'center', alignItems:'center'}}>
                <Image source={require('../../../assets/imgs/3D/player.png')} resizeMode="contain" style={{width:hp('35%'), height:hp('35%')}}/>

                {skill.video_status != 1? 
                <View style={{width:wp('80%'), alignItems:'center', }}> 
                <Text style={{fontSize:hp('2.5%'),textAlign:'center', fontWeight:'bold', opacity:0.7}}>{I18n.t('video3D.sendVideo.title')}</Text>
                <Text style={{fontSize:hp('1.7%'),textAlign:'center', opacity:0.7}}>{I18n.t('video3D.sendVideo.description')}</Text>
                </View>
                :
                <View style={{width:wp('80%'), alignItems:'center', }}> 
                <Text style={{fontSize:hp('2.5%'),textAlign:'center', fontWeight:'bold', opacity:0.7}}>{I18n.t('video3D.sendVideo.title2')}</Text>
                <Text style={{fontSize:hp('1.7%'),textAlign:'center', opacity:0.7}}>{I18n.t('video3D.sendVideo.description2')}</Text>
                </View>
                }

                

                
              </View>
              <View style={{width:wp('100%'), height:hp('10%'), marginTop:hp('5%'), justifyContent:'center', alignItems:'center'}}>
                <TouchableOpacity activeOpacity={0.9} onPress={()=>{setVisiblePostVideo(false); Rolback(); navigation.navigate('Home')}} style={{width:'30%', height:'50%', borderWidth:1, borderColor:"#9A9A9A",  borderRadius:10, flexDirection:'row', justifyContent:'center', alignItems:'center', backgroundColor: '#fff', elevation:3}}>
                  <Icon2 name="home" size={hp('2.5%')} color="#000" />
                  <Text style={{fontSize:hp('2%'), paddingLeft:5}}>{I18n.t('video3D.sendVideo.btn1')} </Text>
                </TouchableOpacity>
              </View>



              {skill.video_status != 1? 
              <View>
              <View style={{justifyContent:'center', alignItems:'center', marginTop:hp('2%')}}>
                  <View>
                    <Text>{I18n.t('video3D.sendVideo.text')} </Text>
                  </View>
                  <View style={{width:wp('100%'), height:hp('5%'), flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                    <View style={{ marginHorizontal:5, justifyContent:'center', alignItems:'center'}}>
                      <Image source={require('../../../assets/imgs/3D/wechat.png')} resizeMode="contain" style={{width:hp('3%'), height:hp('3%')}}/>
                    </View>
                    <View style={{ marginHorizontal:5, justifyContent:'center', alignItems:'center'}}>
                      <Image source={require('../../../assets/imgs/3D/telegrama.png')} resizeMode="contain" style={{width:hp('3%'), height:hp('3%')}}/>
                    </View>
                    <View style={{ marginHorizontal:5, justifyContent:'center', alignItems:'center'}}>
                      <Image source={require('../../../assets/imgs/3D/005-whatsapp.png')} resizeMode="contain" style={{width:hp('3%'), height:hp('3%')}}/>
                    </View>
                    <View style={{ marginHorizontal:5, justifyContent:'center', alignItems:'center'}}>
                      <Image source={require('../../../assets/imgs/3D/instagram.png')} resizeMode="contain" style={{width:hp('3%'), height:hp('3%')}}/>
                    </View>
                    <View style={{ marginHorizontal:5, justifyContent:'center', alignItems:'center'}}>
                      <Image source={require('../../../assets/imgs/3D/tik-tok.png')} resizeMode="contain" style={{width:hp('3%'), height:hp('3%')}}/>
                    </View>
                  </View>
              </View>
              <View style={{width:wp('100%'), height:hp('10%'), marginTop:hp('2%'), justifyContent:'center', alignItems:'center'}}>
                <TouchableOpacity activeOpacity={0.9} 
                onPress={() => {
                  Platform.OS == 'android'?
                   Compartir()
                   : 
                   CompartirIos()
                 }}
               
                style={styles.boton}>
                  <Text style={{fontSize:hp('2%'), paddingLeft:5, color:'#FFF', fontWeight:'bold' }}>{I18n.t('video3D.sendVideo.btn2')}</Text>
                </TouchableOpacity>
              </View>
              </View>
              :null}



            </View>
          </View>
        </View>
      </Modal>


      {/* VISTA PREVIA DEL VIDEO  */}

      {statePrevia? 
          <View style={styles.previa}>
             <Video2 
                    source={{ uri: video.data}}
                    controls={true}
                    resizeMode="cover"
                    // repeat
                    // onLoad={() => setLoader(false)}
                    style={styles.backgroundVideo} 
                />

                <View style={{flexDirection:'row', marginTop:30}}>
               
               
                <TouchableOpacity activeOpacity={0.5} onPress={() => setStatePrevia(false)} 
               style={{backgroundColor:'#f4f4f4', padding:9, borderRadius:5, marginRight:10}}>
                  <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                     <Text style={{color:'#3d3d3d', textTransform:'uppercase', fontWeight:'bold'}}>{I18n.t('video3D.option4')}</Text>
                     <Icon2 name="reload" size={hp('3%')} />
                    </View>
                  </TouchableOpacity>
                  
                   <TouchableOpacity activeOpacity={0.5} onPress={() => {setVisibleMenu(true)}} 
                 style={{backgroundColor:'#11A10F', padding:9, borderRadius:5, marginLeft:10}}>
                  <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                     <Text style={{color:'#fff', textTransform:'uppercase', fontWeight:'bold'}}>{I18n.t('previVideoSend.text1')}</Text>
                     <Icon2 name="arrow-right" size={hp('3%')}  style={{marginLeft:10, color:'#fff',}}/>
                    </View>
                  </TouchableOpacity>

                  
                </View>
          </View>
      :null}
      




    </View>
  );
};

// define your styles
const styles = StyleSheet.create({

  backgroundVideo: {
    // position: 'absolute',
    width:wp('90%'),
    height:hp('70%'),
    justifyContent:'center',
    alignItems:'center'
    // top: 0,
    // left: 0,
    // bottom: 0,
    // right: 0,
},

previa:{
  //  justifyContent:'center',
   alignItems:'center',
   backgroundColor:'white',
   width:wp('100%'),
   height:hp('89%'),
   position:'absolute',
   zIndex:3000
},

  alignMenu:{
    flex: 1, width: '100%', 
    backgroundColor:'#fff',
    borderBottomColor:'#eee',
    borderBottomWidth:1,
  },

  menuItems: {
    flex: 1, width: '100%', flexDirection: 'row',
    alignItems:'center',
    paddingLeft:20
  },

  alignTextMenu:{
    fontWeight:'bold', 
    marginLeft:10,
    color:'#3d3d3d',
    fontSize:hp('2%')
    
  },
  container: {
    flex: 1,
    backgroundColor: '#F3F3F3',
  },
  boton:{
      width: width * 0.8,
      marginTop: hp('10%'),
      shadowColor: '#11A10F',
      backgroundColor: '#11A10F',
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.8,
      borderRadius:10,
      elevation: 5,
      justifyContent:'center',
      alignItems:'center',
      height:hp('6%'),
  }
});

//make this component available to the app
export default Skill3D;
