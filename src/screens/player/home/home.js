import React, { BackHandler ,useState, useEffect,useLayoutEffect, useRef, useFocusEffect} from 'react'
import { Text, View, Button, StyleSheet, FlatList, Image, TouchableOpacity, Animated, Modal,Pressable, Dimensions, NativeModules } from 'react-native'
import { url } from '../../../storage/config';
// import { channel, pusher } from '../../../routes/validationSesion';
import { setSkill, setMethod, LogoutUser, setUser, setModalprimary } from '../../../storage/user/dataUser';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Snackbar from 'react-native-snackbar';
import Pusher from 'pusher-js/react-native'
import Loader from '../../coach/global/loader';
import { Block } from 'galio-framework'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {signal, canal} from '../../../storage/pusher';

import { logoutBackend } from '../../../routes/DrawerPlayer';




// import del api de pagos  
import RNIap, {
  InAppPurchase,
  PurchaseError,
  SubscriptionPurchase,
  acknowledgePurchaseAndroid,
  consumePurchaseAndroid,
  finishTransaction,
  finishTransactionIOS,
  purchaseErrorListener,
  purchaseUpdatedListener,
} from 'react-native-iap';



// Importando la modal global 

import Datos from './Datos';
import { calendarFormat } from 'moment';
import { ScrollView } from 'react-native-gesture-handler';
import I18n from 'i18n-js';



// constantes de vista
const { width, height } = Dimensions.get('screen');

const Home = ({ navigation }) => {

  

  const [modalVisible, setModalVisible] = useState(false);
  const [tutorialLevelVisible, setTutorialLevelVisible] = useState(false);
  const [primaryLevelVisible, setPrimaryLevelVisible] = useState(false);
  const refMethods = useRef(); 
  const [skills, setskills] = useState([]);
  const [methods, setMethods] = useState([]);
  const [userSesion, setUserSesion] = useState({});
  const [token, setToken] = useState(null);
  const [index, setIndex] = useState(1);
  const [init, setInit] = useState(0);
  const [selectMethod, setSelectMethod] = useState(null);
  const itemCard = useRef(new Animated.Value(0)).current;
  const [isCourse, setIsCourse] = useState(null);
  const [loader, setLoader] = useState(false);
  const [aviso, setAviso] = useState('Loading...')
  const [visibleWelcome, setVisibleWelcome] = useState(false);
  const [dataLevel, setDataLevel] = useState({});
  const [lenguaje, setLenguaje] = useState('en');

  



  useEffect( async() => {

    // function handleBackButton() {
    //   navigation.navigate('login');
    //   return true;
    // }


    // BackHandler.addEventListener('hardwareBackPress', () => { return true; });

    // getLanguages().then(languages => {
    //   console.log(languages, '---idioma');
    // });

    // Função responsável por adquirir o idioma utilizado no device
     const getLanguageByDevice = () => {
     return Platform.OS === 'ios'
     ? NativeModules.SettingsManager.settings.AppleLocale // Adquire o idioma no device iOS
     : NativeModules.I18nManager.localeIdentifier // Adquire o idioma no device Android
     }

     
     const language = getLanguageByDevice()
     let idioma = language.substr(0,2);

     console.log(idioma);



      if(idioma == 'es'){
        setLenguaje('es')
      }
      else if(idioma == 'zh'){
        setLenguaje('ch')
      }
      else{
        setLenguaje('en')
      }

      console.log(lenguaje, 'este es el idioma que pasara para el 3D')
      






     const normalizeTranslate = {
      'en': 'en_US',
      'es': 'en_US',
      'es': 'es_ES',
      'ch':'zh_TW',
      'ch':'zh_HK',
      'ch':'zh_MO',
      'ch':'zh_CN',
      'ch':'zh_SG',
      'ch':'zh_TW',
      'ch':'zh_TW',
      
    }







     await RNIap.initConnection(); // iniciando la aplicacion de pagos 

    // const unsub = navigation.addListener('focus', async() => {
      const u = await AsyncStorage.getItem('@user');
      const token1 = await AsyncStorage.getItem('@token');
      const skills = await AsyncStorage.getItem('@skills');
      const method = await AsyncStorage.getItem('@method');
      // const modalprimary = await AsyncStorage.getItem('@modalprimary');

      const methodJson = JSON.parse(method);
      const skillsJson = JSON.parse(skills);
      const userJson = JSON.parse(u);
      const tokenJson = JSON.parse(token1);

     
      AsyncStorage.getItem('@modalprimary')
      .then(res => {

        let jsonmodalprimary = JSON.parse(res);
        console.log("modal inicial", jsonmodalprimary)
        setPrimaryLevelVisible(jsonmodalprimary);
      })


      // const jsonmodalprimary = JSON.parse(modalprimary);

      // validacion test 
      if(userJson && userJson.email != 'dd@gmail.com'){
        if(userJson.invite != true){
          console.log('true 111');
          console.log( userJson.email, 'email')
          console.log("no cumplio la validacion del email ")

          // RNIap.endConnection();

          ValidacionSuscription(tokenJson, userJson); // llamamos a la funcion de verificar si tiene una suscripcion 
        }else {

          console.log('true --wef');
          RNIap.endConnection();
      }
       
      }else {

        console.log('true --wef');
        RNIap.endConnection();
    }

      //  Hook 
      setUserSesion(userJson);
      setToken(tokenJson);
      setskills(skillsJson);
      setMethods(methodJson);
     
      // Fin Hook 
      getUser(tokenJson);


      const canal = signal.subscribe("VFA");
      setTimeout(()=>{
        if (methodJson) {
          if (userJson.name_method !== 'course') {
            isFocusInit(1,methodJson);
          }
        }
      },10) 
      
 

      canal.bind("NotificationReplyPlayerUpdate-"+userJson.id, async (data) =>{
        getUser(tokenJson);
       })


      canal.bind("NotificationX-" + userJson.id, async (data) => {
        console.log(data);
        setDataLevel(data);
        setModalVisible(true)
      });

      canal.bind("NotificationNewVideo-" + userJson.id, async (data) => {
          console.log('si se cumplio el evento que kennth dijo')
          getUser(tokenJson);
          getList(tokenJson,userJson);
      });


      canal.bind("NotificationReprobadoPublic-" + userJson.id, async (data) => {
          console.log(" si entramos en reprobado");
          getList(tokenJson,userJson);
      });

      canal.bind("NotificationAprobadoPublic-" + userJson.id, async (data) => {
        console.log(" si entramos en reprobado");
        getUser(tokenJson);
     });



      canal.bind("Notification-" + userJson.id, async (data) => {

        const token1 = await AsyncStorage.getItem('@token');
        const u1 = await AsyncStorage.getItem('@user');
        const userJson1 = JSON.parse(u1);
        const tokenJson = JSON.parse(token1);
        console.warn(data)
        if (data.tag === 'SkillAssigned' || data.tag === 'SkillReviewed' || data.tag === "ReprobadoPublic" || data.tag === 'SkillAssignedDeleted') {
          getUser(tokenJson);
          getList(tokenJson,userJson);
        }
        if(data.tag === 'ChangePlayerMethod') {
          setUser(data.noti)
          setUserSesion(data.noti)
          getUser(tokenJson);
          setAviso('Changing work method');
        }
      });

      Animated.spring(itemCard,{
        toValue:100
      }).start()

     // });
        // const backHandler = BackHandler.addEventListener('hardwareBackPress', handleBackButton);

        return ()=>{
          canal.disconnect();
          // backHandler.remove();
          // unsub;
    }
  }, [])



  // const isSelectionModeEnabled;
  // const  disableSelectionMode;

  
 

   //   Validar suscripcion 
   async function ValidacionSuscription(tokenJson, userJson){
    try {
      console.info(
         'verificamos si tiene o no planes consumibles'
      );
      const purchases = await RNIap.getAvailablePurchases();
      console.info('Available purchases :: ', purchases);
      if (purchases && purchases.length > 0) {
        RNIap.endConnection();
        console.log("suscripcion activa");
       }else{
         console.log("no tiene suscripciones");
         ResetPaymentAccount(tokenJson, userJson);
      }
      } catch (err) {
       console.warn(err.code, err.message);
       alert(err.message);
    }
  };

  async function  ResetPaymentAccount(tokenJson, userJson){

    try {


      const datos =  {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenJson
        },
        body: JSON.stringify({
            id:userJson.id,
            pay:false
        }),
    }

    console.log(datos, 'enviando datos function ResetPaymentAccount');


      await fetch('https://go.vfa.app/api/play/unpay',datos).then(response => response.json())
      .then((res) => {

        console.log(res, 'respuesta de la consola logout')
            
        // si se cumple que el user se deslogue 
        alert(I18n.t('homeplayer.alert.nosuscription'))  /// idioma --

        logoutBackend(tokenJson);
       
      }).catch(error=>{
          console.log("aqui");
          console.warn(error)
      });
  } catch (error) {
      console.warn(error)
  }

  }


  async function logoutBackend(tokenJson){
    await fetch(url + 'api/logout', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenJson
        },
    }).then(res => res.json())
    .then(async(dat) => {
        console.warn(dat)
        try {
            if (dat.ok) {
                const keys = ['@user', '@token', '@skills', '@reviews', '@team', '@coach']
                
                // signal.disconnect();
                signal.unsubscribe("VFA");
                await AsyncStorage.multiRemove(keys)
                navigation.replace('Login');
            }
        } catch (error) {
            Snackbar.show({
                text:I18n.t('homeplayer.alert.fileLogout'),
                duration: Snackbar.LENGTH_LONG,
            });
        }
    })
}


  //    async function getInfo() {

  //     const u = await AsyncStorage.getItem('@user');
  //     const token = await AsyncStorage.getItem('@token');
  //     const skills = await AsyncStorage.getItem('@skills');
  //     const method = await AsyncStorage.getItem('@method');
  //     const methodJson = JSON.parse(method);
  //     const skillsJson = JSON.parse(skills);
  //     const userJson = JSON.parse(u);
  //     const tokenJson = JSON.parse(token);

  //     setUserSesion(userJson);
  //     setToken(tokenJson);
  //     setskills(skillsJson);
  //     setMethods(methodJson);
  //     getUser(tokenJson);
  //     getList(tokenJson,userJson)
  //     console.warn(userJson)
  //     setTimeout(()=>{
  //       if (methodJson) {
  //         if (userJson.name_method !== 'course') {
  //           isFocusInit(1,methodJson);
  //         }
  //       }
  //     },10)
  //     canal.bind("Notification-" + userJson.id, async (data) => {

  //       const token1 = await AsyncStorage.getItem('@token');
  //       const u1 = await AsyncStorage.getItem('@user');
  //       const userJson1 = JSON.parse(u1);
  //       const tokenJson = JSON.parse(token1);
  //       console.warn(data)
  //       if (data.tag === 'SkillAssigned' || data.tag === 'SkillReviewed' || data.tag === "AprobadoPublic" || data.tag === "ReprobadoPublic" || data.tag === 'SkillAssignedDeleted' || data.tag === 'SkilReviewPlayer' || data.tag === 'Pantalla') {
  //         getUser(tokenJson);
  //         console.warn(tokenJson)
  //       }
  //       if(data.tag === 'ChangePlayerMethod') {
  //         setUser(data.noti)
  //         setUserSesion(data.noti)
  //         getUser(tokenJson);
  //         setAviso('Changing work method');
  //         console.warn(data.noti)
  //       }
  //     });
  //     Animated.spring(itemCard,{
  //       toValue:100
  //     }).start()
  // }

  async function getList(tokenJson,userJson) {
    setLoader(true)
    try {
      await fetch(url + 'api/filter/player/list', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + tokenJson
        }
      }).then(res => res.json())
        .then((dat) => {
          // console.warn(user)
          console.log(dat, 'lista')
          try {
            if (dat.ok) {
              setSkill(dat.data);
              setskills(dat.data);
              setMethod(dat.method);
              setMethods(dat.method);
              if(dat.niveles){
                setIsCourse(dat.niveles[0]);
              }else{
                setIsCourse(null);
              }
              // console.warn(dat.method[0].id)
              if (userSesion.coach_id) {
                if (userJson.name_method === "course") {
                  isFocusInit(dat.method[0].id,dat.method);
                  // // setInit(dat.method[0].id);
                  isFocus(dat.method[0].id,dat.method);
                  setIndex(dat.method[0].id);
                }
                //  / 
                else{
                  var idM = parseInt(userJson.value_method)
                  // console.log('recibo este: '+idM)
                  isFocusInit(idM,dat.method);
                  // console.warn('value method: '+idM);
                  // setInit(idM);
                  isFocus(idM, dat.method);
                  setIndex(idM);
                }
              }
              else {
                var idM = parseInt(userJson.value_method)
                  // console.log('recibo este: '+idM)
                  isFocusInit(idM,dat.method);
                  // console.warn('value method: '+idM);
                  // setInit(idM);
                  isFocus(idM, dat.method);
                  setIndex(idM);
              }
            }
            if (dat.message == 'Unauthenticated.') {
              LogoutUser()
              setTimeout(() => {
                navigation.replace('Login')
              }, 1000)
            }
            setLoader(false)
            setAviso('Loading...')
          } catch (error) {
            console.log(error)
            setLoader(false)
            setAviso('Loading...')
          }
        })

    } catch (error) {
      console.log(error)
      setLoader(false)
      setAviso('Loading...')
    }
  }

  async function Reload(){
    setModalVisible(!modalVisible)
    getUser(token);
  }

  async function getUser(tokenJson) {
    try {
      await fetch(url + 'api/user', {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + tokenJson
        }
      }).then(res => res.json())
        .then((dat) => {
        
          try {
            if (dat) {
              setUser(dat)
              setUserSesion(dat)
              getList(tokenJson,dat)
              console.log("entre aqui 1");
            }
          } catch (error) {
            console.log(error)
          }
        })

    } catch (error) {
      console.log(error)
    }
  }

  function isFocusInit(id,m){
    const array = [...m]
    const i = array.findIndex(
      item => id === item.id
    );
    if (array[i]) {
      array[i].isSelected = true;
    }else{
      array[0].isSelected = true;
    }
    setMethods(array)
    refMethods.current.scrollToIndex(
      {
        animated:true,
        index:array[i]?i:0,
        viewPosition:0,
        useNativeDriver:true,
      });
  }

  function isFocus(id,m){
    const array = [...m]
    array.forEach((item,i)=>{
      array[i].isSelected=false;
    })
    const i = array.findIndex(
      item => id === item.id
    );
    if (array[i]) {
      array[i].isSelected = true;
    }else{
      array[0].isSelected = true;
    }
    setMethods(array)
    refMethods.current.scrollToIndex(
      {
        animated:true,
        index:array[i]?i:0,
        viewPosition:0,
        useNativeDriver:true,
      });
  }

  function getItemLayout(data, i) {
    return (
      {
        length: wp('20'),
        offset: (wp('20%')+10) * i,
        index:i
      }    
    );
  }


  // Detectamos el posible color del item level 
  function colorItem(user, item){
    


     // backgroundColor: userSesion.value_method == item.number_level?'#11A10F':item.number_level >1 && item.number_level <= 3?'#fff':
            // userSesion.value_method < item.number_level?'#D8D8D8':'#fff',


  // Tenemos dos validaciones  
  // 1 levels menosres 

    if (item.number_level <= 3) {
      if(item.level_approved){
        return "#11A10F"
      }else {
        return "white"
      }
    }
    else {

      if(user.value_method == item.number_level){
        return "#fff"
      } else if(item.number_level >1 && item.number_level <= 3) {
        return "#11A10F"
      }
      else if(userSesion.value_method < item.number_level){
        return '#D8D8D8'
      }
      else{
        return "#11A10F"
      }

    }

  }

  function colorText(user, item){
    // userSesion.value_method == item.number_level?'#fff':'#6D706D'

    if (item.number_level <= 3) {
      if(item.level_approved){
        return "#fff"
      }else {
        return "#6D706D"
      }
    }
    else {
      if(user.value_method == item.number_level){
        return "#6D706D"
      }
      else if(item.level_approved){
        return "#fff"
      }else {
        return "#6D706D"
      }
    }


  }

 

  function setNumber(num) {
    num = num.toString().replace(/[^0-9.]/g, '');
    if (num < 1000) {
       return num;
    }
    let si = [{
          v: 1E3,
          s: "K"
       },
       {
          v: 1E6,
          s: "M"
       },
       {
          v: 1E9,
          s: "B"
       },
       {
          v: 1E12,
          s: "T"
       },
       {
          v: 1E15,
          s: "P"
       },
       {
          v: 1E18,
          s: "E"
       }
    ];
    let index;
    for (index = si.length - 1; index > 0; index--) {
       if (num >= si[index].v) {
          break;
       }
    }
    return (num / si[index].v).toFixed(2).replace(/\.0+$|(\.[0-9]*[1-9])0+$/, "$1") + si[index].s;
 }




  const dataSkill = ({ item, index, separators }) => {
    var urlImage = 'https://go.vfa.app/preview/' + item.img_preview;
    var num = item.points;

        return (

      
      <Animated.View key={item} style={{ width: wp('40%'), height: '95%', backgroundColor: '#FFF', marginHorizontal:wp('6%'), marginRight:0, borderRadius: 10, marginTop: 10, elevation: 3, transform:[{scale:itemCard.interpolate({inputRange: [-100, 0],outputRange: [-1, 0]})}] }}>
        <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate('Previa', {item, token: token,lenguaje, method: selectMethod })} style={{ flex: 1 }}>
          <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: wp('40%'), height: 200, borderRadius: 10 }} />
         

         {/* ITEM DEL VALOS DE LOS PUNTOS  */}
          <View style={{ position: 'absolute', flexDirection:'row', width: '40%', height: 20, backgroundColor: '#ECECEC', borderRadius: 5, right: 2, top: 2, justifyContent: 'center', alignItems: 'center' }}>
              <View style={{width:hp('2%'), height:hp('2%'), backgroundColor: '#D8D8D8', borderRadius:5, justifyContent:'center', alignItems:'center'}}>
                  <Icon2 name="flash" size={hp('1.5%')} color="#FFAF20" />
              </View>
              <Text style={{paddingLeft:5, opacity:0.7}}>{setNumber(num)} 
              <Text style={{fontSize:hp('1.2%')}}></Text>
              </Text>
              {/* {userSesion.name_method == "level"?
              <Text style={{ fontSize: hp('1.5%'), fontWeight: 'bold' }}>{index + 1}</Text>
            : 
              <Text style={{fontSize:hp('1.5%'), fontWeight:'bold'}}>{item.number}</Text> 
             }  */}
            
          </View>

          {/* FIN DEL ITEM DEL VALOS DE LOS PUNTOS  */}


          {/* NOMBRE DE LA CARTA  */}
          <View style={{ flex: 2, padding: 10 }}>
            <Text style={{ fontSize: hp('1.7%'),  color: '#3d3d3d', fontWeight:'bold' }}>
              {I18n.t('skills.'+item.id)}
       {/* {item.name} */}
     </Text> 
            <Text numberOfLines={1} style={{ fontSize: hp('1.4%'), color: '#000', opacity:0.5 }}>
              {/* {item.method_name} */}
              {I18n.t('teams.category.'+item.category_id)}
              </Text>
          </View>

          {/* FIN DEL NOMBRE DE LA CARTA*/}

         {/* DESASTRE DEL FOOTER DE LA CARTA  */}
          <View style={{ flex: 1, padding: 10, justifyContent: 'center', flexDirection: 'row' }}>
            {item.qualification ?
              <View style={{ flex:1, justifyContent: 'center', alignItems: 'center', alignItems: 'flex-start', paddingRight:5}}>
                <View style={{flex:1, justifyContent:'center', alignItems: 'center', flexDirection:'row'}}>
                  <Icon name="trophy" size={hp('1.8%')} color="#eab900" />
                  <Text style={{ fontSize: hp('1.8%'), marginLeft:5}}>{item.qualification}</Text>
                </View>
              </View>
              : null}
            {item.puntos ?
              <View style={{ flex:1, marginLeft:-40, flexDirection:'row', alignItems: 'center', paddingRight:5}}>
                <View style={{width:hp('2%'), height:hp('2%'), backgroundColor: '#D8D8D8', borderRadius:5, justifyContent:'center', alignItems:'center'}}>
                  <Icon2 name="flash" size={hp('1.5%')} color="#FFAF20" />
                </View>
                <Text style={{paddingLeft:5, opacity:0.7}}>{item.puntos} </Text>
              </View>
            :null}

            {item.qualification || item.puntos? 
                <View style={{ position:'absolute', right:5, }}>
                <View>
                   <Image
                    style={{width:30, height:25}}
                    source={require('../../../assets/check.png')}
                  />
                </View>
              </View>
            :null}
            {userSesion.coach_id? /// Si tiene coach
              item.skill_user_id?
              item.file_video === null?
              <View style={{ flex:1, justifyContent: 'center', alignItems: 'flex-end', padding: 5 }}>
                <Icon2 name="record-rec" size={hp('2%')} color="red" />
              </View>
              : 
               item.pending_review?
                <View style={{ flex:1, justifyContent: 'center', alignItems: 'flex-end', padding: 5, marginBottom:-5}}>
                  <Icon2 name="account-clock" size={hp('2%')} color="#FFA200" />
                </View>
              :null
             :null

             
            :  null
            
              // : item.file_video && item.public_status === 2?
              // <View style={{flex:1,flexDirection:'row',}}>
              //     <View style={{ width:'20%', justifyContent: 'center', alignItems: 'center', padding: 2 }}>
              //       <Icon2 name="backup-restore" size={hp('2%')} color="red" />
              //     </View>
              //   </View>
              // :null

            }

            {/// Si no tiene coach
              item.upload?   /// Pndiente es nulo para los que no tienen coach
              !item.public_status_0 || item.public_status_0 == 3?
                <View style={{flexDirection:'row', position:'absolute', left:50, bottom:11 }}>
                <View style={{ width:'28%', justifyContent: 'center', alignItems: 'center',  }}>
                  <Icon2 name="account-clock" size={hp('2%')} color="#FFA200" />
                </View>
                <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center', paddingHorizontal:10}}> 
                  <Icon2 name="thumb-up" size={hp('2%')} color="#1877f2" />
                  <Text style={{paddingLeft:5}}>{setNumber(item.votos)}</Text>
                </View>
              </View>
              :null
              
              :null
              }


          </View>


          {/* FIN DEL DESASTRE */}




        </TouchableOpacity>
      </Animated.View>
    );
  }

  


  // LISTADOS DE LOS ITEMS NIVELES 

  const dataMethod = ({ item, index, separators }) => {

    console.log(item.isSelected);
    return (
      <TouchableOpacity activeOpacity={0.8} onPress={() => { 
        setIndex(item.id)
        setSelectMethod(item.title)
        isFocus(item.id, methods)
        Animated.spring(itemCard,{
          toValue:0,
          useNativeDriver:true
        }).start()
        setTimeout(()=>{
          Animated.spring(itemCard,{
            toValue:100,
            useNativeDriver:true
          }).start()
        },400)
       }} 
       style={{
          width: wp('20'),
           height: '80%',

            // backgroundColor: userSesion.value_method == item.number_level?'#11A10F':item.number_level >1 && item.number_level <= 3?'#fff':
            // userSesion.value_method < item.number_level?'#D8D8D8':'#fff',

            backgroundColor:colorItem(userSesion, item),
             borderWidth:2, 
             borderColor:item.isSelected?'#11A10F':'#FFF',
              padding: 5,
               marginHorizontal: 10,
                borderRadius: 10,
                 marginTop: 10, 
                 elevation: 3,
                  justifyContent: 'center', 
                  alignItems: 'center' }}
                  
             >
                    
        <Text numberOfLines={2} style={{ fontSize:item.isSelected?hp('2%'):hp('1.6%'), fontWeight:'bold', textTransform:'uppercase', 
           color:colorText(userSesion, item)
           }}>
          {/* {item.title} */}
          {I18n.t('teams.levels.' + item.id)}
          </Text>
        {item.assigned?
          <View style={{position:'absolute', top:5 , right:5}}>
            <Icon name="circle" size={hp('1%')} color="red"/>
          </View>
        :null}
           {userSesion.value_method >= item.number_level?
           null
          // <Icon name="bolt" size={hp('1.8%')} color="#eab900" style={{left:0}}/>
        :null}
      </TouchableOpacity>
    );
  }
  // FIN  DEL RENDER DE LOS ITEMS NIVELES 


  // LISTAS DE LOS COMPONENTES 
  return (
    <View style={styles.center}>

    
    



      {/* <modalGlobal /> */}
      {tutorialLevelVisible?
      <Datos visibleModal={tutorialLevelVisible} level={index} user={userSesion} botonClick={() =>{setTutorialLevelVisible(false)}}/>
       :null}


      <View style={{backgroundColor:'white', position:'absolute', right:0, zIndex:100, top:hp('25%'), height:hp('6%'), width:wp('6.5%'), justifyContent:'center', alignItems:'center', borderTopLeftRadius:5, borderBottomLeftRadius:5, elevation:3}}>
      <TouchableOpacity onPress={() => {setTutorialLevelVisible(true)}}>
         <Icon2 name="information"  size={20} style={{color:'green'}}></Icon2>
         </TouchableOpacity>
      </View>



{/* MODAL INICIO  */}
    <Modal
        animationType="slide"
        transparent={true}
        visible={primaryLevelVisible}
        onRequestClose={() => {
          // Alert.alert("Modal has been closed.");
          // setModalVisible(!primaryLevelVisible);
        }}
      >

       
        <View style={styles.centeredView}>
        <ScrollView>
          <View style={styles.modalView}>
            {/* <Text style={styles.modalTextTiTle}>Congratulations!!</Text> */}

                <Image
                  style={styles.splah_happyTwo}
                  source={require('../../../assets/home/welcome.png')}
                />

                <View>
                  <Text style={{color:'#3d3d3d'}}>
                 {/* text1 modal inicio  */}
                 {I18n.t('homeplayer.modalInicioPlayer.text1')}
                  </Text>
                  <Text style={{marginBottom:10,marginTop:10, color:'#3d3d3d'}}>
                  {I18n.t('homeplayer.modalInicioPlayer.text2')}
                     </Text>
                  <Text style={[styles.areaTextTow, {color:'#3d3d3d'}]}>
                  {I18n.t('homeplayer.modalInicioPlayer.text3')}
                  </Text>
                  
                  </View>

                  <Pressable
                    style={[styles.button, styles.buttonClose, {justifyContent:'center', alignItems:'center',marginTop:20 }]}
                    onPress={() => {
                      setPrimaryLevelVisible(false)
                      setModalprimary(false)
                    }}
                  >
                    <Text style={styles.textStyle}>{I18n.t('homeplayer.modalInicioPlayer.buttom')}</Text>
                    <Icon name="chevron-right" style={{position:'absolute',right:30, color:'white' }}></Icon>
                  </Pressable>
          </View>
          </ScrollView>
        </View>
        
      </Modal>

      {/* FIN MODAL INICIO */}




{/* MODAL NEXT NIVEL  */}
    <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          // Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalTextTiTle}>Congratulations!!</Text>

                <Image
                  style={styles.splah_happy}
                  source={require('../../../assets/next_level.png')}
                />
                  <Text style={styles.subTitle}>
                  {I18n.t('homeplayer.modalNextLevel.text1')} 
                  {/* {dataLevel.value_method} */}
                  </Text>
                  <Text style={styles.subTitle}>
                  {I18n.t('homeplayer.modalNextLevel.text2')}
                  </Text>

                  <Text style={styles.textPuntos}>You win  
                    <Icon2 name="flash" size={hp('1.5%')} color="#FFAF20" />
                    {dataLevel.points} pts</Text>

                  <Text style={styles.areaText}>
                  {I18n.t('homeplayer.modalNextLevel.text3')}
                    </Text>

                  <Pressable
                    style={[styles.button, styles.buttonClose]}
                    onPress={() => Reload()}
                  >
                    <Text style={styles.textStyle}>{I18n.t('homeplayer.modalNextLevel.buttom')}</Text>
                  </Pressable>
          </View>
        </View>
      </Modal>

      {/* FIN MODAL NEXT LEVEL */}





      {loader?<Loader title={aviso}/>:null}
      <Image source={require('../../../assets/pelota.png')} style={{
        height: '100%',
        width: wp('100%'),
        position: "absolute",
      }} />
      <View style={{ width: wp('100%'), height:isCourse?'22%':'12%', }}>
        {isCourse?
          <View style={{paddingLeft:20, paddingVertical:10, borderBottomWidth:1, borderColor:'#D6D6D6', marginBottom:5}}>
              <Text style={{fontSize:hp('2.5%'), fontWeight:'bold'}}>{isCourse.title}</Text>
          </View>
        :null}
        <FlatList
          ref={refMethods}
          data={methods}
          renderItem={dataMethod}
          keyExtractor={(item, i) => i}
          horizontal
          getItemLayout={(data, index) => getItemLayout(data, index)}
          showsHorizontalScrollIndicator={false}
          initialScrollIndex={init}
          style={{  flex: 1 }}
        />
      </View>
      <FlatList
        data={skills ? skills[index] : skills}
        renderItem={dataSkill}
        keyExtractor={(item, i) => i}
        numColumns={2}
        ListFooterComponent={<View style={{height:20}}/>}
        style={{ width: wp('100%'), flex: 1 }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  splah_happyTwo:{
    width:wp('35%'),
    height:hp('17%'),
    marginBottom:15
  },
  splah_happy:{
    width:wp('40%'),
    height:hp('25%'),
  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 0,
    backgroundColor:'#0000002e',
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    width:wp('85%'),
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 5,
    padding: 10,
    elevation: 2,
    width:wp('50%'),
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#00c900",
    textTransform:'uppercase',
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    textTransform:'uppercase',
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },


  modalTextTiTle: {
    marginBottom: 15,
    textAlign: "center",
    fontSize:wp('9%'),
    fontWeight:'normal',
    color:'#333'
  },

  subTitle:{
    fontWeight:'bold',
    fontSize:wp('6%'),
    textAlign: "center",
    textTransform:'uppercase',
    color:'#404040',
    


  },
  areaText:{
    marginTop:22,
    marginBottom:22,
    fontWeight:'normal',
    fontSize:wp('4%'),
    textAlign: "center",

  },

  textPuntos:{
    backgroundColor:'#eee',
    padding:2,
    paddingLeft:10, 
    paddingRight:10,
    color:'#404040',
    borderRadius:5,
    marginTop:10,
    fontSize:wp('4%'),
    
  },







  center: {
    flex: 1,
    backgroundColor: '#F3F3F3',
  },
  textSmall: {
   fontSize:9,
  },
  container: {
    flex: 1,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor: 'white',
  },
  createButton: {
    width: width * 0.8,
    marginTop: 25,
    marginBottom: 40,
    shadowColor: '#11A10F',
    backgroundColor: '#11A10F',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    borderRadius:10,
    elevation: 5,
    justifyContent:'center',
    alignItems:'center',
    height:hp('6%'),
  },
});

export default Home;