//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Modal,Button, SafeAreaView, Dimensions, Platform } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Snackbar from 'react-native-snackbar';
import Video2 from 'react-native-video';
import Loader from '../../coach/global/loader';
import { url } from '../../../storage/config';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { setUser } from '../../../storage/user/dataUser';
import * as RNFS from 'react-native-fs';
import Share from 'react-native-share';
import I18n from 'i18n-js';

// constantes de vista
const { width, height } = Dimensions.get('screen');

import PreviaVideo from './previaVideo';
// create a component
const Previa = ({ navigation, route }) => {

    const {item, token, method, lenguaje } = route.params;


    console.log(item, 'esta es la skill que ando buscan --------');

    const [loader, setLoader] = useState(false);
    const [loaderShare, setLoaderShare] = useState(false);
    const [loaderVideo, setLoaderVideo] = useState(true);
    const [skill, setSkill] = useState(item);
    const [coach, setCoach] = useState({});
    const [user, setUser] = useState({});
    const [congratulationCoach, setCongratulationCoach] = useState(false);
    const [congratulationSinCoach, setCongratulationSinCoach] = useState(false);
    const [textLoader, setTextLoader] = useState('Loading...');
    var urlImage = 'https://go.vfa.app/preview/' + skill.img_preview;
    var urlVideo = '';
    var urlImageVideo = '';


    // console.log(skill, 'entrando a esta skill --- ');


    // console.log('miskill ----- ', miskill)

    if (skill.file_video) {
        urlVideo = skill.file_video;
        urlImageVideo = skill.file_captura;
    }

    useEffect(async() => {
        const coach = await AsyncStorage.getItem('@coach');
        const user = await AsyncStorage.getItem('@user');
        const userJson = JSON.parse(user);
        const coachJson = JSON.parse(coach);
        console.warn(skill)
        setCoach(coachJson);
        setUser(userJson);
        // if (skill.file_video) {
        //     setLoader(true)
        // }
        
        console.log(lenguaje, 'donde estamos');


        if (userJson.coach_id) {
           if (skill.qualification) {
               if (!skill.status) {
                    setCongratulationCoach(true)
               }
           }
        }else{
            if (!skill.status) {
                if (skill.public_status == 1) {
                    setCongratulationSinCoach(true)
                }
            }
        }
    }, [])

    useLayoutEffect(() => {
        navigation.setOptions({
            title: I18n.t('skills.' + skill.id),
        });
    }, [])

    async function cambiarStado() {
        setLoader(true);
        console.warn(skill.id)
        try {
            await fetch(url+'api/play/skillview',{
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
                },
                body: JSON.stringify({
                  id: skill.skill_user_id,
                }),
            }).then(res => res.json())
            .then((dat) =>{
                console.warn(dat)
                try {
                    if (dat.ok) {
                        setCongratulationCoach(false);
                        setCongratulationSinCoach(false);
                        setLoader(false);
                        skill.status = true;
                        setSkill(skill);
                        console.warn('exito')
                    }
                } catch (error) {
                    console.warn(error)
                    setLoader(false);
                }
            }).catch(err => {
                console.warn(err);
                setLoader(false);
            });
        } catch (error) {
            console.warn(error);
            setLoader(false);
        }
    }


   async function CompartirIos(){

    console.log(user);

    // console.log(`${RNFS.DocumentDirectoryPath}/video.mp4`);
    var ubicacion =  `${RNFS.DocumentDirectoryPath}/video.mp4`;

    RNFS.downloadFile({
        fromUrl:skill.file_video,
        toFile: ubicacion,
    }).promise.then(res =>{
        console.log(res);
        console.log('file://' + ubicacion);


const url = "file://" + ubicacion;

if(skill.video_status == 0){
    var title = I18n.t('ar.shareSocial.text1') +' '+ user.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+user.username+'/'+skill.url_public;
    var message = I18n.t('ar.shareSocial.text1') +' '+ user.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+user.username+'/'+skill.url_public;
}
else {
    var title = '';
    var message = '';
}

const options = Platform.select({
  ios: {
    activityItemSources: [
      {
        // For sharing url with custom title.
        placeholderItem: { type: 'url', content: url },
        item: {
          default: { type: 'url', content: url },
        },
        subject: {
          default: title,
        },
        linkMetadata: { originalUrl: url, url, title },
      },
      {
        // For sharing text.
        placeholderItem: { type: 'text', content: message },
        item: {
          default: { type: 'text', content: message },
          message: null, // Specify no text to share via Messages app.
        },
        linkMetadata: {
          // For showing app icon on share preview.
          title: message,
        },
      },
    ],
  },
  default: {
    title,
    subject: title,
    message: `${message} ${url}`,
  },
});

Share.open(options);



        // Share.open({
        //     title: "Rate my performance on VFA.app using this link!",
        //     message: "Rate my performance on VFA.app using this link! "+ 'https://go.vfa.app/'+userJson.username,
        //     url: "file://" + ubicacion,
       
        //   });
      })
    }

    async function Compartir(){

        console.log("compartiendo en android");
        setLoaderShare(true)

      setTextLoader('Download video...');
      setTimeout(()=>{
        setTextLoader('Wait please...');
      }, 5000);

      
          RNFS.downloadFile({
            fromUrl:skill.file_video,          // URL to download file from
            toFile: `${RNFS.DocumentDirectoryPath}/video.mp4`,           // Local filesystem path to save the file to
            cacheable: true,      // Whether the download can be stored in the shared NSURLCache (iOS only, defaults to true)
            
        }).promise.then((r) => {
            console.log(r);
            setLoaderShare(false)
            setTextLoader('Loading...');
            
            if(skill.video_status == 0){
                var datos = {
                    title: I18n.t('ar.shareSocial.text1') +' '+ user.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+user.username+'/'+skill.url_public,
                    message: I18n.t('ar.shareSocial.text1') +' '+ user.name +' '+ I18n.t('ar.shareSocial.text2') + 'https://go.vfa.app/'+user.username+'/'+skill.url_public,
                }
            }
            else {
                 var datos = {
                title: '',
                message: ''
            }
            }
           
            
            Share.open({
              title: datos.title,
              message: datos.message,
              url: "file://" + `${RNFS.DocumentDirectoryPath}/video.mp4`,
         
            });
          
        });
      

    }



    console.log(skill);


    return (




        <View style={styles.container}>

            

            {loaderShare?<Loader title={textLoader}/>:null} 

            {loader ? <Loader title={skill.file_video ?'Loading...':null} /> : null}

            <Image source={require('../../../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'),
                position: "absolute",
            }} />
            <View style={{ flex: 1,}}>
               
                {/* <TouchableOpacity activeOpacity={0.9} onPress={()=> navigation.navigate('3D', { skill, token, user })} style={{ height: hp('10%'), width:wp('100%'), position:'absolute', elevation:2, marginTop:20, justifyContent:'center', alignItems:'center', zIndex: 1000, flexDirection: 'row', paddingHorizontal:20 }}>
                    <View style={{backgroundColor: 'white', height:'100%', width:'90%', borderRadius:10}}>
                        <View style={{flexDirection: 'row',}}>
                            <View style={{ width: wp('20%'), height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('3D', { skill, token, user })} style={{ width: wp('20%'), height: '90%', justifyContent:'center', alignItems:'center', elevation:3 }}>
                                    <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: hp('7%'), height: hp('7%'), borderRadius: hp('7%') }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{position:'absolute', right:5, top:5, backgroundColor: '#11A10F', padding:5, borderRadius:5}}>
                                <Text style={{color:'#FFF', fontSize:hp('1.5%')}}>Level {skill.level_id}</Text>
                            </View>
                            <View style={{ padding: 10, paddingTop:20 }}>
                                <Text style={{ fontSize: hp('1.7%') }}>{skill.name}</Text>
                                <Text numberOfLines={1} style={{ fontSize: hp('1.4%') }}>{skill.method_name}</Text>
                                {skill.points ?
                                    <View style={{flexDirection:'row', alignItems:'center'}}>
                                        <View style={{width:hp('2.5%'), height:hp('2.5%'), backgroundColor: '#D8D8D8', borderRadius:hp('2%'), justifyContent:'center', alignItems:'center'}}>
                                            <Icon2 name="flash" size={hp('1.7%')} color="#535353" />
                                        </View>
                                        <Text style={{paddingLeft:5, opacity:0.7}}>+ {skill.points} pts</Text>
                                    </View>
                                : null}
                            </View>
                        </View>
                    </View>
                </TouchableOpacity> */}

                 {/* SKILL TINEE VIDEO MUESTRA LA VISTA DEL VIDEO COMPLEA  */}
                {skill.file_video ?
                    <View style={{ width: '100%', height: '90%'}}>
                        <View style={{flex:1, alignItems:'center', backgroundColor:'#000'}}>
                            <Image source={{ uri: urlImageVideo }} style={{ 
                                height: '100%',
                                width: wp('70%'),
                                position: "absolute",
                             }} />
                            <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate('PreviaVideo', { skill, urlVideo })} style={{ position: 'absolute', width: hp('20%'), height: hp('20%'), justifyContent: 'center', alignItems: 'center',top:hp('10%') }}>
                                <Icon name="play" size={hp('6')} color="#FFF" style={{ marginLeft: 10 }} />
                            </TouchableOpacity>
{/* 
                            <TouchableOpacity
                                style={{position:'absolute', bottom:hp('6%'), borderRadius:5, backgroundColor:'#11A10F', padding:7, width:wp('70%'), textAlign:'center', alignItems:'center', height:hp('5%'),  shadowColor: "black", elevation:3}}
                                onPress={() => Compartir()}
                            >
                                <Text style={{fontWeight:'bold', fontSize:wp('5%'),  textTransform:'uppercase', color:'white'}}>Share video</Text>
                            </TouchableOpacity> */}
                        </View>

                        


                        <View style={{flexDirection:'row', width:wp('100%'), paddingRight:20, marginTop:10, marginBottom:10, paddingLeft:15, height:hp('4%'), alignItems:'center'}}>
                          
                         {skill.video_status == 0?
                            <View style={{flexDirection:'row',  alignItems:'center'}}>
                            <View style={{width:hp('4%'), height:hp('4%'), marginRight:5, borderRadius:hp('4%'), justifyContent:'center', alignItems:'center', backgroundColor: "#D8D8D8",}}> 
                                <Icon2 name="thumb-up" color="#3d3d3d" size={hp('2.3%')}/>
                            </View>
                            <Text style={{color:'#3d3d3d', fontSize:hp('2%'), fontWeight:'bold'}}>{skill.votos}</Text>
                            </View>
                            :
                            <View style={{flexDirection:'row',  alignItems:'center'}}>
                            <View style={{width:hp('4%'), height:hp('4%'), marginRight:5, borderRadius:hp('4%'), justifyContent:'center', alignItems:'center', backgroundColor: "#D8D8D8",}}> 
                                <Icon2 name="trophy" color="#eab900" size={hp('2.3%')}/>
                            </View>
                            <Text style={{color:'#3d3d3d', fontSize:hp('2%'), fontWeight:'bold'}}>{skill.qualification}</Text>
                            </View>

                            }

                        </View>




                        <View style={{flexDirection:'row', paddingTop:10, paddingHorizontal:15}}>

                            <View style={{ padding:4,  borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'gray',}}>
                                <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Level  {skill.level_id}</Text>
                            </View>


                            {/* VALIDAMOS EL ESTADO DEL VIDEO SOLO   */}
                            {skill.video_status == 0?
                            skill.public_status == 1?
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'green',}}>
                                <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Approved</Text>
                            </View>
                            :skill.public_status == 2?
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'red',}}>
                            <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Reproved</Text>
                            </View>
                            :
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'orange',}}>
                            <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Pending</Text>
                            </View>
                            :
                            skill.qualification >= 7?
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'green',}}>
                                <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Approved</Text>
                            </View>
                            :skill.qualification > 0 && skill.qualification <= 6?
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'red',}}>
                            <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Reproved {skill.public_status}</Text>
                            </View>
                            :
                            <View style={{marginLeft:10, padding:4, borderRadius:5, justifyContent:'center', alignItems:'center', backgroundColor: 'orange',}}>
                            <Text style={{fontSize:hp('1.7%'), color:'#FFF'}}>Pending</Text>
                            </View>

                            } 

                            {/* FIN DE LA VALIDACION DEL ESTADO DEL VIDEO  */}



                            </View>

                         


                            <TouchableOpacity onPress={()=> navigation.navigate('3D', { skill, token,lenguaje, user })} style={{flexDirection:'row', paddingTop:10, paddingHorizontal:15, marginBottom:20, width:wp('80%')}}>

                            <View >
                            {/* <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('3D', { skill, token, user })} style={{ width: wp('20%'),  justifyContent:'center', alignItems:'center', paddingTop:10 }}> */}
                                    <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: hp('6%'), height: hp('6%'), borderRadius: hp('7%') }} />
                            {/* </TouchableOpacity> */}
                            </View>  

                            <View style={{marginLeft:10, justifyContent:'center'}}>
                                <Text style={{fontSize:hp('2.5%'), paddingTop:5, opacity:0.8, fontWeight:'bold', color:'#333'}}>{skill.name}</Text>
                                <Text style={{fontSize:hp('1.7%'), paddingTop:5, opacity:0.6}}>{skill.method_name}</Text>
                            </View>
                            </TouchableOpacity>




                            <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center', marginTop:30, marginBottom:30}}>

                            {skill.video_status == 0?
                            <TouchableOpacity  activeOpacity={0.9}  onPress={() => {
                                 Platform.OS == 'android'?
                                  Compartir()
                                  : 
                                  CompartirIos()
                                }}  style={styles.buttonShare}>
                                        <Text style={{fontWeight:'bold', fontSize:wp('4%'),  textTransform:'uppercase', color:'white'}}>Share video 
                                        </Text>
                                        <Icon2 name="send" style={{marginLeft:10, fontSize:wp('4%'), color:'white'}}></Icon2>
                            </TouchableOpacity>  
                            : null}



                            <TouchableOpacity  activeOpacity={0.9} onPress={()=>navigation.navigate('3D', { skill, token,lenguaje, user })} style={styles.buttonRepet}>
                                        <Text style={{fontWeight:'bold', fontSize:wp('4%'),  textTransform:'uppercase', color:'#3d3d3d'}}>Record again 
                                        </Text>
                                        <Icon2 name="backup-restore" style={{marginLeft:10, fontSize:wp('4%'), color:'#3d3d3d'}}></Icon2>
                            </TouchableOpacity> 

                            </View> 


                            {
                             skill.video_status == 0?
                            
                            !skill.public_status?
                            skill.votospositivos < 3?
                            <View style={{justifyContent:'center', alignItems:'center'}}>
                                <Text style={{fontWeight:'bold', fontSize:wp('5%'), color:'#3d3d3d'}}>
                                {I18n.t('previaNotifications.text1')}  { 3 - skill.votospositivos}  {I18n.t('previaNotifications.text2')}
                                </Text>
                            </View>
                            :null
                           :null
                           :null
                        }


{/* 
                        {skill.comment ?
                            <View style={{ bottom: 50, left: 0, position: "absolute", width: wp('100%'), }}>
                                <View style={{ marginLeft: 25, zIndex: 10, flexDirection: 'row' }}>
                                    {coach.photo ?
                                        <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: hp('6%'), height: hp('6%'), borderRadius: 50, }} />
                                        :
                                        <View style={{ width: hp('6%'), height: hp('6%'), borderRadius: hp('6%'), zIndex: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#D6D6D6', }}>
                                            <Text style={{ fontSize: hp('1.7%'), fontWeight: 'bold' }}>{((coach.name || "").charAt(0) || "").toUpperCase()}</Text>
                                        </View>
                                    }
                                    <View style={{ backgroundColor: '#FFF', height: 25, marginTop: 15, borderBottomRightRadius: 10, borderTopRightRadius: 10, marginLeft: -10, paddingLeft: 20, paddingRight: 10, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontSize: hp('1.5%') }}>{coach.name}</Text>
                                    </View>
                                </View>
                                <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', }}>
                                    <View style={{ paddingLeft: 10, width: wp('75%'), backgroundColor: '#FFF', padding: 10, marginTop: -10, borderTopEndRadius: 10, borderBottomRightRadius: 10, borderBottomLeftRadius: 10 }}>
                                        <Text style={{ fontSize: hp('1.7%') }}>{skill.comment}</Text>
                                    </View>
                                </View>
                            </View>
                            : null} */}
                    </View>
                    :  //// NO TIENE VIDEO MUESTRA LA IMAGEN GRANDE CON EL BOTON PLAY 
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: '100%', height: '100%', }} />
                        <TouchableOpacity activeOpacity={0.8} onPress={() => navigation.navigate('3D', { skill, token,lenguaje, user })} style={{ position: 'absolute', width: hp('20%'), height: hp('20%'), backgroundColor: 'rgba(0,0,0,0.3)', borderRadius: hp('20%'), borderColor: '#D6D6D6', borderWidth: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name="play" size={hp('6')} color="#FFF" style={{ marginLeft: 10 }} />
                        </TouchableOpacity>
                    </View>
                }
            </View>
                <Modal
                    visible={congratulationCoach}
                    // visible={true}
                >
                    <View style={{flex:1, alignItems:'center'}}>


                     {skill.qualification >= 7?
                        <View style={{justifyContent:'center', alignItems:'center', paddingTop:hp('8%')}}>
                            <Text style={{fontSize:hp('3%'), fontWeight:'bold', color:'#11A10F', textAlign:'center'}}>¡CONGRATULATIONS!</Text>
                            <Text style={{fontSize:hp('3%'), fontWeight:'bold', color:'#11A10F', textAlign:'center'}}>Skill performed</Text>
                        </View>
                         :
                         <View style={{justifyContent:'center', alignItems:'center', paddingTop:hp('8%')}}>
                         <Text style={{fontSize:hp('3%'), fontWeight:'bold', color:'red', textAlign:'center'}}>Try Again</Text>
                         <Text style={{fontSize:hp('3%'), fontWeight:'bold', color:'red', textAlign:'center'}}>You need to improve your performance!</Text>
                        </View>
                         }


                        <View style={{ height: hp('10%'), width:wp('100%'), marginTop:hp('5%'), justifyContent:'center', alignItems:'center', zIndex: 1000, flexDirection: 'row', paddingHorizontal:20 }}>
                            <View style={{backgroundColor: 'white', elevation:3, height:'100%', width:'90%', borderRadius:10}}>
                                <View style={{flexDirection: 'row',}}>
                                    <View style={{ width: wp('20%'), height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                        <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('3D', { skill, token,lenguaje, user })} style={{ width: wp('20%'), height: '90%', justifyContent:'center', alignItems:'center', elevation:3 }}>
                                            <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: hp('7%'), height: hp('7%'), borderRadius: hp('7%') }} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{position:'absolute', right:5, top:5, backgroundColor: '#11A10F', padding:5, borderRadius:10}}>
                                        <Text style={{color:'#FFF', fontSize:hp('1.5%')}}>{method}</Text>
                                    </View>
                                    <View style={{ padding: 10, paddingTop:20 }}>
                                        <Text style={{ fontSize: hp('1.7%') }}>{skill.name}</Text>
                                        <Text numberOfLines={1} style={{ fontSize: hp('1.4%') }}>{skill.method_name}</Text>

                                        {!skill.skill_user_id?
                                        skill.points ?
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <View style={{width:hp('2.5%'), height:hp('2.5%'), backgroundColor: '#D8D8D8', borderRadius:hp('2%'), justifyContent:'center', alignItems:'center'}}>
                                                    <Icon2 name="flash" size={hp('1.7%')} color="#535353" />
                                                </View>
                                                <Text style={{paddingLeft:5, opacity:0.7}}>+ {skill.points} pts</Text>
                                            </View>
                                        : null
                                        :null
                                    }


                                    </View>
                                </View>
                            </View>
                        </View>
                        <View>
                            {skill.qualification >= 6? 
                             <Image source={require('../../../assets/home/skill/image1.png')} resizeMode="contain" style={{width:hp('40%'), height:hp('40%')}}/>
                             :<Image source={require('../../../assets/reprobado.png')} resizeMode="contain" style={{width:hp('40%'), height:hp('37%'), marginTop:15, marginBottom:10}}/>
                            }
                           
                        </View>
                        <View style={{ width:'90%', height:hp('10%'), borderWidth:4, borderColor:'#DDDDDD', borderRadius:10, justifyContent:'center', alignItems:'center', paddingHorizontal:20}}>
                            {/* <View style={{width:'30%', position:'absolute', right:15, top:-20, backgroundColor: '#FFF', borderWidth:4, borderRadius:10, borderColor:'#DDDDDD', padding:5, justifyContent:'center', alignItems:'center'}}>
                             { <Text>EXCELENT</Text>}
                            </View> */}
                            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                                <View style={{flex:1, height:'100%', justifyContent:'center', paddingHorizontal:30}}>
                                    <Text style={{fontSize:hp('2%')}}>SCORE</Text>
                                </View>
                                 
                                 {skill.qualification >= 7?
                                <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                    <Icon2 name="signal-cellular-3" color="#11A10F" size={hp('6%')} style={{opacity:0.5}} />
                                    <Text style={{fontSize:hp('3%'), paddingLeft:10}}>{skill.qualification}</Text>
                                </View>
                                :
                                <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                <Icon2 name="signal-cellular-3" color="red" size={hp('6%')} style={{opacity:0.5}} />
                                <Text style={{fontSize:hp('3%'), paddingLeft:10}}>{skill.qualification}</Text>
                                </View>
                                }

                            </View>
                        </View>
                        <View>
                            <Text style={{fontSize:hp('2%')}}>
                                 {skill.comment}
                                </Text>
                        </View>
                        <View style={{marginTop:hp('5%')}}>
                            <TouchableOpacity activeOpacity={0.9} onPress={() => {
                                cambiarStado()
                            }} style={styles.Button}>
                                    <Text style={{fontWeight:'bold', color:'#FFF', fontSize:hp('1.7%')}}>
                                        CONTINUE
                                    </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                <Modal
                visible={congratulationSinCoach}
                >
                    <View style={{flex:1, alignItems:'center'}}>
                        <View style={{justifyContent:'center', alignItems:'center', paddingTop:hp('8%')}}>
                            <Text style={{fontSize:hp('3%'), fontWeight:'bold', color:'#11A10F', textAlign:'center'}}>¡CONGRATULATIONS!</Text>
                            <Text style={{fontSize:hp('3%'), fontWeight:'bold', color:'#11A10F', textAlign:'center'}}>Skill performed</Text>
                        </View>
                        <View style={{ height: hp('10%'), width:wp('100%'), marginTop:hp('5%'), justifyContent:'center', alignItems:'center', zIndex: 1000, flexDirection: 'row', paddingHorizontal:20 }}>
                            <View style={{backgroundColor: 'white', elevation:3, borderColor:'#d6d6d6', borderWidth:1, height:'100%', width:'90%', borderRadius:10}}>
                                <View style={{flexDirection: 'row',}}>
                                    <View style={{ width: wp('20%'), height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                        <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('3D', { skill, token,lenguaje, user })} style={{ width: wp('20%'), height: '90%', justifyContent:'center', alignItems:'center', elevation:3 }}>
                                            <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: hp('7%'), height: hp('7%'), borderRadius: hp('7%') }} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{position:'absolute', right:5, top:5, backgroundColor: '#11A10F', padding:5, borderRadius:10}}>
                                        <Text style={{color:'#FFF', fontSize:hp('1.5%')}}>Level {skill.level_id}</Text>
                                    </View>
                                    <View style={{ padding: 10, paddingTop:20 }}>
                                        <Text style={{ fontSize: hp('1.7%') }}>{skill.name}</Text>
                                        <Text numberOfLines={1} style={{ fontSize: hp('1.4%') }}>{skill.method_name}</Text>
                                        {skill.points ?
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <View style={{width:hp('3%'), height:hp('3%'), backgroundColor: '#D8D8D8', borderRadius:hp('2%'), justifyContent:'center', alignItems:'center'}}>
                                                    <Icon2 name="flash" size={hp('2%')} color="#FFB33C" />
                                                </View>
                                                <Text style={{paddingLeft:5, fontSize:hp('1.7%'), opacity:0.7}}>+ {skill.points} pts</Text>
                                            </View>
                                        : null}
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View>
                            <Image source={require('../../../assets/home/skill/image1.png')} resizeMode="contain" style={{width:hp('40%'), height:hp('40%')}}/>
                        </View>
                        <View style={{width:'90%', height:hp('10%'), justifyContent:'center', alignItems:'center', paddingHorizontal:20}}>
                            <Image source={require('../../../assets/home/skill/estrellas.png')} resizeMode="contain" style={{width:hp('30%'), height:hp('30%')}}/>
                        </View>
                        <View>
                            <Text style={{fontSize:hp('2%')}}>Excellent, go to the next skill</Text>
                        </View>
                        <View style={{marginTop:hp('2%')}}>
                            <TouchableOpacity activeOpacity={0.9} onPress={() => {
                                cambiarStado()
                                // setCongratulationSinCoach(false)
                            }} style={styles.Button}>
                                    <Text style={{fontWeight:'bold', color:'#FFF', fontSize:hp('1.7%')}}>
                                        CONTINUE
                                    </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                {/* <Modal
                visible={congratulationSinCoachRepro}
                >
                    <View style={{flex:1, alignItems:'center'}}>
                        <View style={{justifyContent:'center', alignItems:'center', paddingTop:hp('8%')}}>
                            <Text style={{fontSize:hp('3%'), fontWeight:'bold', color:'#11A10F', textAlign:'center'}}>¡CONGRATULATIONS!</Text>
                            <Text style={{fontSize:hp('3%'), fontWeight:'bold', color:'#11A10F', textAlign:'center'}}>Skill performed</Text>
                        </View>
                        <View style={{ height: hp('10%'), width:wp('100%'), marginTop:hp('5%'), justifyContent:'center', alignItems:'center', zIndex: 1000, flexDirection: 'row', paddingHorizontal:20 }}>
                            <View style={{backgroundColor: 'white', elevation:3, height:'100%', width:'90%', borderRadius:10}}>
                                <View style={{flexDirection: 'row',}}>
                                    <View style={{ width: wp('20%'), height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                        <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('3D', { skill, token, user })} style={{ width: wp('20%'), height: '90%', justifyContent:'center', alignItems:'center', elevation:3 }}>
                                            <Image source={{ uri: urlImage }} resizeMode="cover" style={{ width: hp('7%'), height: hp('7%'), borderRadius: hp('7%') }} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{position:'absolute', right:5, top:5, backgroundColor: '#11A10F', padding:5, borderRadius:10}}>
                                        <Text style={{color:'#FFF', fontSize:hp('1.5%')}}>Level {skill.level_id}</Text>
                                    </View>
                                    <View style={{ padding: 10, paddingTop:20 }}>
                                        <Text style={{ fontSize: hp('1.7%') }}>{skill.name}</Text>
                                        <Text numberOfLines={1} style={{ fontSize: hp('1.4%') }}>{skill.method_name}</Text>
                                        {skill.points ?
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <View style={{width:hp('3%'), height:hp('3%'), backgroundColor: '#D8D8D8', borderRadius:hp('2%'), justifyContent:'center', alignItems:'center'}}>
                                                    <Icon2 name="flash" size={hp('2%')} color="#FFB33C" />
                                                </View>
                                                <Text style={{paddingLeft:5, fontSize:hp('1.7%'), opacity:0.7}}>+ {skill.points} pts</Text>
                                            </View>
                                        : null}
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View>
                            <Image source={require('../../../assets/home/skill/image1.png')} resizeMode="contain" style={{width:hp('40%'), height:hp('40%')}}/>
                        </View>
                        <View style={{width:'90%', height:hp('10%'), justifyContent:'center', alignItems:'center', paddingHorizontal:20}}>
                            <Image source={require('../../../assets/home/skill/estrellas.png')} resizeMode="contain" style={{width:hp('30%'), height:hp('30%')}}/>
                        </View>
                        <View>
                            <Text style={{fontSize:hp('2%'), color:'red'}}>Not good enough yet</Text>
                        </View>
                        <View style={{marginTop:hp('2%')}}>
                            <TouchableOpacity activeOpacity={0.9} onPress={() => {
                                cambiarStado()
                            }} style={styles.Button}>
                                    <Text style={{fontWeight:'bold', color:'#FFF', fontSize:hp('1.7%')}}>
                                        CONTINUE
                                    </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal> */}
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    buttonRepet: {
        borderRadius:5, 
        backgroundColor:'white', 
        padding:9, 
        marginLeft:10,
        width:wp('40%'), 
        textAlign:'center', 
        alignItems:'center', 
        height:hp('5%'),  
        shadowColor: '#eee',
        shadowOffset: { width: 5, height: 4 },
        shadowOpacity: 0.7,
        flexDirection:'row',
        justifyContent:'center',
        elevation:3,
      },



    buttonShare: {
        borderRadius:5, 
        backgroundColor:'#11A10F', 
        padding:9, 
        width:wp('40%'), 
        textAlign:'center', 
        alignItems:'center', 
        height:hp('5%'),  
        shadowColor: '#eee',
        shadowOffset: { width: 5, height: 4 },
        shadowOpacity: 0.7,
        flexDirection:'row',
        justifyContent:'center',
        elevation:3,
      },


    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    Button: {
        width: width * 0.8,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#11A10F',
        backgroundColor: '#11A10F',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderRadius:10,
        elevation: 5,
        justifyContent:'center',
        alignItems:'center',
        height:hp('6%'),
    },
});

//make this component available to the app
export default Previa;
