import React, { useState, useEffect, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, Dimensions, SafeAreaView, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Video2 from 'react-native-video';
const { width, height } = Dimensions.get('screen');
import Loader from '../../coach/global/loader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'i18n-js';

const PreviaVideo = ({ navigation, route }) => {
    const { skill, urlVideo, view} = route.params;
    const [index, setIndex] = useState(0);
    const [loader, setLoader] = useState(false);

    useEffect(() => {
        setLoader(true)
    }, [])

    useLayoutEffect(() => {
        navigation.setOptions({
            // title: view === 'notification'? skill.skill_name : skill.name,
            title: skill.name,
        })
    }, [])

console.log(route.params);
    console.log(skill, urlVideo);

    return (
        <View style={{flex:1,}}>
            {loader ? <Loader title={skill.file_video ?'Loading...':null} /> : null}
                <View style={{flex:1}}>
                    <Video2 source={{ uri: urlVideo }}
                        controls={true}
                        resizeMode="cover"
                        onLoad={() => setLoader(false)}
                        style={styles.backgroundVideo}
                    />
                </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0 ,
        right: 0,
    },
});

//make this component available to the app
export default PreviaVideo;
