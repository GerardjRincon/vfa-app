//import liraries
import React, { useState, useEffect, useLayoutEffect } from 'react';
import {Modal, View, Text, StyleSheet,Button, TouchableOpacity, Image, Dimensions, TextInput,  KeyboardAvoidingView,
    Keyboard,
    TouchableWithoutFeedback, Platform, ScrollView, Linking } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
// import DatePicker from 'react-native-datepicker'

import DatePicker from 'react-native-date-picker'


import AsyncStorage from '@react-native-async-storage/async-storage';
import md5 from 'md5';
import Snackbar from 'react-native-snackbar';
import Loader from './coach/global/loader';
import { Block, Checkbox, Input, Button as GaButton, theme } from 'galio-framework'
const { width, height } = Dimensions.get('screen');
import { setUser, setToken, setModalprimary, setTeam, setCoach } from "../storage/user/dataUser";
import { RadioButton, Text as Text2 } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import I18n from 'react-native-i18n';
import moment from "moment";

import CuponModal from './modalCupon';


// create a component
const RegisterPlayer = ({navigation, route}) => {

    const [namePlayer, setNamePlayer] = useState('');
    const [emailPlayer, setEmailPlayer] = useState('');
    const [code, setCode] = useState('');
    const [errorcode, setErrorCode] = useState(false);
    const [passwordPlayer, setPasswordPlayer] = useState(null);
    const [datePlayer, setDatePlayer] = useState(null);
    const [cuponModal, setCuponModal] = useState(false);


     


    // variables error player
    const [nameErrorPlayer, setNameErrorPlayer] = useState(false);
    const [emailErrorPlayer, setEmailErrorPlayer] = useState(false);
    const [passwordErrorPlayer, setPasswordErrorPlayer] = useState(false);

    // Terminos y condiciones
    const [politicasPlayer, setPoliticasPlayer] = useState(false);
    const [politicasCoppa, setPoliticasCoppa] = useState(false);
    const [emailPadre, setEmailPadre] = useState(null);
    const [errorEmailPadre, setErrorEmailPadre] = useState(false);
    



    const [date, setDate] = useState(new Date())
    const [open, setOpen] = useState(false)
    const [campo, setCampo] = useState(null);

    
    const [loader, setLoader] = useState(false);
    const [optionCoach, setOptionCoach] = useState('1')
    const [changeOption, setchangeOption] = useState('The Virtual Coach will guide you through the program');
    const [fecha] = useState(moment(new Date()).format("YYYY"));

    // Variable token
    const [token, setTok] = useState(null); // token fcm

    const [modalCoppa, setModalCoppa] = useState(false);
    const [emailStatus, setEmailStatus] = useState(false);
    const [passwordStatus, setPasswordStatus] = useState(false);
    const [passValid, setpassValid] = useState(false);
    const [locatePais, setLocatePais] = useState('');

    useEffect(async() => {
        const fcmToken = await AsyncStorage.getItem('@fcmToken');
        const fcmTokenJson = JSON.parse(fcmToken);
        setTok(fcmTokenJson);
        Pais();

    }, [])

    useLayoutEffect(() => {
        navigation.setOptions({
            title:I18n.t('registerPlayer.player.titlePage'),
        });
    }, [])

    function back() {
        setNamePlayer(null)
        setEmailPlayer(null)
        setDatePlayer(null)
        setPoliticasPlayer(null)
    }

    

    const SubmitPlayer = async (e) => {
            try {
                if (!namePlayer) {
                    setNameErrorPlayer(true)
                    return;
                } else {
                    setNameErrorPlayer(false)
                }
                if (!emailPlayer) {
                    setEmailErrorPlayer(true)
                    return;
                } else {
                    setEmailErrorPlayer(false)
                }
                if (!passwordPlayer) {
                    setPasswordErrorPlayer(true)
                    return;
                } else {
                    setPasswordErrorPlayer(false)
                }
                if (!datePlayer) {
                    Snackbar.show({
                        text: I18n.t('registerPlayer.alert.alertDate'),
                        duration: Snackbar.LENGTH_LONG,
                    });
                    return;
                }
                setLoader(true);


                const datos = {
                    name: namePlayer,
                    codigo:e,
                    email: emailPlayer,
                    password: md5(passwordPlayer),
                    age: datePlayer,
                    type: 'player',
                    onesignal_id: token,
                    typecoach:optionCoach,
                }


                console.log("codigo enviado", datos);
    
    
                await fetch('https://go.vfa.app/api/signup', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(datos),
                }).then(res => res.json())
                    .then(async (dat) => {
                        console.warn(dat, "wdwehdwdhkjwedwd")
                        try {
    
                            if (dat.ok) {
                                if (dat.data.team) {
                                    setTeam(dat.data.team);
                                }
                                if (dat.data.coach) {
                                    setCoach(dat.data.coach);
                                }
                                setUser(dat.data.user);
                                setToken(dat.data.token);
                                setLoader(false);
                                setModalprimary(true);
    
                                if (dat.data.user.pay == true && dat.data.user.invite == true) {
                                    navigation.replace('validationSesion', { user: dat.data.user })
                                }else{
                                    if (dat.data.user.state_cupon) {
                                        navigation.replace('Pay Discount', {user:dat.data.user, token:dat.data.token})
                                    }
                                    else if(!dat.data.user.state_cupon){
                                        navigation.replace('Pay App Player', {user:dat.data.user, token:dat.data.token})
                                    }
                                   
                                }
                                
                            } else {
                                setLoader(false);
    
                                Snackbar.show({
                                    text:I18n.t('registerPlayer.alert.validateAlertCredencial'),
                                    duration: Snackbar.LENGTH_LONG,
                                });
    
                                setModalCoppa(false);
                                setEmailStatus(true);
                            }
    
                        } catch (error) {
                            setLoader(false);
                            // props.aviso(error)
                            Snackbar.show({
                                text:I18n.t('registerPlayer.alert.messageError'),
                                duration: Snackbar.LENGTH_LONG,
                            });
    
                        }
                    })
            } catch (error) {
                // props.aviso(error)
                Snackbar.show({
                    text:I18n.t('registerPlayer.alert.messageError'),
                    duration: Snackbar.LENGTH_LONG,
                });
            }
        
       
        
    }

    async function Pais(){
        let response = await fetch("https://ipwhois.app/json/")
        .then((data) => data.json())
        .then((res) => {
            setLocatePais(res.country_code);
        })
    }

   
    


    function coppaModal(){


        // Validacion de la edad 
        const edad = moment(datePlayer).format("YYYY");
        const fecha = moment().format("YYYY");
        let calculo = fecha - edad;

        let regex = /^.*(?=.{8,})(?=.*\d)(?=.*[a-záéíóúüñ]).*[A-ZÁÉÍÓÚÜÑ]/;
        if (regex.test(passwordPlayer)) {
            setPasswordStatus(false)
            //Validamos que la contraseña cumpla con el minimo de validacion
            if(calculo >= 13){  // Validamos la edad para mostrar el COPPA
               
                setCuponModal(true);
            }
            else {
              setModalCoppa(true);
            }
                                   
        } else {
            setPasswordStatus(true)
        }
        
    }

    // Estilos del boton coppa register 
    function styleCoppa(){
       if(politicasCoppa && emailPadre){
        return  {
           
            backgroundColor: '#11A10F',
            color:'#fff',
        }
       }
       else{
        return  {
            backgroundColor: '#eee',
            color:'#3d33d',
         }
       }
       
       
    }

    const EnviarCoppa = async () => {
        try {
            await Linking.openURL('https://vfa.app/children-policy');
          } catch (e) {
            console.warn(e)
          }
    }


    const policy = async () => {
        try {
          await Linking.openURL('https://www.websitepolicies.com/policies/view/0NueZaX0');
        } catch (e) {
          console.warn(e)
        }
      };

      const terminos = async () => {
        try {
          await Linking.openURL('https://www.websitepolicies.com/policies/view/Qmp6RQvE');
        } catch (e) {
          console.warn(e)
        }
      };

  const ValidarEmail = async () => {
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(emailPadre) === true){
           setErrorEmailPadre(false);
           console.log(errorEmailPadre);
           setCuponModal(true);


        }
        else{
           setErrorEmailPadre(true);
           console.log(errorEmailPadre);
        }
}



    return (


        <View style={{ flex: 1, paddingTop: 30, height: height, backgroundColor:'white' }}>

           

            
            {loader ? <Loader /> : null}

            <Image source={require('../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'), position: "absolute",
            }} />

           


           {/* Modal para levantar el cupon  */}
           {cuponModal?
           <CuponModal 
           modalCloce={() => { setCuponModal(false)}}
           enviarFormulario={(e)=> {SubmitPlayer(e)}}
           visibleModal={cuponModal} />
           :null}




           {/* MODAL COPPA  */}
    <Modal
        animationType="slide"
        transparent={true}
        // visible={true}
       
        visible={modalCoppa}
        // onRequestClose={() => {
        //   Alert.alert("Modal has been closed.");
        //   setModalCoppa(!modalCoppa);
        // }}
      >




        <View style={styles.centeredView }>

          <View style={[styles.modalView]}>

          <Icon name="times" 
          onPress={() => {setModalCoppa(false)}}
          style={{fontSize:wp('5%'),color:'#3d3d3d', position:'absolute', right:20, top:15}}></Icon>

          <ScrollView showsVerticalScrollIndicator={true} persistentScrollbar={true} style={{height:hp('45.5%'), marginTop:20}}>
            <Text style={[styles.modalTextTiTle]}>
                { I18n.t('registerPlayer.childrenPolicy.title')}
                </Text>
                <Text style={[styles.subTitle, {marginBottom:10}]}>
                 { I18n.t('registerPlayer.childrenPolicy.text1')}
                </Text>
                 <Text style={styles.areaText}>
                 { I18n.t('registerPlayer.childrenPolicy.text2')}
                 </Text>

                 {/* <Text style={[styles.areaText, styles.margin5]}>
                 { I18n.t('registerPlayer.childrenPolicy.text3')}
                 </Text> */}

                <Text style={[styles.subTitle, styles.margin10]}>
                { I18n.t('registerPlayer.childrenPolicy.text4')}
                </Text>

                <Text style={[styles.areaText, styles.margin5]}>
                { I18n.t('registerPlayer.childrenPolicy.text5')}
                <Text style={[styles.areaText, styles.margin5]}>
                    </Text>
                    { I18n.t('registerPlayer.childrenPolicy.text6')}
                 </Text>

                 <Text style={{color:'purple', marginTop:10,fontSize:wp('4%'), fontWeight:'bold'}} 
                 onPress={() => { EnviarCoppa()}}>
                   { I18n.t('registerPlayer.childrenPolicy.text7')}
                 </Text>
              </ScrollView>
           

                <View style={{marginTop:20}}>
                          <Input
                            left
                            icon="envelope"
                            family="font-awesome"
                            style={styles.inputs}
                            iconSize={hp('1.4%')}
                            placeholder={'Parent email'}
                            bottomHelp
                            autoCapitalize='none'
                           
                            onChangeText={(e) => setEmailPadre(e)}
                            placeholderTextColor="#818181"

                        />
                        {errorEmailPadre?
                        <Text style={{color:'red', fontWeight:'bold'}}>
                         {I18n.t('registerPlayer.childrenPolicy.emailPadre')}
                        </Text>
                        :null}
                        
                </View>

                <Block
                            style={{ marginVertical: theme.SIZES.BASE, flexDirection: 'row', marginTop:10}}
                            row
                            width={width * 0.75}
                        >
                            <Checkbox
                                checkboxStyle={{
                                    borderWidth: 1,
                                    borderRadius: 2,
                                    borderColor: '#818181',
                                    backgroundColor: '#818181',
                                }}
                                color='#818181'
                                labelStyle={{
                                    color: '#818181',
                                }}
                                value={politicasCoppa}
                                onChange={(val) => setPoliticasCoppa(val)}
                                label={null}
                            />
                            <View style={{flexDirection: 'row', marginLeft:10}}>
                                <Text>
                                {I18n.t('registerPlayer.childrenPolicy.textTerminos1')} <Text>{I18n.t('registerPlayer.childrenPolicy.textTerminos2')}</Text>
                                </Text>
                                
                            </View>
                        </Block>


              <View style={{justifyContent:'center', alignItems:'center'}}>
              <GaButton style={[styles.createButtonCoppa, styleCoppa() ]} color="success" onPress={() => {
                                    if (politicasCoppa && emailPadre) {
                                        // SubmitPlayer()
                                        ValidarEmail();
                                    } else {
                                        console.log('Please accept children privacy to continue.');
                                        Snackbar.show({
                                            text:I18n.t('registerPlayer.childrenPolicy.AcceptTerms'),
                                            duration: Snackbar.LENGTH_LONG,
                                        });
                                    }
                                }}>
                            <Text style={{fontWeight:'bold', textTransform:'uppercase', color:politicasCoppa?'#FFF':'#b4b4b4'}}>
                            {I18n.t('registerPlayer.player.submit')}
                            {/* CONTINUE */}
                            
                            </Text>
                                   
                </GaButton>
                </View>


          </View>
        </View>
      </Modal>

      {/* FIN MODAL COPPA */}



      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "padding"}
        style={{flex:1}}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

            <View style={{ flex: 7, }}>

            <View style={{ flex: 2,alignItems: 'center' }}>
                <Image source={require('../assets/Logo.png')} style={styles.logo} />
                {/* <Text style={{ fontSize: hp('2%'), fontWeight: 'bold' }}>{ I18n.t('registerPlayer.player.title')}</Text> */}
                <Text style={{ fontSize: hp('1.5%'), opacity: 0.8 }}>{ I18n.t('registerPlayer.player.descripcion')}</Text>
            </View>

               
                    <View style={styles.contentInputs}>
                        
                        <Input
                            left
                            icon="user"
                            family="font-awesome"
                            style={styles.inputs}
                            iconSize={hp('1.4%')}
                            placeholder={I18n.t('registerPlayer.player.name')}
                            value={namePlayer}
                            help={nameErrorPlayer ? I18n.t('registerPlayer.player.helpName') : null}
                            bottomHelp
                            onChangeText={(e) => setNamePlayer(e)}
                            placeholderTextColor="#818181"

                        />
                        <Input
                            left
                            icon="envelope"
                            family="font-awesome"
                            style={styles.inputs}
                            iconSize={hp('1.4%')}
                            placeholder={I18n.t('registerPlayer.player.email')}
                            help={emailErrorPlayer ? I18n.t('registerPlayer.player.helpEmail') : null}
                            bottomHelp
                            autoCapitalize='none'
                            onChangeText={(e) => setEmailPlayer(e)}
                            placeholderTextColor="#818181"

                        />
                        {
                            emailStatus?
                            <Text style={{color:'red', fontWeight:'bold'}}>{I18n.t('registerPlayer.player.validateInputEmail')}</Text>:null
                        }
                       


                        <Input
                            left
                            icon="key"
                            family="font-awesome"
                            style={styles.inputs}
                            iconSize={hp('1.4%')}
                            placeholder={I18n.t('registerPlayer.player.password')}
                            help={passwordErrorPlayer ? I18n.t('registerPlayer.player.helpPass') : null}
                            bottomHelp
                            password
                            viewPass
                            onChangeText={(e) => setPasswordPlayer(e)}
                            placeholderTextColor="#818181"
                           
                        />

                            {
                             passwordStatus?
                            <Text style={{color:'red', fontWeight:'bold'}}>{I18n.t('registerPlayer.player.errorPasswordValidate')}</Text>:null
                              }
                       



                        <View style={{ width: '100%', marginTop: 10 }}>
                              
                        <Icon name="calendar" size={hp('1.4%')} color="#818181" style={{ position: 'absolute', zIndex: 1, top: 29, elevation:3, left: 17, shadowColor: '#333', shadowOffset: { width: 0, height: 1 },shadowOpacity: 0.7, }} />
                                    <DatePicker
                                        modal
                                        mode="date"
                                        open={open}
                                        maximumDate={new Date()}
                                        date={date}
                                        onConfirm={(date) => {
                                        setOpen(false)
                                        setDate(date)

                                        const edad = moment(date).format("YYYY-MM-DD");
                                        setDatePlayer(edad);
                                    
                                        }}
                                        onCancel={() => {
                                        setOpen(false)
                                        }}
                                    />



                                    <Input
                                        onPressIn={() =>{setOpen(true)}}
                                        style={[styles.inputs, {paddingLeft:40}]}
                                        placeholder={I18n.t('registerPlayer.player.date')}
                                        bottomHelp
                                        value={datePlayer}
                                        onChangeText={(e) => setDatePlayer(e)}
                                        placeholderTextColor="#818181"
                                    
                                    />


                                  
                        </View>
                        <Block
                            style={{ marginVertical: theme.SIZES.BASE, marginLeft: 15 , flexDirection: 'row',}}
                            row
                            width={width * 0.75}
                        >
                            <Checkbox
                                checkboxStyle={{
                                    borderWidth: 1,
                                    borderRadius: 2,
                                    borderColor: '#818181',
                                    backgroundColor: '#818181',
                                }}
                                color='#818181'
                                labelStyle={{
                                    color: '#818181',
                                }}
                                value={politicasPlayer}
                                onChange={(val) => {setPoliticasPlayer(val)}}
                                label={null}
                            />
                            <View style={{flexDirection: 'row', marginLeft:10}}>
                                <Text style={{fontSize:13}}>
                                    {I18n.t('registerPlayer.player.text1')}
                                </Text>
                                <TouchableOpacity onPress={()=>policy()} style={{marginLeft:5,}}>
                                    <Text style={{color:'green',fontSize:13}}>
                                        {I18n.t('registerPlayer.player.text2')}
                                    </Text>
                                </TouchableOpacity>
                                <Text style={{marginLeft:5, marginRight:5, fontSize:13}}>
                                    {I18n.t('registerPlayer.player.text3')}
                                </Text>
                                <TouchableOpacity onPress={()=>terminos()}>
                                    <Text style={{color:'green', fontSize:13}}>
                                       {I18n.t('registerPlayer.player.text4')}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </Block>
                        <Block center style={{ marginTop: 0 }}>
                            <Block center>
                                <GaButton style={styles.createButton} size="small" color="success" onPress={() => {
                                    
                                    if(emailPlayer 
                                       && passwordPlayer
                                       && datePlayer
                                       && namePlayer){
                                        if (politicasPlayer) {
                                            coppaModal();
     
                                         } else {
                                             Snackbar.show({
                                                 text: I18n.t('registerPlayer.alert.alertSubmit'),
                                                 duration: Snackbar.LENGTH_LONG,
                                             });
                                         }

                                    }else {

                                        Snackbar.show({
                                            text: 'Fill in the fields',
                                            duration: Snackbar.LENGTH_LONG,
                                        });

                                    }

                                    
                                }}>
                                    <Text style={{fontWeight:'bold', color:'white', textTransform:'uppercase'}}>
                                    {I18n.t('registerPlayer.player.submit')}
                                    </Text>
                                   
                                    </GaButton>
                            </Block>
                        </Block>
                    </View>

        
            </View>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
        </View>
       
    );
};

// define your styles
const styles = StyleSheet.create({

    splah_happy:{
        width:wp('40%'),
        height:hp('25%'),
      },
    
      centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0,
        backgroundColor:'#0000002e',
      },
      modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        width:wp('85%'),
        padding: 35,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
      },
      button: {
        borderRadius: 5,
        padding: 10,
        elevation: 2,
        width:wp('50%'),
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        backgroundColor: "#00c900",
        textTransform:'uppercase',
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textTransform:'uppercase',
      },
      modalText: {
        marginBottom: 5,
      },
    
    
      modalTextTiTle: {
        marginBottom: 5,
        fontSize:wp('5%'),
        fontWeight:'normal',
        color:'#333',
        fontWeight:'bold',
      },
    
      subTitle:{
        fontWeight:'bold',
        fontSize:wp('4%'),
        color:'#404040',
        marginTop:10,
        
    
    
      },
      areaText:{
        fontWeight:'normal',
        fontSize:wp('4%'),
        color:'#3d3d3d'
    
      },

      margin5:{
         marginTop:5,
      },
      margin10:{
        marginTop:10,
     },
    
      textPuntos:{
        backgroundColor:'#eee',
        padding:2,
        paddingLeft:10, 
        paddingRight:10,
        color:'#404040',
        borderRadius:5,
        marginTop:10,
        fontSize:wp('4%'),
        
      },
    

      

    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
    logo: {
        height: hp('7%'),
        width: wp('30%'),
    },
    inputs: {
        borderWidth: 1,
        height: 55,
        borderColor: '#E3E3E3',
        borderRadius: 10,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.7,
        elevation: 3,
    },
    contentInputs: {
        paddingHorizontal: 20,
        marginTop:hp('-10%'),
        flex:7
       
    },
    createButton: {
        width: wp('90%'),
        marginTop: 5,
        marginBottom: 40,
        shadowColor: '#11A10F',
        backgroundColor: '#11A10F',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderRadius:10,
        elevation: 5,
        justifyContent:'center',
        alignItems:'center',
        height:hp('6%'),
    },


    createButtonCoppa: {
        width: wp('68%'),
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#11A10F',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderRadius:10,
        elevation: 5,
        justifyContent:'center',
        alignItems:'center',
        height:hp('6%'),
    },


});


//make this component available to the app
export default RegisterPlayer;
