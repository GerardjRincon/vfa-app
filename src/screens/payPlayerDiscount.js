//import liraries
import React, { useState, useEffect, useLayoutEffect, useRef } from 'react'
import {
    StyleSheet,
    Dimensions,
    Image,
    View,
    StatusBar,
    SafeAreaView,
    ActivityIndicator,
    KeyboardAvoidingView,
    ScrollView,
    Platform, 
    FlatList,
    Text,
    DeviceEventEmitter,
    TouchableOpacity
} from 'react-native';
import Swiper from 'react-native-swiper';
import Icon  from 'react-native-vector-icons/MaterialCommunityIcons';
import I18n from 'react-native-i18n';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Block, Checkbox, Input,Radio, Text as T, Button as GaButton, theme } from 'galio-framework'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { setUser, setModalprimary } from '../storage/user/dataUser';
import Loader from './coach/global/loader';
import { url } from '../storage/config';

// import del api de pagos  
import RNIap, {
    InAppPurchase,
    PurchaseError,
    SubscriptionPurchase,
    acknowledgePurchaseAndroid,
    consumePurchaseAndroid,
    finishTransaction,
    finishTransactionIOS,
    purchaseErrorListener,
    purchaseUpdatedListener,
  } from 'react-native-iap';

// constantes de vista
const { width, height } = Dimensions.get('screen');



    
// create a component
const PayAppPlayerDiscount = ({navigation,route}) => {
    // const { user, token } = route.params;
    const [isAnual, setIsAnual] = useState(true); // validar seleccion


    

    const [loader, setLoader] = useState(false);
    const [purchased, setPurchased] = useState(false);
    const [products, setProducts] = useState(null);
    const [purchaseUpdateSubscription, setPurchaseUpdateSubscription] = useState(null)
    const [purchaseErrorSubscription , setPurchaseErrorSubscription] = useState({})

    const [selectPlanIndex, setSelectPlanIndex] = useState(3);

    const [planSelect, setPlanSelect] = useState('vfa_player_annual')

   
    const [user, setU] = useState(null);
    const [token, setToken] = useState(null);
    const [userdato, setUserdato] = useState(null);
    // const [isAnual, setIsAnual] = useState(true); // validar seleccion
   
     
        // Objetos de los planes segun la plataforma 
        const items = Platform.select({
             ios:['vfa_player_annual_10dst', 'vfa_player_biannual_10dst', 'vfa_player_quarterly_10dst', 'vfa_player_monthly_10dst'],
             android:['vfa_player_annual_10dst', 'vfa_player_biannual_10dst', 'vfa_player_quarterly_10dst', 'vfa_player_monthly_10dst']
        });
        

    useEffect(async() => {

        console.log("vista de los descuentos ")
   
        const user = await AsyncStorage.getItem('@user');
        const token = await AsyncStorage.getItem('@token');
        const userJson = JSON.parse(user);
        const tokenJson = JSON.parse(token);
        setU(userJson)
        setToken(tokenJson)
        getUser(tokenJson)


      // const items = Platform.select({
      //   ios:['vfa_player_annual_10dst', 'vfa_player_biannual_10dst', 'vfa_player_quarterly_10dst', 'vfa_player_monthly_10dst'],
      //   android:['vfa_player_annual_10dst', 'vfa_player_biannual_10dst', 'vfa_player_quarterly_10dst', 'vfa_player_monthly_10dst']
      // });
     



       


        // Llamamos a la funcion de list planes 
        // if(Platform.OS == 'ios'){ 

                // las variables de iniciop de iap debemn setearse para que no consuman rendimiento.
   
                await RNIap.initConnection(); // iniciando la aplicacion de pagos 

                if (Platform.OS === 'android') {
                 let carga =  await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
                } else {
                    let carga =  await RNIap.clearTransactionIOS();
                }
    
                ListPlanes();
                console.log("---------------- AREA DE CARGA PRODUCTOS- ------------ ");
                // getAvailablePurchases();

                let purchaseUpdate = purchaseUpdatedListener(
                    async (purchase: InAppPurchase | SubscriptionPurchase) => {
                      console.info('purchase', purchase);
              
                      const receipt = purchase.transactionReceipt
                        ? purchase.transactionReceipt
                        : purchase.originalJson;
                      console.info(receipt);
                      if (receipt) {
                        try {
                          const ackResult = await finishTransaction(purchase);
                          console.info('ackResult', ackResult);
                        } catch (ackErr) {
                          console.warn('ackErr', ackErr);
                        }
              
                        this.setState({receipt}, () => this.goNext());
                      }
                    },
                  );

                 setPurchaseUpdateSubscription(purchaseUpdate)

                
                  let variable  =   purchaseErrorListener(
                      (error: PurchaseError) => {
                      console.log('purchase error', JSON.stringify(error));
                      },
                  )

                  setPurchaseErrorSubscription(variable);
      
    
      }, [])


     


    //   Cancelar Sucription 
    async function CancelSuscription(){
      // purchaseUpdateSubscription.remove();
      setPurchaseUpdateSubscription(null);

      // Cortamos la conxion  
      // OJO API DE PRUEBA NO SE SI ESTA FUNCIONE 
      let cancelar = RNIap.endConnection();
      console.log(cancelar, " -------- RESPUES DE LA PROMESA -----");
     
  }


  //   console.log(purchaseErrorSubscription,'estado del error');
  
  
  async function ListPlanes(){
      try {
        const products = await RNIap.getSubscriptions(items);
      //   Formatear la lista de productos  
       let a = products;
       if (a) {
        a.sort((a, b) => a.price.replace(',', '')- b.price.replace(',', ''));
       }
       setProducts(products);
       console.log(products, "------------ PRODUCTOS ----------")
      } catch (err) {
        console.log('NO PUEDE ACCEDER A LOS PRODUCTOS ');
      }
  
    }

      function ValidateButton(){
        console.log("mi plan seleccionado para comprar = ", planSelect);
        if(Platform.OS == 'android'){
            console.log("entra aqui en android")
            // vali(user);
            PagarSuscription();
      //       console.log("entrando a la function de android");
        }
        else {
            PagarSuscription();
            console.log("entrando a la function de IOS");
        }
      }


    async function PagarSuscription(){

      // vali();
            // DELE AL BOTON  
              try {
                     console.log("esta entrando en esta funtion")
                        let pago =  await RNIap.requestSubscription(planSelect,false).then(res => {
                        vali();
                        console.log("-------- INTENTO DE PAGO  ---------------")
                        }).catch(err => {
                        console.log(err.code, err.message);
                        });
                          console.log("esta entrando en esta funtion")
                          console.log(pago);
                          
                        } catch (err) {
                          console.log(err.message);
                   }
              }





 


// Se pasa por esta funcion validando que pago y que el plan se registro con exito 
async function  ValidatePlan(userData){
    try {
      console.info(
        'Get available purchases (non-consumable or unconsumed consumable)',
      );
      const purchases = await RNIap.getAvailablePurchases();
      console.info('Available purchases :: ', purchases);
      if (purchases && purchases.length > 0) {
        setPurchased(true);
        // Aqui debemos hacer la validacion del correo 
        if(userData.pay){
            validarPay(userData);
        }else {
            console.log("No puede seguir, ya la cuenta ID App store esta asociada");
        }

      }else{
        setPurchased(false);
        // alert("Something went wrong and you were unable to subscribe to a plan.")
      }
      } catch (err) {
        console.warn(err.code, err.message);
        alert(err.message);
      }
    }


  async function validarPay(item) {
      RNIap.endConnection();
      navigation.replace('validationSesion', { user:item })
  }


  async function vali() {
      try {
          await fetch('https://go.vfa.app/api/play/pay', {
              method: 'POST',
              headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': 'Bearer ' + token
              },
              body: JSON.stringify({
                  id:user.id,
                  fecha_suscription:new Date(),
                  plan:planSelect,
                  pay:true
              }),
          }).then(response => response.json())
          .then((res) => {
                  // console.log("en este punto el user ya se registro")
                  ValidatePlan(res.user);
                  // validarPay(res.user);
                  setUser(res.user)
           
          }).catch(error=>{
              console.log("aqui");
              console.warn(error)
          });
      } catch (error) {
          console.warn(error)
      }
  }


  async function getUser(token) {

    console.log("Se cumple la primera lista ------")

    await fetch(url + 'api/user', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
    }).then(res => res.json())
    .then(async(dat) => {
        console.log(dat, ' ----- datos')
        try {
            setUserdato(dat)

        } catch (error) {
            Snackbar.show({
                text: 'Failed to logout',
                duration: Snackbar.LENGTH_LONG,
            });
        }
    })
}




    async function LogoutUser(){ // Se desloguea el user

        console.log("tamo activo")

        try {
            console.log("tamo activo 2")
            logoutBackend()
        } catch (error) {
        //   console.error(error);
          return null;
        }
      }



      async function logoutBackend(){
        setLoader(true);
        await fetch(url + 'api/logout', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
        }).then(res => res.json())
        .then(async(dat) => {
            console.warn(dat)
            try {
                if (dat.ok) {
                    setLoader(false);
                    const keys = ['@user', '@token', '@skills', '@reviews', '@team', '@coach']
                    await AsyncStorage.multiRemove(keys)
                    navigation.replace('Login');
                }else {
                    setLoader(false);

                }
            } catch (error) {
                setLoader(false);
                Snackbar.show({
                    text: 'Failed to logout',
                    duration: Snackbar.LENGTH_LONG,
                });
            }
        })
    }




  

     // SESSION 
     const Planes=({item, index, separators})=>{
      var urlImage = item.file_captura;
      return(
         <View style={{marginTop:20}}> 

         
        <TouchableOpacity activeOpacity={0.9} onPress={()=>{setSelectPlanIndex(index), setPlanSelect(item.productId)}} 
                style={[styles.bloquePlans, {width:wp('42%'), height:hp('26%'), borderColor:selectPlanIndex == index?'#cdffcb':'#eee', marginHorizontal:5, justifyContent:'center', alignItems:'center'}]}>
                      {/* check */}
                        {/* {selectPlanIndex == index?:null } */}

                      <View style={{width:15, height:15, backgroundColor:selectPlanIndex == index?'#ff5722':'#eee', position:'absolute',left:wp('2%'), top:wp('2%'), padding:5, borderRadius:50 }}>
                         <Icon name="check-bold" style={{color:selectPlanIndex == index?'white':null}}></Icon>
                     </View>


                 <Image source={require('../assets/corona.png')} style={{
                    height: '17%',
                    width: wp('17%'), 
                    // position: "absolute",
                  }} /> 

            
                     {item.productId == 'vfa_player_annual_10dst'?
                     <View style={{backgroundColor:'#ff5722', position:'absolute', right:4, top:4, padding:5, borderRadius:5}}>
                         <Text style={{color:'white', fontWeight:'bold',fontSize:9, textTransform:'uppercase'}}>{I18n.t('registerPlayer.payPlayer.text5')}</Text>
                     </View>
                     :null}

                      <View>
                           {/* Validacion de los mensajes titulo  */}
                           <Text style={styles.titlePlan}>{item.productId == 'vfa_player_annual_10dst'?'12 '+I18n.t('registerPlayer.payPlayer.text6') :item.productId == 'vfa_player_biannual_10dst'?'6 '+I18n.t('registerPlayer.payPlayer.text6'):
                           item.productId == 'vfa_player_quarterly_10dst'?'3 '+ I18n.t('registerPlayer.payPlayer.text6'):item.productId == 'vfa_player_monthly_10dst'?'1 '+I18n.t('registerPlayer.payPlayer.text7'):null}</Text>
                      </View>
                      <View style={{justifyContent:'center', alignItems:'center', flexDirection:'row'}}>
                           {/* Validacion de los mensajes precio  */}
                           {/* <View style={{borderBottomColor:'#eee', borderBottomWidth:1, width:100}}></View> */}
                           <Text style={{textAlign:'center', fontWeight:'bold', fontSize:30, color:'#3d3d3d', opacity:0.8}}> {item.localizedPrice}</Text>
                           {/* <View style={{borderBottomColor:'#eee', borderBottomWidth:1, width:100}}></View> */}
                      </View>
                      
                      {item.productId != 'vfa_player_monthly_10dst'?

                      <View style={{flexDirection:'column', justifyContent:'center', alignItems:'center', textAlign:'center', marginTop:10}}>
                          {/* Validacion de los mensajes footer  */}
                          <Text style={{textAlign:'center', fontWeight:'bold',fontSize:15}}>
                           {item.productId == 'vfa_player_annual_10dst'?(Number(item.price)/12).toFixed(2):null}{item.productId == 'vfa_player_biannual_10dst'?(Number(item.price)/6).toFixed(2):null}{item.productId == 'vfa_player_quarterly_10dst'?(Number(item.price)/3).toFixed(2):null} {I18n.t('registerPlayer.payPlayer.Permonth')}
                          </Text>
                          {/* <Text style={{width:wp('36%'), textAlign:'center', fontWeight:'bold', fontSize:20}}> </Text> */}
                          {/* <Text style={{textAlign:'center', fontWeight:'bold', fontSize:15}}>save 50%</Text> */}
                          <Text style={{textAlign:'center', fontWeight:'bold', fontSize:16, marginTop:10}}>{I18n.t('registerPlayer.payPlayer.textSave')}  {item.productId == 'vfa_player_annual_10dst'?'60':null}{item.productId == 'vfa_player_biannual_10dst'?'43':null}{item.productId == 'vfa_player_quarterly_10dst'?'33':null}%</Text>
                      </View>
                      :null}

                    </TouchableOpacity>
                    </View>
          
      );
  }




     
  



  console.log(userdato, '----user-ssssss-');
    return (
        <View style={styles.container}>

           {loader ? <Loader /> : null}
            {/* button cerrar  */}


            <Image source={require('../assets/pelota.png')} style={{
                height: '100%',
                width: wp('100%'), position: "absolute",
                opacity:0.4,

            }} />

           <View  style={{position:'absolute', zIndex:1000, top:hp('3%'), left:10}}>
           <TouchableOpacity onPress={() => {LogoutUser();}}>
                <Icon  name="chevron-left" size={40} ></Icon>
           </TouchableOpacity>
            </View>
            {/* fin del boton cerrar  */}


            <View style={{width:wp('100%'), height:hp('10%'), justifyContent:'center', alignItems:'center'}}>
                <Image source={require('../assets/Logo.png')} resizeMode="contain" style={{width:hp('8%'), height:hp('8%')}}/>  
            </View>

            {/* <SafeAreaView style={styles.containerTow}>
             <ScrollView style={styles.scrollView}> */}


             <View style={{width:wp('100%'), height:hp('3.5%'), justifyContent:'center', alignItems:'center', paddingHorizontal:20}}>
                <Text style={{textAlign:'center', fontSize:19, fontWeight:'bold'}}>
                    {/* {I18n.t('payAppPlayer.title')} */}
                    {I18n.t('registerPlayer.payPlayer.title')}
                    </Text>


                    {userdato?

                    userdato.mycupon.red1?
                    <Text style={{marginTop:5, fontSize:16, fontWeight:'bold', color:'#333'}}>
                      10% OFF thanks to  <Text style={{ color:'purple'}}>{userdato.mycupon.red1}</Text>
                   </Text>
                   : userdato.mycupon.red2?
                   <Text style={{marginTop:5, fontSize:16, fontWeight:'bold', color:'#333'}}>
                      10% OFF thanks to <Text style={{ color:'purple'}}>{userdato.mycupon.red2}</Text>
                   </Text>
                   :null
                   :null}
            </View>

            {/* <View style={{marginTop:hp('6%'), width:wp('100%'), height:hp('10%'), justifyContent:'center', alignItems:'center'}}>
                <Image source={require('../assets/pay2.png')} resizeMode="contain" style={{width:hp('30%'), height:hp('30%')}}/>  
            </View> */}


              <View style={{ flex: 1, justifyContent:'center', alignItems:'center', marginTop:40}}>

    
              <FlatList
                    data={products}
                    renderItem={Planes}
                    keyExtractor={(item, index) => index}
                    numColumns={2}
                    initialNumToRender={1}
                    // style={{width: wp('100%'), flex: 1, marginTop:15}}
                    // onRefresh={() => getVideosHomework(token,user)}
                    refreshing={false}
                    onEndReachedThreshold={0.7}
                    removeClippedSubviews={true}
                />



                
                




               

              </View>





{/* 

    <View style={{marginTop:hp('5%'),flex:1, alignItems:'center', justifyContent:'center', marginHorizontal:20}}>

      {
         products?
         products.map((dato,index)=>{
             return (
                <TouchableOpacity activeOpacity={0.9} onPress={()=>{setSelectPlanIndex(index), setPlanSelect(dato.productId)}} 
                style={[styles.bloquePlans, { borderColor:selectPlanIndex == index?'#cdffcb':'#eee'}]}>
                      <View style={{width:15, height:15, backgroundColor:selectPlanIndex == index?'green':'#eee', position:'absolute',left:wp('2%'), top:wp('2%'), padding:5, borderRadius:5 }}>
                         <Icon name="check-bold" style={{color:selectPlanIndex == index?'white':null}}></Icon>
                     </View>
                     {dato.productId == 'vfa_player_annual'?
                     <View style={{backgroundColor:'#ff5722', position:'absolute', right:wp('2%'), top:hp('1%'), padding:5, borderRadius:5}}>
                         <Text style={{color:'white', fontWeight:'bold'}}>{I18n.t('registerPlayer.payPlayer.text5')}</Text>
                     </View>
                     :null}

                      <View>
                           <Text style={styles.titlePlan}>{dato.productId == 'vfa_player_annual'?'12 '+I18n.t('registerPlayer.payPlayer.text6') :dato.productId == 'vfa_player_biannual_10dst'?'6 '+I18n.t('registerPlayer.payPlayer.text6'):
                           dato.productId == 'vfa_player_quarterly_10dst'?'3 '+ I18n.t('registerPlayer.payPlayer.text6'):dato.productId == 'vfa_player_monthly_10dst'?'1 '+I18n.t('registerPlayer.payPlayer.text7'):null}</Text>
                      </View>
                      <View style={{justifyContent:'center', alignItems:'center', flexDirection:'row'}}>
                           <View style={{borderBottomColor:'#eee', borderBottomWidth:1, width:100}}></View>
                           <Text style={{textAlign:'center', fontWeight:'bold', fontSize:wp('9%'), color:'#3d3d3d'}}> {dato.localizedPrice}</Text>
                           <View style={{borderBottomColor:'#eee', borderBottomWidth:1, width:100}}></View>
                      </View>
                      {dato.productId != 'vfa_player_monthly_10dst'?

                      <View style={{flexDirection:'row', marginTop:10}}>
                          <Text style={{textAlign:'center', fontWeight:'bold',marginRight:20, fontSize:wp('4%')}}>
                           {dato.productId == 'vfa_player_annual'?(Number(dato.price)/12).toFixed(2):null}{dato.productId == 'vfa_player_biannual'?(Number(dato.price)/6).toFixed(2):null}{dato.productId == 'vfa_player_quarterly'?(Number(dato.price)/3).toFixed(2):null} {I18n.t('registerPlayer.payPlayer.Permonth')}
                          </Text>
                          <Text style={{textAlign:'center', fontWeight:'bold', fontSize:15, marginLeft:20}}>Save {dato.productId == 'vfa_player_annual'?'60':null}{dato.productId == 'vfa_player_biannual'?'43':null}{dato.productId == 'vfa_player_quarterly'?'33':null}%</Text>
                      </View>
                      :null}

                    </TouchableOpacity>
             )
         })
         :null

      }

            
            </View> */}


            {/* <View style={{width:wp('100%'),  height:hp('10%'), paddingHorizontal:wp('5%'), justifyContent:'center', alignItems:'center'}}>
                <Text style={{fontSize:hp('1.7%')}}>
                       {Platform.OS =='android'? 
                        <Text>{I18n.t('registerPlayer.payPlayer.text1Android')}</Text>
                       : <Text>{I18n.t('registerPlayer.payPlayer.text1IOS')}
                       </Text>
                       }

                    </Text>
                <View style={{paddingTop:hp('2%')}}>
                    <Text style={{textAlign:'center', fontSize:hp('1.5%')}}>
                             {I18n.t('registerPlayer.payPlayer.text2')}
                             {Platform.OS == 'android'?
                              <Text style={{fontWeight:'bold'}}> 
                              Google Play</Text> : <Text style={{fontWeight:'bold'}}> {I18n.t('registerPlayer.payPlayer.text3')}</Text>
                             } {I18n.t('registerPlayer.payPlayer.text4')}

                        </Text>
                </View>
            </View> */}
            <View>

              
                <Block center>
                    <TouchableOpacity activeOpacity={0.9} onPress={() => {
                        // vali()
                        // buy()
                        ValidateButton()
                    }} style={styles.Button}>
                            <Text style={{fontWeight:'bold', color:'#FFF', fontSize:hp('1.7%')}}>
                                {I18n.t('registerPlayer.payPlayer.bottom')}
                               
                            </Text>
                            <Icon name="cart" size={20} style={{color:'white'}}></Icon>
                    </TouchableOpacity>
                </Block>
            </View>


            {/* </ScrollView>
            </SafeAreaView> */}

           




            
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
//    Stylos de los bloquel del plan \
titlePlan:{
  textAlign:'center', fontWeight:'bold', fontSize:17,
  color:'#3d3d3d',
  opacity:0.4
  },
    bloquePlans: 
    {
      backgroundColor:'white', 
    borderWidth:1,
    
    justifyContent:'center', 
    alignItems:'center',
    width:wp('70%'), 
    marginBottom:20,
    height:hp('16%'),
    elevation:4, 
    borderRadius:10 
    },
//  stylos de los bloques del plan 




    containerTow: {
        flex: 1,
        // paddingTop: StatusBar.currentHeight,
      },
      scrollView: {
        // backgroundColor: 'white',
        // marginHorizontal: 20,
      },


    ViewsubText:{
        justifyContent:'center',
        alignItems:'center', 
        paddingRight:10,

   },

   subText:{
       fontSize:hp('2%'),
       fontWeight:'bold',
       position:'absolute',
       bottom:hp('-5.5%'),
       right:1,
       color:'#787878'
      

   },


TextPlansTitle:{
   marginTop:10,
   fontWeight:'bold', 
   fontSize:hp('2.5%'), 
   color:'#3d3d3d',
   width:'auto'


},

ViewIcon:{
   justifyContent:'center', 
   alignItems:'center', 
   borderWidth:1,
   borderColor:'#9A9A9A', 
   height:hp('2.5%'), 
   width:hp('2.5%'), 
   borderRadius:hp('2.5%'),
   position:'absolute',
   right:wp('2%'),
   top: hp('-3.5%'),
  },

Label: {
   backgroundColor: '#ff5722', 
   paddingLeft:5,
   paddingTop:0,
   paddingBottom:2,
   paddingRight:6,
   borderRadius:6,  
   position:'absolute',
   top: hp('-2.5%'),
   left:wp('1.5%')

   // width:hp('10%')
  },


  



    container: {
        flex: 1,
        justifyContent:'center',
        backgroundColor: '#fff',
    },
    Button: {
        width:wp('90%'),
        marginTop: -110,
        marginBottom: 40,
        shadowColor: '#11A10F',
        backgroundColor: '#11A10F',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderRadius:10,
        elevation: 5,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        height:hp('6%'),
    },
});


//make this component available to the app
export default PayAppPlayerDiscount;
