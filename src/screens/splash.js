import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Platform } from 'react-native'
import Video from 'react-native-video';
import InViewPort from "@coffeebeanslabs/react-native-inviewport";
import { useTheme } from '@react-navigation/native';
import { setFCMTOKEN } from '../storage/user/dataUser';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Splash = ({ navigation }) => {
  const [play, setPlay] = useState(false);
  const [logger, setLogger] = useState(false);
  const { colors } = useTheme();
  useEffect(async() => {
    user();
    // checkPermission();
  })

  // async function requestUserPermission() {
  //   const authStatus = await messaging().requestPermission();
  //   const enabled =
  //     authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
  //     authStatus === messaging.AuthorizationStatus.PROVISIONAL;

  //   if (enabled) {
  //     var fcmToken = await messaging().getToken();
  //     setFCMTOKEN(fcmToken)
  //     console.log('Authorization status:', authStatus);
  //   }
  // }

  // async function checkPermission() {
  //   const enabled = await messaging().hasPermission();
  //   if (enabled) {
  //     requestUserPermission();
  //   } else {
  //     try {
  //       await messaging().requestPermission();
  //       requestUserPermission();
  //     } catch (error) {
  //       console.log('permission rejected');
  //     }
  //   }
  // }


  async function user() {
    try {
      // const user = GetUser;
      const user = await AsyncStorage.getItem('@user');
      const userJson = JSON.parse(user);
      const welcomeVali = await AsyncStorage.getItem('@welcome1');
      const welcome1Json = JSON.parse(welcomeVali); // Verificar si esta habilitado la vista o no
      setTimeout(() => {
        if (userJson) {
          if (userJson.id) {
            navigation.replace('validationSesion', { user: userJson });
          }
        } else {
          if (!welcome1Json) { // Verificar si esta habilitado la vista o no
            navigation.replace('Welcome1');
          }else{
            navigation.replace('Login');
          }
        }
      }, 2100);
    } catch (error) {
      // console.log(error)
    }
  }


  return (
    <View style={{ width: '100%', height: '100%', backgroundColor: colors.blanco }}>
      <Video source={require('../assets/splash/VFAv2.mp4')}
        resizeMode="cover"
        style={styles.backgroundVideo} />
    </View>
  );
}

const styles = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});


export default Splash