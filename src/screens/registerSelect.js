//import liraries
import React, { useState, useEffect, useLayoutEffect, useRef } from 'react'
import {
    StyleSheet,
    Dimensions,
    Image,
    View,
    StatusBar,
    SafeAreaView,
    ActivityIndicator,
    KeyboardAvoidingView,
    Platform, 
    Text,
    DeviceEventEmitter,
    TouchableOpacity
} from 'react-native';
import Swiper from 'react-native-swiper';
import Icon  from 'react-native-vector-icons/MaterialCommunityIcons';
import I18n from 'react-native-i18n';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Block, Checkbox, Input, Text as T, Button as GaButton, theme } from 'galio-framework'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

// constantes de vista
const { width, height } = Dimensions.get('screen');

// create a component
const RegisterSelect = ({navigation,route}) => {
    const [isCoach, setIsCoach] = useState(false); // validar seleccion

    useLayoutEffect(() => {
        navigation.setOptions({
            title:'',
        });
    }, [])

    async function redirectCoach() {
        // const welcomeCoach = await AsyncStorage.getItem('@welcomeCoach');
        // const WCJson = JSON.parse(welcomeCoach);
        // if (!WCJson) {
            navigation.navigate('Welcome Coach');
        // }else{
        //     navigation.navigate('Register Coach')
        // }
    }

    async function redirectPlayer() {
        // const welcomePlayer = await AsyncStorage.getItem('@welcomePlayer');
        // const WPJson = JSON.parse(welcomePlayer);
        // if (!WPJson) {
            navigation.navigate('Welcome Player');
        // }else{
        //     navigation.navigate('Register Player')
        // }
    }

    return (
        <View style={styles.container}>
            <Image source={require('../assets/pelota.png')}  opacity={0.6} style={{
                height: '100%',
                width: wp('100%'), position: "absolute",
                bottom:0, right:0
            }} />



            <View style={{width:wp('100%'), height:hp('10%'), marginTop:hp('7%'), justifyContent:'center', alignItems:'center'}}>
                <Text style={{fontSize:hp('3.5%'),}}>{I18n.t('register.h1')}</Text>
                {/* <Text style={{fontSize:hp('2%')}}>{I18n.t('register.h2')}</Text> */}
            </View>


            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
            <TouchableOpacity activeOpacity={0.9} onPress={()=>setIsCoach(false)} style={{backgroundColor: '#fff', borderWidth:isCoach?1:2, borderColor:isCoach?"#f4f4f4":"#11A10F", justifyContent:'center', alignItems:'center', marginTop:hp('5%'), flexDirection:'row', width:'80%', height:hp('15%'), elevation:2, borderRadius:10 }}>
                    <View>
                        <Image source={require('../assets/imgs/registre/player.png')} resizeMode="cover" style={{width:hp('6%'), height:hp('8%')}}/>
                    </View>
                    <View style={{width:'80%',}}>
                        <View style={{flex:1, paddingHorizontal:10, flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                            <Text style={{fontSize:hp('2%'), paddingTop:5, fontWeight:'bold', color:'#3d3d3d'}}>Player</Text>
                            <View style={{justifyContent:'center', alignItems:'center', borderWidth:1,borderColor:'#9A9A9A', height:hp('2.5%'), width:hp('2.5%'), borderRadius:hp('2.5%')}}>
                                <Icon name="circle" size={hp('2%')} color={isCoach?"#eee":"#11A10F"}/>
                            </View>
                        </View>
                        <View style={{flex:2, paddingHorizontal:10,}}>
                            <Text>
                                {I18n.t('register.player')} 
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity activeOpacity={0.9} onPress={()=>setIsCoach(true)} style={{backgroundColor: '#fff', borderWidth:isCoach?2:1, borderColor:isCoach?"#11A10F":"#f4f4f4", justifyContent:'center', alignItems:'center', flexDirection:'row', width:'80%', height:hp('15%'), marginTop:hp('5%'), elevation:2, borderRadius:10 }}>
                    <View>
                        <Image source={require('../assets/imgs/registre/coach.png')} resizeMode="cover" style={{width:hp('6%'), height:hp('8%')}}/>
                    </View>
                    <View style={{width:'80%',}}>
                        <View style={{flex:1, paddingHorizontal:10, flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                            <Text style={{fontSize:hp('2%'), paddingTop:5, fontWeight:'bold', color:'#3d3d3d'}}>Coach</Text>
                            <View style={{justifyContent:'center', alignItems:'center', borderWidth:1,borderColor:'#9A9A9A', height:hp('2.5%'), width:hp('2.5%'), borderRadius:hp('2.5%')}}>
                                <Icon name="circle" size={hp('2%')} color={isCoach?"#11A10F":"#eee"}/>
                            </View>
                        </View>
                        <View style={{flex:2, paddingHorizontal:10,}}>
                            <Text >
                                {I18n.t('register.coach')} 
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>
               
            </View>
            <View>
                <Block center>
                    <TouchableOpacity activeOpacity={0.9} onPress={() => {
                        if (isCoach) {
                            redirectCoach()
                        }else{
                            redirectPlayer()
                        }
                    }} style={[styles.Button, {flexDirection:'row'}]}>
                            <Text style={{fontWeight:'bold', color:'#FFF', fontSize:hp('1.7%')}}>
                                {I18n.t('register.bottom')}  
                               
                            </Text>
                            <Icon name="arrow-right" size={wp('4%')} style={{marginLeft:10, color:'white'}}></Icon>
                    </TouchableOpacity>
                </Block>
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'center',
        backgroundColor: 'white',
    },
    Button: {
        width: width * 0.8,
        marginTop: 25,
        marginBottom: 40,
        shadowColor: '#11A10F',
        backgroundColor: '#11A10F',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderRadius:10,
        elevation: 5,
        justifyContent:'center',
        alignItems:'center',
        height:hp('6%'),
    },
});


//make this component available to the app
export default RegisterSelect;
