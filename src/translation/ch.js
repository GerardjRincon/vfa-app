
export default  {

    texto4:"Activation Code is not valid. Please verify that you have typed the correct code.",
    tex1:"首页",
    tex2:'我的团队',
    tex3:'排名',
    tex4:'培训计划',
    tex5:'形象',
    tex6:'没有球员',
    tex7:'没有活动',
    tex8:'我的墙',
    tex9:'家庭作业',
    tex10:'统计数据',
    tex11:'评论',
    tex12:'盒装',
    tex13:'待完成的作业',
    tex14:'视频',
    tex15:'点',
    tex16:'聊天',
    tex17:'详细练习', 
    tex18:'你有一个促销代码吗',
    tex19:'邀请一个新玩家加入',
    tex20:'跳过',

	Welcome:{
		title1: "虚拟足球研究",
		descripcion1: "荷兰式方法改变了足球的历史",
		title2: "实际方法",
		descripcion2: "实际方法已经有了超过25年的历史",
		title3: "现实生活运动",
		descripcion3: "我们在真实的运动中获得了运动员演绎的技巧，主要体现在3D与增强现实技术中",
		Buton: "继续",
    },

	login:{
		Bienvenida:"欢迎来到虚拟足球研究",
		email:"邮箱",
		helpEmail:"输入你的邮箱",
		password:"密码",
		helpPass:"输入你的密码",
		login:"登入",
		account:"新人",
		signUp:"建立账号",
		recover:"忘记密码",

		ValidateInputEmail:"输入一个有效的邮箱地址",
		ValidteInput:'输入所有的场地',
		ErrorPasswordValidate:'密码必须包含至少8个字体，一个大写和一个数字',
    },

		
     homeplayer:{
        alert:{
            nosuscription:"你不需要有一个积极订阅",
            fileLogout:"退出失败"
         },
         modalInicioPlayer:{
            text1:"在项目的最初的14级中，我们会一直开始学习热身技巧，这个技巧加倍补步法技能的球感，灵活性和速度。这些技能能够获得球感与其他一些较难的技巧。在之后的等级中，你能够开始独自热身与练习，而且也可以有AR视频的录制",
            text2:"在接下来的21级中，我们建议你在热身时运用之前所有所学的热身技巧",
            text3:"另外我在接下来的几个等级中我们增加了一些目录与技巧，这些都是根据我们的技术足球技巧项目的学习顺序来制定的，这个学习顺序包含了所有的现代足球游戏的技术。你可以用你自己的速度来练习，并且试着尽可能的多学习些。越往下的等级也会越难。可以保证的是在游戏的最后你会看到你努力的结果.",
            buttom:"继续",
       },
       modalNextLevel:{
		text1:"等级",
		text2:"解锁",
		text3:"每一天都能成为更好的玩家! 保持练习!",
		buttom:"继续"
    }
    
    },

    homecoach:{
        alert:{
            nosuscription:"你不需要一个积极订阅",
            fileLogout:"退出失败",

        }, 

    },

TutorialView:{
	niveltitle:"等级",

	buttom:'继续',
	level1:{
		text1:"球感与控球是学习技术足球技巧的基础。我们为初学者制定了简单的方法。作为项目的第一级，我们希望玩家可以尽快获得这个技巧",
		text2:"演绎技巧是要一直保持两只脚，这样你会开始于Tick Tock（像时钟一样的声音），一种很简单的演绎技巧，当你在球场上上下移动时，用两只脚去触碰。折返式路线就像是Tick Tock，只要你在用折返式路线，它就不是一条直线的上下移动",
		text3:"我们也会制定一个初始的控制球与传球，这些也是非常重要的技巧需求同时也是踢足球的基本技能。记住永远用两只脚练习"
    },

	level2:{
		text1:"当你因为对手而改变了方位，有四种不同方位是你可以跟着球的，后传，左传，右传与前传，这四种是最逻辑顺序，因此我们开始于向后，这是一种削减技能",
		text2:"你不仅可以改变你的路线还可以挡住迎面而来的对手的球，然后继续控球",
		text3:"从对手那里接球与运球也是非常重要的，然后再运动中处理球。 从这里开始就是第二等级了",
    },

	level3:{
		text1:"在V Tick Tock里达到一个等级时，我们现在结合之前两个热身技巧和新学的一个技巧，使用脚背",
		text2:"现在开始横传中的避开技巧，这是学会如何通过接触对手来改变方向是左传还是右传",
    },

    level4:{
		text1:"你可能注意到了在我们热身技巧中我们已近使用到了脚上的所有部位，现在我们要开始使用我们脚部内侧与脚底。因为你已经学会了如何运球，现在我们希望你在运球上加快速度，加速时也不要忘记观察你的周围",
		text2:"外切是一个非常难以对付的技巧，并且难以学会。通过学习这个技巧，你将会有能力从对手那里护住你的球，然后用你脚部的内侧与外侧后传"
    },

	level5:{
		text1:"在这个等级中，我们将会学习另外一种避开技巧，现在开始两边传球，通过对手可以使用你脚部的内外侧",
		text2:"回想一下，你先在有能力改变后传与横传方向，并且一直保持运球如果有对手一直攻击你，另一种前传是用脚前踢球，脚前踢球不仅仅是为了进球，还是为了长线传球"
    },

	level6:{
		text1:"新的实用技巧，界外球，这个技能看起来比较简单，让你的脚保持在地面并且在线的后面! 用你的脚的外侧横传，这个需要练习很长时间，这个技能在运动中对控球与运球是非常重要的",
		text2:"记住在运动中接球与运球一定要到一个比较空的地方"
    },

	level7:{
		text1:"热身技巧已经越来越难了，特别是握踝翻滚，地滚球需要很强的球感",
		text2:"什么是颠球？我们开始只用一只脚，通过颠球你会增强你的球感，当然颠球也很有趣，可以和朋友们挑战一下! 你注意到了热身技巧中的避开与外切技能中都是通过不同的方式来练习的吗？我们制定这个是希望你能玩的越来越好",
    },

	level8:{
		text1:"在这个等级中，我们将会给你一些选项，通过结合之前的所学的热身技巧来控制与避开球。提示，注意你要旋转180度会到你原来的地方"
    },

	level9:{
		text1:"只是我最喜欢的技巧之一，趾行，行走时只能用你的脚趾触碰球并且观察你的周围，不要仅仅只是看着你的球，这个很有趣"
    },

	level10:{
		text1:"在热身技巧中现在你已经有很大进步了，界外球是需要积累的，在对付你的对手时假动作技巧也是非常有效的，特别是当你有力的去做一个踢的动作（假踢"
    },

	level11:{
		text1:"在热身技巧中，我们将要做一些比较花式步法，现在我们要学一个非常著名并且有效的技巧，名叫The Maedtro （克鲁伊夫转身），这是已世上最出名的一个足球运动员命名的，荷兰足球运动员约翰 克鲁伊夫，现在很多顶级足球运动员一直在复制这个动作"
    },

	level12:{
		text1:"现在你已经通过了大概一半的等级了，非常好! 保持练习并且重复之前的技能，熟能生巧"
    },

	level13:{
		text1:"这个等级是一个很难的挑战，铲球，练习这个技能要非常小心并且逐渐增强你的训练强度，记住当代的足球运动中就算是攻击性运动员也知道如何使用铲球这一技巧"
    },

	level14:{
		text1:"除了花式热身的感觉，技巧中你现在需要练习的是长线球，这个技巧要求集中精神的精准传递",
		text2:"练习断线传球并且逐渐增强强度，运球时需要一个支撑脚，齐达内用过一样的技巧",
    },

	level15:{
		text1:"现在我们要介绍一些新的技巧，非常难的技巧! 在接球与运球时你需要运用踢反弹球的技术，这个技巧需要非常的球感",
		text2:"逐渐增长距离在你与你同伴或朋友之间，不要灰心，学习如何用头顶球需要时间! 现在是颠球时间但是使用两只脚，这个技巧是非常有用的且有趣，试着能不能进入全球排名，这是一个挑战"
    },

	level16:{
		text1:"在这个等级中我将要学会攻击对手，直传，在1对1的情况下我们开始会学习单个或者双个技能来攻击对手，攻击对手在足球中是最难的一个方面",
		text2:"单个或双个技能意思是你可以攻击一个对手的任意一边，对手无法预测你的动作， 内弧线球是当今足球中非常重要的技巧，将任意球踢入网中，这个技巧需要非常多的练习，努力的学会内弧线球的技巧，所以不要放弃! 放轻松点!"
    },

	level17:{
		text1:"现在我们会给你更多的选择，当你在1对1的时候让你攻击你的对手时无法预料，我们也会学一些技巧去攻击你的对手当他们在你的身后时，前锋/直传特别需要学习这个技巧，这个可以是你成为非常好的球员"},

	level18:{
		text1:"另外一个踢反弹球技巧是用你的脚步外侧，这个是非常难的，并且需要很长的时间去学，但是现在开始时我们可以简单一点的练习跳起来头部顶球"
    },

	level19:{
		text1:"外弧线是非常实用性的传球技巧，非常有效的技巧当你运用的非常精通时，这是在铲球中最后要学的一个技能，这个能让你成为一个全面的球员",
		text2:"目前为止你应该有能力实用这些技巧与你的对手对弈了，当然你还是需要保持练习，这样可以加快你在运动的速度"
    },

	level20:{
		text1:"在这个等级中我们讲学会假动作，这个的意思是在你接球之前用假动作来迷惑你的对手，抱起来头球需要很长时间，徐汇这个你需要花很长时间来练习"
    },

	level21:{
		text1:"很多著名的运动员会使用更多的一些技巧来攻击的对手"
    },

	level22:{
		text1:"到这里，截击空中球是你需要学习的一个技巧，一个非常惊人的踢球技巧"
    },

	level23:{
		text1:"现在快要到终点了，保持练习"
    },

	level24:{
		text1:"马上到终点了，还剩最后一个等级了"
    },

	level25:{
		text1:"非常好，25个等级100个技能，你全部做到了，我们会更新你的等级，我们会分享你的视频与成绩在我们的社交平台上",
    }
},


ar:{
    alert: {
        permitcamera:'摄像许可',
        msgpermitcamera:'VFA需要摄像访问',
        textsuccessfully:'保存成功',
        textdenied:'拒绝访问',
        hasoccurred:'出现错误',
        necessarypermissions:'必要权限不允许储存',
        uploading:'正在下载视频',
        errorUploading:'视频下载出现错误',
        notassigned:'技能没有指定',
        desnotassigned:'你的教练没有分配你任务',
        allowedpost:'你不能上传',
        needaprove:'你需要完成上一个等级才能上传这个视频',
        thevideocoach:'这个视频已经发给教练了',
        postvideo:'上传视频',
        cancel:'取消',

    },

    shareSocial:{
        text1:'帮助',
        // en medio de estos dos aparece el username 
        text2:'评估她的视频技巧',

    },

},


chat:{
    messajeInfo:'你并不需要一个指定的教练',
    placeholderMessage:'写一条信息'
},

notifications:{
    messageInfo:'没有通知可用',
    alerMessage:'你确定要删除这条通知'
},

previaNotifications:{
    record:'再次录制',
    // oracion de los votos 
    text1:'你需要',
    text2:'好评认可',

    buttonRecord:'录制',
},

myTeam:{

    menssagePage:'你的团队没有玩家，邀请玩家加入'

},




registerCoach:{
    titlePage: '建立教练账号',
    playerTab: '建立教练账号',
    coachTab: '教练',
    alert:{
        alertDate: '出生日期',
        alertSubmit: '你必须同意条款',
        alertSubmitPlayer: '凭证不正确',

        // Nuevas --- alexis  --------------------------------------------------------
        alertEmailalready:'这个邮箱已经被使用',
        errorMessage:'出现一个错误',
        errorPasswordValidate:'密码必须至少包含8个文字，一个大写与一个数字',
        //   ----------------------------------------------------------------------------
    },
   
    player:{
        titlePage: '建立一个玩家账号',
        title:'尝试使用免费',
        descripcion: '建立一个7天免费使用账号',
        coachVirtual: '虚拟教练会教你通过这些程序',
        coachPersonal: '你会加入你的教练与团队',
        optionCoachVirtual:'我想要一个虚拟教练',
        optionCoachPersonal:'我有一个教练',
        optionSelect:'选择一个正确的选项',
        name:'名字',
        helpName:'输入你的名字',
        email:'邮箱',
        helpEmail:'输入你的邮箱',
        password: '密码',
        helpPass: '输入密码',
        date: '我同意',
        text1: '我同意',
        text2: '隐私权政策',
        text3: 'and',
        text4: 'Terms and conditions.',
        submit: '提交',
    },
    coach:{
        titlePage: '建立玩家账号',
        title:'免费尝试',
        descripcion: '建立一个7天免费账号',
        name:'全名',
        helpName:'输入你的全名',
        email:'邮箱',
        helpEmail:'输入你的邮箱',
        password:'密码',
        helpPass:'输入你的密码',
        text1: '我同意',
        text2: '隐私权政策',
        text3: '与',
        text4: '条款',
        submit: '提交',
    }
},


registerPlayer:{
    titlePage: '建立教练账号',
    playerTab: '玩家',
    coachTab: '教练',
    alert:{
        alertDate: '出生日期',
        alertSubmit: '你必须同意这些条款',
        alertSubmitPlayer: '凭证不正确',

        // nuevo 
        validateAlertCredencial:'凭证错误',
        messageError:'出现一个错误',

    },
    // Nuevo  ----------------------------------------------------------------------------
    childrenPolicy:{

      title:'儿童在线隐私保护法案',
      text1:'父母注意',
      text2:'根据儿童在线隐私保护法案，年龄在13岁以下的小孩需要在父母（监护人）的同意下使用Sports Innovations LLC公司的产品与服务，如 VFA（虚拟足球研究），同意Sports Innovation LLC使用小孩的个人信息',
      text3:'根据儿童在线隐私保护法案，年龄在13岁以下的小孩需要在父母（监护人）的同意下使用Sports Innovations LLC公司的产品与服务，如 VFA（虚拟足球研究），同意Sports Innovation LLC使用小孩的个人信息',
      text4:'VFA(虚拟足球研究)的订阅支付方式，同意包括明示的同意使用信用的政策',
      text5:'父母事先同意的免责条款，在这一条款中可证实的父母同意书可以使用，收集，披露之前父母同意的小孩的个人信息',
      text6:'收集父母上线信息的目的是提供自愿提醒通知，然后可以为父母更新小孩上线的参与过程，这个网络服务并不会收集，使用，披露其他的小孩的个人信息，在此情况下，父母的网络联系信息不会被使用货披露在其他目的上，在这种情况下，网络公司必须做出合理的要求，使用信息时需要考虑到可用的技术，确保父母收到这样的通知，这里在条款312.4（c）（2）中有提到',
      text7:'更多的信息',
      emailPadre:'请输入正确的邮箱',
      textTerminos1:'我同意',
      textTerminos2:'儿童隐私',
      AcceptTerms:'同意儿童隐私条例',
      
    },
    //  ------------------------------------------------------------------------------------

    player:{
        titlePage: '建立玩家账号',
        title:'免费使用',
        descripcion: '建立一个7天免费使用的账号',
        coachVirtual: '这个虚拟的教练会教你通过这个程序',
        coachPersonal: '你将会加入你的教练与你的团队',
        optionCoachVirtual:'我想要一个虚拟教练',
        optionCoachPersonal:'我有一个教练',
        optionSelect:'选择正确的一个选项',
        name:'名字',
        helpName:'输入你的名字',
        email:'邮箱',
        helpEmail:'输入你的邮箱',
        password: '密码',
        helpPass: '输入你的密码',
        date: '出生日期',
        text1: '我同意',
        text2: '隐私权政策',
        text3: '与',
        text4: '条款和条件',
        // Nuevo
        // -----------------------------------------------------------------
        submit: '继续',
        validateInputEmail:'输入一个有效的邮箱地址',
        errorPasswordValidate:'密码必须包含8个字体，一个大写和一个数字',
        // -------------------------------------------------------------------
    },
    //  Nuevo  ---------------------------------------------------------------
    payPlayer:{
        title:'7天免费使用',
        Permonth:'每月',
        // PARRAFO DEL FINAL DE LOS PLANES PLAYER 
        text1Android: '任何时候都可以取消谷歌支付',
        text1IOS: '随时取消苹果订阅',
        text2:'你的订阅会重新下个月的同一时间订阅，除非你提前24小时取消，你可以在任何时候取消订阅',
        text3:'苹果账号订阅',
        text4:'没有额外消费，你的订阅会在最后一天取消',
        text5:'最划算',
        text6:'月',
        text7:'月',
        bottom:"开始我的免费使用",
        textSave:'保存 '
    },
    
    // --------------------------------------------------------------------------

//   Global coach 
 
    coach:{
        titlePage: '建立玩家账号',
        title:'免费尝试',
        descripcion: '建立7天免费使用账号',
        name:'全名',
        helpName:'输入你的全名',
        email:'邮箱',
        helpEmail:'输入你的邮箱',
        password:'密码',
        helpPass:'输入你的密码',
        text1: '我同意',
        text2: '隐私权政策',
        text3: 'and',
        text4: '条款与条件',
        submit: '提交',
    }
},


reviews:{
    text1:'出现一个未知错误',
    text2:'没有等待审核',
 },


 calendarCoach:{
      text1:'描述',
      text2:'选择日期',
      text3:'保存更改',
      text4:'你可以在这个练习中练习的技能',
      text5:'左脚',
      text6:'右脚',
      text7:'其他',
      text66:'详细练习'


 },


 // my teams 

 teams:{

     levels: {
         1:'等级 1',
         2:'等级 2',
         3:'等级 3',
         4:'等级 4',
         5:'等级 5',
         6:'等级 6',
         7:'等级 7',
         8:'等级 8',
         9:'等级 9',
         10:'等级 10',
         11:'等级 11',
         12:'等级 12',
         13:'等级 13',
         14:'等级 14',
         15:'等级 15',
         16:'等级 16',
         17:'等级 17',
         18:'等级 18',
         19:'等级 19',
         20:'等级 20',
         21:'等级 21',
         22:'等级 22',
         23:'等级 23',
         24:'等级 24',
         25:'等级 25',
     },

     category:{
         1:'步法速度____热身运动',
         2:'控球',
         3:'踢球技术、传球',
         4:'消减',
         5:'避开',
         6:'接球和运球',
         7:'界外球',
         8:'头球',
         9:'铲球',
         10:'攻击对手的技巧',
         11:'攻击对手的双控技巧',
         12:'背后攻击的对手的技巧',
         13:'射门得分',
     },

     course:{
         1:'基础课程',
         2:'精通课程',
         3:'对付对手的精通课程',
         4:'攻击对手课程',
         5:'优化课程',
     }, 

     exercises_categories :{
        1:'削减',
        2:'转身离开',
        3:'回切转弯组合',
        4:'球体控制',
        5:'控球/踢球技术/传球--组合',
        6:'接收并进行',
        7:'踢球技术/传球',
        8:'击败对手的技巧',
        9:'双重技能击败对手',
        10:'击败对手的技巧在你的背后',
        11:'射门',
      },



      exercises_description: {
        1:'试着查一下',
        2:'增加面积',
        3:'无球改变位置。冲刺。增加面积',
        4:'无球改变位置。冲刺。增加面积',
        5:'增加面积',
        6:'增加面积',
        7:'无球改变位置。冲刺。增加面积',
        8:'无球改变位置。冲刺。增加面积',
        9:'无球改变位置。冲刺。增加面积',
        10:'迈向球',
        11:'无球改变位置。一触即发',
        12:'没有任何附加信息',
        13:'传球前先看一下队友',
        14:'走向球。1分钟后更换队员。一触即发 - 1/2组合',
        15:'没有任何附加信息',
        16:'迈向球',
        17:'无球情况下改变位置。迈向球。一触即发 - 1/2组合',
        18:'无球情况下改变位置。迈向球。一触即发 - 1/2组合',
        19:'没有任何附加信息',
        20:'无球改变位置',
        21:'传球前先看一下队友',
        22:'2分钟后更换传球手',
        23:'传球前先看一下队友. 一触即发',
        24:'无球改变位置',
        25:'无球情况下改变位置。迈向球。一触即发 - 1/2组合',
        26:'无球情况下改变位置。迈向球。一触即发 - 1/2组合',
        27:'无球情况下改变位置。迈向球。一次触球',
        28:'迈向球。一次触球。1分钟后改变位置',
        29:'迈向球。一次触球',
        30:'无球情况下改变位置。迈向球。一触即发-2/3组合。1分钟后更换传球队员',
        31:'无球情况下改变位置。迈向球。一触即发-1/2组合。',
        32:'无球情况下改变位置。迈向球。一触即发-1/2组合。',
        33:'无球改变位置。无球移动',
        34:'无球情况下改变位置。迈向球。一触即发-1/2组合。',
        35:'无球改变位置',
        36:'无球改变位置。单次触球。迈向球',
        37:'无球改变位置。单次触球。迈向球',
        38:'迈向球。一触即发-2/2组合。1分钟后改变位置',
        39:'无球时改变位置。迈向球。一触即发-1/2组合',
        40:'无球时改变位置。迈向球。一触即发-1/2组合',
        41:'无球时改变位置。迈向球。一触即发-1/2组合',
        42:'无球时改变位置。迈向球。一触即发-1/2组合',
        43:'一触即发',
        44:'一触即发。无球改变位置',
        45:'一触即发。无球改变位置',
        46:'一触即发。无球改变位置',
        47:'增加面积。无球改变位置。一触即发 - 1/2组合。走向球。1分钟后更换队员',
        48:'增加面积。无球改变位置。一触即发--1/2组合。迈向球',
        49:'增加面积。一触即发--1/2组合',
        50:'无球移动。单次触摸',
        51:'无球移动。单次触摸',
        52:'无球改变位置',
        53:'无球改变位置',
        54:'无球改变位置',
        55:'无球改变位置',
        56:'一触即发',
        57:'无球改变位置。一触即发',
        58:'一触即发',
        59:'一触即发。无球改变位置。1分钟更换后卫',
        60:'无球改变位置。一触即发',
        61:'无球改变位置。一触即发',
        62:'无球改变位置。一触即发',
        63:'没有任何附加信息',
        64:'无球改变位置。一触即发',
        65:'一触即发',
        66:'一触即发。无球改变位置',
        67:'一触即发。1分钟后更换传球队员',
        68:'无球改变位置',
        69:'无球改变位置。一触即发',
        70:'无球改变位置。一触即发',
        71:'无球改变位置',
        72:'无球改变位置。一触即发',
        73:'没有任何添加物信息没有任何添加物信息',
        74:'迈向球。无球换位',
        75:'一触即发。迈向球。无球换位。1分钟后更换队员',
        76:'一触即发。迈向球。无球变位',
        77:'一触即发。迈向球。无球变位',
        78:'迈向球。无球换位',
        79:'一触即发。迈向球。无球变位',
        80:'一触即发',
        81:'一触即发。迈向球。无球变位',
        82:'一触即发。迈向球。无球变位',
        83:'一触即发。试着抬头看看',
    },




     alert:{
         text1:'要求场地',
         text2:'要求方法',
         text3:'运行价值要求',
     },

     textInfo:'你的团队没有队友，邀请队友加入',



 },



 // Navigations  
 navigations:{
     text1:'技巧细节',
 },
 


 Permissions:{

     text1:'拍摄许可',
     text2:'拍摄许可证',
     text3:'成功保存',
     text4:'拒绝访问',
     text5:'出现一个错误',
     text6:'必要权限不允许拍摄',
     text7:'未经证实',


 },


 tutorial:{
     text1:'通过这个主页你可以浏览你的评论与使用VFA（虚拟足球研究',
     text2:'聊天',
     text3:'发送一条私人信息给玩家',
     text4:'之前的',
     text5:'下一个',
     text6:'跳过访问',
     text7:'横向菜单',
     text8:'选项，VFA（虚拟足球研究）设置',
     text9:'完成',
     text10:'主页',
     text11:'在这个管理上你可以看到所有的技巧与训练',
     text12:'通知',
     text13:'查看你所有的通知，选择一个查看',
     text14:'评论',
     text15:'查看玩家的作业并且评价',
     text16:'统计',
     text17:'教练可以查看团队的统计信息',
     text18:'团队',
     text19:'教练可以看团队',
     // --
     text20:'训练计划',
     text21:'教练可以查看训练计划',
     // --
     text22:"更换脚，左脚换右脚",
     text23:"激活视频",
     text24:"更换语言",
     text25:"点击暂停键__播放3D",
     text26:"速度条",
     text27:"点击逐帧动画",
     text28:"更换拍照到增强现实技术（AR",
     text29:"更换到3D",


     


 },



 skillData:{
    alert:{
        text1:'日起要求',
    },

    notassigned:'这个技巧还没有分配给任何玩家',
    videofavorite:'你有最喜欢的视频',
    text1:'视频',
    text2:'玩家',
    text3:'喜欢',
    text4:'选择玩家，然后发送任务给他',
    send:'发送任务',
    text5:'Schedule',
 },

 //   Global coach 

 coach:{

     alert:{
         text1:'没有权限不允许拍摄',
         text2:'权限许可',
         text3:'necesita acceso a la galeria',
         text4:'无法访问权限',
         text5:'No se han otorgado los permisos necesarios para acceder a tu almacenamiento.',
         text6:'名字',
         text7:'选择玩法',
         text8:'任务',
         text9:'你的头像已经更新',
         text10:'更新时出现一个错误',
         text11:'请求不能进行',
         text12:'无法访问权限',
         text13:'出现一个错误',
         text14:'没有权限无法拍摄',
         text15:'访问权限',
         text16:'necesita acceso a la galeria',
         text17:"确定吗",
         text18:"这个步骤不可恢复，请确认你的决定",
         text19:'退出失败',
         text20:'你的头像已经更新',
         text21:'你的头像已经更新',
         text22:'更新时出现一个错误',
         text23:'你至少要更新一条个人信息',
         text24:'这个请求不能进行',
         text25:'场地要求',
         text26:'选择任务方式值',
         text27:'团队建立成功',
         text28:'你确定你要删除你的团队',
     },


     text1:'如果你不想再使用VFA（虚拟足球研究），你可以永久删除你的账号',
     text2:'删除账号',

     // Team 
     newTeam:'选择默认任务方式',
     buttom:'保存更新',
     selectImg:'选择照片或拍摄照片',
     sendButton:'发送邀请',


     // profile coach

     
     deleteMessage:'删除我的账号',
     messageInfodelete:'删除账号，你将会失去你的数据和所有视频，这个程序不可恢复，作为用户的独家专用，VFA（虚拟足球研究）对使用这个系统功能性没有责任',
     buttonCancel:'取消',
     buttonNext:'继续',
     editAccount:'编辑我的账户信息',

     formulario:{
         name:'名字',
         password:'密码',
         button:'更改'
     },







 },






 register:{
     titleView: '选择注册',
     h1:'选择你的头像',
     h2: '建立哪个账号',
     coach:'建立多个团队，邀请玩家并且单独管理每一个团队',
     player:'在AR中记录技巧非常有趣。成为一个专业玩家把',
     bottom: '建立账号',
 },
 welcomeCoach:{
     titleView: '教练账号',
     carrouselOneTitle: '储存100个技巧与83个训练',
     carrouselOneDescription: '根据目录，超过80个团队训练里有2个或3个玩家在单独训练这100个技巧',
     carrouselTwoTitle: '分配技巧作为任务',
     carrouselTwoDescription: '玩家可以在AR中记录技巧，他们会发送视频给你，之后你也可以从1到10来评价',
     carrouseThreeTitle:'Optimize your time with the Training Plan',
     carrouselThreeDescription:'组织培训性会议，在一个特定的日期分配技巧与训练，在达到一定高度之前，你的玩家可以查看训练',
     bottom: '继续',
 },
 welcomePlayer:{
    titleView: '玩家账号',
    carrouselOneTitle: '成为一个专业玩家',
    carrouselOneDescription: '在25个不同等级里，提高你玩游戏的100个技巧',
    carrouselTwoTitle: '在AR里记录技巧',
    carrouselTwoDescription: '与3D动画对比你演绎的技巧',
    carrouseThreeTitle:'分享你的账号',
    carrouselThreeDescription:'提高你的评估等级',
    bottom: '继续',
},

profile:{
    update:'你的头像已经更新',
    errorUpdate:'更新时出现一个错误',
    updateInfo:'你必须更新至少一条个人信息',
    required:'请求不能进行',
    textInfo:'更新你的账号信息',
    buttomSave:'保存更新',
    textUpdatePass:'更改密码',
    updatePhoto:'你的头像已经更新',
    remove:'删除玩家',

    // listado de los videos 
    listEmpty:'没有视频',
    listEmpty2:'这个部分是为了那些有教练的玩家准备的',


    menuProvider:{
        option1:'编辑头像',
        option2:'复制链接',
        option3:'分享链接',
        option4:'下载图片',
        option5:'复制头像网址',
        option6:'分享头像',
        option7:'家长监控',
       
    },
    // subir foto 
    messagePhoto:'头像照片',
    messajeModal:'选择图片或拍照',

    previaVideo:{
        option1:'复制视频链接',
        option2:'分享视频',
        option3:'删除'
    }
},

seguridad:{
    alert:{
        text1:'确定吗',
        text2:'这个程序不能恢复，请确认你的决定',
        buttonCancel:'取消',
        buttonOk:'确定',

    },

    text1:' 删除我的账号',
    text2:'删除账号之后，你将会失去你所有的数据与视频，这个程序不可恢复，作为用户的独家专用，VFA（虚拟足球研究）对使用这个系统功能性没有责任',
    button1:'取消',
    button2:'继续',
    text3:'如果你不在使用VFA(虚拟足球研究)，你将永久删除你的账号',
    text4:'删除账号',
},

ranking: {
 text1:'全球排名',
 text2:'最佳',
 text3:'最佳技巧',
},

videoPlayer:{
  text1:'同意',
  text2:'责备',
  text3:' 待处理',
},


training:{
    text1:'你不需要一个分配的教练',
    text2:'你确定你要删除这个日历项目',
    text3:'删除日历项目',
    text4:'没有赛事',
    text5:'训练计划'
  },


  video3D:{
    sendVideo:{
        title:'视频下载成功',
        title2:'VIDEO SENT TO YOUR COACH',

       description:'你现在可以评价你的视频了',
       description2:'Your coach will rate your video',


        text:'分享到你的网络平台',
        text2:'',

        btn1:'首页',
        btn2:'分享',
        message:'发送视频给教练',
        message2:'Video sent successfully.',


    },
    option1SinCoach:'上传视频',
    option1ConCoach:'发送视频',
    option2: '分享',
    option3: '保存',
    option4: '放弃'
},
bottomMenu:{
    Home: '首页',
    Reviews: '评价',
    My_Team: '我的团队',
    Training: '训练',
    Stats:'游戏统计',
    Profile:'头像',
    Ranking:'排名'
},

newText:{
    profileEdit: '编辑头像'
    
},

previVideoSend:{
    text1:'继续',
},


drawer:{
    text1:'儿童政策',
    text2:'联系方式',
    text3:'退出',
    text4:'隐私权政策',
    text5:'常见问题',
    text6:'不是分配队伍',
    text7:'不是分配教练',
    text8:'玩家',
    text9:'团队',
    text10:'头像',
    text11:'我的队伍',
    text12:'教练指导',
    text13:'学习指南',
    text14:'3D学习指南',
    text15:'退出'

},
skills:{
	1:'波状运动',
	2:'埃米尔动作',
	3:'握踝滚翻',
	4:'抢断球',
	5:'替换',
	6:'基本滑行',
	7:'背后铲球',
	8:'倒勾球',
	9:'胸部挡球',
	10:'倒地铲球',
	11:'联合',
	12:'交换',
	13:'双闪',
	14:'侧步躲闪',
	15:'双发快射',
	16:'V字推拉模式',
	17:'双之字型',
	18:'双向滚动',
	19:'双交移动',
	20:'拖拉剪刀脚',
	21:'拖拉',
	22:'020',
	23:'控球',
	24:'假停',
	25:'假踢反弹球',
	26:'假脚弓踢球',
	27:'假脚外侧反弹球',
	28:'假:脚外侧踢球',
	29:'弗伦基',
	30:'半倒勾球',
	31:'头球',
	32:'转身踢球',
	33:'倒钩踢球',
	34:'脚背控球',
	35:'脚背踢球',
	36:'脚背停球',
	37:'内弧线球',
	38:'内插入',
	39:'踢反弹球',
	40:'内侧脚踢球',
	41:'横传',
	42:'内侧回避',
	43:'脚背扣球',
	44:'脚内侧控球',
	45:'脚背踢反弹球',
	46:'颠球',
	47:'一只脚颠球',
	48:'踢转身球',
	49:'多方位踢球',
	50:'外侧插入',
	51:'脚外侧踢反弹球',
	52:'脚外侧踢球',
	53:'外弧线球',
	54:'外侧踢球',
	55:'外侧回避',
	56:'凌空踢球',
	57:'拉姆齐',
	58:'击球',
	59:'三步法',
	60:'侧飞球',
	61:'跑位',
	62:'后门',
	63:'连接球',
	64:'侧滑球',
	65:'侧步躲闪',
	66:'凌空横传',
	67:'精通横传',
	68:'内侧踢球',
	69:'滑球',
	70:'轻踢球',
	71:'铲球',
	72:'超级滚球',
	73:'中锋',
	74:'隐蔽替换',
	75:'转身',
	76:'交叉踢球',
	77:'闪躲',
	78:'倒钩滚球',
	79:'精通',
	80:'业余运动',
	81:'混合',
	82:'单步',
	83:'三步法',
	84:'击低近球',
	85:'名次表',
	86:'踢踏步法踢球',
	87:'趾行',
	88:'V字型踢球',
	89:'Vzi型变化',
	90:'截击空中球',
	91:'连续移动',
	92:'z字形运球',
	93:'z字型轻踢',
	94:'z字形',
	95:'假摔',
	96:'快速运球',
	97:'跳球',
	98:'长高球',
	99:'击低近球',
	100:'亚亚图雷',
	101:'迈过球',
},

textos: {
    text1: '技能',
    text2:'团体练习',
    text3:'按级别排序',
    text4:'按类别排序',
    text5:'按课程排序',
    text6:'2名球员的练习s',
    text7:'3名球员的练习',
    text8:'锻炼身体',
    text9:'促销代码',
    text10:'编码'


}


}

		

		 

		
	
	

	
















