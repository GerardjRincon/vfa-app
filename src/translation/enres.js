export default { 
   
    welcome:{
        title1: 'THE VIRTUAL FOOTBALL ACADEMY',
        descripcion1:'The Dutch method that changed football history!',
        title2:'REAL METHOD',
        descripcion2:'A real method with more than 25 years of development.',
        title3:'REAL LIFE MOTIONS',
        descripcion3:'We captured real movements of players performing the skills, represented in 3D and Augmented Reality.',
        Buton: 'CONTINUE',
    },
    login:{
        Bienvenida: 'Welcome to VFA',
        email: 'Email',
        helpEmail: 'Enter your email',
        password: 'Password',
        helpPass: 'Enter your password',
        login: 'LOG IN',
        account: 'New Here?',
        signUp: 'Create an account',
        recover: 'Forgot password?',
        // nuevos --------------------------------------------------------------------------
       // nuevos
       validateInputEmail:'Enter a valid e-mail address',
       validateInputs:'Enter all fields',
       errorPasswordValidate:'Password must be at least 8  characters, including: one uppercase letter and one number.',
        // ---------------------------------------------------------------------------------
    },


    /////////  player 
    homeplayer:{
        alert:{
            nosuscription:'You do not have an active subscription',
            fileLogout:'Failed to logout',

        }, 

        modalInicioPlayer:{
            text1:'For the first 14 levels of the program, we always start with learning a warming up skill which doubles as ball feeling, flexibility, and speed of footwork skill. These skills are essential to acquire ball feeling to perform the other, and more difficult skills, in the levels ahead and at the same time you get warmed up and ready for your individual practice and or AR video recording.',
            text2:'For the remaining 21 levels we recommend that you warm up with all the previous learned warming up skills.',
            text3:'Further to that we add categories and skills in the following levels according to the learning sequence of our technical football skills development program which will cover all the techniques of the modern game of football. You can practice at your own pace and try to learn as much as you can. It will get more difficult the further you move through the levels. Guaranteed you will see the result of your hard work in the games you play.',
            buttom:'continue',
        },

        modalNextLevel:{
            text1:'Level',
            text2:'unlocked',
            text3:'You are better player every day! Keep developing your skills!',
            buttom:'continue',
        }
        

    },


    // home coach 

    homecoach:{
        alert:{
            nosuscription:'You do not have an active subscription',
            fileLogout:'Failed to logout',

        }, 

    },

    TutorialView:{
        niveltitle:'Level',
        buttom:'Continue',
        level1:{
            text1:'Ball feeling and ball control are the cornerstones of how to learn technical football skills. To get you started we made it easy for you, as this is the first skill of the level program, it is just to get you acquainted with the way we would like you to work/perform the skills.',
            text2:'Always perform the skills with both feet, so therefore you start with the Tick Tock, an easy to perform skill, touching the ball with both feet while moving up and down the field. The Zig Zag tap is like the Tick Tock except you move in a zig zag way and not straight up and down.',
            text3:'We will also make a start  with controlling and passing the ball, these are essential skills needed  and the perfect basics of playing football. Always practice with both feet! '
        },
        level2:{
            text1:'There are four different directions where you can go to with the ball when you are challenged by an opponent, backwards, to the left, to the right and forwards. The previous this is the most logical sequence. So, we start with going backwards, the Cutback skills. ',
            text2:'You are not only changing direction, but you also shield the ball from the oncoming opponent and keep ball possession.',
            text3:'Receiving and proceeding with the ball is very important to get and stay away from your opponent and to handle the ball in motion, we make a start with that here in level 2.'
        },
        level3:{
            text1:'Going up a notch, we are now combining the two previous warming up skills in the V Tick Tock skill and learning a new one, using the instep of your foot. ',
            text2:'Going sideways now, with the Turn Away skills you will learn how to change direction to the left or right if approached by an opponent.',
        },
        level4:{
            text1:'You may have noticed that we use all parts of the foot in our warming up skills, now we are using the inside and the sole of your foot. As you have already learned how to dribble with the ball, we now want you to speed that up, go fast and do not forget to look up now and again. ',
            text2:'The Outside Cut is a very tricky skill to learn but oh so important. By learning this skill, you will now be able to shield the ball from an opponent and go backwards with your inside and outside foot!',
        },
        level5:{
            text1:'In this level we learn another Turn Away skill, you can now go sidewards, hence Turn Away, using the inside and outside of your foot if approached by an opponent.',
            text2:'Think about it, you are now able to change direction backwards and sidewards and keep ball possession if an opponent comes at you. Another step upwards with the Instep Kick, the Instep Kick is not only needed for shooting on the goal but also to give passes over a longer distance.',
        },
        level6:{
            text1:'A new featured skill, The Throw In. It looks easier than it is as you will notice. Keep your feet on the ground and behind the line! Taking the ball sideways with the outside of your foot takes a lot of practice but this skill is essential for controlling and proceeding with the ball in one motion. ',
            text2:'Remember always receive and proceed with the ball in motion and move into a free space.',
        },
        level7:{
            text1:'The warming up skills is getting more difficult now especially with the Ankle Roll, rolling over the ball requires a lot of ball feeling.',
            text2:'And what about the Juggling , we start with one leg only, by juggling with the ball you will also increase your ball feeling and it is fun at the same time, challenge your friends! Did you notice that some of the Turn Away and Cutback skills are practiced in a different way in the warming up skills, we did this to prepare you better.',
        },

        level8:{
            text1:'In this level we are giving you some more options on how to control and turn away with the ball and combining some previous warming up skills. Hint, in the Turn make sure you turn around a 180’ to go back to where you came from.',
        },

        level9:{
            text1:'This is one of my favourite skills, The Toe Walk. Walking with the ball, touching it with your toes only and looking up and around you, try not to look at the ball, it’s FUN.',
        },

        level10:{
            text1:'You are getting very advanced now in the Warming up skills and throwing in the ball with a run up. The Dummy Stop skill is very effective in tricking your opponent, especially when you make a vigorous kicking move (a fake kick)!',
        },

        level11:{
            text1:'In the warming ups skills, we are going to do some fancy footwork now. Here we learn a very famous and effective skill called The Maestro, named after one of the best players in the world, The Dutch football player Johan Cruyff. Many of the top-class players currently playing copied that move.',
        },
        level12:{
            text1:'You are almost halfway through the levels, well done! Keep practicing and repeating the previous skills. “Practice makes perfect”.',
        },
        level13:{
            text1:'In this level there is a big challenge, The Basic Sliding tackle. Start practicing this very carefully and gradually increasing the intensity of your practice. Bear in mind in modern football even the attacking players know how to use the sliding tackle. ',
        },
        level14:{
            text1:'Besides a fancy warming up/ball feeling skill you now need to practice a long high ball; this requires a lot of concentration and focus to deliver an accurate pass. ',
            text2:'Start practicing on short distance passing and gradually increase. Proceed with the ball taking it behind your standing leg, in the same way “Zidane” used to do.',
        },
        level15:{
            text1:'We are introducing some new skills here, very difficult ones! To start with receiving and proceeding with the ball with a dropkick technique, this requires a lot of ball feeling. Heading, practice this very carefully and easy.',
            text2:'Gradually increase distance between your partner/friend and you. Do not get frustrated, learning how to head a ball takes time! It is juggling time now but with two legs, this is considered as being very skilful and it is fun to do. Try to get on the Global ranking with this, it is a challenge!',
        },
        level16:{
            text1:'In this level we are going to beat the opponent, going forwards. We make a start with learning single and double skills to beat an opponent in a 1 v 1 situation. Beating an opponent is one of most difficult aspects of football.',
            text2:'Single and double skills mean that you can beat an opponent on any side, being unpredictable in your moves. The Inswinger is in modern football an important skill, curling a free kick into the net is done with an Inswinger. It takes a lot of practice and effort to perfect the inswinger so don’t give up! Go easy on the sliding!',
        },
        level17:{
            text1:'Here we give you more options in being unpredictable in beating your opponent in the 1 v 1. We also learn here skills to beat an opponent when he is in your back, especially strikers/forwards need to do this. This will make you an outstanding player.',
        },
        level18:{
            text1:'Another Dropkick skill but this time with the outside of your foot, very difficult and it might take a bit longer to learn. Now take it easy in the beginning with practicing the Jumping Header.',
        },
        level19:{
            text1:'The Outswinger is a very advanced and skilful kicking/passing technique, very efficient when mastered. This is the last of the sliding/tackle you need to learn, it makes you a more complete player. ',
            text2:'By now you should be able to feel comfortable with the skills to beat an opponent, however you need to keep practicing them regularly, and increase the speed of performing the skills.',
        },
        level20:{
            text1:'In this level we are introducing the fake move, this means making a fake move before you receive the ball to trick your opponent. Heading the ball with a run up requires great timing, you get that by practicing a lot!',

        },
        level21:{
            text1:'Some more skills to beat an opponent that the great players make and do.',
            
        },
        level22:{
            text1:'Hang in there, The Volley is definitely a skill you need to learn, spectacular kicking/shooting skill.',
            
        },
        level23:{
            text1:'You are nearing the end, keep practicing.',
            
        },
        level24:{
            text1:'Almost there, after this level one more!',
            
        },
        level25:{
            text1:' Great achievement, fantastic, 25 levels with 100 skills and you did them all. You made VFA proud! We will follow you in the rankings. Keep sharing your AR videos and achievements with us on Social Media. More to come soon, keep watching!',
            
        },

    },

    ar:{
        alert: {
            permitcamera:'Camera permit',
            msgpermitcamera:'VFA needs access to the camera',
            textsuccessfully:'Saved successfully.',
            textdenied:'Permission denied.',
            hasoccurred:'An error has occurred.',
            necessarypermissions:'Necessary permissions have not been granted to store files.',
            uploading:'Uploading video please wait...',
            errorUploading:'Error video upload',
            notassigned:'Skill not assigned',
            desnotassigned:'Your trainer has not assigned you this task.',
            allowedpost:'You are not allowed to post',
            needaprove:'You need to aprove the previous levels to be able to post this video.',
            thevideocoach:'The video is being sent to the coach.',
            postvideo:'Post video',
            cancel:'Cancel',


        },

        shareSocial:{
            text1:'Help to',
            // en medio de estos dos aparece el username 
            text2:'rating her video skill! ',

        },

    },

    chat:{
        messajeInfo:'You do not have an assigned coach',
        placeholderMessage:'Write a message'
    },


    notifications:{
        messageInfo:'No notifications available',
        alerMessage:'You sure want to delete the notification?'
    },

    previaNotifications:{
        record:'Record again',
        // oracion de los votos 
        text1:'You need',
        text2:'positive votes to get approved!',

        buttonRecord:'RECORD',
    },


    myTeam:{

        menssagePage:'There are no players in your team, please invite them to join'

    },




  




    registerCoach:{
        titlePage: 'Create coach account',
        playerTab: 'PLAYER',
        coachTab: 'COACH',
        alert:{
            alertDate: 'Date of birth is required',
            alertSubmit: 'You must accept the terms and conditions',
            alertSubmitPlayer: 'Credentials are incorrect',

            // Nuevas --- alexis  --------------------------------------------------------
            alertEmailalready:'The email has already been taken',
            errorMessage:'An error has occurred',
            errorPasswordValidate:'Password must be at least 8  characters, including: one uppercase letter and one number',
            //   ----------------------------------------------------------------------------
        },
       
        player:{
            titlePage: 'Create player account',
            title:'TRY IT FOR FREE',
            descripcion: 'Create your account to unlock 7 days for FREE.',
            coachVirtual: 'The virtual trainer will guide you through the program.',
            coachPersonal: 'You will join your trainer and your team',
            optionCoachVirtual:'I want a Virtual coach',
            optionCoachPersonal:'I have a coach',
            optionSelect:'choose the correct option',
            name:'Name',
            helpName:'Enter your name',
            email:'Email',
            helpEmail:'Enter your email',
            password: 'Password',
            helpPass: 'Enter your password',
            date: 'Date of birth',
            text1: 'I agree the',
            text2: 'Privacy policy',
            text3: 'and',
            text4: 'Terms and conditions.',
            submit: 'Submit',
        },
        coach:{
            titlePage: 'Create player account',
            title:'TRY IT FOR FREE',
            descripcion: 'Create your account to unlock 7 days for FREE.',
            name:'Full Name',
            helpName:'Enter your full name',
            email:'Email',
            helpEmail:'Enter your email',
            password:'Password',
            helpPass:'Enter your password',
            text1: 'I agree the',
            text2: 'Privacy policy',
            text3: 'and',
            text4: 'Terms and conditions.',
            submit: 'Submit',
        }
    },
    registerPlayer:{
        titlePage: 'Create coach account',
        playerTab: 'PLAYER',
        coachTab: 'COACH',
        alert:{
            alertDate: 'Date of birth is required',
            alertSubmit: 'You must accept the terms and conditions',
            alertSubmitPlayer: 'Credentials are incorrect',

            // nuevo 
            validateAlertCredencial:'The credentials are wrong',
            messageError:'An error has occurred',

        },
        // Nuevo  ----------------------------------------------------------------------------
        childrenPolicy:{

          title:'CHILDRENS POLICY COPPA',
          text1:'Notice to Parents',
          text2:'In compliance with the Children’s Online Privacy Protection Act (COPPA), parents (or legal guardians) of children under 13 years of age must consent to collections, uses and disclosures of the personal information of their children collected by Sports Innovations LLC products and services such as “Virtual Football Academy” VFA.',
        //   text3:'In compliance with the Children’s Online Privacy Protection Act (COPPA), parents (or legal guardians) of children under 13 years of age must consent to collections, uses and disclosures of the personal information of their children collected by Sports Innovations LLC products and services such as “Virtual Football Academy” VFA.',
          text4:'The payment of a subscription for VFA, including the use of a  credit card, will be considered express consent to this Privacy Policy.',
          text5:'Exceptions to prior parental consent. Verifiable parental consent is required prior to any collection, use, or disclosure of personal information from a child except as set forth in this paragraph:',
          text6:'Where the purpose of collecting a parent’s online contact information is to provide voluntary notice to, and subsequently update the parent about, the child’s participation in a Web site or online service that does not otherwise collect, use, or disclose children’s personal information. In such cases, the parent’s online contact information may not be used or disclosed for any other purpose. In such cases, the operator must make reasonable efforts, taking into consideration available technology, to ensure that the parent receives notice as described in §312.4(c)(2);',
          text7:'More information',
          emailPadre:'Please enter a correct email',
          textTerminos1:'I Agree the',
          textTerminos2:'PRIVACY OF CHILDREN',
          AcceptTerms:'Please accept children privacy to continue.',
          
        },
        //  ------------------------------------------------------------------------------------

        player:{
            titlePage: 'Create player account',
            title:'TRY IT FOR FREE',
            descripcion: 'Create your account to unlock 7 days for FREE.',
            coachVirtual: 'The virtual trainer will guide you through the program.',
            coachPersonal: 'You will join your trainer and your team',
            optionCoachVirtual:'I want a Virtual coach',
            optionCoachPersonal:'I have a coach',
            optionSelect:'choose the correct option',
            name:'Name',
            helpName:'Enter your name',
            email:'Email',
            helpEmail:'Enter your email',
            password: 'Password',
            helpPass: 'Enter your password',
            date: 'Date of birth',
            text1: 'I agree the',
            text2: 'Privacy policy',
            text3: 'and',
            text4: 'Terms and conditions.',
            // Nuevo
            // -----------------------------------------------------------------
            submit: 'CONTINUE',
            validateInputEmail:'Enter a valid e-mail address',
            errorPasswordValidate:'Password must be at least 8  characters, including: one uppercase letter and one number',
            // -------------------------------------------------------------------
        },
        //  Nuevo  ---------------------------------------------------------------
        payPlayer:{
            title:' Continue to unlock your 7 days for free!',
            Permonth:'Per month',
            // PARRAFO DEL FINAL DE LOS PLANES PLAYER 
            text1Android: 'Cancel at any time on Google Play',
            text1IOS: 'Cancel at any time on Apple ID / Suscriptions',
            text2:'Your subscription will renew for the same period of time unless you cancel it at least 24 hours before the end of the current period. You can cancel at any time from',
            text3:'Apple ID / Suscriptions',
            text4:'at no additional cost. By doing so, your subscription will end at the end of the current period.',
            text5:'Best Value',
            text6:'months',
            text7:'month',
            bottom:"START MY FREE TRIAL",
            textSave:'Save '
        },

        
        // --------------------------------------------------------------------------

    //   Global coach 
     
        coach:{

            titlePage: 'Create player account',
            title:'TRY IT FOR FREE',
            descripcion: 'Create your account to unlock 7 days for FREE.',
            name:'Full Name',
            helpName:'Enter your full name',
            email:'Email',
            helpEmail:'Enter your email',
            password:'Password',
            helpPass:'Enter your password',
            text1: 'I agree the',
            text2: 'Privacy policy',
            text3: 'and',
            text4: 'Terms and conditions.',
            submit: 'Submit',
        }
    },


    // Reviews

    reviews:{
       text1:'An unexpected error has occurred',
       text2:'No pending reviews',
    },


    // my teams 

    teams:{

        levels: {
            text1:'Level 1',
            text2:'Level 2',
            text3:'Level 3',
            text4:'Level 4',
            text5:'Level 5',
            text6:'Level 6',
            text7:'Level 7',
            text8:'Level 8',
            text9:'Level 9',
            text10:'Level 10',
            text11:'Level 11',
            text12:'Level 12',
            text13:'Level 13',
            text14:'Level 14',
            text15:'Level 15',
            text16:'Level 16',
            text17:'Level 17',
            text18:'Level 18',
            text19:'Level 19',
            text20:'Level 20',
            text21:'Level 21',
            text22:'Level 22',
            text23:'Level 23',
            text24:'Level 24',
            text25:'Level 25',
        },

        category:{
            text1:'SPEED OF FOOTWORK - WARMING UP',
            text2:'BALL CONTROL',
            text3:'KICKING TECHNIQUE / PASSING',
            text4:'CUTBACK',
            text5:'TURN AWAY',
            text6:'RECEIVE AND PROCEED',
            text7:'THROW IN',
            text8:'HEADING',
            text9:'SLIDING TACKLE',
            text10:'SKILLS TO BEAT AN OPPONENT',
            text11:'DOUBLE SKILLS TO BEAT AN OPPONENT',
            text12:'SKILLS TO BEAT AN OPPONENT IN YOUR BACK',
        },

        course:{
            text1:'BASIC COURSE',
            text2:'MASTER THE BALL COURSE',
            text3:'MASTER THE OPPONENT COURSE',
            text4:'BEAT THE OPPONENT COURSE',
            text5:'PERFECTION COURSE',
        }, 

        alert:{
            text1:'The fields are required',
            text2:'The method working is required',
            text3:'The value to working is required',
        },

        textInfo:'There are no players in your team, please invite them to join',



    },



    // Navigations  
    navigations:{
        text1:'Skill details',
    },
    


    Permissions:{

        text1:'Permiso Camara',
        text2:'Necesita acceso a la camara',
        text3:'Saved successfully.',
        text4:'Permiso denegado.',
        text5:'Ha ocurrido un error.',
        text6:'No se han concedido los permisos necesarios para acceder a la camara.',
        text7:'Unauthenticated.',


    },


    tutorial:{
        text1:'Through this main menu you can navigate between views and use vfa',
        text2:'CHAT',
        text3:'Send a private message to the player(s)',
        text4:'Previous',
        text5:'Next',
        text6:'Skip tour',
        text7:'LATERAL MENU',
        text8:'Options, vfa settings',
        text9:'Finish',
        text10:'HOME',
        text11:'Here you can see all the skills and exercises organized simply for your management',
        text12:'NOTIFICATIONS',
        text13:'See all your notifications, choose one to see',
        text14:'REVIEWS',
        text15:'Check the players homework and rate it',
        text16:'STATISTICS',
        text17:'Coach can see the statistics of the team',
        text18:'TEAMS',
        text19:'Coach can see the teams',
        // --
        text20:'TRAINING PLAN',
        text21:' Coach can view the training plan schedule',
        // --
        text22:"Change foot:Left foot - Right foot.",
        text23:"Activate the audio.",
        text24:"Change the language.",
        text25:"Tap to pause - play the 3D.",
        text26:"Speed bar.",
        text27:"Tap to frame by frame",
        text28:"Change the camera to AR",
        text29:"Change to 3D",


        


    },



    skillData:{
       alert:{
           text1:'The date is required',
       },

       notassigned:'This skill has not been assigned to any player',
       videofavorite:'No favorite videos',
       text1:'VIDEOS',
       text2:'PLAYERS',
       text3:'FAVORITES',
       text4:'Select player to send homework',
       send:'Send homework'
    },

    //   Global coach 

    coach:{

        alert:{
            text1:'No se han concedido los permisos necesarios para acceder a la camara.',
            text2:'Permiso Galeria',
            text3:'Tamyda necesita acceso a la galeria',
            text4:'Permiso denegado.',
            text5:'No se han otorgado los permisos necesarios para acceder a tu almacenamiento.',
            text6:'Name is required',
            text7:'Select the working method',
            text8:'Working is required',
            text9:'Your profile picture was updated',
            text10:'An error occurred while updating',
            text11:'The request could not be processed',
            text12:'Permiso denegado.',
            text13:'Ha ocurrido un error.',
            text14:'No se han concedido los permisos necesarios para acceder a la camara.',
            text15:'Permiso Galeria',
            text16:'Tamyda necesita acceso a la galeria',
            text17:"Are you sure?",
            text18:"This process is not reversible. Please confirm your decision",
            text19:'Failed to logout',
            text20:'Your profile info was updated',
            text21:'Your profile info was updated',
            text22:'An error occurred while updating',
            text23:'You must update at least one piece of information',
            text24:'The request could not be processed',
            text25:'The fields are required',
            text26:'Choose the value of the working method',
            text27:'Team successfully created',
            text28:'Are you sure that you want delete the team?',
        },


        text1:'If you no longer want to use the VFA, you can permanently delete your account.',
        text2:'Delete account',

        // Team 
        newTeam:'Select the default working method:',
        buttom:'Save Changes',
        selectImg:'Select an image or capture a photo',
        sendButton:'Send Invitation',



        // profile coach

        
        deleteMessage:'Delete my account',
        messageInfodelete:'By deleting the account you will lose your access data and all the videos you have in your profile will be deleted. This process is not reversible, VFA is not responsible for the use of this functionality of the system as it is for the exclusive use of the user.',
        buttonCancel:'CANCEL',
        buttonNext:'CONTINUE',
        editAccount:'Edit my account info',

        formulario:{
            name:'Name',
            password:'Password',
            button:'Change'
        },







    },






    register:{
        titleView: 'Select to register',
        h1:'Select your profile!',
        h2: 'Under which your account will be created',
        coach:'Create multiple teams, invite players, and manage each team independently.',
        player:'Have fun recording skills in AR and become a pro!',
        bottom: 'CREATE ACCOUNT',
    },
    welcomeCoach:{
        titleView: 'Account Coach',
        carrouselOneTitle: 'Library of 100 skills and 83 exercises',
        carrouselOneDescription: 'Over 80 group exercises of 2 and 3 players to practice the 100 individual skills according to their categories.',
        carrouselTwoTitle: 'Assign skills as homework!',
        carrouselTwoDescription: 'Players can record skills in Augmented Reality, they will send you the video and then you can rate them from 1 to 10.',
        carrouseThreeTitle:'Optimize your time with the Training Plan',
        carrouselThreeDescription:'Organize the training sessions, assign skills and exercises for a particular date. Your player will be able to see the exercises before reaching the pitch.',
        bottom: 'CONTINUE',
    },
    welcomePlayer:{
        titleView: 'Account Player',
        carrouselOneTitle: 'Become a Pro!',
        carrouselOneDescription: 'Improve your skills and your game with 100 skills based on 25 levels of difficulty.',
        carrouselTwoTitle: 'Record the skills in AR!',
        carrouselTwoDescription: 'Compare your performance of the skill against the 3D animation.',
        carrouseThreeTitle:'Share your account',
        carrouselThreeDescription:'Get rated and improve your rankings.',
        bottom: 'CONTINUE',
    },

    profile:{
        update:'Your profile info was updated',
        errorUpdate:'An error occurred while updating',
        updateInfo:'You must update at least one piece of information',
        required:'The request could not be processed',
        textInfo:'Update your account information',
        buttomSave:'SAVE CHANGES',
        textUpdatePass:'Change password',
        updatePhoto:'Your profile picture was updated',
        remove:'Remove player',

        // listado de los videos 
        listEmpty:'No videos',
        listEmpty2:'This section is available only for the use of players with coach',


        menuProvider:{
            option1:'Edit profile',
            option2:'Copy link',
            option3:'Share link',
            option4:'Upload photo',
            option5:'Copy profile URL',
            option6:'Share this profile',
            option7:'Parental control',
           
        },
        // subir foto 
        messagePhoto:'Profile image',
        messajeModal:'Select an image or capture a photo',

        previaVideo:{
            option1:'Copy link video',
            option2:'Share video',
            option3:'Delete'
        }
    },

    seguridad:{
          alert:{
              text1:'Are you sure?',
              text2:'This process is not reversible. Please confirm your decision',
              buttonCancel:'Cancel',
              buttonOk:'OK',

          },

          text1:' Delete my account',
          text2:'By deleting the account you will lose your access data and all the videos you have in your profile will be deleted. This process is not reversible, VFA is not responsible for the use of this functionality of the system as it is for the exclusive use of the user.',
          button1:'CANCEL',
          button2:'CONTINUE',
          text3:'If you no longer want to use the VFA, you can permanently delete your account.',
          text4:'Delete account',
    },

    ranking: {
       text1:'Global ranking',
       text2:'Best of',
       text3:'Top skills',
    },

    videoPlayer:{
        text1:'Approved',
        text2:'Reproved',
        text3:' Pending',
    },

    training:{
      text1:'You do not have an assigned coach',
      text2:'You sure want to delete the agenda item?',
      text3:'Delete agenda item',
      text4:'No events',
      text5:'Training Plan'
    },




    video3D:{
        sendVideo:{
            title:'VIDEO UPLOADED SUCCESSFULLY',
            description:'Now your video can be rated',
            text:'Share to your Social Networks!',
            btn1:'HOME',
            btn2:'Share',
            message:'Video send to coach'
        },
        option1SinCoach:'Post video',
        option1ConCoach:'Send to coach',
        option2: 'Share',
        option3: 'Save',
        option4: 'Discard'
    },
    bottomMenu:{
        Home: 'Home',
        Reviews: 'Reviews',
        My_Team: 'My Team',
        Training: 'Training',
        Stats:'Stats',
        Profile:'Profile',
        Ranking:'Ranking'
    },

    NewText:{
        profileEdit: 'edit profile'
        
    },


    drawer:{
         text1:'Children Policy',
         text2:'Contact',
         text3:'Log out',
         text4:'Privacy policy',
         text5:'FAQ',
         text6:'Not assigned team',
         text7:'Not assigned coach',
         text8:'Players',
         text9:'Teams',
         text10:'Profile',
         text11:'My Teams',
         text12:'Coach Guide',
         text13:'Tutorial',
         text14:'Tutorial 3D',
         text15:'Logout'

     },


    skills: {
        1: "The Wave",
        2: "The Amir",
        3: "The Ankle Roll",
        4: "The Ankle Tap Roll",
        5: "The Swop",
        6: "The Basic Sliding",
        7: "Behind Standing Leg",
        8: "Bicycle Kick",
        9: "Chest Control",
        10: "The Clamp Sliding",
        11: "The Combination",
        12: "The Reverse",
        13: "The Double Flash",
        14: "The Double Sidestep",
        15: "The Double Tap",
        16: "The Double V",
        17: "The Double Zigzag",
        18: "The Double Roll",
        19: "The Double Scissor",
        20: "The Dragging Scissor",
        21: "The Drag",
        22: "same as 020",
        23: "Dribbling",
        24: "The Dummy Stop",
        25: "Fake Inside Foot Dropkick TBS",
        26: "Fake Inside Foot TBS",
        27: "Fake Outside Foot Drop Kick TBS",
        28: "Fake Outside Foot TBS",
        29: "The Frenkie",
        30: "Half Bicycle Kick",
        31: "Heading Standing",
        32: "The Hip Pivot",
        33: "The Hook Roll",
        34: "Instep Control",
        35: "Instep Kick",
        36: "The Instep Stop",
        37: "Inswinger",
        38: "The Inside Cut",
        39: "Inside Foot Dropkick TBS",
        40: "Inside Foot Kick",
        41: "The Sideways",
        42: "The Inside Turn Away",
        43: "The Instep Tap",
        44: "Inside Foot Control",
        45: "Instep Dropkick",
        46: "Juggling",
        47: "Juggling with one leg",
        48: "The Go Around",
        49: "The Musa",
        50: "The Outside Cut",
        51: "Outside Foot dropkick TBS",
        52: "Outside Foot Kick",
        53: "Outswinger",
        54: "Outside Foot TBS",
        55: "The Outside Turn Away",
        56: "The Turn",
        57: "The Ramzy",
        58: "The Stroke",
        59: "The Tri Step",
        60: "The Swing",
        61: "The Roll Off",
        62: "The Backdoor",
        63: "The Junction",
        64: "The Shuffle",
        65: "The Sidestep",
        66: "The Sideways Half Turn",
        67: "The Sideways Maestro",
        68: "Inside Foot TBS",
        69: "The SkateBall",
        70: "The Skim Kick",
        71: "The Spread Sliding",
        72: "The Super Roll",
        73: "The Pivot",
        74: "The Switch Trick",
        75: "Turn Away",
        76: "The V",
        77: "The Flash",
        78: "The Hook Roll",
        79: "The Maestro",
        80: "The Midget",
        81: "The Mix",
        82: "The Step Over",
        83: "The Three Taps",
        84: "Run up",
        85: "Standing",
        86: "The Tick Tock",
        87: "The Toe Walk",
        88: "The V Tick Tock",
        89: "The V Variation",
        90: "Volley",
        91: "The Flow",
        92: "Zigzag Dribbling",
        93: "The Zigzag Tap",
        94: "The Zigzag",
        95: "Dive",
        96: "Faster Dribbling",
        97: "Jumping",
        98: "Long High Ball",
        99: "Run up",
        100: "The Yaya",
        101: "The Step Over",
      },






};