export default {  
    Bienvenida: 'Welcome to VFA',
    email: 'Email',
    helpEmail: 'Enter your email',
    password: 'Password',
    helpPass: 'Enter your password',
    login: 'Login',
    account: 'Already an Account?',
    signUp: 'Sign Up',

    // nuevos
    validateInputEmail:'Enter a valid e-mail address',
    validateInputs:'Enter all fields'
  };