export default { 
    Bienvenida: '欢迎来到 VFA',
    email: '电子邮件',
    helpEmail: '输入你的电子邮件',
    password: '密码',
    helpPass: '输入你的密码',
    login: '登录',
    account: '已经有一个账户?',
    signUp: '注册',

     // nuevos
     validateInputEmail:'Enter a valid e-mail address',
     validateInputs:'Enter all fields'
  };