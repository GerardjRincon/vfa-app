export default {  
    Bienvenida: 'Bienvenido a VFA',
    email: 'Correo',
    helpEmail: 'Ingresa tu correo',
    password: 'Contraseña',
    helpPass: 'Ingresa tu contraseña',
    login: 'Acceder',
    account: '¿Ya tienes una cuenta?',
    signUp: 'Crear cuenta',

     // nuevos
     validateInputEmail:'Enter a valid e-mail address',
     validateInputs:'Enter all fields'
     
  };