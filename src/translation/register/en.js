export default {  
    titleView: 'Select to register',
    h1:'¡Select the profile!',
    h2: 'Under which your account will be created',
    bottom: 'CREATE ACCOUNT',
};