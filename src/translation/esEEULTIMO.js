export default { 
    
    texto4:"Codigo de activación incorrecto, verifica que lo hayas escrito bien.",
   
    welcome:{
        title1: 'THE VIRTUAL FOOTBALL ACADEMY',
        descripcion1:'El método holandés que cambió la historia del futbol!',
        title2:'UN MÉTODO CON HISTORIA',
        descripcion2:'VFA está basado en el método de Wiel Coerver, con 25 años de desarrollo y miles de niños practicándolo en los mejores clubs del mundo.',
        title3:'MOVIMIENTOS REALES',
        descripcion3:'Hemos capturado los movimientos de los mejores jugadores representándolos en 3D y Realidad Aumentada.',
        Buton: 'CONTINUAR',
    },
    login:{
        Bienvenida: 'Bienvenido a VFA',
        email: 'Email',
        helpEmail: 'Tu email',
        password: 'Contraseña',
        helpPass: 'Introduce tu contraseña',
        login: 'LOGIN',
        account: 'Eres nuevo?',
        signUp: 'Crear una cuenta',
        recover: 'Contraseña perdida?',
        // nuevos --------------------------------------------------------------------------
       // nuevos
       validateInputEmail:'Introduce un email válido o ',
       validateInputs:'Rellena todos los campos',
       errorPasswordValidate:'La contraseña tiene que tener mínimo 8 carácteres, incluyendo mayúscula y un número.',
        // ---------------------------------------------------------------------------------
    },


    /////////  player 
    homeplayer:{
        alert:{
            nosuscription:'No tienes una suscripción activa',
            fileLogout:'Fallo al cerrar la sesión',

        }, 

        modalInicioPlayer:{
            text1:'En los primeros 14 niveles del método VFA, comenzamos realizando un skill (habilidad) de calentamiento que también funciona para adquirir flexibilidad y velocidad de juego de pies. Estos skills son esenciales para realizar las demás skills mas difíciles.',
            text2:'Para los 21 niveles restantes, recomendamos que calientes con las skills de calentamiento aprendidas anteriormente.',
            text3:'Además de esto, agregamos skills en los siguientes niveles de acuerdo con la secuencia de aprendizaje de nuestro programa de desarrollo de habilidades técnicas que cubrirá todas las técnicas del juego de fútbol moderno.',
            buttom:'continuar',
        },

        modalNextLevel:{
            text1:'Nivel',
            text2:'desbloqueado!',
            text3:'Eres mejor jugador cada dia! Mantén la calidad entrenando los skills!',
            buttom:'continuar',
        }
        

    },


    // home coach 

    homecoach:{
        alert:{
            nosuscription:'No tienes una suscripción activa',
            fileLogout:'Fallo al cerrar la sesión',

        }, 

    },

    TutorialView:{
        niveltitle:'Nivel',
        buttom:'Continuar',
        level1:{
            text1:'La sensación y el control del balón son las piedras angulares de cómo aprender las habilidades técnicas del fútbol. Para comenzar, te lo pondremos fácil, ya que esta es el primer skill del método VFA, es solo para que se familiarice con la forma en vamos a trabajar.',

            text2:'Realiza siempre los skills con ambos pies, por lo tanto, comience con el Tick Tock, un skill fácil de realizar, tocando la pelota con ambos pies mientras se mueve hacia arriba y hacia abajo por el campo. El toque en Zig Zag es como el Tick Tock, excepto que te mueves en zig zag y no hacia arriba y hacia abajo.',

            text3:'También comenzaremos controlando y pasando el balón, estas skills son esenciales como conceptos básicos para jugar al fútbol. ¡Practica siempre con ambos pies!',
        },

        level2:{
            text1:'Hay cuatro direcciones diferentes en las que puedes ir con el balón cuando un oponente te desafía, hacia atrás, hacia la izquierda, hacia la derecha y hacia adelante. Lo anterior esta es la secuencia más lógica. Entonces, comenzamos yendo hacia atrás, las skills de Cutback.',

            text2:'No sólo cambias de dirección, sino que también proteges el balón del adversario que se aproxima y mantienes la posesión del mismo.',

            text3:'El Control Orientado con el balón es muy importante para conseguir y mantenerte alejado de tu oponente y para conducir el balón en movimiento. Empezamos con esto aquí en el nivel 2.'
        },
        level3:{

            text1:'Subiendo de nivel, ahora combinamos los dos skills de calentamiento anteriores en el skill V Tick Tock y aprendemos una nueva, utilizando el empeine del pie.',

            text2:'Ahora vamos de lado, con los skills de cambio de dirección aprenderás a cambiar de dirección a la izquierda o a la derecha si se te acerca un oponente.',

        },
        level4:{
            text1:'Ya habrás notado que en el calentamiento utilizamos todas las partes del pie, ahora vamos a utilizar el interior y la planta del pie. Como ya has aprendido a regatear con el balón, ahora queremos que lo aceleres, que vayas rápido y que no te olvides de mirar hacia arriba de vez en cuando. ',
            text2:'The Outside Cut es un skill difícil de aprender pero muy importante. Si lo aprendes serás capaz de proteger el balón de un oponente e ir hacia atrás con el pie interior y exterior.',
        },



        level5:{
            text1:'En este nivel aprendemos el skill Turn Away, ahora puedes ir hacia los lados, por lo tanto Turn Away, usando el interior y el exterior de tu pie si se te acerca un oponente.',

        text2:'Piensa en ello, ahora eres capaz de cambiar de dirección hacia atrás y hacia los lados y mantener la posesión del balón si un oponente se acerca a ti. Otro skill que te hará avanzar es Instep Kick. Instep Kick no sólo es necesario para disparar a la portería sino también para dar pases largos o cambios de sentido.',



        },
        level6:{
            text1:'Una nuevo skill destacado, The Throw In (saque de banda). Parece más fácil de lo que es, como podrás comprobar. Mantén tus pies en el suelo y detrás de la línea. Además Taking the ball con el exterior del pie requiere mucha práctica, pero esta habilidad es esencial para el control orientado con el balón en un solo movimiento. ',
            text2:'Recuerda que siempre debes orientar el balón en movimiento y desplazarte hacia un espacio libre.',
        },


        level7:{
            text1:'Los skills de calentamiento se están volviendo más difíciles ahora, especialmente con el Ankle Roll, rodar sobre la pelota requiere mimarla mucho.',
            text2:'Y qué hay del Juggling, empezamos con una sola pierna, haciendo toques también aumentarás tu feeling con la pelota y es divertido al mismo tiempo, desafía a tus amigos... ¿Te has dado cuenta de que algunos de los skills de Turn Away y Cutback se practican de una manera diferente en las habilidades de calentamiento? lo hicimos para prepararte mejor!',
        },

        level8:{
            text1:'En este nivel te damos algunas opciones más sobre cómo controlar, cambios de dirección con el balón y combinar algunos skils de calentamiento anteriores. Por cierto, en The Turn asegúrate de dar una vuelta de 180 grados para volver por donde has venido...',
        },

        level9:{
            text1:'Este es una de mis skills favoritos, The Toe Walk. Caminar con la pelota, tocándola sólo con los dedos de los pies y mirando hacia arriba y a tu alrededor, intenta no mirar la pelota, es divertido...',
        },

        level10:{
            text1:'Ahora ya has avanzando mucho en los skills de calentamiento y en el lanzamiento del balón con una carrera hacia arriba. El skill The Dummy Stop es muy efectivo para engañar a tu oponente, especialmente cuando simulas que vas a disparar!',
        },

        level11:{
            text1:'Para calentar ahora vamos a hacer un juego de pies elegante. Aquí aprenderemos un skill muy famoso y efectivo llamado The Maestro, que lleva el nombre de uno de los mejores jugadores del mundo, el futbolista holandés Johan Cruyff. Muchos de los jugadores de primera clase que juegan actualmente copiaron este movimiento.',
        },
        level12:{
            text1:'Ya casi has llegado a la mitad de los niveles, ¡bien hecho! Sigue practicando y repitiendo los skills anteriores. "La práctica hace la perfección".',
        },
        level13:{
            text1:'En este nivel hay un gran reto, The Basic Sliding (entrada deslizante básica). Empieza a practicarlo con mucho cuidado y aumenta gradualmente la intensidad de tu práctica. Ten en cuenta que en el fútbol moderno incluso los jugadores de ataque saben cómo utilizar la entrada deslizante.',
        },
        level14:{
            text1:'Además de los skills de calentar/toque de balón, ahora necesitas practicar un balón largo y alto; esto requiere mucha concentración y enfoque para dar un pase preciso.',
            text2:'Empieza practicando en pases de corta distancia y aumenta gradualmente.',
        },
        level15:{
            text1:'Aquí introducimos algunos skills nuevos y más difíciles. Para empezar vamos a hacer control orientado con una técnica de drop kick, esto requiere mucho control de balón. Luego vamos a practicar remates de cabeza. Practica esto con cuidado.',
            text2:'Gradually increase distance between your partner/friend and you. Do not get frustrated, learning how to head a ball takes time! It is juggling time now but with two legs, this is considered as being very skilful and it is fun to do. Try to get on the Global ranking with this, it is a challenge!',
        },
        level16:{
            text1:'En este nivel vamos a vencer al oponente, yendo hacia adelante. Comenzamos con el aprendizaje de skills individuales y dobles para vencer a un oponente en una situación de 1 contra 1. Vencer al adversario es uno de los aspectos más difíciles del fútbol',
            text2:'Las skills simples y dobles significan que puedes vencer a un oponente en cualquier lado, siendo impredecible los regates simples. La Inswinger es una habilidad importante en el fútbol moderno, ya que un tiro libre se ejecuta con la Inswinger. Se necesita mucha práctica y esfuerzo para perfeccionar la inswinger, así que no te rindas. No te preocupes por el deslizamiento',
        },
        level17:{
            text1:'Aquí te damos más opciones para ser imprevisible a la hora de vencer a tu oponente en el 1 contra 1. También aprendemos aquí habilidades para regatear de espaldas, especialmente los atacantes/delanteros necesitan hacer esto. Esto te convertirá en un jugador excepcional',
        },
        level18:{
            text1:'Otra habilidad de Dropkick pero esta vez con el exterior de tu pie, muy difícil y puede tomar un poco más de tiempo para aprender. Ahora tómatelo con calma al principio practicando el Jumping Header',
        },
      level19:{            text1:'El Outswinger es una  técnica de disparo/pase muy avanzada y hábil, muy eficiente cuando se domina. Es la última de las técnicas de barridas que debes aprender, te convierte en un jugador más completo. ',
            text2:'A estas alturas deberías sentirte cómodo con las skills de regates simples, sin embargo necesitas seguir practicándolas regularmente, y aumentar la velocidad de ejecución de las skills.'
        },
        level:{
            text1:'En este nivel introducimos el movimiento falso, esto significa hacer un movimiento falso antes de recibir el balón para engañar a tu oponente. Rematar el balón con una carrera hacia arriba requiere una gran sincronización, ¡eso se consigue practicando mucho!',

        },
        level21:{
            text1:'Algunas habilidades más para realizar regates simples que los grandes jugadores realizan y hacen',
            
        },
        level22:{
            text1:'The Volley es definitivamente una skill que necesitas aprender, espectacular habilidad de disparo y pase.'
            
        },
        level23:{
            text1:'Te estás acercando al final, sigue practicando',
            
        },
        level24:{
            text1:'¡Ya casi está, después de este nivel uno más!',
            
        },
        level5:{
            text1:' Gran logro, fantástico, 25 niveles con 100 habilidades y los hiciste todos. ¡Has hecho que VFA se sienta orgullosa! Te seguiremos en la clasificación. Sigue compartiendo tus vídeos de RA y tus logros con nosotros en las redes sociales. Pronto habrá más, ¡sigue atento!',
            
        },

    },

    ar:{
        alert: {
            permitcamera:'Permiso de cámara',
            msgpermitcamera:'VFA necesita acceder a la cámara',
            textsuccessfully:'Guardado correctamente.',
            textdenied:'Permiso denegado.',
            hasoccurred:'Ocurrió un error inesperado.',
            necessarypermissions:'No tienes permisos para acceder a almacenar archivos.',
            uploading:'Enviando video, por favor espera...',
            errorUploading:'Error video upload',
            notassigned:'Skill no asignada',
            desnotassigned:'Tu coach no te ha asignado esta tarea.',
            allowedpost:'No tienes permitido publicar',
            needaprove:'Necesitas aprobar los niveles anteriores para poder publicar el video.',
            thevideocoach:'Enviando video a tu coach...',
            postvideo:'Publicar Video',
            cancel:'Cancelar',


        },

        shareSocial:{
            text1:'Ayuda a',
            // en medio de estos dos aparece el username 
            text2:'valorando su técnica! ',

        },

    },

    chat:{
        messajeInfo:'No tienes asignado Coach',
        placeholderMessage:'Escribe un mensaje'
    },


    notifications:{
        messageInfo:'Sin notificaciones',
        alerMessage:'¿Eliminar notificación?'
    },

    previaNotifications:{
        record:'Volver a grabar',
        // oracion de los votos 
        text1:'Necesitas',
        text2:'votos positivos para aprobar!',

        buttonRecord:'GRABAR',
    },


    myTeam:{

        menssagePage:'No hay jugadores en tu equipo, invítalos a unirse...'

    },




   
    registerCoach:{
        titlePage: 'Crear cuenta Coach',
        playerTab: 'PLAYER',
        coachTab: 'COACH',
        alert:{
            alertDate: 'La fecha de nacimiento es requerida',
            alertSubmit: 'Debes de aceptar los términos y condiciones.',
            alertSubmitPlayer: 'Usuario y/o contraseña incorrecto',

            // Nuevas --- alexis  --------------------------------------------------------
            alertEmailalready:'Este email ya está en uso. Prueba con otro.',
            errorMessage:'Ha ocurrido un erros',
            errorPasswordValidate:'La contraseña debe de tener mínimo 8 caracteres incluyendo una letra mayúscula y un número.',
            //   ----------------------------------------------------------------------------
        },
       
        player:{
            titlePage: 'Crear cuenta de Player',
            title:'PRUEBA GRATIS',
            descripcion: 'Crea tu cuenta y disfruta de 7 días GRATIS!',
            coachVirtual: 'The virtual trainer will guide you through the program.',
            coachPersonal: 'Te unirás a tu entrador y a tu equipo',
            optionCoachVirtual:'Quiero un entrenador virtual',
            optionCoachPersonal:'Tengo un entrenador',
            optionSelect:'Elige la opción correcta.',
            name:'Nombre',
            helpName:'Introduce tu nombre',
            email:'Email',
            helpEmail:'Introduce tu email',
            password: 'Contraseña',
            helpPass: 'Introduce tu contraseña',
            date: 'Fecha de Nacimiento',
            text1: 'Acepto',
            text2: 'Políticas de privacidad',
            text3: 'y',
            text4: 'términos y condiciones.',
            submit: 'Enviar',
        },
        coach:{
            titlePage: 'Crea una cuenta de Coach',
            title:'PRUEBA GRATIS',
            descripcion: 'Crea tu cuenta y disfruta de 7 días GRATIS!',
            name:'Nombre',
            helpName:'Introduce tu nombre',
            email:'Email',
            helpEmail:'Introduce tu email',
            password:'Contraseña',
            helpPass:'Introduce tu contraseña',
            text1: 'Acepto',
            text2: 'Políticas de privacidad',
            text3: 'y',
            text4: 'términos y condiciones.',
            submit: 'Enviar',
        }
    },
    registerPlayer:{
        titlePage: 'Crear cuenta Coach',
        playerTab: 'Player',
        coachTab: 'Coach',
        alert:{
            alertDate: 'Fecha de nacimiento requerida',
            alertSubmit: 'Debes de aceptar los términos y condiciones',
            alertSubmitPlayer: 'Usuario y/o contraseña incorrecto',

            // nuevo 
            validateAlertCredencial:'Usuario y/o contraseña incorrecto',
            messageError:'Ha ocurrido un error',

        },
        // Nuevo  ----------------------------------------------------------------------------
        childrenPolicy:{

          title:'POLÍTICA DE NIÑOS COPPA',
          text1:'Aviso a los padres',
          text2:'En cumplimiento de la Ley de Protección de la Privacidad de los Niños en Internet (COPPA), los padres (o tutores legales) de los niños menores de 13 años deben dar su consentimiento a la recopilación, uso y divulgación de la información personal de sus hijos recogida por los productos y servicios de Sports Innovations LLC, como "Virtual Football Academy" VFA.',
        // text3:'En cumplimiento de la Ley de Protección de la Privacidad de los Niños en Internet (COPPA), los padres (o tutores legales) de los niños menores de 13 años deben dar su consentimiento para la recopilación, el uso y la divulgación de la información personal de sus hijos recopilada por los productos y servicios de Sports Innovations LLC como "Virtual Football Academy" VFA.',
          text4:'El pago de una suscripción a VFA, incluyendo el uso de una tarjeta de crédito, se considerará un consentimiento expreso a esta Política de Privacidad.',
          text5:'Excepciones al consentimiento paterno previo. Se requiere el consentimiento paterno verificable antes de cualquier recogida, uso o divulgación de información personal de un niño, excepto en los casos establecidos en este párrafo:',
          text6:'Cuando el propósito de la recogida de la información de contacto en línea de los padres sea proporcionar una notificación voluntaria a los padres, y posteriormente actualizarlos, sobre la participación del niño en un sitio web o servicio en línea que no recoja, utilice o revele de otro modo la información personal de los niños. En estos casos, la información de contacto en línea de los padres no puede utilizarse ni divulgarse para ningún otro fin. En estos casos, el operador debe hacer esfuerzos razonables, teniendo en cuenta la tecnología disponible, para garantizar que el padre reciba la notificación descrita en §312.4(c)(2);',
          text7:'Más información',
          emailPadre:'Por favor, introduzca un correo electrónico correcto',
          textTerminos1:'Estoy de acuerdo con ',
          textTerminos2:'PRIVACIDAD DE LOS NIÑOS',
          AcceptTerms:'Por favor, acepte la Política de Privacidad de los niños para continuar',
         
        },
        //  ------------------------------------------------------------------------------------

        player:{
            titlePage: 'Crear cuenta Player',
            title:'PRUEBA GRATIS',
            descripcion: 'Crea tu cuenta y obtén 7 días de prueba GRATIS.',
            coachVirtual: 'The virtual trainer will guide you through the program.',
            coachPersonal: 'Te unirás a tu entrador y a tu equipo',
            optionCoachVirtual:'Quiero un entrenador virtual',
            optionCoachPersonal:'Tengo un entrenador',
            optionSelect:'Elige la opción correcta',
            name:'Nombre',
            helpName:'Introduce el nombre',
            email:'Email',
            helpEmail:'Introduce tu email',
            password: 'Contraseña',
            helpPass: 'Introduce  tu contraseña',
            date: 'Fecha de Nacimiento',
            text1: 'Acepto las',
            text2: 'Políticas de privacidad',
            text3: 'y',
            text4: 'Términos y condiciones',
            // Nuevo
            // -----------------------------------------------------------------
            submit: 'CONTINUAR',
            validateInputEmail:'Introduce un email válido o no en uso.',
            errorPasswordValidate:'La contraseña debe de tener mínimo 8 caracteres incluyendo una mayúscula, una letra y un número.',
            // -------------------------------------------------------------------
        },
        //  Nuevo  ---------------------------------------------------------------
        payPlayer:{
            title:' Continua para desbloquear tus 7 días gratis',
            Permonth:'Mensual',
            // PARRAFO DEL FINAL DE LOS PLANES PLAYER 
            text1Android: 'Cancelar en cualquier momento en Google Play',
            text1IOS: 'Cancelar en cualquier momento en Apple ID / Suscripciones',
            text2:'Tu suscripción se renovará por el mismo periodo de tiempo a menos que la canceles 24 horas antes del final del periodo actual. Puedes cancelar en cualquier momento desde',
            text3:'ID de Apple / Suscripciones',
            text4:'sin coste adicional. Al hacerlo, tu suscripción terminará al final del periodo actual',
            text5:'Mejor valorado',
            text6:'meses',
            text7:'mes',
            bottom:"Empezar mi prueba gratis",
            textSave:'Guardar'
        },

        
        // --------------------------------------------------------------------------

    //   Global coach 
     
        coach:{

            titlePage: 'Crear cuenta de Player',
            title:'PRUEBA GRATIS',
            descripcion: 'Create la cuenta y disfruta de 7 días GRATIS',
            name:'Nombre',
            helpName:'Introduce tu nombre',
            email:'Email',
            helpEmail:'Introduce tu email',
            password:'Contraseña',
            helpPass:'Introduce la contraseña',
            text1: 'Estoy de acuerdo con ',
            text2: 'Políticas de privacidad',
            text3: 'y',
            text4: 'términos y condiciones.',
            submit: 'Enviar',
        }
    },


    // Reviews

    reviews:{
       text1:'Se ha producido un error inesperado',
       text2:'No hay videos pendientes de revisión',
    },


    calendarCoach:{
        text1:'Descripción',
        text2:'Selecciona la fecha',
        text3:'Agendar',
        text4:'Los siguientes skills se pueden practicar en este ejercicio',
        text5:'Pié Derecho',
         text6:'Pié Izquierdo',
         text7:'Otros',
         text66:'Detalles del ejercicio'
   },




   
    // my teams 

    teams:{

        levels: {
            1:'Nivel 1',
            2:'Nivel 2',
            3:'Nivel 3',
            4:'Nivel 4',
            5:'Nivel 5',
            6:'Nivel 6',
            7:'Nivel 7',
            8:'Nivel 8',
            9:'Nivel 9',
            10:'Nivel 10',
            11:'Nivel 11',
            12:'Nivel 12',
            13:'Nivel 13',
            14:'Nivel 14',
            15:'Nivel 15',
            16:'Nivel 16',
            17:'Nivel 17',
            18:'Nivel 18',
            19:'Nivel 19',
            20:'Nivel 20',
            21:'Nivel 21',
            22:'Nivel 22',
            23:'Nivel 23',
            24:'Nivel 24',
            25:'Nivel 25',
        },

        category:{
            1:'CALENTAMIENTO',
            2:'CONTROL DE BALÓN',
            3:'TÉCNICA DE PASE Y DISPARO ',
            4:'RECORTE',
            5:'CAMBIO DE DIRECCIÓN',
            6:'CONTROL ORIENTADO',
            7:'SAQUE DE BANDA',
            8:'REMATE DE CABEZA',
            9:'ENTRADA DEFENSIVA',
            10:'REGATE',
            11:'REGATE DOBLE',
            12:'REGATE DE ESPALDA',
            13:'DISPARO A PUERTA',
        },

        course:{
            1: 'CURSO - BASICO',
            2: 'CURSO - CONTROLA EL BALÓN',
            3: 'CURSO - DOMINA AL OPONENTE',
            4: 'CURSO - REGATES MAGICOS',
            5: 'CURSO - PERFECCION',
          },

        exercises_categories :{
            1:'RECORTE',
            2:'CAMBIO DE DIRECCIÓN',
            3:'RECORTES CON CAMBIO DE DIRECCIÓN',
            4:'CONTROL DE BALÓN',
            5:'CONTROL DE BALÓN, TÉCNICA DE PASE Y DISPARO.',
            6:'CONTROL ORIENTADO',
            7:'TÉCNICA DE DISPARO Y PASE',
            8:'REGATE SIMPLE',
            9:'REGATE DOBLE',
            10:'REGATE DE ESPALDAS',
            11:'DISPARO A PUERTA',
          },



        alert:{
            text1:'Campos requeridos',
            text2:'El método de trabajo es requerido',
            text3:'Se requiere valor de método de trabajo',
        },

        textInfo:' No hay jugadores en tu equipo, invítalos a unirse!',



    },



    // Navigations  
    navigations:{
        text1:'Detalle del Skill',
    },
    


    Permissions:{

        text1:'Permiso Cámara',
        text2:'Necesita acceso a la cámara',
        text3:'Guardado correctamente.',
        text4:'Permiso denegado.',
        text5:'Ha ocurrido un error.',
        text6:'No se han concedido los permisos necesarios para acceder a la cámara.',
        text7:'Sin autenticar.',


    },


    tutorial:{
        text1:'A través de este menú principal puede navegar entre las vistas y utilizar VFA',
        text2:'CHAT',
        text3:'Envía mensaje privado al/los player(s)',
        text4:'Anterior',
        text5:'Siguiente',
        text6:'Saltar tutorial',
        text7:'Menú Lateral',
        text8:'Opciones, VFA',
        text9:'Finalizar',
        text10:'INICIO',
        text11:'Aquí puedes ver todos los skills organizados',
        text12:'NOTIFICACIONES',
        text13:'Todas las notificaciones en este lugar, deja pulsado encima para eliminar cada una.',
        text14:'REVISION',
        text15:'Revisa los video skills que te mandan tus jugadores',
        text16:'ESTADISTICAS',
        text17:'Revisa las estadísticas de tus equipos y jugadores',
        text18:'MIS EQUIPOS',
        text19:'Aquí puedes ver tus equipos',
        // --
        text20:'AGENDA',
        text21:' Puedes ver tu agenda/plan de entrenamiento con los skills/ejercicios que has agendado previamente.',
        // --
        text22:"Cambiar pié: Pié Izquierdo - Pié Derecho",
        text23:"Activar audio.",
        text24:"Cambiar idioma de audio",
        text25:"Pause - Play el 3D.",
        text26:"Barra de velocidad.",
        text27:"Adelantar frame a frame",
        text28:"Cambiar a AR",
        text29:"Cambiar a 3D",


        


    },



    skillData:{
       alert:{
           text1:'Dato requerido',
       },

       notassigned:'Este skill no ha sido asignado a ningún player.',
       videofavorite:'No hay videos favoritos',
       text1:'VIDEOS',
       text2:'JUGADORES',
       text3:'FAVORITOS',
       text4:'Selecciona player(s) para enviar skills como tarea(s)',
       send:'Enviar tareas',
       text5:'Agendar',
    },

    //   Global coach 

    coach:{

        alert:{
            text1:'No se han concedido los permisos necesarios para acceder a la cámara.',
            text2:'Permiso Galeria',
            text3:'Tamyda necesita acceso a la galería',
            text4:'Permiso denegado.',
            text5:'No se han otorgado los permisos necesarios para acceder a tu almacenamiento.',
            text6:'Nombre requerido',
            text7:'Selecciona método de trabajo',
            text8:'Método de trabajo',
            text9:'Se ha actualizado tu foto de perfil',
            text10:'Se ha producido un error al actualizar',
            text11:'La solicitud no pudo ser procesada',
            text12:'Permiso denegado',
            text13:'Ha ocurrido un error.',
            text14:'No se han concedido los permisos necesarios para acceder a la cámara.',
            text15:'Permiso Galeria',
            text16:'Tamyda necesita acceso a la galeria',
            text17: "¿Estás seguro?",
            text18: "Este proceso no es reversible. Por favor, confirme su decisión",
            text19:'Fallo al cerrar la sesión',
            text20: "La información de tu perfil fue actualizada",
            text21:'La información de tu perfil fue actualizada',
            text22:'Se produjo un error al actualizar',
            text23:'Debes actualizar al menos un dato',
            text24:'La solicitud no pudo ser procesada',
            text25:'Los campos son obligatorios',
            text26:'Elija el método de trabajo',
            text27:'Equipo creado con éxito',
            text28:'¿Está seguro de que quiere eliminar el equipo?',
        },


        text1:'Si ya no quieres utilizar el VFA, puedes eliminar tu cuenta de forma permanente',
        text2:'Eliminar cuenta',

        // Team 
        newTeam:'Selecciona el método de trabajo por defecto:',
        buttom:'Guardar cambios',
        selectImg:'Selecciona una imagen o captura una foto',
        sendButton:'Enviar invitación',



        // profile coach

        
        deleteMessage:'Eliminar mi cuenta',
        messageInfodelete:'Al eliminar la cuenta perderás tus datos de acceso y se borrarán todos los vídeos que tengas en tu perfil. Este proceso no es reversible, VFA no se hace responsable del uso de esta funcionalidad del sistema ya que es de uso exclusivo del usuario.',
        buttonCancel:'CANCELAR',
        buttonNext:'CONTINUAR',
        editAccount:'Editar la información de mi cuenta',

        formulario:{
            nombre:'Nombre',
            password:'Contraseña',
            button:'Cambiar'
        },







    },






    register:{
        titleView: 'Selecciona para registrarte',
        h1:'¡Selecciona tu perfil!',
        h2: 'Para crear tu cuenta',
        coach:'Crea varios equipos, invita a jugadores y gestiona cada equipo de forma independiente',
        player:'¡Diviértete grabando skills en RA y conviértete en un profesional!',
        bottom: 'CREAR CUENTA',
    },
    welcomeCoach:{
        titleView: 'Cuenta de entrenador',
        carrouselOneTitle: 'Biblioteca de 100 skills y 83 ejercicios',
        carrouselOneDescription: 'Más de 80 ejercicios en grupo de 2 y 3 jugadores para practicar las 100 skills individuales según sus categorías',
        carrouselTwoTitle: '¡Asigna skills como tarea!',
        carrouselTwoDescription: 'Los jugadores pueden grabar skills en Realidad Aumentada, te enviarán el vídeo y luego podrás puntuar del 1 al 10.',
        carrouselThreeTitle:'Optimiza tu tiempo con el Plan de Entrenamiento',
        carrouselThreeDescription:'Organiza las sesiones de entrenamiento, asigna skills y ejercicios para una fecha determinada. Tu jugador podrá ver los ejercicios antes de llegar al campo',
        bottom:'CONTINUAR',
    },
    welcomePlayer:{
        titleView: 'Cuenta de jugador',
        carrouselOneTitle: '¡Conviértete en un profesional!',
        carrouselOneDescription: 'Mejora tus habilidades y tu juego con 100 skills basadas en 25 niveles de dificultad',
        carrouselTwoTitle: '¡Graba los skills en AR!',
        carrouselTwoDescription: 'Compara tu desempeño de la habilidad contra la animación 3D.',
        carrouselThreeTitle:'Comparte tu cuenta',
        carrouselThreeDescription:'Consigue una puntuación y mejora tu clasificación',
        bottom: 'CONTINUAR',
    },

    profile:{
        update:'Se ha actualizado la información de su perfil',
        errorUpdate:'Se ha producido un error al actualizar',
        updateInfo:'Debes actualizar al menos un dato',
        required:'No se ha podido procesar la solicitud',
        textInfo:'Actualizar la información de su cuenta',
        buttomSave:'Guarda los cambios',
        textUpdatePass:'Cambiar contraseña',
        updatePhoto:'Tu foto de perfil ha sido actualizada',
        remove:'Eliminar jugador',

        // listado de los videos 
        listEmpty:'No hay vídeos',
        listEmpty2:'Esta sección está disponible sólo para el uso de los jugadores con entrenador',


        menuProvider:{
            option1:'Editar perfil',
            option2:'Copiar enlace',
            option3:'Compartir enlace',
            option4:'Subir foto',
            option5:'Copiar URL del perfil',
            option6:'Compartir este perfil',
            option7:'Control parental',           
        },
        // subir foto 
        messagePhoto:'Imagen de perfil',
        messajeModal:'Selecciona una imagen o captura una foto',

        previaVideo:{
            option1:'Copiar enlace de vídeo',
            option2:'Compartir vídeo',
            option3:'Eliminar'        }
    },

    seguridad:{
          alert:{
              text1:'¿Estás seguro?',
              text2:'Este proceso no es reversible. Por favor, confirme su decisión',
              buttonCancel:'Cancelar',
              buttonOk:'OK',

          },

          text1:'Eliminar mi cuenta',
          text2:'Al eliminar la cuenta perderás tus datos de acceso y se borrarán todos los vídeos que tengas en tu perfil. Este proceso no es reversible, VFA no se hace responsable del uso de esta funcionalidad del sistema ya que es de uso exclusivo del usuario.',
          botón1:'CANCELAR',
          botón2:'CONTINUAR',
          text3:'Si no quieres seguir utilizando el VFA, puedes eliminar tu cuenta de forma permanente.',
          text4:'Eliminar cuenta',
    },

    ranking: {
       text1:'Global',
       text2:'Mejor de',
       text3:'Mejores skills',
    },

    videoPlayer:{
        text1:'Aprobado',
        text2:'Reprobado',
        text3:' Pendiente',
    },

    training:{
      text1:'No tienes un entrenador asignado',
      text2:'¿Seguro que quieres eliminar el elemento de la agenda?',
      text3:'Eliminar elemento de la agenda',
      text4:'No hay eventos',
      text5:'Plan de entrenamiento'
    },




    video3D:{
        sendVideo:{
            title:  'VIDEO SUBIDO CON ÉXITO A TU MURO',
            title2: 'VIDEO ENVIADO A TU COACH CON ÉXITO',

            description:'Ahora tu video puede ser calificado',
            description2:'Tu entrenador calificará tu video',
            
            text:'Comparte en las Redes Sociales!',
            text2:'',


            btn1:'INICIO',
            btn2:'COMPARTIR',

            message:'Video subido con éxito',
            message2:'Video enviado con éxito'
       

 },
        option1SinCoach:'Post video',
        option1ConCoach:'Enviar al coach',
        option2: 'Compartir',
        option3: 'Guardar',
        option4: 'Descartar'
    },
    bottomMenu:{
        Home: 'Inicio',
        Reviews: 'Revisión',
        My_Team: 'Mi Equipo',
        Training: 'Agenda',
        Stats:'Estadísticas',
        Profile:'Perfil',
        Ranking:'Ranking'
    },

    NewText:{
        profileEdit: 'Editar perfil'
        
    },

    previVideoSend:{
        text1:'CONTINUAR',
    },



    drawer:{
         text1:'Privacidad de los niños',
         text2:'Contacto',
         text3:'Cerrar sesión',
         text4:'Política de privacidad',
         text5:'FAQ',
         text6:'Jugador Single',
         text7:'Sin coach asignado',
         text8:'Jugadores',
         text9:'Equipos',
         text10:'Perfil',
         text11:'Mis equipos',
         text12:'Guía del Coach',
         text13:'Tutorial',
         text14:'Tutorial 3D',
         text15:'Cerrar sesión'

     },


    skills: {
        1: "The Wave",
        2: "The Amir",
        3: "The Ankle Roll",
        4: "The Ankle Tap Roll",
        5: "The Swop",
        6: "The Basic Sliding",
        7: "Behind Standing Leg",
        8: "Bicycle Kick",
        9: "Chest Control",
        10: "The Clamp Sliding",
        11: "The Combination",
        12: "The Reverse",
        13: "The Double Flash",
        14: "The Double Sidestep",
        15: "The Double Tap",
        16: "The Double V",
        17: "The Double Zigzag",
        18: "The Double Roll",
        19: "The Double Scissor",
        20: "The Dragging Scissor",
        21: "The Drag",
        22: "same as 020",
        23: "Dribbling",
        24: "The Dummy Stop",
        25: "Fake Inside Foot Dropkick TBS",
        26: "Fake Inside Foot TBS",
        27: "Fake Outside Foot Drop Kick TBS",
        28: "Fake Outside Foot TBS",
        29: "The Frenkie",
        30: "Half Bicycle Kick",
        31: "Heading Standing",
        32: "The Hip Pivot",
        33: "The Hook Roll",
        34: "Instep Control",
        35: "Instep Kick",
        36: "The Instep Stop",
        37: "Inswinger",
        38: "The Inside Cut",
        39: "Inside Foot Dropkick TBS",
        40: "Inside Foot Kick",
        41: "The Sideways",
        42: "The Inside Turn Away",
        43: "The Instep Tap",
        44: "Inside Foot Control",
        45: "Instep Dropkick",
        46: "Juggling",
        47: "Juggling with one leg",
        48: "The Go Around",
        49: "The Musa",
        50: "The Outside Cut",
        51: "Outside Foot dropkick TBS",
        52: "Outside Foot Kick",
        53: "Outswinger",
        54: "Outside Foot TBS",
        55: "The Outside Turn Away",
        56: "The Turn",
        57: "The Ramzy",
        58: "The Stroke",
        59: "The Tri Step",
        60: "The Swing",
        61: "The Roll Off",
        62: "The Backdoor",
        63: "The Junction",
        64: "The Shuffle",
        65: "The Sidestep",
        66: "The Sideways Half Turn",
        67: "The Sideways Maestro",
        68: "Inside Foot TBS",
        69: "The SkateBall",
        70: "The Skim Kick",
        71: "The Spread Sliding",
        72: "The Super Roll",
        73: "The Pivot",
        74: "The Switch Trick",
        75: "Turn Away",
        76: "The V",
        77: "The Flash",
        78: "The Hook Roll",
        79: "The Maestro",
        80: "The Midget",
        81: "The Mix",
        82: "The Step Over",
        83: "The Three Taps",
        84: "Run up",
        85: "Standing",
        86: "The Tick Tock",
        87: "The Toe Walk",
        88: "The V Tick Tock",
        89: "The V Variation",
        90: "Volley",
        91: "The Flow",
        92: "Zigzag Dribbling",
        93: "The Zigzag Tap",
        94: "The Zigzag",
        95: "Dive",
        96: "Faster Dribbling",
        97: "Jumping",
        98: "Long High Ball",
        99: "Run up",
        100: "The Yaya",
        101: "The Step Over",
      },


      textos: {
        text1: 'SKILLS',
        text2:'EJERCICIOS GRUPALES',
        text3:'Ordernar por Niveles',
        text4:'Ordernar por  Categorias',
        text5:'Ordernar por  Cursos',
        text6:'Ejercicios para 2 jugadores',
        text7:'Ejercicios para 3 jugadores',
        text8:'Ejercicio',
        text9:'Tienes un codigo de invitado?',
        text10:'Codigo'


    }



};