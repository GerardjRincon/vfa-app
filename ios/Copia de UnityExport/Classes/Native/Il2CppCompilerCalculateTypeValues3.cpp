﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DentedPixel.LTExamples.TestingUnitTests
struct TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB;
// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396;
// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0
struct U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97;
// LTBezierPath
struct LTBezierPath_t942F5E0A4B5DDDD5E08F2CE947CB5A47AEB7F22D;
// LTDescr
struct LTDescr_t88934797D6A324735209180E7958727F7E3746A9;
// LTDescr[]
struct LTDescrU5BU5D_tA2196F2BCBCF136D725BED68B3C73FA238926239;
// NLayer.Decoder.BitReservoir
struct BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB;
// NLayer.Decoder.Huffman/HuffmanListNode[]
struct HuffmanListNodeU5BU5D_tA134A87CE2CFFD5607B297D08F348FF2C73CBD9D;
// NLayer.Decoder.ID3Frame
struct ID3Frame_tAB95743953BDF6FD6810C1AB92F25841D41135A8;
// NLayer.Decoder.LayerIDecoder
struct LayerIDecoder_t29E88F178B1D31CBFCDBF7BF51304FED69AF8D55;
// NLayer.Decoder.LayerIIDecoder
struct LayerIIDecoder_t06D951DC8F7021B2CF4D70FCE55297ECFE87ABFE;
// NLayer.Decoder.LayerIIIDecoder
struct LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1;
// NLayer.Decoder.LayerIIIDecoder/HybridMDCT
struct HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878;
// NLayer.Decoder.MpegFrame
struct MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51;
// NLayer.Decoder.MpegStreamReader
struct MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40;
// NLayer.Decoder.MpegStreamReader/ReadBuffer
struct ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB;
// NLayer.Decoder.RiffHeaderFrame
struct RiffHeaderFrame_tD195C40825041F5DA7AE518B989C8EEEBB9CAC8C;
// NLayer.Decoder.VBRInfo
struct VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428;
// NLayer.MpegFrameDecoder
struct MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2;
// NatSuite.Examples.Components.CameraPreview
struct CameraPreview_t14366C64F8D63434B92D4F4DE771EDEE07EAE89C;
// NatSuite.Examples.Components.RecordButton
struct RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D;
// NatSuite.Examples.Giffy
struct Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4;
// NatSuite.Examples.ReplayCam
struct ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE;
// NatSuite.Recorders.Clocks.IClock
struct IClock_t796976DA6493A5EC5882EBF931921C1A2BE1E25C;
// NatSuite.Recorders.GIFRecorder
struct GIFRecorder_tE570AFA9B4B6F339A1C9026D6DF39407515F7215;
// NatSuite.Recorders.IMediaRecorder
struct IMediaRecorder_t8CBE53C26311E39E36003C7BC7144520A1FC084C;
// NatSuite.Recorders.Inputs.AudioInput
struct AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF;
// NatSuite.Recorders.Inputs.AudioInput/AudioInputAttachment
struct AudioInputAttachment_tBD4C16D24B6BB510C28C62437F470268DD97F38B;
// NatSuite.Recorders.Inputs.CameraInput
struct CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655;
// NatSuite.Recorders.Inputs.CameraInput/<>c__DisplayClass11_0
struct U3CU3Ec__DisplayClass11_0_t19832433766A7855A211E71E6FD20B6C30613D2C;
// NatSuite.Recorders.Inputs.CameraInput/CameraInputAttachment
struct CameraInputAttachment_t415800A4510BE48007515F8FA8201B5FCD986328;
// NatSuite.Recorders.JPGRecorder
struct JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Action`1<System.Single>
struct Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B;
// System.Action`1<System.Single[]>
struct Action_1_tF14E2B60916B5AA685432504A9BC1036ED6AAB84;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_t08BAF0B9143320EA6FA33CF25A59B6F1641EA5B6;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[][]
struct BooleanU5BU5DU5BU5D_tD67717CF466EECE15F5A827693454E4F8E20FAE8;
// System.Byte[0...,0...][]
struct ByteU5B0___U2C0___U5DU5BU5D_tCF5D5F0A15F6B2D8B72D764107BCB74EE48ADEC4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Single[]>
struct List_1_t389DEBD2C92F3A31DC21A2AE3CBEED8DDA76FF5C;
// System.Collections.Generic.List`1<System.Threading.Tasks.Task>
struct List_1_tC62C1E1B0AD84992F0A374A5A4679609955E117E;
// System.Comparison`1<NLayer.Decoder.Huffman/HuffmanListNode>
struct Comparison_1_t145272BF3F25310B6B700255B5F8557FCE90D339;
// System.Comparison`1<UnityEngine.Camera>
struct Comparison_1_t22D55C0E9B690772B778163906A02DEE7E89FFA5;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.Stopwatch
struct Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4;
// System.Func`1<System.Boolean>
struct Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.FileStream
struct FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43;
// System.Int32[][][]
struct Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_tEFDFBE18E061A6065AB2FF735F1425FB59F919BC;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.Single[][]
struct SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E;
// System.Single[][][]
struct SingleU5BU5DU5BU5DU5BU5D_tA077D87E42A03FD45A6BEFFD79858AF9938E28A8;
// System.String
struct String_t;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7;
// System.Threading.Tasks.Task
struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2;
// System.Threading.Tasks.TaskCompletionSource`1<System.String>
struct TaskCompletionSource_1_t740D6B4559A97E4E740413E4EB4F1F90A84CB317;
// System.Threading.Tasks.Task`1<System.String>
struct Task_1_t4A75FEC8F36C5D4F8793BB8C94E4DAA7457D0286;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Animation
struct Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C;
// UnityEngine.AnimationClip
struct AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera[]
struct CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform[]
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804;
// UnityEngine.UI.AspectRatioFitter
struct AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA;
// UnityEngine.WebCamTexture
struct WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// DentedPixel.LTExamples.TestingUnitTests/<>c
struct  U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields
{
public:
	// DentedPixel.LTExamples.TestingUnitTests/<>c DentedPixel.LTExamples.TestingUnitTests/<>c::<>9
	U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4 * ___U3CU3E9_0;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_3
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__22_3_1;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_22
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__22_22_2;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_7
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__22_7_3;
	// System.Action`1<System.Single> DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_12
	Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * ___U3CU3E9__22_12_4;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_18
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__22_18_5;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__26_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__26_0_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_3_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields, ___U3CU3E9__22_3_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__22_3_1() const { return ___U3CU3E9__22_3_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__22_3_1() { return &___U3CU3E9__22_3_1; }
	inline void set_U3CU3E9__22_3_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__22_3_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_3_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_22_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields, ___U3CU3E9__22_22_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__22_22_2() const { return ___U3CU3E9__22_22_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__22_22_2() { return &___U3CU3E9__22_22_2; }
	inline void set_U3CU3E9__22_22_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__22_22_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_22_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_7_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields, ___U3CU3E9__22_7_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__22_7_3() const { return ___U3CU3E9__22_7_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__22_7_3() { return &___U3CU3E9__22_7_3; }
	inline void set_U3CU3E9__22_7_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__22_7_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_7_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_12_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields, ___U3CU3E9__22_12_4)); }
	inline Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * get_U3CU3E9__22_12_4() const { return ___U3CU3E9__22_12_4; }
	inline Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B ** get_address_of_U3CU3E9__22_12_4() { return &___U3CU3E9__22_12_4; }
	inline void set_U3CU3E9__22_12_4(Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * value)
	{
		___U3CU3E9__22_12_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_12_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_18_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields, ___U3CU3E9__22_18_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__22_18_5() const { return ___U3CU3E9__22_18_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__22_18_5() { return &___U3CU3E9__22_18_5; }
	inline void set_U3CU3E9__22_18_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__22_18_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_18_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields, ___U3CU3E9__26_0_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__26_0_6() const { return ___U3CU3E9__26_0_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__26_0_6() { return &___U3CU3E9__26_0_6; }
	inline void set_U3CU3E9__26_0_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__26_0_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__26_0_6), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1
struct  U3CU3Ec__DisplayClass22_1_tCE05255B60D85F5EF3F3593FD20544CE1C8805AA  : public RuntimeObject
{
public:
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1::beforeX
	float ___beforeX_0;
	// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_beforeX_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_1_tCE05255B60D85F5EF3F3593FD20544CE1C8805AA, ___beforeX_0)); }
	inline float get_beforeX_0() const { return ___beforeX_0; }
	inline float* get_address_of_beforeX_0() { return &___beforeX_0; }
	inline void set_beforeX_0(float value)
	{
		___beforeX_0 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_1_tCE05255B60D85F5EF3F3593FD20544CE1C8805AA, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CSU24U3CU3E8__locals1_1), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2
struct  U3CU3Ec__DisplayClass22_2_tBC56C65485DAFCD7F524F15767C83EF35620F245  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::totalTweenTypeLength
	int32_t ___totalTweenTypeLength_0;
	// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::CS$<>8__locals2
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396 * ___CSU24U3CU3E8__locals2_1;
	// System.Action`1<System.Object> DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::<>9__24
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___U3CU3E9__24_2;

public:
	inline static int32_t get_offset_of_totalTweenTypeLength_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_2_tBC56C65485DAFCD7F524F15767C83EF35620F245, ___totalTweenTypeLength_0)); }
	inline int32_t get_totalTweenTypeLength_0() const { return ___totalTweenTypeLength_0; }
	inline int32_t* get_address_of_totalTweenTypeLength_0() { return &___totalTweenTypeLength_0; }
	inline void set_totalTweenTypeLength_0(int32_t value)
	{
		___totalTweenTypeLength_0 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_2_tBC56C65485DAFCD7F524F15767C83EF35620F245, ___CSU24U3CU3E8__locals2_1)); }
	inline U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396 * get_CSU24U3CU3E8__locals2_1() const { return ___CSU24U3CU3E8__locals2_1; }
	inline U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396 ** get_address_of_CSU24U3CU3E8__locals2_1() { return &___CSU24U3CU3E8__locals2_1; }
	inline void set_CSU24U3CU3E8__locals2_1(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396 * value)
	{
		___CSU24U3CU3E8__locals2_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CSU24U3CU3E8__locals2_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_2_tBC56C65485DAFCD7F524F15767C83EF35620F245, ___U3CU3E9__24_2)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get_U3CU3E9__24_2() const { return ___U3CU3E9__24_2; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of_U3CU3E9__24_2() { return &___U3CU3E9__24_2; }
	inline void set_U3CU3E9__24_2(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		___U3CU3E9__24_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__24_2), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25
struct  U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<>4__this
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * ___U3CU3E4__this_2;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<cubeCount>5__2
	int32_t ___U3CcubeCountU3E5__2_3;
	// System.Int32[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<tweensA>5__3
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___U3CtweensAU3E5__3_4;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<aGOs>5__4
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___U3CaGOsU3E5__4_5;
	// System.Int32[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<tweensB>5__5
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___U3CtweensBU3E5__5_6;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<bGOs>5__6
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___U3CbGOsU3E5__6_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71, ___U3CU3E4__this_2)); }
	inline TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcubeCountU3E5__2_3() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71, ___U3CcubeCountU3E5__2_3)); }
	inline int32_t get_U3CcubeCountU3E5__2_3() const { return ___U3CcubeCountU3E5__2_3; }
	inline int32_t* get_address_of_U3CcubeCountU3E5__2_3() { return &___U3CcubeCountU3E5__2_3; }
	inline void set_U3CcubeCountU3E5__2_3(int32_t value)
	{
		___U3CcubeCountU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CtweensAU3E5__3_4() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71, ___U3CtweensAU3E5__3_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_U3CtweensAU3E5__3_4() const { return ___U3CtweensAU3E5__3_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_U3CtweensAU3E5__3_4() { return &___U3CtweensAU3E5__3_4; }
	inline void set_U3CtweensAU3E5__3_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___U3CtweensAU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtweensAU3E5__3_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CaGOsU3E5__4_5() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71, ___U3CaGOsU3E5__4_5)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_U3CaGOsU3E5__4_5() const { return ___U3CaGOsU3E5__4_5; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_U3CaGOsU3E5__4_5() { return &___U3CaGOsU3E5__4_5; }
	inline void set_U3CaGOsU3E5__4_5(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___U3CaGOsU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CaGOsU3E5__4_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtweensBU3E5__5_6() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71, ___U3CtweensBU3E5__5_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_U3CtweensBU3E5__5_6() const { return ___U3CtweensBU3E5__5_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_U3CtweensBU3E5__5_6() { return &___U3CtweensBU3E5__5_6; }
	inline void set_U3CtweensBU3E5__5_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___U3CtweensBU3E5__5_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtweensBU3E5__5_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CbGOsU3E5__6_7() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71, ___U3CbGOsU3E5__6_7)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_U3CbGOsU3E5__6_7() const { return ___U3CbGOsU3E5__6_7; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_U3CbGOsU3E5__6_7() { return &___U3CbGOsU3E5__6_7; }
	inline void set_U3CbGOsU3E5__6_7(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___U3CbGOsU3E5__6_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbGOsU3E5__6_7), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26
struct  U3CpauseTimeNowU3Ed__26_tFB7F0244BCF4168EB299472ADE265C9E924DD28E  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::<>4__this
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CpauseTimeNowU3Ed__26_tFB7F0244BCF4168EB299472ADE265C9E924DD28E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CpauseTimeNowU3Ed__26_tFB7F0244BCF4168EB299472ADE265C9E924DD28E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CpauseTimeNowU3Ed__26_tFB7F0244BCF4168EB299472ADE265C9E924DD28E, ___U3CU3E4__this_2)); }
	inline TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24
struct  U3CtimeBasedTestingU3Ed__24_t595811661E70EDE9543C16FFE5F39DCB9FA9F5A4  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>4__this
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * ___U3CU3E4__this_2;
	// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0 DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>8__1
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97 * ___U3CU3E8__1_3;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<descriptionMatchCount>5__2
	int32_t ___U3CdescriptionMatchCountU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_t595811661E70EDE9543C16FFE5F39DCB9FA9F5A4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_t595811661E70EDE9543C16FFE5F39DCB9FA9F5A4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_t595811661E70EDE9543C16FFE5F39DCB9FA9F5A4, ___U3CU3E4__this_2)); }
	inline TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_t595811661E70EDE9543C16FFE5F39DCB9FA9F5A4, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97 * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97 ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97 * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E8__1_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdescriptionMatchCountU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_t595811661E70EDE9543C16FFE5F39DCB9FA9F5A4, ___U3CdescriptionMatchCountU3E5__2_4)); }
	inline int32_t get_U3CdescriptionMatchCountU3E5__2_4() const { return ___U3CdescriptionMatchCountU3E5__2_4; }
	inline int32_t* get_address_of_U3CdescriptionMatchCountU3E5__2_4() { return &___U3CdescriptionMatchCountU3E5__2_4; }
	inline void set_U3CdescriptionMatchCountU3E5__2_4(int32_t value)
	{
		___U3CdescriptionMatchCountU3E5__2_4 = value;
	}
};


// DentedPixel.LeanDummy
struct  LeanDummy_tF5204357A79E55834CC8269D24B3CF3CB87D07B9  : public RuntimeObject
{
public:

public:
};


// NLayer.Decoder.BitReservoir
struct  BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB  : public RuntimeObject
{
public:
	// System.Byte[] NLayer.Decoder.BitReservoir::_buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buf_0;
	// System.Int32 NLayer.Decoder.BitReservoir::_start
	int32_t ____start_1;
	// System.Int32 NLayer.Decoder.BitReservoir::_end
	int32_t ____end_2;
	// System.Int32 NLayer.Decoder.BitReservoir::_bitsLeft
	int32_t ____bitsLeft_3;
	// System.Int64 NLayer.Decoder.BitReservoir::_bitsRead
	int64_t ____bitsRead_4;

public:
	inline static int32_t get_offset_of__buf_0() { return static_cast<int32_t>(offsetof(BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB, ____buf_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buf_0() const { return ____buf_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buf_0() { return &____buf_0; }
	inline void set__buf_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buf_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buf_0), (void*)value);
	}

	inline static int32_t get_offset_of__start_1() { return static_cast<int32_t>(offsetof(BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB, ____start_1)); }
	inline int32_t get__start_1() const { return ____start_1; }
	inline int32_t* get_address_of__start_1() { return &____start_1; }
	inline void set__start_1(int32_t value)
	{
		____start_1 = value;
	}

	inline static int32_t get_offset_of__end_2() { return static_cast<int32_t>(offsetof(BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB, ____end_2)); }
	inline int32_t get__end_2() const { return ____end_2; }
	inline int32_t* get_address_of__end_2() { return &____end_2; }
	inline void set__end_2(int32_t value)
	{
		____end_2 = value;
	}

	inline static int32_t get_offset_of__bitsLeft_3() { return static_cast<int32_t>(offsetof(BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB, ____bitsLeft_3)); }
	inline int32_t get__bitsLeft_3() const { return ____bitsLeft_3; }
	inline int32_t* get_address_of__bitsLeft_3() { return &____bitsLeft_3; }
	inline void set__bitsLeft_3(int32_t value)
	{
		____bitsLeft_3 = value;
	}

	inline static int32_t get_offset_of__bitsRead_4() { return static_cast<int32_t>(offsetof(BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB, ____bitsRead_4)); }
	inline int64_t get__bitsRead_4() const { return ____bitsRead_4; }
	inline int64_t* get_address_of__bitsRead_4() { return &____bitsRead_4; }
	inline void set__bitsRead_4(int64_t value)
	{
		____bitsRead_4 = value;
	}
};


// NLayer.Decoder.FrameBase
struct  FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA  : public RuntimeObject
{
public:
	// System.Int64 NLayer.Decoder.FrameBase::<Offset>k__BackingField
	int64_t ___U3COffsetU3Ek__BackingField_1;
	// System.Int32 NLayer.Decoder.FrameBase::<Length>k__BackingField
	int32_t ___U3CLengthU3Ek__BackingField_2;
	// NLayer.Decoder.MpegStreamReader NLayer.Decoder.FrameBase::_reader
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40 * ____reader_3;
	// System.Byte[] NLayer.Decoder.FrameBase::_savedBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____savedBuffer_4;

public:
	inline static int32_t get_offset_of_U3COffsetU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA, ___U3COffsetU3Ek__BackingField_1)); }
	inline int64_t get_U3COffsetU3Ek__BackingField_1() const { return ___U3COffsetU3Ek__BackingField_1; }
	inline int64_t* get_address_of_U3COffsetU3Ek__BackingField_1() { return &___U3COffsetU3Ek__BackingField_1; }
	inline void set_U3COffsetU3Ek__BackingField_1(int64_t value)
	{
		___U3COffsetU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA, ___U3CLengthU3Ek__BackingField_2)); }
	inline int32_t get_U3CLengthU3Ek__BackingField_2() const { return ___U3CLengthU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CLengthU3Ek__BackingField_2() { return &___U3CLengthU3Ek__BackingField_2; }
	inline void set_U3CLengthU3Ek__BackingField_2(int32_t value)
	{
		___U3CLengthU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of__reader_3() { return static_cast<int32_t>(offsetof(FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA, ____reader_3)); }
	inline MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40 * get__reader_3() const { return ____reader_3; }
	inline MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40 ** get_address_of__reader_3() { return &____reader_3; }
	inline void set__reader_3(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40 * value)
	{
		____reader_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____reader_3), (void*)value);
	}

	inline static int32_t get_offset_of__savedBuffer_4() { return static_cast<int32_t>(offsetof(FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA, ____savedBuffer_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__savedBuffer_4() const { return ____savedBuffer_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__savedBuffer_4() { return &____savedBuffer_4; }
	inline void set__savedBuffer_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____savedBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____savedBuffer_4), (void*)value);
	}
};

struct FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA_StaticFields
{
public:
	// System.Int32 NLayer.Decoder.FrameBase::_totalAllocation
	int32_t ____totalAllocation_0;

public:
	inline static int32_t get_offset_of__totalAllocation_0() { return static_cast<int32_t>(offsetof(FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA_StaticFields, ____totalAllocation_0)); }
	inline int32_t get__totalAllocation_0() const { return ____totalAllocation_0; }
	inline int32_t* get_address_of__totalAllocation_0() { return &____totalAllocation_0; }
	inline void set__totalAllocation_0(int32_t value)
	{
		____totalAllocation_0 = value;
	}
};


// NLayer.Decoder.Huffman
struct  Huffman_t45D572D2B5E0A0C54E838E4FE31EA44E00535CFA  : public RuntimeObject
{
public:

public:
};

struct Huffman_t45D572D2B5E0A0C54E838E4FE31EA44E00535CFA_StaticFields
{
public:
	// System.Byte[0...,0...][] NLayer.Decoder.Huffman::_codeTables
	ByteU5B0___U2C0___U5DU5BU5D_tCF5D5F0A15F6B2D8B72D764107BCB74EE48ADEC4* ____codeTables_0;
	// System.Single[] NLayer.Decoder.Huffman::_floatLookup
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____floatLookup_1;
	// NLayer.Decoder.Huffman/HuffmanListNode[] NLayer.Decoder.Huffman::_llCache
	HuffmanListNodeU5BU5D_tA134A87CE2CFFD5607B297D08F348FF2C73CBD9D* ____llCache_2;
	// System.Int32[] NLayer.Decoder.Huffman::_llCacheMaxBits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____llCacheMaxBits_3;
	// System.Int32[] NLayer.Decoder.Huffman::LIN_BITS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___LIN_BITS_4;

public:
	inline static int32_t get_offset_of__codeTables_0() { return static_cast<int32_t>(offsetof(Huffman_t45D572D2B5E0A0C54E838E4FE31EA44E00535CFA_StaticFields, ____codeTables_0)); }
	inline ByteU5B0___U2C0___U5DU5BU5D_tCF5D5F0A15F6B2D8B72D764107BCB74EE48ADEC4* get__codeTables_0() const { return ____codeTables_0; }
	inline ByteU5B0___U2C0___U5DU5BU5D_tCF5D5F0A15F6B2D8B72D764107BCB74EE48ADEC4** get_address_of__codeTables_0() { return &____codeTables_0; }
	inline void set__codeTables_0(ByteU5B0___U2C0___U5DU5BU5D_tCF5D5F0A15F6B2D8B72D764107BCB74EE48ADEC4* value)
	{
		____codeTables_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____codeTables_0), (void*)value);
	}

	inline static int32_t get_offset_of__floatLookup_1() { return static_cast<int32_t>(offsetof(Huffman_t45D572D2B5E0A0C54E838E4FE31EA44E00535CFA_StaticFields, ____floatLookup_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__floatLookup_1() const { return ____floatLookup_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__floatLookup_1() { return &____floatLookup_1; }
	inline void set__floatLookup_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____floatLookup_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____floatLookup_1), (void*)value);
	}

	inline static int32_t get_offset_of__llCache_2() { return static_cast<int32_t>(offsetof(Huffman_t45D572D2B5E0A0C54E838E4FE31EA44E00535CFA_StaticFields, ____llCache_2)); }
	inline HuffmanListNodeU5BU5D_tA134A87CE2CFFD5607B297D08F348FF2C73CBD9D* get__llCache_2() const { return ____llCache_2; }
	inline HuffmanListNodeU5BU5D_tA134A87CE2CFFD5607B297D08F348FF2C73CBD9D** get_address_of__llCache_2() { return &____llCache_2; }
	inline void set__llCache_2(HuffmanListNodeU5BU5D_tA134A87CE2CFFD5607B297D08F348FF2C73CBD9D* value)
	{
		____llCache_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____llCache_2), (void*)value);
	}

	inline static int32_t get_offset_of__llCacheMaxBits_3() { return static_cast<int32_t>(offsetof(Huffman_t45D572D2B5E0A0C54E838E4FE31EA44E00535CFA_StaticFields, ____llCacheMaxBits_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__llCacheMaxBits_3() const { return ____llCacheMaxBits_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__llCacheMaxBits_3() { return &____llCacheMaxBits_3; }
	inline void set__llCacheMaxBits_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____llCacheMaxBits_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____llCacheMaxBits_3), (void*)value);
	}

	inline static int32_t get_offset_of_LIN_BITS_4() { return static_cast<int32_t>(offsetof(Huffman_t45D572D2B5E0A0C54E838E4FE31EA44E00535CFA_StaticFields, ___LIN_BITS_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_LIN_BITS_4() const { return ___LIN_BITS_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_LIN_BITS_4() { return &___LIN_BITS_4; }
	inline void set_LIN_BITS_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___LIN_BITS_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LIN_BITS_4), (void*)value);
	}
};


// NLayer.Decoder.Huffman/<>c
struct  U3CU3Ec_t8C529531DE57E8ACB6F5FB010DFAAB9E682BBA9C  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t8C529531DE57E8ACB6F5FB010DFAAB9E682BBA9C_StaticFields
{
public:
	// NLayer.Decoder.Huffman/<>c NLayer.Decoder.Huffman/<>c::<>9
	U3CU3Ec_t8C529531DE57E8ACB6F5FB010DFAAB9E682BBA9C * ___U3CU3E9_0;
	// System.Comparison`1<NLayer.Decoder.Huffman/HuffmanListNode> NLayer.Decoder.Huffman/<>c::<>9__12_0
	Comparison_1_t145272BF3F25310B6B700255B5F8557FCE90D339 * ___U3CU3E9__12_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8C529531DE57E8ACB6F5FB010DFAAB9E682BBA9C_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t8C529531DE57E8ACB6F5FB010DFAAB9E682BBA9C * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t8C529531DE57E8ACB6F5FB010DFAAB9E682BBA9C ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t8C529531DE57E8ACB6F5FB010DFAAB9E682BBA9C * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8C529531DE57E8ACB6F5FB010DFAAB9E682BBA9C_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Comparison_1_t145272BF3F25310B6B700255B5F8557FCE90D339 * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Comparison_1_t145272BF3F25310B6B700255B5F8557FCE90D339 ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Comparison_1_t145272BF3F25310B6B700255B5F8557FCE90D339 * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__12_0_1), (void*)value);
	}
};


// NLayer.Decoder.Huffman/HuffmanListNode
struct  HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2  : public RuntimeObject
{
public:
	// System.Byte NLayer.Decoder.Huffman/HuffmanListNode::Value
	uint8_t ___Value_0;
	// System.Int32 NLayer.Decoder.Huffman/HuffmanListNode::Length
	int32_t ___Length_1;
	// System.Int32 NLayer.Decoder.Huffman/HuffmanListNode::Bits
	int32_t ___Bits_2;
	// System.Int32 NLayer.Decoder.Huffman/HuffmanListNode::Mask
	int32_t ___Mask_3;
	// NLayer.Decoder.Huffman/HuffmanListNode NLayer.Decoder.Huffman/HuffmanListNode::Next
	HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2 * ___Next_4;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2, ___Value_0)); }
	inline uint8_t get_Value_0() const { return ___Value_0; }
	inline uint8_t* get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(uint8_t value)
	{
		___Value_0 = value;
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2, ___Length_1)); }
	inline int32_t get_Length_1() const { return ___Length_1; }
	inline int32_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(int32_t value)
	{
		___Length_1 = value;
	}

	inline static int32_t get_offset_of_Bits_2() { return static_cast<int32_t>(offsetof(HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2, ___Bits_2)); }
	inline int32_t get_Bits_2() const { return ___Bits_2; }
	inline int32_t* get_address_of_Bits_2() { return &___Bits_2; }
	inline void set_Bits_2(int32_t value)
	{
		___Bits_2 = value;
	}

	inline static int32_t get_offset_of_Mask_3() { return static_cast<int32_t>(offsetof(HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2, ___Mask_3)); }
	inline int32_t get_Mask_3() const { return ___Mask_3; }
	inline int32_t* get_address_of_Mask_3() { return &___Mask_3; }
	inline void set_Mask_3(int32_t value)
	{
		___Mask_3 = value;
	}

	inline static int32_t get_offset_of_Next_4() { return static_cast<int32_t>(offsetof(HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2, ___Next_4)); }
	inline HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2 * get_Next_4() const { return ___Next_4; }
	inline HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2 ** get_address_of_Next_4() { return &___Next_4; }
	inline void set_Next_4(HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2 * value)
	{
		___Next_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Next_4), (void*)value);
	}
};


// NLayer.Decoder.LayerIIIDecoder/HybridMDCT
struct  HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Single[]> NLayer.Decoder.LayerIIIDecoder/HybridMDCT::_prevBlock
	List_1_t389DEBD2C92F3A31DC21A2AE3CBEED8DDA76FF5C * ____prevBlock_3;
	// System.Collections.Generic.List`1<System.Single[]> NLayer.Decoder.LayerIIIDecoder/HybridMDCT::_nextBlock
	List_1_t389DEBD2C92F3A31DC21A2AE3CBEED8DDA76FF5C * ____nextBlock_4;
	// System.Single[] NLayer.Decoder.LayerIIIDecoder/HybridMDCT::_imdctTemp
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____imdctTemp_5;
	// System.Single[] NLayer.Decoder.LayerIIIDecoder/HybridMDCT::_imdctResult
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____imdctResult_6;

public:
	inline static int32_t get_offset_of__prevBlock_3() { return static_cast<int32_t>(offsetof(HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878, ____prevBlock_3)); }
	inline List_1_t389DEBD2C92F3A31DC21A2AE3CBEED8DDA76FF5C * get__prevBlock_3() const { return ____prevBlock_3; }
	inline List_1_t389DEBD2C92F3A31DC21A2AE3CBEED8DDA76FF5C ** get_address_of__prevBlock_3() { return &____prevBlock_3; }
	inline void set__prevBlock_3(List_1_t389DEBD2C92F3A31DC21A2AE3CBEED8DDA76FF5C * value)
	{
		____prevBlock_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____prevBlock_3), (void*)value);
	}

	inline static int32_t get_offset_of__nextBlock_4() { return static_cast<int32_t>(offsetof(HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878, ____nextBlock_4)); }
	inline List_1_t389DEBD2C92F3A31DC21A2AE3CBEED8DDA76FF5C * get__nextBlock_4() const { return ____nextBlock_4; }
	inline List_1_t389DEBD2C92F3A31DC21A2AE3CBEED8DDA76FF5C ** get_address_of__nextBlock_4() { return &____nextBlock_4; }
	inline void set__nextBlock_4(List_1_t389DEBD2C92F3A31DC21A2AE3CBEED8DDA76FF5C * value)
	{
		____nextBlock_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____nextBlock_4), (void*)value);
	}

	inline static int32_t get_offset_of__imdctTemp_5() { return static_cast<int32_t>(offsetof(HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878, ____imdctTemp_5)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__imdctTemp_5() const { return ____imdctTemp_5; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__imdctTemp_5() { return &____imdctTemp_5; }
	inline void set__imdctTemp_5(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____imdctTemp_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____imdctTemp_5), (void*)value);
	}

	inline static int32_t get_offset_of__imdctResult_6() { return static_cast<int32_t>(offsetof(HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878, ____imdctResult_6)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__imdctResult_6() const { return ____imdctResult_6; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__imdctResult_6() { return &____imdctResult_6; }
	inline void set__imdctResult_6(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____imdctResult_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____imdctResult_6), (void*)value);
	}
};

struct HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878_StaticFields
{
public:
	// System.Single[][] NLayer.Decoder.LayerIIIDecoder/HybridMDCT::_swin
	SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* ____swin_1;
	// System.Single[] NLayer.Decoder.LayerIIIDecoder/HybridMDCT::icos72_table
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___icos72_table_2;

public:
	inline static int32_t get_offset_of__swin_1() { return static_cast<int32_t>(offsetof(HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878_StaticFields, ____swin_1)); }
	inline SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* get__swin_1() const { return ____swin_1; }
	inline SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E** get_address_of__swin_1() { return &____swin_1; }
	inline void set__swin_1(SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* value)
	{
		____swin_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____swin_1), (void*)value);
	}

	inline static int32_t get_offset_of_icos72_table_2() { return static_cast<int32_t>(offsetof(HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878_StaticFields, ___icos72_table_2)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_icos72_table_2() const { return ___icos72_table_2; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_icos72_table_2() { return &___icos72_table_2; }
	inline void set_icos72_table_2(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___icos72_table_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___icos72_table_2), (void*)value);
	}
};


// NLayer.Decoder.MpegStreamReader
struct  MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40  : public RuntimeObject
{
public:
	// NLayer.Decoder.ID3Frame NLayer.Decoder.MpegStreamReader::_id3Frame
	ID3Frame_tAB95743953BDF6FD6810C1AB92F25841D41135A8 * ____id3Frame_0;
	// NLayer.Decoder.ID3Frame NLayer.Decoder.MpegStreamReader::_id3v1Frame
	ID3Frame_tAB95743953BDF6FD6810C1AB92F25841D41135A8 * ____id3v1Frame_1;
	// NLayer.Decoder.RiffHeaderFrame NLayer.Decoder.MpegStreamReader::_riffHeaderFrame
	RiffHeaderFrame_tD195C40825041F5DA7AE518B989C8EEEBB9CAC8C * ____riffHeaderFrame_2;
	// NLayer.Decoder.VBRInfo NLayer.Decoder.MpegStreamReader::_vbrInfo
	VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428 * ____vbrInfo_3;
	// NLayer.Decoder.MpegFrame NLayer.Decoder.MpegStreamReader::_first
	MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * ____first_4;
	// NLayer.Decoder.MpegFrame NLayer.Decoder.MpegStreamReader::_current
	MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * ____current_5;
	// NLayer.Decoder.MpegFrame NLayer.Decoder.MpegStreamReader::_last
	MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * ____last_6;
	// NLayer.Decoder.MpegFrame NLayer.Decoder.MpegStreamReader::_lastFree
	MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * ____lastFree_7;
	// System.Int64 NLayer.Decoder.MpegStreamReader::_readOffset
	int64_t ____readOffset_8;
	// System.Int64 NLayer.Decoder.MpegStreamReader::_eofOffset
	int64_t ____eofOffset_9;
	// System.IO.Stream NLayer.Decoder.MpegStreamReader::_source
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____source_10;
	// System.Boolean NLayer.Decoder.MpegStreamReader::_canSeek
	bool ____canSeek_11;
	// System.Boolean NLayer.Decoder.MpegStreamReader::_endFound
	bool ____endFound_12;
	// System.Boolean NLayer.Decoder.MpegStreamReader::_mixedFrameSize
	bool ____mixedFrameSize_13;
	// System.Object NLayer.Decoder.MpegStreamReader::_readLock
	RuntimeObject * ____readLock_14;
	// System.Object NLayer.Decoder.MpegStreamReader::_frameLock
	RuntimeObject * ____frameLock_15;
	// NLayer.Decoder.MpegStreamReader/ReadBuffer NLayer.Decoder.MpegStreamReader::_readBuf
	ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB * ____readBuf_16;

public:
	inline static int32_t get_offset_of__id3Frame_0() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____id3Frame_0)); }
	inline ID3Frame_tAB95743953BDF6FD6810C1AB92F25841D41135A8 * get__id3Frame_0() const { return ____id3Frame_0; }
	inline ID3Frame_tAB95743953BDF6FD6810C1AB92F25841D41135A8 ** get_address_of__id3Frame_0() { return &____id3Frame_0; }
	inline void set__id3Frame_0(ID3Frame_tAB95743953BDF6FD6810C1AB92F25841D41135A8 * value)
	{
		____id3Frame_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____id3Frame_0), (void*)value);
	}

	inline static int32_t get_offset_of__id3v1Frame_1() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____id3v1Frame_1)); }
	inline ID3Frame_tAB95743953BDF6FD6810C1AB92F25841D41135A8 * get__id3v1Frame_1() const { return ____id3v1Frame_1; }
	inline ID3Frame_tAB95743953BDF6FD6810C1AB92F25841D41135A8 ** get_address_of__id3v1Frame_1() { return &____id3v1Frame_1; }
	inline void set__id3v1Frame_1(ID3Frame_tAB95743953BDF6FD6810C1AB92F25841D41135A8 * value)
	{
		____id3v1Frame_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____id3v1Frame_1), (void*)value);
	}

	inline static int32_t get_offset_of__riffHeaderFrame_2() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____riffHeaderFrame_2)); }
	inline RiffHeaderFrame_tD195C40825041F5DA7AE518B989C8EEEBB9CAC8C * get__riffHeaderFrame_2() const { return ____riffHeaderFrame_2; }
	inline RiffHeaderFrame_tD195C40825041F5DA7AE518B989C8EEEBB9CAC8C ** get_address_of__riffHeaderFrame_2() { return &____riffHeaderFrame_2; }
	inline void set__riffHeaderFrame_2(RiffHeaderFrame_tD195C40825041F5DA7AE518B989C8EEEBB9CAC8C * value)
	{
		____riffHeaderFrame_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____riffHeaderFrame_2), (void*)value);
	}

	inline static int32_t get_offset_of__vbrInfo_3() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____vbrInfo_3)); }
	inline VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428 * get__vbrInfo_3() const { return ____vbrInfo_3; }
	inline VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428 ** get_address_of__vbrInfo_3() { return &____vbrInfo_3; }
	inline void set__vbrInfo_3(VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428 * value)
	{
		____vbrInfo_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____vbrInfo_3), (void*)value);
	}

	inline static int32_t get_offset_of__first_4() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____first_4)); }
	inline MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * get__first_4() const { return ____first_4; }
	inline MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 ** get_address_of__first_4() { return &____first_4; }
	inline void set__first_4(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * value)
	{
		____first_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____first_4), (void*)value);
	}

	inline static int32_t get_offset_of__current_5() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____current_5)); }
	inline MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * get__current_5() const { return ____current_5; }
	inline MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 ** get_address_of__current_5() { return &____current_5; }
	inline void set__current_5(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * value)
	{
		____current_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____current_5), (void*)value);
	}

	inline static int32_t get_offset_of__last_6() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____last_6)); }
	inline MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * get__last_6() const { return ____last_6; }
	inline MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 ** get_address_of__last_6() { return &____last_6; }
	inline void set__last_6(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * value)
	{
		____last_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____last_6), (void*)value);
	}

	inline static int32_t get_offset_of__lastFree_7() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____lastFree_7)); }
	inline MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * get__lastFree_7() const { return ____lastFree_7; }
	inline MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 ** get_address_of__lastFree_7() { return &____lastFree_7; }
	inline void set__lastFree_7(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * value)
	{
		____lastFree_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lastFree_7), (void*)value);
	}

	inline static int32_t get_offset_of__readOffset_8() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____readOffset_8)); }
	inline int64_t get__readOffset_8() const { return ____readOffset_8; }
	inline int64_t* get_address_of__readOffset_8() { return &____readOffset_8; }
	inline void set__readOffset_8(int64_t value)
	{
		____readOffset_8 = value;
	}

	inline static int32_t get_offset_of__eofOffset_9() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____eofOffset_9)); }
	inline int64_t get__eofOffset_9() const { return ____eofOffset_9; }
	inline int64_t* get_address_of__eofOffset_9() { return &____eofOffset_9; }
	inline void set__eofOffset_9(int64_t value)
	{
		____eofOffset_9 = value;
	}

	inline static int32_t get_offset_of__source_10() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____source_10)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__source_10() const { return ____source_10; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__source_10() { return &____source_10; }
	inline void set__source_10(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____source_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_10), (void*)value);
	}

	inline static int32_t get_offset_of__canSeek_11() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____canSeek_11)); }
	inline bool get__canSeek_11() const { return ____canSeek_11; }
	inline bool* get_address_of__canSeek_11() { return &____canSeek_11; }
	inline void set__canSeek_11(bool value)
	{
		____canSeek_11 = value;
	}

	inline static int32_t get_offset_of__endFound_12() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____endFound_12)); }
	inline bool get__endFound_12() const { return ____endFound_12; }
	inline bool* get_address_of__endFound_12() { return &____endFound_12; }
	inline void set__endFound_12(bool value)
	{
		____endFound_12 = value;
	}

	inline static int32_t get_offset_of__mixedFrameSize_13() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____mixedFrameSize_13)); }
	inline bool get__mixedFrameSize_13() const { return ____mixedFrameSize_13; }
	inline bool* get_address_of__mixedFrameSize_13() { return &____mixedFrameSize_13; }
	inline void set__mixedFrameSize_13(bool value)
	{
		____mixedFrameSize_13 = value;
	}

	inline static int32_t get_offset_of__readLock_14() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____readLock_14)); }
	inline RuntimeObject * get__readLock_14() const { return ____readLock_14; }
	inline RuntimeObject ** get_address_of__readLock_14() { return &____readLock_14; }
	inline void set__readLock_14(RuntimeObject * value)
	{
		____readLock_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____readLock_14), (void*)value);
	}

	inline static int32_t get_offset_of__frameLock_15() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____frameLock_15)); }
	inline RuntimeObject * get__frameLock_15() const { return ____frameLock_15; }
	inline RuntimeObject ** get_address_of__frameLock_15() { return &____frameLock_15; }
	inline void set__frameLock_15(RuntimeObject * value)
	{
		____frameLock_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameLock_15), (void*)value);
	}

	inline static int32_t get_offset_of__readBuf_16() { return static_cast<int32_t>(offsetof(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40, ____readBuf_16)); }
	inline ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB * get__readBuf_16() const { return ____readBuf_16; }
	inline ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB ** get_address_of__readBuf_16() { return &____readBuf_16; }
	inline void set__readBuf_16(ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB * value)
	{
		____readBuf_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____readBuf_16), (void*)value);
	}
};


// NLayer.Decoder.MpegStreamReader/ReadBuffer
struct  ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB  : public RuntimeObject
{
public:
	// System.Byte[] NLayer.Decoder.MpegStreamReader/ReadBuffer::Data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Data_0;
	// System.Int64 NLayer.Decoder.MpegStreamReader/ReadBuffer::BaseOffset
	int64_t ___BaseOffset_1;
	// System.Int32 NLayer.Decoder.MpegStreamReader/ReadBuffer::End
	int32_t ___End_2;
	// System.Int32 NLayer.Decoder.MpegStreamReader/ReadBuffer::DiscardCount
	int32_t ___DiscardCount_3;
	// System.Object NLayer.Decoder.MpegStreamReader/ReadBuffer::_localLock
	RuntimeObject * ____localLock_4;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB, ___Data_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Data_0() const { return ___Data_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Data_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Data_0), (void*)value);
	}

	inline static int32_t get_offset_of_BaseOffset_1() { return static_cast<int32_t>(offsetof(ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB, ___BaseOffset_1)); }
	inline int64_t get_BaseOffset_1() const { return ___BaseOffset_1; }
	inline int64_t* get_address_of_BaseOffset_1() { return &___BaseOffset_1; }
	inline void set_BaseOffset_1(int64_t value)
	{
		___BaseOffset_1 = value;
	}

	inline static int32_t get_offset_of_End_2() { return static_cast<int32_t>(offsetof(ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB, ___End_2)); }
	inline int32_t get_End_2() const { return ___End_2; }
	inline int32_t* get_address_of_End_2() { return &___End_2; }
	inline void set_End_2(int32_t value)
	{
		___End_2 = value;
	}

	inline static int32_t get_offset_of_DiscardCount_3() { return static_cast<int32_t>(offsetof(ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB, ___DiscardCount_3)); }
	inline int32_t get_DiscardCount_3() const { return ___DiscardCount_3; }
	inline int32_t* get_address_of_DiscardCount_3() { return &___DiscardCount_3; }
	inline void set_DiscardCount_3(int32_t value)
	{
		___DiscardCount_3 = value;
	}

	inline static int32_t get_offset_of__localLock_4() { return static_cast<int32_t>(offsetof(ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB, ____localLock_4)); }
	inline RuntimeObject * get__localLock_4() const { return ____localLock_4; }
	inline RuntimeObject ** get_address_of__localLock_4() { return &____localLock_4; }
	inline void set__localLock_4(RuntimeObject * value)
	{
		____localLock_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____localLock_4), (void*)value);
	}
};


// NLayer.Decoder.VBRInfo
struct  VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428  : public RuntimeObject
{
public:
	// System.Int32 NLayer.Decoder.VBRInfo::<SampleCount>k__BackingField
	int32_t ___U3CSampleCountU3Ek__BackingField_0;
	// System.Int32 NLayer.Decoder.VBRInfo::<SampleRate>k__BackingField
	int32_t ___U3CSampleRateU3Ek__BackingField_1;
	// System.Int32 NLayer.Decoder.VBRInfo::<Channels>k__BackingField
	int32_t ___U3CChannelsU3Ek__BackingField_2;
	// System.Int32 NLayer.Decoder.VBRInfo::<VBRFrames>k__BackingField
	int32_t ___U3CVBRFramesU3Ek__BackingField_3;
	// System.Int32 NLayer.Decoder.VBRInfo::<VBRBytes>k__BackingField
	int32_t ___U3CVBRBytesU3Ek__BackingField_4;
	// System.Int32 NLayer.Decoder.VBRInfo::<VBRQuality>k__BackingField
	int32_t ___U3CVBRQualityU3Ek__BackingField_5;
	// System.Int32 NLayer.Decoder.VBRInfo::<VBRDelay>k__BackingField
	int32_t ___U3CVBRDelayU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CSampleCountU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428, ___U3CSampleCountU3Ek__BackingField_0)); }
	inline int32_t get_U3CSampleCountU3Ek__BackingField_0() const { return ___U3CSampleCountU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CSampleCountU3Ek__BackingField_0() { return &___U3CSampleCountU3Ek__BackingField_0; }
	inline void set_U3CSampleCountU3Ek__BackingField_0(int32_t value)
	{
		___U3CSampleCountU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CSampleRateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428, ___U3CSampleRateU3Ek__BackingField_1)); }
	inline int32_t get_U3CSampleRateU3Ek__BackingField_1() const { return ___U3CSampleRateU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CSampleRateU3Ek__BackingField_1() { return &___U3CSampleRateU3Ek__BackingField_1; }
	inline void set_U3CSampleRateU3Ek__BackingField_1(int32_t value)
	{
		___U3CSampleRateU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CChannelsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428, ___U3CChannelsU3Ek__BackingField_2)); }
	inline int32_t get_U3CChannelsU3Ek__BackingField_2() const { return ___U3CChannelsU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CChannelsU3Ek__BackingField_2() { return &___U3CChannelsU3Ek__BackingField_2; }
	inline void set_U3CChannelsU3Ek__BackingField_2(int32_t value)
	{
		___U3CChannelsU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CVBRFramesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428, ___U3CVBRFramesU3Ek__BackingField_3)); }
	inline int32_t get_U3CVBRFramesU3Ek__BackingField_3() const { return ___U3CVBRFramesU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CVBRFramesU3Ek__BackingField_3() { return &___U3CVBRFramesU3Ek__BackingField_3; }
	inline void set_U3CVBRFramesU3Ek__BackingField_3(int32_t value)
	{
		___U3CVBRFramesU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CVBRBytesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428, ___U3CVBRBytesU3Ek__BackingField_4)); }
	inline int32_t get_U3CVBRBytesU3Ek__BackingField_4() const { return ___U3CVBRBytesU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CVBRBytesU3Ek__BackingField_4() { return &___U3CVBRBytesU3Ek__BackingField_4; }
	inline void set_U3CVBRBytesU3Ek__BackingField_4(int32_t value)
	{
		___U3CVBRBytesU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CVBRQualityU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428, ___U3CVBRQualityU3Ek__BackingField_5)); }
	inline int32_t get_U3CVBRQualityU3Ek__BackingField_5() const { return ___U3CVBRQualityU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CVBRQualityU3Ek__BackingField_5() { return &___U3CVBRQualityU3Ek__BackingField_5; }
	inline void set_U3CVBRQualityU3Ek__BackingField_5(int32_t value)
	{
		___U3CVBRQualityU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CVBRDelayU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428, ___U3CVBRDelayU3Ek__BackingField_6)); }
	inline int32_t get_U3CVBRDelayU3Ek__BackingField_6() const { return ___U3CVBRDelayU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CVBRDelayU3Ek__BackingField_6() { return &___U3CVBRDelayU3Ek__BackingField_6; }
	inline void set_U3CVBRDelayU3Ek__BackingField_6(int32_t value)
	{
		___U3CVBRDelayU3Ek__BackingField_6 = value;
	}
};


// NLayer.MpegFile
struct  MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC  : public RuntimeObject
{
public:
	// System.IO.Stream NLayer.MpegFile::_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____stream_0;
	// System.Boolean NLayer.MpegFile::_closeStream
	bool ____closeStream_1;
	// System.Boolean NLayer.MpegFile::_eofFound
	bool ____eofFound_2;
	// NLayer.Decoder.MpegStreamReader NLayer.MpegFile::_reader
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40 * ____reader_3;
	// NLayer.MpegFrameDecoder NLayer.MpegFile::_decoder
	MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2 * ____decoder_4;
	// System.Object NLayer.MpegFile::_seekLock
	RuntimeObject * ____seekLock_5;
	// System.Int64 NLayer.MpegFile::_position
	int64_t ____position_6;
	// System.Single[] NLayer.MpegFile::_readBuf
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____readBuf_7;
	// System.Int32 NLayer.MpegFile::_readBufLen
	int32_t ____readBufLen_8;
	// System.Int32 NLayer.MpegFile::_readBufOfs
	int32_t ____readBufOfs_9;

public:
	inline static int32_t get_offset_of__stream_0() { return static_cast<int32_t>(offsetof(MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC, ____stream_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__stream_0() const { return ____stream_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__stream_0() { return &____stream_0; }
	inline void set__stream_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____stream_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stream_0), (void*)value);
	}

	inline static int32_t get_offset_of__closeStream_1() { return static_cast<int32_t>(offsetof(MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC, ____closeStream_1)); }
	inline bool get__closeStream_1() const { return ____closeStream_1; }
	inline bool* get_address_of__closeStream_1() { return &____closeStream_1; }
	inline void set__closeStream_1(bool value)
	{
		____closeStream_1 = value;
	}

	inline static int32_t get_offset_of__eofFound_2() { return static_cast<int32_t>(offsetof(MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC, ____eofFound_2)); }
	inline bool get__eofFound_2() const { return ____eofFound_2; }
	inline bool* get_address_of__eofFound_2() { return &____eofFound_2; }
	inline void set__eofFound_2(bool value)
	{
		____eofFound_2 = value;
	}

	inline static int32_t get_offset_of__reader_3() { return static_cast<int32_t>(offsetof(MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC, ____reader_3)); }
	inline MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40 * get__reader_3() const { return ____reader_3; }
	inline MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40 ** get_address_of__reader_3() { return &____reader_3; }
	inline void set__reader_3(MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40 * value)
	{
		____reader_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____reader_3), (void*)value);
	}

	inline static int32_t get_offset_of__decoder_4() { return static_cast<int32_t>(offsetof(MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC, ____decoder_4)); }
	inline MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2 * get__decoder_4() const { return ____decoder_4; }
	inline MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2 ** get_address_of__decoder_4() { return &____decoder_4; }
	inline void set__decoder_4(MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2 * value)
	{
		____decoder_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____decoder_4), (void*)value);
	}

	inline static int32_t get_offset_of__seekLock_5() { return static_cast<int32_t>(offsetof(MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC, ____seekLock_5)); }
	inline RuntimeObject * get__seekLock_5() const { return ____seekLock_5; }
	inline RuntimeObject ** get_address_of__seekLock_5() { return &____seekLock_5; }
	inline void set__seekLock_5(RuntimeObject * value)
	{
		____seekLock_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____seekLock_5), (void*)value);
	}

	inline static int32_t get_offset_of__position_6() { return static_cast<int32_t>(offsetof(MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC, ____position_6)); }
	inline int64_t get__position_6() const { return ____position_6; }
	inline int64_t* get_address_of__position_6() { return &____position_6; }
	inline void set__position_6(int64_t value)
	{
		____position_6 = value;
	}

	inline static int32_t get_offset_of__readBuf_7() { return static_cast<int32_t>(offsetof(MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC, ____readBuf_7)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__readBuf_7() const { return ____readBuf_7; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__readBuf_7() { return &____readBuf_7; }
	inline void set__readBuf_7(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____readBuf_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____readBuf_7), (void*)value);
	}

	inline static int32_t get_offset_of__readBufLen_8() { return static_cast<int32_t>(offsetof(MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC, ____readBufLen_8)); }
	inline int32_t get__readBufLen_8() const { return ____readBufLen_8; }
	inline int32_t* get_address_of__readBufLen_8() { return &____readBufLen_8; }
	inline void set__readBufLen_8(int32_t value)
	{
		____readBufLen_8 = value;
	}

	inline static int32_t get_offset_of__readBufOfs_9() { return static_cast<int32_t>(offsetof(MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC, ____readBufOfs_9)); }
	inline int32_t get__readBufOfs_9() const { return ____readBufOfs_9; }
	inline int32_t* get_address_of__readBufOfs_9() { return &____readBufOfs_9; }
	inline void set__readBufOfs_9(int32_t value)
	{
		____readBufOfs_9 = value;
	}
};


// NatSuite.Examples.Components.CameraPreview/<>c
struct  U3CU3Ec_t2D6BAFF26CEBC190E8316071402B17B5D9C4E5D6  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2D6BAFF26CEBC190E8316071402B17B5D9C4E5D6_StaticFields
{
public:
	// NatSuite.Examples.Components.CameraPreview/<>c NatSuite.Examples.Components.CameraPreview/<>c::<>9
	U3CU3Ec_t2D6BAFF26CEBC190E8316071402B17B5D9C4E5D6 * ___U3CU3E9_0;
	// System.Func`1<System.Boolean> NatSuite.Examples.Components.CameraPreview/<>c::<>9__6_0
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2D6BAFF26CEBC190E8316071402B17B5D9C4E5D6_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2D6BAFF26CEBC190E8316071402B17B5D9C4E5D6 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2D6BAFF26CEBC190E8316071402B17B5D9C4E5D6 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2D6BAFF26CEBC190E8316071402B17B5D9C4E5D6 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2D6BAFF26CEBC190E8316071402B17B5D9C4E5D6_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__6_0_1), (void*)value);
	}
};


// NatSuite.Examples.Components.CameraPreview/<Start>d__6
struct  U3CStartU3Ed__6_t12D537C13EA3027063A6CB61F4714C598B9F2D43  : public RuntimeObject
{
public:
	// System.Int32 NatSuite.Examples.Components.CameraPreview/<Start>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NatSuite.Examples.Components.CameraPreview/<Start>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// NatSuite.Examples.Components.CameraPreview NatSuite.Examples.Components.CameraPreview/<Start>d__6::<>4__this
	CameraPreview_t14366C64F8D63434B92D4F4DE771EDEE07EAE89C * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__6_t12D537C13EA3027063A6CB61F4714C598B9F2D43, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__6_t12D537C13EA3027063A6CB61F4714C598B9F2D43, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__6_t12D537C13EA3027063A6CB61F4714C598B9F2D43, ___U3CU3E4__this_2)); }
	inline CameraPreview_t14366C64F8D63434B92D4F4DE771EDEE07EAE89C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CameraPreview_t14366C64F8D63434B92D4F4DE771EDEE07EAE89C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CameraPreview_t14366C64F8D63434B92D4F4DE771EDEE07EAE89C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// NatSuite.Examples.Components.RecordButton/<Countdown>d__13
struct  U3CCountdownU3Ed__13_t1BB74FE43DC79072927C6AC6C880E97878B9D413  : public RuntimeObject
{
public:
	// System.Int32 NatSuite.Examples.Components.RecordButton/<Countdown>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NatSuite.Examples.Components.RecordButton/<Countdown>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// NatSuite.Examples.Components.RecordButton NatSuite.Examples.Components.RecordButton/<Countdown>d__13::<>4__this
	RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D * ___U3CU3E4__this_2;
	// System.Single NatSuite.Examples.Components.RecordButton/<Countdown>d__13::<startTime>5__2
	float ___U3CstartTimeU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCountdownU3Ed__13_t1BB74FE43DC79072927C6AC6C880E97878B9D413, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCountdownU3Ed__13_t1BB74FE43DC79072927C6AC6C880E97878B9D413, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCountdownU3Ed__13_t1BB74FE43DC79072927C6AC6C880E97878B9D413, ___U3CU3E4__this_2)); }
	inline RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CCountdownU3Ed__13_t1BB74FE43DC79072927C6AC6C880E97878B9D413, ___U3CstartTimeU3E5__2_3)); }
	inline float get_U3CstartTimeU3E5__2_3() const { return ___U3CstartTimeU3E5__2_3; }
	inline float* get_address_of_U3CstartTimeU3E5__2_3() { return &___U3CstartTimeU3E5__2_3; }
	inline void set_U3CstartTimeU3E5__2_3(float value)
	{
		___U3CstartTimeU3E5__2_3 = value;
	}
};


// NatSuite.Examples.ReplayCam/<>c
struct  U3CU3Ec_t6A10C091B0B7F58EACA4EF20DE59E51FB30A338B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6A10C091B0B7F58EACA4EF20DE59E51FB30A338B_StaticFields
{
public:
	// NatSuite.Examples.ReplayCam/<>c NatSuite.Examples.ReplayCam/<>c::<>9
	U3CU3Ec_t6A10C091B0B7F58EACA4EF20DE59E51FB30A338B * ___U3CU3E9_0;
	// System.Func`1<System.Boolean> NatSuite.Examples.ReplayCam/<>c::<>9__10_0
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ___U3CU3E9__10_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6A10C091B0B7F58EACA4EF20DE59E51FB30A338B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6A10C091B0B7F58EACA4EF20DE59E51FB30A338B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6A10C091B0B7F58EACA4EF20DE59E51FB30A338B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6A10C091B0B7F58EACA4EF20DE59E51FB30A338B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6A10C091B0B7F58EACA4EF20DE59E51FB30A338B_StaticFields, ___U3CU3E9__10_0_1)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get_U3CU3E9__10_0_1() const { return ___U3CU3E9__10_0_1; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of_U3CU3E9__10_0_1() { return &___U3CU3E9__10_0_1; }
	inline void set_U3CU3E9__10_0_1(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		___U3CU3E9__10_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__10_0_1), (void*)value);
	}
};


// NatSuite.Examples.ReplayCam/<Start>d__10
struct  U3CStartU3Ed__10_tDE1CC8D7C86FD20D03525680F18B0D1A1018225C  : public RuntimeObject
{
public:
	// System.Int32 NatSuite.Examples.ReplayCam/<Start>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NatSuite.Examples.ReplayCam/<Start>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// NatSuite.Examples.ReplayCam NatSuite.Examples.ReplayCam/<Start>d__10::<>4__this
	ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_tDE1CC8D7C86FD20D03525680F18B0D1A1018225C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_tDE1CC8D7C86FD20D03525680F18B0D1A1018225C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_tDE1CC8D7C86FD20D03525680F18B0D1A1018225C, ___U3CU3E4__this_2)); }
	inline ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// NatSuite.Recorders.Clocks.FixedIntervalClock
struct  FixedIntervalClock_t9B5E58E18BEAA393B60708928FC6ABFA2B001820  : public RuntimeObject
{
public:
	// System.Double NatSuite.Recorders.Clocks.FixedIntervalClock::<interval>k__BackingField
	double ___U3CintervalU3Ek__BackingField_0;
	// System.Boolean NatSuite.Recorders.Clocks.FixedIntervalClock::autoTick
	bool ___autoTick_1;
	// System.Int32 NatSuite.Recorders.Clocks.FixedIntervalClock::ticks
	int32_t ___ticks_2;

public:
	inline static int32_t get_offset_of_U3CintervalU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FixedIntervalClock_t9B5E58E18BEAA393B60708928FC6ABFA2B001820, ___U3CintervalU3Ek__BackingField_0)); }
	inline double get_U3CintervalU3Ek__BackingField_0() const { return ___U3CintervalU3Ek__BackingField_0; }
	inline double* get_address_of_U3CintervalU3Ek__BackingField_0() { return &___U3CintervalU3Ek__BackingField_0; }
	inline void set_U3CintervalU3Ek__BackingField_0(double value)
	{
		___U3CintervalU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_autoTick_1() { return static_cast<int32_t>(offsetof(FixedIntervalClock_t9B5E58E18BEAA393B60708928FC6ABFA2B001820, ___autoTick_1)); }
	inline bool get_autoTick_1() const { return ___autoTick_1; }
	inline bool* get_address_of_autoTick_1() { return &___autoTick_1; }
	inline void set_autoTick_1(bool value)
	{
		___autoTick_1 = value;
	}

	inline static int32_t get_offset_of_ticks_2() { return static_cast<int32_t>(offsetof(FixedIntervalClock_t9B5E58E18BEAA393B60708928FC6ABFA2B001820, ___ticks_2)); }
	inline int32_t get_ticks_2() const { return ___ticks_2; }
	inline int32_t* get_address_of_ticks_2() { return &___ticks_2; }
	inline void set_ticks_2(int32_t value)
	{
		___ticks_2 = value;
	}
};


// NatSuite.Recorders.Clocks.RealtimeClock
struct  RealtimeClock_tD38272D4375095BF717959322C645000A0C954ED  : public RuntimeObject
{
public:
	// System.Diagnostics.Stopwatch NatSuite.Recorders.Clocks.RealtimeClock::stopwatch
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * ___stopwatch_0;

public:
	inline static int32_t get_offset_of_stopwatch_0() { return static_cast<int32_t>(offsetof(RealtimeClock_tD38272D4375095BF717959322C645000A0C954ED, ___stopwatch_0)); }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * get_stopwatch_0() const { return ___stopwatch_0; }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 ** get_address_of_stopwatch_0() { return &___stopwatch_0; }
	inline void set_stopwatch_0(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * value)
	{
		___stopwatch_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stopwatch_0), (void*)value);
	}
};


// NatSuite.Recorders.Inputs.AudioInput
struct  AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF  : public RuntimeObject
{
public:
	// NatSuite.Recorders.IMediaRecorder NatSuite.Recorders.Inputs.AudioInput::recorder
	RuntimeObject* ___recorder_0;
	// NatSuite.Recorders.Clocks.IClock NatSuite.Recorders.Inputs.AudioInput::clock
	RuntimeObject* ___clock_1;
	// NatSuite.Recorders.Inputs.AudioInput/AudioInputAttachment NatSuite.Recorders.Inputs.AudioInput::attachment
	AudioInputAttachment_tBD4C16D24B6BB510C28C62437F470268DD97F38B * ___attachment_2;
	// System.Boolean NatSuite.Recorders.Inputs.AudioInput::mute
	bool ___mute_3;

public:
	inline static int32_t get_offset_of_recorder_0() { return static_cast<int32_t>(offsetof(AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF, ___recorder_0)); }
	inline RuntimeObject* get_recorder_0() const { return ___recorder_0; }
	inline RuntimeObject** get_address_of_recorder_0() { return &___recorder_0; }
	inline void set_recorder_0(RuntimeObject* value)
	{
		___recorder_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___recorder_0), (void*)value);
	}

	inline static int32_t get_offset_of_clock_1() { return static_cast<int32_t>(offsetof(AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF, ___clock_1)); }
	inline RuntimeObject* get_clock_1() const { return ___clock_1; }
	inline RuntimeObject** get_address_of_clock_1() { return &___clock_1; }
	inline void set_clock_1(RuntimeObject* value)
	{
		___clock_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clock_1), (void*)value);
	}

	inline static int32_t get_offset_of_attachment_2() { return static_cast<int32_t>(offsetof(AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF, ___attachment_2)); }
	inline AudioInputAttachment_tBD4C16D24B6BB510C28C62437F470268DD97F38B * get_attachment_2() const { return ___attachment_2; }
	inline AudioInputAttachment_tBD4C16D24B6BB510C28C62437F470268DD97F38B ** get_address_of_attachment_2() { return &___attachment_2; }
	inline void set_attachment_2(AudioInputAttachment_tBD4C16D24B6BB510C28C62437F470268DD97F38B * value)
	{
		___attachment_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___attachment_2), (void*)value);
	}

	inline static int32_t get_offset_of_mute_3() { return static_cast<int32_t>(offsetof(AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF, ___mute_3)); }
	inline bool get_mute_3() const { return ___mute_3; }
	inline bool* get_address_of_mute_3() { return &___mute_3; }
	inline void set_mute_3(bool value)
	{
		___mute_3 = value;
	}
};


// NatSuite.Recorders.Inputs.CameraInput/<>c
struct  U3CU3Ec_tC0939739735059A80353C8C1C499B9E1BFF18280  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tC0939739735059A80353C8C1C499B9E1BFF18280_StaticFields
{
public:
	// NatSuite.Recorders.Inputs.CameraInput/<>c NatSuite.Recorders.Inputs.CameraInput/<>c::<>9
	U3CU3Ec_tC0939739735059A80353C8C1C499B9E1BFF18280 * ___U3CU3E9_0;
	// System.Comparison`1<UnityEngine.Camera> NatSuite.Recorders.Inputs.CameraInput/<>c::<>9__1_0
	Comparison_1_t22D55C0E9B690772B778163906A02DEE7E89FFA5 * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC0939739735059A80353C8C1C499B9E1BFF18280_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tC0939739735059A80353C8C1C499B9E1BFF18280 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tC0939739735059A80353C8C1C499B9E1BFF18280 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tC0939739735059A80353C8C1C499B9E1BFF18280 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC0939739735059A80353C8C1C499B9E1BFF18280_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Comparison_1_t22D55C0E9B690772B778163906A02DEE7E89FFA5 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Comparison_1_t22D55C0E9B690772B778163906A02DEE7E89FFA5 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Comparison_1_t22D55C0E9B690772B778163906A02DEE7E89FFA5 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__1_0_1), (void*)value);
	}
};


// NatSuite.Recorders.Inputs.CameraInput/<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t19832433766A7855A211E71E6FD20B6C30613D2C  : public RuntimeObject
{
public:
	// System.Int64 NatSuite.Recorders.Inputs.CameraInput/<>c__DisplayClass11_0::timestamp
	int64_t ___timestamp_0;
	// NatSuite.Recorders.Inputs.CameraInput NatSuite.Recorders.Inputs.CameraInput/<>c__DisplayClass11_0::<>4__this
	CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t19832433766A7855A211E71E6FD20B6C30613D2C, ___timestamp_0)); }
	inline int64_t get_timestamp_0() const { return ___timestamp_0; }
	inline int64_t* get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(int64_t value)
	{
		___timestamp_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t19832433766A7855A211E71E6FD20B6C30613D2C, ___U3CU3E4__this_1)); }
	inline CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// NatSuite.Recorders.Inputs.CameraInput/<OnFrame>d__11
struct  U3COnFrameU3Ed__11_tB518C4F529E311E9780F6FF80E33F41D7F927375  : public RuntimeObject
{
public:
	// System.Int32 NatSuite.Recorders.Inputs.CameraInput/<OnFrame>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NatSuite.Recorders.Inputs.CameraInput/<OnFrame>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// NatSuite.Recorders.Inputs.CameraInput NatSuite.Recorders.Inputs.CameraInput/<OnFrame>d__11::<>4__this
	CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 * ___U3CU3E4__this_2;
	// NatSuite.Recorders.Inputs.CameraInput/<>c__DisplayClass11_0 NatSuite.Recorders.Inputs.CameraInput/<OnFrame>d__11::<>8__1
	U3CU3Ec__DisplayClass11_0_t19832433766A7855A211E71E6FD20B6C30613D2C * ___U3CU3E8__1_3;
	// UnityEngine.WaitForEndOfFrame NatSuite.Recorders.Inputs.CameraInput/<OnFrame>d__11::<endOfFrame>5__2
	WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * ___U3CendOfFrameU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnFrameU3Ed__11_tB518C4F529E311E9780F6FF80E33F41D7F927375, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3COnFrameU3Ed__11_tB518C4F529E311E9780F6FF80E33F41D7F927375, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3COnFrameU3Ed__11_tB518C4F529E311E9780F6FF80E33F41D7F927375, ___U3CU3E4__this_2)); }
	inline CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3COnFrameU3Ed__11_tB518C4F529E311E9780F6FF80E33F41D7F927375, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass11_0_t19832433766A7855A211E71E6FD20B6C30613D2C * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass11_0_t19832433766A7855A211E71E6FD20B6C30613D2C ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass11_0_t19832433766A7855A211E71E6FD20B6C30613D2C * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E8__1_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CendOfFrameU3E5__2_4() { return static_cast<int32_t>(offsetof(U3COnFrameU3Ed__11_tB518C4F529E311E9780F6FF80E33F41D7F927375, ___U3CendOfFrameU3E5__2_4)); }
	inline WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * get_U3CendOfFrameU3E5__2_4() const { return ___U3CendOfFrameU3E5__2_4; }
	inline WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA ** get_address_of_U3CendOfFrameU3E5__2_4() { return &___U3CendOfFrameU3E5__2_4; }
	inline void set_U3CendOfFrameU3E5__2_4(WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * value)
	{
		___U3CendOfFrameU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CendOfFrameU3E5__2_4), (void*)value);
	}
};


// NatSuite.Recorders.Internal.Bridge
struct  Bridge_t72201E3009ACF44D05451C0D299A20DBE9283230  : public RuntimeObject
{
public:

public:
};


// NatSuite.Recorders.Internal.Utility
struct  Utility_tEED46C1B403EC0D670D68290D5D83957973E203D  : public RuntimeObject
{
public:

public:
};

struct Utility_tEED46C1B403EC0D670D68290D5D83957973E203D_StaticFields
{
public:
	// System.String NatSuite.Recorders.Internal.Utility::directory
	String_t* ___directory_0;

public:
	inline static int32_t get_offset_of_directory_0() { return static_cast<int32_t>(offsetof(Utility_tEED46C1B403EC0D670D68290D5D83957973E203D_StaticFields, ___directory_0)); }
	inline String_t* get_directory_0() const { return ___directory_0; }
	inline String_t** get_address_of_directory_0() { return &___directory_0; }
	inline void set_directory_0(String_t* value)
	{
		___directory_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___directory_0), (void*)value);
	}
};


// NatSuite.Recorders.JPGRecorder
struct  JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D NatSuite.Recorders.JPGRecorder::framebuffer
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___framebuffer_0;
	// System.String NatSuite.Recorders.JPGRecorder::recordingPath
	String_t* ___recordingPath_1;
	// System.Collections.Generic.List`1<System.Threading.Tasks.Task> NatSuite.Recorders.JPGRecorder::writeTasks
	List_1_tC62C1E1B0AD84992F0A374A5A4679609955E117E * ___writeTasks_2;
	// System.Int32 NatSuite.Recorders.JPGRecorder::frameCount
	int32_t ___frameCount_3;

public:
	inline static int32_t get_offset_of_framebuffer_0() { return static_cast<int32_t>(offsetof(JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7, ___framebuffer_0)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_framebuffer_0() const { return ___framebuffer_0; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_framebuffer_0() { return &___framebuffer_0; }
	inline void set_framebuffer_0(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___framebuffer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___framebuffer_0), (void*)value);
	}

	inline static int32_t get_offset_of_recordingPath_1() { return static_cast<int32_t>(offsetof(JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7, ___recordingPath_1)); }
	inline String_t* get_recordingPath_1() const { return ___recordingPath_1; }
	inline String_t** get_address_of_recordingPath_1() { return &___recordingPath_1; }
	inline void set_recordingPath_1(String_t* value)
	{
		___recordingPath_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___recordingPath_1), (void*)value);
	}

	inline static int32_t get_offset_of_writeTasks_2() { return static_cast<int32_t>(offsetof(JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7, ___writeTasks_2)); }
	inline List_1_tC62C1E1B0AD84992F0A374A5A4679609955E117E * get_writeTasks_2() const { return ___writeTasks_2; }
	inline List_1_tC62C1E1B0AD84992F0A374A5A4679609955E117E ** get_address_of_writeTasks_2() { return &___writeTasks_2; }
	inline void set_writeTasks_2(List_1_tC62C1E1B0AD84992F0A374A5A4679609955E117E * value)
	{
		___writeTasks_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___writeTasks_2), (void*)value);
	}

	inline static int32_t get_offset_of_frameCount_3() { return static_cast<int32_t>(offsetof(JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7, ___frameCount_3)); }
	inline int32_t get_frameCount_3() const { return ___frameCount_3; }
	inline int32_t* get_address_of_frameCount_3() { return &___frameCount_3; }
	inline void set_frameCount_3(int32_t value)
	{
		___frameCount_3 = value;
	}
};


// NatSuite.Recorders.JPGRecorder/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_tDA27A42B0978F6B299DA38B87701B7E7D4EA25ED  : public RuntimeObject
{
public:
	// NatSuite.Recorders.JPGRecorder NatSuite.Recorders.JPGRecorder/<>c__DisplayClass4_0::<>4__this
	JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7 * ___U3CU3E4__this_0;
	// System.Int32 NatSuite.Recorders.JPGRecorder/<>c__DisplayClass4_0::frameIndex
	int32_t ___frameIndex_1;
	// System.Byte[] NatSuite.Recorders.JPGRecorder/<>c__DisplayClass4_0::frameData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___frameData_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tDA27A42B0978F6B299DA38B87701B7E7D4EA25ED, ___U3CU3E4__this_0)); }
	inline JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_frameIndex_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tDA27A42B0978F6B299DA38B87701B7E7D4EA25ED, ___frameIndex_1)); }
	inline int32_t get_frameIndex_1() const { return ___frameIndex_1; }
	inline int32_t* get_address_of_frameIndex_1() { return &___frameIndex_1; }
	inline void set_frameIndex_1(int32_t value)
	{
		___frameIndex_1 = value;
	}

	inline static int32_t get_offset_of_frameData_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tDA27A42B0978F6B299DA38B87701B7E7D4EA25ED, ___frameData_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_frameData_2() const { return ___frameData_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_frameData_2() { return &___frameData_2; }
	inline void set_frameData_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___frameData_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frameData_2), (void*)value);
	}
};


// NatSuite.Recorders.MP4Recorder
struct  MP4Recorder_tBF21F6E0118C545DB1DF7089E708E1B4BB85492A  : public RuntimeObject
{
public:
	// NatSuite.Recorders.IMediaRecorder NatSuite.Recorders.MP4Recorder::recorder
	RuntimeObject* ___recorder_0;

public:
	inline static int32_t get_offset_of_recorder_0() { return static_cast<int32_t>(offsetof(MP4Recorder_tBF21F6E0118C545DB1DF7089E708E1B4BB85492A, ___recorder_0)); }
	inline RuntimeObject* get_recorder_0() const { return ___recorder_0; }
	inline RuntimeObject** get_address_of_recorder_0() { return &___recorder_0; }
	inline void set_recorder_0(RuntimeObject* value)
	{
		___recorder_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___recorder_0), (void*)value);
	}
};


// NatSuite.Recorders.MP4Recorder/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA  : public RuntimeObject
{
public:
	// System.Int32 NatSuite.Recorders.MP4Recorder/<>c__DisplayClass2_0::width
	int32_t ___width_0;
	// System.Int32 NatSuite.Recorders.MP4Recorder/<>c__DisplayClass2_0::height
	int32_t ___height_1;
	// System.Single NatSuite.Recorders.MP4Recorder/<>c__DisplayClass2_0::frameRate
	float ___frameRate_2;
	// System.Int32 NatSuite.Recorders.MP4Recorder/<>c__DisplayClass2_0::bitrate
	int32_t ___bitrate_3;
	// System.Int32 NatSuite.Recorders.MP4Recorder/<>c__DisplayClass2_0::keyframeInterval
	int32_t ___keyframeInterval_4;
	// System.Int32 NatSuite.Recorders.MP4Recorder/<>c__DisplayClass2_0::sampleRate
	int32_t ___sampleRate_5;
	// System.Int32 NatSuite.Recorders.MP4Recorder/<>c__DisplayClass2_0::channelCount
	int32_t ___channelCount_6;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_frameRate_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA, ___frameRate_2)); }
	inline float get_frameRate_2() const { return ___frameRate_2; }
	inline float* get_address_of_frameRate_2() { return &___frameRate_2; }
	inline void set_frameRate_2(float value)
	{
		___frameRate_2 = value;
	}

	inline static int32_t get_offset_of_bitrate_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA, ___bitrate_3)); }
	inline int32_t get_bitrate_3() const { return ___bitrate_3; }
	inline int32_t* get_address_of_bitrate_3() { return &___bitrate_3; }
	inline void set_bitrate_3(int32_t value)
	{
		___bitrate_3 = value;
	}

	inline static int32_t get_offset_of_keyframeInterval_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA, ___keyframeInterval_4)); }
	inline int32_t get_keyframeInterval_4() const { return ___keyframeInterval_4; }
	inline int32_t* get_address_of_keyframeInterval_4() { return &___keyframeInterval_4; }
	inline void set_keyframeInterval_4(int32_t value)
	{
		___keyframeInterval_4 = value;
	}

	inline static int32_t get_offset_of_sampleRate_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA, ___sampleRate_5)); }
	inline int32_t get_sampleRate_5() const { return ___sampleRate_5; }
	inline int32_t* get_address_of_sampleRate_5() { return &___sampleRate_5; }
	inline void set_sampleRate_5(int32_t value)
	{
		___sampleRate_5 = value;
	}

	inline static int32_t get_offset_of_channelCount_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA, ___channelCount_6)); }
	inline int32_t get_channelCount_6() const { return ___channelCount_6; }
	inline int32_t* get_address_of_channelCount_6() { return &___channelCount_6; }
	inline void set_channelCount_6(int32_t value)
	{
		___channelCount_6 = value;
	}
};


// NatSuite.Recorders.WAVRecorder
struct  WAVRecorder_tFCA0A292E50A43554D1DE1EE220B9F4A81BFE266  : public RuntimeObject
{
public:
	// System.Int32 NatSuite.Recorders.WAVRecorder::sampleRate
	int32_t ___sampleRate_0;
	// System.Int32 NatSuite.Recorders.WAVRecorder::channelCount
	int32_t ___channelCount_1;
	// System.IO.FileStream NatSuite.Recorders.WAVRecorder::stream
	FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * ___stream_2;
	// System.Int32 NatSuite.Recorders.WAVRecorder::sampleCount
	int32_t ___sampleCount_3;

public:
	inline static int32_t get_offset_of_sampleRate_0() { return static_cast<int32_t>(offsetof(WAVRecorder_tFCA0A292E50A43554D1DE1EE220B9F4A81BFE266, ___sampleRate_0)); }
	inline int32_t get_sampleRate_0() const { return ___sampleRate_0; }
	inline int32_t* get_address_of_sampleRate_0() { return &___sampleRate_0; }
	inline void set_sampleRate_0(int32_t value)
	{
		___sampleRate_0 = value;
	}

	inline static int32_t get_offset_of_channelCount_1() { return static_cast<int32_t>(offsetof(WAVRecorder_tFCA0A292E50A43554D1DE1EE220B9F4A81BFE266, ___channelCount_1)); }
	inline int32_t get_channelCount_1() const { return ___channelCount_1; }
	inline int32_t* get_address_of_channelCount_1() { return &___channelCount_1; }
	inline void set_channelCount_1(int32_t value)
	{
		___channelCount_1 = value;
	}

	inline static int32_t get_offset_of_stream_2() { return static_cast<int32_t>(offsetof(WAVRecorder_tFCA0A292E50A43554D1DE1EE220B9F4A81BFE266, ___stream_2)); }
	inline FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * get_stream_2() const { return ___stream_2; }
	inline FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 ** get_address_of_stream_2() { return &___stream_2; }
	inline void set_stream_2(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * value)
	{
		___stream_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stream_2), (void*)value);
	}

	inline static int32_t get_offset_of_sampleCount_3() { return static_cast<int32_t>(offsetof(WAVRecorder_tFCA0A292E50A43554D1DE1EE220B9F4A81BFE266, ___sampleCount_3)); }
	inline int32_t get_sampleCount_3() const { return ___sampleCount_3; }
	inline int32_t* get_address_of_sampleCount_3() { return &___sampleCount_3; }
	inline void set_sampleCount_3(int32_t value)
	{
		___sampleCount_3 = value;
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=1022
struct  __StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140__padding[1022];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=1024
struct  __StaticArrayInitTypeSizeU3D1024_tC455CA8308094CC5E6ADD1A24E321B05B3134F97 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D1024_tC455CA8308094CC5E6ADD1A24E321B05B3134F97__padding[1024];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=108
struct  __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF__padding[108];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91__padding[12];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120
struct  __StaticArrayInitTypeSizeU3D120_tFE429A3C8C8CC2AA48009B28DC92C5FAC405B976 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_tFE429A3C8C8CC2AA48009B28DC92C5FAC405B976__padding[120];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=124
struct  __StaticArrayInitTypeSizeU3D124_tD3D6FDB698030D188404AEB38EF634BB404CBB7C 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D124_tD3D6FDB698030D188404AEB38EF634BB404CBB7C__padding[124];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128
struct  __StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726__padding[128];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=14
struct  __StaticArrayInitTypeSizeU3D14_t8CCDC05F13040DD154D81376695EEB6BA93ACABA 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D14_t8CCDC05F13040DD154D81376695EEB6BA93ACABA__padding[14];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=140
struct  __StaticArrayInitTypeSizeU3D140_t3F4097685F037DBBFCD03ED53B59913F53BBFAAC 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D140_t3F4097685F037DBBFCD03ED53B59913F53BBFAAC__padding[140];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=142
struct  __StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61__padding[142];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16
struct  __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D__padding[16];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20
struct  __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382__padding[20];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=2048
struct  __StaticArrayInitTypeSizeU3D2048_t23ED45646FF6C31C0F8587980EEB1123BC08E49E 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D2048_t23ED45646FF6C31C0F8587980EEB1123BC08E49E__padding[2048];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34__padding[24];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=254
struct  __StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D__padding[254];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256
struct  __StaticArrayInitTypeSizeU3D256_t6F967FC9ACA9D51E8C10E3FEF9B3C0033CEB171F 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D256_t6F967FC9ACA9D51E8C10E3FEF9B3C0033CEB171F__padding[256];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28
struct  __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1__padding[28];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct  __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315__padding[32];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=34
struct  __StaticArrayInitTypeSizeU3D34_t9C713A05A7B6B5C6D004D41B59A4F6B7D561459A 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D34_t9C713A05A7B6B5C6D004D41B59A4F6B7D561459A__padding[34];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36
struct  __StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647__padding[36];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44
struct  __StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D__padding[44];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=48
struct  __StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6__padding[48];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=56
struct  __StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816__padding[56];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=60
struct  __StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF__padding[60];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=62
struct  __StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE__padding[62];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64
struct  __StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0__padding[64];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=68
struct  __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D__padding[68];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=88
struct  __StaticArrayInitTypeSizeU3D88_tACC0D3E292CC0817B7908ECD8382C424C141A9C9 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D88_tACC0D3E292CC0817B7908ECD8382C424C141A9C9__padding[88];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=92
struct  __StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946__padding[92];
	};

public:
};


// NLayer.Decoder.ID3Frame
struct  ID3Frame_tAB95743953BDF6FD6810C1AB92F25841D41135A8  : public FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA
{
public:
	// System.Int32 NLayer.Decoder.ID3Frame::_version
	int32_t ____version_5;

public:
	inline static int32_t get_offset_of__version_5() { return static_cast<int32_t>(offsetof(ID3Frame_tAB95743953BDF6FD6810C1AB92F25841D41135A8, ____version_5)); }
	inline int32_t get__version_5() const { return ____version_5; }
	inline int32_t* get_address_of__version_5() { return &____version_5; }
	inline void set__version_5(int32_t value)
	{
		____version_5 = value;
	}
};


// NLayer.Decoder.MpegFrame
struct  MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51  : public FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA
{
public:
	// NLayer.Decoder.MpegFrame NLayer.Decoder.MpegFrame::Next
	MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * ___Next_6;
	// System.Int32 NLayer.Decoder.MpegFrame::Number
	int32_t ___Number_7;
	// System.Int32 NLayer.Decoder.MpegFrame::_syncBits
	int32_t ____syncBits_8;
	// System.Int32 NLayer.Decoder.MpegFrame::_readOffset
	int32_t ____readOffset_9;
	// System.Int32 NLayer.Decoder.MpegFrame::_bitsRead
	int32_t ____bitsRead_10;
	// System.UInt64 NLayer.Decoder.MpegFrame::_bitBucket
	uint64_t ____bitBucket_11;
	// System.Int64 NLayer.Decoder.MpegFrame::_offset
	int64_t ____offset_12;
	// System.Boolean NLayer.Decoder.MpegFrame::_isMuted
	bool ____isMuted_13;

public:
	inline static int32_t get_offset_of_Next_6() { return static_cast<int32_t>(offsetof(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51, ___Next_6)); }
	inline MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * get_Next_6() const { return ___Next_6; }
	inline MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 ** get_address_of_Next_6() { return &___Next_6; }
	inline void set_Next_6(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51 * value)
	{
		___Next_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Next_6), (void*)value);
	}

	inline static int32_t get_offset_of_Number_7() { return static_cast<int32_t>(offsetof(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51, ___Number_7)); }
	inline int32_t get_Number_7() const { return ___Number_7; }
	inline int32_t* get_address_of_Number_7() { return &___Number_7; }
	inline void set_Number_7(int32_t value)
	{
		___Number_7 = value;
	}

	inline static int32_t get_offset_of__syncBits_8() { return static_cast<int32_t>(offsetof(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51, ____syncBits_8)); }
	inline int32_t get__syncBits_8() const { return ____syncBits_8; }
	inline int32_t* get_address_of__syncBits_8() { return &____syncBits_8; }
	inline void set__syncBits_8(int32_t value)
	{
		____syncBits_8 = value;
	}

	inline static int32_t get_offset_of__readOffset_9() { return static_cast<int32_t>(offsetof(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51, ____readOffset_9)); }
	inline int32_t get__readOffset_9() const { return ____readOffset_9; }
	inline int32_t* get_address_of__readOffset_9() { return &____readOffset_9; }
	inline void set__readOffset_9(int32_t value)
	{
		____readOffset_9 = value;
	}

	inline static int32_t get_offset_of__bitsRead_10() { return static_cast<int32_t>(offsetof(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51, ____bitsRead_10)); }
	inline int32_t get__bitsRead_10() const { return ____bitsRead_10; }
	inline int32_t* get_address_of__bitsRead_10() { return &____bitsRead_10; }
	inline void set__bitsRead_10(int32_t value)
	{
		____bitsRead_10 = value;
	}

	inline static int32_t get_offset_of__bitBucket_11() { return static_cast<int32_t>(offsetof(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51, ____bitBucket_11)); }
	inline uint64_t get__bitBucket_11() const { return ____bitBucket_11; }
	inline uint64_t* get_address_of__bitBucket_11() { return &____bitBucket_11; }
	inline void set__bitBucket_11(uint64_t value)
	{
		____bitBucket_11 = value;
	}

	inline static int32_t get_offset_of__offset_12() { return static_cast<int32_t>(offsetof(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51, ____offset_12)); }
	inline int64_t get__offset_12() const { return ____offset_12; }
	inline int64_t* get_address_of__offset_12() { return &____offset_12; }
	inline void set__offset_12(int64_t value)
	{
		____offset_12 = value;
	}

	inline static int32_t get_offset_of__isMuted_13() { return static_cast<int32_t>(offsetof(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51, ____isMuted_13)); }
	inline bool get__isMuted_13() const { return ____isMuted_13; }
	inline bool* get_address_of__isMuted_13() { return &____isMuted_13; }
	inline void set__isMuted_13(bool value)
	{
		____isMuted_13 = value;
	}
};

struct MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51_StaticFields
{
public:
	// System.Int32[][][] NLayer.Decoder.MpegFrame::_bitRateTable
	Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* ____bitRateTable_5;

public:
	inline static int32_t get_offset_of__bitRateTable_5() { return static_cast<int32_t>(offsetof(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51_StaticFields, ____bitRateTable_5)); }
	inline Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* get__bitRateTable_5() const { return ____bitRateTable_5; }
	inline Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141** get_address_of__bitRateTable_5() { return &____bitRateTable_5; }
	inline void set__bitRateTable_5(Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* value)
	{
		____bitRateTable_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bitRateTable_5), (void*)value);
	}
};


// NLayer.Decoder.RiffHeaderFrame
struct  RiffHeaderFrame_tD195C40825041F5DA7AE518B989C8EEEBB9CAC8C  : public FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA
{
public:

public:
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateMachine_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_defaultContextAction_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_defaultContextAction_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};

// System.Runtime.CompilerServices.TaskAwaiter`1<System.String>
struct  TaskAwaiter_1_t0139085A70D09D28A04F5187195DC0184E53D214 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.TaskAwaiter`1::m_task
	Task_1_t4A75FEC8F36C5D4F8793BB8C94E4DAA7457D0286 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_1_t0139085A70D09D28A04F5187195DC0184E53D214, ___m_task_0)); }
	inline Task_1_t4A75FEC8F36C5D4F8793BB8C94E4DAA7457D0286 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t4A75FEC8F36C5D4F8793BB8C94E4DAA7457D0286 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t4A75FEC8F36C5D4F8793BB8C94E4DAA7457D0286 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_0), (void*)value);
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::0183F51B580B1EAF92C43990FB8C2AD83C313BF8
	__StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  ___0183F51B580B1EAF92C43990FB8C2AD83C313BF8_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=60 <PrivateImplementationDetails>::02DECBD4B98B83C88DF26F1D316D967144C964AC
	__StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  ___02DECBD4B98B83C88DF26F1D316D967144C964AC_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=14 <PrivateImplementationDetails>::052C20430DC5407CAC4A7E36BAF553EAFE144CDE
	__StaticArrayInitTypeSizeU3D14_t8CCDC05F13040DD154D81376695EEB6BA93ACABA  ___052C20430DC5407CAC4A7E36BAF553EAFE144CDE_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::056EDD3469AB29404C03DFFED0CC7D751CC62ABF
	__StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0  ___056EDD3469AB29404C03DFFED0CC7D751CC62ABF_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::06A66430C02C24BB46E9D45096F3612F52BAAAAD
	__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  ___06A66430C02C24BB46E9D45096F3612F52BAAAAD_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128 <PrivateImplementationDetails>::06D0A13FF2634CA2D099C7C996C6B3B2CDC71A27
	__StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  ___06D0A13FF2634CA2D099C7C996C6B3B2CDC71A27_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::0CA9EF2C32AFFE256210DE1874B3FD80A301DD44
	__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  ___0CA9EF2C32AFFE256210DE1874B3FD80A301DD44_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::0D7F9193AF20C2D7369CE21411B3F4B96169D931
	__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  ___0D7F9193AF20C2D7369CE21411B3F4B96169D931_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::0DD7A9B3BA80B5B1E2B69AAA2DFC56CFEEE8F3CD
	__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  ___0DD7A9B3BA80B5B1E2B69AAA2DFC56CFEEE8F3CD_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::0E24731E028D4046A165EA260DA1DA95BF0201B5
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___0E24731E028D4046A165EA260DA1DA95BF0201B5_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=1022 <PrivateImplementationDetails>::0F53B5E6268C6545E9270A0169A1FA196C6092DF
	__StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140  ___0F53B5E6268C6545E9270A0169A1FA196C6092DF_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::10CE4BD8D40A44E534A61F607B710FF1950B246C
	__StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  ___10CE4BD8D40A44E534A61F607B710FF1950B246C_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=60 <PrivateImplementationDetails>::117ED604063D78A294769B182459666034519D79
	__StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  ___117ED604063D78A294769B182459666034519D79_12;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=34 <PrivateImplementationDetails>::12976064F8F8160F188A01AEEBCD007449BD365B
	__StaticArrayInitTypeSizeU3D34_t9C713A05A7B6B5C6D004D41B59A4F6B7D561459A  ___12976064F8F8160F188A01AEEBCD007449BD365B_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::167CE10EAB7DBB2FAC19002E7CB7F7E81D63F341
	__StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0  ___167CE10EAB7DBB2FAC19002E7CB7F7E81D63F341_14;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=56 <PrivateImplementationDetails>::1766F40539CB527A97DEE60B6B1AD5D7559C3EED
	__StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  ___1766F40539CB527A97DEE60B6B1AD5D7559C3EED_15;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=62 <PrivateImplementationDetails>::1EED620D880B5566ADF1E6D57BE501A951D55057
	__StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE  ___1EED620D880B5566ADF1E6D57BE501A951D55057_16;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::1F9B007B0BF8944263622345602943223197642D
	__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  ___1F9B007B0BF8944263622345602943223197642D_17;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::24CD06EFA9678B79D54A086200F29D1D8579F8D5
	__StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  ___24CD06EFA9678B79D54A086200F29D1D8579F8D5_18;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256 <PrivateImplementationDetails>::2664319BEB31A007A1D31844B199D2EDFCBF29F9
	__StaticArrayInitTypeSizeU3D256_t6F967FC9ACA9D51E8C10E3FEF9B3C0033CEB171F  ___2664319BEB31A007A1D31844B199D2EDFCBF29F9_19;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::28C710520D4675C71D8BEFAA19BC179951374DF7
	__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  ___28C710520D4675C71D8BEFAA19BC179951374DF7_20;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::2A2D28482BBB64FA5869C4116711B95BE18A1479
	__StaticArrayInitTypeSizeU3D120_tFE429A3C8C8CC2AA48009B28DC92C5FAC405B976  ___2A2D28482BBB64FA5869C4116711B95BE18A1479_21;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=124 <PrivateImplementationDetails>::2B42D3D832DBF5D5ACE91E6073EBD7BB8E12DEA1
	__StaticArrayInitTypeSizeU3D124_tD3D6FDB698030D188404AEB38EF634BB404CBB7C  ___2B42D3D832DBF5D5ACE91E6073EBD7BB8E12DEA1_22;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=1024 <PrivateImplementationDetails>::2BD232BF9BBB26D4E46CCB5C2D3B3B12274F1984
	__StaticArrayInitTypeSizeU3D1024_tC455CA8308094CC5E6ADD1A24E321B05B3134F97  ___2BD232BF9BBB26D4E46CCB5C2D3B3B12274F1984_23;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::2ECAC8F6748E7A92931BE07DFCAB9A916B8D04DA
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___2ECAC8F6748E7A92931BE07DFCAB9A916B8D04DA_24;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::2FA444E8BC1E6356A9BD772EA3A1EFB53FF2BFC7
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___2FA444E8BC1E6356A9BD772EA3A1EFB53FF2BFC7_25;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::33084732F8B9869A26F8DF0103AC90B04390AC9A
	__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  ___33084732F8B9869A26F8DF0103AC90B04390AC9A_26;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::34B53DD5686A849D0EC74344559E1E50D4776239
	__StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  ___34B53DD5686A849D0EC74344559E1E50D4776239_27;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44 <PrivateImplementationDetails>::36B1D334FE412331CDF33316031195E0C56477DB
	__StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D  ___36B1D334FE412331CDF33316031195E0C56477DB_28;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=62 <PrivateImplementationDetails>::37D9C085BE6D5012609F925C1C5068721BF3EB15
	__StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE  ___37D9C085BE6D5012609F925C1C5068721BF3EB15_29;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=140 <PrivateImplementationDetails>::3D729EDB583C1FC88A9D866EA0B3FE3BBE6738E3
	__StaticArrayInitTypeSizeU3D140_t3F4097685F037DBBFCD03ED53B59913F53BBFAAC  ___3D729EDB583C1FC88A9D866EA0B3FE3BBE6738E3_30;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=60 <PrivateImplementationDetails>::3DFD4D28BC67D3A4AF48AFFEA3F91AA2FC7E6A49
	__StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  ___3DFD4D28BC67D3A4AF48AFFEA3F91AA2FC7E6A49_31;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44 <PrivateImplementationDetails>::41DD0957261A602402E73BAA98B6B90A59CDF8C9
	__StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D  ___41DD0957261A602402E73BAA98B6B90A59CDF8C9_32;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=2048 <PrivateImplementationDetails>::4327074201FBB3C079CEAC5028A88C45836D33ED
	__StaticArrayInitTypeSizeU3D2048_t23ED45646FF6C31C0F8587980EEB1123BC08E49E  ___4327074201FBB3C079CEAC5028A88C45836D33ED_33;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::45CC202B2D1D51E86B4E8E96653F784B771B59CE
	__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  ___45CC202B2D1D51E86B4E8E96653F784B771B59CE_34;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=254 <PrivateImplementationDetails>::470B7F4FDB90F6D25C0F976D634BA13E5176CA87
	__StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D  ___470B7F4FDB90F6D25C0F976D634BA13E5176CA87_35;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=68 <PrivateImplementationDetails>::480480975A2F2351B4BC347BBF5B019CB39B8479
	__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  ___480480975A2F2351B4BC347BBF5B019CB39B8479_36;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=68 <PrivateImplementationDetails>::4E33ADF11BDB70C7312C8C14A3327CBAAFA43877
	__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  ___4E33ADF11BDB70C7312C8C14A3327CBAAFA43877_37;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::511E29D606BCD9CCF19925FEC4AB93A0BE58E6C5
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___511E29D606BCD9CCF19925FEC4AB93A0BE58E6C5_38;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::5210B03C13F919278C0704371268EDDDF80A4C4A
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___5210B03C13F919278C0704371268EDDDF80A4C4A_39;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::5D9373D97CCDD1C47F4B9AF262444CBB734B73E5
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___5D9373D97CCDD1C47F4B9AF262444CBB734B73E5_40;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::694F8C359BDCF3B5E89D474E257815B6BC6976F5
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___694F8C359BDCF3B5E89D474E257815B6BC6976F5_41;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::6A177B9D2E9E08FC7B8D074AE21BD8F3B936FD4B
	__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  ___6A177B9D2E9E08FC7B8D074AE21BD8F3B936FD4B_42;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=48 <PrivateImplementationDetails>::6A4898AE468C33B9D33857E1A1690163D02666C9
	__StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6  ___6A4898AE468C33B9D33857E1A1690163D02666C9_43;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::6A977F8999D2379359A8DFC8992B162FAB6C4A67
	__StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  ___6A977F8999D2379359A8DFC8992B162FAB6C4A67_44;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=56 <PrivateImplementationDetails>::6C3172601CE476B8B4CB4063F75FEE986E5BEA3E
	__StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  ___6C3172601CE476B8B4CB4063F75FEE986E5BEA3E_45;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::6C6307D0B0F6F4B22D271404E3A097CC7B078DDD
	__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  ___6C6307D0B0F6F4B22D271404E3A097CC7B078DDD_46;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6C99786448610AA9A550CE200DFBFE653496CCFA
	__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  ___6C99786448610AA9A550CE200DFBFE653496CCFA_47;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=56 <PrivateImplementationDetails>::6E3D89B679A0E66DD78E53B4640C9726537E11A4
	__StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  ___6E3D89B679A0E66DD78E53B4640C9726537E11A4_48;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::6F71EBB9EAEAD5A412B4323B2780EB7ABC873579
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___6F71EBB9EAEAD5A412B4323B2780EB7ABC873579_49;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=68 <PrivateImplementationDetails>::708D10A4339CBCA79AF1F908E9277FA653F670EF
	__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  ___708D10A4339CBCA79AF1F908E9277FA653F670EF_50;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=92 <PrivateImplementationDetails>::709F63077E0EC8A637406E5C4D99FEBF9404DE0A
	__StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  ___709F63077E0EC8A637406E5C4D99FEBF9404DE0A_51;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::72ED9CD72B35925260074C944C96CADDFE306044
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___72ED9CD72B35925260074C944C96CADDFE306044_52;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::79A70DABA50850BC84C5F1D76C64024CE247C598
	__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  ___79A70DABA50850BC84C5F1D76C64024CE247C598_53;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::7A7A5C9196FC30D2B49EDFEA5DCA11DCBCB649A3
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___7A7A5C9196FC30D2B49EDFEA5DCA11DCBCB649A3_54;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::7E95FAD40702678CE0940C0AAF00B7C00586771C
	__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  ___7E95FAD40702678CE0940C0AAF00B7C00586771C_55;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::7F415D9373FBBEFF31E2E51C7B7F968158CA85DD
	__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  ___7F415D9373FBBEFF31E2E51C7B7F968158CA85DD_56;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::8146FDE3318DC1C6DEF0B41B5A61BB29BBF64CAE
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___8146FDE3318DC1C6DEF0B41B5A61BB29BBF64CAE_57;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::826510337F148AA44E14E8E54E08BF04A8B294CE
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___826510337F148AA44E14E8E54E08BF04A8B294CE_58;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::84069833E42A8083F930F322499FC4BCFE543E4C
	__StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0  ___84069833E42A8083F930F322499FC4BCFE543E4C_59;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::8793BA75E6A8E1D82395EF60EE5DC3A69977A62B
	__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  ___8793BA75E6A8E1D82395EF60EE5DC3A69977A62B_60;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::8ABAB393752BC1119B0EAD989DF450C6E04D8FF2
	__StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  ___8ABAB393752BC1119B0EAD989DF450C6E04D8FF2_61;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::8C7294D418CBC68C2F3874E06AED8C38C573FDF4
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___8C7294D418CBC68C2F3874E06AED8C38C573FDF4_62;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256 <PrivateImplementationDetails>::91B948814BF180E7FA42137C83AAC043E1410E0F
	__StaticArrayInitTypeSizeU3D256_t6F967FC9ACA9D51E8C10E3FEF9B3C0033CEB171F  ___91B948814BF180E7FA42137C83AAC043E1410E0F_63;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128 <PrivateImplementationDetails>::92E3424CA2DC6ACE639842303D9E4959BC763087
	__StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  ___92E3424CA2DC6ACE639842303D9E4959BC763087_64;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=1024 <PrivateImplementationDetails>::967A5B1C1344A31AF4E7E1AAB1E89EE24D8A3FAB
	__StaticArrayInitTypeSizeU3D1024_tC455CA8308094CC5E6ADD1A24E321B05B3134F97  ___967A5B1C1344A31AF4E7E1AAB1E89EE24D8A3FAB_65;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::96DA30D37D8C819EE7A1D9733912F74C912EB5E4
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___96DA30D37D8C819EE7A1D9733912F74C912EB5E4_66;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::993F10D8449193619D5641268926D30980AB1EDE
	__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  ___993F10D8449193619D5641268926D30980AB1EDE_67;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=48 <PrivateImplementationDetails>::9A1D259207846B1C51748E932A5DD3904EB818E4
	__StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6  ___9A1D259207846B1C51748E932A5DD3904EB818E4_68;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::9D23B3B23CCAE110C3CE9C5390F8C739F67D87AD
	__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  ___9D23B3B23CCAE110C3CE9C5390F8C739F67D87AD_69;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=62 <PrivateImplementationDetails>::9FF23D5ECF10A5EB1F8BC25B3B3C71AB581369EE
	__StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE  ___9FF23D5ECF10A5EB1F8BC25B3B3C71AB581369EE_70;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::A1131FF166B71E9853F0B0BD1DD153A30418529F
	__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  ___A1131FF166B71E9853F0B0BD1DD153A30418529F_71;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=142 <PrivateImplementationDetails>::A3105F394A59519BE301861D340BC3D09E41CB19
	__StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61  ___A3105F394A59519BE301861D340BC3D09E41CB19_72;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::A328A2984A4E6A35011BFE9252B01DA208C532BA
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___A328A2984A4E6A35011BFE9252B01DA208C532BA_73;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::A4A22152FAAB8DB4A63DE422AE0D5852FD35B04A
	__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  ___A4A22152FAAB8DB4A63DE422AE0D5852FD35B04A_74;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=56 <PrivateImplementationDetails>::A62278C03345A9248C734AF81513A1F8D1B07D46
	__StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  ___A62278C03345A9248C734AF81513A1F8D1B07D46_75;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::A67ECCA7AD5CEABF13FA3BD68ED24B3F5FF81CC7
	__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  ___A67ECCA7AD5CEABF13FA3BD68ED24B3F5FF81CC7_76;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=68 <PrivateImplementationDetails>::A68289F5B4251B321F6F03C741C46974A0418E64
	__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  ___A68289F5B4251B321F6F03C741C46974A0418E64_77;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::A6AB6C1208F342FA0C67CE497AF09D47AFA065E8
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___A6AB6C1208F342FA0C67CE497AF09D47AFA065E8_78;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=92 <PrivateImplementationDetails>::A7CBA58371A7AF46C17847174F40F4663D2F59F5
	__StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  ___A7CBA58371A7AF46C17847174F40F4663D2F59F5_79;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=254 <PrivateImplementationDetails>::A92F46ED19BFF6E5CAB48F7C846477C9AAD02DA6
	__StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D  ___A92F46ED19BFF6E5CAB48F7C846477C9AAD02DA6_80;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128 <PrivateImplementationDetails>::A988F7089BBB957B7EA2A8F2D66C7DF62B59CFD9
	__StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  ___A988F7089BBB957B7EA2A8F2D66C7DF62B59CFD9_81;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=62 <PrivateImplementationDetails>::ADA84571EAB5163CC2C40A5CF9F2DF898ED1180C
	__StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE  ___ADA84571EAB5163CC2C40A5CF9F2DF898ED1180C_82;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::B07DA862851422D423B591975D256548DFDA3DC2
	__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  ___B07DA862851422D423B591975D256548DFDA3DC2_83;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=60 <PrivateImplementationDetails>::B14468BE5B21A8E0D79DA7988BBF773FDA08701A
	__StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  ___B14468BE5B21A8E0D79DA7988BBF773FDA08701A_84;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44 <PrivateImplementationDetails>::B3124AD30FFF743F5EF54086F4B0192CF1A880CF
	__StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D  ___B3124AD30FFF743F5EF54086F4B0192CF1A880CF_85;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::BC22A77A549E72722CCDC844425C7EA8615F3771
	__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  ___BC22A77A549E72722CCDC844425C7EA8615F3771_86;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::BE61A28EE8D252FD73D06806809B2E51FEB6F53C
	__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  ___BE61A28EE8D252FD73D06806809B2E51FEB6F53C_87;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128 <PrivateImplementationDetails>::BFB130DEFFB14CE563713BDDF442B98CCACE8A8F
	__StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  ___BFB130DEFFB14CE563713BDDF442B98CCACE8A8F_88;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::C06E47E37A990899E03C953F64C95F7E3210B17E
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___C06E47E37A990899E03C953F64C95F7E3210B17E_89;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=108 <PrivateImplementationDetails>::C0A8C45EE4F4DF297B1688A2C5BDE5BC312071C4
	__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  ___C0A8C45EE4F4DF297B1688A2C5BDE5BC312071C4_90;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=68 <PrivateImplementationDetails>::C5709FC40EFF23BB977BBC3DA4BCA193930C5B92
	__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  ___C5709FC40EFF23BB977BBC3DA4BCA193930C5B92_91;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=60 <PrivateImplementationDetails>::C6DFEE18BA4DC30A7D7D90AB72AB0228B66ED858
	__StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  ___C6DFEE18BA4DC30A7D7D90AB72AB0228B66ED858_92;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=68 <PrivateImplementationDetails>::C881D9C683CE2D13A2438CAD551803C8B1100C53
	__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  ___C881D9C683CE2D13A2438CAD551803C8B1100C53_93;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=1022 <PrivateImplementationDetails>::CBD12AC86FBCD56155B93A9CA77947E68793C2B5
	__StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140  ___CBD12AC86FBCD56155B93A9CA77947E68793C2B5_94;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=34 <PrivateImplementationDetails>::CC3901AF2214B4D6ADEB73EB932BC1F0E40C318C
	__StaticArrayInitTypeSizeU3D34_t9C713A05A7B6B5C6D004D41B59A4F6B7D561459A  ___CC3901AF2214B4D6ADEB73EB932BC1F0E40C318C_95;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128 <PrivateImplementationDetails>::CE23A1D2337FC685E2FA96AC9F8B8BA6EA0C5BDD
	__StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  ___CE23A1D2337FC685E2FA96AC9F8B8BA6EA0C5BDD_96;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::D087BC49F1FEB5D2824F510253A805FCC2506F71
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___D087BC49F1FEB5D2824F510253A805FCC2506F71_97;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=142 <PrivateImplementationDetails>::D177776BF8B5B13646CB32C3E3C2E3A3F6389454
	__StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61  ___D177776BF8B5B13646CB32C3E3C2E3A3F6389454_98;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::D2CE52450BFB35255920A8F83FADB36945ABF964
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___D2CE52450BFB35255920A8F83FADB36945ABF964_99;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::D5C60CDEF63D72F5032E90D8B9D5EFF3E566D8F4
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___D5C60CDEF63D72F5032E90D8B9D5EFF3E566D8F4_100;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::D5C92607E010304E7241899C8E6ADF0EE0CBFD31
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___D5C92607E010304E7241899C8E6ADF0EE0CBFD31_101;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::D8D801E3DDC2422ADA99C1D4210803A5F734F968
	__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  ___D8D801E3DDC2422ADA99C1D4210803A5F734F968_102;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=92 <PrivateImplementationDetails>::DDF6D11A3E93BE497F5BAC07290983B9D3832ECE
	__StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  ___DDF6D11A3E93BE497F5BAC07290983B9D3832ECE_103;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::DF97F08BD5310D2EBC2E21DE6536AE30347D8434
	__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  ___DF97F08BD5310D2EBC2E21DE6536AE30347D8434_104;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=92 <PrivateImplementationDetails>::E2EF5AD9211AEBF1A7D759556F680C7916E434D8
	__StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  ___E2EF5AD9211AEBF1A7D759556F680C7916E434D8_105;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::E44BF9B4DEFF99F72DA9F21344C344E2CD352BCB
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___E44BF9B4DEFF99F72DA9F21344C344E2CD352BCB_106;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=56 <PrivateImplementationDetails>::E78171182D510376B72BC5204A0D1D488DED6D00
	__StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  ___E78171182D510376B72BC5204A0D1D488DED6D00_107;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::E99B63E754303E8FA67A1C2AEA3F576D805E2FA9
	__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  ___E99B63E754303E8FA67A1C2AEA3F576D805E2FA9_108;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=1022 <PrivateImplementationDetails>::EBB7471D163460A589A0BB5C1FB2B55811F80E11
	__StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140  ___EBB7471D163460A589A0BB5C1FB2B55811F80E11_109;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::EC71709D141BB87D172E6D9D919DF53D7A4EB217
	__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  ___EC71709D141BB87D172E6D9D919DF53D7A4EB217_110;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=254 <PrivateImplementationDetails>::ED6CCE320D9C94CDED123AD06BD6A29CC75A012C
	__StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D  ___ED6CCE320D9C94CDED123AD06BD6A29CC75A012C_111;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::F0E124B606159405D7C0D19C7158FD45A4F27EF4
	__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  ___F0E124B606159405D7C0D19C7158FD45A4F27EF4_112;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=56 <PrivateImplementationDetails>::F1781D4343E58B231D363C7698B8B7C63C173F92
	__StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  ___F1781D4343E58B231D363C7698B8B7C63C173F92_113;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=68 <PrivateImplementationDetails>::F24A3495EC779AA95747314593B416FA283F5A15
	__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  ___F24A3495EC779AA95747314593B416FA283F5A15_114;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=92 <PrivateImplementationDetails>::F2BCD6EC6BB1D97B183FA5A1C54B5725010F0038
	__StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  ___F2BCD6EC6BB1D97B183FA5A1C54B5725010F0038_115;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::F301F2CE6EEF9D8AC92595F503B297E3F20B5267
	__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  ___F301F2CE6EEF9D8AC92595F503B297E3F20B5267_116;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::F4F4DAB1172A7E33DC53F7AA668C2AC7E9AF6828
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___F4F4DAB1172A7E33DC53F7AA668C2AC7E9AF6828_117;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=88 <PrivateImplementationDetails>::F7A1E3117FA8F941095E83511E1B55F5E050747C
	__StaticArrayInitTypeSizeU3D88_tACC0D3E292CC0817B7908ECD8382C424C141A9C9  ___F7A1E3117FA8F941095E83511E1B55F5E050747C_118;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::F7C62050EA6AAC50646295736EF3E5083D7781D4
	__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  ___F7C62050EA6AAC50646295736EF3E5083D7781D4_119;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::FA2D9B0982EC9C8B3280A5D325C7A19C52E902D6
	__StaticArrayInitTypeSizeU3D120_tFE429A3C8C8CC2AA48009B28DC92C5FAC405B976  ___FA2D9B0982EC9C8B3280A5D325C7A19C52E902D6_120;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::FB4FBD597E39496564E6322822D8DD70A68ADE47
	__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  ___FB4FBD597E39496564E6322822D8DD70A68ADE47_121;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::FD06A67F6CBC0BE9E112D56B89B86432104BD055
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___FD06A67F6CBC0BE9E112D56B89B86432104BD055_122;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=142 <PrivateImplementationDetails>::FD84D4E70F766B0C44E5012C056C44B2E5E90E9A
	__StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61  ___FD84D4E70F766B0C44E5012C056C44B2E5E90E9A_123;

public:
	inline static int32_t get_offset_of_U30183F51B580B1EAF92C43990FB8C2AD83C313BF8_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___0183F51B580B1EAF92C43990FB8C2AD83C313BF8_0)); }
	inline __StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  get_U30183F51B580B1EAF92C43990FB8C2AD83C313BF8_0() const { return ___0183F51B580B1EAF92C43990FB8C2AD83C313BF8_0; }
	inline __StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647 * get_address_of_U30183F51B580B1EAF92C43990FB8C2AD83C313BF8_0() { return &___0183F51B580B1EAF92C43990FB8C2AD83C313BF8_0; }
	inline void set_U30183F51B580B1EAF92C43990FB8C2AD83C313BF8_0(__StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  value)
	{
		___0183F51B580B1EAF92C43990FB8C2AD83C313BF8_0 = value;
	}

	inline static int32_t get_offset_of_U302DECBD4B98B83C88DF26F1D316D967144C964AC_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___02DECBD4B98B83C88DF26F1D316D967144C964AC_1)); }
	inline __StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  get_U302DECBD4B98B83C88DF26F1D316D967144C964AC_1() const { return ___02DECBD4B98B83C88DF26F1D316D967144C964AC_1; }
	inline __StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF * get_address_of_U302DECBD4B98B83C88DF26F1D316D967144C964AC_1() { return &___02DECBD4B98B83C88DF26F1D316D967144C964AC_1; }
	inline void set_U302DECBD4B98B83C88DF26F1D316D967144C964AC_1(__StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  value)
	{
		___02DECBD4B98B83C88DF26F1D316D967144C964AC_1 = value;
	}

	inline static int32_t get_offset_of_U3052C20430DC5407CAC4A7E36BAF553EAFE144CDE_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___052C20430DC5407CAC4A7E36BAF553EAFE144CDE_2)); }
	inline __StaticArrayInitTypeSizeU3D14_t8CCDC05F13040DD154D81376695EEB6BA93ACABA  get_U3052C20430DC5407CAC4A7E36BAF553EAFE144CDE_2() const { return ___052C20430DC5407CAC4A7E36BAF553EAFE144CDE_2; }
	inline __StaticArrayInitTypeSizeU3D14_t8CCDC05F13040DD154D81376695EEB6BA93ACABA * get_address_of_U3052C20430DC5407CAC4A7E36BAF553EAFE144CDE_2() { return &___052C20430DC5407CAC4A7E36BAF553EAFE144CDE_2; }
	inline void set_U3052C20430DC5407CAC4A7E36BAF553EAFE144CDE_2(__StaticArrayInitTypeSizeU3D14_t8CCDC05F13040DD154D81376695EEB6BA93ACABA  value)
	{
		___052C20430DC5407CAC4A7E36BAF553EAFE144CDE_2 = value;
	}

	inline static int32_t get_offset_of_U3056EDD3469AB29404C03DFFED0CC7D751CC62ABF_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___056EDD3469AB29404C03DFFED0CC7D751CC62ABF_3)); }
	inline __StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0  get_U3056EDD3469AB29404C03DFFED0CC7D751CC62ABF_3() const { return ___056EDD3469AB29404C03DFFED0CC7D751CC62ABF_3; }
	inline __StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0 * get_address_of_U3056EDD3469AB29404C03DFFED0CC7D751CC62ABF_3() { return &___056EDD3469AB29404C03DFFED0CC7D751CC62ABF_3; }
	inline void set_U3056EDD3469AB29404C03DFFED0CC7D751CC62ABF_3(__StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0  value)
	{
		___056EDD3469AB29404C03DFFED0CC7D751CC62ABF_3 = value;
	}

	inline static int32_t get_offset_of_U306A66430C02C24BB46E9D45096F3612F52BAAAAD_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___06A66430C02C24BB46E9D45096F3612F52BAAAAD_4)); }
	inline __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  get_U306A66430C02C24BB46E9D45096F3612F52BAAAAD_4() const { return ___06A66430C02C24BB46E9D45096F3612F52BAAAAD_4; }
	inline __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1 * get_address_of_U306A66430C02C24BB46E9D45096F3612F52BAAAAD_4() { return &___06A66430C02C24BB46E9D45096F3612F52BAAAAD_4; }
	inline void set_U306A66430C02C24BB46E9D45096F3612F52BAAAAD_4(__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  value)
	{
		___06A66430C02C24BB46E9D45096F3612F52BAAAAD_4 = value;
	}

	inline static int32_t get_offset_of_U306D0A13FF2634CA2D099C7C996C6B3B2CDC71A27_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___06D0A13FF2634CA2D099C7C996C6B3B2CDC71A27_5)); }
	inline __StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  get_U306D0A13FF2634CA2D099C7C996C6B3B2CDC71A27_5() const { return ___06D0A13FF2634CA2D099C7C996C6B3B2CDC71A27_5; }
	inline __StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726 * get_address_of_U306D0A13FF2634CA2D099C7C996C6B3B2CDC71A27_5() { return &___06D0A13FF2634CA2D099C7C996C6B3B2CDC71A27_5; }
	inline void set_U306D0A13FF2634CA2D099C7C996C6B3B2CDC71A27_5(__StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  value)
	{
		___06D0A13FF2634CA2D099C7C996C6B3B2CDC71A27_5 = value;
	}

	inline static int32_t get_offset_of_U30CA9EF2C32AFFE256210DE1874B3FD80A301DD44_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___0CA9EF2C32AFFE256210DE1874B3FD80A301DD44_6)); }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  get_U30CA9EF2C32AFFE256210DE1874B3FD80A301DD44_6() const { return ___0CA9EF2C32AFFE256210DE1874B3FD80A301DD44_6; }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 * get_address_of_U30CA9EF2C32AFFE256210DE1874B3FD80A301DD44_6() { return &___0CA9EF2C32AFFE256210DE1874B3FD80A301DD44_6; }
	inline void set_U30CA9EF2C32AFFE256210DE1874B3FD80A301DD44_6(__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  value)
	{
		___0CA9EF2C32AFFE256210DE1874B3FD80A301DD44_6 = value;
	}

	inline static int32_t get_offset_of_U30D7F9193AF20C2D7369CE21411B3F4B96169D931_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___0D7F9193AF20C2D7369CE21411B3F4B96169D931_7)); }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  get_U30D7F9193AF20C2D7369CE21411B3F4B96169D931_7() const { return ___0D7F9193AF20C2D7369CE21411B3F4B96169D931_7; }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315 * get_address_of_U30D7F9193AF20C2D7369CE21411B3F4B96169D931_7() { return &___0D7F9193AF20C2D7369CE21411B3F4B96169D931_7; }
	inline void set_U30D7F9193AF20C2D7369CE21411B3F4B96169D931_7(__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  value)
	{
		___0D7F9193AF20C2D7369CE21411B3F4B96169D931_7 = value;
	}

	inline static int32_t get_offset_of_U30DD7A9B3BA80B5B1E2B69AAA2DFC56CFEEE8F3CD_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___0DD7A9B3BA80B5B1E2B69AAA2DFC56CFEEE8F3CD_8)); }
	inline __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  get_U30DD7A9B3BA80B5B1E2B69AAA2DFC56CFEEE8F3CD_8() const { return ___0DD7A9B3BA80B5B1E2B69AAA2DFC56CFEEE8F3CD_8; }
	inline __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1 * get_address_of_U30DD7A9B3BA80B5B1E2B69AAA2DFC56CFEEE8F3CD_8() { return &___0DD7A9B3BA80B5B1E2B69AAA2DFC56CFEEE8F3CD_8; }
	inline void set_U30DD7A9B3BA80B5B1E2B69AAA2DFC56CFEEE8F3CD_8(__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  value)
	{
		___0DD7A9B3BA80B5B1E2B69AAA2DFC56CFEEE8F3CD_8 = value;
	}

	inline static int32_t get_offset_of_U30E24731E028D4046A165EA260DA1DA95BF0201B5_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___0E24731E028D4046A165EA260DA1DA95BF0201B5_9)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U30E24731E028D4046A165EA260DA1DA95BF0201B5_9() const { return ___0E24731E028D4046A165EA260DA1DA95BF0201B5_9; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U30E24731E028D4046A165EA260DA1DA95BF0201B5_9() { return &___0E24731E028D4046A165EA260DA1DA95BF0201B5_9; }
	inline void set_U30E24731E028D4046A165EA260DA1DA95BF0201B5_9(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___0E24731E028D4046A165EA260DA1DA95BF0201B5_9 = value;
	}

	inline static int32_t get_offset_of_U30F53B5E6268C6545E9270A0169A1FA196C6092DF_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___0F53B5E6268C6545E9270A0169A1FA196C6092DF_10)); }
	inline __StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140  get_U30F53B5E6268C6545E9270A0169A1FA196C6092DF_10() const { return ___0F53B5E6268C6545E9270A0169A1FA196C6092DF_10; }
	inline __StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140 * get_address_of_U30F53B5E6268C6545E9270A0169A1FA196C6092DF_10() { return &___0F53B5E6268C6545E9270A0169A1FA196C6092DF_10; }
	inline void set_U30F53B5E6268C6545E9270A0169A1FA196C6092DF_10(__StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140  value)
	{
		___0F53B5E6268C6545E9270A0169A1FA196C6092DF_10 = value;
	}

	inline static int32_t get_offset_of_U310CE4BD8D40A44E534A61F607B710FF1950B246C_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___10CE4BD8D40A44E534A61F607B710FF1950B246C_11)); }
	inline __StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  get_U310CE4BD8D40A44E534A61F607B710FF1950B246C_11() const { return ___10CE4BD8D40A44E534A61F607B710FF1950B246C_11; }
	inline __StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647 * get_address_of_U310CE4BD8D40A44E534A61F607B710FF1950B246C_11() { return &___10CE4BD8D40A44E534A61F607B710FF1950B246C_11; }
	inline void set_U310CE4BD8D40A44E534A61F607B710FF1950B246C_11(__StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  value)
	{
		___10CE4BD8D40A44E534A61F607B710FF1950B246C_11 = value;
	}

	inline static int32_t get_offset_of_U3117ED604063D78A294769B182459666034519D79_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___117ED604063D78A294769B182459666034519D79_12)); }
	inline __StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  get_U3117ED604063D78A294769B182459666034519D79_12() const { return ___117ED604063D78A294769B182459666034519D79_12; }
	inline __StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF * get_address_of_U3117ED604063D78A294769B182459666034519D79_12() { return &___117ED604063D78A294769B182459666034519D79_12; }
	inline void set_U3117ED604063D78A294769B182459666034519D79_12(__StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  value)
	{
		___117ED604063D78A294769B182459666034519D79_12 = value;
	}

	inline static int32_t get_offset_of_U312976064F8F8160F188A01AEEBCD007449BD365B_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___12976064F8F8160F188A01AEEBCD007449BD365B_13)); }
	inline __StaticArrayInitTypeSizeU3D34_t9C713A05A7B6B5C6D004D41B59A4F6B7D561459A  get_U312976064F8F8160F188A01AEEBCD007449BD365B_13() const { return ___12976064F8F8160F188A01AEEBCD007449BD365B_13; }
	inline __StaticArrayInitTypeSizeU3D34_t9C713A05A7B6B5C6D004D41B59A4F6B7D561459A * get_address_of_U312976064F8F8160F188A01AEEBCD007449BD365B_13() { return &___12976064F8F8160F188A01AEEBCD007449BD365B_13; }
	inline void set_U312976064F8F8160F188A01AEEBCD007449BD365B_13(__StaticArrayInitTypeSizeU3D34_t9C713A05A7B6B5C6D004D41B59A4F6B7D561459A  value)
	{
		___12976064F8F8160F188A01AEEBCD007449BD365B_13 = value;
	}

	inline static int32_t get_offset_of_U3167CE10EAB7DBB2FAC19002E7CB7F7E81D63F341_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___167CE10EAB7DBB2FAC19002E7CB7F7E81D63F341_14)); }
	inline __StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0  get_U3167CE10EAB7DBB2FAC19002E7CB7F7E81D63F341_14() const { return ___167CE10EAB7DBB2FAC19002E7CB7F7E81D63F341_14; }
	inline __StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0 * get_address_of_U3167CE10EAB7DBB2FAC19002E7CB7F7E81D63F341_14() { return &___167CE10EAB7DBB2FAC19002E7CB7F7E81D63F341_14; }
	inline void set_U3167CE10EAB7DBB2FAC19002E7CB7F7E81D63F341_14(__StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0  value)
	{
		___167CE10EAB7DBB2FAC19002E7CB7F7E81D63F341_14 = value;
	}

	inline static int32_t get_offset_of_U31766F40539CB527A97DEE60B6B1AD5D7559C3EED_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___1766F40539CB527A97DEE60B6B1AD5D7559C3EED_15)); }
	inline __StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  get_U31766F40539CB527A97DEE60B6B1AD5D7559C3EED_15() const { return ___1766F40539CB527A97DEE60B6B1AD5D7559C3EED_15; }
	inline __StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816 * get_address_of_U31766F40539CB527A97DEE60B6B1AD5D7559C3EED_15() { return &___1766F40539CB527A97DEE60B6B1AD5D7559C3EED_15; }
	inline void set_U31766F40539CB527A97DEE60B6B1AD5D7559C3EED_15(__StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  value)
	{
		___1766F40539CB527A97DEE60B6B1AD5D7559C3EED_15 = value;
	}

	inline static int32_t get_offset_of_U31EED620D880B5566ADF1E6D57BE501A951D55057_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___1EED620D880B5566ADF1E6D57BE501A951D55057_16)); }
	inline __StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE  get_U31EED620D880B5566ADF1E6D57BE501A951D55057_16() const { return ___1EED620D880B5566ADF1E6D57BE501A951D55057_16; }
	inline __StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE * get_address_of_U31EED620D880B5566ADF1E6D57BE501A951D55057_16() { return &___1EED620D880B5566ADF1E6D57BE501A951D55057_16; }
	inline void set_U31EED620D880B5566ADF1E6D57BE501A951D55057_16(__StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE  value)
	{
		___1EED620D880B5566ADF1E6D57BE501A951D55057_16 = value;
	}

	inline static int32_t get_offset_of_U31F9B007B0BF8944263622345602943223197642D_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___1F9B007B0BF8944263622345602943223197642D_17)); }
	inline __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  get_U31F9B007B0BF8944263622345602943223197642D_17() const { return ___1F9B007B0BF8944263622345602943223197642D_17; }
	inline __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1 * get_address_of_U31F9B007B0BF8944263622345602943223197642D_17() { return &___1F9B007B0BF8944263622345602943223197642D_17; }
	inline void set_U31F9B007B0BF8944263622345602943223197642D_17(__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  value)
	{
		___1F9B007B0BF8944263622345602943223197642D_17 = value;
	}

	inline static int32_t get_offset_of_U324CD06EFA9678B79D54A086200F29D1D8579F8D5_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___24CD06EFA9678B79D54A086200F29D1D8579F8D5_18)); }
	inline __StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  get_U324CD06EFA9678B79D54A086200F29D1D8579F8D5_18() const { return ___24CD06EFA9678B79D54A086200F29D1D8579F8D5_18; }
	inline __StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647 * get_address_of_U324CD06EFA9678B79D54A086200F29D1D8579F8D5_18() { return &___24CD06EFA9678B79D54A086200F29D1D8579F8D5_18; }
	inline void set_U324CD06EFA9678B79D54A086200F29D1D8579F8D5_18(__StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  value)
	{
		___24CD06EFA9678B79D54A086200F29D1D8579F8D5_18 = value;
	}

	inline static int32_t get_offset_of_U32664319BEB31A007A1D31844B199D2EDFCBF29F9_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___2664319BEB31A007A1D31844B199D2EDFCBF29F9_19)); }
	inline __StaticArrayInitTypeSizeU3D256_t6F967FC9ACA9D51E8C10E3FEF9B3C0033CEB171F  get_U32664319BEB31A007A1D31844B199D2EDFCBF29F9_19() const { return ___2664319BEB31A007A1D31844B199D2EDFCBF29F9_19; }
	inline __StaticArrayInitTypeSizeU3D256_t6F967FC9ACA9D51E8C10E3FEF9B3C0033CEB171F * get_address_of_U32664319BEB31A007A1D31844B199D2EDFCBF29F9_19() { return &___2664319BEB31A007A1D31844B199D2EDFCBF29F9_19; }
	inline void set_U32664319BEB31A007A1D31844B199D2EDFCBF29F9_19(__StaticArrayInitTypeSizeU3D256_t6F967FC9ACA9D51E8C10E3FEF9B3C0033CEB171F  value)
	{
		___2664319BEB31A007A1D31844B199D2EDFCBF29F9_19 = value;
	}

	inline static int32_t get_offset_of_U328C710520D4675C71D8BEFAA19BC179951374DF7_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___28C710520D4675C71D8BEFAA19BC179951374DF7_20)); }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  get_U328C710520D4675C71D8BEFAA19BC179951374DF7_20() const { return ___28C710520D4675C71D8BEFAA19BC179951374DF7_20; }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 * get_address_of_U328C710520D4675C71D8BEFAA19BC179951374DF7_20() { return &___28C710520D4675C71D8BEFAA19BC179951374DF7_20; }
	inline void set_U328C710520D4675C71D8BEFAA19BC179951374DF7_20(__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  value)
	{
		___28C710520D4675C71D8BEFAA19BC179951374DF7_20 = value;
	}

	inline static int32_t get_offset_of_U32A2D28482BBB64FA5869C4116711B95BE18A1479_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___2A2D28482BBB64FA5869C4116711B95BE18A1479_21)); }
	inline __StaticArrayInitTypeSizeU3D120_tFE429A3C8C8CC2AA48009B28DC92C5FAC405B976  get_U32A2D28482BBB64FA5869C4116711B95BE18A1479_21() const { return ___2A2D28482BBB64FA5869C4116711B95BE18A1479_21; }
	inline __StaticArrayInitTypeSizeU3D120_tFE429A3C8C8CC2AA48009B28DC92C5FAC405B976 * get_address_of_U32A2D28482BBB64FA5869C4116711B95BE18A1479_21() { return &___2A2D28482BBB64FA5869C4116711B95BE18A1479_21; }
	inline void set_U32A2D28482BBB64FA5869C4116711B95BE18A1479_21(__StaticArrayInitTypeSizeU3D120_tFE429A3C8C8CC2AA48009B28DC92C5FAC405B976  value)
	{
		___2A2D28482BBB64FA5869C4116711B95BE18A1479_21 = value;
	}

	inline static int32_t get_offset_of_U32B42D3D832DBF5D5ACE91E6073EBD7BB8E12DEA1_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___2B42D3D832DBF5D5ACE91E6073EBD7BB8E12DEA1_22)); }
	inline __StaticArrayInitTypeSizeU3D124_tD3D6FDB698030D188404AEB38EF634BB404CBB7C  get_U32B42D3D832DBF5D5ACE91E6073EBD7BB8E12DEA1_22() const { return ___2B42D3D832DBF5D5ACE91E6073EBD7BB8E12DEA1_22; }
	inline __StaticArrayInitTypeSizeU3D124_tD3D6FDB698030D188404AEB38EF634BB404CBB7C * get_address_of_U32B42D3D832DBF5D5ACE91E6073EBD7BB8E12DEA1_22() { return &___2B42D3D832DBF5D5ACE91E6073EBD7BB8E12DEA1_22; }
	inline void set_U32B42D3D832DBF5D5ACE91E6073EBD7BB8E12DEA1_22(__StaticArrayInitTypeSizeU3D124_tD3D6FDB698030D188404AEB38EF634BB404CBB7C  value)
	{
		___2B42D3D832DBF5D5ACE91E6073EBD7BB8E12DEA1_22 = value;
	}

	inline static int32_t get_offset_of_U32BD232BF9BBB26D4E46CCB5C2D3B3B12274F1984_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___2BD232BF9BBB26D4E46CCB5C2D3B3B12274F1984_23)); }
	inline __StaticArrayInitTypeSizeU3D1024_tC455CA8308094CC5E6ADD1A24E321B05B3134F97  get_U32BD232BF9BBB26D4E46CCB5C2D3B3B12274F1984_23() const { return ___2BD232BF9BBB26D4E46CCB5C2D3B3B12274F1984_23; }
	inline __StaticArrayInitTypeSizeU3D1024_tC455CA8308094CC5E6ADD1A24E321B05B3134F97 * get_address_of_U32BD232BF9BBB26D4E46CCB5C2D3B3B12274F1984_23() { return &___2BD232BF9BBB26D4E46CCB5C2D3B3B12274F1984_23; }
	inline void set_U32BD232BF9BBB26D4E46CCB5C2D3B3B12274F1984_23(__StaticArrayInitTypeSizeU3D1024_tC455CA8308094CC5E6ADD1A24E321B05B3134F97  value)
	{
		___2BD232BF9BBB26D4E46CCB5C2D3B3B12274F1984_23 = value;
	}

	inline static int32_t get_offset_of_U32ECAC8F6748E7A92931BE07DFCAB9A916B8D04DA_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___2ECAC8F6748E7A92931BE07DFCAB9A916B8D04DA_24)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U32ECAC8F6748E7A92931BE07DFCAB9A916B8D04DA_24() const { return ___2ECAC8F6748E7A92931BE07DFCAB9A916B8D04DA_24; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U32ECAC8F6748E7A92931BE07DFCAB9A916B8D04DA_24() { return &___2ECAC8F6748E7A92931BE07DFCAB9A916B8D04DA_24; }
	inline void set_U32ECAC8F6748E7A92931BE07DFCAB9A916B8D04DA_24(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___2ECAC8F6748E7A92931BE07DFCAB9A916B8D04DA_24 = value;
	}

	inline static int32_t get_offset_of_U32FA444E8BC1E6356A9BD772EA3A1EFB53FF2BFC7_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___2FA444E8BC1E6356A9BD772EA3A1EFB53FF2BFC7_25)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U32FA444E8BC1E6356A9BD772EA3A1EFB53FF2BFC7_25() const { return ___2FA444E8BC1E6356A9BD772EA3A1EFB53FF2BFC7_25; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U32FA444E8BC1E6356A9BD772EA3A1EFB53FF2BFC7_25() { return &___2FA444E8BC1E6356A9BD772EA3A1EFB53FF2BFC7_25; }
	inline void set_U32FA444E8BC1E6356A9BD772EA3A1EFB53FF2BFC7_25(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___2FA444E8BC1E6356A9BD772EA3A1EFB53FF2BFC7_25 = value;
	}

	inline static int32_t get_offset_of_U333084732F8B9869A26F8DF0103AC90B04390AC9A_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___33084732F8B9869A26F8DF0103AC90B04390AC9A_26)); }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  get_U333084732F8B9869A26F8DF0103AC90B04390AC9A_26() const { return ___33084732F8B9869A26F8DF0103AC90B04390AC9A_26; }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34 * get_address_of_U333084732F8B9869A26F8DF0103AC90B04390AC9A_26() { return &___33084732F8B9869A26F8DF0103AC90B04390AC9A_26; }
	inline void set_U333084732F8B9869A26F8DF0103AC90B04390AC9A_26(__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  value)
	{
		___33084732F8B9869A26F8DF0103AC90B04390AC9A_26 = value;
	}

	inline static int32_t get_offset_of_U334B53DD5686A849D0EC74344559E1E50D4776239_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___34B53DD5686A849D0EC74344559E1E50D4776239_27)); }
	inline __StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  get_U334B53DD5686A849D0EC74344559E1E50D4776239_27() const { return ___34B53DD5686A849D0EC74344559E1E50D4776239_27; }
	inline __StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647 * get_address_of_U334B53DD5686A849D0EC74344559E1E50D4776239_27() { return &___34B53DD5686A849D0EC74344559E1E50D4776239_27; }
	inline void set_U334B53DD5686A849D0EC74344559E1E50D4776239_27(__StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  value)
	{
		___34B53DD5686A849D0EC74344559E1E50D4776239_27 = value;
	}

	inline static int32_t get_offset_of_U336B1D334FE412331CDF33316031195E0C56477DB_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___36B1D334FE412331CDF33316031195E0C56477DB_28)); }
	inline __StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D  get_U336B1D334FE412331CDF33316031195E0C56477DB_28() const { return ___36B1D334FE412331CDF33316031195E0C56477DB_28; }
	inline __StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D * get_address_of_U336B1D334FE412331CDF33316031195E0C56477DB_28() { return &___36B1D334FE412331CDF33316031195E0C56477DB_28; }
	inline void set_U336B1D334FE412331CDF33316031195E0C56477DB_28(__StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D  value)
	{
		___36B1D334FE412331CDF33316031195E0C56477DB_28 = value;
	}

	inline static int32_t get_offset_of_U337D9C085BE6D5012609F925C1C5068721BF3EB15_29() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___37D9C085BE6D5012609F925C1C5068721BF3EB15_29)); }
	inline __StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE  get_U337D9C085BE6D5012609F925C1C5068721BF3EB15_29() const { return ___37D9C085BE6D5012609F925C1C5068721BF3EB15_29; }
	inline __StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE * get_address_of_U337D9C085BE6D5012609F925C1C5068721BF3EB15_29() { return &___37D9C085BE6D5012609F925C1C5068721BF3EB15_29; }
	inline void set_U337D9C085BE6D5012609F925C1C5068721BF3EB15_29(__StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE  value)
	{
		___37D9C085BE6D5012609F925C1C5068721BF3EB15_29 = value;
	}

	inline static int32_t get_offset_of_U33D729EDB583C1FC88A9D866EA0B3FE3BBE6738E3_30() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___3D729EDB583C1FC88A9D866EA0B3FE3BBE6738E3_30)); }
	inline __StaticArrayInitTypeSizeU3D140_t3F4097685F037DBBFCD03ED53B59913F53BBFAAC  get_U33D729EDB583C1FC88A9D866EA0B3FE3BBE6738E3_30() const { return ___3D729EDB583C1FC88A9D866EA0B3FE3BBE6738E3_30; }
	inline __StaticArrayInitTypeSizeU3D140_t3F4097685F037DBBFCD03ED53B59913F53BBFAAC * get_address_of_U33D729EDB583C1FC88A9D866EA0B3FE3BBE6738E3_30() { return &___3D729EDB583C1FC88A9D866EA0B3FE3BBE6738E3_30; }
	inline void set_U33D729EDB583C1FC88A9D866EA0B3FE3BBE6738E3_30(__StaticArrayInitTypeSizeU3D140_t3F4097685F037DBBFCD03ED53B59913F53BBFAAC  value)
	{
		___3D729EDB583C1FC88A9D866EA0B3FE3BBE6738E3_30 = value;
	}

	inline static int32_t get_offset_of_U33DFD4D28BC67D3A4AF48AFFEA3F91AA2FC7E6A49_31() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___3DFD4D28BC67D3A4AF48AFFEA3F91AA2FC7E6A49_31)); }
	inline __StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  get_U33DFD4D28BC67D3A4AF48AFFEA3F91AA2FC7E6A49_31() const { return ___3DFD4D28BC67D3A4AF48AFFEA3F91AA2FC7E6A49_31; }
	inline __StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF * get_address_of_U33DFD4D28BC67D3A4AF48AFFEA3F91AA2FC7E6A49_31() { return &___3DFD4D28BC67D3A4AF48AFFEA3F91AA2FC7E6A49_31; }
	inline void set_U33DFD4D28BC67D3A4AF48AFFEA3F91AA2FC7E6A49_31(__StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  value)
	{
		___3DFD4D28BC67D3A4AF48AFFEA3F91AA2FC7E6A49_31 = value;
	}

	inline static int32_t get_offset_of_U341DD0957261A602402E73BAA98B6B90A59CDF8C9_32() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___41DD0957261A602402E73BAA98B6B90A59CDF8C9_32)); }
	inline __StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D  get_U341DD0957261A602402E73BAA98B6B90A59CDF8C9_32() const { return ___41DD0957261A602402E73BAA98B6B90A59CDF8C9_32; }
	inline __StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D * get_address_of_U341DD0957261A602402E73BAA98B6B90A59CDF8C9_32() { return &___41DD0957261A602402E73BAA98B6B90A59CDF8C9_32; }
	inline void set_U341DD0957261A602402E73BAA98B6B90A59CDF8C9_32(__StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D  value)
	{
		___41DD0957261A602402E73BAA98B6B90A59CDF8C9_32 = value;
	}

	inline static int32_t get_offset_of_U34327074201FBB3C079CEAC5028A88C45836D33ED_33() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___4327074201FBB3C079CEAC5028A88C45836D33ED_33)); }
	inline __StaticArrayInitTypeSizeU3D2048_t23ED45646FF6C31C0F8587980EEB1123BC08E49E  get_U34327074201FBB3C079CEAC5028A88C45836D33ED_33() const { return ___4327074201FBB3C079CEAC5028A88C45836D33ED_33; }
	inline __StaticArrayInitTypeSizeU3D2048_t23ED45646FF6C31C0F8587980EEB1123BC08E49E * get_address_of_U34327074201FBB3C079CEAC5028A88C45836D33ED_33() { return &___4327074201FBB3C079CEAC5028A88C45836D33ED_33; }
	inline void set_U34327074201FBB3C079CEAC5028A88C45836D33ED_33(__StaticArrayInitTypeSizeU3D2048_t23ED45646FF6C31C0F8587980EEB1123BC08E49E  value)
	{
		___4327074201FBB3C079CEAC5028A88C45836D33ED_33 = value;
	}

	inline static int32_t get_offset_of_U345CC202B2D1D51E86B4E8E96653F784B771B59CE_34() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___45CC202B2D1D51E86B4E8E96653F784B771B59CE_34)); }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  get_U345CC202B2D1D51E86B4E8E96653F784B771B59CE_34() const { return ___45CC202B2D1D51E86B4E8E96653F784B771B59CE_34; }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 * get_address_of_U345CC202B2D1D51E86B4E8E96653F784B771B59CE_34() { return &___45CC202B2D1D51E86B4E8E96653F784B771B59CE_34; }
	inline void set_U345CC202B2D1D51E86B4E8E96653F784B771B59CE_34(__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  value)
	{
		___45CC202B2D1D51E86B4E8E96653F784B771B59CE_34 = value;
	}

	inline static int32_t get_offset_of_U3470B7F4FDB90F6D25C0F976D634BA13E5176CA87_35() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___470B7F4FDB90F6D25C0F976D634BA13E5176CA87_35)); }
	inline __StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D  get_U3470B7F4FDB90F6D25C0F976D634BA13E5176CA87_35() const { return ___470B7F4FDB90F6D25C0F976D634BA13E5176CA87_35; }
	inline __StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D * get_address_of_U3470B7F4FDB90F6D25C0F976D634BA13E5176CA87_35() { return &___470B7F4FDB90F6D25C0F976D634BA13E5176CA87_35; }
	inline void set_U3470B7F4FDB90F6D25C0F976D634BA13E5176CA87_35(__StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D  value)
	{
		___470B7F4FDB90F6D25C0F976D634BA13E5176CA87_35 = value;
	}

	inline static int32_t get_offset_of_U3480480975A2F2351B4BC347BBF5B019CB39B8479_36() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___480480975A2F2351B4BC347BBF5B019CB39B8479_36)); }
	inline __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  get_U3480480975A2F2351B4BC347BBF5B019CB39B8479_36() const { return ___480480975A2F2351B4BC347BBF5B019CB39B8479_36; }
	inline __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D * get_address_of_U3480480975A2F2351B4BC347BBF5B019CB39B8479_36() { return &___480480975A2F2351B4BC347BBF5B019CB39B8479_36; }
	inline void set_U3480480975A2F2351B4BC347BBF5B019CB39B8479_36(__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  value)
	{
		___480480975A2F2351B4BC347BBF5B019CB39B8479_36 = value;
	}

	inline static int32_t get_offset_of_U34E33ADF11BDB70C7312C8C14A3327CBAAFA43877_37() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___4E33ADF11BDB70C7312C8C14A3327CBAAFA43877_37)); }
	inline __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  get_U34E33ADF11BDB70C7312C8C14A3327CBAAFA43877_37() const { return ___4E33ADF11BDB70C7312C8C14A3327CBAAFA43877_37; }
	inline __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D * get_address_of_U34E33ADF11BDB70C7312C8C14A3327CBAAFA43877_37() { return &___4E33ADF11BDB70C7312C8C14A3327CBAAFA43877_37; }
	inline void set_U34E33ADF11BDB70C7312C8C14A3327CBAAFA43877_37(__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  value)
	{
		___4E33ADF11BDB70C7312C8C14A3327CBAAFA43877_37 = value;
	}

	inline static int32_t get_offset_of_U3511E29D606BCD9CCF19925FEC4AB93A0BE58E6C5_38() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___511E29D606BCD9CCF19925FEC4AB93A0BE58E6C5_38)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U3511E29D606BCD9CCF19925FEC4AB93A0BE58E6C5_38() const { return ___511E29D606BCD9CCF19925FEC4AB93A0BE58E6C5_38; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U3511E29D606BCD9CCF19925FEC4AB93A0BE58E6C5_38() { return &___511E29D606BCD9CCF19925FEC4AB93A0BE58E6C5_38; }
	inline void set_U3511E29D606BCD9CCF19925FEC4AB93A0BE58E6C5_38(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___511E29D606BCD9CCF19925FEC4AB93A0BE58E6C5_38 = value;
	}

	inline static int32_t get_offset_of_U35210B03C13F919278C0704371268EDDDF80A4C4A_39() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___5210B03C13F919278C0704371268EDDDF80A4C4A_39)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U35210B03C13F919278C0704371268EDDDF80A4C4A_39() const { return ___5210B03C13F919278C0704371268EDDDF80A4C4A_39; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U35210B03C13F919278C0704371268EDDDF80A4C4A_39() { return &___5210B03C13F919278C0704371268EDDDF80A4C4A_39; }
	inline void set_U35210B03C13F919278C0704371268EDDDF80A4C4A_39(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___5210B03C13F919278C0704371268EDDDF80A4C4A_39 = value;
	}

	inline static int32_t get_offset_of_U35D9373D97CCDD1C47F4B9AF262444CBB734B73E5_40() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___5D9373D97CCDD1C47F4B9AF262444CBB734B73E5_40)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U35D9373D97CCDD1C47F4B9AF262444CBB734B73E5_40() const { return ___5D9373D97CCDD1C47F4B9AF262444CBB734B73E5_40; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U35D9373D97CCDD1C47F4B9AF262444CBB734B73E5_40() { return &___5D9373D97CCDD1C47F4B9AF262444CBB734B73E5_40; }
	inline void set_U35D9373D97CCDD1C47F4B9AF262444CBB734B73E5_40(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___5D9373D97CCDD1C47F4B9AF262444CBB734B73E5_40 = value;
	}

	inline static int32_t get_offset_of_U3694F8C359BDCF3B5E89D474E257815B6BC6976F5_41() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___694F8C359BDCF3B5E89D474E257815B6BC6976F5_41)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U3694F8C359BDCF3B5E89D474E257815B6BC6976F5_41() const { return ___694F8C359BDCF3B5E89D474E257815B6BC6976F5_41; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U3694F8C359BDCF3B5E89D474E257815B6BC6976F5_41() { return &___694F8C359BDCF3B5E89D474E257815B6BC6976F5_41; }
	inline void set_U3694F8C359BDCF3B5E89D474E257815B6BC6976F5_41(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___694F8C359BDCF3B5E89D474E257815B6BC6976F5_41 = value;
	}

	inline static int32_t get_offset_of_U36A177B9D2E9E08FC7B8D074AE21BD8F3B936FD4B_42() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___6A177B9D2E9E08FC7B8D074AE21BD8F3B936FD4B_42)); }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  get_U36A177B9D2E9E08FC7B8D074AE21BD8F3B936FD4B_42() const { return ___6A177B9D2E9E08FC7B8D074AE21BD8F3B936FD4B_42; }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 * get_address_of_U36A177B9D2E9E08FC7B8D074AE21BD8F3B936FD4B_42() { return &___6A177B9D2E9E08FC7B8D074AE21BD8F3B936FD4B_42; }
	inline void set_U36A177B9D2E9E08FC7B8D074AE21BD8F3B936FD4B_42(__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  value)
	{
		___6A177B9D2E9E08FC7B8D074AE21BD8F3B936FD4B_42 = value;
	}

	inline static int32_t get_offset_of_U36A4898AE468C33B9D33857E1A1690163D02666C9_43() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___6A4898AE468C33B9D33857E1A1690163D02666C9_43)); }
	inline __StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6  get_U36A4898AE468C33B9D33857E1A1690163D02666C9_43() const { return ___6A4898AE468C33B9D33857E1A1690163D02666C9_43; }
	inline __StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6 * get_address_of_U36A4898AE468C33B9D33857E1A1690163D02666C9_43() { return &___6A4898AE468C33B9D33857E1A1690163D02666C9_43; }
	inline void set_U36A4898AE468C33B9D33857E1A1690163D02666C9_43(__StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6  value)
	{
		___6A4898AE468C33B9D33857E1A1690163D02666C9_43 = value;
	}

	inline static int32_t get_offset_of_U36A977F8999D2379359A8DFC8992B162FAB6C4A67_44() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___6A977F8999D2379359A8DFC8992B162FAB6C4A67_44)); }
	inline __StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  get_U36A977F8999D2379359A8DFC8992B162FAB6C4A67_44() const { return ___6A977F8999D2379359A8DFC8992B162FAB6C4A67_44; }
	inline __StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647 * get_address_of_U36A977F8999D2379359A8DFC8992B162FAB6C4A67_44() { return &___6A977F8999D2379359A8DFC8992B162FAB6C4A67_44; }
	inline void set_U36A977F8999D2379359A8DFC8992B162FAB6C4A67_44(__StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  value)
	{
		___6A977F8999D2379359A8DFC8992B162FAB6C4A67_44 = value;
	}

	inline static int32_t get_offset_of_U36C3172601CE476B8B4CB4063F75FEE986E5BEA3E_45() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___6C3172601CE476B8B4CB4063F75FEE986E5BEA3E_45)); }
	inline __StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  get_U36C3172601CE476B8B4CB4063F75FEE986E5BEA3E_45() const { return ___6C3172601CE476B8B4CB4063F75FEE986E5BEA3E_45; }
	inline __StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816 * get_address_of_U36C3172601CE476B8B4CB4063F75FEE986E5BEA3E_45() { return &___6C3172601CE476B8B4CB4063F75FEE986E5BEA3E_45; }
	inline void set_U36C3172601CE476B8B4CB4063F75FEE986E5BEA3E_45(__StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  value)
	{
		___6C3172601CE476B8B4CB4063F75FEE986E5BEA3E_45 = value;
	}

	inline static int32_t get_offset_of_U36C6307D0B0F6F4B22D271404E3A097CC7B078DDD_46() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___6C6307D0B0F6F4B22D271404E3A097CC7B078DDD_46)); }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  get_U36C6307D0B0F6F4B22D271404E3A097CC7B078DDD_46() const { return ___6C6307D0B0F6F4B22D271404E3A097CC7B078DDD_46; }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 * get_address_of_U36C6307D0B0F6F4B22D271404E3A097CC7B078DDD_46() { return &___6C6307D0B0F6F4B22D271404E3A097CC7B078DDD_46; }
	inline void set_U36C6307D0B0F6F4B22D271404E3A097CC7B078DDD_46(__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  value)
	{
		___6C6307D0B0F6F4B22D271404E3A097CC7B078DDD_46 = value;
	}

	inline static int32_t get_offset_of_U36C99786448610AA9A550CE200DFBFE653496CCFA_47() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___6C99786448610AA9A550CE200DFBFE653496CCFA_47)); }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  get_U36C99786448610AA9A550CE200DFBFE653496CCFA_47() const { return ___6C99786448610AA9A550CE200DFBFE653496CCFA_47; }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34 * get_address_of_U36C99786448610AA9A550CE200DFBFE653496CCFA_47() { return &___6C99786448610AA9A550CE200DFBFE653496CCFA_47; }
	inline void set_U36C99786448610AA9A550CE200DFBFE653496CCFA_47(__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  value)
	{
		___6C99786448610AA9A550CE200DFBFE653496CCFA_47 = value;
	}

	inline static int32_t get_offset_of_U36E3D89B679A0E66DD78E53B4640C9726537E11A4_48() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___6E3D89B679A0E66DD78E53B4640C9726537E11A4_48)); }
	inline __StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  get_U36E3D89B679A0E66DD78E53B4640C9726537E11A4_48() const { return ___6E3D89B679A0E66DD78E53B4640C9726537E11A4_48; }
	inline __StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816 * get_address_of_U36E3D89B679A0E66DD78E53B4640C9726537E11A4_48() { return &___6E3D89B679A0E66DD78E53B4640C9726537E11A4_48; }
	inline void set_U36E3D89B679A0E66DD78E53B4640C9726537E11A4_48(__StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  value)
	{
		___6E3D89B679A0E66DD78E53B4640C9726537E11A4_48 = value;
	}

	inline static int32_t get_offset_of_U36F71EBB9EAEAD5A412B4323B2780EB7ABC873579_49() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___6F71EBB9EAEAD5A412B4323B2780EB7ABC873579_49)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U36F71EBB9EAEAD5A412B4323B2780EB7ABC873579_49() const { return ___6F71EBB9EAEAD5A412B4323B2780EB7ABC873579_49; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U36F71EBB9EAEAD5A412B4323B2780EB7ABC873579_49() { return &___6F71EBB9EAEAD5A412B4323B2780EB7ABC873579_49; }
	inline void set_U36F71EBB9EAEAD5A412B4323B2780EB7ABC873579_49(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___6F71EBB9EAEAD5A412B4323B2780EB7ABC873579_49 = value;
	}

	inline static int32_t get_offset_of_U3708D10A4339CBCA79AF1F908E9277FA653F670EF_50() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___708D10A4339CBCA79AF1F908E9277FA653F670EF_50)); }
	inline __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  get_U3708D10A4339CBCA79AF1F908E9277FA653F670EF_50() const { return ___708D10A4339CBCA79AF1F908E9277FA653F670EF_50; }
	inline __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D * get_address_of_U3708D10A4339CBCA79AF1F908E9277FA653F670EF_50() { return &___708D10A4339CBCA79AF1F908E9277FA653F670EF_50; }
	inline void set_U3708D10A4339CBCA79AF1F908E9277FA653F670EF_50(__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  value)
	{
		___708D10A4339CBCA79AF1F908E9277FA653F670EF_50 = value;
	}

	inline static int32_t get_offset_of_U3709F63077E0EC8A637406E5C4D99FEBF9404DE0A_51() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___709F63077E0EC8A637406E5C4D99FEBF9404DE0A_51)); }
	inline __StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  get_U3709F63077E0EC8A637406E5C4D99FEBF9404DE0A_51() const { return ___709F63077E0EC8A637406E5C4D99FEBF9404DE0A_51; }
	inline __StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946 * get_address_of_U3709F63077E0EC8A637406E5C4D99FEBF9404DE0A_51() { return &___709F63077E0EC8A637406E5C4D99FEBF9404DE0A_51; }
	inline void set_U3709F63077E0EC8A637406E5C4D99FEBF9404DE0A_51(__StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  value)
	{
		___709F63077E0EC8A637406E5C4D99FEBF9404DE0A_51 = value;
	}

	inline static int32_t get_offset_of_U372ED9CD72B35925260074C944C96CADDFE306044_52() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___72ED9CD72B35925260074C944C96CADDFE306044_52)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U372ED9CD72B35925260074C944C96CADDFE306044_52() const { return ___72ED9CD72B35925260074C944C96CADDFE306044_52; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U372ED9CD72B35925260074C944C96CADDFE306044_52() { return &___72ED9CD72B35925260074C944C96CADDFE306044_52; }
	inline void set_U372ED9CD72B35925260074C944C96CADDFE306044_52(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___72ED9CD72B35925260074C944C96CADDFE306044_52 = value;
	}

	inline static int32_t get_offset_of_U379A70DABA50850BC84C5F1D76C64024CE247C598_53() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___79A70DABA50850BC84C5F1D76C64024CE247C598_53)); }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  get_U379A70DABA50850BC84C5F1D76C64024CE247C598_53() const { return ___79A70DABA50850BC84C5F1D76C64024CE247C598_53; }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315 * get_address_of_U379A70DABA50850BC84C5F1D76C64024CE247C598_53() { return &___79A70DABA50850BC84C5F1D76C64024CE247C598_53; }
	inline void set_U379A70DABA50850BC84C5F1D76C64024CE247C598_53(__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  value)
	{
		___79A70DABA50850BC84C5F1D76C64024CE247C598_53 = value;
	}

	inline static int32_t get_offset_of_U37A7A5C9196FC30D2B49EDFEA5DCA11DCBCB649A3_54() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___7A7A5C9196FC30D2B49EDFEA5DCA11DCBCB649A3_54)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U37A7A5C9196FC30D2B49EDFEA5DCA11DCBCB649A3_54() const { return ___7A7A5C9196FC30D2B49EDFEA5DCA11DCBCB649A3_54; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U37A7A5C9196FC30D2B49EDFEA5DCA11DCBCB649A3_54() { return &___7A7A5C9196FC30D2B49EDFEA5DCA11DCBCB649A3_54; }
	inline void set_U37A7A5C9196FC30D2B49EDFEA5DCA11DCBCB649A3_54(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___7A7A5C9196FC30D2B49EDFEA5DCA11DCBCB649A3_54 = value;
	}

	inline static int32_t get_offset_of_U37E95FAD40702678CE0940C0AAF00B7C00586771C_55() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___7E95FAD40702678CE0940C0AAF00B7C00586771C_55)); }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  get_U37E95FAD40702678CE0940C0AAF00B7C00586771C_55() const { return ___7E95FAD40702678CE0940C0AAF00B7C00586771C_55; }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315 * get_address_of_U37E95FAD40702678CE0940C0AAF00B7C00586771C_55() { return &___7E95FAD40702678CE0940C0AAF00B7C00586771C_55; }
	inline void set_U37E95FAD40702678CE0940C0AAF00B7C00586771C_55(__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  value)
	{
		___7E95FAD40702678CE0940C0AAF00B7C00586771C_55 = value;
	}

	inline static int32_t get_offset_of_U37F415D9373FBBEFF31E2E51C7B7F968158CA85DD_56() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___7F415D9373FBBEFF31E2E51C7B7F968158CA85DD_56)); }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  get_U37F415D9373FBBEFF31E2E51C7B7F968158CA85DD_56() const { return ___7F415D9373FBBEFF31E2E51C7B7F968158CA85DD_56; }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 * get_address_of_U37F415D9373FBBEFF31E2E51C7B7F968158CA85DD_56() { return &___7F415D9373FBBEFF31E2E51C7B7F968158CA85DD_56; }
	inline void set_U37F415D9373FBBEFF31E2E51C7B7F968158CA85DD_56(__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  value)
	{
		___7F415D9373FBBEFF31E2E51C7B7F968158CA85DD_56 = value;
	}

	inline static int32_t get_offset_of_U38146FDE3318DC1C6DEF0B41B5A61BB29BBF64CAE_57() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___8146FDE3318DC1C6DEF0B41B5A61BB29BBF64CAE_57)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U38146FDE3318DC1C6DEF0B41B5A61BB29BBF64CAE_57() const { return ___8146FDE3318DC1C6DEF0B41B5A61BB29BBF64CAE_57; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U38146FDE3318DC1C6DEF0B41B5A61BB29BBF64CAE_57() { return &___8146FDE3318DC1C6DEF0B41B5A61BB29BBF64CAE_57; }
	inline void set_U38146FDE3318DC1C6DEF0B41B5A61BB29BBF64CAE_57(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___8146FDE3318DC1C6DEF0B41B5A61BB29BBF64CAE_57 = value;
	}

	inline static int32_t get_offset_of_U3826510337F148AA44E14E8E54E08BF04A8B294CE_58() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___826510337F148AA44E14E8E54E08BF04A8B294CE_58)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U3826510337F148AA44E14E8E54E08BF04A8B294CE_58() const { return ___826510337F148AA44E14E8E54E08BF04A8B294CE_58; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U3826510337F148AA44E14E8E54E08BF04A8B294CE_58() { return &___826510337F148AA44E14E8E54E08BF04A8B294CE_58; }
	inline void set_U3826510337F148AA44E14E8E54E08BF04A8B294CE_58(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___826510337F148AA44E14E8E54E08BF04A8B294CE_58 = value;
	}

	inline static int32_t get_offset_of_U384069833E42A8083F930F322499FC4BCFE543E4C_59() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___84069833E42A8083F930F322499FC4BCFE543E4C_59)); }
	inline __StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0  get_U384069833E42A8083F930F322499FC4BCFE543E4C_59() const { return ___84069833E42A8083F930F322499FC4BCFE543E4C_59; }
	inline __StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0 * get_address_of_U384069833E42A8083F930F322499FC4BCFE543E4C_59() { return &___84069833E42A8083F930F322499FC4BCFE543E4C_59; }
	inline void set_U384069833E42A8083F930F322499FC4BCFE543E4C_59(__StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0  value)
	{
		___84069833E42A8083F930F322499FC4BCFE543E4C_59 = value;
	}

	inline static int32_t get_offset_of_U38793BA75E6A8E1D82395EF60EE5DC3A69977A62B_60() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___8793BA75E6A8E1D82395EF60EE5DC3A69977A62B_60)); }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  get_U38793BA75E6A8E1D82395EF60EE5DC3A69977A62B_60() const { return ___8793BA75E6A8E1D82395EF60EE5DC3A69977A62B_60; }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315 * get_address_of_U38793BA75E6A8E1D82395EF60EE5DC3A69977A62B_60() { return &___8793BA75E6A8E1D82395EF60EE5DC3A69977A62B_60; }
	inline void set_U38793BA75E6A8E1D82395EF60EE5DC3A69977A62B_60(__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  value)
	{
		___8793BA75E6A8E1D82395EF60EE5DC3A69977A62B_60 = value;
	}

	inline static int32_t get_offset_of_U38ABAB393752BC1119B0EAD989DF450C6E04D8FF2_61() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___8ABAB393752BC1119B0EAD989DF450C6E04D8FF2_61)); }
	inline __StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  get_U38ABAB393752BC1119B0EAD989DF450C6E04D8FF2_61() const { return ___8ABAB393752BC1119B0EAD989DF450C6E04D8FF2_61; }
	inline __StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647 * get_address_of_U38ABAB393752BC1119B0EAD989DF450C6E04D8FF2_61() { return &___8ABAB393752BC1119B0EAD989DF450C6E04D8FF2_61; }
	inline void set_U38ABAB393752BC1119B0EAD989DF450C6E04D8FF2_61(__StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647  value)
	{
		___8ABAB393752BC1119B0EAD989DF450C6E04D8FF2_61 = value;
	}

	inline static int32_t get_offset_of_U38C7294D418CBC68C2F3874E06AED8C38C573FDF4_62() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___8C7294D418CBC68C2F3874E06AED8C38C573FDF4_62)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U38C7294D418CBC68C2F3874E06AED8C38C573FDF4_62() const { return ___8C7294D418CBC68C2F3874E06AED8C38C573FDF4_62; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U38C7294D418CBC68C2F3874E06AED8C38C573FDF4_62() { return &___8C7294D418CBC68C2F3874E06AED8C38C573FDF4_62; }
	inline void set_U38C7294D418CBC68C2F3874E06AED8C38C573FDF4_62(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___8C7294D418CBC68C2F3874E06AED8C38C573FDF4_62 = value;
	}

	inline static int32_t get_offset_of_U391B948814BF180E7FA42137C83AAC043E1410E0F_63() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___91B948814BF180E7FA42137C83AAC043E1410E0F_63)); }
	inline __StaticArrayInitTypeSizeU3D256_t6F967FC9ACA9D51E8C10E3FEF9B3C0033CEB171F  get_U391B948814BF180E7FA42137C83AAC043E1410E0F_63() const { return ___91B948814BF180E7FA42137C83AAC043E1410E0F_63; }
	inline __StaticArrayInitTypeSizeU3D256_t6F967FC9ACA9D51E8C10E3FEF9B3C0033CEB171F * get_address_of_U391B948814BF180E7FA42137C83AAC043E1410E0F_63() { return &___91B948814BF180E7FA42137C83AAC043E1410E0F_63; }
	inline void set_U391B948814BF180E7FA42137C83AAC043E1410E0F_63(__StaticArrayInitTypeSizeU3D256_t6F967FC9ACA9D51E8C10E3FEF9B3C0033CEB171F  value)
	{
		___91B948814BF180E7FA42137C83AAC043E1410E0F_63 = value;
	}

	inline static int32_t get_offset_of_U392E3424CA2DC6ACE639842303D9E4959BC763087_64() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___92E3424CA2DC6ACE639842303D9E4959BC763087_64)); }
	inline __StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  get_U392E3424CA2DC6ACE639842303D9E4959BC763087_64() const { return ___92E3424CA2DC6ACE639842303D9E4959BC763087_64; }
	inline __StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726 * get_address_of_U392E3424CA2DC6ACE639842303D9E4959BC763087_64() { return &___92E3424CA2DC6ACE639842303D9E4959BC763087_64; }
	inline void set_U392E3424CA2DC6ACE639842303D9E4959BC763087_64(__StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  value)
	{
		___92E3424CA2DC6ACE639842303D9E4959BC763087_64 = value;
	}

	inline static int32_t get_offset_of_U3967A5B1C1344A31AF4E7E1AAB1E89EE24D8A3FAB_65() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___967A5B1C1344A31AF4E7E1AAB1E89EE24D8A3FAB_65)); }
	inline __StaticArrayInitTypeSizeU3D1024_tC455CA8308094CC5E6ADD1A24E321B05B3134F97  get_U3967A5B1C1344A31AF4E7E1AAB1E89EE24D8A3FAB_65() const { return ___967A5B1C1344A31AF4E7E1AAB1E89EE24D8A3FAB_65; }
	inline __StaticArrayInitTypeSizeU3D1024_tC455CA8308094CC5E6ADD1A24E321B05B3134F97 * get_address_of_U3967A5B1C1344A31AF4E7E1AAB1E89EE24D8A3FAB_65() { return &___967A5B1C1344A31AF4E7E1AAB1E89EE24D8A3FAB_65; }
	inline void set_U3967A5B1C1344A31AF4E7E1AAB1E89EE24D8A3FAB_65(__StaticArrayInitTypeSizeU3D1024_tC455CA8308094CC5E6ADD1A24E321B05B3134F97  value)
	{
		___967A5B1C1344A31AF4E7E1AAB1E89EE24D8A3FAB_65 = value;
	}

	inline static int32_t get_offset_of_U396DA30D37D8C819EE7A1D9733912F74C912EB5E4_66() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___96DA30D37D8C819EE7A1D9733912F74C912EB5E4_66)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U396DA30D37D8C819EE7A1D9733912F74C912EB5E4_66() const { return ___96DA30D37D8C819EE7A1D9733912F74C912EB5E4_66; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U396DA30D37D8C819EE7A1D9733912F74C912EB5E4_66() { return &___96DA30D37D8C819EE7A1D9733912F74C912EB5E4_66; }
	inline void set_U396DA30D37D8C819EE7A1D9733912F74C912EB5E4_66(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___96DA30D37D8C819EE7A1D9733912F74C912EB5E4_66 = value;
	}

	inline static int32_t get_offset_of_U3993F10D8449193619D5641268926D30980AB1EDE_67() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___993F10D8449193619D5641268926D30980AB1EDE_67)); }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  get_U3993F10D8449193619D5641268926D30980AB1EDE_67() const { return ___993F10D8449193619D5641268926D30980AB1EDE_67; }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315 * get_address_of_U3993F10D8449193619D5641268926D30980AB1EDE_67() { return &___993F10D8449193619D5641268926D30980AB1EDE_67; }
	inline void set_U3993F10D8449193619D5641268926D30980AB1EDE_67(__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  value)
	{
		___993F10D8449193619D5641268926D30980AB1EDE_67 = value;
	}

	inline static int32_t get_offset_of_U39A1D259207846B1C51748E932A5DD3904EB818E4_68() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___9A1D259207846B1C51748E932A5DD3904EB818E4_68)); }
	inline __StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6  get_U39A1D259207846B1C51748E932A5DD3904EB818E4_68() const { return ___9A1D259207846B1C51748E932A5DD3904EB818E4_68; }
	inline __StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6 * get_address_of_U39A1D259207846B1C51748E932A5DD3904EB818E4_68() { return &___9A1D259207846B1C51748E932A5DD3904EB818E4_68; }
	inline void set_U39A1D259207846B1C51748E932A5DD3904EB818E4_68(__StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6  value)
	{
		___9A1D259207846B1C51748E932A5DD3904EB818E4_68 = value;
	}

	inline static int32_t get_offset_of_U39D23B3B23CCAE110C3CE9C5390F8C739F67D87AD_69() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___9D23B3B23CCAE110C3CE9C5390F8C739F67D87AD_69)); }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  get_U39D23B3B23CCAE110C3CE9C5390F8C739F67D87AD_69() const { return ___9D23B3B23CCAE110C3CE9C5390F8C739F67D87AD_69; }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 * get_address_of_U39D23B3B23CCAE110C3CE9C5390F8C739F67D87AD_69() { return &___9D23B3B23CCAE110C3CE9C5390F8C739F67D87AD_69; }
	inline void set_U39D23B3B23CCAE110C3CE9C5390F8C739F67D87AD_69(__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  value)
	{
		___9D23B3B23CCAE110C3CE9C5390F8C739F67D87AD_69 = value;
	}

	inline static int32_t get_offset_of_U39FF23D5ECF10A5EB1F8BC25B3B3C71AB581369EE_70() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___9FF23D5ECF10A5EB1F8BC25B3B3C71AB581369EE_70)); }
	inline __StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE  get_U39FF23D5ECF10A5EB1F8BC25B3B3C71AB581369EE_70() const { return ___9FF23D5ECF10A5EB1F8BC25B3B3C71AB581369EE_70; }
	inline __StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE * get_address_of_U39FF23D5ECF10A5EB1F8BC25B3B3C71AB581369EE_70() { return &___9FF23D5ECF10A5EB1F8BC25B3B3C71AB581369EE_70; }
	inline void set_U39FF23D5ECF10A5EB1F8BC25B3B3C71AB581369EE_70(__StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE  value)
	{
		___9FF23D5ECF10A5EB1F8BC25B3B3C71AB581369EE_70 = value;
	}

	inline static int32_t get_offset_of_A1131FF166B71E9853F0B0BD1DD153A30418529F_71() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___A1131FF166B71E9853F0B0BD1DD153A30418529F_71)); }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  get_A1131FF166B71E9853F0B0BD1DD153A30418529F_71() const { return ___A1131FF166B71E9853F0B0BD1DD153A30418529F_71; }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 * get_address_of_A1131FF166B71E9853F0B0BD1DD153A30418529F_71() { return &___A1131FF166B71E9853F0B0BD1DD153A30418529F_71; }
	inline void set_A1131FF166B71E9853F0B0BD1DD153A30418529F_71(__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  value)
	{
		___A1131FF166B71E9853F0B0BD1DD153A30418529F_71 = value;
	}

	inline static int32_t get_offset_of_A3105F394A59519BE301861D340BC3D09E41CB19_72() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___A3105F394A59519BE301861D340BC3D09E41CB19_72)); }
	inline __StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61  get_A3105F394A59519BE301861D340BC3D09E41CB19_72() const { return ___A3105F394A59519BE301861D340BC3D09E41CB19_72; }
	inline __StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61 * get_address_of_A3105F394A59519BE301861D340BC3D09E41CB19_72() { return &___A3105F394A59519BE301861D340BC3D09E41CB19_72; }
	inline void set_A3105F394A59519BE301861D340BC3D09E41CB19_72(__StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61  value)
	{
		___A3105F394A59519BE301861D340BC3D09E41CB19_72 = value;
	}

	inline static int32_t get_offset_of_A328A2984A4E6A35011BFE9252B01DA208C532BA_73() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___A328A2984A4E6A35011BFE9252B01DA208C532BA_73)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_A328A2984A4E6A35011BFE9252B01DA208C532BA_73() const { return ___A328A2984A4E6A35011BFE9252B01DA208C532BA_73; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_A328A2984A4E6A35011BFE9252B01DA208C532BA_73() { return &___A328A2984A4E6A35011BFE9252B01DA208C532BA_73; }
	inline void set_A328A2984A4E6A35011BFE9252B01DA208C532BA_73(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___A328A2984A4E6A35011BFE9252B01DA208C532BA_73 = value;
	}

	inline static int32_t get_offset_of_A4A22152FAAB8DB4A63DE422AE0D5852FD35B04A_74() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___A4A22152FAAB8DB4A63DE422AE0D5852FD35B04A_74)); }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  get_A4A22152FAAB8DB4A63DE422AE0D5852FD35B04A_74() const { return ___A4A22152FAAB8DB4A63DE422AE0D5852FD35B04A_74; }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 * get_address_of_A4A22152FAAB8DB4A63DE422AE0D5852FD35B04A_74() { return &___A4A22152FAAB8DB4A63DE422AE0D5852FD35B04A_74; }
	inline void set_A4A22152FAAB8DB4A63DE422AE0D5852FD35B04A_74(__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  value)
	{
		___A4A22152FAAB8DB4A63DE422AE0D5852FD35B04A_74 = value;
	}

	inline static int32_t get_offset_of_A62278C03345A9248C734AF81513A1F8D1B07D46_75() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___A62278C03345A9248C734AF81513A1F8D1B07D46_75)); }
	inline __StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  get_A62278C03345A9248C734AF81513A1F8D1B07D46_75() const { return ___A62278C03345A9248C734AF81513A1F8D1B07D46_75; }
	inline __StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816 * get_address_of_A62278C03345A9248C734AF81513A1F8D1B07D46_75() { return &___A62278C03345A9248C734AF81513A1F8D1B07D46_75; }
	inline void set_A62278C03345A9248C734AF81513A1F8D1B07D46_75(__StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  value)
	{
		___A62278C03345A9248C734AF81513A1F8D1B07D46_75 = value;
	}

	inline static int32_t get_offset_of_A67ECCA7AD5CEABF13FA3BD68ED24B3F5FF81CC7_76() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___A67ECCA7AD5CEABF13FA3BD68ED24B3F5FF81CC7_76)); }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  get_A67ECCA7AD5CEABF13FA3BD68ED24B3F5FF81CC7_76() const { return ___A67ECCA7AD5CEABF13FA3BD68ED24B3F5FF81CC7_76; }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 * get_address_of_A67ECCA7AD5CEABF13FA3BD68ED24B3F5FF81CC7_76() { return &___A67ECCA7AD5CEABF13FA3BD68ED24B3F5FF81CC7_76; }
	inline void set_A67ECCA7AD5CEABF13FA3BD68ED24B3F5FF81CC7_76(__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  value)
	{
		___A67ECCA7AD5CEABF13FA3BD68ED24B3F5FF81CC7_76 = value;
	}

	inline static int32_t get_offset_of_A68289F5B4251B321F6F03C741C46974A0418E64_77() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___A68289F5B4251B321F6F03C741C46974A0418E64_77)); }
	inline __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  get_A68289F5B4251B321F6F03C741C46974A0418E64_77() const { return ___A68289F5B4251B321F6F03C741C46974A0418E64_77; }
	inline __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D * get_address_of_A68289F5B4251B321F6F03C741C46974A0418E64_77() { return &___A68289F5B4251B321F6F03C741C46974A0418E64_77; }
	inline void set_A68289F5B4251B321F6F03C741C46974A0418E64_77(__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  value)
	{
		___A68289F5B4251B321F6F03C741C46974A0418E64_77 = value;
	}

	inline static int32_t get_offset_of_A6AB6C1208F342FA0C67CE497AF09D47AFA065E8_78() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___A6AB6C1208F342FA0C67CE497AF09D47AFA065E8_78)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_A6AB6C1208F342FA0C67CE497AF09D47AFA065E8_78() const { return ___A6AB6C1208F342FA0C67CE497AF09D47AFA065E8_78; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_A6AB6C1208F342FA0C67CE497AF09D47AFA065E8_78() { return &___A6AB6C1208F342FA0C67CE497AF09D47AFA065E8_78; }
	inline void set_A6AB6C1208F342FA0C67CE497AF09D47AFA065E8_78(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___A6AB6C1208F342FA0C67CE497AF09D47AFA065E8_78 = value;
	}

	inline static int32_t get_offset_of_A7CBA58371A7AF46C17847174F40F4663D2F59F5_79() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___A7CBA58371A7AF46C17847174F40F4663D2F59F5_79)); }
	inline __StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  get_A7CBA58371A7AF46C17847174F40F4663D2F59F5_79() const { return ___A7CBA58371A7AF46C17847174F40F4663D2F59F5_79; }
	inline __StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946 * get_address_of_A7CBA58371A7AF46C17847174F40F4663D2F59F5_79() { return &___A7CBA58371A7AF46C17847174F40F4663D2F59F5_79; }
	inline void set_A7CBA58371A7AF46C17847174F40F4663D2F59F5_79(__StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  value)
	{
		___A7CBA58371A7AF46C17847174F40F4663D2F59F5_79 = value;
	}

	inline static int32_t get_offset_of_A92F46ED19BFF6E5CAB48F7C846477C9AAD02DA6_80() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___A92F46ED19BFF6E5CAB48F7C846477C9AAD02DA6_80)); }
	inline __StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D  get_A92F46ED19BFF6E5CAB48F7C846477C9AAD02DA6_80() const { return ___A92F46ED19BFF6E5CAB48F7C846477C9AAD02DA6_80; }
	inline __StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D * get_address_of_A92F46ED19BFF6E5CAB48F7C846477C9AAD02DA6_80() { return &___A92F46ED19BFF6E5CAB48F7C846477C9AAD02DA6_80; }
	inline void set_A92F46ED19BFF6E5CAB48F7C846477C9AAD02DA6_80(__StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D  value)
	{
		___A92F46ED19BFF6E5CAB48F7C846477C9AAD02DA6_80 = value;
	}

	inline static int32_t get_offset_of_A988F7089BBB957B7EA2A8F2D66C7DF62B59CFD9_81() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___A988F7089BBB957B7EA2A8F2D66C7DF62B59CFD9_81)); }
	inline __StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  get_A988F7089BBB957B7EA2A8F2D66C7DF62B59CFD9_81() const { return ___A988F7089BBB957B7EA2A8F2D66C7DF62B59CFD9_81; }
	inline __StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726 * get_address_of_A988F7089BBB957B7EA2A8F2D66C7DF62B59CFD9_81() { return &___A988F7089BBB957B7EA2A8F2D66C7DF62B59CFD9_81; }
	inline void set_A988F7089BBB957B7EA2A8F2D66C7DF62B59CFD9_81(__StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  value)
	{
		___A988F7089BBB957B7EA2A8F2D66C7DF62B59CFD9_81 = value;
	}

	inline static int32_t get_offset_of_ADA84571EAB5163CC2C40A5CF9F2DF898ED1180C_82() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___ADA84571EAB5163CC2C40A5CF9F2DF898ED1180C_82)); }
	inline __StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE  get_ADA84571EAB5163CC2C40A5CF9F2DF898ED1180C_82() const { return ___ADA84571EAB5163CC2C40A5CF9F2DF898ED1180C_82; }
	inline __StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE * get_address_of_ADA84571EAB5163CC2C40A5CF9F2DF898ED1180C_82() { return &___ADA84571EAB5163CC2C40A5CF9F2DF898ED1180C_82; }
	inline void set_ADA84571EAB5163CC2C40A5CF9F2DF898ED1180C_82(__StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE  value)
	{
		___ADA84571EAB5163CC2C40A5CF9F2DF898ED1180C_82 = value;
	}

	inline static int32_t get_offset_of_B07DA862851422D423B591975D256548DFDA3DC2_83() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___B07DA862851422D423B591975D256548DFDA3DC2_83)); }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  get_B07DA862851422D423B591975D256548DFDA3DC2_83() const { return ___B07DA862851422D423B591975D256548DFDA3DC2_83; }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34 * get_address_of_B07DA862851422D423B591975D256548DFDA3DC2_83() { return &___B07DA862851422D423B591975D256548DFDA3DC2_83; }
	inline void set_B07DA862851422D423B591975D256548DFDA3DC2_83(__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  value)
	{
		___B07DA862851422D423B591975D256548DFDA3DC2_83 = value;
	}

	inline static int32_t get_offset_of_B14468BE5B21A8E0D79DA7988BBF773FDA08701A_84() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___B14468BE5B21A8E0D79DA7988BBF773FDA08701A_84)); }
	inline __StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  get_B14468BE5B21A8E0D79DA7988BBF773FDA08701A_84() const { return ___B14468BE5B21A8E0D79DA7988BBF773FDA08701A_84; }
	inline __StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF * get_address_of_B14468BE5B21A8E0D79DA7988BBF773FDA08701A_84() { return &___B14468BE5B21A8E0D79DA7988BBF773FDA08701A_84; }
	inline void set_B14468BE5B21A8E0D79DA7988BBF773FDA08701A_84(__StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  value)
	{
		___B14468BE5B21A8E0D79DA7988BBF773FDA08701A_84 = value;
	}

	inline static int32_t get_offset_of_B3124AD30FFF743F5EF54086F4B0192CF1A880CF_85() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___B3124AD30FFF743F5EF54086F4B0192CF1A880CF_85)); }
	inline __StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D  get_B3124AD30FFF743F5EF54086F4B0192CF1A880CF_85() const { return ___B3124AD30FFF743F5EF54086F4B0192CF1A880CF_85; }
	inline __StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D * get_address_of_B3124AD30FFF743F5EF54086F4B0192CF1A880CF_85() { return &___B3124AD30FFF743F5EF54086F4B0192CF1A880CF_85; }
	inline void set_B3124AD30FFF743F5EF54086F4B0192CF1A880CF_85(__StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D  value)
	{
		___B3124AD30FFF743F5EF54086F4B0192CF1A880CF_85 = value;
	}

	inline static int32_t get_offset_of_BC22A77A549E72722CCDC844425C7EA8615F3771_86() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___BC22A77A549E72722CCDC844425C7EA8615F3771_86)); }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  get_BC22A77A549E72722CCDC844425C7EA8615F3771_86() const { return ___BC22A77A549E72722CCDC844425C7EA8615F3771_86; }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 * get_address_of_BC22A77A549E72722CCDC844425C7EA8615F3771_86() { return &___BC22A77A549E72722CCDC844425C7EA8615F3771_86; }
	inline void set_BC22A77A549E72722CCDC844425C7EA8615F3771_86(__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  value)
	{
		___BC22A77A549E72722CCDC844425C7EA8615F3771_86 = value;
	}

	inline static int32_t get_offset_of_BE61A28EE8D252FD73D06806809B2E51FEB6F53C_87() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___BE61A28EE8D252FD73D06806809B2E51FEB6F53C_87)); }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  get_BE61A28EE8D252FD73D06806809B2E51FEB6F53C_87() const { return ___BE61A28EE8D252FD73D06806809B2E51FEB6F53C_87; }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34 * get_address_of_BE61A28EE8D252FD73D06806809B2E51FEB6F53C_87() { return &___BE61A28EE8D252FD73D06806809B2E51FEB6F53C_87; }
	inline void set_BE61A28EE8D252FD73D06806809B2E51FEB6F53C_87(__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  value)
	{
		___BE61A28EE8D252FD73D06806809B2E51FEB6F53C_87 = value;
	}

	inline static int32_t get_offset_of_BFB130DEFFB14CE563713BDDF442B98CCACE8A8F_88() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___BFB130DEFFB14CE563713BDDF442B98CCACE8A8F_88)); }
	inline __StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  get_BFB130DEFFB14CE563713BDDF442B98CCACE8A8F_88() const { return ___BFB130DEFFB14CE563713BDDF442B98CCACE8A8F_88; }
	inline __StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726 * get_address_of_BFB130DEFFB14CE563713BDDF442B98CCACE8A8F_88() { return &___BFB130DEFFB14CE563713BDDF442B98CCACE8A8F_88; }
	inline void set_BFB130DEFFB14CE563713BDDF442B98CCACE8A8F_88(__StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  value)
	{
		___BFB130DEFFB14CE563713BDDF442B98CCACE8A8F_88 = value;
	}

	inline static int32_t get_offset_of_C06E47E37A990899E03C953F64C95F7E3210B17E_89() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___C06E47E37A990899E03C953F64C95F7E3210B17E_89)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_C06E47E37A990899E03C953F64C95F7E3210B17E_89() const { return ___C06E47E37A990899E03C953F64C95F7E3210B17E_89; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_C06E47E37A990899E03C953F64C95F7E3210B17E_89() { return &___C06E47E37A990899E03C953F64C95F7E3210B17E_89; }
	inline void set_C06E47E37A990899E03C953F64C95F7E3210B17E_89(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___C06E47E37A990899E03C953F64C95F7E3210B17E_89 = value;
	}

	inline static int32_t get_offset_of_C0A8C45EE4F4DF297B1688A2C5BDE5BC312071C4_90() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___C0A8C45EE4F4DF297B1688A2C5BDE5BC312071C4_90)); }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  get_C0A8C45EE4F4DF297B1688A2C5BDE5BC312071C4_90() const { return ___C0A8C45EE4F4DF297B1688A2C5BDE5BC312071C4_90; }
	inline __StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF * get_address_of_C0A8C45EE4F4DF297B1688A2C5BDE5BC312071C4_90() { return &___C0A8C45EE4F4DF297B1688A2C5BDE5BC312071C4_90; }
	inline void set_C0A8C45EE4F4DF297B1688A2C5BDE5BC312071C4_90(__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF  value)
	{
		___C0A8C45EE4F4DF297B1688A2C5BDE5BC312071C4_90 = value;
	}

	inline static int32_t get_offset_of_C5709FC40EFF23BB977BBC3DA4BCA193930C5B92_91() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___C5709FC40EFF23BB977BBC3DA4BCA193930C5B92_91)); }
	inline __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  get_C5709FC40EFF23BB977BBC3DA4BCA193930C5B92_91() const { return ___C5709FC40EFF23BB977BBC3DA4BCA193930C5B92_91; }
	inline __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D * get_address_of_C5709FC40EFF23BB977BBC3DA4BCA193930C5B92_91() { return &___C5709FC40EFF23BB977BBC3DA4BCA193930C5B92_91; }
	inline void set_C5709FC40EFF23BB977BBC3DA4BCA193930C5B92_91(__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  value)
	{
		___C5709FC40EFF23BB977BBC3DA4BCA193930C5B92_91 = value;
	}

	inline static int32_t get_offset_of_C6DFEE18BA4DC30A7D7D90AB72AB0228B66ED858_92() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___C6DFEE18BA4DC30A7D7D90AB72AB0228B66ED858_92)); }
	inline __StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  get_C6DFEE18BA4DC30A7D7D90AB72AB0228B66ED858_92() const { return ___C6DFEE18BA4DC30A7D7D90AB72AB0228B66ED858_92; }
	inline __StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF * get_address_of_C6DFEE18BA4DC30A7D7D90AB72AB0228B66ED858_92() { return &___C6DFEE18BA4DC30A7D7D90AB72AB0228B66ED858_92; }
	inline void set_C6DFEE18BA4DC30A7D7D90AB72AB0228B66ED858_92(__StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF  value)
	{
		___C6DFEE18BA4DC30A7D7D90AB72AB0228B66ED858_92 = value;
	}

	inline static int32_t get_offset_of_C881D9C683CE2D13A2438CAD551803C8B1100C53_93() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___C881D9C683CE2D13A2438CAD551803C8B1100C53_93)); }
	inline __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  get_C881D9C683CE2D13A2438CAD551803C8B1100C53_93() const { return ___C881D9C683CE2D13A2438CAD551803C8B1100C53_93; }
	inline __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D * get_address_of_C881D9C683CE2D13A2438CAD551803C8B1100C53_93() { return &___C881D9C683CE2D13A2438CAD551803C8B1100C53_93; }
	inline void set_C881D9C683CE2D13A2438CAD551803C8B1100C53_93(__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  value)
	{
		___C881D9C683CE2D13A2438CAD551803C8B1100C53_93 = value;
	}

	inline static int32_t get_offset_of_CBD12AC86FBCD56155B93A9CA77947E68793C2B5_94() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___CBD12AC86FBCD56155B93A9CA77947E68793C2B5_94)); }
	inline __StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140  get_CBD12AC86FBCD56155B93A9CA77947E68793C2B5_94() const { return ___CBD12AC86FBCD56155B93A9CA77947E68793C2B5_94; }
	inline __StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140 * get_address_of_CBD12AC86FBCD56155B93A9CA77947E68793C2B5_94() { return &___CBD12AC86FBCD56155B93A9CA77947E68793C2B5_94; }
	inline void set_CBD12AC86FBCD56155B93A9CA77947E68793C2B5_94(__StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140  value)
	{
		___CBD12AC86FBCD56155B93A9CA77947E68793C2B5_94 = value;
	}

	inline static int32_t get_offset_of_CC3901AF2214B4D6ADEB73EB932BC1F0E40C318C_95() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___CC3901AF2214B4D6ADEB73EB932BC1F0E40C318C_95)); }
	inline __StaticArrayInitTypeSizeU3D34_t9C713A05A7B6B5C6D004D41B59A4F6B7D561459A  get_CC3901AF2214B4D6ADEB73EB932BC1F0E40C318C_95() const { return ___CC3901AF2214B4D6ADEB73EB932BC1F0E40C318C_95; }
	inline __StaticArrayInitTypeSizeU3D34_t9C713A05A7B6B5C6D004D41B59A4F6B7D561459A * get_address_of_CC3901AF2214B4D6ADEB73EB932BC1F0E40C318C_95() { return &___CC3901AF2214B4D6ADEB73EB932BC1F0E40C318C_95; }
	inline void set_CC3901AF2214B4D6ADEB73EB932BC1F0E40C318C_95(__StaticArrayInitTypeSizeU3D34_t9C713A05A7B6B5C6D004D41B59A4F6B7D561459A  value)
	{
		___CC3901AF2214B4D6ADEB73EB932BC1F0E40C318C_95 = value;
	}

	inline static int32_t get_offset_of_CE23A1D2337FC685E2FA96AC9F8B8BA6EA0C5BDD_96() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___CE23A1D2337FC685E2FA96AC9F8B8BA6EA0C5BDD_96)); }
	inline __StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  get_CE23A1D2337FC685E2FA96AC9F8B8BA6EA0C5BDD_96() const { return ___CE23A1D2337FC685E2FA96AC9F8B8BA6EA0C5BDD_96; }
	inline __StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726 * get_address_of_CE23A1D2337FC685E2FA96AC9F8B8BA6EA0C5BDD_96() { return &___CE23A1D2337FC685E2FA96AC9F8B8BA6EA0C5BDD_96; }
	inline void set_CE23A1D2337FC685E2FA96AC9F8B8BA6EA0C5BDD_96(__StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726  value)
	{
		___CE23A1D2337FC685E2FA96AC9F8B8BA6EA0C5BDD_96 = value;
	}

	inline static int32_t get_offset_of_D087BC49F1FEB5D2824F510253A805FCC2506F71_97() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___D087BC49F1FEB5D2824F510253A805FCC2506F71_97)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_D087BC49F1FEB5D2824F510253A805FCC2506F71_97() const { return ___D087BC49F1FEB5D2824F510253A805FCC2506F71_97; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_D087BC49F1FEB5D2824F510253A805FCC2506F71_97() { return &___D087BC49F1FEB5D2824F510253A805FCC2506F71_97; }
	inline void set_D087BC49F1FEB5D2824F510253A805FCC2506F71_97(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___D087BC49F1FEB5D2824F510253A805FCC2506F71_97 = value;
	}

	inline static int32_t get_offset_of_D177776BF8B5B13646CB32C3E3C2E3A3F6389454_98() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___D177776BF8B5B13646CB32C3E3C2E3A3F6389454_98)); }
	inline __StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61  get_D177776BF8B5B13646CB32C3E3C2E3A3F6389454_98() const { return ___D177776BF8B5B13646CB32C3E3C2E3A3F6389454_98; }
	inline __StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61 * get_address_of_D177776BF8B5B13646CB32C3E3C2E3A3F6389454_98() { return &___D177776BF8B5B13646CB32C3E3C2E3A3F6389454_98; }
	inline void set_D177776BF8B5B13646CB32C3E3C2E3A3F6389454_98(__StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61  value)
	{
		___D177776BF8B5B13646CB32C3E3C2E3A3F6389454_98 = value;
	}

	inline static int32_t get_offset_of_D2CE52450BFB35255920A8F83FADB36945ABF964_99() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___D2CE52450BFB35255920A8F83FADB36945ABF964_99)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_D2CE52450BFB35255920A8F83FADB36945ABF964_99() const { return ___D2CE52450BFB35255920A8F83FADB36945ABF964_99; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_D2CE52450BFB35255920A8F83FADB36945ABF964_99() { return &___D2CE52450BFB35255920A8F83FADB36945ABF964_99; }
	inline void set_D2CE52450BFB35255920A8F83FADB36945ABF964_99(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___D2CE52450BFB35255920A8F83FADB36945ABF964_99 = value;
	}

	inline static int32_t get_offset_of_D5C60CDEF63D72F5032E90D8B9D5EFF3E566D8F4_100() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___D5C60CDEF63D72F5032E90D8B9D5EFF3E566D8F4_100)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_D5C60CDEF63D72F5032E90D8B9D5EFF3E566D8F4_100() const { return ___D5C60CDEF63D72F5032E90D8B9D5EFF3E566D8F4_100; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_D5C60CDEF63D72F5032E90D8B9D5EFF3E566D8F4_100() { return &___D5C60CDEF63D72F5032E90D8B9D5EFF3E566D8F4_100; }
	inline void set_D5C60CDEF63D72F5032E90D8B9D5EFF3E566D8F4_100(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___D5C60CDEF63D72F5032E90D8B9D5EFF3E566D8F4_100 = value;
	}

	inline static int32_t get_offset_of_D5C92607E010304E7241899C8E6ADF0EE0CBFD31_101() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___D5C92607E010304E7241899C8E6ADF0EE0CBFD31_101)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_D5C92607E010304E7241899C8E6ADF0EE0CBFD31_101() const { return ___D5C92607E010304E7241899C8E6ADF0EE0CBFD31_101; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_D5C92607E010304E7241899C8E6ADF0EE0CBFD31_101() { return &___D5C92607E010304E7241899C8E6ADF0EE0CBFD31_101; }
	inline void set_D5C92607E010304E7241899C8E6ADF0EE0CBFD31_101(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___D5C92607E010304E7241899C8E6ADF0EE0CBFD31_101 = value;
	}

	inline static int32_t get_offset_of_D8D801E3DDC2422ADA99C1D4210803A5F734F968_102() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___D8D801E3DDC2422ADA99C1D4210803A5F734F968_102)); }
	inline __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  get_D8D801E3DDC2422ADA99C1D4210803A5F734F968_102() const { return ___D8D801E3DDC2422ADA99C1D4210803A5F734F968_102; }
	inline __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1 * get_address_of_D8D801E3DDC2422ADA99C1D4210803A5F734F968_102() { return &___D8D801E3DDC2422ADA99C1D4210803A5F734F968_102; }
	inline void set_D8D801E3DDC2422ADA99C1D4210803A5F734F968_102(__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  value)
	{
		___D8D801E3DDC2422ADA99C1D4210803A5F734F968_102 = value;
	}

	inline static int32_t get_offset_of_DDF6D11A3E93BE497F5BAC07290983B9D3832ECE_103() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___DDF6D11A3E93BE497F5BAC07290983B9D3832ECE_103)); }
	inline __StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  get_DDF6D11A3E93BE497F5BAC07290983B9D3832ECE_103() const { return ___DDF6D11A3E93BE497F5BAC07290983B9D3832ECE_103; }
	inline __StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946 * get_address_of_DDF6D11A3E93BE497F5BAC07290983B9D3832ECE_103() { return &___DDF6D11A3E93BE497F5BAC07290983B9D3832ECE_103; }
	inline void set_DDF6D11A3E93BE497F5BAC07290983B9D3832ECE_103(__StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  value)
	{
		___DDF6D11A3E93BE497F5BAC07290983B9D3832ECE_103 = value;
	}

	inline static int32_t get_offset_of_DF97F08BD5310D2EBC2E21DE6536AE30347D8434_104() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___DF97F08BD5310D2EBC2E21DE6536AE30347D8434_104)); }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  get_DF97F08BD5310D2EBC2E21DE6536AE30347D8434_104() const { return ___DF97F08BD5310D2EBC2E21DE6536AE30347D8434_104; }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 * get_address_of_DF97F08BD5310D2EBC2E21DE6536AE30347D8434_104() { return &___DF97F08BD5310D2EBC2E21DE6536AE30347D8434_104; }
	inline void set_DF97F08BD5310D2EBC2E21DE6536AE30347D8434_104(__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  value)
	{
		___DF97F08BD5310D2EBC2E21DE6536AE30347D8434_104 = value;
	}

	inline static int32_t get_offset_of_E2EF5AD9211AEBF1A7D759556F680C7916E434D8_105() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___E2EF5AD9211AEBF1A7D759556F680C7916E434D8_105)); }
	inline __StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  get_E2EF5AD9211AEBF1A7D759556F680C7916E434D8_105() const { return ___E2EF5AD9211AEBF1A7D759556F680C7916E434D8_105; }
	inline __StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946 * get_address_of_E2EF5AD9211AEBF1A7D759556F680C7916E434D8_105() { return &___E2EF5AD9211AEBF1A7D759556F680C7916E434D8_105; }
	inline void set_E2EF5AD9211AEBF1A7D759556F680C7916E434D8_105(__StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  value)
	{
		___E2EF5AD9211AEBF1A7D759556F680C7916E434D8_105 = value;
	}

	inline static int32_t get_offset_of_E44BF9B4DEFF99F72DA9F21344C344E2CD352BCB_106() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___E44BF9B4DEFF99F72DA9F21344C344E2CD352BCB_106)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_E44BF9B4DEFF99F72DA9F21344C344E2CD352BCB_106() const { return ___E44BF9B4DEFF99F72DA9F21344C344E2CD352BCB_106; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_E44BF9B4DEFF99F72DA9F21344C344E2CD352BCB_106() { return &___E44BF9B4DEFF99F72DA9F21344C344E2CD352BCB_106; }
	inline void set_E44BF9B4DEFF99F72DA9F21344C344E2CD352BCB_106(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___E44BF9B4DEFF99F72DA9F21344C344E2CD352BCB_106 = value;
	}

	inline static int32_t get_offset_of_E78171182D510376B72BC5204A0D1D488DED6D00_107() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___E78171182D510376B72BC5204A0D1D488DED6D00_107)); }
	inline __StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  get_E78171182D510376B72BC5204A0D1D488DED6D00_107() const { return ___E78171182D510376B72BC5204A0D1D488DED6D00_107; }
	inline __StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816 * get_address_of_E78171182D510376B72BC5204A0D1D488DED6D00_107() { return &___E78171182D510376B72BC5204A0D1D488DED6D00_107; }
	inline void set_E78171182D510376B72BC5204A0D1D488DED6D00_107(__StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  value)
	{
		___E78171182D510376B72BC5204A0D1D488DED6D00_107 = value;
	}

	inline static int32_t get_offset_of_E99B63E754303E8FA67A1C2AEA3F576D805E2FA9_108() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___E99B63E754303E8FA67A1C2AEA3F576D805E2FA9_108)); }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  get_E99B63E754303E8FA67A1C2AEA3F576D805E2FA9_108() const { return ___E99B63E754303E8FA67A1C2AEA3F576D805E2FA9_108; }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 * get_address_of_E99B63E754303E8FA67A1C2AEA3F576D805E2FA9_108() { return &___E99B63E754303E8FA67A1C2AEA3F576D805E2FA9_108; }
	inline void set_E99B63E754303E8FA67A1C2AEA3F576D805E2FA9_108(__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  value)
	{
		___E99B63E754303E8FA67A1C2AEA3F576D805E2FA9_108 = value;
	}

	inline static int32_t get_offset_of_EBB7471D163460A589A0BB5C1FB2B55811F80E11_109() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___EBB7471D163460A589A0BB5C1FB2B55811F80E11_109)); }
	inline __StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140  get_EBB7471D163460A589A0BB5C1FB2B55811F80E11_109() const { return ___EBB7471D163460A589A0BB5C1FB2B55811F80E11_109; }
	inline __StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140 * get_address_of_EBB7471D163460A589A0BB5C1FB2B55811F80E11_109() { return &___EBB7471D163460A589A0BB5C1FB2B55811F80E11_109; }
	inline void set_EBB7471D163460A589A0BB5C1FB2B55811F80E11_109(__StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140  value)
	{
		___EBB7471D163460A589A0BB5C1FB2B55811F80E11_109 = value;
	}

	inline static int32_t get_offset_of_EC71709D141BB87D172E6D9D919DF53D7A4EB217_110() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___EC71709D141BB87D172E6D9D919DF53D7A4EB217_110)); }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  get_EC71709D141BB87D172E6D9D919DF53D7A4EB217_110() const { return ___EC71709D141BB87D172E6D9D919DF53D7A4EB217_110; }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 * get_address_of_EC71709D141BB87D172E6D9D919DF53D7A4EB217_110() { return &___EC71709D141BB87D172E6D9D919DF53D7A4EB217_110; }
	inline void set_EC71709D141BB87D172E6D9D919DF53D7A4EB217_110(__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  value)
	{
		___EC71709D141BB87D172E6D9D919DF53D7A4EB217_110 = value;
	}

	inline static int32_t get_offset_of_ED6CCE320D9C94CDED123AD06BD6A29CC75A012C_111() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___ED6CCE320D9C94CDED123AD06BD6A29CC75A012C_111)); }
	inline __StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D  get_ED6CCE320D9C94CDED123AD06BD6A29CC75A012C_111() const { return ___ED6CCE320D9C94CDED123AD06BD6A29CC75A012C_111; }
	inline __StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D * get_address_of_ED6CCE320D9C94CDED123AD06BD6A29CC75A012C_111() { return &___ED6CCE320D9C94CDED123AD06BD6A29CC75A012C_111; }
	inline void set_ED6CCE320D9C94CDED123AD06BD6A29CC75A012C_111(__StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D  value)
	{
		___ED6CCE320D9C94CDED123AD06BD6A29CC75A012C_111 = value;
	}

	inline static int32_t get_offset_of_F0E124B606159405D7C0D19C7158FD45A4F27EF4_112() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___F0E124B606159405D7C0D19C7158FD45A4F27EF4_112)); }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  get_F0E124B606159405D7C0D19C7158FD45A4F27EF4_112() const { return ___F0E124B606159405D7C0D19C7158FD45A4F27EF4_112; }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 * get_address_of_F0E124B606159405D7C0D19C7158FD45A4F27EF4_112() { return &___F0E124B606159405D7C0D19C7158FD45A4F27EF4_112; }
	inline void set_F0E124B606159405D7C0D19C7158FD45A4F27EF4_112(__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  value)
	{
		___F0E124B606159405D7C0D19C7158FD45A4F27EF4_112 = value;
	}

	inline static int32_t get_offset_of_F1781D4343E58B231D363C7698B8B7C63C173F92_113() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___F1781D4343E58B231D363C7698B8B7C63C173F92_113)); }
	inline __StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  get_F1781D4343E58B231D363C7698B8B7C63C173F92_113() const { return ___F1781D4343E58B231D363C7698B8B7C63C173F92_113; }
	inline __StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816 * get_address_of_F1781D4343E58B231D363C7698B8B7C63C173F92_113() { return &___F1781D4343E58B231D363C7698B8B7C63C173F92_113; }
	inline void set_F1781D4343E58B231D363C7698B8B7C63C173F92_113(__StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816  value)
	{
		___F1781D4343E58B231D363C7698B8B7C63C173F92_113 = value;
	}

	inline static int32_t get_offset_of_F24A3495EC779AA95747314593B416FA283F5A15_114() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___F24A3495EC779AA95747314593B416FA283F5A15_114)); }
	inline __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  get_F24A3495EC779AA95747314593B416FA283F5A15_114() const { return ___F24A3495EC779AA95747314593B416FA283F5A15_114; }
	inline __StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D * get_address_of_F24A3495EC779AA95747314593B416FA283F5A15_114() { return &___F24A3495EC779AA95747314593B416FA283F5A15_114; }
	inline void set_F24A3495EC779AA95747314593B416FA283F5A15_114(__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D  value)
	{
		___F24A3495EC779AA95747314593B416FA283F5A15_114 = value;
	}

	inline static int32_t get_offset_of_F2BCD6EC6BB1D97B183FA5A1C54B5725010F0038_115() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___F2BCD6EC6BB1D97B183FA5A1C54B5725010F0038_115)); }
	inline __StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  get_F2BCD6EC6BB1D97B183FA5A1C54B5725010F0038_115() const { return ___F2BCD6EC6BB1D97B183FA5A1C54B5725010F0038_115; }
	inline __StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946 * get_address_of_F2BCD6EC6BB1D97B183FA5A1C54B5725010F0038_115() { return &___F2BCD6EC6BB1D97B183FA5A1C54B5725010F0038_115; }
	inline void set_F2BCD6EC6BB1D97B183FA5A1C54B5725010F0038_115(__StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946  value)
	{
		___F2BCD6EC6BB1D97B183FA5A1C54B5725010F0038_115 = value;
	}

	inline static int32_t get_offset_of_F301F2CE6EEF9D8AC92595F503B297E3F20B5267_116() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___F301F2CE6EEF9D8AC92595F503B297E3F20B5267_116)); }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  get_F301F2CE6EEF9D8AC92595F503B297E3F20B5267_116() const { return ___F301F2CE6EEF9D8AC92595F503B297E3F20B5267_116; }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34 * get_address_of_F301F2CE6EEF9D8AC92595F503B297E3F20B5267_116() { return &___F301F2CE6EEF9D8AC92595F503B297E3F20B5267_116; }
	inline void set_F301F2CE6EEF9D8AC92595F503B297E3F20B5267_116(__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  value)
	{
		___F301F2CE6EEF9D8AC92595F503B297E3F20B5267_116 = value;
	}

	inline static int32_t get_offset_of_F4F4DAB1172A7E33DC53F7AA668C2AC7E9AF6828_117() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___F4F4DAB1172A7E33DC53F7AA668C2AC7E9AF6828_117)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_F4F4DAB1172A7E33DC53F7AA668C2AC7E9AF6828_117() const { return ___F4F4DAB1172A7E33DC53F7AA668C2AC7E9AF6828_117; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_F4F4DAB1172A7E33DC53F7AA668C2AC7E9AF6828_117() { return &___F4F4DAB1172A7E33DC53F7AA668C2AC7E9AF6828_117; }
	inline void set_F4F4DAB1172A7E33DC53F7AA668C2AC7E9AF6828_117(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___F4F4DAB1172A7E33DC53F7AA668C2AC7E9AF6828_117 = value;
	}

	inline static int32_t get_offset_of_F7A1E3117FA8F941095E83511E1B55F5E050747C_118() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___F7A1E3117FA8F941095E83511E1B55F5E050747C_118)); }
	inline __StaticArrayInitTypeSizeU3D88_tACC0D3E292CC0817B7908ECD8382C424C141A9C9  get_F7A1E3117FA8F941095E83511E1B55F5E050747C_118() const { return ___F7A1E3117FA8F941095E83511E1B55F5E050747C_118; }
	inline __StaticArrayInitTypeSizeU3D88_tACC0D3E292CC0817B7908ECD8382C424C141A9C9 * get_address_of_F7A1E3117FA8F941095E83511E1B55F5E050747C_118() { return &___F7A1E3117FA8F941095E83511E1B55F5E050747C_118; }
	inline void set_F7A1E3117FA8F941095E83511E1B55F5E050747C_118(__StaticArrayInitTypeSizeU3D88_tACC0D3E292CC0817B7908ECD8382C424C141A9C9  value)
	{
		___F7A1E3117FA8F941095E83511E1B55F5E050747C_118 = value;
	}

	inline static int32_t get_offset_of_F7C62050EA6AAC50646295736EF3E5083D7781D4_119() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___F7C62050EA6AAC50646295736EF3E5083D7781D4_119)); }
	inline __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  get_F7C62050EA6AAC50646295736EF3E5083D7781D4_119() const { return ___F7C62050EA6AAC50646295736EF3E5083D7781D4_119; }
	inline __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1 * get_address_of_F7C62050EA6AAC50646295736EF3E5083D7781D4_119() { return &___F7C62050EA6AAC50646295736EF3E5083D7781D4_119; }
	inline void set_F7C62050EA6AAC50646295736EF3E5083D7781D4_119(__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  value)
	{
		___F7C62050EA6AAC50646295736EF3E5083D7781D4_119 = value;
	}

	inline static int32_t get_offset_of_FA2D9B0982EC9C8B3280A5D325C7A19C52E902D6_120() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___FA2D9B0982EC9C8B3280A5D325C7A19C52E902D6_120)); }
	inline __StaticArrayInitTypeSizeU3D120_tFE429A3C8C8CC2AA48009B28DC92C5FAC405B976  get_FA2D9B0982EC9C8B3280A5D325C7A19C52E902D6_120() const { return ___FA2D9B0982EC9C8B3280A5D325C7A19C52E902D6_120; }
	inline __StaticArrayInitTypeSizeU3D120_tFE429A3C8C8CC2AA48009B28DC92C5FAC405B976 * get_address_of_FA2D9B0982EC9C8B3280A5D325C7A19C52E902D6_120() { return &___FA2D9B0982EC9C8B3280A5D325C7A19C52E902D6_120; }
	inline void set_FA2D9B0982EC9C8B3280A5D325C7A19C52E902D6_120(__StaticArrayInitTypeSizeU3D120_tFE429A3C8C8CC2AA48009B28DC92C5FAC405B976  value)
	{
		___FA2D9B0982EC9C8B3280A5D325C7A19C52E902D6_120 = value;
	}

	inline static int32_t get_offset_of_FB4FBD597E39496564E6322822D8DD70A68ADE47_121() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___FB4FBD597E39496564E6322822D8DD70A68ADE47_121)); }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  get_FB4FBD597E39496564E6322822D8DD70A68ADE47_121() const { return ___FB4FBD597E39496564E6322822D8DD70A68ADE47_121; }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315 * get_address_of_FB4FBD597E39496564E6322822D8DD70A68ADE47_121() { return &___FB4FBD597E39496564E6322822D8DD70A68ADE47_121; }
	inline void set_FB4FBD597E39496564E6322822D8DD70A68ADE47_121(__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  value)
	{
		___FB4FBD597E39496564E6322822D8DD70A68ADE47_121 = value;
	}

	inline static int32_t get_offset_of_FD06A67F6CBC0BE9E112D56B89B86432104BD055_122() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___FD06A67F6CBC0BE9E112D56B89B86432104BD055_122)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_FD06A67F6CBC0BE9E112D56B89B86432104BD055_122() const { return ___FD06A67F6CBC0BE9E112D56B89B86432104BD055_122; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_FD06A67F6CBC0BE9E112D56B89B86432104BD055_122() { return &___FD06A67F6CBC0BE9E112D56B89B86432104BD055_122; }
	inline void set_FD06A67F6CBC0BE9E112D56B89B86432104BD055_122(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___FD06A67F6CBC0BE9E112D56B89B86432104BD055_122 = value;
	}

	inline static int32_t get_offset_of_FD84D4E70F766B0C44E5012C056C44B2E5E90E9A_123() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___FD84D4E70F766B0C44E5012C056C44B2E5E90E9A_123)); }
	inline __StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61  get_FD84D4E70F766B0C44E5012C056C44B2E5E90E9A_123() const { return ___FD84D4E70F766B0C44E5012C056C44B2E5E90E9A_123; }
	inline __StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61 * get_address_of_FD84D4E70F766B0C44E5012C056C44B2E5E90E9A_123() { return &___FD84D4E70F766B0C44E5012C056C44B2E5E90E9A_123; }
	inline void set_FD84D4E70F766B0C44E5012C056C44B2E5E90E9A_123(__StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61  value)
	{
		___FD84D4E70F766B0C44E5012C056C44B2E5E90E9A_123 = value;
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396  : public RuntimeObject
{
public:
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<>4__this
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * ___U3CU3E4__this_0;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubes
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___cubes_1;
	// System.Int32[] DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::tweenIds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___tweenIds_2;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::onCompleteCount
	int32_t ___onCompleteCount_3;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeToTrans
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cubeToTrans_4;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeDestEnd
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___cubeDestEnd_5;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeSpline
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cubeSpline_6;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::jumpTimeId
	int32_t ___jumpTimeId_7;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::jumpCube
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___jumpCube_8;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::zeroCube
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___zeroCube_9;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeScale
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cubeScale_10;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeRotate
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cubeRotate_11;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeRotateA
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cubeRotateA_12;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeRotateB
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cubeRotateB_13;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::onStartTime
	float ___onStartTime_14;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::beforePos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___beforePos_15;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::beforePos2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___beforePos2_16;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::totalEasingCheck
	int32_t ___totalEasingCheck_17;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::totalEasingCheckSuccess
	int32_t ___totalEasingCheckSuccess_18;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::value2UpdateCalled
	bool ___value2UpdateCalled_19;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<>9__21
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__21_20;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___U3CU3E4__this_0)); }
	inline TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_cubes_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___cubes_1)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_cubes_1() const { return ___cubes_1; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_cubes_1() { return &___cubes_1; }
	inline void set_cubes_1(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___cubes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubes_1), (void*)value);
	}

	inline static int32_t get_offset_of_tweenIds_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___tweenIds_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_tweenIds_2() const { return ___tweenIds_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_tweenIds_2() { return &___tweenIds_2; }
	inline void set_tweenIds_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___tweenIds_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweenIds_2), (void*)value);
	}

	inline static int32_t get_offset_of_onCompleteCount_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___onCompleteCount_3)); }
	inline int32_t get_onCompleteCount_3() const { return ___onCompleteCount_3; }
	inline int32_t* get_address_of_onCompleteCount_3() { return &___onCompleteCount_3; }
	inline void set_onCompleteCount_3(int32_t value)
	{
		___onCompleteCount_3 = value;
	}

	inline static int32_t get_offset_of_cubeToTrans_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___cubeToTrans_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cubeToTrans_4() const { return ___cubeToTrans_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cubeToTrans_4() { return &___cubeToTrans_4; }
	inline void set_cubeToTrans_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cubeToTrans_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeToTrans_4), (void*)value);
	}

	inline static int32_t get_offset_of_cubeDestEnd_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___cubeDestEnd_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_cubeDestEnd_5() const { return ___cubeDestEnd_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_cubeDestEnd_5() { return &___cubeDestEnd_5; }
	inline void set_cubeDestEnd_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___cubeDestEnd_5 = value;
	}

	inline static int32_t get_offset_of_cubeSpline_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___cubeSpline_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cubeSpline_6() const { return ___cubeSpline_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cubeSpline_6() { return &___cubeSpline_6; }
	inline void set_cubeSpline_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cubeSpline_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeSpline_6), (void*)value);
	}

	inline static int32_t get_offset_of_jumpTimeId_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___jumpTimeId_7)); }
	inline int32_t get_jumpTimeId_7() const { return ___jumpTimeId_7; }
	inline int32_t* get_address_of_jumpTimeId_7() { return &___jumpTimeId_7; }
	inline void set_jumpTimeId_7(int32_t value)
	{
		___jumpTimeId_7 = value;
	}

	inline static int32_t get_offset_of_jumpCube_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___jumpCube_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_jumpCube_8() const { return ___jumpCube_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_jumpCube_8() { return &___jumpCube_8; }
	inline void set_jumpCube_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___jumpCube_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jumpCube_8), (void*)value);
	}

	inline static int32_t get_offset_of_zeroCube_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___zeroCube_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_zeroCube_9() const { return ___zeroCube_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_zeroCube_9() { return &___zeroCube_9; }
	inline void set_zeroCube_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___zeroCube_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___zeroCube_9), (void*)value);
	}

	inline static int32_t get_offset_of_cubeScale_10() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___cubeScale_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cubeScale_10() const { return ___cubeScale_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cubeScale_10() { return &___cubeScale_10; }
	inline void set_cubeScale_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cubeScale_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeScale_10), (void*)value);
	}

	inline static int32_t get_offset_of_cubeRotate_11() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___cubeRotate_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cubeRotate_11() const { return ___cubeRotate_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cubeRotate_11() { return &___cubeRotate_11; }
	inline void set_cubeRotate_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cubeRotate_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeRotate_11), (void*)value);
	}

	inline static int32_t get_offset_of_cubeRotateA_12() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___cubeRotateA_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cubeRotateA_12() const { return ___cubeRotateA_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cubeRotateA_12() { return &___cubeRotateA_12; }
	inline void set_cubeRotateA_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cubeRotateA_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeRotateA_12), (void*)value);
	}

	inline static int32_t get_offset_of_cubeRotateB_13() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___cubeRotateB_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cubeRotateB_13() const { return ___cubeRotateB_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cubeRotateB_13() { return &___cubeRotateB_13; }
	inline void set_cubeRotateB_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cubeRotateB_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeRotateB_13), (void*)value);
	}

	inline static int32_t get_offset_of_onStartTime_14() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___onStartTime_14)); }
	inline float get_onStartTime_14() const { return ___onStartTime_14; }
	inline float* get_address_of_onStartTime_14() { return &___onStartTime_14; }
	inline void set_onStartTime_14(float value)
	{
		___onStartTime_14 = value;
	}

	inline static int32_t get_offset_of_beforePos_15() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___beforePos_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_beforePos_15() const { return ___beforePos_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_beforePos_15() { return &___beforePos_15; }
	inline void set_beforePos_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___beforePos_15 = value;
	}

	inline static int32_t get_offset_of_beforePos2_16() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___beforePos2_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_beforePos2_16() const { return ___beforePos2_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_beforePos2_16() { return &___beforePos2_16; }
	inline void set_beforePos2_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___beforePos2_16 = value;
	}

	inline static int32_t get_offset_of_totalEasingCheck_17() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___totalEasingCheck_17)); }
	inline int32_t get_totalEasingCheck_17() const { return ___totalEasingCheck_17; }
	inline int32_t* get_address_of_totalEasingCheck_17() { return &___totalEasingCheck_17; }
	inline void set_totalEasingCheck_17(int32_t value)
	{
		___totalEasingCheck_17 = value;
	}

	inline static int32_t get_offset_of_totalEasingCheckSuccess_18() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___totalEasingCheckSuccess_18)); }
	inline int32_t get_totalEasingCheckSuccess_18() const { return ___totalEasingCheckSuccess_18; }
	inline int32_t* get_address_of_totalEasingCheckSuccess_18() { return &___totalEasingCheckSuccess_18; }
	inline void set_totalEasingCheckSuccess_18(int32_t value)
	{
		___totalEasingCheckSuccess_18 = value;
	}

	inline static int32_t get_offset_of_value2UpdateCalled_19() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___value2UpdateCalled_19)); }
	inline bool get_value2UpdateCalled_19() const { return ___value2UpdateCalled_19; }
	inline bool* get_address_of_value2UpdateCalled_19() { return &___value2UpdateCalled_19; }
	inline void set_value2UpdateCalled_19(bool value)
	{
		___value2UpdateCalled_19 = value;
	}

	inline static int32_t get_offset_of_U3CU3E9__21_20() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396, ___U3CU3E9__21_20)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__21_20() const { return ___U3CU3E9__21_20; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__21_20() { return &___U3CU3E9__21_20; }
	inline void set_U3CU3E9__21_20(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__21_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__21_20), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0
struct  U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97  : public RuntimeObject
{
public:
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>4__this
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * ___U3CU3E4__this_0;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::pauseCount
	int32_t ___pauseCount_1;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeRound
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cubeRound_2;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::onStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___onStartPos_3;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::onStartPosSpline
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___onStartPosSpline_4;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeSpline
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cubeSpline_5;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeSeq
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cubeSeq_6;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeBounds
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cubeBounds_7;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::didPassBounds
	bool ___didPassBounds_8;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::failPoint
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___failPoint_9;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::setOnStartNum
	int32_t ___setOnStartNum_10;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::setPosOnUpdate
	bool ___setPosOnUpdate_11;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::setPosNum
	int32_t ___setPosNum_12;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::hasGroupTweensCheckStarted
	bool ___hasGroupTweensCheckStarted_13;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::previousXlt4
	float ___previousXlt4_14;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::onUpdateWasCalled
	bool ___onUpdateWasCalled_15;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::start
	float ___start_16;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::expectedTime
	float ___expectedTime_17;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::didGetCorrectOnUpdate
	bool ___didGetCorrectOnUpdate_18;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__13
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__13_19;
	// System.Action`1<UnityEngine.Vector3> DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__14
	Action_1_t08BAF0B9143320EA6FA33CF25A59B6F1641EA5B6 * ___U3CU3E9__14_20;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__16
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__16_21;
	// System.Action`1<System.Object> DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__15
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___U3CU3E9__15_22;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___U3CU3E4__this_0)); }
	inline TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_pauseCount_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___pauseCount_1)); }
	inline int32_t get_pauseCount_1() const { return ___pauseCount_1; }
	inline int32_t* get_address_of_pauseCount_1() { return &___pauseCount_1; }
	inline void set_pauseCount_1(int32_t value)
	{
		___pauseCount_1 = value;
	}

	inline static int32_t get_offset_of_cubeRound_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___cubeRound_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cubeRound_2() const { return ___cubeRound_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cubeRound_2() { return &___cubeRound_2; }
	inline void set_cubeRound_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cubeRound_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeRound_2), (void*)value);
	}

	inline static int32_t get_offset_of_onStartPos_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___onStartPos_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_onStartPos_3() const { return ___onStartPos_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_onStartPos_3() { return &___onStartPos_3; }
	inline void set_onStartPos_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___onStartPos_3 = value;
	}

	inline static int32_t get_offset_of_onStartPosSpline_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___onStartPosSpline_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_onStartPosSpline_4() const { return ___onStartPosSpline_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_onStartPosSpline_4() { return &___onStartPosSpline_4; }
	inline void set_onStartPosSpline_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___onStartPosSpline_4 = value;
	}

	inline static int32_t get_offset_of_cubeSpline_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___cubeSpline_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cubeSpline_5() const { return ___cubeSpline_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cubeSpline_5() { return &___cubeSpline_5; }
	inline void set_cubeSpline_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cubeSpline_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeSpline_5), (void*)value);
	}

	inline static int32_t get_offset_of_cubeSeq_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___cubeSeq_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cubeSeq_6() const { return ___cubeSeq_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cubeSeq_6() { return &___cubeSeq_6; }
	inline void set_cubeSeq_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cubeSeq_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeSeq_6), (void*)value);
	}

	inline static int32_t get_offset_of_cubeBounds_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___cubeBounds_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cubeBounds_7() const { return ___cubeBounds_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cubeBounds_7() { return &___cubeBounds_7; }
	inline void set_cubeBounds_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cubeBounds_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeBounds_7), (void*)value);
	}

	inline static int32_t get_offset_of_didPassBounds_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___didPassBounds_8)); }
	inline bool get_didPassBounds_8() const { return ___didPassBounds_8; }
	inline bool* get_address_of_didPassBounds_8() { return &___didPassBounds_8; }
	inline void set_didPassBounds_8(bool value)
	{
		___didPassBounds_8 = value;
	}

	inline static int32_t get_offset_of_failPoint_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___failPoint_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_failPoint_9() const { return ___failPoint_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_failPoint_9() { return &___failPoint_9; }
	inline void set_failPoint_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___failPoint_9 = value;
	}

	inline static int32_t get_offset_of_setOnStartNum_10() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___setOnStartNum_10)); }
	inline int32_t get_setOnStartNum_10() const { return ___setOnStartNum_10; }
	inline int32_t* get_address_of_setOnStartNum_10() { return &___setOnStartNum_10; }
	inline void set_setOnStartNum_10(int32_t value)
	{
		___setOnStartNum_10 = value;
	}

	inline static int32_t get_offset_of_setPosOnUpdate_11() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___setPosOnUpdate_11)); }
	inline bool get_setPosOnUpdate_11() const { return ___setPosOnUpdate_11; }
	inline bool* get_address_of_setPosOnUpdate_11() { return &___setPosOnUpdate_11; }
	inline void set_setPosOnUpdate_11(bool value)
	{
		___setPosOnUpdate_11 = value;
	}

	inline static int32_t get_offset_of_setPosNum_12() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___setPosNum_12)); }
	inline int32_t get_setPosNum_12() const { return ___setPosNum_12; }
	inline int32_t* get_address_of_setPosNum_12() { return &___setPosNum_12; }
	inline void set_setPosNum_12(int32_t value)
	{
		___setPosNum_12 = value;
	}

	inline static int32_t get_offset_of_hasGroupTweensCheckStarted_13() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___hasGroupTweensCheckStarted_13)); }
	inline bool get_hasGroupTweensCheckStarted_13() const { return ___hasGroupTweensCheckStarted_13; }
	inline bool* get_address_of_hasGroupTweensCheckStarted_13() { return &___hasGroupTweensCheckStarted_13; }
	inline void set_hasGroupTweensCheckStarted_13(bool value)
	{
		___hasGroupTweensCheckStarted_13 = value;
	}

	inline static int32_t get_offset_of_previousXlt4_14() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___previousXlt4_14)); }
	inline float get_previousXlt4_14() const { return ___previousXlt4_14; }
	inline float* get_address_of_previousXlt4_14() { return &___previousXlt4_14; }
	inline void set_previousXlt4_14(float value)
	{
		___previousXlt4_14 = value;
	}

	inline static int32_t get_offset_of_onUpdateWasCalled_15() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___onUpdateWasCalled_15)); }
	inline bool get_onUpdateWasCalled_15() const { return ___onUpdateWasCalled_15; }
	inline bool* get_address_of_onUpdateWasCalled_15() { return &___onUpdateWasCalled_15; }
	inline void set_onUpdateWasCalled_15(bool value)
	{
		___onUpdateWasCalled_15 = value;
	}

	inline static int32_t get_offset_of_start_16() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___start_16)); }
	inline float get_start_16() const { return ___start_16; }
	inline float* get_address_of_start_16() { return &___start_16; }
	inline void set_start_16(float value)
	{
		___start_16 = value;
	}

	inline static int32_t get_offset_of_expectedTime_17() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___expectedTime_17)); }
	inline float get_expectedTime_17() const { return ___expectedTime_17; }
	inline float* get_address_of_expectedTime_17() { return &___expectedTime_17; }
	inline void set_expectedTime_17(float value)
	{
		___expectedTime_17 = value;
	}

	inline static int32_t get_offset_of_didGetCorrectOnUpdate_18() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___didGetCorrectOnUpdate_18)); }
	inline bool get_didGetCorrectOnUpdate_18() const { return ___didGetCorrectOnUpdate_18; }
	inline bool* get_address_of_didGetCorrectOnUpdate_18() { return &___didGetCorrectOnUpdate_18; }
	inline void set_didGetCorrectOnUpdate_18(bool value)
	{
		___didGetCorrectOnUpdate_18 = value;
	}

	inline static int32_t get_offset_of_U3CU3E9__13_19() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___U3CU3E9__13_19)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__13_19() const { return ___U3CU3E9__13_19; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__13_19() { return &___U3CU3E9__13_19; }
	inline void set_U3CU3E9__13_19(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__13_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__13_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_20() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___U3CU3E9__14_20)); }
	inline Action_1_t08BAF0B9143320EA6FA33CF25A59B6F1641EA5B6 * get_U3CU3E9__14_20() const { return ___U3CU3E9__14_20; }
	inline Action_1_t08BAF0B9143320EA6FA33CF25A59B6F1641EA5B6 ** get_address_of_U3CU3E9__14_20() { return &___U3CU3E9__14_20; }
	inline void set_U3CU3E9__14_20(Action_1_t08BAF0B9143320EA6FA33CF25A59B6F1641EA5B6 * value)
	{
		___U3CU3E9__14_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__14_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_21() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___U3CU3E9__16_21)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__16_21() const { return ___U3CU3E9__16_21; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__16_21() { return &___U3CU3E9__16_21; }
	inline void set_U3CU3E9__16_21(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__16_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__16_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_22() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97, ___U3CU3E9__15_22)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get_U3CU3E9__15_22() const { return ___U3CU3E9__15_22; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of_U3CU3E9__15_22() { return &___U3CU3E9__15_22; }
	inline void set_U3CU3E9__15_22(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		___U3CU3E9__15_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__15_22), (void*)value);
	}
};


// NLayer.MpegChannelMode
struct  MpegChannelMode_t74BC5F302EBB6E8C26940521EC4EB53607A61D66 
{
public:
	// System.Int32 NLayer.MpegChannelMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MpegChannelMode_t74BC5F302EBB6E8C26940521EC4EB53607A61D66, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NLayer.MpegLayer
struct  MpegLayer_t6B18823B84C63A924C8DBE88AC67E59BCFDD7460 
{
public:
	// System.Int32 NLayer.MpegLayer::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MpegLayer_t6B18823B84C63A924C8DBE88AC67E59BCFDD7460, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NLayer.MpegVersion
struct  MpegVersion_tF02D5976231BDBAE02A806210351177E5619911F 
{
public:
	// System.Int32 NLayer.MpegVersion::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MpegVersion_tF02D5976231BDBAE02A806210351177E5619911F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NLayer.StereoMode
struct  StereoMode_t31B767D6A76DB640B68A3D2F0DE974C479E4C701 
{
public:
	// System.Int32 NLayer.StereoMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StereoMode_t31B767D6A76DB640B68A3D2F0DE974C479E4C701, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NatSuite.Recorders.Internal.NativeRecorder
struct  NativeRecorder_tB0869D7A0DFF8328DD1BEEC8E664CE1C57DCB624  : public RuntimeObject
{
public:
	// System.IntPtr NatSuite.Recorders.Internal.NativeRecorder::recorder
	intptr_t ___recorder_0;
	// System.Threading.Tasks.TaskCompletionSource`1<System.String> NatSuite.Recorders.Internal.NativeRecorder::recordingTask
	TaskCompletionSource_1_t740D6B4559A97E4E740413E4EB4F1F90A84CB317 * ___recordingTask_1;

public:
	inline static int32_t get_offset_of_recorder_0() { return static_cast<int32_t>(offsetof(NativeRecorder_tB0869D7A0DFF8328DD1BEEC8E664CE1C57DCB624, ___recorder_0)); }
	inline intptr_t get_recorder_0() const { return ___recorder_0; }
	inline intptr_t* get_address_of_recorder_0() { return &___recorder_0; }
	inline void set_recorder_0(intptr_t value)
	{
		___recorder_0 = value;
	}

	inline static int32_t get_offset_of_recordingTask_1() { return static_cast<int32_t>(offsetof(NativeRecorder_tB0869D7A0DFF8328DD1BEEC8E664CE1C57DCB624, ___recordingTask_1)); }
	inline TaskCompletionSource_1_t740D6B4559A97E4E740413E4EB4F1F90A84CB317 * get_recordingTask_1() const { return ___recordingTask_1; }
	inline TaskCompletionSource_1_t740D6B4559A97E4E740413E4EB4F1F90A84CB317 ** get_address_of_recordingTask_1() { return &___recordingTask_1; }
	inline void set_recordingTask_1(TaskCompletionSource_1_t740D6B4559A97E4E740413E4EB4F1F90A84CB317 * value)
	{
		___recordingTask_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___recordingTask_1), (void*)value);
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct  AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF 
{
public:
	// System.Threading.SynchronizationContext System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_synchronizationContext
	SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * ___m_synchronizationContext_0;
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_coreState
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  ___m_coreState_1;
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_task
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_synchronizationContext_0() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF, ___m_synchronizationContext_0)); }
	inline SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * get_m_synchronizationContext_0() const { return ___m_synchronizationContext_0; }
	inline SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 ** get_address_of_m_synchronizationContext_0() { return &___m_synchronizationContext_0; }
	inline void set_m_synchronizationContext_0(SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * value)
	{
		___m_synchronizationContext_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_synchronizationContext_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  value)
	{
		___m_coreState_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF, ___m_task_2)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_m_task_2() const { return ___m_task_2; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF_marshaled_pinvoke
{
	SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_pinvoke ___m_coreState_1;
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_2;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF_marshaled_com
{
	SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_com ___m_coreState_1;
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_2;
};

// UnityEngine.Experimental.Rendering.GraphicsFormat
struct  GraphicsFormat_t512915BBE299AE115F4DB0B96DF1DA2E72ECA181 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.GraphicsFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GraphicsFormat_t512915BBE299AE115F4DB0B96DF1DA2E72ECA181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.RenderTextureCreationFlags
struct  RenderTextureCreationFlags_tF63E06301E4BB4746F7E07759B359872BD4BFB1E 
{
public:
	// System.Int32 UnityEngine.RenderTextureCreationFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureCreationFlags_tF63E06301E4BB4746F7E07759B359872BD4BFB1E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RenderTextureMemoryless
struct  RenderTextureMemoryless_t19E37ADD57C1F00D67146A2BB4521D06F370D2E9 
{
public:
	// System.Int32 UnityEngine.RenderTextureMemoryless::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureMemoryless_t19E37ADD57C1F00D67146A2BB4521D06F370D2E9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Rendering.ShadowSamplingMode
struct  ShadowSamplingMode_t585A9BDECAC505FF19FF785F55CDD403A2E5DA73 
{
public:
	// System.Int32 UnityEngine.Rendering.ShadowSamplingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShadowSamplingMode_t585A9BDECAC505FF19FF785F55CDD403A2E5DA73, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Rendering.TextureDimension
struct  TextureDimension_t90D0E4110D3F4D062F3E8C0F69809BFBBDF8E19C 
{
public:
	// System.Int32 UnityEngine.Rendering.TextureDimension::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureDimension_t90D0E4110D3F4D062F3E8C0F69809BFBBDF8E19C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.VRTextureUsage
struct  VRTextureUsage_t2D7C2397ABF03DD28086B969100F7D91DDD978A0 
{
public:
	// System.Int32 UnityEngine.VRTextureUsage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VRTextureUsage_t2D7C2397ABF03DD28086B969100F7D91DDD978A0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NLayer.Decoder.LayerDecoderBase
struct  LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Single[]> NLayer.Decoder.LayerDecoderBase::_synBuf
	List_1_t389DEBD2C92F3A31DC21A2AE3CBEED8DDA76FF5C * ____synBuf_4;
	// System.Collections.Generic.List`1<System.Int32> NLayer.Decoder.LayerDecoderBase::_bufOffset
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____bufOffset_5;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::_eq
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____eq_6;
	// NLayer.StereoMode NLayer.Decoder.LayerDecoderBase::<StereoMode>k__BackingField
	int32_t ___U3CStereoModeU3Ek__BackingField_7;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::ippuv
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___ippuv_8;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::ei32
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___ei32_9;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::eo32
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___eo32_10;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::oi32
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___oi32_11;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::oo32
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___oo32_12;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::ei16
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___ei16_13;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::eo16
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___eo16_14;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::oi16
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___oi16_15;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::oo16
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___oo16_16;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::ei8
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___ei8_17;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::tmp8
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___tmp8_18;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::oi8
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___oi8_19;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::oo8
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___oo8_20;

public:
	inline static int32_t get_offset_of__synBuf_4() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ____synBuf_4)); }
	inline List_1_t389DEBD2C92F3A31DC21A2AE3CBEED8DDA76FF5C * get__synBuf_4() const { return ____synBuf_4; }
	inline List_1_t389DEBD2C92F3A31DC21A2AE3CBEED8DDA76FF5C ** get_address_of__synBuf_4() { return &____synBuf_4; }
	inline void set__synBuf_4(List_1_t389DEBD2C92F3A31DC21A2AE3CBEED8DDA76FF5C * value)
	{
		____synBuf_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____synBuf_4), (void*)value);
	}

	inline static int32_t get_offset_of__bufOffset_5() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ____bufOffset_5)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__bufOffset_5() const { return ____bufOffset_5; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__bufOffset_5() { return &____bufOffset_5; }
	inline void set__bufOffset_5(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____bufOffset_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bufOffset_5), (void*)value);
	}

	inline static int32_t get_offset_of__eq_6() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ____eq_6)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__eq_6() const { return ____eq_6; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__eq_6() { return &____eq_6; }
	inline void set__eq_6(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____eq_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____eq_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CStereoModeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ___U3CStereoModeU3Ek__BackingField_7)); }
	inline int32_t get_U3CStereoModeU3Ek__BackingField_7() const { return ___U3CStereoModeU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CStereoModeU3Ek__BackingField_7() { return &___U3CStereoModeU3Ek__BackingField_7; }
	inline void set_U3CStereoModeU3Ek__BackingField_7(int32_t value)
	{
		___U3CStereoModeU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_ippuv_8() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ___ippuv_8)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_ippuv_8() const { return ___ippuv_8; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_ippuv_8() { return &___ippuv_8; }
	inline void set_ippuv_8(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___ippuv_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ippuv_8), (void*)value);
	}

	inline static int32_t get_offset_of_ei32_9() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ___ei32_9)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_ei32_9() const { return ___ei32_9; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_ei32_9() { return &___ei32_9; }
	inline void set_ei32_9(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___ei32_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ei32_9), (void*)value);
	}

	inline static int32_t get_offset_of_eo32_10() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ___eo32_10)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_eo32_10() const { return ___eo32_10; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_eo32_10() { return &___eo32_10; }
	inline void set_eo32_10(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___eo32_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eo32_10), (void*)value);
	}

	inline static int32_t get_offset_of_oi32_11() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ___oi32_11)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_oi32_11() const { return ___oi32_11; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_oi32_11() { return &___oi32_11; }
	inline void set_oi32_11(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___oi32_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___oi32_11), (void*)value);
	}

	inline static int32_t get_offset_of_oo32_12() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ___oo32_12)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_oo32_12() const { return ___oo32_12; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_oo32_12() { return &___oo32_12; }
	inline void set_oo32_12(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___oo32_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___oo32_12), (void*)value);
	}

	inline static int32_t get_offset_of_ei16_13() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ___ei16_13)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_ei16_13() const { return ___ei16_13; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_ei16_13() { return &___ei16_13; }
	inline void set_ei16_13(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___ei16_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ei16_13), (void*)value);
	}

	inline static int32_t get_offset_of_eo16_14() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ___eo16_14)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_eo16_14() const { return ___eo16_14; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_eo16_14() { return &___eo16_14; }
	inline void set_eo16_14(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___eo16_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eo16_14), (void*)value);
	}

	inline static int32_t get_offset_of_oi16_15() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ___oi16_15)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_oi16_15() const { return ___oi16_15; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_oi16_15() { return &___oi16_15; }
	inline void set_oi16_15(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___oi16_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___oi16_15), (void*)value);
	}

	inline static int32_t get_offset_of_oo16_16() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ___oo16_16)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_oo16_16() const { return ___oo16_16; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_oo16_16() { return &___oo16_16; }
	inline void set_oo16_16(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___oo16_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___oo16_16), (void*)value);
	}

	inline static int32_t get_offset_of_ei8_17() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ___ei8_17)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_ei8_17() const { return ___ei8_17; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_ei8_17() { return &___ei8_17; }
	inline void set_ei8_17(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___ei8_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ei8_17), (void*)value);
	}

	inline static int32_t get_offset_of_tmp8_18() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ___tmp8_18)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_tmp8_18() const { return ___tmp8_18; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_tmp8_18() { return &___tmp8_18; }
	inline void set_tmp8_18(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___tmp8_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tmp8_18), (void*)value);
	}

	inline static int32_t get_offset_of_oi8_19() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ___oi8_19)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_oi8_19() const { return ___oi8_19; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_oi8_19() { return &___oi8_19; }
	inline void set_oi8_19(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___oi8_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___oi8_19), (void*)value);
	}

	inline static int32_t get_offset_of_oo8_20() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B, ___oo8_20)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_oo8_20() const { return ___oo8_20; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_oo8_20() { return &___oo8_20; }
	inline void set_oo8_20(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___oo8_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___oo8_20), (void*)value);
	}
};

struct LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B_StaticFields
{
public:
	// System.Single[] NLayer.Decoder.LayerDecoderBase::DEWINDOW_TABLE
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___DEWINDOW_TABLE_2;
	// System.Single[] NLayer.Decoder.LayerDecoderBase::SYNTH_COS64_TABLE
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___SYNTH_COS64_TABLE_3;

public:
	inline static int32_t get_offset_of_DEWINDOW_TABLE_2() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B_StaticFields, ___DEWINDOW_TABLE_2)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_DEWINDOW_TABLE_2() const { return ___DEWINDOW_TABLE_2; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_DEWINDOW_TABLE_2() { return &___DEWINDOW_TABLE_2; }
	inline void set_DEWINDOW_TABLE_2(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___DEWINDOW_TABLE_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DEWINDOW_TABLE_2), (void*)value);
	}

	inline static int32_t get_offset_of_SYNTH_COS64_TABLE_3() { return static_cast<int32_t>(offsetof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B_StaticFields, ___SYNTH_COS64_TABLE_3)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_SYNTH_COS64_TABLE_3() const { return ___SYNTH_COS64_TABLE_3; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_SYNTH_COS64_TABLE_3() { return &___SYNTH_COS64_TABLE_3; }
	inline void set_SYNTH_COS64_TABLE_3(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___SYNTH_COS64_TABLE_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SYNTH_COS64_TABLE_3), (void*)value);
	}
};


// NLayer.MpegFrameDecoder
struct  MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2  : public RuntimeObject
{
public:
	// NLayer.Decoder.LayerIDecoder NLayer.MpegFrameDecoder::_layerIDecoder
	LayerIDecoder_t29E88F178B1D31CBFCDBF7BF51304FED69AF8D55 * ____layerIDecoder_0;
	// NLayer.Decoder.LayerIIDecoder NLayer.MpegFrameDecoder::_layerIIDecoder
	LayerIIDecoder_t06D951DC8F7021B2CF4D70FCE55297ECFE87ABFE * ____layerIIDecoder_1;
	// NLayer.Decoder.LayerIIIDecoder NLayer.MpegFrameDecoder::_layerIIIDecoder
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1 * ____layerIIIDecoder_2;
	// System.Single[] NLayer.MpegFrameDecoder::_eqFactors
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____eqFactors_3;
	// System.Single[] NLayer.MpegFrameDecoder::_ch0
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____ch0_4;
	// System.Single[] NLayer.MpegFrameDecoder::_ch1
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____ch1_5;
	// NLayer.StereoMode NLayer.MpegFrameDecoder::<StereoMode>k__BackingField
	int32_t ___U3CStereoModeU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of__layerIDecoder_0() { return static_cast<int32_t>(offsetof(MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2, ____layerIDecoder_0)); }
	inline LayerIDecoder_t29E88F178B1D31CBFCDBF7BF51304FED69AF8D55 * get__layerIDecoder_0() const { return ____layerIDecoder_0; }
	inline LayerIDecoder_t29E88F178B1D31CBFCDBF7BF51304FED69AF8D55 ** get_address_of__layerIDecoder_0() { return &____layerIDecoder_0; }
	inline void set__layerIDecoder_0(LayerIDecoder_t29E88F178B1D31CBFCDBF7BF51304FED69AF8D55 * value)
	{
		____layerIDecoder_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____layerIDecoder_0), (void*)value);
	}

	inline static int32_t get_offset_of__layerIIDecoder_1() { return static_cast<int32_t>(offsetof(MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2, ____layerIIDecoder_1)); }
	inline LayerIIDecoder_t06D951DC8F7021B2CF4D70FCE55297ECFE87ABFE * get__layerIIDecoder_1() const { return ____layerIIDecoder_1; }
	inline LayerIIDecoder_t06D951DC8F7021B2CF4D70FCE55297ECFE87ABFE ** get_address_of__layerIIDecoder_1() { return &____layerIIDecoder_1; }
	inline void set__layerIIDecoder_1(LayerIIDecoder_t06D951DC8F7021B2CF4D70FCE55297ECFE87ABFE * value)
	{
		____layerIIDecoder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____layerIIDecoder_1), (void*)value);
	}

	inline static int32_t get_offset_of__layerIIIDecoder_2() { return static_cast<int32_t>(offsetof(MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2, ____layerIIIDecoder_2)); }
	inline LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1 * get__layerIIIDecoder_2() const { return ____layerIIIDecoder_2; }
	inline LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1 ** get_address_of__layerIIIDecoder_2() { return &____layerIIIDecoder_2; }
	inline void set__layerIIIDecoder_2(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1 * value)
	{
		____layerIIIDecoder_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____layerIIIDecoder_2), (void*)value);
	}

	inline static int32_t get_offset_of__eqFactors_3() { return static_cast<int32_t>(offsetof(MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2, ____eqFactors_3)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__eqFactors_3() const { return ____eqFactors_3; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__eqFactors_3() { return &____eqFactors_3; }
	inline void set__eqFactors_3(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____eqFactors_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____eqFactors_3), (void*)value);
	}

	inline static int32_t get_offset_of__ch0_4() { return static_cast<int32_t>(offsetof(MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2, ____ch0_4)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__ch0_4() const { return ____ch0_4; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__ch0_4() { return &____ch0_4; }
	inline void set__ch0_4(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____ch0_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____ch0_4), (void*)value);
	}

	inline static int32_t get_offset_of__ch1_5() { return static_cast<int32_t>(offsetof(MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2, ____ch1_5)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__ch1_5() const { return ____ch1_5; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__ch1_5() { return &____ch1_5; }
	inline void set__ch1_5(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____ch1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____ch1_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CStereoModeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2, ___U3CStereoModeU3Ek__BackingField_6)); }
	inline int32_t get_U3CStereoModeU3Ek__BackingField_6() const { return ___U3CStereoModeU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CStereoModeU3Ek__BackingField_6() { return &___U3CStereoModeU3Ek__BackingField_6; }
	inline void set_U3CStereoModeU3Ek__BackingField_6(int32_t value)
	{
		___U3CStereoModeU3Ek__BackingField_6 = value;
	}
};


// NatSuite.Examples.Giffy/<StopRecording>d__6
struct  U3CStopRecordingU3Ed__6_t6831C9609F3B520123F95F9DC34C80ADEDC45F16 
{
public:
	// System.Int32 NatSuite.Examples.Giffy/<StopRecording>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncVoidMethodBuilder NatSuite.Examples.Giffy/<StopRecording>d__6::<>t__builder
	AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  ___U3CU3Et__builder_1;
	// NatSuite.Examples.Giffy NatSuite.Examples.Giffy/<StopRecording>d__6::<>4__this
	Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4 * ___U3CU3E4__this_2;
	// System.Runtime.CompilerServices.TaskAwaiter`1<System.String> NatSuite.Examples.Giffy/<StopRecording>d__6::<>u__1
	TaskAwaiter_1_t0139085A70D09D28A04F5187195DC0184E53D214  ___U3CU3Eu__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStopRecordingU3Ed__6_t6831C9609F3B520123F95F9DC34C80ADEDC45F16, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CStopRecordingU3Ed__6_t6831C9609F3B520123F95F9DC34C80ADEDC45F16, ___U3CU3Et__builder_1)); }
	inline AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  value)
	{
		___U3CU3Et__builder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Et__builder_1))->___m_synchronizationContext_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Et__builder_1))->___m_task_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStopRecordingU3Ed__6_t6831C9609F3B520123F95F9DC34C80ADEDC45F16, ___U3CU3E4__this_2)); }
	inline Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_3() { return static_cast<int32_t>(offsetof(U3CStopRecordingU3Ed__6_t6831C9609F3B520123F95F9DC34C80ADEDC45F16, ___U3CU3Eu__1_3)); }
	inline TaskAwaiter_1_t0139085A70D09D28A04F5187195DC0184E53D214  get_U3CU3Eu__1_3() const { return ___U3CU3Eu__1_3; }
	inline TaskAwaiter_1_t0139085A70D09D28A04F5187195DC0184E53D214 * get_address_of_U3CU3Eu__1_3() { return &___U3CU3Eu__1_3; }
	inline void set_U3CU3Eu__1_3(TaskAwaiter_1_t0139085A70D09D28A04F5187195DC0184E53D214  value)
	{
		___U3CU3Eu__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Eu__1_3))->___m_task_0), (void*)NULL);
	}
};


// NatSuite.Examples.ReplayCam/<FinishRecording>d__14
struct  U3CFinishRecordingU3Ed__14_t356A404CC498364A41990D36CCE2DF7610E7B4ED 
{
public:
	// System.Int32 NatSuite.Examples.ReplayCam/<FinishRecording>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncVoidMethodBuilder NatSuite.Examples.ReplayCam/<FinishRecording>d__14::<>t__builder
	AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  ___U3CU3Et__builder_1;
	// NatSuite.Examples.ReplayCam NatSuite.Examples.ReplayCam/<FinishRecording>d__14::<>4__this
	ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE * ___U3CU3E4__this_2;
	// System.Runtime.CompilerServices.TaskAwaiter`1<System.String> NatSuite.Examples.ReplayCam/<FinishRecording>d__14::<>u__1
	TaskAwaiter_1_t0139085A70D09D28A04F5187195DC0184E53D214  ___U3CU3Eu__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFinishRecordingU3Ed__14_t356A404CC498364A41990D36CCE2DF7610E7B4ED, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CFinishRecordingU3Ed__14_t356A404CC498364A41990D36CCE2DF7610E7B4ED, ___U3CU3Et__builder_1)); }
	inline AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  value)
	{
		___U3CU3Et__builder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Et__builder_1))->___m_synchronizationContext_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Et__builder_1))->___m_task_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFinishRecordingU3Ed__14_t356A404CC498364A41990D36CCE2DF7610E7B4ED, ___U3CU3E4__this_2)); }
	inline ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_3() { return static_cast<int32_t>(offsetof(U3CFinishRecordingU3Ed__14_t356A404CC498364A41990D36CCE2DF7610E7B4ED, ___U3CU3Eu__1_3)); }
	inline TaskAwaiter_1_t0139085A70D09D28A04F5187195DC0184E53D214  get_U3CU3Eu__1_3() const { return ___U3CU3Eu__1_3; }
	inline TaskAwaiter_1_t0139085A70D09D28A04F5187195DC0184E53D214 * get_address_of_U3CU3Eu__1_3() { return &___U3CU3Eu__1_3; }
	inline void set_U3CU3Eu__1_3(TaskAwaiter_1_t0139085A70D09D28A04F5187195DC0184E53D214  value)
	{
		___U3CU3Eu__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Eu__1_3))->___m_task_0), (void*)NULL);
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.RenderTextureDescriptor
struct  RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E 
{
public:
	// System.Int32 UnityEngine.RenderTextureDescriptor::<width>k__BackingField
	int32_t ___U3CwidthU3Ek__BackingField_0;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<height>k__BackingField
	int32_t ___U3CheightU3Ek__BackingField_1;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<msaaSamples>k__BackingField
	int32_t ___U3CmsaaSamplesU3Ek__BackingField_2;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<volumeDepth>k__BackingField
	int32_t ___U3CvolumeDepthU3Ek__BackingField_3;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<mipCount>k__BackingField
	int32_t ___U3CmipCountU3Ek__BackingField_4;
	// UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTextureDescriptor::_graphicsFormat
	int32_t ____graphicsFormat_5;
	// UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTextureDescriptor::<stencilFormat>k__BackingField
	int32_t ___U3CstencilFormatU3Ek__BackingField_6;
	// System.Int32 UnityEngine.RenderTextureDescriptor::_depthBufferBits
	int32_t ____depthBufferBits_7;
	// UnityEngine.Rendering.TextureDimension UnityEngine.RenderTextureDescriptor::<dimension>k__BackingField
	int32_t ___U3CdimensionU3Ek__BackingField_9;
	// UnityEngine.Rendering.ShadowSamplingMode UnityEngine.RenderTextureDescriptor::<shadowSamplingMode>k__BackingField
	int32_t ___U3CshadowSamplingModeU3Ek__BackingField_10;
	// UnityEngine.VRTextureUsage UnityEngine.RenderTextureDescriptor::<vrUsage>k__BackingField
	int32_t ___U3CvrUsageU3Ek__BackingField_11;
	// UnityEngine.RenderTextureCreationFlags UnityEngine.RenderTextureDescriptor::_flags
	int32_t ____flags_12;
	// UnityEngine.RenderTextureMemoryless UnityEngine.RenderTextureDescriptor::<memoryless>k__BackingField
	int32_t ___U3CmemorylessU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CwidthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CwidthU3Ek__BackingField_0)); }
	inline int32_t get_U3CwidthU3Ek__BackingField_0() const { return ___U3CwidthU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CwidthU3Ek__BackingField_0() { return &___U3CwidthU3Ek__BackingField_0; }
	inline void set_U3CwidthU3Ek__BackingField_0(int32_t value)
	{
		___U3CwidthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CheightU3Ek__BackingField_1)); }
	inline int32_t get_U3CheightU3Ek__BackingField_1() const { return ___U3CheightU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CheightU3Ek__BackingField_1() { return &___U3CheightU3Ek__BackingField_1; }
	inline void set_U3CheightU3Ek__BackingField_1(int32_t value)
	{
		___U3CheightU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CmsaaSamplesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CmsaaSamplesU3Ek__BackingField_2)); }
	inline int32_t get_U3CmsaaSamplesU3Ek__BackingField_2() const { return ___U3CmsaaSamplesU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CmsaaSamplesU3Ek__BackingField_2() { return &___U3CmsaaSamplesU3Ek__BackingField_2; }
	inline void set_U3CmsaaSamplesU3Ek__BackingField_2(int32_t value)
	{
		___U3CmsaaSamplesU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CvolumeDepthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CvolumeDepthU3Ek__BackingField_3)); }
	inline int32_t get_U3CvolumeDepthU3Ek__BackingField_3() const { return ___U3CvolumeDepthU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CvolumeDepthU3Ek__BackingField_3() { return &___U3CvolumeDepthU3Ek__BackingField_3; }
	inline void set_U3CvolumeDepthU3Ek__BackingField_3(int32_t value)
	{
		___U3CvolumeDepthU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CmipCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CmipCountU3Ek__BackingField_4)); }
	inline int32_t get_U3CmipCountU3Ek__BackingField_4() const { return ___U3CmipCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CmipCountU3Ek__BackingField_4() { return &___U3CmipCountU3Ek__BackingField_4; }
	inline void set_U3CmipCountU3Ek__BackingField_4(int32_t value)
	{
		___U3CmipCountU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of__graphicsFormat_5() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ____graphicsFormat_5)); }
	inline int32_t get__graphicsFormat_5() const { return ____graphicsFormat_5; }
	inline int32_t* get_address_of__graphicsFormat_5() { return &____graphicsFormat_5; }
	inline void set__graphicsFormat_5(int32_t value)
	{
		____graphicsFormat_5 = value;
	}

	inline static int32_t get_offset_of_U3CstencilFormatU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CstencilFormatU3Ek__BackingField_6)); }
	inline int32_t get_U3CstencilFormatU3Ek__BackingField_6() const { return ___U3CstencilFormatU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CstencilFormatU3Ek__BackingField_6() { return &___U3CstencilFormatU3Ek__BackingField_6; }
	inline void set_U3CstencilFormatU3Ek__BackingField_6(int32_t value)
	{
		___U3CstencilFormatU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__depthBufferBits_7() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ____depthBufferBits_7)); }
	inline int32_t get__depthBufferBits_7() const { return ____depthBufferBits_7; }
	inline int32_t* get_address_of__depthBufferBits_7() { return &____depthBufferBits_7; }
	inline void set__depthBufferBits_7(int32_t value)
	{
		____depthBufferBits_7 = value;
	}

	inline static int32_t get_offset_of_U3CdimensionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CdimensionU3Ek__BackingField_9)); }
	inline int32_t get_U3CdimensionU3Ek__BackingField_9() const { return ___U3CdimensionU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CdimensionU3Ek__BackingField_9() { return &___U3CdimensionU3Ek__BackingField_9; }
	inline void set_U3CdimensionU3Ek__BackingField_9(int32_t value)
	{
		___U3CdimensionU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CshadowSamplingModeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CshadowSamplingModeU3Ek__BackingField_10)); }
	inline int32_t get_U3CshadowSamplingModeU3Ek__BackingField_10() const { return ___U3CshadowSamplingModeU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CshadowSamplingModeU3Ek__BackingField_10() { return &___U3CshadowSamplingModeU3Ek__BackingField_10; }
	inline void set_U3CshadowSamplingModeU3Ek__BackingField_10(int32_t value)
	{
		___U3CshadowSamplingModeU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CvrUsageU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CvrUsageU3Ek__BackingField_11)); }
	inline int32_t get_U3CvrUsageU3Ek__BackingField_11() const { return ___U3CvrUsageU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CvrUsageU3Ek__BackingField_11() { return &___U3CvrUsageU3Ek__BackingField_11; }
	inline void set_U3CvrUsageU3Ek__BackingField_11(int32_t value)
	{
		___U3CvrUsageU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of__flags_12() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ____flags_12)); }
	inline int32_t get__flags_12() const { return ____flags_12; }
	inline int32_t* get_address_of__flags_12() { return &____flags_12; }
	inline void set__flags_12(int32_t value)
	{
		____flags_12 = value;
	}

	inline static int32_t get_offset_of_U3CmemorylessU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CmemorylessU3Ek__BackingField_13)); }
	inline int32_t get_U3CmemorylessU3Ek__BackingField_13() const { return ___U3CmemorylessU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CmemorylessU3Ek__BackingField_13() { return &___U3CmemorylessU3Ek__BackingField_13; }
	inline void set_U3CmemorylessU3Ek__BackingField_13(int32_t value)
	{
		___U3CmemorylessU3Ek__BackingField_13 = value;
	}
};

struct RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E_StaticFields
{
public:
	// System.Int32[] UnityEngine.RenderTextureDescriptor::depthFormatBits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___depthFormatBits_8;

public:
	inline static int32_t get_offset_of_depthFormatBits_8() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E_StaticFields, ___depthFormatBits_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_depthFormatBits_8() const { return ___depthFormatBits_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_depthFormatBits_8() { return &___depthFormatBits_8; }
	inline void set_depthFormatBits_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___depthFormatBits_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___depthFormatBits_8), (void*)value);
	}
};


// NLayer.Decoder.LayerIIDecoderBase
struct  LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF  : public LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B
{
public:
	// System.Int32 NLayer.Decoder.LayerIIDecoderBase::_channels
	int32_t ____channels_27;
	// System.Int32 NLayer.Decoder.LayerIIDecoderBase::_jsbound
	int32_t ____jsbound_28;
	// System.Int32 NLayer.Decoder.LayerIIDecoderBase::_granuleCount
	int32_t ____granuleCount_29;
	// System.Int32[][] NLayer.Decoder.LayerIIDecoderBase::_allocLookupTable
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____allocLookupTable_30;
	// System.Int32[][] NLayer.Decoder.LayerIIDecoderBase::_scfsi
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____scfsi_31;
	// System.Int32[][] NLayer.Decoder.LayerIIDecoderBase::_samples
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____samples_32;
	// System.Int32[][][] NLayer.Decoder.LayerIIDecoderBase::_scalefac
	Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* ____scalefac_33;
	// System.Single[] NLayer.Decoder.LayerIIDecoderBase::_polyPhaseBuf
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____polyPhaseBuf_34;
	// System.Int32[][] NLayer.Decoder.LayerIIDecoderBase::_allocation
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____allocation_35;

public:
	inline static int32_t get_offset_of__channels_27() { return static_cast<int32_t>(offsetof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF, ____channels_27)); }
	inline int32_t get__channels_27() const { return ____channels_27; }
	inline int32_t* get_address_of__channels_27() { return &____channels_27; }
	inline void set__channels_27(int32_t value)
	{
		____channels_27 = value;
	}

	inline static int32_t get_offset_of__jsbound_28() { return static_cast<int32_t>(offsetof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF, ____jsbound_28)); }
	inline int32_t get__jsbound_28() const { return ____jsbound_28; }
	inline int32_t* get_address_of__jsbound_28() { return &____jsbound_28; }
	inline void set__jsbound_28(int32_t value)
	{
		____jsbound_28 = value;
	}

	inline static int32_t get_offset_of__granuleCount_29() { return static_cast<int32_t>(offsetof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF, ____granuleCount_29)); }
	inline int32_t get__granuleCount_29() const { return ____granuleCount_29; }
	inline int32_t* get_address_of__granuleCount_29() { return &____granuleCount_29; }
	inline void set__granuleCount_29(int32_t value)
	{
		____granuleCount_29 = value;
	}

	inline static int32_t get_offset_of__allocLookupTable_30() { return static_cast<int32_t>(offsetof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF, ____allocLookupTable_30)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__allocLookupTable_30() const { return ____allocLookupTable_30; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__allocLookupTable_30() { return &____allocLookupTable_30; }
	inline void set__allocLookupTable_30(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____allocLookupTable_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____allocLookupTable_30), (void*)value);
	}

	inline static int32_t get_offset_of__scfsi_31() { return static_cast<int32_t>(offsetof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF, ____scfsi_31)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__scfsi_31() const { return ____scfsi_31; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__scfsi_31() { return &____scfsi_31; }
	inline void set__scfsi_31(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____scfsi_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____scfsi_31), (void*)value);
	}

	inline static int32_t get_offset_of__samples_32() { return static_cast<int32_t>(offsetof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF, ____samples_32)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__samples_32() const { return ____samples_32; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__samples_32() { return &____samples_32; }
	inline void set__samples_32(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____samples_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____samples_32), (void*)value);
	}

	inline static int32_t get_offset_of__scalefac_33() { return static_cast<int32_t>(offsetof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF, ____scalefac_33)); }
	inline Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* get__scalefac_33() const { return ____scalefac_33; }
	inline Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141** get_address_of__scalefac_33() { return &____scalefac_33; }
	inline void set__scalefac_33(Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* value)
	{
		____scalefac_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____scalefac_33), (void*)value);
	}

	inline static int32_t get_offset_of__polyPhaseBuf_34() { return static_cast<int32_t>(offsetof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF, ____polyPhaseBuf_34)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__polyPhaseBuf_34() const { return ____polyPhaseBuf_34; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__polyPhaseBuf_34() { return &____polyPhaseBuf_34; }
	inline void set__polyPhaseBuf_34(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____polyPhaseBuf_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____polyPhaseBuf_34), (void*)value);
	}

	inline static int32_t get_offset_of__allocation_35() { return static_cast<int32_t>(offsetof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF, ____allocation_35)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__allocation_35() const { return ____allocation_35; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__allocation_35() { return &____allocation_35; }
	inline void set__allocation_35(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____allocation_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____allocation_35), (void*)value);
	}
};

struct LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF_StaticFields
{
public:
	// System.Single[] NLayer.Decoder.LayerIIDecoderBase::_groupedC
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____groupedC_22;
	// System.Single[] NLayer.Decoder.LayerIIDecoderBase::_groupedD
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____groupedD_23;
	// System.Single[] NLayer.Decoder.LayerIIDecoderBase::_C
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____C_24;
	// System.Single[] NLayer.Decoder.LayerIIDecoderBase::_D
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____D_25;
	// System.Single[] NLayer.Decoder.LayerIIDecoderBase::_denormalMultiplier
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____denormalMultiplier_26;

public:
	inline static int32_t get_offset_of__groupedC_22() { return static_cast<int32_t>(offsetof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF_StaticFields, ____groupedC_22)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__groupedC_22() const { return ____groupedC_22; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__groupedC_22() { return &____groupedC_22; }
	inline void set__groupedC_22(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____groupedC_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____groupedC_22), (void*)value);
	}

	inline static int32_t get_offset_of__groupedD_23() { return static_cast<int32_t>(offsetof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF_StaticFields, ____groupedD_23)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__groupedD_23() const { return ____groupedD_23; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__groupedD_23() { return &____groupedD_23; }
	inline void set__groupedD_23(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____groupedD_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____groupedD_23), (void*)value);
	}

	inline static int32_t get_offset_of__C_24() { return static_cast<int32_t>(offsetof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF_StaticFields, ____C_24)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__C_24() const { return ____C_24; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__C_24() { return &____C_24; }
	inline void set__C_24(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____C_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____C_24), (void*)value);
	}

	inline static int32_t get_offset_of__D_25() { return static_cast<int32_t>(offsetof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF_StaticFields, ____D_25)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__D_25() const { return ____D_25; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__D_25() { return &____D_25; }
	inline void set__D_25(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____D_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____D_25), (void*)value);
	}

	inline static int32_t get_offset_of__denormalMultiplier_26() { return static_cast<int32_t>(offsetof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF_StaticFields, ____denormalMultiplier_26)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__denormalMultiplier_26() const { return ____denormalMultiplier_26; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__denormalMultiplier_26() { return &____denormalMultiplier_26; }
	inline void set__denormalMultiplier_26(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____denormalMultiplier_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____denormalMultiplier_26), (void*)value);
	}
};


// NLayer.Decoder.LayerIIIDecoder
struct  LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1  : public LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B
{
public:
	// NLayer.Decoder.LayerIIIDecoder/HybridMDCT NLayer.Decoder.LayerIIIDecoder::_hybrid
	HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878 * ____hybrid_22;
	// NLayer.Decoder.BitReservoir NLayer.Decoder.LayerIIIDecoder::_bitRes
	BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB * ____bitRes_23;
	// System.Int32 NLayer.Decoder.LayerIIIDecoder::_channels
	int32_t ____channels_24;
	// System.Int32 NLayer.Decoder.LayerIIIDecoder::_privBits
	int32_t ____privBits_25;
	// System.Int32 NLayer.Decoder.LayerIIIDecoder::_mainDataBegin
	int32_t ____mainDataBegin_26;
	// System.Int32[][] NLayer.Decoder.LayerIIIDecoder::_scfsi
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____scfsi_27;
	// System.Int32[][] NLayer.Decoder.LayerIIIDecoder::_part23Length
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____part23Length_28;
	// System.Int32[][] NLayer.Decoder.LayerIIIDecoder::_bigValues
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____bigValues_29;
	// System.Single[][] NLayer.Decoder.LayerIIIDecoder::_globalGain
	SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* ____globalGain_30;
	// System.Int32[][] NLayer.Decoder.LayerIIIDecoder::_scalefacCompress
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____scalefacCompress_31;
	// System.Boolean[][] NLayer.Decoder.LayerIIIDecoder::_blockSplitFlag
	BooleanU5BU5DU5BU5D_tD67717CF466EECE15F5A827693454E4F8E20FAE8* ____blockSplitFlag_32;
	// System.Boolean[][] NLayer.Decoder.LayerIIIDecoder::_mixedBlockFlag
	BooleanU5BU5DU5BU5D_tD67717CF466EECE15F5A827693454E4F8E20FAE8* ____mixedBlockFlag_33;
	// System.Int32[][] NLayer.Decoder.LayerIIIDecoder::_blockType
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____blockType_34;
	// System.Int32[][][] NLayer.Decoder.LayerIIIDecoder::_tableSelect
	Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* ____tableSelect_35;
	// System.Single[][][] NLayer.Decoder.LayerIIIDecoder::_subblockGain
	SingleU5BU5DU5BU5DU5BU5D_tA077D87E42A03FD45A6BEFFD79858AF9938E28A8* ____subblockGain_36;
	// System.Int32[][] NLayer.Decoder.LayerIIIDecoder::_regionAddress1
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____regionAddress1_37;
	// System.Int32[][] NLayer.Decoder.LayerIIIDecoder::_regionAddress2
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____regionAddress2_38;
	// System.Int32[][] NLayer.Decoder.LayerIIIDecoder::_preflag
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____preflag_39;
	// System.Single[][] NLayer.Decoder.LayerIIIDecoder::_scalefacScale
	SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* ____scalefacScale_40;
	// System.Int32[][] NLayer.Decoder.LayerIIIDecoder::_count1TableSelect
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____count1TableSelect_41;
	// System.Int32[] NLayer.Decoder.LayerIIIDecoder::_sfBandIndexL
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____sfBandIndexL_43;
	// System.Int32[] NLayer.Decoder.LayerIIIDecoder::_sfBandIndexS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____sfBandIndexS_44;
	// System.Byte[] NLayer.Decoder.LayerIIIDecoder::_cbLookupL
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____cbLookupL_45;
	// System.Byte[] NLayer.Decoder.LayerIIIDecoder::_cbLookupS
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____cbLookupS_46;
	// System.Byte[] NLayer.Decoder.LayerIIIDecoder::_cbwLookupS
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____cbwLookupS_47;
	// System.Int32 NLayer.Decoder.LayerIIIDecoder::_cbLookupSR
	int32_t ____cbLookupSR_48;
	// System.Int32[][][] NLayer.Decoder.LayerIIIDecoder::_scalefac
	Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* ____scalefac_51;
	// System.Single[][] NLayer.Decoder.LayerIIIDecoder::_samples
	SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* ____samples_54;
	// System.Single[] NLayer.Decoder.LayerIIIDecoder::_reorderBuf
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____reorderBuf_59;
	// System.Single[] NLayer.Decoder.LayerIIIDecoder::_polyPhase
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____polyPhase_62;

public:
	inline static int32_t get_offset_of__hybrid_22() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____hybrid_22)); }
	inline HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878 * get__hybrid_22() const { return ____hybrid_22; }
	inline HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878 ** get_address_of__hybrid_22() { return &____hybrid_22; }
	inline void set__hybrid_22(HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878 * value)
	{
		____hybrid_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____hybrid_22), (void*)value);
	}

	inline static int32_t get_offset_of__bitRes_23() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____bitRes_23)); }
	inline BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB * get__bitRes_23() const { return ____bitRes_23; }
	inline BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB ** get_address_of__bitRes_23() { return &____bitRes_23; }
	inline void set__bitRes_23(BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB * value)
	{
		____bitRes_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bitRes_23), (void*)value);
	}

	inline static int32_t get_offset_of__channels_24() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____channels_24)); }
	inline int32_t get__channels_24() const { return ____channels_24; }
	inline int32_t* get_address_of__channels_24() { return &____channels_24; }
	inline void set__channels_24(int32_t value)
	{
		____channels_24 = value;
	}

	inline static int32_t get_offset_of__privBits_25() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____privBits_25)); }
	inline int32_t get__privBits_25() const { return ____privBits_25; }
	inline int32_t* get_address_of__privBits_25() { return &____privBits_25; }
	inline void set__privBits_25(int32_t value)
	{
		____privBits_25 = value;
	}

	inline static int32_t get_offset_of__mainDataBegin_26() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____mainDataBegin_26)); }
	inline int32_t get__mainDataBegin_26() const { return ____mainDataBegin_26; }
	inline int32_t* get_address_of__mainDataBegin_26() { return &____mainDataBegin_26; }
	inline void set__mainDataBegin_26(int32_t value)
	{
		____mainDataBegin_26 = value;
	}

	inline static int32_t get_offset_of__scfsi_27() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____scfsi_27)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__scfsi_27() const { return ____scfsi_27; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__scfsi_27() { return &____scfsi_27; }
	inline void set__scfsi_27(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____scfsi_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____scfsi_27), (void*)value);
	}

	inline static int32_t get_offset_of__part23Length_28() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____part23Length_28)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__part23Length_28() const { return ____part23Length_28; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__part23Length_28() { return &____part23Length_28; }
	inline void set__part23Length_28(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____part23Length_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____part23Length_28), (void*)value);
	}

	inline static int32_t get_offset_of__bigValues_29() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____bigValues_29)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__bigValues_29() const { return ____bigValues_29; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__bigValues_29() { return &____bigValues_29; }
	inline void set__bigValues_29(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____bigValues_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bigValues_29), (void*)value);
	}

	inline static int32_t get_offset_of__globalGain_30() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____globalGain_30)); }
	inline SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* get__globalGain_30() const { return ____globalGain_30; }
	inline SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E** get_address_of__globalGain_30() { return &____globalGain_30; }
	inline void set__globalGain_30(SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* value)
	{
		____globalGain_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____globalGain_30), (void*)value);
	}

	inline static int32_t get_offset_of__scalefacCompress_31() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____scalefacCompress_31)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__scalefacCompress_31() const { return ____scalefacCompress_31; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__scalefacCompress_31() { return &____scalefacCompress_31; }
	inline void set__scalefacCompress_31(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____scalefacCompress_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____scalefacCompress_31), (void*)value);
	}

	inline static int32_t get_offset_of__blockSplitFlag_32() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____blockSplitFlag_32)); }
	inline BooleanU5BU5DU5BU5D_tD67717CF466EECE15F5A827693454E4F8E20FAE8* get__blockSplitFlag_32() const { return ____blockSplitFlag_32; }
	inline BooleanU5BU5DU5BU5D_tD67717CF466EECE15F5A827693454E4F8E20FAE8** get_address_of__blockSplitFlag_32() { return &____blockSplitFlag_32; }
	inline void set__blockSplitFlag_32(BooleanU5BU5DU5BU5D_tD67717CF466EECE15F5A827693454E4F8E20FAE8* value)
	{
		____blockSplitFlag_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____blockSplitFlag_32), (void*)value);
	}

	inline static int32_t get_offset_of__mixedBlockFlag_33() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____mixedBlockFlag_33)); }
	inline BooleanU5BU5DU5BU5D_tD67717CF466EECE15F5A827693454E4F8E20FAE8* get__mixedBlockFlag_33() const { return ____mixedBlockFlag_33; }
	inline BooleanU5BU5DU5BU5D_tD67717CF466EECE15F5A827693454E4F8E20FAE8** get_address_of__mixedBlockFlag_33() { return &____mixedBlockFlag_33; }
	inline void set__mixedBlockFlag_33(BooleanU5BU5DU5BU5D_tD67717CF466EECE15F5A827693454E4F8E20FAE8* value)
	{
		____mixedBlockFlag_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mixedBlockFlag_33), (void*)value);
	}

	inline static int32_t get_offset_of__blockType_34() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____blockType_34)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__blockType_34() const { return ____blockType_34; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__blockType_34() { return &____blockType_34; }
	inline void set__blockType_34(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____blockType_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____blockType_34), (void*)value);
	}

	inline static int32_t get_offset_of__tableSelect_35() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____tableSelect_35)); }
	inline Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* get__tableSelect_35() const { return ____tableSelect_35; }
	inline Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141** get_address_of__tableSelect_35() { return &____tableSelect_35; }
	inline void set__tableSelect_35(Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* value)
	{
		____tableSelect_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____tableSelect_35), (void*)value);
	}

	inline static int32_t get_offset_of__subblockGain_36() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____subblockGain_36)); }
	inline SingleU5BU5DU5BU5DU5BU5D_tA077D87E42A03FD45A6BEFFD79858AF9938E28A8* get__subblockGain_36() const { return ____subblockGain_36; }
	inline SingleU5BU5DU5BU5DU5BU5D_tA077D87E42A03FD45A6BEFFD79858AF9938E28A8** get_address_of__subblockGain_36() { return &____subblockGain_36; }
	inline void set__subblockGain_36(SingleU5BU5DU5BU5DU5BU5D_tA077D87E42A03FD45A6BEFFD79858AF9938E28A8* value)
	{
		____subblockGain_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____subblockGain_36), (void*)value);
	}

	inline static int32_t get_offset_of__regionAddress1_37() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____regionAddress1_37)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__regionAddress1_37() const { return ____regionAddress1_37; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__regionAddress1_37() { return &____regionAddress1_37; }
	inline void set__regionAddress1_37(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____regionAddress1_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____regionAddress1_37), (void*)value);
	}

	inline static int32_t get_offset_of__regionAddress2_38() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____regionAddress2_38)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__regionAddress2_38() const { return ____regionAddress2_38; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__regionAddress2_38() { return &____regionAddress2_38; }
	inline void set__regionAddress2_38(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____regionAddress2_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____regionAddress2_38), (void*)value);
	}

	inline static int32_t get_offset_of__preflag_39() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____preflag_39)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__preflag_39() const { return ____preflag_39; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__preflag_39() { return &____preflag_39; }
	inline void set__preflag_39(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____preflag_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____preflag_39), (void*)value);
	}

	inline static int32_t get_offset_of__scalefacScale_40() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____scalefacScale_40)); }
	inline SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* get__scalefacScale_40() const { return ____scalefacScale_40; }
	inline SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E** get_address_of__scalefacScale_40() { return &____scalefacScale_40; }
	inline void set__scalefacScale_40(SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* value)
	{
		____scalefacScale_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____scalefacScale_40), (void*)value);
	}

	inline static int32_t get_offset_of__count1TableSelect_41() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____count1TableSelect_41)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__count1TableSelect_41() const { return ____count1TableSelect_41; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__count1TableSelect_41() { return &____count1TableSelect_41; }
	inline void set__count1TableSelect_41(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____count1TableSelect_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____count1TableSelect_41), (void*)value);
	}

	inline static int32_t get_offset_of__sfBandIndexL_43() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____sfBandIndexL_43)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__sfBandIndexL_43() const { return ____sfBandIndexL_43; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__sfBandIndexL_43() { return &____sfBandIndexL_43; }
	inline void set__sfBandIndexL_43(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____sfBandIndexL_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sfBandIndexL_43), (void*)value);
	}

	inline static int32_t get_offset_of__sfBandIndexS_44() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____sfBandIndexS_44)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__sfBandIndexS_44() const { return ____sfBandIndexS_44; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__sfBandIndexS_44() { return &____sfBandIndexS_44; }
	inline void set__sfBandIndexS_44(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____sfBandIndexS_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sfBandIndexS_44), (void*)value);
	}

	inline static int32_t get_offset_of__cbLookupL_45() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____cbLookupL_45)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__cbLookupL_45() const { return ____cbLookupL_45; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__cbLookupL_45() { return &____cbLookupL_45; }
	inline void set__cbLookupL_45(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____cbLookupL_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cbLookupL_45), (void*)value);
	}

	inline static int32_t get_offset_of__cbLookupS_46() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____cbLookupS_46)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__cbLookupS_46() const { return ____cbLookupS_46; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__cbLookupS_46() { return &____cbLookupS_46; }
	inline void set__cbLookupS_46(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____cbLookupS_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cbLookupS_46), (void*)value);
	}

	inline static int32_t get_offset_of__cbwLookupS_47() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____cbwLookupS_47)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__cbwLookupS_47() const { return ____cbwLookupS_47; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__cbwLookupS_47() { return &____cbwLookupS_47; }
	inline void set__cbwLookupS_47(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____cbwLookupS_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cbwLookupS_47), (void*)value);
	}

	inline static int32_t get_offset_of__cbLookupSR_48() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____cbLookupSR_48)); }
	inline int32_t get__cbLookupSR_48() const { return ____cbLookupSR_48; }
	inline int32_t* get_address_of__cbLookupSR_48() { return &____cbLookupSR_48; }
	inline void set__cbLookupSR_48(int32_t value)
	{
		____cbLookupSR_48 = value;
	}

	inline static int32_t get_offset_of__scalefac_51() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____scalefac_51)); }
	inline Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* get__scalefac_51() const { return ____scalefac_51; }
	inline Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141** get_address_of__scalefac_51() { return &____scalefac_51; }
	inline void set__scalefac_51(Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* value)
	{
		____scalefac_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____scalefac_51), (void*)value);
	}

	inline static int32_t get_offset_of__samples_54() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____samples_54)); }
	inline SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* get__samples_54() const { return ____samples_54; }
	inline SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E** get_address_of__samples_54() { return &____samples_54; }
	inline void set__samples_54(SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* value)
	{
		____samples_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____samples_54), (void*)value);
	}

	inline static int32_t get_offset_of__reorderBuf_59() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____reorderBuf_59)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__reorderBuf_59() const { return ____reorderBuf_59; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__reorderBuf_59() { return &____reorderBuf_59; }
	inline void set__reorderBuf_59(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____reorderBuf_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____reorderBuf_59), (void*)value);
	}

	inline static int32_t get_offset_of__polyPhase_62() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1, ____polyPhase_62)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__polyPhase_62() const { return ____polyPhase_62; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__polyPhase_62() { return &____polyPhase_62; }
	inline void set__polyPhase_62(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____polyPhase_62 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____polyPhase_62), (void*)value);
	}
};

struct LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields
{
public:
	// System.Single[] NLayer.Decoder.LayerIIIDecoder::GAIN_TAB
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___GAIN_TAB_42;
	// System.Int32[][] NLayer.Decoder.LayerIIIDecoder::_sfBandIndexLTable
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____sfBandIndexLTable_49;
	// System.Int32[][] NLayer.Decoder.LayerIIIDecoder::_sfBandIndexSTable
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____sfBandIndexSTable_50;
	// System.Int32[][] NLayer.Decoder.LayerIIIDecoder::_slen
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____slen_52;
	// System.Int32[][][] NLayer.Decoder.LayerIIIDecoder::_sfbBlockCntTab
	Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* ____sfbBlockCntTab_53;
	// System.Int32[] NLayer.Decoder.LayerIIIDecoder::PRETAB
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___PRETAB_55;
	// System.Single[] NLayer.Decoder.LayerIIIDecoder::POW2_TAB
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___POW2_TAB_56;
	// System.Single[][] NLayer.Decoder.LayerIIIDecoder::_isRatio
	SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* ____isRatio_57;
	// System.Single[][][] NLayer.Decoder.LayerIIIDecoder::_lsfRatio
	SingleU5BU5DU5BU5DU5BU5D_tA077D87E42A03FD45A6BEFFD79858AF9938E28A8* ____lsfRatio_58;
	// System.Single[] NLayer.Decoder.LayerIIIDecoder::_scs
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____scs_60;
	// System.Single[] NLayer.Decoder.LayerIIIDecoder::_sca
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____sca_61;

public:
	inline static int32_t get_offset_of_GAIN_TAB_42() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields, ___GAIN_TAB_42)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_GAIN_TAB_42() const { return ___GAIN_TAB_42; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_GAIN_TAB_42() { return &___GAIN_TAB_42; }
	inline void set_GAIN_TAB_42(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___GAIN_TAB_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GAIN_TAB_42), (void*)value);
	}

	inline static int32_t get_offset_of__sfBandIndexLTable_49() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields, ____sfBandIndexLTable_49)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__sfBandIndexLTable_49() const { return ____sfBandIndexLTable_49; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__sfBandIndexLTable_49() { return &____sfBandIndexLTable_49; }
	inline void set__sfBandIndexLTable_49(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____sfBandIndexLTable_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sfBandIndexLTable_49), (void*)value);
	}

	inline static int32_t get_offset_of__sfBandIndexSTable_50() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields, ____sfBandIndexSTable_50)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__sfBandIndexSTable_50() const { return ____sfBandIndexSTable_50; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__sfBandIndexSTable_50() { return &____sfBandIndexSTable_50; }
	inline void set__sfBandIndexSTable_50(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____sfBandIndexSTable_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sfBandIndexSTable_50), (void*)value);
	}

	inline static int32_t get_offset_of__slen_52() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields, ____slen_52)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__slen_52() const { return ____slen_52; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__slen_52() { return &____slen_52; }
	inline void set__slen_52(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____slen_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____slen_52), (void*)value);
	}

	inline static int32_t get_offset_of__sfbBlockCntTab_53() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields, ____sfbBlockCntTab_53)); }
	inline Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* get__sfbBlockCntTab_53() const { return ____sfbBlockCntTab_53; }
	inline Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141** get_address_of__sfbBlockCntTab_53() { return &____sfbBlockCntTab_53; }
	inline void set__sfbBlockCntTab_53(Int32U5BU5DU5BU5DU5BU5D_tA2A4BFB86627B22F7F53ECBA14FBF56167097141* value)
	{
		____sfbBlockCntTab_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sfbBlockCntTab_53), (void*)value);
	}

	inline static int32_t get_offset_of_PRETAB_55() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields, ___PRETAB_55)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_PRETAB_55() const { return ___PRETAB_55; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_PRETAB_55() { return &___PRETAB_55; }
	inline void set_PRETAB_55(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___PRETAB_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PRETAB_55), (void*)value);
	}

	inline static int32_t get_offset_of_POW2_TAB_56() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields, ___POW2_TAB_56)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_POW2_TAB_56() const { return ___POW2_TAB_56; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_POW2_TAB_56() { return &___POW2_TAB_56; }
	inline void set_POW2_TAB_56(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___POW2_TAB_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___POW2_TAB_56), (void*)value);
	}

	inline static int32_t get_offset_of__isRatio_57() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields, ____isRatio_57)); }
	inline SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* get__isRatio_57() const { return ____isRatio_57; }
	inline SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E** get_address_of__isRatio_57() { return &____isRatio_57; }
	inline void set__isRatio_57(SingleU5BU5DU5BU5D_tC2E25498616DDBEA3B03D43855DEBC928046392E* value)
	{
		____isRatio_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____isRatio_57), (void*)value);
	}

	inline static int32_t get_offset_of__lsfRatio_58() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields, ____lsfRatio_58)); }
	inline SingleU5BU5DU5BU5DU5BU5D_tA077D87E42A03FD45A6BEFFD79858AF9938E28A8* get__lsfRatio_58() const { return ____lsfRatio_58; }
	inline SingleU5BU5DU5BU5DU5BU5D_tA077D87E42A03FD45A6BEFFD79858AF9938E28A8** get_address_of__lsfRatio_58() { return &____lsfRatio_58; }
	inline void set__lsfRatio_58(SingleU5BU5DU5BU5DU5BU5D_tA077D87E42A03FD45A6BEFFD79858AF9938E28A8* value)
	{
		____lsfRatio_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lsfRatio_58), (void*)value);
	}

	inline static int32_t get_offset_of__scs_60() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields, ____scs_60)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__scs_60() const { return ____scs_60; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__scs_60() { return &____scs_60; }
	inline void set__scs_60(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____scs_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____scs_60), (void*)value);
	}

	inline static int32_t get_offset_of__sca_61() { return static_cast<int32_t>(offsetof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields, ____sca_61)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__sca_61() const { return ____sca_61; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__sca_61() { return &____sca_61; }
	inline void set__sca_61(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____sca_61 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sca_61), (void*)value);
	}
};


// NatSuite.Recorders.Inputs.CameraInput
struct  CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655  : public RuntimeObject
{
public:
	// System.Int32 NatSuite.Recorders.Inputs.CameraInput::frameSkip
	int32_t ___frameSkip_0;
	// NatSuite.Recorders.IMediaRecorder NatSuite.Recorders.Inputs.CameraInput::recorder
	RuntimeObject* ___recorder_1;
	// NatSuite.Recorders.Clocks.IClock NatSuite.Recorders.Inputs.CameraInput::clock
	RuntimeObject* ___clock_2;
	// UnityEngine.Camera[] NatSuite.Recorders.Inputs.CameraInput::cameras
	CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* ___cameras_3;
	// UnityEngine.RenderTextureDescriptor NatSuite.Recorders.Inputs.CameraInput::frameDescriptor
	RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E  ___frameDescriptor_4;
	// UnityEngine.Texture2D NatSuite.Recorders.Inputs.CameraInput::readbackBuffer
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___readbackBuffer_5;
	// NatSuite.Recorders.Inputs.CameraInput/CameraInputAttachment NatSuite.Recorders.Inputs.CameraInput::attachment
	CameraInputAttachment_t415800A4510BE48007515F8FA8201B5FCD986328 * ___attachment_6;
	// System.Byte[] NatSuite.Recorders.Inputs.CameraInput::pixelBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pixelBuffer_7;
	// System.Int32 NatSuite.Recorders.Inputs.CameraInput::frameCount
	int32_t ___frameCount_8;

public:
	inline static int32_t get_offset_of_frameSkip_0() { return static_cast<int32_t>(offsetof(CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655, ___frameSkip_0)); }
	inline int32_t get_frameSkip_0() const { return ___frameSkip_0; }
	inline int32_t* get_address_of_frameSkip_0() { return &___frameSkip_0; }
	inline void set_frameSkip_0(int32_t value)
	{
		___frameSkip_0 = value;
	}

	inline static int32_t get_offset_of_recorder_1() { return static_cast<int32_t>(offsetof(CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655, ___recorder_1)); }
	inline RuntimeObject* get_recorder_1() const { return ___recorder_1; }
	inline RuntimeObject** get_address_of_recorder_1() { return &___recorder_1; }
	inline void set_recorder_1(RuntimeObject* value)
	{
		___recorder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___recorder_1), (void*)value);
	}

	inline static int32_t get_offset_of_clock_2() { return static_cast<int32_t>(offsetof(CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655, ___clock_2)); }
	inline RuntimeObject* get_clock_2() const { return ___clock_2; }
	inline RuntimeObject** get_address_of_clock_2() { return &___clock_2; }
	inline void set_clock_2(RuntimeObject* value)
	{
		___clock_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clock_2), (void*)value);
	}

	inline static int32_t get_offset_of_cameras_3() { return static_cast<int32_t>(offsetof(CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655, ___cameras_3)); }
	inline CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* get_cameras_3() const { return ___cameras_3; }
	inline CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9** get_address_of_cameras_3() { return &___cameras_3; }
	inline void set_cameras_3(CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* value)
	{
		___cameras_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameras_3), (void*)value);
	}

	inline static int32_t get_offset_of_frameDescriptor_4() { return static_cast<int32_t>(offsetof(CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655, ___frameDescriptor_4)); }
	inline RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E  get_frameDescriptor_4() const { return ___frameDescriptor_4; }
	inline RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E * get_address_of_frameDescriptor_4() { return &___frameDescriptor_4; }
	inline void set_frameDescriptor_4(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E  value)
	{
		___frameDescriptor_4 = value;
	}

	inline static int32_t get_offset_of_readbackBuffer_5() { return static_cast<int32_t>(offsetof(CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655, ___readbackBuffer_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_readbackBuffer_5() const { return ___readbackBuffer_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_readbackBuffer_5() { return &___readbackBuffer_5; }
	inline void set_readbackBuffer_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___readbackBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___readbackBuffer_5), (void*)value);
	}

	inline static int32_t get_offset_of_attachment_6() { return static_cast<int32_t>(offsetof(CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655, ___attachment_6)); }
	inline CameraInputAttachment_t415800A4510BE48007515F8FA8201B5FCD986328 * get_attachment_6() const { return ___attachment_6; }
	inline CameraInputAttachment_t415800A4510BE48007515F8FA8201B5FCD986328 ** get_address_of_attachment_6() { return &___attachment_6; }
	inline void set_attachment_6(CameraInputAttachment_t415800A4510BE48007515F8FA8201B5FCD986328 * value)
	{
		___attachment_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___attachment_6), (void*)value);
	}

	inline static int32_t get_offset_of_pixelBuffer_7() { return static_cast<int32_t>(offsetof(CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655, ___pixelBuffer_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_pixelBuffer_7() const { return ___pixelBuffer_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_pixelBuffer_7() { return &___pixelBuffer_7; }
	inline void set_pixelBuffer_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___pixelBuffer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pixelBuffer_7), (void*)value);
	}

	inline static int32_t get_offset_of_frameCount_8() { return static_cast<int32_t>(offsetof(CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655, ___frameCount_8)); }
	inline int32_t get_frameCount_8() const { return ___frameCount_8; }
	inline int32_t* get_address_of_frameCount_8() { return &___frameCount_8; }
	inline void set_frameCount_8(int32_t value)
	{
		___frameCount_8 = value;
	}
};


// NatSuite.Recorders.Internal.Bridge/RecordingHandler
struct  RecordingHandler_t4D3C0E8013CFB4506D712CB5C9AB047B3D82A329  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// NLayer.Decoder.LayerIDecoder
struct  LayerIDecoder_t29E88F178B1D31CBFCDBF7BF51304FED69AF8D55  : public LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF
{
public:

public:
};

struct LayerIDecoder_t29E88F178B1D31CBFCDBF7BF51304FED69AF8D55_StaticFields
{
public:
	// System.Int32[] NLayer.Decoder.LayerIDecoder::_rateTable
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____rateTable_36;
	// System.Int32[][] NLayer.Decoder.LayerIDecoder::_allocLookupTable
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____allocLookupTable_37;

public:
	inline static int32_t get_offset_of__rateTable_36() { return static_cast<int32_t>(offsetof(LayerIDecoder_t29E88F178B1D31CBFCDBF7BF51304FED69AF8D55_StaticFields, ____rateTable_36)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__rateTable_36() const { return ____rateTable_36; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__rateTable_36() { return &____rateTable_36; }
	inline void set__rateTable_36(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____rateTable_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rateTable_36), (void*)value);
	}

	inline static int32_t get_offset_of__allocLookupTable_37() { return static_cast<int32_t>(offsetof(LayerIDecoder_t29E88F178B1D31CBFCDBF7BF51304FED69AF8D55_StaticFields, ____allocLookupTable_37)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__allocLookupTable_37() const { return ____allocLookupTable_37; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__allocLookupTable_37() { return &____allocLookupTable_37; }
	inline void set__allocLookupTable_37(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____allocLookupTable_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____allocLookupTable_37), (void*)value);
	}
};


// NLayer.Decoder.LayerIIDecoder
struct  LayerIIDecoder_t06D951DC8F7021B2CF4D70FCE55297ECFE87ABFE  : public LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF
{
public:

public:
};

struct LayerIIDecoder_t06D951DC8F7021B2CF4D70FCE55297ECFE87ABFE_StaticFields
{
public:
	// System.Int32[][] NLayer.Decoder.LayerIIDecoder::_rateLookupTable
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____rateLookupTable_36;
	// System.Int32[][] NLayer.Decoder.LayerIIDecoder::_allocLookupTable
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ____allocLookupTable_37;

public:
	inline static int32_t get_offset_of__rateLookupTable_36() { return static_cast<int32_t>(offsetof(LayerIIDecoder_t06D951DC8F7021B2CF4D70FCE55297ECFE87ABFE_StaticFields, ____rateLookupTable_36)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__rateLookupTable_36() const { return ____rateLookupTable_36; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__rateLookupTable_36() { return &____rateLookupTable_36; }
	inline void set__rateLookupTable_36(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____rateLookupTable_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rateLookupTable_36), (void*)value);
	}

	inline static int32_t get_offset_of__allocLookupTable_37() { return static_cast<int32_t>(offsetof(LayerIIDecoder_t06D951DC8F7021B2CF4D70FCE55297ECFE87ABFE_StaticFields, ____allocLookupTable_37)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get__allocLookupTable_37() const { return ____allocLookupTable_37; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of__allocLookupTable_37() { return &____allocLookupTable_37; }
	inline void set__allocLookupTable_37(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		____allocLookupTable_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____allocLookupTable_37), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// DentedPixel.LTExamples.PathBezier
struct  PathBezier_tE51A8452CDA44A02AB58E56D6F880E81512557D9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform[] DentedPixel.LTExamples.PathBezier::trans
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ___trans_4;
	// LTBezierPath DentedPixel.LTExamples.PathBezier::cr
	LTBezierPath_t942F5E0A4B5DDDD5E08F2CE947CB5A47AEB7F22D * ___cr_5;
	// UnityEngine.GameObject DentedPixel.LTExamples.PathBezier::avatar1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___avatar1_6;
	// System.Single DentedPixel.LTExamples.PathBezier::iter
	float ___iter_7;

public:
	inline static int32_t get_offset_of_trans_4() { return static_cast<int32_t>(offsetof(PathBezier_tE51A8452CDA44A02AB58E56D6F880E81512557D9, ___trans_4)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get_trans_4() const { return ___trans_4; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of_trans_4() { return &___trans_4; }
	inline void set_trans_4(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		___trans_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trans_4), (void*)value);
	}

	inline static int32_t get_offset_of_cr_5() { return static_cast<int32_t>(offsetof(PathBezier_tE51A8452CDA44A02AB58E56D6F880E81512557D9, ___cr_5)); }
	inline LTBezierPath_t942F5E0A4B5DDDD5E08F2CE947CB5A47AEB7F22D * get_cr_5() const { return ___cr_5; }
	inline LTBezierPath_t942F5E0A4B5DDDD5E08F2CE947CB5A47AEB7F22D ** get_address_of_cr_5() { return &___cr_5; }
	inline void set_cr_5(LTBezierPath_t942F5E0A4B5DDDD5E08F2CE947CB5A47AEB7F22D * value)
	{
		___cr_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cr_5), (void*)value);
	}

	inline static int32_t get_offset_of_avatar1_6() { return static_cast<int32_t>(offsetof(PathBezier_tE51A8452CDA44A02AB58E56D6F880E81512557D9, ___avatar1_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_avatar1_6() const { return ___avatar1_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_avatar1_6() { return &___avatar1_6; }
	inline void set_avatar1_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___avatar1_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___avatar1_6), (void*)value);
	}

	inline static int32_t get_offset_of_iter_7() { return static_cast<int32_t>(offsetof(PathBezier_tE51A8452CDA44A02AB58E56D6F880E81512557D9, ___iter_7)); }
	inline float get_iter_7() const { return ___iter_7; }
	inline float* get_address_of_iter_7() { return &___iter_7; }
	inline void set_iter_7(float value)
	{
		___iter_7 = value;
	}
};


// DentedPixel.LTExamples.TestingUnitTests
struct  TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cube1_4;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cube2_5;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube3
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cube3_6;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube4
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cube4_7;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cubeAlpha1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cubeAlpha1_8;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cubeAlpha2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cubeAlpha2_9;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests::eventGameObjectWasCalled
	bool ___eventGameObjectWasCalled_10;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests::eventGeneralWasCalled
	bool ___eventGeneralWasCalled_11;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::lt1Id
	int32_t ___lt1Id_12;
	// LTDescr DentedPixel.LTExamples.TestingUnitTests::lt2
	LTDescr_t88934797D6A324735209180E7958727F7E3746A9 * ___lt2_13;
	// LTDescr DentedPixel.LTExamples.TestingUnitTests::lt3
	LTDescr_t88934797D6A324735209180E7958727F7E3746A9 * ___lt3_14;
	// LTDescr DentedPixel.LTExamples.TestingUnitTests::lt4
	LTDescr_t88934797D6A324735209180E7958727F7E3746A9 * ___lt4_15;
	// LTDescr[] DentedPixel.LTExamples.TestingUnitTests::groupTweens
	LTDescrU5BU5D_tA2196F2BCBCF136D725BED68B3C73FA238926239* ___groupTweens_16;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests::groupGOs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___groupGOs_17;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::groupTweensCnt
	int32_t ___groupTweensCnt_18;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::rotateRepeat
	int32_t ___rotateRepeat_19;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::rotateRepeatAngle
	int32_t ___rotateRepeatAngle_20;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::boxNoCollider
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___boxNoCollider_21;
	// System.Single DentedPixel.LTExamples.TestingUnitTests::timeElapsedNormalTimeScale
	float ___timeElapsedNormalTimeScale_22;
	// System.Single DentedPixel.LTExamples.TestingUnitTests::timeElapsedIgnoreTimeScale
	float ___timeElapsedIgnoreTimeScale_23;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests::pauseTweenDidFinish
	bool ___pauseTweenDidFinish_24;

public:
	inline static int32_t get_offset_of_cube1_4() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___cube1_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cube1_4() const { return ___cube1_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cube1_4() { return &___cube1_4; }
	inline void set_cube1_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cube1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube1_4), (void*)value);
	}

	inline static int32_t get_offset_of_cube2_5() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___cube2_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cube2_5() const { return ___cube2_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cube2_5() { return &___cube2_5; }
	inline void set_cube2_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cube2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube2_5), (void*)value);
	}

	inline static int32_t get_offset_of_cube3_6() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___cube3_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cube3_6() const { return ___cube3_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cube3_6() { return &___cube3_6; }
	inline void set_cube3_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cube3_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube3_6), (void*)value);
	}

	inline static int32_t get_offset_of_cube4_7() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___cube4_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cube4_7() const { return ___cube4_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cube4_7() { return &___cube4_7; }
	inline void set_cube4_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cube4_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube4_7), (void*)value);
	}

	inline static int32_t get_offset_of_cubeAlpha1_8() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___cubeAlpha1_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cubeAlpha1_8() const { return ___cubeAlpha1_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cubeAlpha1_8() { return &___cubeAlpha1_8; }
	inline void set_cubeAlpha1_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cubeAlpha1_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeAlpha1_8), (void*)value);
	}

	inline static int32_t get_offset_of_cubeAlpha2_9() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___cubeAlpha2_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cubeAlpha2_9() const { return ___cubeAlpha2_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cubeAlpha2_9() { return &___cubeAlpha2_9; }
	inline void set_cubeAlpha2_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cubeAlpha2_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeAlpha2_9), (void*)value);
	}

	inline static int32_t get_offset_of_eventGameObjectWasCalled_10() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___eventGameObjectWasCalled_10)); }
	inline bool get_eventGameObjectWasCalled_10() const { return ___eventGameObjectWasCalled_10; }
	inline bool* get_address_of_eventGameObjectWasCalled_10() { return &___eventGameObjectWasCalled_10; }
	inline void set_eventGameObjectWasCalled_10(bool value)
	{
		___eventGameObjectWasCalled_10 = value;
	}

	inline static int32_t get_offset_of_eventGeneralWasCalled_11() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___eventGeneralWasCalled_11)); }
	inline bool get_eventGeneralWasCalled_11() const { return ___eventGeneralWasCalled_11; }
	inline bool* get_address_of_eventGeneralWasCalled_11() { return &___eventGeneralWasCalled_11; }
	inline void set_eventGeneralWasCalled_11(bool value)
	{
		___eventGeneralWasCalled_11 = value;
	}

	inline static int32_t get_offset_of_lt1Id_12() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___lt1Id_12)); }
	inline int32_t get_lt1Id_12() const { return ___lt1Id_12; }
	inline int32_t* get_address_of_lt1Id_12() { return &___lt1Id_12; }
	inline void set_lt1Id_12(int32_t value)
	{
		___lt1Id_12 = value;
	}

	inline static int32_t get_offset_of_lt2_13() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___lt2_13)); }
	inline LTDescr_t88934797D6A324735209180E7958727F7E3746A9 * get_lt2_13() const { return ___lt2_13; }
	inline LTDescr_t88934797D6A324735209180E7958727F7E3746A9 ** get_address_of_lt2_13() { return &___lt2_13; }
	inline void set_lt2_13(LTDescr_t88934797D6A324735209180E7958727F7E3746A9 * value)
	{
		___lt2_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lt2_13), (void*)value);
	}

	inline static int32_t get_offset_of_lt3_14() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___lt3_14)); }
	inline LTDescr_t88934797D6A324735209180E7958727F7E3746A9 * get_lt3_14() const { return ___lt3_14; }
	inline LTDescr_t88934797D6A324735209180E7958727F7E3746A9 ** get_address_of_lt3_14() { return &___lt3_14; }
	inline void set_lt3_14(LTDescr_t88934797D6A324735209180E7958727F7E3746A9 * value)
	{
		___lt3_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lt3_14), (void*)value);
	}

	inline static int32_t get_offset_of_lt4_15() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___lt4_15)); }
	inline LTDescr_t88934797D6A324735209180E7958727F7E3746A9 * get_lt4_15() const { return ___lt4_15; }
	inline LTDescr_t88934797D6A324735209180E7958727F7E3746A9 ** get_address_of_lt4_15() { return &___lt4_15; }
	inline void set_lt4_15(LTDescr_t88934797D6A324735209180E7958727F7E3746A9 * value)
	{
		___lt4_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lt4_15), (void*)value);
	}

	inline static int32_t get_offset_of_groupTweens_16() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___groupTweens_16)); }
	inline LTDescrU5BU5D_tA2196F2BCBCF136D725BED68B3C73FA238926239* get_groupTweens_16() const { return ___groupTweens_16; }
	inline LTDescrU5BU5D_tA2196F2BCBCF136D725BED68B3C73FA238926239** get_address_of_groupTweens_16() { return &___groupTweens_16; }
	inline void set_groupTweens_16(LTDescrU5BU5D_tA2196F2BCBCF136D725BED68B3C73FA238926239* value)
	{
		___groupTweens_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groupTweens_16), (void*)value);
	}

	inline static int32_t get_offset_of_groupGOs_17() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___groupGOs_17)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_groupGOs_17() const { return ___groupGOs_17; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_groupGOs_17() { return &___groupGOs_17; }
	inline void set_groupGOs_17(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___groupGOs_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groupGOs_17), (void*)value);
	}

	inline static int32_t get_offset_of_groupTweensCnt_18() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___groupTweensCnt_18)); }
	inline int32_t get_groupTweensCnt_18() const { return ___groupTweensCnt_18; }
	inline int32_t* get_address_of_groupTweensCnt_18() { return &___groupTweensCnt_18; }
	inline void set_groupTweensCnt_18(int32_t value)
	{
		___groupTweensCnt_18 = value;
	}

	inline static int32_t get_offset_of_rotateRepeat_19() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___rotateRepeat_19)); }
	inline int32_t get_rotateRepeat_19() const { return ___rotateRepeat_19; }
	inline int32_t* get_address_of_rotateRepeat_19() { return &___rotateRepeat_19; }
	inline void set_rotateRepeat_19(int32_t value)
	{
		___rotateRepeat_19 = value;
	}

	inline static int32_t get_offset_of_rotateRepeatAngle_20() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___rotateRepeatAngle_20)); }
	inline int32_t get_rotateRepeatAngle_20() const { return ___rotateRepeatAngle_20; }
	inline int32_t* get_address_of_rotateRepeatAngle_20() { return &___rotateRepeatAngle_20; }
	inline void set_rotateRepeatAngle_20(int32_t value)
	{
		___rotateRepeatAngle_20 = value;
	}

	inline static int32_t get_offset_of_boxNoCollider_21() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___boxNoCollider_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_boxNoCollider_21() const { return ___boxNoCollider_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_boxNoCollider_21() { return &___boxNoCollider_21; }
	inline void set_boxNoCollider_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___boxNoCollider_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___boxNoCollider_21), (void*)value);
	}

	inline static int32_t get_offset_of_timeElapsedNormalTimeScale_22() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___timeElapsedNormalTimeScale_22)); }
	inline float get_timeElapsedNormalTimeScale_22() const { return ___timeElapsedNormalTimeScale_22; }
	inline float* get_address_of_timeElapsedNormalTimeScale_22() { return &___timeElapsedNormalTimeScale_22; }
	inline void set_timeElapsedNormalTimeScale_22(float value)
	{
		___timeElapsedNormalTimeScale_22 = value;
	}

	inline static int32_t get_offset_of_timeElapsedIgnoreTimeScale_23() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___timeElapsedIgnoreTimeScale_23)); }
	inline float get_timeElapsedIgnoreTimeScale_23() const { return ___timeElapsedIgnoreTimeScale_23; }
	inline float* get_address_of_timeElapsedIgnoreTimeScale_23() { return &___timeElapsedIgnoreTimeScale_23; }
	inline void set_timeElapsedIgnoreTimeScale_23(float value)
	{
		___timeElapsedIgnoreTimeScale_23 = value;
	}

	inline static int32_t get_offset_of_pauseTweenDidFinish_24() { return static_cast<int32_t>(offsetof(TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB, ___pauseTweenDidFinish_24)); }
	inline bool get_pauseTweenDidFinish_24() const { return ___pauseTweenDidFinish_24; }
	inline bool* get_address_of_pauseTweenDidFinish_24() { return &___pauseTweenDidFinish_24; }
	inline void set_pauseTweenDidFinish_24(bool value)
	{
		___pauseTweenDidFinish_24 = value;
	}
};


// NatSuite.Examples.Components.CameraPreview
struct  CameraPreview_t14366C64F8D63434B92D4F4DE771EDEE07EAE89C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.WebCamTexture NatSuite.Examples.Components.CameraPreview::<cameraTexture>k__BackingField
	WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * ___U3CcameraTextureU3Ek__BackingField_4;
	// UnityEngine.UI.RawImage NatSuite.Examples.Components.CameraPreview::rawImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___rawImage_5;
	// UnityEngine.UI.AspectRatioFitter NatSuite.Examples.Components.CameraPreview::aspectFitter
	AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6 * ___aspectFitter_6;

public:
	inline static int32_t get_offset_of_U3CcameraTextureU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CameraPreview_t14366C64F8D63434B92D4F4DE771EDEE07EAE89C, ___U3CcameraTextureU3Ek__BackingField_4)); }
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * get_U3CcameraTextureU3Ek__BackingField_4() const { return ___U3CcameraTextureU3Ek__BackingField_4; }
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 ** get_address_of_U3CcameraTextureU3Ek__BackingField_4() { return &___U3CcameraTextureU3Ek__BackingField_4; }
	inline void set_U3CcameraTextureU3Ek__BackingField_4(WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * value)
	{
		___U3CcameraTextureU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcameraTextureU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_rawImage_5() { return static_cast<int32_t>(offsetof(CameraPreview_t14366C64F8D63434B92D4F4DE771EDEE07EAE89C, ___rawImage_5)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_rawImage_5() const { return ___rawImage_5; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_rawImage_5() { return &___rawImage_5; }
	inline void set_rawImage_5(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___rawImage_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rawImage_5), (void*)value);
	}

	inline static int32_t get_offset_of_aspectFitter_6() { return static_cast<int32_t>(offsetof(CameraPreview_t14366C64F8D63434B92D4F4DE771EDEE07EAE89C, ___aspectFitter_6)); }
	inline AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6 * get_aspectFitter_6() const { return ___aspectFitter_6; }
	inline AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6 ** get_address_of_aspectFitter_6() { return &___aspectFitter_6; }
	inline void set_aspectFitter_6(AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6 * value)
	{
		___aspectFitter_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___aspectFitter_6), (void*)value);
	}
};


// NatSuite.Examples.Components.RecordButton
struct  RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image NatSuite.Examples.Components.RecordButton::button
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___button_4;
	// UnityEngine.UI.Image NatSuite.Examples.Components.RecordButton::countdown
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___countdown_5;
	// UnityEngine.Events.UnityEvent NatSuite.Examples.Components.RecordButton::onTouchDown
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onTouchDown_6;
	// UnityEngine.Events.UnityEvent NatSuite.Examples.Components.RecordButton::onTouchUp
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onTouchUp_7;
	// System.Boolean NatSuite.Examples.Components.RecordButton::pressed
	bool ___pressed_8;
	// UnityEngine.Animation NatSuite.Examples.Components.RecordButton::_Animator
	Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * ____Animator_10;
	// UnityEngine.AnimationClip NatSuite.Examples.Components.RecordButton::_Start
	AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * ____Start_11;
	// UnityEngine.AnimationClip NatSuite.Examples.Components.RecordButton::_Stop
	AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * ____Stop_12;

public:
	inline static int32_t get_offset_of_button_4() { return static_cast<int32_t>(offsetof(RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D, ___button_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_button_4() const { return ___button_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_button_4() { return &___button_4; }
	inline void set_button_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___button_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___button_4), (void*)value);
	}

	inline static int32_t get_offset_of_countdown_5() { return static_cast<int32_t>(offsetof(RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D, ___countdown_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_countdown_5() const { return ___countdown_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_countdown_5() { return &___countdown_5; }
	inline void set_countdown_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___countdown_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___countdown_5), (void*)value);
	}

	inline static int32_t get_offset_of_onTouchDown_6() { return static_cast<int32_t>(offsetof(RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D, ___onTouchDown_6)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onTouchDown_6() const { return ___onTouchDown_6; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onTouchDown_6() { return &___onTouchDown_6; }
	inline void set_onTouchDown_6(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onTouchDown_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onTouchDown_6), (void*)value);
	}

	inline static int32_t get_offset_of_onTouchUp_7() { return static_cast<int32_t>(offsetof(RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D, ___onTouchUp_7)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onTouchUp_7() const { return ___onTouchUp_7; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onTouchUp_7() { return &___onTouchUp_7; }
	inline void set_onTouchUp_7(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onTouchUp_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onTouchUp_7), (void*)value);
	}

	inline static int32_t get_offset_of_pressed_8() { return static_cast<int32_t>(offsetof(RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D, ___pressed_8)); }
	inline bool get_pressed_8() const { return ___pressed_8; }
	inline bool* get_address_of_pressed_8() { return &___pressed_8; }
	inline void set_pressed_8(bool value)
	{
		___pressed_8 = value;
	}

	inline static int32_t get_offset_of__Animator_10() { return static_cast<int32_t>(offsetof(RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D, ____Animator_10)); }
	inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * get__Animator_10() const { return ____Animator_10; }
	inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C ** get_address_of__Animator_10() { return &____Animator_10; }
	inline void set__Animator_10(Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * value)
	{
		____Animator_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____Animator_10), (void*)value);
	}

	inline static int32_t get_offset_of__Start_11() { return static_cast<int32_t>(offsetof(RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D, ____Start_11)); }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * get__Start_11() const { return ____Start_11; }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE ** get_address_of__Start_11() { return &____Start_11; }
	inline void set__Start_11(AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * value)
	{
		____Start_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____Start_11), (void*)value);
	}

	inline static int32_t get_offset_of__Stop_12() { return static_cast<int32_t>(offsetof(RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D, ____Stop_12)); }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * get__Stop_12() const { return ____Stop_12; }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE ** get_address_of__Stop_12() { return &____Stop_12; }
	inline void set__Stop_12(AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * value)
	{
		____Stop_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____Stop_12), (void*)value);
	}
};


// NatSuite.Examples.Giffy
struct  Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 NatSuite.Examples.Giffy::imageWidth
	int32_t ___imageWidth_4;
	// System.Int32 NatSuite.Examples.Giffy::imageHeight
	int32_t ___imageHeight_5;
	// System.Single NatSuite.Examples.Giffy::frameDuration
	float ___frameDuration_6;
	// NatSuite.Recorders.GIFRecorder NatSuite.Examples.Giffy::recorder
	GIFRecorder_tE570AFA9B4B6F339A1C9026D6DF39407515F7215 * ___recorder_7;
	// NatSuite.Recorders.Inputs.CameraInput NatSuite.Examples.Giffy::cameraInput
	CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 * ___cameraInput_8;

public:
	inline static int32_t get_offset_of_imageWidth_4() { return static_cast<int32_t>(offsetof(Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4, ___imageWidth_4)); }
	inline int32_t get_imageWidth_4() const { return ___imageWidth_4; }
	inline int32_t* get_address_of_imageWidth_4() { return &___imageWidth_4; }
	inline void set_imageWidth_4(int32_t value)
	{
		___imageWidth_4 = value;
	}

	inline static int32_t get_offset_of_imageHeight_5() { return static_cast<int32_t>(offsetof(Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4, ___imageHeight_5)); }
	inline int32_t get_imageHeight_5() const { return ___imageHeight_5; }
	inline int32_t* get_address_of_imageHeight_5() { return &___imageHeight_5; }
	inline void set_imageHeight_5(int32_t value)
	{
		___imageHeight_5 = value;
	}

	inline static int32_t get_offset_of_frameDuration_6() { return static_cast<int32_t>(offsetof(Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4, ___frameDuration_6)); }
	inline float get_frameDuration_6() const { return ___frameDuration_6; }
	inline float* get_address_of_frameDuration_6() { return &___frameDuration_6; }
	inline void set_frameDuration_6(float value)
	{
		___frameDuration_6 = value;
	}

	inline static int32_t get_offset_of_recorder_7() { return static_cast<int32_t>(offsetof(Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4, ___recorder_7)); }
	inline GIFRecorder_tE570AFA9B4B6F339A1C9026D6DF39407515F7215 * get_recorder_7() const { return ___recorder_7; }
	inline GIFRecorder_tE570AFA9B4B6F339A1C9026D6DF39407515F7215 ** get_address_of_recorder_7() { return &___recorder_7; }
	inline void set_recorder_7(GIFRecorder_tE570AFA9B4B6F339A1C9026D6DF39407515F7215 * value)
	{
		___recorder_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___recorder_7), (void*)value);
	}

	inline static int32_t get_offset_of_cameraInput_8() { return static_cast<int32_t>(offsetof(Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4, ___cameraInput_8)); }
	inline CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 * get_cameraInput_8() const { return ___cameraInput_8; }
	inline CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 ** get_address_of_cameraInput_8() { return &___cameraInput_8; }
	inline void set_cameraInput_8(CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 * value)
	{
		___cameraInput_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameraInput_8), (void*)value);
	}
};


// NatSuite.Examples.ReplayCam
struct  ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 NatSuite.Examples.ReplayCam::videoHeight
	int32_t ___videoHeight_4;
	// System.Boolean NatSuite.Examples.ReplayCam::recordMicrophone
	bool ___recordMicrophone_5;
	// UnityEngine.Camera NatSuite.Examples.ReplayCam::_ARCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ____ARCamera_6;
	// System.Boolean NatSuite.Examples.ReplayCam::CanWrite
	bool ___CanWrite_7;
	// NatSuite.Recorders.IMediaRecorder NatSuite.Examples.ReplayCam::recorder
	RuntimeObject* ___recorder_8;
	// NatSuite.Recorders.Inputs.CameraInput NatSuite.Examples.ReplayCam::cameraInput
	CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 * ___cameraInput_9;
	// NatSuite.Recorders.Inputs.AudioInput NatSuite.Examples.ReplayCam::audioInput
	AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF * ___audioInput_10;
	// UnityEngine.AudioSource NatSuite.Examples.ReplayCam::microphoneSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___microphoneSource_11;
	// UnityEngine.GameObject NatSuite.Examples.ReplayCam::_Outro
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____Outro_12;
	// UnityEngine.GameObject NatSuite.Examples.ReplayCam::_LoadingScreen
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____LoadingScreen_13;

public:
	inline static int32_t get_offset_of_videoHeight_4() { return static_cast<int32_t>(offsetof(ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE, ___videoHeight_4)); }
	inline int32_t get_videoHeight_4() const { return ___videoHeight_4; }
	inline int32_t* get_address_of_videoHeight_4() { return &___videoHeight_4; }
	inline void set_videoHeight_4(int32_t value)
	{
		___videoHeight_4 = value;
	}

	inline static int32_t get_offset_of_recordMicrophone_5() { return static_cast<int32_t>(offsetof(ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE, ___recordMicrophone_5)); }
	inline bool get_recordMicrophone_5() const { return ___recordMicrophone_5; }
	inline bool* get_address_of_recordMicrophone_5() { return &___recordMicrophone_5; }
	inline void set_recordMicrophone_5(bool value)
	{
		___recordMicrophone_5 = value;
	}

	inline static int32_t get_offset_of__ARCamera_6() { return static_cast<int32_t>(offsetof(ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE, ____ARCamera_6)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get__ARCamera_6() const { return ____ARCamera_6; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of__ARCamera_6() { return &____ARCamera_6; }
	inline void set__ARCamera_6(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		____ARCamera_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____ARCamera_6), (void*)value);
	}

	inline static int32_t get_offset_of_CanWrite_7() { return static_cast<int32_t>(offsetof(ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE, ___CanWrite_7)); }
	inline bool get_CanWrite_7() const { return ___CanWrite_7; }
	inline bool* get_address_of_CanWrite_7() { return &___CanWrite_7; }
	inline void set_CanWrite_7(bool value)
	{
		___CanWrite_7 = value;
	}

	inline static int32_t get_offset_of_recorder_8() { return static_cast<int32_t>(offsetof(ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE, ___recorder_8)); }
	inline RuntimeObject* get_recorder_8() const { return ___recorder_8; }
	inline RuntimeObject** get_address_of_recorder_8() { return &___recorder_8; }
	inline void set_recorder_8(RuntimeObject* value)
	{
		___recorder_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___recorder_8), (void*)value);
	}

	inline static int32_t get_offset_of_cameraInput_9() { return static_cast<int32_t>(offsetof(ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE, ___cameraInput_9)); }
	inline CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 * get_cameraInput_9() const { return ___cameraInput_9; }
	inline CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 ** get_address_of_cameraInput_9() { return &___cameraInput_9; }
	inline void set_cameraInput_9(CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655 * value)
	{
		___cameraInput_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameraInput_9), (void*)value);
	}

	inline static int32_t get_offset_of_audioInput_10() { return static_cast<int32_t>(offsetof(ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE, ___audioInput_10)); }
	inline AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF * get_audioInput_10() const { return ___audioInput_10; }
	inline AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF ** get_address_of_audioInput_10() { return &___audioInput_10; }
	inline void set_audioInput_10(AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF * value)
	{
		___audioInput_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioInput_10), (void*)value);
	}

	inline static int32_t get_offset_of_microphoneSource_11() { return static_cast<int32_t>(offsetof(ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE, ___microphoneSource_11)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_microphoneSource_11() const { return ___microphoneSource_11; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_microphoneSource_11() { return &___microphoneSource_11; }
	inline void set_microphoneSource_11(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___microphoneSource_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___microphoneSource_11), (void*)value);
	}

	inline static int32_t get_offset_of__Outro_12() { return static_cast<int32_t>(offsetof(ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE, ____Outro_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__Outro_12() const { return ____Outro_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__Outro_12() { return &____Outro_12; }
	inline void set__Outro_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____Outro_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____Outro_12), (void*)value);
	}

	inline static int32_t get_offset_of__LoadingScreen_13() { return static_cast<int32_t>(offsetof(ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE, ____LoadingScreen_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__LoadingScreen_13() const { return ____LoadingScreen_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__LoadingScreen_13() { return &____LoadingScreen_13; }
	inline void set__LoadingScreen_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____LoadingScreen_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____LoadingScreen_13), (void*)value);
	}
};


// NatSuite.Recorders.Inputs.AudioInput/AudioInputAttachment
struct  AudioInputAttachment_tBD4C16D24B6BB510C28C62437F470268DD97F38B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action`1<System.Single[]> NatSuite.Recorders.Inputs.AudioInput/AudioInputAttachment::sampleBufferDelegate
	Action_1_tF14E2B60916B5AA685432504A9BC1036ED6AAB84 * ___sampleBufferDelegate_4;

public:
	inline static int32_t get_offset_of_sampleBufferDelegate_4() { return static_cast<int32_t>(offsetof(AudioInputAttachment_tBD4C16D24B6BB510C28C62437F470268DD97F38B, ___sampleBufferDelegate_4)); }
	inline Action_1_tF14E2B60916B5AA685432504A9BC1036ED6AAB84 * get_sampleBufferDelegate_4() const { return ___sampleBufferDelegate_4; }
	inline Action_1_tF14E2B60916B5AA685432504A9BC1036ED6AAB84 ** get_address_of_sampleBufferDelegate_4() { return &___sampleBufferDelegate_4; }
	inline void set_sampleBufferDelegate_4(Action_1_tF14E2B60916B5AA685432504A9BC1036ED6AAB84 * value)
	{
		___sampleBufferDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sampleBufferDelegate_4), (void*)value);
	}
};


// NatSuite.Recorders.Inputs.CameraInput/CameraInputAttachment
struct  CameraInputAttachment_t415800A4510BE48007515F8FA8201B5FCD986328  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5679;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5679 = { sizeof (JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5679[4] = 
{
	JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7::get_offset_of_framebuffer_0(),
	JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7::get_offset_of_recordingPath_1(),
	JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7::get_offset_of_writeTasks_2(),
	JPGRecorder_t3235513B5885AA31376347BA45E11148F13880B7::get_offset_of_frameCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5680;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5680 = { sizeof (U3CU3Ec__DisplayClass4_0_tDA27A42B0978F6B299DA38B87701B7E7D4EA25ED), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5680[3] = 
{
	U3CU3Ec__DisplayClass4_0_tDA27A42B0978F6B299DA38B87701B7E7D4EA25ED::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass4_0_tDA27A42B0978F6B299DA38B87701B7E7D4EA25ED::get_offset_of_frameIndex_1(),
	U3CU3Ec__DisplayClass4_0_tDA27A42B0978F6B299DA38B87701B7E7D4EA25ED::get_offset_of_frameData_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5681;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5681 = { sizeof (MP4Recorder_tBF21F6E0118C545DB1DF7089E708E1B4BB85492A), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5681[1] = 
{
	MP4Recorder_tBF21F6E0118C545DB1DF7089E708E1B4BB85492A::get_offset_of_recorder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5682;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5682 = { sizeof (U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5682[7] = 
{
	U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA::get_offset_of_width_0(),
	U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA::get_offset_of_height_1(),
	U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA::get_offset_of_frameRate_2(),
	U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA::get_offset_of_bitrate_3(),
	U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA::get_offset_of_keyframeInterval_4(),
	U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA::get_offset_of_sampleRate_5(),
	U3CU3Ec__DisplayClass2_0_tF43AE2522E761DFB6202DEE84B9520E4555573BA::get_offset_of_channelCount_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5683;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5683 = { sizeof (WAVRecorder_tFCA0A292E50A43554D1DE1EE220B9F4A81BFE266), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5683[4] = 
{
	WAVRecorder_tFCA0A292E50A43554D1DE1EE220B9F4A81BFE266::get_offset_of_sampleRate_0(),
	WAVRecorder_tFCA0A292E50A43554D1DE1EE220B9F4A81BFE266::get_offset_of_channelCount_1(),
	WAVRecorder_tFCA0A292E50A43554D1DE1EE220B9F4A81BFE266::get_offset_of_stream_2(),
	WAVRecorder_tFCA0A292E50A43554D1DE1EE220B9F4A81BFE266::get_offset_of_sampleCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5684;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5684 = { sizeof (Bridge_t72201E3009ACF44D05451C0D299A20DBE9283230), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5684[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5685;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5685 = { sizeof (RecordingHandler_t4D3C0E8013CFB4506D712CB5C9AB047B3D82A329), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5686;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5686 = { sizeof (NativeRecorder_tB0869D7A0DFF8328DD1BEEC8E664CE1C57DCB624), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5686[2] = 
{
	NativeRecorder_tB0869D7A0DFF8328DD1BEEC8E664CE1C57DCB624::get_offset_of_recorder_0(),
	NativeRecorder_tB0869D7A0DFF8328DD1BEEC8E664CE1C57DCB624::get_offset_of_recordingTask_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5687;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5687 = { sizeof (Utility_tEED46C1B403EC0D670D68290D5D83957973E203D), -1, sizeof(Utility_tEED46C1B403EC0D670D68290D5D83957973E203D_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5687[1] = 
{
	Utility_tEED46C1B403EC0D670D68290D5D83957973E203D_StaticFields::get_offset_of_directory_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5688;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5688 = { sizeof (AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5688[4] = 
{
	AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF::get_offset_of_recorder_0(),
	AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF::get_offset_of_clock_1(),
	AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF::get_offset_of_attachment_2(),
	AudioInput_t63B28DB00E4B9928C39404FE1927019D072BABBF::get_offset_of_mute_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5689;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5689 = { sizeof (AudioInputAttachment_tBD4C16D24B6BB510C28C62437F470268DD97F38B), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5689[1] = 
{
	AudioInputAttachment_tBD4C16D24B6BB510C28C62437F470268DD97F38B::get_offset_of_sampleBufferDelegate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5690;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5690 = { sizeof (CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5690[9] = 
{
	CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655::get_offset_of_frameSkip_0(),
	CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655::get_offset_of_recorder_1(),
	CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655::get_offset_of_clock_2(),
	CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655::get_offset_of_cameras_3(),
	CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655::get_offset_of_frameDescriptor_4(),
	CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655::get_offset_of_readbackBuffer_5(),
	CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655::get_offset_of_attachment_6(),
	CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655::get_offset_of_pixelBuffer_7(),
	CameraInput_t9BCA45F18EBA5CE8E2AC580FF855842144CFB655::get_offset_of_frameCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5691;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5691 = { sizeof (CameraInputAttachment_t415800A4510BE48007515F8FA8201B5FCD986328), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5692;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5692 = { sizeof (U3CU3Ec_tC0939739735059A80353C8C1C499B9E1BFF18280), -1, sizeof(U3CU3Ec_tC0939739735059A80353C8C1C499B9E1BFF18280_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5692[2] = 
{
	U3CU3Ec_tC0939739735059A80353C8C1C499B9E1BFF18280_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tC0939739735059A80353C8C1C499B9E1BFF18280_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5693;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5693 = { sizeof (U3CU3Ec__DisplayClass11_0_t19832433766A7855A211E71E6FD20B6C30613D2C), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5693[2] = 
{
	U3CU3Ec__DisplayClass11_0_t19832433766A7855A211E71E6FD20B6C30613D2C::get_offset_of_timestamp_0(),
	U3CU3Ec__DisplayClass11_0_t19832433766A7855A211E71E6FD20B6C30613D2C::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5694;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5694 = { sizeof (U3COnFrameU3Ed__11_tB518C4F529E311E9780F6FF80E33F41D7F927375), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5694[5] = 
{
	U3COnFrameU3Ed__11_tB518C4F529E311E9780F6FF80E33F41D7F927375::get_offset_of_U3CU3E1__state_0(),
	U3COnFrameU3Ed__11_tB518C4F529E311E9780F6FF80E33F41D7F927375::get_offset_of_U3CU3E2__current_1(),
	U3COnFrameU3Ed__11_tB518C4F529E311E9780F6FF80E33F41D7F927375::get_offset_of_U3CU3E4__this_2(),
	U3COnFrameU3Ed__11_tB518C4F529E311E9780F6FF80E33F41D7F927375::get_offset_of_U3CU3E8__1_3(),
	U3COnFrameU3Ed__11_tB518C4F529E311E9780F6FF80E33F41D7F927375::get_offset_of_U3CendOfFrameU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5695;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5695 = { sizeof (FixedIntervalClock_t9B5E58E18BEAA393B60708928FC6ABFA2B001820), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5695[3] = 
{
	FixedIntervalClock_t9B5E58E18BEAA393B60708928FC6ABFA2B001820::get_offset_of_U3CintervalU3Ek__BackingField_0(),
	FixedIntervalClock_t9B5E58E18BEAA393B60708928FC6ABFA2B001820::get_offset_of_autoTick_1(),
	FixedIntervalClock_t9B5E58E18BEAA393B60708928FC6ABFA2B001820::get_offset_of_ticks_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5696;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5696 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5697;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5697 = { sizeof (RealtimeClock_tD38272D4375095BF717959322C645000A0C954ED), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5697[1] = 
{
	RealtimeClock_tD38272D4375095BF717959322C645000A0C954ED::get_offset_of_stopwatch_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5698;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5698 = { sizeof (Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5698[5] = 
{
	Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4::get_offset_of_imageWidth_4(),
	Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4::get_offset_of_imageHeight_5(),
	Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4::get_offset_of_frameDuration_6(),
	Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4::get_offset_of_recorder_7(),
	Giffy_t8EAB1F1EDB06B15B0BC470AA4DADA82154F7E1A4::get_offset_of_cameraInput_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5699;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5699 = { sizeof (U3CStopRecordingU3Ed__6_t6831C9609F3B520123F95F9DC34C80ADEDC45F16)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5699[4] = 
{
	U3CStopRecordingU3Ed__6_t6831C9609F3B520123F95F9DC34C80ADEDC45F16::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStopRecordingU3Ed__6_t6831C9609F3B520123F95F9DC34C80ADEDC45F16::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStopRecordingU3Ed__6_t6831C9609F3B520123F95F9DC34C80ADEDC45F16::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStopRecordingU3Ed__6_t6831C9609F3B520123F95F9DC34C80ADEDC45F16::get_offset_of_U3CU3Eu__1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5700;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5700 = { sizeof (ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5700[10] = 
{
	ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE::get_offset_of_videoHeight_4(),
	ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE::get_offset_of_recordMicrophone_5(),
	ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE::get_offset_of__ARCamera_6(),
	ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE::get_offset_of_CanWrite_7(),
	ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE::get_offset_of_recorder_8(),
	ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE::get_offset_of_cameraInput_9(),
	ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE::get_offset_of_audioInput_10(),
	ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE::get_offset_of_microphoneSource_11(),
	ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE::get_offset_of__Outro_12(),
	ReplayCam_t51C9F76668DE4A91212B6D19F6206A34D95550FE::get_offset_of__LoadingScreen_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5701;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5701 = { sizeof (U3CU3Ec_t6A10C091B0B7F58EACA4EF20DE59E51FB30A338B), -1, sizeof(U3CU3Ec_t6A10C091B0B7F58EACA4EF20DE59E51FB30A338B_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5701[2] = 
{
	U3CU3Ec_t6A10C091B0B7F58EACA4EF20DE59E51FB30A338B_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6A10C091B0B7F58EACA4EF20DE59E51FB30A338B_StaticFields::get_offset_of_U3CU3E9__10_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5702;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5702 = { sizeof (U3CStartU3Ed__10_tDE1CC8D7C86FD20D03525680F18B0D1A1018225C), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5702[3] = 
{
	U3CStartU3Ed__10_tDE1CC8D7C86FD20D03525680F18B0D1A1018225C::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__10_tDE1CC8D7C86FD20D03525680F18B0D1A1018225C::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__10_tDE1CC8D7C86FD20D03525680F18B0D1A1018225C::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5703;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5703 = { sizeof (U3CFinishRecordingU3Ed__14_t356A404CC498364A41990D36CCE2DF7610E7B4ED)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5703[4] = 
{
	U3CFinishRecordingU3Ed__14_t356A404CC498364A41990D36CCE2DF7610E7B4ED::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CFinishRecordingU3Ed__14_t356A404CC498364A41990D36CCE2DF7610E7B4ED::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CFinishRecordingU3Ed__14_t356A404CC498364A41990D36CCE2DF7610E7B4ED::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CFinishRecordingU3Ed__14_t356A404CC498364A41990D36CCE2DF7610E7B4ED::get_offset_of_U3CU3Eu__1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5704;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5704 = { sizeof (CameraPreview_t14366C64F8D63434B92D4F4DE771EDEE07EAE89C), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5704[3] = 
{
	CameraPreview_t14366C64F8D63434B92D4F4DE771EDEE07EAE89C::get_offset_of_U3CcameraTextureU3Ek__BackingField_4(),
	CameraPreview_t14366C64F8D63434B92D4F4DE771EDEE07EAE89C::get_offset_of_rawImage_5(),
	CameraPreview_t14366C64F8D63434B92D4F4DE771EDEE07EAE89C::get_offset_of_aspectFitter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5705;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5705 = { sizeof (U3CU3Ec_t2D6BAFF26CEBC190E8316071402B17B5D9C4E5D6), -1, sizeof(U3CU3Ec_t2D6BAFF26CEBC190E8316071402B17B5D9C4E5D6_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5705[2] = 
{
	U3CU3Ec_t2D6BAFF26CEBC190E8316071402B17B5D9C4E5D6_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2D6BAFF26CEBC190E8316071402B17B5D9C4E5D6_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5706;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5706 = { sizeof (U3CStartU3Ed__6_t12D537C13EA3027063A6CB61F4714C598B9F2D43), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5706[3] = 
{
	U3CStartU3Ed__6_t12D537C13EA3027063A6CB61F4714C598B9F2D43::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__6_t12D537C13EA3027063A6CB61F4714C598B9F2D43::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__6_t12D537C13EA3027063A6CB61F4714C598B9F2D43::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5707;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5707 = { sizeof (RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5707[9] = 
{
	RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D::get_offset_of_button_4(),
	RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D::get_offset_of_countdown_5(),
	RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D::get_offset_of_onTouchDown_6(),
	RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D::get_offset_of_onTouchUp_7(),
	RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D::get_offset_of_pressed_8(),
	0,
	RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D::get_offset_of__Animator_10(),
	RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D::get_offset_of__Start_11(),
	RecordButton_tA155E9932279A15071302B11FEFE350B53E7967D::get_offset_of__Stop_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5708;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5708 = { sizeof (U3CCountdownU3Ed__13_t1BB74FE43DC79072927C6AC6C880E97878B9D413), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5708[4] = 
{
	U3CCountdownU3Ed__13_t1BB74FE43DC79072927C6AC6C880E97878B9D413::get_offset_of_U3CU3E1__state_0(),
	U3CCountdownU3Ed__13_t1BB74FE43DC79072927C6AC6C880E97878B9D413::get_offset_of_U3CU3E2__current_1(),
	U3CCountdownU3Ed__13_t1BB74FE43DC79072927C6AC6C880E97878B9D413::get_offset_of_U3CU3E4__this_2(),
	U3CCountdownU3Ed__13_t1BB74FE43DC79072927C6AC6C880E97878B9D413::get_offset_of_U3CstartTimeU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5709;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5709 = { sizeof (LeanDummy_tF5204357A79E55834CC8269D24B3CF3CB87D07B9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5710;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5710 = { sizeof (PathBezier_tE51A8452CDA44A02AB58E56D6F880E81512557D9), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5710[4] = 
{
	PathBezier_tE51A8452CDA44A02AB58E56D6F880E81512557D9::get_offset_of_trans_4(),
	PathBezier_tE51A8452CDA44A02AB58E56D6F880E81512557D9::get_offset_of_cr_5(),
	PathBezier_tE51A8452CDA44A02AB58E56D6F880E81512557D9::get_offset_of_avatar1_6(),
	PathBezier_tE51A8452CDA44A02AB58E56D6F880E81512557D9::get_offset_of_iter_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5711;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5711 = { sizeof (TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5711[21] = 
{
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_cube1_4(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_cube2_5(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_cube3_6(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_cube4_7(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_cubeAlpha1_8(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_cubeAlpha2_9(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_eventGameObjectWasCalled_10(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_eventGeneralWasCalled_11(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_lt1Id_12(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_lt2_13(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_lt3_14(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_lt4_15(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_groupTweens_16(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_groupGOs_17(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_groupTweensCnt_18(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_rotateRepeat_19(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_rotateRepeatAngle_20(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_boxNoCollider_21(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_timeElapsedNormalTimeScale_22(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_timeElapsedIgnoreTimeScale_23(),
	TestingUnitTests_t35438115B3A90BC273276E879EB3306ADE5165FB::get_offset_of_pauseTweenDidFinish_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5712;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5712 = { sizeof (U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5712[21] = 
{
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_cubes_1(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_tweenIds_2(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_onCompleteCount_3(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_cubeToTrans_4(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_cubeDestEnd_5(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_cubeSpline_6(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_jumpTimeId_7(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_jumpCube_8(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_zeroCube_9(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_cubeScale_10(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_cubeRotate_11(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_cubeRotateA_12(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_cubeRotateB_13(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_onStartTime_14(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_beforePos_15(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_beforePos2_16(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_totalEasingCheck_17(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_totalEasingCheckSuccess_18(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_value2UpdateCalled_19(),
	U3CU3Ec__DisplayClass22_0_t35B8217F92EB3F7480EB103267472C6C7A211396::get_offset_of_U3CU3E9__21_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5713;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5713 = { sizeof (U3CU3Ec__DisplayClass22_1_tCE05255B60D85F5EF3F3593FD20544CE1C8805AA), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5713[2] = 
{
	U3CU3Ec__DisplayClass22_1_tCE05255B60D85F5EF3F3593FD20544CE1C8805AA::get_offset_of_beforeX_0(),
	U3CU3Ec__DisplayClass22_1_tCE05255B60D85F5EF3F3593FD20544CE1C8805AA::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5714;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5714 = { sizeof (U3CU3Ec__DisplayClass22_2_tBC56C65485DAFCD7F524F15767C83EF35620F245), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5714[3] = 
{
	U3CU3Ec__DisplayClass22_2_tBC56C65485DAFCD7F524F15767C83EF35620F245::get_offset_of_totalTweenTypeLength_0(),
	U3CU3Ec__DisplayClass22_2_tBC56C65485DAFCD7F524F15767C83EF35620F245::get_offset_of_CSU24U3CU3E8__locals2_1(),
	U3CU3Ec__DisplayClass22_2_tBC56C65485DAFCD7F524F15767C83EF35620F245::get_offset_of_U3CU3E9__24_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5715;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5715 = { sizeof (U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4), -1, sizeof(U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5715[7] = 
{
	U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields::get_offset_of_U3CU3E9__22_3_1(),
	U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields::get_offset_of_U3CU3E9__22_22_2(),
	U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields::get_offset_of_U3CU3E9__22_7_3(),
	U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields::get_offset_of_U3CU3E9__22_12_4(),
	U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields::get_offset_of_U3CU3E9__22_18_5(),
	U3CU3Ec_t7005C3BB59DC3ECD8E7B3A78A33D5C7790797EA4_StaticFields::get_offset_of_U3CU3E9__26_0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5716;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5716 = { sizeof (U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5716[23] = 
{
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_pauseCount_1(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_cubeRound_2(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_onStartPos_3(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_onStartPosSpline_4(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_cubeSpline_5(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_cubeSeq_6(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_cubeBounds_7(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_didPassBounds_8(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_failPoint_9(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_setOnStartNum_10(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_setPosOnUpdate_11(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_setPosNum_12(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_hasGroupTweensCheckStarted_13(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_previousXlt4_14(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_onUpdateWasCalled_15(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_start_16(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_expectedTime_17(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_didGetCorrectOnUpdate_18(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_U3CU3E9__13_19(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_U3CU3E9__14_20(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_U3CU3E9__16_21(),
	U3CU3Ec__DisplayClass24_0_tE645E7D116D067CB442D43894FEA966052A8FF97::get_offset_of_U3CU3E9__15_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5717;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5717 = { sizeof (U3CtimeBasedTestingU3Ed__24_t595811661E70EDE9543C16FFE5F39DCB9FA9F5A4), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5717[5] = 
{
	U3CtimeBasedTestingU3Ed__24_t595811661E70EDE9543C16FFE5F39DCB9FA9F5A4::get_offset_of_U3CU3E1__state_0(),
	U3CtimeBasedTestingU3Ed__24_t595811661E70EDE9543C16FFE5F39DCB9FA9F5A4::get_offset_of_U3CU3E2__current_1(),
	U3CtimeBasedTestingU3Ed__24_t595811661E70EDE9543C16FFE5F39DCB9FA9F5A4::get_offset_of_U3CU3E4__this_2(),
	U3CtimeBasedTestingU3Ed__24_t595811661E70EDE9543C16FFE5F39DCB9FA9F5A4::get_offset_of_U3CU3E8__1_3(),
	U3CtimeBasedTestingU3Ed__24_t595811661E70EDE9543C16FFE5F39DCB9FA9F5A4::get_offset_of_U3CdescriptionMatchCountU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5718;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5718 = { sizeof (U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5718[8] = 
{
	U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71::get_offset_of_U3CU3E1__state_0(),
	U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71::get_offset_of_U3CU3E2__current_1(),
	U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71::get_offset_of_U3CU3E4__this_2(),
	U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71::get_offset_of_U3CcubeCountU3E5__2_3(),
	U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71::get_offset_of_U3CtweensAU3E5__3_4(),
	U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71::get_offset_of_U3CaGOsU3E5__4_5(),
	U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71::get_offset_of_U3CtweensBU3E5__5_6(),
	U3ClotsOfCancelsU3Ed__25_t685E74B9E42BD99FFC112B4FCFCCCC82E7076E71::get_offset_of_U3CbGOsU3E5__6_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5719;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5719 = { sizeof (U3CpauseTimeNowU3Ed__26_tFB7F0244BCF4168EB299472ADE265C9E924DD28E), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5719[3] = 
{
	U3CpauseTimeNowU3Ed__26_tFB7F0244BCF4168EB299472ADE265C9E924DD28E::get_offset_of_U3CU3E1__state_0(),
	U3CpauseTimeNowU3Ed__26_tFB7F0244BCF4168EB299472ADE265C9E924DD28E::get_offset_of_U3CU3E2__current_1(),
	U3CpauseTimeNowU3Ed__26_tFB7F0244BCF4168EB299472ADE265C9E924DD28E::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5720;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5720 = { sizeof (MpegVersion_tF02D5976231BDBAE02A806210351177E5619911F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5720[5] = 
{
	MpegVersion_tF02D5976231BDBAE02A806210351177E5619911F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5721;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5721 = { sizeof (MpegLayer_t6B18823B84C63A924C8DBE88AC67E59BCFDD7460)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5721[5] = 
{
	MpegLayer_t6B18823B84C63A924C8DBE88AC67E59BCFDD7460::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5722;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5722 = { sizeof (MpegChannelMode_t74BC5F302EBB6E8C26940521EC4EB53607A61D66)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5722[5] = 
{
	MpegChannelMode_t74BC5F302EBB6E8C26940521EC4EB53607A61D66::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5723;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5723 = { sizeof (StereoMode_t31B767D6A76DB640B68A3D2F0DE974C479E4C701)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5723[5] = 
{
	StereoMode_t31B767D6A76DB640B68A3D2F0DE974C479E4C701::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5724;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5724 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5725;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5725 = { sizeof (MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5725[10] = 
{
	MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC::get_offset_of__stream_0(),
	MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC::get_offset_of__closeStream_1(),
	MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC::get_offset_of__eofFound_2(),
	MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC::get_offset_of__reader_3(),
	MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC::get_offset_of__decoder_4(),
	MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC::get_offset_of__seekLock_5(),
	MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC::get_offset_of__position_6(),
	MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC::get_offset_of__readBuf_7(),
	MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC::get_offset_of__readBufLen_8(),
	MpegFile_tD60FBD28FA83EE9CF37C251C996B3BFB486718AC::get_offset_of__readBufOfs_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5726;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5726 = { sizeof (MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5726[7] = 
{
	MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2::get_offset_of__layerIDecoder_0(),
	MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2::get_offset_of__layerIIDecoder_1(),
	MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2::get_offset_of__layerIIIDecoder_2(),
	MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2::get_offset_of__eqFactors_3(),
	MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2::get_offset_of__ch0_4(),
	MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2::get_offset_of__ch1_5(),
	MpegFrameDecoder_t0EDE3035A18A1AE15D32009400BB34FCD614C0A2::get_offset_of_U3CStereoModeU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5727;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5727 = { sizeof (BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5727[5] = 
{
	BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB::get_offset_of__buf_0(),
	BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB::get_offset_of__start_1(),
	BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB::get_offset_of__end_2(),
	BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB::get_offset_of__bitsLeft_3(),
	BitReservoir_t6C77CAFE7734B5319CE02E0A896AEEA3D0C1B5DB::get_offset_of__bitsRead_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5728;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5728 = { sizeof (FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA), -1, sizeof(FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5728[5] = 
{
	FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA_StaticFields::get_offset_of__totalAllocation_0(),
	FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA::get_offset_of_U3COffsetU3Ek__BackingField_1(),
	FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA::get_offset_of_U3CLengthU3Ek__BackingField_2(),
	FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA::get_offset_of__reader_3(),
	FrameBase_t3A1AB5F1CF95CCF18C2EF7D21E320A0F73CCE3FA::get_offset_of__savedBuffer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5729;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5729 = { sizeof (Huffman_t45D572D2B5E0A0C54E838E4FE31EA44E00535CFA), -1, sizeof(Huffman_t45D572D2B5E0A0C54E838E4FE31EA44E00535CFA_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5729[5] = 
{
	Huffman_t45D572D2B5E0A0C54E838E4FE31EA44E00535CFA_StaticFields::get_offset_of__codeTables_0(),
	Huffman_t45D572D2B5E0A0C54E838E4FE31EA44E00535CFA_StaticFields::get_offset_of__floatLookup_1(),
	Huffman_t45D572D2B5E0A0C54E838E4FE31EA44E00535CFA_StaticFields::get_offset_of__llCache_2(),
	Huffman_t45D572D2B5E0A0C54E838E4FE31EA44E00535CFA_StaticFields::get_offset_of__llCacheMaxBits_3(),
	Huffman_t45D572D2B5E0A0C54E838E4FE31EA44E00535CFA_StaticFields::get_offset_of_LIN_BITS_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5730;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5730 = { sizeof (HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5730[5] = 
{
	HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2::get_offset_of_Value_0(),
	HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2::get_offset_of_Length_1(),
	HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2::get_offset_of_Bits_2(),
	HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2::get_offset_of_Mask_3(),
	HuffmanListNode_tD22EC3E9E11A534213D4D6F7591F4087E82974E2::get_offset_of_Next_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5731;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5731 = { sizeof (U3CU3Ec_t8C529531DE57E8ACB6F5FB010DFAAB9E682BBA9C), -1, sizeof(U3CU3Ec_t8C529531DE57E8ACB6F5FB010DFAAB9E682BBA9C_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5731[2] = 
{
	U3CU3Ec_t8C529531DE57E8ACB6F5FB010DFAAB9E682BBA9C_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t8C529531DE57E8ACB6F5FB010DFAAB9E682BBA9C_StaticFields::get_offset_of_U3CU3E9__12_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5732;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5732 = { sizeof (ID3Frame_tAB95743953BDF6FD6810C1AB92F25841D41135A8), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5732[1] = 
{
	ID3Frame_tAB95743953BDF6FD6810C1AB92F25841D41135A8::get_offset_of__version_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5733;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5733 = { sizeof (LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B), -1, sizeof(LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5733[21] = 
{
	0,
	0,
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B_StaticFields::get_offset_of_DEWINDOW_TABLE_2(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B_StaticFields::get_offset_of_SYNTH_COS64_TABLE_3(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of__synBuf_4(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of__bufOffset_5(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of__eq_6(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of_U3CStereoModeU3Ek__BackingField_7(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of_ippuv_8(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of_ei32_9(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of_eo32_10(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of_oi32_11(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of_oo32_12(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of_ei16_13(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of_eo16_14(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of_oi16_15(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of_oo16_16(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of_ei8_17(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of_tmp8_18(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of_oi8_19(),
	LayerDecoderBase_t34551FEAB4A3C5193FC94F47E5C8BE5DA369F48B::get_offset_of_oo8_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5734;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5734 = { sizeof (LayerIDecoder_t29E88F178B1D31CBFCDBF7BF51304FED69AF8D55), -1, sizeof(LayerIDecoder_t29E88F178B1D31CBFCDBF7BF51304FED69AF8D55_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5734[2] = 
{
	LayerIDecoder_t29E88F178B1D31CBFCDBF7BF51304FED69AF8D55_StaticFields::get_offset_of__rateTable_36(),
	LayerIDecoder_t29E88F178B1D31CBFCDBF7BF51304FED69AF8D55_StaticFields::get_offset_of__allocLookupTable_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5735;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5735 = { sizeof (LayerIIDecoder_t06D951DC8F7021B2CF4D70FCE55297ECFE87ABFE), -1, sizeof(LayerIIDecoder_t06D951DC8F7021B2CF4D70FCE55297ECFE87ABFE_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5735[2] = 
{
	LayerIIDecoder_t06D951DC8F7021B2CF4D70FCE55297ECFE87ABFE_StaticFields::get_offset_of__rateLookupTable_36(),
	LayerIIDecoder_t06D951DC8F7021B2CF4D70FCE55297ECFE87ABFE_StaticFields::get_offset_of__allocLookupTable_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5736;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5736 = { sizeof (LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF), -1, sizeof(LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5736[15] = 
{
	0,
	LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF_StaticFields::get_offset_of__groupedC_22(),
	LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF_StaticFields::get_offset_of__groupedD_23(),
	LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF_StaticFields::get_offset_of__C_24(),
	LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF_StaticFields::get_offset_of__D_25(),
	LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF_StaticFields::get_offset_of__denormalMultiplier_26(),
	LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF::get_offset_of__channels_27(),
	LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF::get_offset_of__jsbound_28(),
	LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF::get_offset_of__granuleCount_29(),
	LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF::get_offset_of__allocLookupTable_30(),
	LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF::get_offset_of__scfsi_31(),
	LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF::get_offset_of__samples_32(),
	LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF::get_offset_of__scalefac_33(),
	LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF::get_offset_of__polyPhaseBuf_34(),
	LayerIIDecoderBase_tE302A060F265083109178DD2360E23CAC17CC7CF::get_offset_of__allocation_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5737;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5737 = { sizeof (LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1), -1, sizeof(LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5737[42] = 
{
	0,
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__hybrid_22(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__bitRes_23(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__channels_24(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__privBits_25(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__mainDataBegin_26(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__scfsi_27(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__part23Length_28(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__bigValues_29(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__globalGain_30(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__scalefacCompress_31(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__blockSplitFlag_32(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__mixedBlockFlag_33(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__blockType_34(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__tableSelect_35(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__subblockGain_36(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__regionAddress1_37(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__regionAddress2_38(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__preflag_39(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__scalefacScale_40(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__count1TableSelect_41(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields::get_offset_of_GAIN_TAB_42(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__sfBandIndexL_43(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__sfBandIndexS_44(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__cbLookupL_45(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__cbLookupS_46(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__cbwLookupS_47(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__cbLookupSR_48(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields::get_offset_of__sfBandIndexLTable_49(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields::get_offset_of__sfBandIndexSTable_50(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__scalefac_51(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields::get_offset_of__slen_52(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields::get_offset_of__sfbBlockCntTab_53(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__samples_54(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields::get_offset_of_PRETAB_55(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields::get_offset_of_POW2_TAB_56(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields::get_offset_of__isRatio_57(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields::get_offset_of__lsfRatio_58(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__reorderBuf_59(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields::get_offset_of__scs_60(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1_StaticFields::get_offset_of__sca_61(),
	LayerIIIDecoder_t86F56778A0327DA5F333ADD648909A334AF185E1::get_offset_of__polyPhase_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5738;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5738 = { sizeof (HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878), -1, sizeof(HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5738[8] = 
{
	0,
	HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878_StaticFields::get_offset_of__swin_1(),
	HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878_StaticFields::get_offset_of_icos72_table_2(),
	HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878::get_offset_of__prevBlock_3(),
	HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878::get_offset_of__nextBlock_4(),
	HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878::get_offset_of__imdctTemp_5(),
	HybridMDCT_tF9691D97EF83291CF5BC1A65B53933FF1C249878::get_offset_of__imdctResult_6(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5739;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5739 = { sizeof (MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51), -1, sizeof(MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5739[9] = 
{
	MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51_StaticFields::get_offset_of__bitRateTable_5(),
	MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51::get_offset_of_Next_6(),
	MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51::get_offset_of_Number_7(),
	MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51::get_offset_of__syncBits_8(),
	MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51::get_offset_of__readOffset_9(),
	MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51::get_offset_of__bitsRead_10(),
	MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51::get_offset_of__bitBucket_11(),
	MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51::get_offset_of__offset_12(),
	MpegFrame_t978911EA53BE165E985AA6384EC97C070401CF51::get_offset_of__isMuted_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5740;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5740 = { sizeof (MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5740[17] = 
{
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__id3Frame_0(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__id3v1Frame_1(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__riffHeaderFrame_2(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__vbrInfo_3(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__first_4(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__current_5(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__last_6(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__lastFree_7(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__readOffset_8(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__eofOffset_9(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__source_10(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__canSeek_11(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__endFound_12(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__mixedFrameSize_13(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__readLock_14(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__frameLock_15(),
	MpegStreamReader_tFBFDE6238BF5C77542EA5125F5E577FF9CCB6E40::get_offset_of__readBuf_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5741;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5741 = { sizeof (ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5741[5] = 
{
	ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB::get_offset_of_Data_0(),
	ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB::get_offset_of_BaseOffset_1(),
	ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB::get_offset_of_End_2(),
	ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB::get_offset_of_DiscardCount_3(),
	ReadBuffer_t1F4EBAF7591F6E9C7CB69372F128AABCAC7FEBBB::get_offset_of__localLock_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5742;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5742 = { sizeof (RiffHeaderFrame_tD195C40825041F5DA7AE518B989C8EEEBB9CAC8C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5743;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5743 = { sizeof (VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5743[7] = 
{
	VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428::get_offset_of_U3CSampleCountU3Ek__BackingField_0(),
	VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428::get_offset_of_U3CSampleRateU3Ek__BackingField_1(),
	VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428::get_offset_of_U3CChannelsU3Ek__BackingField_2(),
	VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428::get_offset_of_U3CVBRFramesU3Ek__BackingField_3(),
	VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428::get_offset_of_U3CVBRBytesU3Ek__BackingField_4(),
	VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428::get_offset_of_U3CVBRQualityU3Ek__BackingField_5(),
	VBRInfo_t33C16EFA0E88149524B708A111DECAD76DE54428::get_offset_of_U3CVBRDelayU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5744;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5744 = { sizeof (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A), -1, sizeof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable5744[124] = 
{
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U30183F51B580B1EAF92C43990FB8C2AD83C313BF8_0(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U302DECBD4B98B83C88DF26F1D316D967144C964AC_1(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U3052C20430DC5407CAC4A7E36BAF553EAFE144CDE_2(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U3056EDD3469AB29404C03DFFED0CC7D751CC62ABF_3(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U306A66430C02C24BB46E9D45096F3612F52BAAAAD_4(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U306D0A13FF2634CA2D099C7C996C6B3B2CDC71A27_5(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U30CA9EF2C32AFFE256210DE1874B3FD80A301DD44_6(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U30D7F9193AF20C2D7369CE21411B3F4B96169D931_7(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U30DD7A9B3BA80B5B1E2B69AAA2DFC56CFEEE8F3CD_8(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U30E24731E028D4046A165EA260DA1DA95BF0201B5_9(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U30F53B5E6268C6545E9270A0169A1FA196C6092DF_10(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U310CE4BD8D40A44E534A61F607B710FF1950B246C_11(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U3117ED604063D78A294769B182459666034519D79_12(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U312976064F8F8160F188A01AEEBCD007449BD365B_13(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U3167CE10EAB7DBB2FAC19002E7CB7F7E81D63F341_14(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U31766F40539CB527A97DEE60B6B1AD5D7559C3EED_15(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U31EED620D880B5566ADF1E6D57BE501A951D55057_16(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U31F9B007B0BF8944263622345602943223197642D_17(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U324CD06EFA9678B79D54A086200F29D1D8579F8D5_18(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U32664319BEB31A007A1D31844B199D2EDFCBF29F9_19(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U328C710520D4675C71D8BEFAA19BC179951374DF7_20(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U32A2D28482BBB64FA5869C4116711B95BE18A1479_21(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U32B42D3D832DBF5D5ACE91E6073EBD7BB8E12DEA1_22(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U32BD232BF9BBB26D4E46CCB5C2D3B3B12274F1984_23(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U32ECAC8F6748E7A92931BE07DFCAB9A916B8D04DA_24(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U32FA444E8BC1E6356A9BD772EA3A1EFB53FF2BFC7_25(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U333084732F8B9869A26F8DF0103AC90B04390AC9A_26(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U334B53DD5686A849D0EC74344559E1E50D4776239_27(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U336B1D334FE412331CDF33316031195E0C56477DB_28(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U337D9C085BE6D5012609F925C1C5068721BF3EB15_29(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U33D729EDB583C1FC88A9D866EA0B3FE3BBE6738E3_30(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U33DFD4D28BC67D3A4AF48AFFEA3F91AA2FC7E6A49_31(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U341DD0957261A602402E73BAA98B6B90A59CDF8C9_32(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U34327074201FBB3C079CEAC5028A88C45836D33ED_33(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U345CC202B2D1D51E86B4E8E96653F784B771B59CE_34(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U3470B7F4FDB90F6D25C0F976D634BA13E5176CA87_35(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U3480480975A2F2351B4BC347BBF5B019CB39B8479_36(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U34E33ADF11BDB70C7312C8C14A3327CBAAFA43877_37(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U3511E29D606BCD9CCF19925FEC4AB93A0BE58E6C5_38(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U35210B03C13F919278C0704371268EDDDF80A4C4A_39(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U35D9373D97CCDD1C47F4B9AF262444CBB734B73E5_40(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U3694F8C359BDCF3B5E89D474E257815B6BC6976F5_41(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U36A177B9D2E9E08FC7B8D074AE21BD8F3B936FD4B_42(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U36A4898AE468C33B9D33857E1A1690163D02666C9_43(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U36A977F8999D2379359A8DFC8992B162FAB6C4A67_44(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U36C3172601CE476B8B4CB4063F75FEE986E5BEA3E_45(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U36C6307D0B0F6F4B22D271404E3A097CC7B078DDD_46(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U36C99786448610AA9A550CE200DFBFE653496CCFA_47(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U36E3D89B679A0E66DD78E53B4640C9726537E11A4_48(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U36F71EBB9EAEAD5A412B4323B2780EB7ABC873579_49(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U3708D10A4339CBCA79AF1F908E9277FA653F670EF_50(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U3709F63077E0EC8A637406E5C4D99FEBF9404DE0A_51(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U372ED9CD72B35925260074C944C96CADDFE306044_52(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U379A70DABA50850BC84C5F1D76C64024CE247C598_53(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U37A7A5C9196FC30D2B49EDFEA5DCA11DCBCB649A3_54(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U37E95FAD40702678CE0940C0AAF00B7C00586771C_55(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U37F415D9373FBBEFF31E2E51C7B7F968158CA85DD_56(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U38146FDE3318DC1C6DEF0B41B5A61BB29BBF64CAE_57(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U3826510337F148AA44E14E8E54E08BF04A8B294CE_58(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U384069833E42A8083F930F322499FC4BCFE543E4C_59(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U38793BA75E6A8E1D82395EF60EE5DC3A69977A62B_60(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U38ABAB393752BC1119B0EAD989DF450C6E04D8FF2_61(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U38C7294D418CBC68C2F3874E06AED8C38C573FDF4_62(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U391B948814BF180E7FA42137C83AAC043E1410E0F_63(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U392E3424CA2DC6ACE639842303D9E4959BC763087_64(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U3967A5B1C1344A31AF4E7E1AAB1E89EE24D8A3FAB_65(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U396DA30D37D8C819EE7A1D9733912F74C912EB5E4_66(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U3993F10D8449193619D5641268926D30980AB1EDE_67(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U39A1D259207846B1C51748E932A5DD3904EB818E4_68(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U39D23B3B23CCAE110C3CE9C5390F8C739F67D87AD_69(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U39FF23D5ECF10A5EB1F8BC25B3B3C71AB581369EE_70(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_A1131FF166B71E9853F0B0BD1DD153A30418529F_71(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_A3105F394A59519BE301861D340BC3D09E41CB19_72(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_A328A2984A4E6A35011BFE9252B01DA208C532BA_73(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_A4A22152FAAB8DB4A63DE422AE0D5852FD35B04A_74(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_A62278C03345A9248C734AF81513A1F8D1B07D46_75(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_A67ECCA7AD5CEABF13FA3BD68ED24B3F5FF81CC7_76(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_A68289F5B4251B321F6F03C741C46974A0418E64_77(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_A6AB6C1208F342FA0C67CE497AF09D47AFA065E8_78(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_A7CBA58371A7AF46C17847174F40F4663D2F59F5_79(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_A92F46ED19BFF6E5CAB48F7C846477C9AAD02DA6_80(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_A988F7089BBB957B7EA2A8F2D66C7DF62B59CFD9_81(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_ADA84571EAB5163CC2C40A5CF9F2DF898ED1180C_82(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_B07DA862851422D423B591975D256548DFDA3DC2_83(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_B14468BE5B21A8E0D79DA7988BBF773FDA08701A_84(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_B3124AD30FFF743F5EF54086F4B0192CF1A880CF_85(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_BC22A77A549E72722CCDC844425C7EA8615F3771_86(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_BE61A28EE8D252FD73D06806809B2E51FEB6F53C_87(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_BFB130DEFFB14CE563713BDDF442B98CCACE8A8F_88(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_C06E47E37A990899E03C953F64C95F7E3210B17E_89(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_C0A8C45EE4F4DF297B1688A2C5BDE5BC312071C4_90(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_C5709FC40EFF23BB977BBC3DA4BCA193930C5B92_91(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_C6DFEE18BA4DC30A7D7D90AB72AB0228B66ED858_92(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_C881D9C683CE2D13A2438CAD551803C8B1100C53_93(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_CBD12AC86FBCD56155B93A9CA77947E68793C2B5_94(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_CC3901AF2214B4D6ADEB73EB932BC1F0E40C318C_95(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_CE23A1D2337FC685E2FA96AC9F8B8BA6EA0C5BDD_96(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_D087BC49F1FEB5D2824F510253A805FCC2506F71_97(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_D177776BF8B5B13646CB32C3E3C2E3A3F6389454_98(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_D2CE52450BFB35255920A8F83FADB36945ABF964_99(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_D5C60CDEF63D72F5032E90D8B9D5EFF3E566D8F4_100(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_D5C92607E010304E7241899C8E6ADF0EE0CBFD31_101(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_D8D801E3DDC2422ADA99C1D4210803A5F734F968_102(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_DDF6D11A3E93BE497F5BAC07290983B9D3832ECE_103(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_DF97F08BD5310D2EBC2E21DE6536AE30347D8434_104(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_E2EF5AD9211AEBF1A7D759556F680C7916E434D8_105(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_E44BF9B4DEFF99F72DA9F21344C344E2CD352BCB_106(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_E78171182D510376B72BC5204A0D1D488DED6D00_107(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_E99B63E754303E8FA67A1C2AEA3F576D805E2FA9_108(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_EBB7471D163460A589A0BB5C1FB2B55811F80E11_109(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_EC71709D141BB87D172E6D9D919DF53D7A4EB217_110(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_ED6CCE320D9C94CDED123AD06BD6A29CC75A012C_111(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_F0E124B606159405D7C0D19C7158FD45A4F27EF4_112(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_F1781D4343E58B231D363C7698B8B7C63C173F92_113(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_F24A3495EC779AA95747314593B416FA283F5A15_114(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_F2BCD6EC6BB1D97B183FA5A1C54B5725010F0038_115(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_F301F2CE6EEF9D8AC92595F503B297E3F20B5267_116(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_F4F4DAB1172A7E33DC53F7AA668C2AC7E9AF6828_117(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_F7A1E3117FA8F941095E83511E1B55F5E050747C_118(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_F7C62050EA6AAC50646295736EF3E5083D7781D4_119(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_FA2D9B0982EC9C8B3280A5D325C7A19C52E902D6_120(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_FB4FBD597E39496564E6322822D8DD70A68ADE47_121(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_FD06A67F6CBC0BE9E112D56B89B86432104BD055_122(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_FD84D4E70F766B0C44E5012C056C44B2E5E90E9A_123(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5745;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5745 = { sizeof (__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5746;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5746 = { sizeof (__StaticArrayInitTypeSizeU3D14_t8CCDC05F13040DD154D81376695EEB6BA93ACABA)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D14_t8CCDC05F13040DD154D81376695EEB6BA93ACABA ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5747;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5747 = { sizeof (__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5748;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5748 = { sizeof (__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5749;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5749 = { sizeof (__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5750;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5750 = { sizeof (__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5751;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5751 = { sizeof (__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5752;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5752 = { sizeof (__StaticArrayInitTypeSizeU3D34_t9C713A05A7B6B5C6D004D41B59A4F6B7D561459A)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D34_t9C713A05A7B6B5C6D004D41B59A4F6B7D561459A ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5753;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5753 = { sizeof (__StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D36_tB11DD7146197C35EC027B5117260DFBD243BB647 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5754;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5754 = { sizeof (__StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D44_t23B28CF7DA9E9A0279013DBD8D892AC5F26A236D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5755;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5755 = { sizeof (__StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5756;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5756 = { sizeof (__StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D56_t8D3DFA87C31E8ED8877F777D9F0504EA567DF816 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5757;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5757 = { sizeof (__StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D60_t0397476031507426B41017E8EC056DD9CD5D97AF ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5758;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5758 = { sizeof (__StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D62_t1BE65393F9917779A2B34D8C2AC6D5767DCA2CDE ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5759;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5759 = { sizeof (__StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D64_t702E955CFCF83D5C66E478D1CFA07B549225E6C0 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5760;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5760 = { sizeof (__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D68_tE525D93B7D5D9BE287192726EF3CC960BCD75F5D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5761;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5761 = { sizeof (__StaticArrayInitTypeSizeU3D88_tACC0D3E292CC0817B7908ECD8382C424C141A9C9)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D88_tACC0D3E292CC0817B7908ECD8382C424C141A9C9 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5762;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5762 = { sizeof (__StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D92_t78867FD9A432D47D4E4F383AD8D3FEEDE9B1E946 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5763;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5763 = { sizeof (__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D108_t1D3815BD9654C521776B19932355EA4A81BD71FF ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5764;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5764 = { sizeof (__StaticArrayInitTypeSizeU3D120_tFE429A3C8C8CC2AA48009B28DC92C5FAC405B976)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D120_tFE429A3C8C8CC2AA48009B28DC92C5FAC405B976 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5765;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5765 = { sizeof (__StaticArrayInitTypeSizeU3D124_tD3D6FDB698030D188404AEB38EF634BB404CBB7C)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D124_tD3D6FDB698030D188404AEB38EF634BB404CBB7C ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5766;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5766 = { sizeof (__StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D128_t7DF98758AE4B35BF3B9F0D005DB4F3712B59F726 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5767;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5767 = { sizeof (__StaticArrayInitTypeSizeU3D140_t3F4097685F037DBBFCD03ED53B59913F53BBFAAC)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D140_t3F4097685F037DBBFCD03ED53B59913F53BBFAAC ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5768;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5768 = { sizeof (__StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D142_t6450F42DCAF87A0548155A119585FF6ECF1F8A61 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5769;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5769 = { sizeof (__StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D254_t6018CBBBE9AF46D550046C8250CAD97A448AA98D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5770;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5770 = { sizeof (__StaticArrayInitTypeSizeU3D256_t6F967FC9ACA9D51E8C10E3FEF9B3C0033CEB171F)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D256_t6F967FC9ACA9D51E8C10E3FEF9B3C0033CEB171F ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5771;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5771 = { sizeof (__StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D1022_t1C9A3423F18B0BF140CB040CCC6469830CBE2140 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5772;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5772 = { sizeof (__StaticArrayInitTypeSizeU3D1024_tC455CA8308094CC5E6ADD1A24E321B05B3134F97)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D1024_tC455CA8308094CC5E6ADD1A24E321B05B3134F97 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5773;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5773 = { sizeof (__StaticArrayInitTypeSizeU3D2048_t23ED45646FF6C31C0F8587980EEB1123BC08E49E)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D2048_t23ED45646FF6C31C0F8587980EEB1123BC08E49E ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
