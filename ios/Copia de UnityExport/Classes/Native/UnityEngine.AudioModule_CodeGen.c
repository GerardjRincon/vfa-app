﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.AudioSpeakerMode UnityEngine.AudioSettings::GetSpeakerMode()
extern void AudioSettings_GetSpeakerMode_m40ABC1D6AF8296E9C963CEC9F183FA09ACF03F02 (void);
// 0x00000002 System.Int32 UnityEngine.AudioSettings::GetSampleRate()
extern void AudioSettings_GetSampleRate_mB74463D0A8B5FF5BBDFE9FA4EAB295405202220E (void);
// 0x00000003 UnityEngine.AudioSpeakerMode UnityEngine.AudioSettings::get_speakerMode()
extern void AudioSettings_get_speakerMode_mFE3A64C54935E53E463624EFD3FF4A83CB504D60 (void);
// 0x00000004 System.Int32 UnityEngine.AudioSettings::get_outputSampleRate()
extern void AudioSettings_get_outputSampleRate_mA9092240D8A06109EA34644BD7FB239483F9A5F9 (void);
// 0x00000005 System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern void AudioSettings_InvokeOnAudioConfigurationChanged_m8D251791C6A402B12E93C22F43475DE3033FC8E7 (void);
// 0x00000006 System.Boolean UnityEngine.AudioSettings::StartAudioOutput()
extern void AudioSettings_StartAudioOutput_m0D831FF470163273341701B1054B28FF962F7712 (void);
// 0x00000007 System.Boolean UnityEngine.AudioSettings::StopAudioOutput()
extern void AudioSettings_StopAudioOutput_m7F6B15A6B8E9F0CEE05E4DA9B09EF4EC1158B588 (void);
// 0x00000008 System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern void AudioConfigurationChangeHandler__ctor_mF9399769D5BB18D740774B9E3129958868BD6D9A (void);
// 0x00000009 System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern void AudioConfigurationChangeHandler_Invoke_m62D72B397E1DC117C8C92A450D2C86C535A2BF49 (void);
// 0x0000000A System.IAsyncResult UnityEngine.AudioSettings/AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void AudioConfigurationChangeHandler_BeginInvoke_mB0B0ACF6281B999FA11037CA130CA3C72BEC7827 (void);
// 0x0000000B System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern void AudioConfigurationChangeHandler_EndInvoke_mBB53599C34E3944D3A1DD71EFD2D73AF105CF830 (void);
// 0x0000000C System.Boolean UnityEngine.AudioSettings/Mobile::get_muteState()
extern void Mobile_get_muteState_mD70625E75D05D14E2E6E18FACFFFFE324661906B (void);
// 0x0000000D System.Void UnityEngine.AudioSettings/Mobile::set_muteState(System.Boolean)
extern void Mobile_set_muteState_m878F00ADEB5D9EAF6AD1B8BC427C505A89A4EDE4 (void);
// 0x0000000E System.Boolean UnityEngine.AudioSettings/Mobile::get_stopAudioOutputOnMute()
extern void Mobile_get_stopAudioOutputOnMute_m2B8075BC7894966E20D0ED22A66FD39A884ECD81 (void);
// 0x0000000F System.Void UnityEngine.AudioSettings/Mobile::InvokeOnMuteStateChanged(System.Boolean)
extern void Mobile_InvokeOnMuteStateChanged_m01961F68C19CCF813239484FB3B76EFC164173D1 (void);
// 0x00000010 System.Void UnityEngine.AudioSettings/Mobile::StartAudioOutput()
extern void Mobile_StartAudioOutput_m19AF9680E60C92480171A84480DCB93487FFA6C6 (void);
// 0x00000011 System.Void UnityEngine.AudioSettings/Mobile::StopAudioOutput()
extern void Mobile_StopAudioOutput_mA7D45E035C9ABF5E4FE67C93A018E14E05748745 (void);
// 0x00000012 System.Void UnityEngine.AudioSettings/Mobile::.cctor()
extern void Mobile__cctor_m917C30310A8E4193197B8D2AA6EF256369258B6F (void);
// 0x00000013 System.Void UnityEngine.AudioClip::.ctor()
extern void AudioClip__ctor_m52425138C3A036FC847A0E4C4ADA31CEF81CD10D (void);
// 0x00000014 System.Boolean UnityEngine.AudioClip::GetData(UnityEngine.AudioClip,System.Single[],System.Int32,System.Int32)
extern void AudioClip_GetData_mEF59EDCD10F83C280DB82E14FF938FF574BBD128 (void);
// 0x00000015 System.Boolean UnityEngine.AudioClip::SetData(UnityEngine.AudioClip,System.Single[],System.Int32,System.Int32)
extern void AudioClip_SetData_m9422A5FB335AFB42C2CBAD4EE650C5F71ECB3559 (void);
// 0x00000016 UnityEngine.AudioClip UnityEngine.AudioClip::Construct_Internal()
extern void AudioClip_Construct_Internal_mEEA2165F0467FA67FA05361A85434DF5068E79B0 (void);
// 0x00000017 System.String UnityEngine.AudioClip::GetName()
extern void AudioClip_GetName_mD69F28B33950F21E36A2F3A659DD89725796D257 (void);
// 0x00000018 System.Void UnityEngine.AudioClip::CreateUserSound(System.String,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void AudioClip_CreateUserSound_m56B1F9909AB1F6CABCA8C526657F99A8BC0E73BD (void);
// 0x00000019 System.Single UnityEngine.AudioClip::get_length()
extern void AudioClip_get_length_mFF1E21363B1860453451C4DA1C1459E9B9504317 (void);
// 0x0000001A System.Int32 UnityEngine.AudioClip::get_samples()
extern void AudioClip_get_samples_m7AD532D9288680102A452D2949107BDA88268CA0 (void);
// 0x0000001B System.Int32 UnityEngine.AudioClip::get_channels()
extern void AudioClip_get_channels_m2CF01E121CEBBF3B69EC7EEE7EC28172AB6078EC (void);
// 0x0000001C UnityEngine.AudioDataLoadState UnityEngine.AudioClip::get_loadState()
extern void AudioClip_get_loadState_mD0CAA76E09B705ED1EBD7F576792A64AACE13BFD (void);
// 0x0000001D System.Boolean UnityEngine.AudioClip::GetData(System.Single[],System.Int32)
extern void AudioClip_GetData_m8150E67D6068CAA88BE4155CB5924B2359272EE0 (void);
// 0x0000001E System.Boolean UnityEngine.AudioClip::SetData(System.Single[],System.Int32)
extern void AudioClip_SetData_m7B400A0E491EDFE98D3A732D189443846E74CD6C (void);
// 0x0000001F UnityEngine.AudioClip UnityEngine.AudioClip::Create(System.String,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void AudioClip_Create_m76744E6CF5A521F79D0282A6FBEAAC37FE9C0BF5 (void);
// 0x00000020 UnityEngine.AudioClip UnityEngine.AudioClip::Create(System.String,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.AudioClip/PCMReaderCallback,UnityEngine.AudioClip/PCMSetPositionCallback)
extern void AudioClip_Create_m7D20A6B52ACA39506B505EF162AF7EF81B173995 (void);
// 0x00000021 System.Void UnityEngine.AudioClip::add_m_PCMReaderCallback(UnityEngine.AudioClip/PCMReaderCallback)
extern void AudioClip_add_m_PCMReaderCallback_mE70789B25C74C552769FB2DAB96FC416A8508C62 (void);
// 0x00000022 System.Void UnityEngine.AudioClip::remove_m_PCMReaderCallback(UnityEngine.AudioClip/PCMReaderCallback)
extern void AudioClip_remove_m_PCMReaderCallback_m0AEFEC9C086DEBABE06F1294AFE0B4031D805FE7 (void);
// 0x00000023 System.Void UnityEngine.AudioClip::add_m_PCMSetPositionCallback(UnityEngine.AudioClip/PCMSetPositionCallback)
extern void AudioClip_add_m_PCMSetPositionCallback_mC0EBAE94510712D3E1481ED8B3FB7956169754F5 (void);
// 0x00000024 System.Void UnityEngine.AudioClip::remove_m_PCMSetPositionCallback(UnityEngine.AudioClip/PCMSetPositionCallback)
extern void AudioClip_remove_m_PCMSetPositionCallback_m21FB4ECFD0CE6C754C5D3BBFDCE9AD21E6D074A2 (void);
// 0x00000025 System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern void AudioClip_InvokePCMReaderCallback_Internal_mF087FCAD425EAC299C1156BA809DC535D00757F9 (void);
// 0x00000026 System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern void AudioClip_InvokePCMSetPositionCallback_Internal_mBB8265A5BFF660F8AF39718DDB193319AB7EFA6F (void);
// 0x00000027 System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern void PCMReaderCallback__ctor_mF9EB2467704F5E13196BBA93F41FA275AC5432F6 (void);
// 0x00000028 System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern void PCMReaderCallback_Invoke_m7B101820DB35BEFC8D2724DF96900367863B93B6 (void);
// 0x00000029 System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern void PCMReaderCallback_BeginInvoke_m94035E11B2B9BD6114EF3D7F4B7E367572E7AE1F (void);
// 0x0000002A System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern void PCMReaderCallback_EndInvoke_m6730FD7DFD7246F137C437BC470F995D6C75E15B (void);
// 0x0000002B System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern void PCMSetPositionCallback__ctor_m31EA578C3CCFDFC9335B8C67353878AEE4B3905F (void);
// 0x0000002C System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern void PCMSetPositionCallback_Invoke_m8EA4736B43191A8E6F95E1548AFF124519EC533C (void);
// 0x0000002D System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void PCMSetPositionCallback_BeginInvoke_m88CDF70D75854621CA69ED3D53CD53B8206A5093 (void);
// 0x0000002E System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern void PCMSetPositionCallback_EndInvoke_mB711E23CFD370348A1680B281A3DFE04F970792C (void);
// 0x0000002F System.Void UnityEngine.AudioSource::PlayHelper(UnityEngine.AudioSource,System.UInt64)
extern void AudioSource_PlayHelper_m361C17B583E05D2A5FA0F03BD7CD98D74FBF83AC (void);
// 0x00000030 System.Void UnityEngine.AudioSource::Stop(System.Boolean)
extern void AudioSource_Stop_mD3712B98BC6DBEA9CEEF778CE9CCB8DBA62F47A8 (void);
// 0x00000031 System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern void AudioSource_set_volume_mF1757D70EE113871724334D13F70EF1ED033BA06 (void);
// 0x00000032 System.Single UnityEngine.AudioSource::get_time()
extern void AudioSource_get_time_m9EE836ADDDAB3598FD7F6D198F847C43E0FFBF52 (void);
// 0x00000033 System.Void UnityEngine.AudioSource::set_time(System.Single)
extern void AudioSource_set_time_m76B72F7AEF8B07469847BF8EFFC51991C9D695B1 (void);
// 0x00000034 UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern void AudioSource_get_clip_m773ECEF5566EA64C74E316D7EF1A63AA01604643 (void);
// 0x00000035 System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern void AudioSource_set_clip_mF574231E0B749E0167CAF9E4FCBA06BAA0F9ED9B (void);
// 0x00000036 System.Void UnityEngine.AudioSource::Play()
extern void AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164 (void);
// 0x00000037 System.Void UnityEngine.AudioSource::Stop()
extern void AudioSource_Stop_m488F7AA7F7067DE3EC92CEE3413E86C2E5940200 (void);
// 0x00000038 System.Boolean UnityEngine.AudioSource::get_isPlaying()
extern void AudioSource_get_isPlaying_m5112A878573652681F40C82F0D8103C999978F3C (void);
// 0x00000039 System.Void UnityEngine.AudioSource::PlayClipAtPoint(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern void AudioSource_PlayClipAtPoint_m484E3DF855E6D40448E5AAED9D77846617B8C0CE (void);
// 0x0000003A System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
extern void AudioSource_set_loop_m4DEE785C31213E964D7014B633F0FFC7E98B79F4 (void);
// 0x0000003B System.Void UnityEngine.AudioSource::set_spatialBlend(System.Single)
extern void AudioSource_set_spatialBlend_mC144B8230F08743505D4B0A92C1B9F809BC6D9C5 (void);
// 0x0000003C System.Void UnityEngine.AudioSource::set_bypassEffects(System.Boolean)
extern void AudioSource_set_bypassEffects_m6E1BBD3084EEEA68DDD468C8D488DC85A1112AD3 (void);
// 0x0000003D System.Void UnityEngine.AudioSource::set_bypassListenerEffects(System.Boolean)
extern void AudioSource_set_bypassListenerEffects_m34B3031437F9BC2D8A99584B4906DD23FC73A16C (void);
// 0x0000003E System.Void UnityEngine.AudioSource::set_mute(System.Boolean)
extern void AudioSource_set_mute_m04D579849D7D37D6CC39DE31DB928176B2A9C2CF (void);
// 0x0000003F System.Void UnityEngine.AudioSource::set_minDistance(System.Single)
extern void AudioSource_set_minDistance_m6BE1B716538D146E7DAFED645624204A6F49496C (void);
// 0x00000040 System.Int32 UnityEngine.Microphone::GetMicrophoneDeviceIDFromName(System.String)
extern void Microphone_GetMicrophoneDeviceIDFromName_m08C8735264E4D5FEAED224C8E7957CF5916D9A5E (void);
// 0x00000041 UnityEngine.AudioClip UnityEngine.Microphone::StartRecord(System.Int32,System.Boolean,System.Single,System.Int32)
extern void Microphone_StartRecord_m88F707BD42E1494928AA8D2E915062826E9021E8 (void);
// 0x00000042 System.Void UnityEngine.Microphone::EndRecord(System.Int32)
extern void Microphone_EndRecord_m6DDB48A30B1A34A6E996A1A1FC194DA184B46733 (void);
// 0x00000043 System.Int32 UnityEngine.Microphone::GetRecordPosition(System.Int32)
extern void Microphone_GetRecordPosition_m39EF5B7BA939CBE97875B9DA5265D8036F5D13ED (void);
// 0x00000044 UnityEngine.AudioClip UnityEngine.Microphone::Start(System.String,System.Boolean,System.Int32,System.Int32)
extern void Microphone_Start_mF756A7EBA3E62EF0D138A220482B725D16E96047 (void);
// 0x00000045 System.Void UnityEngine.Microphone::End(System.String)
extern void Microphone_End_m2E3D0E4890AE014AF687987F6160CA3D5ACDC29F (void);
// 0x00000046 System.Int32 UnityEngine.Microphone::GetPosition(System.String)
extern void Microphone_GetPosition_m1C177D77958EB1BBADE1EEBB721428059B14A7FF (void);
// 0x00000047 System.Void UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)
extern void WebCamTexture_Internal_CreateWebCamTexture_mE80CEFDA08815EEF5581C8385462887A62D84BB5 (void);
// 0x00000048 System.Void UnityEngine.WebCamTexture::.ctor(System.String,System.Int32,System.Int32,System.Int32)
extern void WebCamTexture__ctor_mCDA59B88B6D7F96B76663FA98EF12B7AF2DCFD61 (void);
// 0x00000049 System.Void UnityEngine.WebCamTexture::Play()
extern void WebCamTexture_Play_mCF10A9B5EE587A066396B6378A972B31C9134436 (void);
// 0x0000004A System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)
extern void WebCamTexture_INTERNAL_CALL_Play_mF95EBF45A6EE05B6FCA20EAC623542A046013801 (void);
// 0x0000004B System.Int32 UnityEngine.WebCamTexture::get_videoRotationAngle()
extern void WebCamTexture_get_videoRotationAngle_m02878E5708942CE6149A57E6E10C453358D2B2A9 (void);
// 0x0000004C System.Boolean UnityEngine.WebCamTexture::get_videoVerticallyMirrored()
extern void WebCamTexture_get_videoVerticallyMirrored_m4E0EB16E94118818A000761778F2672B5D2DD8AD (void);
// 0x0000004D UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::GetHandle()
extern void AudioClipPlayable_GetHandle_mEE8F62E9DA2A0DDDB064A9AA2391909C425CB3B1 (void);
// 0x0000004E System.Boolean UnityEngine.Audio.AudioClipPlayable::Equals(UnityEngine.Audio.AudioClipPlayable)
extern void AudioClipPlayable_Equals_mEB47B5F2E1C643D403FA916C8961F117593DCFC0 (void);
// 0x0000004F UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::GetHandle()
extern void AudioMixerPlayable_GetHandle_mDBC7135DF653E0E19675B6694EA89958E609587D (void);
// 0x00000050 System.Boolean UnityEngine.Audio.AudioMixerPlayable::Equals(UnityEngine.Audio.AudioMixerPlayable)
extern void AudioMixerPlayable_Equals_m6B84D1A5AEDEAAE12AEFB77319B2662506ABC9C4 (void);
// 0x00000051 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InvokeSampleFramesAvailable(System.Int32)
extern void AudioSampleProvider_InvokeSampleFramesAvailable_m7604AAF1AC01473A29DCDAD1AEC06165504BE832 (void);
// 0x00000052 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InvokeSampleFramesOverflow(System.Int32)
extern void AudioSampleProvider_InvokeSampleFramesOverflow_mC81A014388E535569EF02E3DA6B9831B0FB8A8D4 (void);
// 0x00000053 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler::.ctor(System.Object,System.IntPtr)
extern void SampleFramesHandler__ctor_mFDA0769E55F136D1B8EC8AA4B40EF43069934EB5 (void);
// 0x00000054 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler::Invoke(UnityEngine.Experimental.Audio.AudioSampleProvider,System.UInt32)
extern void SampleFramesHandler_Invoke_m52F0148F680B36E04A7F850E617FBEF1CA9809FD (void);
// 0x00000055 System.IAsyncResult UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler::BeginInvoke(UnityEngine.Experimental.Audio.AudioSampleProvider,System.UInt32,System.AsyncCallback,System.Object)
extern void SampleFramesHandler_BeginInvoke_mE516B77CCC50738663D10DDD2D7BDB4391FDFF92 (void);
// 0x00000056 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler::EndInvoke(System.IAsyncResult)
extern void SampleFramesHandler_EndInvoke_mF5305B3BA179CE3C49836790DE3FEB02EB088D28 (void);
static Il2CppMethodPointer s_methodPointers[86] = 
{
	AudioSettings_GetSpeakerMode_m40ABC1D6AF8296E9C963CEC9F183FA09ACF03F02,
	AudioSettings_GetSampleRate_mB74463D0A8B5FF5BBDFE9FA4EAB295405202220E,
	AudioSettings_get_speakerMode_mFE3A64C54935E53E463624EFD3FF4A83CB504D60,
	AudioSettings_get_outputSampleRate_mA9092240D8A06109EA34644BD7FB239483F9A5F9,
	AudioSettings_InvokeOnAudioConfigurationChanged_m8D251791C6A402B12E93C22F43475DE3033FC8E7,
	AudioSettings_StartAudioOutput_m0D831FF470163273341701B1054B28FF962F7712,
	AudioSettings_StopAudioOutput_m7F6B15A6B8E9F0CEE05E4DA9B09EF4EC1158B588,
	AudioConfigurationChangeHandler__ctor_mF9399769D5BB18D740774B9E3129958868BD6D9A,
	AudioConfigurationChangeHandler_Invoke_m62D72B397E1DC117C8C92A450D2C86C535A2BF49,
	AudioConfigurationChangeHandler_BeginInvoke_mB0B0ACF6281B999FA11037CA130CA3C72BEC7827,
	AudioConfigurationChangeHandler_EndInvoke_mBB53599C34E3944D3A1DD71EFD2D73AF105CF830,
	Mobile_get_muteState_mD70625E75D05D14E2E6E18FACFFFFE324661906B,
	Mobile_set_muteState_m878F00ADEB5D9EAF6AD1B8BC427C505A89A4EDE4,
	Mobile_get_stopAudioOutputOnMute_m2B8075BC7894966E20D0ED22A66FD39A884ECD81,
	Mobile_InvokeOnMuteStateChanged_m01961F68C19CCF813239484FB3B76EFC164173D1,
	Mobile_StartAudioOutput_m19AF9680E60C92480171A84480DCB93487FFA6C6,
	Mobile_StopAudioOutput_mA7D45E035C9ABF5E4FE67C93A018E14E05748745,
	Mobile__cctor_m917C30310A8E4193197B8D2AA6EF256369258B6F,
	AudioClip__ctor_m52425138C3A036FC847A0E4C4ADA31CEF81CD10D,
	AudioClip_GetData_mEF59EDCD10F83C280DB82E14FF938FF574BBD128,
	AudioClip_SetData_m9422A5FB335AFB42C2CBAD4EE650C5F71ECB3559,
	AudioClip_Construct_Internal_mEEA2165F0467FA67FA05361A85434DF5068E79B0,
	AudioClip_GetName_mD69F28B33950F21E36A2F3A659DD89725796D257,
	AudioClip_CreateUserSound_m56B1F9909AB1F6CABCA8C526657F99A8BC0E73BD,
	AudioClip_get_length_mFF1E21363B1860453451C4DA1C1459E9B9504317,
	AudioClip_get_samples_m7AD532D9288680102A452D2949107BDA88268CA0,
	AudioClip_get_channels_m2CF01E121CEBBF3B69EC7EEE7EC28172AB6078EC,
	AudioClip_get_loadState_mD0CAA76E09B705ED1EBD7F576792A64AACE13BFD,
	AudioClip_GetData_m8150E67D6068CAA88BE4155CB5924B2359272EE0,
	AudioClip_SetData_m7B400A0E491EDFE98D3A732D189443846E74CD6C,
	AudioClip_Create_m76744E6CF5A521F79D0282A6FBEAAC37FE9C0BF5,
	AudioClip_Create_m7D20A6B52ACA39506B505EF162AF7EF81B173995,
	AudioClip_add_m_PCMReaderCallback_mE70789B25C74C552769FB2DAB96FC416A8508C62,
	AudioClip_remove_m_PCMReaderCallback_m0AEFEC9C086DEBABE06F1294AFE0B4031D805FE7,
	AudioClip_add_m_PCMSetPositionCallback_mC0EBAE94510712D3E1481ED8B3FB7956169754F5,
	AudioClip_remove_m_PCMSetPositionCallback_m21FB4ECFD0CE6C754C5D3BBFDCE9AD21E6D074A2,
	AudioClip_InvokePCMReaderCallback_Internal_mF087FCAD425EAC299C1156BA809DC535D00757F9,
	AudioClip_InvokePCMSetPositionCallback_Internal_mBB8265A5BFF660F8AF39718DDB193319AB7EFA6F,
	PCMReaderCallback__ctor_mF9EB2467704F5E13196BBA93F41FA275AC5432F6,
	PCMReaderCallback_Invoke_m7B101820DB35BEFC8D2724DF96900367863B93B6,
	PCMReaderCallback_BeginInvoke_m94035E11B2B9BD6114EF3D7F4B7E367572E7AE1F,
	PCMReaderCallback_EndInvoke_m6730FD7DFD7246F137C437BC470F995D6C75E15B,
	PCMSetPositionCallback__ctor_m31EA578C3CCFDFC9335B8C67353878AEE4B3905F,
	PCMSetPositionCallback_Invoke_m8EA4736B43191A8E6F95E1548AFF124519EC533C,
	PCMSetPositionCallback_BeginInvoke_m88CDF70D75854621CA69ED3D53CD53B8206A5093,
	PCMSetPositionCallback_EndInvoke_mB711E23CFD370348A1680B281A3DFE04F970792C,
	AudioSource_PlayHelper_m361C17B583E05D2A5FA0F03BD7CD98D74FBF83AC,
	AudioSource_Stop_mD3712B98BC6DBEA9CEEF778CE9CCB8DBA62F47A8,
	AudioSource_set_volume_mF1757D70EE113871724334D13F70EF1ED033BA06,
	AudioSource_get_time_m9EE836ADDDAB3598FD7F6D198F847C43E0FFBF52,
	AudioSource_set_time_m76B72F7AEF8B07469847BF8EFFC51991C9D695B1,
	AudioSource_get_clip_m773ECEF5566EA64C74E316D7EF1A63AA01604643,
	AudioSource_set_clip_mF574231E0B749E0167CAF9E4FCBA06BAA0F9ED9B,
	AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164,
	AudioSource_Stop_m488F7AA7F7067DE3EC92CEE3413E86C2E5940200,
	AudioSource_get_isPlaying_m5112A878573652681F40C82F0D8103C999978F3C,
	AudioSource_PlayClipAtPoint_m484E3DF855E6D40448E5AAED9D77846617B8C0CE,
	AudioSource_set_loop_m4DEE785C31213E964D7014B633F0FFC7E98B79F4,
	AudioSource_set_spatialBlend_mC144B8230F08743505D4B0A92C1B9F809BC6D9C5,
	AudioSource_set_bypassEffects_m6E1BBD3084EEEA68DDD468C8D488DC85A1112AD3,
	AudioSource_set_bypassListenerEffects_m34B3031437F9BC2D8A99584B4906DD23FC73A16C,
	AudioSource_set_mute_m04D579849D7D37D6CC39DE31DB928176B2A9C2CF,
	AudioSource_set_minDistance_m6BE1B716538D146E7DAFED645624204A6F49496C,
	Microphone_GetMicrophoneDeviceIDFromName_m08C8735264E4D5FEAED224C8E7957CF5916D9A5E,
	Microphone_StartRecord_m88F707BD42E1494928AA8D2E915062826E9021E8,
	Microphone_EndRecord_m6DDB48A30B1A34A6E996A1A1FC194DA184B46733,
	Microphone_GetRecordPosition_m39EF5B7BA939CBE97875B9DA5265D8036F5D13ED,
	Microphone_Start_mF756A7EBA3E62EF0D138A220482B725D16E96047,
	Microphone_End_m2E3D0E4890AE014AF687987F6160CA3D5ACDC29F,
	Microphone_GetPosition_m1C177D77958EB1BBADE1EEBB721428059B14A7FF,
	WebCamTexture_Internal_CreateWebCamTexture_mE80CEFDA08815EEF5581C8385462887A62D84BB5,
	WebCamTexture__ctor_mCDA59B88B6D7F96B76663FA98EF12B7AF2DCFD61,
	WebCamTexture_Play_mCF10A9B5EE587A066396B6378A972B31C9134436,
	WebCamTexture_INTERNAL_CALL_Play_mF95EBF45A6EE05B6FCA20EAC623542A046013801,
	WebCamTexture_get_videoRotationAngle_m02878E5708942CE6149A57E6E10C453358D2B2A9,
	WebCamTexture_get_videoVerticallyMirrored_m4E0EB16E94118818A000761778F2672B5D2DD8AD,
	AudioClipPlayable_GetHandle_mEE8F62E9DA2A0DDDB064A9AA2391909C425CB3B1,
	AudioClipPlayable_Equals_mEB47B5F2E1C643D403FA916C8961F117593DCFC0,
	AudioMixerPlayable_GetHandle_mDBC7135DF653E0E19675B6694EA89958E609587D,
	AudioMixerPlayable_Equals_m6B84D1A5AEDEAAE12AEFB77319B2662506ABC9C4,
	AudioSampleProvider_InvokeSampleFramesAvailable_m7604AAF1AC01473A29DCDAD1AEC06165504BE832,
	AudioSampleProvider_InvokeSampleFramesOverflow_mC81A014388E535569EF02E3DA6B9831B0FB8A8D4,
	SampleFramesHandler__ctor_mFDA0769E55F136D1B8EC8AA4B40EF43069934EB5,
	SampleFramesHandler_Invoke_m52F0148F680B36E04A7F850E617FBEF1CA9809FD,
	SampleFramesHandler_BeginInvoke_mE516B77CCC50738663D10DDD2D7BDB4391FDFF92,
	SampleFramesHandler_EndInvoke_mF5305B3BA179CE3C49836790DE3FEB02EB088D28,
};
extern void AudioClipPlayable_GetHandle_mEE8F62E9DA2A0DDDB064A9AA2391909C425CB3B1_AdjustorThunk (void);
extern void AudioClipPlayable_Equals_mEB47B5F2E1C643D403FA916C8961F117593DCFC0_AdjustorThunk (void);
extern void AudioMixerPlayable_GetHandle_mDBC7135DF653E0E19675B6694EA89958E609587D_AdjustorThunk (void);
extern void AudioMixerPlayable_Equals_m6B84D1A5AEDEAAE12AEFB77319B2662506ABC9C4_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[4] = 
{
	{ 0x0600004D, AudioClipPlayable_GetHandle_mEE8F62E9DA2A0DDDB064A9AA2391909C425CB3B1_AdjustorThunk },
	{ 0x0600004E, AudioClipPlayable_Equals_mEB47B5F2E1C643D403FA916C8961F117593DCFC0_AdjustorThunk },
	{ 0x0600004F, AudioMixerPlayable_GetHandle_mDBC7135DF653E0E19675B6694EA89958E609587D_AdjustorThunk },
	{ 0x06000050, AudioMixerPlayable_Equals_m6B84D1A5AEDEAAE12AEFB77319B2662506ABC9C4_AdjustorThunk },
};
static const int32_t s_InvokerIndices[86] = 
{
	106,
	106,
	106,
	106,
	849,
	49,
	49,
	124,
	31,
	1411,
	26,
	49,
	849,
	49,
	849,
	3,
	3,
	3,
	23,
	1101,
	1101,
	4,
	14,
	2267,
	726,
	10,
	10,
	10,
	141,
	141,
	2268,
	2269,
	26,
	26,
	26,
	26,
	26,
	32,
	124,
	26,
	205,
	26,
	124,
	32,
	598,
	26,
	152,
	31,
	334,
	726,
	334,
	14,
	26,
	23,
	23,
	89,
	2270,
	31,
	334,
	31,
	31,
	31,
	334,
	94,
	2271,
	164,
	21,
	2272,
	154,
	94,
	2273,
	192,
	23,
	154,
	10,
	89,
	1760,
	2274,
	1760,
	2275,
	32,
	32,
	124,
	130,
	142,
	26,
};
extern const Il2CppCodeGenModule g_UnityEngine_AudioModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AudioModuleCodeGenModule = 
{
	"UnityEngine.AudioModule.dll",
	86,
	s_methodPointers,
	4,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
