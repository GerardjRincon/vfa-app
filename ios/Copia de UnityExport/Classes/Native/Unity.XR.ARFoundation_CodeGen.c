﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* ARFoundationBackgroundRenderer_ResetGlState_mF47B550F91756D479D3E93AAB9D69C674892E94E_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer UnityEngine.XR.ARFoundation.ARBackgroundRendererAsset::CreateARBackgroundRenderer()
// 0x00000002 System.Void UnityEngine.XR.ARFoundation.ARBackgroundRendererAsset::CreateHelperComponents(UnityEngine.GameObject)
// 0x00000003 UnityEngine.Material UnityEngine.XR.ARFoundation.ARBackgroundRendererAsset::CreateCustomMaterial()
// 0x00000004 System.Void UnityEngine.XR.ARFoundation.ARBackgroundRendererAsset::.ctor()
extern void ARBackgroundRendererAsset__ctor_m176A55D92E105E2A70E14AB312D7D24EF3B89306 (void);
// 0x00000005 System.Boolean UnityEngine.XR.ARFoundation.ARCameraBackground::get_useCustomMaterial()
extern void ARCameraBackground_get_useCustomMaterial_mD74747ED7EE8105D789E8BC42E90AAA54E992896 (void);
// 0x00000006 System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::set_useCustomMaterial(System.Boolean)
extern void ARCameraBackground_set_useCustomMaterial_m4B4941B89CD4768AE7B0388E0DF2757B09EF6977 (void);
// 0x00000007 UnityEngine.Material UnityEngine.XR.ARFoundation.ARCameraBackground::get_customMaterial()
extern void ARCameraBackground_get_customMaterial_m5405AC6064562E48113BDE0A3FF52869F2AFA611 (void);
// 0x00000008 System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::set_customMaterial(UnityEngine.Material)
extern void ARCameraBackground_set_customMaterial_m856F6BD371FB08A072539B5B6AEB35A1733F9697 (void);
// 0x00000009 UnityEngine.Material UnityEngine.XR.ARFoundation.ARCameraBackground::get_material()
extern void ARCameraBackground_get_material_m3163C241851D9445E405ACFDA9200921E65288ED (void);
// 0x0000000A System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::set_material(UnityEngine.Material)
extern void ARCameraBackground_set_material_mD35D84A442B74B54A215CA16AA5C602BD9A6A355 (void);
// 0x0000000B System.Boolean UnityEngine.XR.ARFoundation.ARCameraBackground::get_useCustomRendererAsset()
extern void ARCameraBackground_get_useCustomRendererAsset_mB40673385E0549AA635214EB9043E985C4C91304 (void);
// 0x0000000C System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::set_useCustomRendererAsset(System.Boolean)
extern void ARCameraBackground_set_useCustomRendererAsset_mB60A83699045452C991F829CDA47938F5977C5F3 (void);
// 0x0000000D UnityEngine.XR.ARFoundation.ARBackgroundRendererAsset UnityEngine.XR.ARFoundation.ARCameraBackground::get_customRendererAsset()
extern void ARCameraBackground_get_customRendererAsset_m5F868FBA42CAB728508F1F90534100E04A6D6B27 (void);
// 0x0000000E System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::set_customRendererAsset(UnityEngine.XR.ARFoundation.ARBackgroundRendererAsset)
extern void ARCameraBackground_set_customRendererAsset_m3897137531E97E8DDF5EA075CCAB83995018DD52 (void);
// 0x0000000F UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer UnityEngine.XR.ARFoundation.ARCameraBackground::get_m_BackgroundRenderer()
extern void ARCameraBackground_get_m_BackgroundRenderer_m1B313D75B85F24DAC3E814F8A407A16AF21ED72C (void);
// 0x00000010 System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::set_m_BackgroundRenderer(UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer)
extern void ARCameraBackground_set_m_BackgroundRenderer_m81BE6FDDB7346A6BEBF8C2A90637F9EABCD2928D (void);
// 0x00000011 UnityEngine.Material UnityEngine.XR.ARFoundation.ARCameraBackground::CreateMaterialFromSubsystemShader()
extern void ARCameraBackground_CreateMaterialFromSubsystemShader_m91CBB645E549260938C0CC7E4E2231009106E112 (void);
// 0x00000012 System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::OnCameraFrameReceived(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern void ARCameraBackground_OnCameraFrameReceived_m3D8C38D609F7E4A2F5F829A799F6E228C00EF3B1 (void);
// 0x00000013 System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::SetupBackgroundRenderer()
extern void ARCameraBackground_SetupBackgroundRenderer_mA7B43BB265F49199D7CFD7BE55F4EA76F2929760 (void);
// 0x00000014 System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::Awake()
extern void ARCameraBackground_Awake_m533DD57AB5745778694DFD05036953081632F7C1 (void);
// 0x00000015 System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::OnEnable()
extern void ARCameraBackground_OnEnable_m9637B9AC38620F199D7A7282D58DFEF38912B738 (void);
// 0x00000016 System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::OnDisable()
extern void ARCameraBackground_OnDisable_m61D075560663A9A8B9CAE93CC240F525D3785CC1 (void);
// 0x00000017 System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::OnSessionStateChanged(UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs)
extern void ARCameraBackground_OnSessionStateChanged_m140FAA76DE26615403D3FF3335EC21B615AFCB7F (void);
// 0x00000018 System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::UpdateMaterial()
extern void ARCameraBackground_UpdateMaterial_mF3A8305A178B874F2B100C5C6B6AEEDB875EFAED (void);
// 0x00000019 UnityEngine.Material UnityEngine.XR.ARFoundation.ARCameraBackground::get_subsystemMaterial()
extern void ARCameraBackground_get_subsystemMaterial_m82EA024B4F69FAF76136AB8C594CC38D0525653E (void);
// 0x0000001A UnityEngine.Material UnityEngine.XR.ARFoundation.ARCameraBackground::get_lwrpMaterial()
extern void ARCameraBackground_get_lwrpMaterial_m876FA7A579D262F1D67F5DBF501FA63963C577BC (void);
// 0x0000001B UnityEngine.XR.ARFoundation.ARRenderMode UnityEngine.XR.ARFoundation.ARCameraBackground::get_mode()
extern void ARCameraBackground_get_mode_m7C4BDFF6F9EBBEF9074852AE08604AE6F83D464F (void);
// 0x0000001C System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::set_mode(UnityEngine.XR.ARFoundation.ARRenderMode)
extern void ARCameraBackground_set_mode_mF1D34C25DA3B0699B1A53217AC5290B8DF21E2D5 (void);
// 0x0000001D System.Boolean UnityEngine.XR.ARFoundation.ARCameraBackground::get_useRenderPipeline()
extern void ARCameraBackground_get_useRenderPipeline_m0DFF39BBBDEE59497ADF65D7D4DBFA552134EC07 (void);
// 0x0000001E System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::.ctor()
extern void ARCameraBackground__ctor_m6180F629FF4C5FD28ADF5815B04D4378FB8EADDC (void);
// 0x0000001F System.Void UnityEngine.XR.ARFoundation.ARCameraBackground::.cctor()
extern void ARCameraBackground__cctor_m45FB63609D064ECDC2B90E6BE0E0B4371ED13FD5 (void);
// 0x00000020 UnityEngine.XR.ARFoundation.ARLightEstimationData UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::get_lightEstimation()
extern void ARCameraFrameEventArgs_get_lightEstimation_m4F226F18EC0E91AEA1F152B165703869D31C9600 (void);
// 0x00000021 System.Void UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::set_lightEstimation(UnityEngine.XR.ARFoundation.ARLightEstimationData)
extern void ARCameraFrameEventArgs_set_lightEstimation_mE109AD7E91548FEF74DF045FB85B598210895657 (void);
// 0x00000022 System.Nullable`1<System.Int64> UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::get_timestampNs()
extern void ARCameraFrameEventArgs_get_timestampNs_m26633672AA4D433551422517ADA8F0ECED1B1A9E (void);
// 0x00000023 System.Void UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::set_timestampNs(System.Nullable`1<System.Int64>)
extern void ARCameraFrameEventArgs_set_timestampNs_mDA520DBF61A930154758C9FF620B639861EE920B (void);
// 0x00000024 System.Nullable`1<UnityEngine.Matrix4x4> UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::get_projectionMatrix()
extern void ARCameraFrameEventArgs_get_projectionMatrix_mEBEA65211112C8080149BB535637D3BED21EC273 (void);
// 0x00000025 System.Void UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::set_projectionMatrix(System.Nullable`1<UnityEngine.Matrix4x4>)
extern void ARCameraFrameEventArgs_set_projectionMatrix_mC6137EDADA29E8383CFFAAB4F9C839A176220142 (void);
// 0x00000026 System.Nullable`1<UnityEngine.Matrix4x4> UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::get_displayMatrix()
extern void ARCameraFrameEventArgs_get_displayMatrix_m9FCD5AF461B96162C2D1E9CCBEDE76974F32AAB0 (void);
// 0x00000027 System.Void UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::set_displayMatrix(System.Nullable`1<UnityEngine.Matrix4x4>)
extern void ARCameraFrameEventArgs_set_displayMatrix_mC37DE33664D4EBA5788105502751A981E37E71AB (void);
// 0x00000028 System.Collections.Generic.List`1<UnityEngine.Texture2D> UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::get_textures()
extern void ARCameraFrameEventArgs_get_textures_m7062757F64E5497A602D2E5E55929ECE9C9CAC58 (void);
// 0x00000029 System.Void UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::set_textures(System.Collections.Generic.List`1<UnityEngine.Texture2D>)
extern void ARCameraFrameEventArgs_set_textures_m11E835B5FF52F8FACF26836715ED85B785FB3E79 (void);
// 0x0000002A System.Collections.Generic.List`1<System.Int32> UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::get_propertyNameIds()
extern void ARCameraFrameEventArgs_get_propertyNameIds_m84B811AA86E7BE4E1D8D79C88F7892D35FF38946 (void);
// 0x0000002B System.Void UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::set_propertyNameIds(System.Collections.Generic.List`1<System.Int32>)
extern void ARCameraFrameEventArgs_set_propertyNameIds_m122F7477BC8728536A0C240FD62A2CDFA26743A9 (void);
// 0x0000002C System.Int32 UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::GetHashCode()
extern void ARCameraFrameEventArgs_GetHashCode_m406B3D310D34F27D4BEAC18178A13E57F1BF8E7E (void);
// 0x0000002D System.Boolean UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::Equals(System.Object)
extern void ARCameraFrameEventArgs_Equals_mFB12BA2256481CDBA32F843FA14CE365E9FB1F17 (void);
// 0x0000002E System.String UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::ToString()
extern void ARCameraFrameEventArgs_ToString_m49FFD9A837C51548F9F25E341869E9CA1637C108 (void);
// 0x0000002F System.Boolean UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::Equals(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern void ARCameraFrameEventArgs_Equals_m3C3D3D4D19A38B553D21664F58B52A6DDF745AE8 (void);
// 0x00000030 System.Boolean UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::op_Equality(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs,UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern void ARCameraFrameEventArgs_op_Equality_m28337A36DEDAA5E9A103A862F2534FB4BE72D67C (void);
// 0x00000031 System.Boolean UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::op_Inequality(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs,UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern void ARCameraFrameEventArgs_op_Inequality_m232AF17EF5D3C1A18CA8A7D0CBBCFA5F48542112 (void);
// 0x00000032 UnityEngine.XR.ARSubsystems.CameraFocusMode UnityEngine.XR.ARFoundation.ARCameraManager::get_focusMode()
extern void ARCameraManager_get_focusMode_mF8E49519D985F5108A8417F51B53BFEFBC42D5A1 (void);
// 0x00000033 System.Void UnityEngine.XR.ARFoundation.ARCameraManager::set_focusMode(UnityEngine.XR.ARSubsystems.CameraFocusMode)
extern void ARCameraManager_set_focusMode_m43E1ABCEA28C77FB476B9A81E0C869A54D167677 (void);
// 0x00000034 UnityEngine.XR.ARSubsystems.LightEstimationMode UnityEngine.XR.ARFoundation.ARCameraManager::get_lightEstimationMode()
extern void ARCameraManager_get_lightEstimationMode_m92906F93977637966A91F1096F5E36C8085E253E (void);
// 0x00000035 System.Void UnityEngine.XR.ARFoundation.ARCameraManager::set_lightEstimationMode(UnityEngine.XR.ARSubsystems.LightEstimationMode)
extern void ARCameraManager_set_lightEstimationMode_mB8099A10ECE4DCA4CA90491B7EDD98F70125B27F (void);
// 0x00000036 System.Boolean UnityEngine.XR.ARFoundation.ARCameraManager::get_permissionGranted()
extern void ARCameraManager_get_permissionGranted_m53AF1AD25F10B8AD4B5FE2EF1BAFB2619C6630B9 (void);
// 0x00000037 System.Void UnityEngine.XR.ARFoundation.ARCameraManager::add_frameReceived(System.Action`1<UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs>)
extern void ARCameraManager_add_frameReceived_mC892BC9299D54D118573AD11EC2AD7FB01FDE824 (void);
// 0x00000038 System.Void UnityEngine.XR.ARFoundation.ARCameraManager::remove_frameReceived(System.Action`1<UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs>)
extern void ARCameraManager_remove_frameReceived_m001AAE0A292720A4BFE91B4604B8151DA93ED730 (void);
// 0x00000039 System.String UnityEngine.XR.ARFoundation.ARCameraManager::get_shaderName()
extern void ARCameraManager_get_shaderName_m6A2F691CCEF93426C0C6C13A514914CAC93ED46C (void);
// 0x0000003A System.Boolean UnityEngine.XR.ARFoundation.ARCameraManager::TryGetIntrinsics(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics&)
extern void ARCameraManager_TryGetIntrinsics_mC131D22073841EA1D44B43EA1A763ED4A0712567 (void);
// 0x0000003B Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARFoundation.ARCameraManager::GetConfigurations(Unity.Collections.Allocator)
extern void ARCameraManager_GetConfigurations_m8E6514724AE3B3FD6CD6A03303739F05DB45B7F2 (void);
// 0x0000003C System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARFoundation.ARCameraManager::get_currentConfiguration()
extern void ARCameraManager_get_currentConfiguration_m01ECFF2D814A2D6C63390866420BA18F6BE02ECF (void);
// 0x0000003D System.Void UnityEngine.XR.ARFoundation.ARCameraManager::set_currentConfiguration(System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration>)
extern void ARCameraManager_set_currentConfiguration_m7C89FF4948F9C9AD0F094391795A25EB9E127E32 (void);
// 0x0000003E System.Boolean UnityEngine.XR.ARFoundation.ARCameraManager::TryGetLatestImage(UnityEngine.XR.ARSubsystems.XRCameraImage&)
extern void ARCameraManager_TryGetLatestImage_m100D8C01169594321DE493AB5590E71F710367CE (void);
// 0x0000003F System.Void UnityEngine.XR.ARFoundation.ARCameraManager::Awake()
extern void ARCameraManager_Awake_m69804098263656B4466DBD5A331F2B6B8BF377A7 (void);
// 0x00000040 System.Void UnityEngine.XR.ARFoundation.ARCameraManager::OnBeforeStart()
extern void ARCameraManager_OnBeforeStart_mBF04D69FA21C519C53D02EFCAA3F6BE9CF31B933 (void);
// 0x00000041 System.Void UnityEngine.XR.ARFoundation.ARCameraManager::OnDisable()
extern void ARCameraManager_OnDisable_m8F490C3886573FCC2A48AF1E5F55058616E5541F (void);
// 0x00000042 System.Void UnityEngine.XR.ARFoundation.ARCameraManager::Update()
extern void ARCameraManager_Update_m619CE64F8F0CCFA87D5E853CCF936B362FB58490 (void);
// 0x00000043 System.Void UnityEngine.XR.ARFoundation.ARCameraManager::OnPreRender()
extern void ARCameraManager_OnPreRender_m5945C4B5E7496C9B1F83BD6FFB5927D2C1EA8627 (void);
// 0x00000044 System.Void UnityEngine.XR.ARFoundation.ARCameraManager::OnPostRender()
extern void ARCameraManager_OnPostRender_m4355C0AA56022921C0DC378461B0918776EB3E11 (void);
// 0x00000045 System.Void UnityEngine.XR.ARFoundation.ARCameraManager::UpdateTexturesInfos()
extern void ARCameraManager_UpdateTexturesInfos_mEF1F6080FD54142B25AC1BCE9A74DBF02634A4FF (void);
// 0x00000046 System.Void UnityEngine.XR.ARFoundation.ARCameraManager::InvokeFrameReceivedEvent(UnityEngine.XR.ARSubsystems.XRCameraFrame)
extern void ARCameraManager_InvokeFrameReceivedEvent_m30221EE34D16F2E319A6FBA8EA09467174D2E225 (void);
// 0x00000047 System.Void UnityEngine.XR.ARFoundation.ARCameraManager::.ctor()
extern void ARCameraManager__ctor_m230D7CC23E284E7B5E6D2130EC244BC0D8322A1C (void);
// 0x00000048 System.Void UnityEngine.XR.ARFoundation.ARCameraManager::.cctor()
extern void ARCameraManager__cctor_m51CDF011C24CE085E7C23E09C3A301C982B5B366 (void);
// 0x00000049 UnityEngine.FilterMode UnityEngine.XR.ARFoundation.AREnvironmentProbe::get_environmentTextureFilterMode()
extern void AREnvironmentProbe_get_environmentTextureFilterMode_m2B5BA9DC0B04BAA623972475F874E1AAD252B4B7 (void);
// 0x0000004A System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbe::set_environmentTextureFilterMode(UnityEngine.FilterMode)
extern void AREnvironmentProbe_set_environmentTextureFilterMode_m5C00F6A4AD84EACEA6886012AD7C612CD031DE12 (void);
// 0x0000004B UnityEngine.XR.ARFoundation.AREnvironmentProbePlacementType UnityEngine.XR.ARFoundation.AREnvironmentProbe::get_placementType()
extern void AREnvironmentProbe_get_placementType_mDE1B7634B143C16E7A6460BEA8C06428DF7EB3ED (void);
// 0x0000004C System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbe::set_placementType(UnityEngine.XR.ARFoundation.AREnvironmentProbePlacementType)
extern void AREnvironmentProbe_set_placementType_m9F0AD7D8D186E40E64EF012B7F4291FF2ED2378D (void);
// 0x0000004D UnityEngine.Vector3 UnityEngine.XR.ARFoundation.AREnvironmentProbe::get_size()
extern void AREnvironmentProbe_get_size_mFE109731EA55FF9DDF82992686D57196B88AF8FC (void);
// 0x0000004E UnityEngine.Vector3 UnityEngine.XR.ARFoundation.AREnvironmentProbe::get_extents()
extern void AREnvironmentProbe_get_extents_mFC169B39CBD2CCCA53084795FA1F01FE7A73DEC5 (void);
// 0x0000004F System.IntPtr UnityEngine.XR.ARFoundation.AREnvironmentProbe::get_nativePtr()
extern void AREnvironmentProbe_get_nativePtr_m086E391F4F2FF3B65F2025E3BE92D9421DD3BBC3 (void);
// 0x00000050 UnityEngine.XR.ARSubsystems.XRTextureDescriptor UnityEngine.XR.ARFoundation.AREnvironmentProbe::get_textureDescriptor()
extern void AREnvironmentProbe_get_textureDescriptor_m10810988B25A99EB6FED275F48C751F2BBABA5ED (void);
// 0x00000051 System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbe::Awake()
extern void AREnvironmentProbe_Awake_mA160B91C2BB6E98C328103B21A65AF7FD162DE59 (void);
// 0x00000052 System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbe::OnAfterSetSessionRelativeData()
extern void AREnvironmentProbe_OnAfterSetSessionRelativeData_m3FA8BCBB7D951BBA0E3535EBDD784328FB401188 (void);
// 0x00000053 System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbe::UpdateEnvironmentTexture(UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void AREnvironmentProbe_UpdateEnvironmentTexture_mAFAA53AB561160343F832D9926C93196BDE4A9F5 (void);
// 0x00000054 UnityEngine.Cubemap UnityEngine.XR.ARFoundation.AREnvironmentProbe::CreateEnvironmentTexture(UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void AREnvironmentProbe_CreateEnvironmentTexture_m39D6391EA40701D6C01CA13C5533B84083988265 (void);
// 0x00000055 System.String UnityEngine.XR.ARFoundation.AREnvironmentProbe::ToString()
extern void AREnvironmentProbe_ToString_m2D9DAB7EA4B6FCCC22ED1468CFDE0CDDEB18BEA6 (void);
// 0x00000056 System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbe::.ctor()
extern void AREnvironmentProbe__ctor_m395755A4E1DD4F34B33288E042C7F417A0C287C1 (void);
// 0x00000057 System.Boolean UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::get_automaticPlacement()
extern void AREnvironmentProbeManager_get_automaticPlacement_m3AD52806C0CBA15EBD4D14AA89895AAD93FFDCAD (void);
// 0x00000058 System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::set_automaticPlacement(System.Boolean)
extern void AREnvironmentProbeManager_set_automaticPlacement_m7DB8F9C5B456A6E3C749AFF0C74ADEF31504BEBB (void);
// 0x00000059 UnityEngine.FilterMode UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::get_environmentTextureFilterMode()
extern void AREnvironmentProbeManager_get_environmentTextureFilterMode_m3CFE876910AFCF994883863B11F718DC28FD2FDF (void);
// 0x0000005A System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::set_environmentTextureFilterMode(UnityEngine.FilterMode)
extern void AREnvironmentProbeManager_set_environmentTextureFilterMode_mF837C56600DBA2FDAB9FECDB5EF200107EBE3582 (void);
// 0x0000005B UnityEngine.GameObject UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::get_debugPrefab()
extern void AREnvironmentProbeManager_get_debugPrefab_m77463EEC9592EDDEB2020C934F95F1D3AD67C1E0 (void);
// 0x0000005C System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::set_debugPrefab(UnityEngine.GameObject)
extern void AREnvironmentProbeManager_set_debugPrefab_m89082B692DFCD9010BF6FA9B229ADF579AF9018B (void);
// 0x0000005D System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::add_environmentProbesChanged(System.Action`1<UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent>)
extern void AREnvironmentProbeManager_add_environmentProbesChanged_m2D795CC563BF0C56BE5885EDEF93451E5DFDCE7D (void);
// 0x0000005E System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::remove_environmentProbesChanged(System.Action`1<UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent>)
extern void AREnvironmentProbeManager_remove_environmentProbesChanged_mCE43A47A88A4FF347D8E74B9D5FDFBAC1C3E616B (void);
// 0x0000005F UnityEngine.XR.ARFoundation.AREnvironmentProbe UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::GetEnvironmentProbe(UnityEngine.XR.ARSubsystems.TrackableId)
extern void AREnvironmentProbeManager_GetEnvironmentProbe_m7C1863C4F116EEF962B8C4135932FD4325EBCDA6 (void);
// 0x00000060 UnityEngine.XR.ARFoundation.AREnvironmentProbe UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::AddEnvironmentProbe(UnityEngine.Pose,UnityEngine.Vector3,UnityEngine.Vector3)
extern void AREnvironmentProbeManager_AddEnvironmentProbe_mA8B34934DB01DCBF00291EA5DC402786FF86D6C2 (void);
// 0x00000061 System.Boolean UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::RemoveEnvironmentProbe(UnityEngine.XR.ARFoundation.AREnvironmentProbe)
extern void AREnvironmentProbeManager_RemoveEnvironmentProbe_m9C3FAB868462C90A3DF6F7822C83115FEC4D01C8 (void);
// 0x00000062 System.String UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::get_gameObjectName()
extern void AREnvironmentProbeManager_get_gameObjectName_mDF2444D465C494D5821382055E60BF7BC160C5F0 (void);
// 0x00000063 UnityEngine.GameObject UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::GetPrefab()
extern void AREnvironmentProbeManager_GetPrefab_mA5337D413659A58A19C53B9CDD35CF58BBE7E4E0 (void);
// 0x00000064 System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::OnBeforeStart()
extern void AREnvironmentProbeManager_OnBeforeStart_m6ECC81FCA226D5605382FA59BC341F7180B1177B (void);
// 0x00000065 System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::OnDestroy()
extern void AREnvironmentProbeManager_OnDestroy_m2D98A8EA182A5A9D64D29363FF629768DA283EEC (void);
// 0x00000066 System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::OnTrackablesChanged(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.AREnvironmentProbe>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.AREnvironmentProbe>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.AREnvironmentProbe>)
extern void AREnvironmentProbeManager_OnTrackablesChanged_m5176B9A2000F304677252914562AE7095BA1FF5A (void);
// 0x00000067 System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::OnCreateTrackable(UnityEngine.XR.ARFoundation.AREnvironmentProbe)
extern void AREnvironmentProbeManager_OnCreateTrackable_m29B7CA9CA37FF0311E1CA33507A20380E5C875B9 (void);
// 0x00000068 System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::SetAutomaticPlacementStateOnSubsystem()
extern void AREnvironmentProbeManager_SetAutomaticPlacementStateOnSubsystem_m24F2292692263F84507C48E9B98C3136701FED16 (void);
// 0x00000069 System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbeManager::.ctor()
extern void AREnvironmentProbeManager__ctor_mA7BFA20548EF5E203775703BD7654626062DE75A (void);
// 0x0000006A System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.AREnvironmentProbe> UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent::get_added()
extern void AREnvironmentProbesChangedEvent_get_added_m9BB5E81DFA9C421C674F7A730155A8401E1909C4 (void);
// 0x0000006B System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent::set_added(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.AREnvironmentProbe>)
extern void AREnvironmentProbesChangedEvent_set_added_mBA9CCAEA557D0793D123E22B4DDAA8FC8B2A8F5B (void);
// 0x0000006C System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.AREnvironmentProbe> UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent::get_updated()
extern void AREnvironmentProbesChangedEvent_get_updated_m64A6BF00CB47EB9AF3D5F868254CBC3F6768681C (void);
// 0x0000006D System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent::set_updated(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.AREnvironmentProbe>)
extern void AREnvironmentProbesChangedEvent_set_updated_m1EC2DA3AD720493D88D6EE126CABCE43B1041821 (void);
// 0x0000006E System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.AREnvironmentProbe> UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent::get_removed()
extern void AREnvironmentProbesChangedEvent_get_removed_mE849182B7E2F464312887B3DDCFE153E73872CCE (void);
// 0x0000006F System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent::set_removed(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.AREnvironmentProbe>)
extern void AREnvironmentProbesChangedEvent_set_removed_mBA8F20E7B9B35B23023E5513D4F908951FA96738 (void);
// 0x00000070 System.Void UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent::.ctor(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.AREnvironmentProbe>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.AREnvironmentProbe>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.AREnvironmentProbe>)
extern void AREnvironmentProbesChangedEvent__ctor_m6B3E7444BC6E8E50407CAE8FF136B128F85B71CC (void);
// 0x00000071 System.Int32 UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent::GetHashCode()
extern void AREnvironmentProbesChangedEvent_GetHashCode_mB2DA47D276F5AFD887049840F016940EAF1913B7 (void);
// 0x00000072 System.Boolean UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent::Equals(System.Object)
extern void AREnvironmentProbesChangedEvent_Equals_m7F5FF62122376EB1CD88F50959F6A297A36E42BC (void);
// 0x00000073 System.String UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent::ToString()
extern void AREnvironmentProbesChangedEvent_ToString_m917D72B83F15A2DAABCAE4BAA80F68F9259E4304 (void);
// 0x00000074 System.Boolean UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent::Equals(UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent)
extern void AREnvironmentProbesChangedEvent_Equals_mA2A9DD5565680439D9C22E9972C4C7582B341739 (void);
// 0x00000075 System.Boolean UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent::op_Equality(UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent,UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent)
extern void AREnvironmentProbesChangedEvent_op_Equality_mAA6F2EC184F4C8A908B118C4B49AF8B831CEA209 (void);
// 0x00000076 System.Boolean UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent::op_Inequality(UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent,UnityEngine.XR.ARFoundation.AREnvironmentProbesChangedEvent)
extern void AREnvironmentProbesChangedEvent_op_Inequality_m97AD812132F5340870142AF0C6DD83B95F39FB09 (void);
// 0x00000077 System.Void UnityEngine.XR.ARFoundation.ARFace::add_updated(System.Action`1<UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs>)
extern void ARFace_add_updated_mC76C6961391C3AA87BF12ECF7B2C09C36D2A83D5 (void);
// 0x00000078 System.Void UnityEngine.XR.ARFoundation.ARFace::remove_updated(System.Action`1<UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs>)
extern void ARFace_remove_updated_mC371EEDD673DC8ED0522E766CFE73CDE92B53C40 (void);
// 0x00000079 Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARFoundation.ARFace::get_vertices()
extern void ARFace_get_vertices_m3ED72F651B9A0E73A11ADCF9E58A152631B779D5 (void);
// 0x0000007A Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARFoundation.ARFace::get_normals()
extern void ARFace_get_normals_mB78A18AC0EBB168A45292C9655D580A85C6E732E (void);
// 0x0000007B Unity.Collections.NativeArray`1<System.Int32> UnityEngine.XR.ARFoundation.ARFace::get_indices()
extern void ARFace_get_indices_m2CF54ACF97DDC886A39272B536F551A2BDE9C5BB (void);
// 0x0000007C Unity.Collections.NativeArray`1<UnityEngine.Vector2> UnityEngine.XR.ARFoundation.ARFace::get_uvs()
extern void ARFace_get_uvs_m14F8F1713F862CE6306790FA417CFE42E541E8DF (void);
// 0x0000007D System.Void UnityEngine.XR.ARFoundation.ARFace::Update()
extern void ARFace_Update_mBBDBC4E62AA22F8086B34EB97193A5A5DD557EFA (void);
// 0x0000007E System.Void UnityEngine.XR.ARFoundation.ARFace::OnDestroy()
extern void ARFace_OnDestroy_m628AAE79CC879965D3574E00C95CDBA8BA2B6F34 (void);
// 0x0000007F Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARFoundation.ARFace::GetUndisposable(Unity.Collections.NativeArray`1<T>)
// 0x00000080 System.Void UnityEngine.XR.ARFoundation.ARFace::UpdateMesh(UnityEngine.XR.ARSubsystems.XRFaceSubsystem)
extern void ARFace_UpdateMesh_m3D32D18FCA504BEFE5337E902A591B74883319D0 (void);
// 0x00000081 System.Void UnityEngine.XR.ARFoundation.ARFace::.ctor()
extern void ARFace__ctor_m22E819375DF72B8CDF3FAC2F4318673CB5A1B008 (void);
// 0x00000082 UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARFaceManager::get_facePrefab()
extern void ARFaceManager_get_facePrefab_m5E7CBC1B1A124A11B0146E55B30D0012839CFBD8 (void);
// 0x00000083 System.Void UnityEngine.XR.ARFoundation.ARFaceManager::set_facePrefab(UnityEngine.GameObject)
extern void ARFaceManager_set_facePrefab_m666350470A61048423D3175686CFCFB87B42BA18 (void);
// 0x00000084 System.Boolean UnityEngine.XR.ARFoundation.ARFaceManager::get_supported()
extern void ARFaceManager_get_supported_mDB9E6477890DAC79D3941357BCBFF745E711E288 (void);
// 0x00000085 System.Void UnityEngine.XR.ARFoundation.ARFaceManager::add_facesChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs>)
extern void ARFaceManager_add_facesChanged_mC09C76E089CE72D4D37CA6D06D258EF00E4F91C6 (void);
// 0x00000086 System.Void UnityEngine.XR.ARFoundation.ARFaceManager::remove_facesChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs>)
extern void ARFaceManager_remove_facesChanged_mF8E16DC0EA4533D67C78EB6F7AB13685A61AAF38 (void);
// 0x00000087 UnityEngine.XR.ARFoundation.ARFace UnityEngine.XR.ARFoundation.ARFaceManager::TryGetFace(UnityEngine.XR.ARSubsystems.TrackableId)
extern void ARFaceManager_TryGetFace_m54BDE8E1E445BAC4813D5140E8BB35175DF8599E (void);
// 0x00000088 System.Void UnityEngine.XR.ARFoundation.ARFaceManager::OnEnable()
extern void ARFaceManager_OnEnable_m4C4892859FFC657EB76A0AEA571FE79E328EEE43 (void);
// 0x00000089 System.Void UnityEngine.XR.ARFoundation.ARFaceManager::OnAfterSetSessionRelativeData(UnityEngine.XR.ARFoundation.ARFace,UnityEngine.XR.ARSubsystems.XRFace)
extern void ARFaceManager_OnAfterSetSessionRelativeData_m53DFEB2A0F56478FFEF84DF85B9986B0D7610A0B (void);
// 0x0000008A System.Void UnityEngine.XR.ARFoundation.ARFaceManager::OnTrackablesChanged(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARFace>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARFace>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARFace>)
extern void ARFaceManager_OnTrackablesChanged_m3AF746DD0FD5D615CC936505BF0E936B28156CEC (void);
// 0x0000008B UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARFaceManager::GetPrefab()
extern void ARFaceManager_GetPrefab_m47D3E3DFA54EB38C517D2621687759F7FBCF31BF (void);
// 0x0000008C System.String UnityEngine.XR.ARFoundation.ARFaceManager::get_gameObjectName()
extern void ARFaceManager_get_gameObjectName_m56E7CFE79A04D3CD478CDABA75F1B444D65685E0 (void);
// 0x0000008D System.Void UnityEngine.XR.ARFoundation.ARFaceManager::.ctor()
extern void ARFaceManager__ctor_m4CF8658707D546CDAF84C2A9F50FB46BE66B01D7 (void);
// 0x0000008E UnityEngine.Mesh UnityEngine.XR.ARFoundation.ARFaceMeshVisualizer::get_mesh()
extern void ARFaceMeshVisualizer_get_mesh_m235A1EDEA1D2C61A9564893316012E9D932FA6D6 (void);
// 0x0000008F System.Void UnityEngine.XR.ARFoundation.ARFaceMeshVisualizer::set_mesh(UnityEngine.Mesh)
extern void ARFaceMeshVisualizer_set_mesh_m308FC4FCE7816F2812B97FE002BBEC88D3FD6B97 (void);
// 0x00000090 System.Void UnityEngine.XR.ARFoundation.ARFaceMeshVisualizer::SetVisible(System.Boolean)
extern void ARFaceMeshVisualizer_SetVisible_m905A63BC9381E7D66C70BE6F9E63C043E6836FF5 (void);
// 0x00000091 System.Boolean UnityEngine.XR.ARFoundation.ARFaceMeshVisualizer::TryCopyToList(Unity.Collections.NativeArray`1<T>,System.Collections.Generic.List`1<T>)
// 0x00000092 System.Void UnityEngine.XR.ARFoundation.ARFaceMeshVisualizer::SetMeshTopology()
extern void ARFaceMeshVisualizer_SetMeshTopology_mD1A3878995AF31167B80C7F8D1220D306F0759B7 (void);
// 0x00000093 System.Void UnityEngine.XR.ARFoundation.ARFaceMeshVisualizer::UpdateVisibility()
extern void ARFaceMeshVisualizer_UpdateVisibility_mF26DFBAE77B1E8FF366D5153AC9D0E7ABF5E616B (void);
// 0x00000094 System.Void UnityEngine.XR.ARFoundation.ARFaceMeshVisualizer::OnUpdated(UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs)
extern void ARFaceMeshVisualizer_OnUpdated_mB54FD44BBAB032710E26DCA9096BC2ECBF3E605E (void);
// 0x00000095 System.Void UnityEngine.XR.ARFoundation.ARFaceMeshVisualizer::OnSessionStateChanged(UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs)
extern void ARFaceMeshVisualizer_OnSessionStateChanged_m251478CC19F73057DA519059B8EC04BF309CAD9B (void);
// 0x00000096 System.Void UnityEngine.XR.ARFoundation.ARFaceMeshVisualizer::Awake()
extern void ARFaceMeshVisualizer_Awake_m62DBB1C0A7D9F70198EE0EFD0173B7F230455510 (void);
// 0x00000097 System.Void UnityEngine.XR.ARFoundation.ARFaceMeshVisualizer::OnEnable()
extern void ARFaceMeshVisualizer_OnEnable_m55B7834B78827AFE7D120F0811357560890AC53B (void);
// 0x00000098 System.Void UnityEngine.XR.ARFoundation.ARFaceMeshVisualizer::OnDisable()
extern void ARFaceMeshVisualizer_OnDisable_m8882820E446917B0CF728687F98063CCB5298FAC (void);
// 0x00000099 System.Void UnityEngine.XR.ARFoundation.ARFaceMeshVisualizer::.ctor()
extern void ARFaceMeshVisualizer__ctor_m32D407CBD57E5B408B593649E79BF6EE1835228D (void);
// 0x0000009A UnityEngine.XR.ARFoundation.ARFace UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs::get_face()
extern void ARFaceUpdatedEventArgs_get_face_mECE51067B415A317E9F43CFDAD67D17E81D93CA0 (void);
// 0x0000009B System.Void UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs::set_face(UnityEngine.XR.ARFoundation.ARFace)
extern void ARFaceUpdatedEventArgs_set_face_mB64678F0DDE0D4C5CA7F729D338EDD86C04B16C4 (void);
// 0x0000009C System.Void UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs::.ctor(UnityEngine.XR.ARFoundation.ARFace)
extern void ARFaceUpdatedEventArgs__ctor_m3FB5F8EE705C0F4F9F10194CEAF835AFF04F4B81 (void);
// 0x0000009D System.Int32 UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs::GetHashCode()
extern void ARFaceUpdatedEventArgs_GetHashCode_m941B2292502C893BCAA6808A6D71F36935B470BA (void);
// 0x0000009E System.Boolean UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs::Equals(System.Object)
extern void ARFaceUpdatedEventArgs_Equals_m08148B0B8B75DA7D44C1DFE693386D7E82959F35 (void);
// 0x0000009F System.Boolean UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs)
extern void ARFaceUpdatedEventArgs_Equals_mFFCF8F02B10115669047395AF4BC245EE74ACBB9 (void);
// 0x000000A0 System.Boolean UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs::op_Equality(UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs,UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs)
extern void ARFaceUpdatedEventArgs_op_Equality_mC3C96286D6AF05F23D7590498FE23F5E42967F11 (void);
// 0x000000A1 System.Boolean UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs::op_Inequality(UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs,UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs)
extern void ARFaceUpdatedEventArgs_op_Inequality_m1304B2612248EBC6C6306515F024558B9138124A (void);
// 0x000000A2 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARFace> UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs::get_added()
extern void ARFacesChangedEventArgs_get_added_m78E85E8C2A286ADA233ACB9EE04CED7F71100889 (void);
// 0x000000A3 System.Void UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs::set_added(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARFace>)
extern void ARFacesChangedEventArgs_set_added_mFDB1B3EFA9E097B2AA4EBABCA139B79E89759AC5 (void);
// 0x000000A4 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARFace> UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs::get_updated()
extern void ARFacesChangedEventArgs_get_updated_m7877418FAC42584DA75C820DD34928418F3336BD (void);
// 0x000000A5 System.Void UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs::set_updated(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARFace>)
extern void ARFacesChangedEventArgs_set_updated_m3993021EC9472BCE3E9A993C3C4B40D5318D8352 (void);
// 0x000000A6 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARFace> UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs::get_removed()
extern void ARFacesChangedEventArgs_get_removed_m2A3A1BAC623C9D00D642422772FFC5F90961A729 (void);
// 0x000000A7 System.Void UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs::set_removed(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARFace>)
extern void ARFacesChangedEventArgs_set_removed_m670EC88FBAF24F8F7FD8CD3E1DCF57765B538CEA (void);
// 0x000000A8 System.Void UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs::.ctor(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARFace>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARFace>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARFace>)
extern void ARFacesChangedEventArgs__ctor_m4033277E36C6AE75A2561BE9E1B8B88652FDDE25 (void);
// 0x000000A9 System.Int32 UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs::GetHashCode()
extern void ARFacesChangedEventArgs_GetHashCode_mB888F6AF90BC4A28ABC14AE8A5CBA2080D404D42 (void);
// 0x000000AA System.Boolean UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs::Equals(System.Object)
extern void ARFacesChangedEventArgs_Equals_m2869D9928B1AF6540AA012371399B69CE46ADC83 (void);
// 0x000000AB System.String UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs::ToString()
extern void ARFacesChangedEventArgs_ToString_m6EE382E0E4CC5C5540D9918D346C0085E13C3C1D (void);
// 0x000000AC System.Boolean UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs)
extern void ARFacesChangedEventArgs_Equals_mE74910D42FA40C32E16E8C9D14506277BBBF4149 (void);
// 0x000000AD System.Boolean UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs::op_Equality(UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs,UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs)
extern void ARFacesChangedEventArgs_op_Equality_mD3696BD92E38AA97EA668CFB238585ED73EFE1B4 (void);
// 0x000000AE System.Boolean UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs::op_Inequality(UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs,UnityEngine.XR.ARFoundation.ARFacesChangedEventArgs)
extern void ARFacesChangedEventArgs_op_Inequality_m4D97917B41E61822D23877DB3C6B49E9CAD9BB6D (void);
// 0x000000AF System.Void UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::ResetGlState(System.Int32)
extern void ARFoundationBackgroundRenderer_ResetGlState_mF47B550F91756D479D3E93AAB9D69C674892E94E (void);
// 0x000000B0 System.Void UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::add_backgroundRendererChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARFoundationBackgroundRendererChangedEventArgs>)
extern void ARFoundationBackgroundRenderer_add_backgroundRendererChanged_mBD685A8A649DFA20BBB66AC67C6CCD0A912436F9 (void);
// 0x000000B1 System.Void UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::remove_backgroundRendererChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARFoundationBackgroundRendererChangedEventArgs>)
extern void ARFoundationBackgroundRenderer_remove_backgroundRendererChanged_m1E943EFD3D621926CE9C5A46E681D33230057EEC (void);
// 0x000000B2 UnityEngine.Material UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::get_backgroundMaterial()
extern void ARFoundationBackgroundRenderer_get_backgroundMaterial_m20B5F85B3D15F8B87ED8C77ECD85E88DEA997D48 (void);
// 0x000000B3 System.Void UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::set_backgroundMaterial(UnityEngine.Material)
extern void ARFoundationBackgroundRenderer_set_backgroundMaterial_m1D7D7F4F782DCC9B326445E46B53127CB1FC03BC (void);
// 0x000000B4 UnityEngine.Texture UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::get_backgroundTexture()
extern void ARFoundationBackgroundRenderer_get_backgroundTexture_mAAD551D8041DB863224BFD36CFE22CA560D274CE (void);
// 0x000000B5 System.Void UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::set_backgroundTexture(UnityEngine.Texture)
extern void ARFoundationBackgroundRenderer_set_backgroundTexture_m82ABDD84C3ED41274DDF32ABDE69B955775E08BF (void);
// 0x000000B6 UnityEngine.Camera UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::get_camera()
extern void ARFoundationBackgroundRenderer_get_camera_m51C36CF275B3D4BC01BACB6D3DB24EEB98B1B066 (void);
// 0x000000B7 System.Void UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::set_camera(UnityEngine.Camera)
extern void ARFoundationBackgroundRenderer_set_camera_mA26857C4B8E78DBC4E2DBE9A46D144341B1A3808 (void);
// 0x000000B8 UnityEngine.XR.ARFoundation.ARRenderMode UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::get_mode()
extern void ARFoundationBackgroundRenderer_get_mode_m592D96C6C96206856B95C594D5F7CC0F42C28050 (void);
// 0x000000B9 System.Void UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::set_mode(UnityEngine.XR.ARFoundation.ARRenderMode)
extern void ARFoundationBackgroundRenderer_set_mode_mF7A41852F4D170E441C82B2840DFF9E95142650E (void);
// 0x000000BA System.Void UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::AddOpenGLES3ResetStateCommand(UnityEngine.Rendering.CommandBuffer)
extern void ARFoundationBackgroundRenderer_AddOpenGLES3ResetStateCommand_mA8DCA9AEBAB9D006AD55A3C2F313FCF7BA7B4E9B (void);
// 0x000000BB System.Boolean UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::EnableARBackgroundRendering()
extern void ARFoundationBackgroundRenderer_EnableARBackgroundRendering_m5ECC8D3D8679D45352F6A5A59B548B7F78C95FBE (void);
// 0x000000BC System.Void UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::DisableARBackgroundRendering()
extern void ARFoundationBackgroundRenderer_DisableARBackgroundRendering_m50922D9BDCC3B4B21B344280245A386AEF5EB96C (void);
// 0x000000BD System.Boolean UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::ReapplyCommandBuffersIfNeeded()
extern void ARFoundationBackgroundRenderer_ReapplyCommandBuffersIfNeeded_mF30006D468EE5240E4DBD3EC8AD57EA997FDB9C5 (void);
// 0x000000BE System.Boolean UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::RemoveCommandBuffersIfNeeded()
extern void ARFoundationBackgroundRenderer_RemoveCommandBuffersIfNeeded_m3B40A725ED00379B6AE36D2D0501955997078C73 (void);
// 0x000000BF System.Void UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::.ctor()
extern void ARFoundationBackgroundRenderer__ctor_m59FF990B39B2C11FB45B3861CC81B1DC13FBA6D3 (void);
// 0x000000C0 System.Void UnityEngine.XR.ARFoundation.ARFoundationBackgroundRenderer::.cctor()
extern void ARFoundationBackgroundRenderer__cctor_m797A95E8E064137DDAF76ADA0A3A1F3EC12E44FD (void);
// 0x000000C1 System.Int32 UnityEngine.XR.ARFoundation.ARFoundationBackgroundRendererChangedEventArgs::GetHashCode()
extern void ARFoundationBackgroundRendererChangedEventArgs_GetHashCode_m8C37A3197487C438791442E94615A4158AB90C81 (void);
// 0x000000C2 System.Boolean UnityEngine.XR.ARFoundation.ARFoundationBackgroundRendererChangedEventArgs::Equals(System.Object)
extern void ARFoundationBackgroundRendererChangedEventArgs_Equals_mDB252BA86BC70B445DD29B5BD144B3605E764AB0 (void);
// 0x000000C3 System.Boolean UnityEngine.XR.ARFoundation.ARFoundationBackgroundRendererChangedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARFoundationBackgroundRendererChangedEventArgs)
extern void ARFoundationBackgroundRendererChangedEventArgs_Equals_m13499F72035E3D1175ACDB88B3520BDF83DF0B9C (void);
// 0x000000C4 System.Boolean UnityEngine.XR.ARFoundation.ARFoundationBackgroundRendererChangedEventArgs::op_Equality(UnityEngine.XR.ARFoundation.ARFoundationBackgroundRendererChangedEventArgs,UnityEngine.XR.ARFoundation.ARFoundationBackgroundRendererChangedEventArgs)
extern void ARFoundationBackgroundRendererChangedEventArgs_op_Equality_mED1A70ACCDF2FFD4EBA30C95B52EBC60BE198C9F (void);
// 0x000000C5 System.Boolean UnityEngine.XR.ARFoundation.ARFoundationBackgroundRendererChangedEventArgs::op_Inequality(UnityEngine.XR.ARFoundation.ARFoundationBackgroundRendererChangedEventArgs,UnityEngine.XR.ARFoundation.ARFoundationBackgroundRendererChangedEventArgs)
extern void ARFoundationBackgroundRendererChangedEventArgs_op_Inequality_m9CE525D585E83043185B4D11CE2C673FC6FD3567 (void);
// 0x000000C6 UnityEngine.XR.XRInputSubsystem UnityEngine.XR.ARFoundation.ARInputManager::get_subsystem()
extern void ARInputManager_get_subsystem_mB6B87FB0E7A0CE152C8C193EE9DE3ECFEFD450A2 (void);
// 0x000000C7 System.Void UnityEngine.XR.ARFoundation.ARInputManager::set_subsystem(UnityEngine.XR.XRInputSubsystem)
extern void ARInputManager_set_subsystem_m9B6400C1A5A4E932C742193CC5F0E774664A27AA (void);
// 0x000000C8 System.Void UnityEngine.XR.ARFoundation.ARInputManager::OnEnable()
extern void ARInputManager_OnEnable_m43ADB54D8FBE01265C77AAED9680C80A5B75432A (void);
// 0x000000C9 System.Void UnityEngine.XR.ARFoundation.ARInputManager::OnDisable()
extern void ARInputManager_OnDisable_m109E1AF8CAB37C2BD574D326C58BDACCFD49FDAF (void);
// 0x000000CA System.Void UnityEngine.XR.ARFoundation.ARInputManager::OnDestroy()
extern void ARInputManager_OnDestroy_mD610DE54A4C1396CF3399518496CA6CB5E8ACCA1 (void);
// 0x000000CB System.Void UnityEngine.XR.ARFoundation.ARInputManager::CreateSubsystemIfNecessary()
extern void ARInputManager_CreateSubsystemIfNecessary_m8E21A700CBB9A3C694D622900047944D762A7CFE (void);
// 0x000000CC UnityEngine.XR.XRInputSubsystem UnityEngine.XR.ARFoundation.ARInputManager::CreateSubsystem()
extern void ARInputManager_CreateSubsystem_mBC202793130ED485EF3ADD88055ABA3085645FBF (void);
// 0x000000CD UnityEngine.XR.XRInputSubsystem UnityEngine.XR.ARFoundation.ARInputManager::GetActiveSubsystemInstance()
extern void ARInputManager_GetActiveSubsystemInstance_mBC10E6AD7F1B4319B544B60C6B85F79759EDB308 (void);
// 0x000000CE System.Void UnityEngine.XR.ARFoundation.ARInputManager::.ctor()
extern void ARInputManager__ctor_mAF16A842A222B3303CB2050409E91EC060255A47 (void);
// 0x000000CF System.Void UnityEngine.XR.ARFoundation.ARInputManager::.cctor()
extern void ARInputManager__cctor_m83B727E8827BE46BFA66249FBE442D7124CB8464 (void);
// 0x000000D0 System.Nullable`1<System.Single> UnityEngine.XR.ARFoundation.ARLightEstimationData::get_averageBrightness()
extern void ARLightEstimationData_get_averageBrightness_mB339C036AE0CBCF8DAA8FCDE4A4D04A19EFB7233 (void);
// 0x000000D1 System.Void UnityEngine.XR.ARFoundation.ARLightEstimationData::set_averageBrightness(System.Nullable`1<System.Single>)
extern void ARLightEstimationData_set_averageBrightness_m9C0FEF62B371B067D51E1E930A42ABE40F0EEDAB (void);
// 0x000000D2 System.Nullable`1<System.Single> UnityEngine.XR.ARFoundation.ARLightEstimationData::get_averageColorTemperature()
extern void ARLightEstimationData_get_averageColorTemperature_m7C37859EB80C2606270F72DEF3E292299CE81778 (void);
// 0x000000D3 System.Void UnityEngine.XR.ARFoundation.ARLightEstimationData::set_averageColorTemperature(System.Nullable`1<System.Single>)
extern void ARLightEstimationData_set_averageColorTemperature_mDCDE66CD7D7D2BD24434DE00D68B171449D9207A (void);
// 0x000000D4 System.Nullable`1<UnityEngine.Color> UnityEngine.XR.ARFoundation.ARLightEstimationData::get_colorCorrection()
extern void ARLightEstimationData_get_colorCorrection_mD318D06DB8535937FD4BB5E5DEF1BDE6471A2A2D (void);
// 0x000000D5 System.Void UnityEngine.XR.ARFoundation.ARLightEstimationData::set_colorCorrection(System.Nullable`1<UnityEngine.Color>)
extern void ARLightEstimationData_set_colorCorrection_m7551D5F797A9E203BAB64416C4B1637848A7EBBD (void);
// 0x000000D6 System.Int32 UnityEngine.XR.ARFoundation.ARLightEstimationData::GetHashCode()
extern void ARLightEstimationData_GetHashCode_mE8A3FF44F64AEC6C3FCFF1629777C45EDD89B06D (void);
// 0x000000D7 System.Boolean UnityEngine.XR.ARFoundation.ARLightEstimationData::Equals(System.Object)
extern void ARLightEstimationData_Equals_mAB9336917F38BCB1ACD25EFE8B7F6C07C979F478 (void);
// 0x000000D8 System.String UnityEngine.XR.ARFoundation.ARLightEstimationData::ToString()
extern void ARLightEstimationData_ToString_m4068C6C855EABAA601CB607A080453B094479DE1 (void);
// 0x000000D9 System.Boolean UnityEngine.XR.ARFoundation.ARLightEstimationData::Equals(UnityEngine.XR.ARFoundation.ARLightEstimationData)
extern void ARLightEstimationData_Equals_m2A61749DD4B3227BE1727779503F7580C05F8495 (void);
// 0x000000DA System.Boolean UnityEngine.XR.ARFoundation.ARLightEstimationData::op_Equality(UnityEngine.XR.ARFoundation.ARLightEstimationData,UnityEngine.XR.ARFoundation.ARLightEstimationData)
extern void ARLightEstimationData_op_Equality_m35E79A4E46AE792EEB1E0807D438062442DCF520 (void);
// 0x000000DB System.Boolean UnityEngine.XR.ARFoundation.ARLightEstimationData::op_Inequality(UnityEngine.XR.ARFoundation.ARLightEstimationData,UnityEngine.XR.ARFoundation.ARLightEstimationData)
extern void ARLightEstimationData_op_Inequality_m11532BCDCA49F6DEDD6AC3C6AFD2CDDE06197D22 (void);
// 0x000000DC UnityEngine.MeshFilter UnityEngine.XR.ARFoundation.ARMeshManager::get_meshPrefab()
extern void ARMeshManager_get_meshPrefab_mE0271046EBF41B280F14B0A773666C73A3489CC7 (void);
// 0x000000DD System.Void UnityEngine.XR.ARFoundation.ARMeshManager::set_meshPrefab(UnityEngine.MeshFilter)
extern void ARMeshManager_set_meshPrefab_m08BFB7AC287E9B89554E750B9A0179A00D979577 (void);
// 0x000000DE System.Single UnityEngine.XR.ARFoundation.ARMeshManager::get_density()
extern void ARMeshManager_get_density_m0EDF78AE3FB79684768E68F5D9FFD946237DAFD0 (void);
// 0x000000DF System.Void UnityEngine.XR.ARFoundation.ARMeshManager::set_density(System.Single)
extern void ARMeshManager_set_density_m485913E0ED10AF606DFE6D1095D24ADE15FE2C39 (void);
// 0x000000E0 System.Boolean UnityEngine.XR.ARFoundation.ARMeshManager::get_normals()
extern void ARMeshManager_get_normals_m5DE2A8C09BDEDC026C46C45EA807C0D86D9AD4CF (void);
// 0x000000E1 System.Void UnityEngine.XR.ARFoundation.ARMeshManager::set_normals(System.Boolean)
extern void ARMeshManager_set_normals_m7112C481FE146A20CBE5A2B90AC12020EE94A8BB (void);
// 0x000000E2 System.Boolean UnityEngine.XR.ARFoundation.ARMeshManager::get_tangents()
extern void ARMeshManager_get_tangents_m3FE960BCDD1A46FC6F1040E9966CFA5F2698AEF0 (void);
// 0x000000E3 System.Void UnityEngine.XR.ARFoundation.ARMeshManager::set_tangents(System.Boolean)
extern void ARMeshManager_set_tangents_mE6A70D1D30FC468DBEB157FF7D654BFFCB9E1DD9 (void);
// 0x000000E4 System.Boolean UnityEngine.XR.ARFoundation.ARMeshManager::get_textureCoordinates()
extern void ARMeshManager_get_textureCoordinates_m5F144CC814A5AE853C6CDA1819FB96FA04FF2D0C (void);
// 0x000000E5 System.Void UnityEngine.XR.ARFoundation.ARMeshManager::set_textureCoordinates(System.Boolean)
extern void ARMeshManager_set_textureCoordinates_m5AA472A4D24260428DF732B432B6BC6DE3905AA8 (void);
// 0x000000E6 System.Boolean UnityEngine.XR.ARFoundation.ARMeshManager::get_colors()
extern void ARMeshManager_get_colors_mDE61B9163BD95DB03519E46A17E6E267C5DC1A44 (void);
// 0x000000E7 System.Void UnityEngine.XR.ARFoundation.ARMeshManager::set_colors(System.Boolean)
extern void ARMeshManager_set_colors_m6B87CDCAF924BAB0F6FAE89A312F417B7054C9A2 (void);
// 0x000000E8 System.Int32 UnityEngine.XR.ARFoundation.ARMeshManager::get_concurrentQueueSize()
extern void ARMeshManager_get_concurrentQueueSize_m11826C3996CC1A1F2DF7F495781DBF95AC00BA0D (void);
// 0x000000E9 System.Void UnityEngine.XR.ARFoundation.ARMeshManager::set_concurrentQueueSize(System.Int32)
extern void ARMeshManager_set_concurrentQueueSize_mF7841046A6C36BD67EEEF6CB0B184A6EBD812D26 (void);
// 0x000000EA System.Void UnityEngine.XR.ARFoundation.ARMeshManager::add_meshesChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs>)
extern void ARMeshManager_add_meshesChanged_mEE9E53B27ADB4427DCFA369F9D0C913B356DF4E4 (void);
// 0x000000EB System.Void UnityEngine.XR.ARFoundation.ARMeshManager::remove_meshesChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs>)
extern void ARMeshManager_remove_meshesChanged_mA46004B024552977D9495EBBE41A031E6CE7699C (void);
// 0x000000EC UnityEngine.XR.XRMeshSubsystem UnityEngine.XR.ARFoundation.ARMeshManager::get_subsystem()
extern void ARMeshManager_get_subsystem_m75AF924F0383AEAEC0046E1D5B552314F2D86909 (void);
// 0x000000ED System.Collections.Generic.IList`1<UnityEngine.MeshFilter> UnityEngine.XR.ARFoundation.ARMeshManager::get_meshes()
extern void ARMeshManager_get_meshes_mDF28D58C7C7D3021F01B15A99CCC49AA754392C4 (void);
// 0x000000EE System.Void UnityEngine.XR.ARFoundation.ARMeshManager::DestroyAllMeshes()
extern void ARMeshManager_DestroyAllMeshes_m9DC72181A5DA9211A2B4D7776965A3F0BDE1B550 (void);
// 0x000000EF UnityEngine.XR.ARFoundation.ARSessionOrigin UnityEngine.XR.ARFoundation.ARMeshManager::GetSessionOrigin()
extern void ARMeshManager_GetSessionOrigin_m1AA18CD54C9C3E42C4A9A38CB21E139714F79B9F (void);
// 0x000000F0 System.Void UnityEngine.XR.ARFoundation.ARMeshManager::SetBoundingVolume()
extern void ARMeshManager_SetBoundingVolume_m11BC2D4DF62C5440AA4EE24526F5740453E34EED (void);
// 0x000000F1 System.Void UnityEngine.XR.ARFoundation.ARMeshManager::OnEnable()
extern void ARMeshManager_OnEnable_mCA928F5B676C85E3DB01A9DA19F71EC45968B9EE (void);
// 0x000000F2 System.Void UnityEngine.XR.ARFoundation.ARMeshManager::OnDrawGizmosSelected()
extern void ARMeshManager_OnDrawGizmosSelected_mD1FD9FAB0E4653F71A230706D53A20AFC0B7099C (void);
// 0x000000F3 System.Void UnityEngine.XR.ARFoundation.ARMeshManager::Update()
extern void ARMeshManager_Update_mF2A044D4D9F005948F9BBEAF64159EC6D0599358 (void);
// 0x000000F4 System.Void UnityEngine.XR.ARFoundation.ARMeshManager::Generate()
extern void ARMeshManager_Generate_m239A013DAB4790B83EB74ACD9095917EBBC60D0B (void);
// 0x000000F5 System.Void UnityEngine.XR.ARFoundation.ARMeshManager::OnMeshGenerated(UnityEngine.XR.MeshGenerationResult)
extern void ARMeshManager_OnMeshGenerated_m981F27D548806FD4D1F4FF74AE0431BCC3D6777E (void);
// 0x000000F6 System.Void UnityEngine.XR.ARFoundation.ARMeshManager::UpdateMeshInfos()
extern void ARMeshManager_UpdateMeshInfos_m2F704B4E3F5BE441508F6C52F9538D5E57749E1B (void);
// 0x000000F7 System.Void UnityEngine.XR.ARFoundation.ARMeshManager::OnDisable()
extern void ARMeshManager_OnDisable_mE8C25004398E1C9EC52A22BDDCA7984CACCA76E8 (void);
// 0x000000F8 System.Void UnityEngine.XR.ARFoundation.ARMeshManager::OnDestroy()
extern void ARMeshManager_OnDestroy_m8CB92235F351A9052FE60F59789598D57B4069A0 (void);
// 0x000000F9 UnityEngine.MeshFilter UnityEngine.XR.ARFoundation.ARMeshManager::GetOrCreateMeshFilter(UnityEngine.XR.ARSubsystems.TrackableId)
extern void ARMeshManager_GetOrCreateMeshFilter_m2CBE90F9D53D72AD6F5E3E0CBC4D3EB000CFE54D (void);
// 0x000000FA UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARFoundation.ARMeshManager::GetTrackableId(UnityEngine.XR.MeshId)
extern void ARMeshManager_GetTrackableId_m66F6AB46495F45B31EBFFC9E4547845190631E11 (void);
// 0x000000FB UnityEngine.XR.MeshId UnityEngine.XR.ARFoundation.ARMeshManager::GetLegacyMeshId(UnityEngine.XR.ARSubsystems.TrackableId)
extern void ARMeshManager_GetLegacyMeshId_m03296A7F349BAE02872FE64406C8B82D6DE71B88 (void);
// 0x000000FC System.Void UnityEngine.XR.ARFoundation.ARMeshManager::Awake()
extern void ARMeshManager_Awake_m415C9B21B176C501BD3E694600BF1B0679D7B912 (void);
// 0x000000FD UnityEngine.XR.XRMeshSubsystem UnityEngine.XR.ARFoundation.ARMeshManager::CreateSubsystem()
extern void ARMeshManager_CreateSubsystem_mC8DCAE337AAE273FF351430114BA866CFE3F02FA (void);
// 0x000000FE System.Void UnityEngine.XR.ARFoundation.ARMeshManager::.ctor()
extern void ARMeshManager__ctor_m193FA3F74B01501D99FF8AE57CE68CC71650DE6C (void);
// 0x000000FF System.Void UnityEngine.XR.ARFoundation.ARMeshManager::.cctor()
extern void ARMeshManager__cctor_m9FA9AF5610FBD1AC1254EA99140EA969D043E315 (void);
// 0x00000100 System.Collections.Generic.List`1<UnityEngine.MeshFilter> UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs::get_added()
extern void ARMeshesChangedEventArgs_get_added_m416B22A9406017FC5E40EB473774CE9149EB1BC7 (void);
// 0x00000101 System.Void UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs::set_added(System.Collections.Generic.List`1<UnityEngine.MeshFilter>)
extern void ARMeshesChangedEventArgs_set_added_m07AC944AFF173C597B045BB60E40BCEE02A646FF (void);
// 0x00000102 System.Collections.Generic.List`1<UnityEngine.MeshFilter> UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs::get_updated()
extern void ARMeshesChangedEventArgs_get_updated_m3E49A6C32750983B744168851591634FDA4B542F (void);
// 0x00000103 System.Void UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs::set_updated(System.Collections.Generic.List`1<UnityEngine.MeshFilter>)
extern void ARMeshesChangedEventArgs_set_updated_m2A97C612DD863055AE6CEA0D1598E687A93EEAEF (void);
// 0x00000104 System.Collections.Generic.List`1<UnityEngine.MeshFilter> UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs::get_removed()
extern void ARMeshesChangedEventArgs_get_removed_m88699CFBAE47A01A6C053A351E02D32B7C61E084 (void);
// 0x00000105 System.Void UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs::set_removed(System.Collections.Generic.List`1<UnityEngine.MeshFilter>)
extern void ARMeshesChangedEventArgs_set_removed_mB3D40BCE5693E2436C19CA66879C0C1F3B57756A (void);
// 0x00000106 System.Void UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs::.ctor(System.Collections.Generic.List`1<UnityEngine.MeshFilter>,System.Collections.Generic.List`1<UnityEngine.MeshFilter>,System.Collections.Generic.List`1<UnityEngine.MeshFilter>)
extern void ARMeshesChangedEventArgs__ctor_m40CF8F7E678F5D03DEB1AE5AE6330F6F6B2958EE (void);
// 0x00000107 System.Int32 UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs::GetHashCode()
extern void ARMeshesChangedEventArgs_GetHashCode_m7E79DE6C2468873DC4CD40005D850221480E34E6 (void);
// 0x00000108 System.Boolean UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs::Equals(System.Object)
extern void ARMeshesChangedEventArgs_Equals_mB538D8CA2C6C59B16D1F67E354D49FB6B47B0EE4 (void);
// 0x00000109 System.String UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs::ToString()
extern void ARMeshesChangedEventArgs_ToString_m0ED4C46371B9B64DAA5956A5A90CF5933B300AFF (void);
// 0x0000010A System.Boolean UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs)
extern void ARMeshesChangedEventArgs_Equals_m20539FBB146BA19C19AE6996CF6FD53492C57A8F (void);
// 0x0000010B System.Boolean UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs::op_Equality(UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs,UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs)
extern void ARMeshesChangedEventArgs_op_Equality_m5FDE7E6EC10551B14E832F26F210BBBE9785807F (void);
// 0x0000010C System.Boolean UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs::op_Inequality(UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs,UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs)
extern void ARMeshesChangedEventArgs_op_Inequality_mC4CA3029E394F285609C6179EA9380EE673A65DE (void);
// 0x0000010D System.Single UnityEngine.XR.ARFoundation.ARPlane::get_vertexChangedThreshold()
extern void ARPlane_get_vertexChangedThreshold_m7D0635EBA7DE0BFA18D4F9C83C74E78CB841E18F (void);
// 0x0000010E System.Void UnityEngine.XR.ARFoundation.ARPlane::set_vertexChangedThreshold(System.Single)
extern void ARPlane_set_vertexChangedThreshold_m1C7F8CD3037AE652731CA77B23E63826F3CA23A6 (void);
// 0x0000010F System.Void UnityEngine.XR.ARFoundation.ARPlane::add_boundaryChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs>)
extern void ARPlane_add_boundaryChanged_m29CFF978ACA0B60DE6BA14688B7A113C3EDA2E31 (void);
// 0x00000110 System.Void UnityEngine.XR.ARFoundation.ARPlane::remove_boundaryChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs>)
extern void ARPlane_remove_boundaryChanged_m49F278652DDB9BE23D3CB939B061C848C1E7B06C (void);
// 0x00000111 UnityEngine.Vector3 UnityEngine.XR.ARFoundation.ARPlane::get_normal()
extern void ARPlane_get_normal_mA454AC0BDC53BB29B23252CA933C2488B1D65E05 (void);
// 0x00000112 UnityEngine.XR.ARFoundation.ARPlane UnityEngine.XR.ARFoundation.ARPlane::get_subsumedBy()
extern void ARPlane_get_subsumedBy_mBE978F9E4BD0B0FE8544457CC4770E69378F66AA (void);
// 0x00000113 System.Void UnityEngine.XR.ARFoundation.ARPlane::set_subsumedBy(UnityEngine.XR.ARFoundation.ARPlane)
extern void ARPlane_set_subsumedBy_m9E1CF16AFEA932801A0F20B93577A717EF3A6278 (void);
// 0x00000114 UnityEngine.XR.ARSubsystems.PlaneAlignment UnityEngine.XR.ARFoundation.ARPlane::get_alignment()
extern void ARPlane_get_alignment_m48654B659D193EEF5DB28095ABF7B1BDBB3A9BD5 (void);
// 0x00000115 UnityEngine.Vector2 UnityEngine.XR.ARFoundation.ARPlane::get_centerInPlaneSpace()
extern void ARPlane_get_centerInPlaneSpace_m0111B536A21E35BA1B70651EF6BC6EF3CD407529 (void);
// 0x00000116 UnityEngine.Vector3 UnityEngine.XR.ARFoundation.ARPlane::get_center()
extern void ARPlane_get_center_m30B400A4510C2F9D4412E74C0ADC97DD8892EB07 (void);
// 0x00000117 UnityEngine.Vector2 UnityEngine.XR.ARFoundation.ARPlane::get_extents()
extern void ARPlane_get_extents_m23C7BEC8A2485EB82E7F7EE665304BF1AE431690 (void);
// 0x00000118 UnityEngine.Vector2 UnityEngine.XR.ARFoundation.ARPlane::get_size()
extern void ARPlane_get_size_m716429374DDDEBE4C5D5C4EE4094F16F8A96D2BD (void);
// 0x00000119 UnityEngine.Plane UnityEngine.XR.ARFoundation.ARPlane::get_infinitePlane()
extern void ARPlane_get_infinitePlane_m3E5A49987D9B10446EE57CA081ABF611D9151BDE (void);
// 0x0000011A System.IntPtr UnityEngine.XR.ARFoundation.ARPlane::get_nativePtr()
extern void ARPlane_get_nativePtr_m98D11221375A16A9CA121C162F7E9D1758BAE72E (void);
// 0x0000011B Unity.Collections.NativeArray`1<UnityEngine.Vector2> UnityEngine.XR.ARFoundation.ARPlane::get_boundary()
extern void ARPlane_get_boundary_mE2365FCC5D0232A1F5F444844B93CCD01FFC2559 (void);
// 0x0000011C System.Void UnityEngine.XR.ARFoundation.ARPlane::UpdateBoundary(UnityEngine.XR.ARSubsystems.XRPlaneSubsystem)
extern void ARPlane_UpdateBoundary_m53446E5350109A75B9F96A3042EE037E4521D7B9 (void);
// 0x0000011D System.Void UnityEngine.XR.ARFoundation.ARPlane::OnValidate()
extern void ARPlane_OnValidate_m4A8849A53B054ED7977324349B8275250D0C5C04 (void);
// 0x0000011E System.Void UnityEngine.XR.ARFoundation.ARPlane::OnDestroy()
extern void ARPlane_OnDestroy_m92A8E4C2946F18356B563F3B34D7ACA657F221E5 (void);
// 0x0000011F System.Void UnityEngine.XR.ARFoundation.ARPlane::CheckForBoundaryChanges()
extern void ARPlane_CheckForBoundaryChanges_m6E300E7748E9F1CC241347A7A519231B2BCF5D96 (void);
// 0x00000120 System.Void UnityEngine.XR.ARFoundation.ARPlane::CopyBoundaryAndSetChangedFlag()
extern void ARPlane_CopyBoundaryAndSetChangedFlag_mEBE6B4CFD45C3865D5304CD78D9F792D248375F0 (void);
// 0x00000121 System.Void UnityEngine.XR.ARFoundation.ARPlane::Update()
extern void ARPlane_Update_mCD9290EFD789F73B747101141CAD1FD944F886B2 (void);
// 0x00000122 System.Void UnityEngine.XR.ARFoundation.ARPlane::.ctor()
extern void ARPlane__ctor_mEFCE074188D91A380B684E00E1EB3A8633856BC9 (void);
// 0x00000123 UnityEngine.XR.ARFoundation.ARPlane UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::get_plane()
extern void ARPlaneBoundaryChangedEventArgs_get_plane_mE14F32BD989ADD2F0FBDBAB9AB293CDAB4CCB03B (void);
// 0x00000124 System.Void UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::set_plane(UnityEngine.XR.ARFoundation.ARPlane)
extern void ARPlaneBoundaryChangedEventArgs_set_plane_m5DC6678054CF729894BBE1BB359ECD6ABB21C018 (void);
// 0x00000125 System.Void UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::.ctor(UnityEngine.XR.ARFoundation.ARPlane)
extern void ARPlaneBoundaryChangedEventArgs__ctor_mCF92554C8AB31E6FB30004DE9480EE749767720D (void);
// 0x00000126 System.Int32 UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::GetHashCode()
extern void ARPlaneBoundaryChangedEventArgs_GetHashCode_m6380A99937DA7544630D3E7D025BCCBA75C92E2C (void);
// 0x00000127 System.Boolean UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::Equals(System.Object)
extern void ARPlaneBoundaryChangedEventArgs_Equals_mCAE90FE42E59A12583A7EA34758578304D10BEC4 (void);
// 0x00000128 System.String UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::ToString()
extern void ARPlaneBoundaryChangedEventArgs_ToString_m459F343E2184B7AFE40965471117B25F7783F873 (void);
// 0x00000129 System.Boolean UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARPlaneBoundaryChangedEventArgs_Equals_m5168B0124859C641CFE47EF454D01E1C42BBE2AA (void);
// 0x0000012A System.Boolean UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::op_Equality(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs,UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARPlaneBoundaryChangedEventArgs_op_Equality_m4E6BC2B245E0AC2361F18C7C8E9D8B39864A9A44 (void);
// 0x0000012B System.Boolean UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::op_Inequality(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs,UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARPlaneBoundaryChangedEventArgs_op_Inequality_m6B6C4B7437B45CE4E5E43D931E45EFB1D364AE09 (void);
// 0x0000012C UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARPlaneManager::get_planePrefab()
extern void ARPlaneManager_get_planePrefab_m420C466CEE70BB4900C7C8BB699FF4CB288DB1C9 (void);
// 0x0000012D System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::set_planePrefab(UnityEngine.GameObject)
extern void ARPlaneManager_set_planePrefab_mA8D37D90B73308F931871C17C30CEAE1AD0B5922 (void);
// 0x0000012E UnityEngine.XR.ARSubsystems.PlaneDetectionMode UnityEngine.XR.ARFoundation.ARPlaneManager::get_detectionMode()
extern void ARPlaneManager_get_detectionMode_m9213906802A76764C2B75D2BC4DC3CF838EEE76B (void);
// 0x0000012F System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::set_detectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void ARPlaneManager_set_detectionMode_mDBF9B7E80B9785A2288AB78EF6BCF0BBE4CB2BD9 (void);
// 0x00000130 System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::add_planesChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs>)
extern void ARPlaneManager_add_planesChanged_m9866605669B3B910AAC344FF40D4195686404D2B (void);
// 0x00000131 System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::remove_planesChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs>)
extern void ARPlaneManager_remove_planesChanged_m3DDBB16626916DD0F6F0BAA1FEF5AA695B924846 (void);
// 0x00000132 UnityEngine.XR.ARFoundation.ARPlane UnityEngine.XR.ARFoundation.ARPlaneManager::GetPlane(UnityEngine.XR.ARSubsystems.TrackableId)
extern void ARPlaneManager_GetPlane_m07E47479942F326E19BEFCFF4599579637A03227 (void);
// 0x00000133 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARFoundation.ARPlaneManager::Raycast(UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void ARPlaneManager_Raycast_m7598B32F45C2E8612B131D13C4E1AB283F006C67 (void);
// 0x00000134 System.Single UnityEngine.XR.ARFoundation.ARPlaneManager::GetCrossDirection(UnityEngine.Vector2,UnityEngine.Vector2)
extern void ARPlaneManager_GetCrossDirection_mC2F352184BDF9B212CA8E3A0A2FDDD60F8645006 (void);
// 0x00000135 System.Int32 UnityEngine.XR.ARFoundation.ARPlaneManager::WindingNumber(UnityEngine.Vector2,Unity.Collections.NativeArray`1<UnityEngine.Vector2>)
extern void ARPlaneManager_WindingNumber_m181C484F9F116133407CDC939D5B3898A2B5D873 (void);
// 0x00000136 UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARPlaneManager::GetPrefab()
extern void ARPlaneManager_GetPrefab_m45F6E55A085930E2EC7D01737F6E6A4CCDE8832B (void);
// 0x00000137 System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::OnBeforeStart()
extern void ARPlaneManager_OnBeforeStart_mB9D2CBA5D2B05C6A524F9692DD57FB0AE9263383 (void);
// 0x00000138 System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::OnAfterSetSessionRelativeData(UnityEngine.XR.ARFoundation.ARPlane,UnityEngine.XR.ARSubsystems.BoundedPlane)
extern void ARPlaneManager_OnAfterSetSessionRelativeData_m5824123176AA49FD51C6EACEA8B521D97B775502 (void);
// 0x00000139 System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::OnTrackablesChanged(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>)
extern void ARPlaneManager_OnTrackablesChanged_mC594D8189E68C158025CD7F467D992BDA2076A6C (void);
// 0x0000013A System.String UnityEngine.XR.ARFoundation.ARPlaneManager::get_gameObjectName()
extern void ARPlaneManager_get_gameObjectName_m5D2D94A1E80B8C7240D2717225FF4F2CA2AFA2F7 (void);
// 0x0000013B System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::OnEnable()
extern void ARPlaneManager_OnEnable_m251D7B94FD6766A3F26806ADAAFA3AD73E142C4F (void);
// 0x0000013C System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::OnDisable()
extern void ARPlaneManager_OnDisable_mA42AED97FC24125D7FF7A16B8EF29361A4DF885A (void);
// 0x0000013D System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::.ctor()
extern void ARPlaneManager__ctor_m3D15D9E2AF6D14B0613EABB82FDD465D7A3BA111 (void);
// 0x0000013E System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::.cctor()
extern void ARPlaneManager__cctor_m3F6450FDA4A33161452FB66D0998E7FF315E22CF (void);
// 0x0000013F System.Boolean UnityEngine.XR.ARFoundation.ARPlaneMeshGenerators::GenerateMesh(UnityEngine.Mesh,UnityEngine.Pose,Unity.Collections.NativeArray`1<UnityEngine.Vector2>,System.Single)
extern void ARPlaneMeshGenerators_GenerateMesh_m31B11251353F699F456EFF5307CF35C8EA48BD76 (void);
// 0x00000140 System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshGenerators::GenerateUvs(System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Pose,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void ARPlaneMeshGenerators_GenerateUvs_mBC2A77E6B10BC1331B3F0036C57A532665D37173 (void);
// 0x00000141 System.Boolean UnityEngine.XR.ARFoundation.ARPlaneMeshGenerators::GenerateIndices(System.Collections.Generic.List`1<System.Int32>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single)
extern void ARPlaneMeshGenerators_GenerateIndices_mD8B027666DC55E2DA3A4F251D61E03E2C2D05AF1 (void);
// 0x00000142 System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshGenerators::.cctor()
extern void ARPlaneMeshGenerators__cctor_mE3BF52F68272A91EDBF6A51E128B3E268210B0D5 (void);
// 0x00000143 UnityEngine.Mesh UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::get_mesh()
extern void ARPlaneMeshVisualizer_get_mesh_m74A28DC83789468390901498492ECD91A58D9D88 (void);
// 0x00000144 System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::set_mesh(UnityEngine.Mesh)
extern void ARPlaneMeshVisualizer_set_mesh_m8B42F851E8410BED82E67F135AD0A3AB7C6A9DC7 (void);
// 0x00000145 System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::OnBoundaryChanged(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARPlaneMeshVisualizer_OnBoundaryChanged_m1787BC532ABCA0DB773DA98D6DE54D9FA840BA4B (void);
// 0x00000146 System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::DisableComponents()
extern void ARPlaneMeshVisualizer_DisableComponents_m49A77252B75A9ABFDBF17FFE43C2E264A0CE9BF5 (void);
// 0x00000147 System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::SetVisible(System.Boolean)
extern void ARPlaneMeshVisualizer_SetVisible_m3278C868A00EF82B5D863CC92B1D5980C7B893BB (void);
// 0x00000148 System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::UpdateVisibility()
extern void ARPlaneMeshVisualizer_UpdateVisibility_m893626BA96546175A81B5DC60EDA9AFA1BB1C725 (void);
// 0x00000149 System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::Awake()
extern void ARPlaneMeshVisualizer_Awake_mDF40B09B5BCD21A6FD95B137386FA76174678107 (void);
// 0x0000014A System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::OnEnable()
extern void ARPlaneMeshVisualizer_OnEnable_mBF3848FD5ED4106FF4B298B0459500EF28C3B0A9 (void);
// 0x0000014B System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::OnDisable()
extern void ARPlaneMeshVisualizer_OnDisable_m2F868BBB379E9D9A34BED36C2F4192518EF69A99 (void);
// 0x0000014C System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::Update()
extern void ARPlaneMeshVisualizer_Update_m5A0802B6146884AAA3358E08CD786C208EE7B747 (void);
// 0x0000014D System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::.ctor()
extern void ARPlaneMeshVisualizer__ctor_mCB3EC94CAE888A2D5219D840A92FA6E0652E1955 (void);
// 0x0000014E System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane> UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::get_added()
extern void ARPlanesChangedEventArgs_get_added_m26B6F1AEAD621FD8A939521235C7B45AE08FDA1D (void);
// 0x0000014F System.Void UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::set_added(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>)
extern void ARPlanesChangedEventArgs_set_added_m363F2A1024103B4E741741E4BBB09F952293E21B (void);
// 0x00000150 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane> UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::get_updated()
extern void ARPlanesChangedEventArgs_get_updated_m674C4C76BC3C8A2D49793B254B103021A4E33C5A (void);
// 0x00000151 System.Void UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::set_updated(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>)
extern void ARPlanesChangedEventArgs_set_updated_mDB2AA3FD2A483CE633ABBDBBEC8C5193B421D5A6 (void);
// 0x00000152 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane> UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::get_removed()
extern void ARPlanesChangedEventArgs_get_removed_m5F0AC740E7C61322659EC224A4EEC4DBC8B3E405 (void);
// 0x00000153 System.Void UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::set_removed(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>)
extern void ARPlanesChangedEventArgs_set_removed_mD249F70F7D051CE0030B6D671DE8FDE8CEEDD860 (void);
// 0x00000154 System.Void UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::.ctor(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>)
extern void ARPlanesChangedEventArgs__ctor_m88626999299D44A1F2D39060A37CCE03B6A7B51C (void);
// 0x00000155 System.Int32 UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::GetHashCode()
extern void ARPlanesChangedEventArgs_GetHashCode_m12A40FC12326E1B13B615985A5CC188ADA83088B (void);
// 0x00000156 System.Boolean UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::Equals(System.Object)
extern void ARPlanesChangedEventArgs_Equals_m004DBCB6C164D7FBAA6EA23D6FDDD7730795EFEE (void);
// 0x00000157 System.String UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::ToString()
extern void ARPlanesChangedEventArgs_ToString_mB3CAC22890529BED6B5FA38DAE92D2DFC250DB72 (void);
// 0x00000158 System.Boolean UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs)
extern void ARPlanesChangedEventArgs_Equals_m82227BEF1235CDE33ADCE37382B7964629CFBAD9 (void);
// 0x00000159 System.Boolean UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::op_Equality(UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs,UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs)
extern void ARPlanesChangedEventArgs_op_Equality_m1BD9BEF2A02C5E7EDB68A5F493C6BFD2EF5806D8 (void);
// 0x0000015A System.Boolean UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::op_Inequality(UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs,UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs)
extern void ARPlanesChangedEventArgs_op_Inequality_mD61DA03E0C14E646E81D6C37C5B0B9B878887F91 (void);
// 0x0000015B System.Void UnityEngine.XR.ARFoundation.ARPointCloud::add_updated(System.Action`1<UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs>)
extern void ARPointCloud_add_updated_mFA6E98BC9EE3C6E1D632E0A22FCF96C6670158A1 (void);
// 0x0000015C System.Void UnityEngine.XR.ARFoundation.ARPointCloud::remove_updated(System.Action`1<UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs>)
extern void ARPointCloud_remove_updated_m9DD76FD146C7BFE6BA0AFD672700B75D71474F17 (void);
// 0x0000015D Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARFoundation.ARPointCloud::get_positions()
extern void ARPointCloud_get_positions_m9FBA4F24BEE6BD88710890321082897B648D4813 (void);
// 0x0000015E Unity.Collections.NativeArray`1<System.UInt64> UnityEngine.XR.ARFoundation.ARPointCloud::get_identifiers()
extern void ARPointCloud_get_identifiers_m6A0A77296BB198B2F42B4A14B5468A1E56F01921 (void);
// 0x0000015F Unity.Collections.NativeArray`1<System.Single> UnityEngine.XR.ARFoundation.ARPointCloud::get_confidenceValues()
extern void ARPointCloud_get_confidenceValues_mD9E3378A48FA4CF0E504320C14B78255BAE989BB (void);
// 0x00000160 System.Void UnityEngine.XR.ARFoundation.ARPointCloud::Update()
extern void ARPointCloud_Update_mC992FA846C53C7F28DED02D49C8BA06D3A44B198 (void);
// 0x00000161 System.Void UnityEngine.XR.ARFoundation.ARPointCloud::OnDestroy()
extern void ARPointCloud_OnDestroy_mDADD946B93109041238570823C7A067CC81B3119 (void);
// 0x00000162 Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARFoundation.ARPointCloud::GetUndisposable(Unity.Collections.NativeArray`1<T>)
// 0x00000163 System.Void UnityEngine.XR.ARFoundation.ARPointCloud::UpdateData(UnityEngine.XR.ARSubsystems.XRDepthSubsystem)
extern void ARPointCloud_UpdateData_m8EAB6AB87441F91F462CD2D130334177750FB7C3 (void);
// 0x00000164 System.Void UnityEngine.XR.ARFoundation.ARPointCloud::.ctor()
extern void ARPointCloud__ctor_m526C7A48B22B46189646D842885CBB84FDCD23C3 (void);
// 0x00000165 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPointCloud> UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs::get_added()
extern void ARPointCloudChangedEventArgs_get_added_m9F934F1FA3322402E50F72E9B78669042367DF75 (void);
// 0x00000166 System.Void UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs::set_added(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPointCloud>)
extern void ARPointCloudChangedEventArgs_set_added_m21B8ABD00EFDA37DB3409B10A07F45B26C34F9B6 (void);
// 0x00000167 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPointCloud> UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs::get_updated()
extern void ARPointCloudChangedEventArgs_get_updated_mCE2A84457D78408052DAF251ACFC8D591D9B0FBA (void);
// 0x00000168 System.Void UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs::set_updated(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPointCloud>)
extern void ARPointCloudChangedEventArgs_set_updated_mDCB2376E57C283B084A04870BAAFB597989D45C3 (void);
// 0x00000169 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPointCloud> UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs::get_removed()
extern void ARPointCloudChangedEventArgs_get_removed_mCAFAC3A438975DE62575CB41D02A46ABD8C67842 (void);
// 0x0000016A System.Void UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs::set_removed(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPointCloud>)
extern void ARPointCloudChangedEventArgs_set_removed_m36A3675CA4A35C7061EA7F5EEF114C56F932A076 (void);
// 0x0000016B System.Void UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs::.ctor(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPointCloud>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPointCloud>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPointCloud>)
extern void ARPointCloudChangedEventArgs__ctor_m0BF590246450830BDA1923F1C79B383BD032FD40 (void);
// 0x0000016C System.Int32 UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs::GetHashCode()
extern void ARPointCloudChangedEventArgs_GetHashCode_m28D5E34EB0E025E21935BF0FD1E46A35C49FC04C (void);
// 0x0000016D System.Boolean UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs::Equals(System.Object)
extern void ARPointCloudChangedEventArgs_Equals_m6BD50C01CB9435D9CBC7BE0F913199CAD3C38D4D (void);
// 0x0000016E System.String UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs::ToString()
extern void ARPointCloudChangedEventArgs_ToString_m96CAAE9D0926B9A3CE4FC16D942AE7571EDC83A5 (void);
// 0x0000016F System.Boolean UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs)
extern void ARPointCloudChangedEventArgs_Equals_mF2BB005AFBD3B0418D29AAC35DDE9566C23A14FE (void);
// 0x00000170 System.Boolean UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs::op_Equality(UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs,UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs)
extern void ARPointCloudChangedEventArgs_op_Equality_mE45AAA1C835846CFA39B30C8B7168AC2D408D160 (void);
// 0x00000171 System.Boolean UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs::op_Inequality(UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs,UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs)
extern void ARPointCloudChangedEventArgs_op_Inequality_mD405A9CB1E1A52C1FBBA44FE4CCF4A8529E0EDEC (void);
// 0x00000172 UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARPointCloudManager::get_pointCloudPrefab()
extern void ARPointCloudManager_get_pointCloudPrefab_m8EF0DDF8D272A4C7703411CA7E05B36DAD94DD66 (void);
// 0x00000173 System.Void UnityEngine.XR.ARFoundation.ARPointCloudManager::set_pointCloudPrefab(UnityEngine.GameObject)
extern void ARPointCloudManager_set_pointCloudPrefab_m1B13F4CCD6D4F1A85783CEBC1567EC00D7AC4D5D (void);
// 0x00000174 System.Void UnityEngine.XR.ARFoundation.ARPointCloudManager::add_pointCloudsChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs>)
extern void ARPointCloudManager_add_pointCloudsChanged_mA1FE3FBDADFA9B534ABBDA0668FA6FEC279D0A84 (void);
// 0x00000175 System.Void UnityEngine.XR.ARFoundation.ARPointCloudManager::remove_pointCloudsChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs>)
extern void ARPointCloudManager_remove_pointCloudsChanged_m4AD9312BDE821627A52C95948D70D35BBD1E527E (void);
// 0x00000176 System.Void UnityEngine.XR.ARFoundation.ARPointCloudManager::OnEnable()
extern void ARPointCloudManager_OnEnable_m916C5325A4B95E562704DAAF6EA9D9FAA75E3CA8 (void);
// 0x00000177 System.Void UnityEngine.XR.ARFoundation.ARPointCloudManager::OnDisable()
extern void ARPointCloudManager_OnDisable_m28A3932B1892E865675642D30EE32F5FED805662 (void);
// 0x00000178 UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARPointCloudManager::GetPrefab()
extern void ARPointCloudManager_GetPrefab_m0D7BF714EB65584D01D4D61D03AD21874B770124 (void);
// 0x00000179 System.String UnityEngine.XR.ARFoundation.ARPointCloudManager::get_gameObjectName()
extern void ARPointCloudManager_get_gameObjectName_m6A0CE6B2D0F0E8ADE18FAEA2E60BEC4B234A31DD (void);
// 0x0000017A System.Void UnityEngine.XR.ARFoundation.ARPointCloudManager::OnAfterSetSessionRelativeData(UnityEngine.XR.ARFoundation.ARPointCloud,UnityEngine.XR.ARSubsystems.XRPointCloud)
extern void ARPointCloudManager_OnAfterSetSessionRelativeData_mF7E960689FB8C1273EC77771101D5BFF39F7BFE9 (void);
// 0x0000017B System.Void UnityEngine.XR.ARFoundation.ARPointCloudManager::OnTrackablesChanged(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPointCloud>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPointCloud>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPointCloud>)
extern void ARPointCloudManager_OnTrackablesChanged_mAC96B361D7FF8582531603E0BEEDF7CDF96AD552 (void);
// 0x0000017C Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARFoundation.ARPointCloudManager::Raycast(UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void ARPointCloudManager_Raycast_mBCB5C701727AECBA0BA2B6BE721B8A45F1F9E105 (void);
// 0x0000017D System.Void UnityEngine.XR.ARFoundation.ARPointCloudManager::Append(Unity.Collections.NativeArray`1<T>&,Unity.Collections.NativeArray`1<T>,System.Int32,Unity.Collections.Allocator)
// 0x0000017E System.Void UnityEngine.XR.ARFoundation.ARPointCloudManager::.ctor()
extern void ARPointCloudManager__ctor_m5FC517F01824F0201DA0DF05C3777CA3FAA6BA3D (void);
// 0x0000017F UnityEngine.Mesh UnityEngine.XR.ARFoundation.ARPointCloudMeshVisualizer::get_mesh()
extern void ARPointCloudMeshVisualizer_get_mesh_mAF6BC728E087BCD55EDBB2E58E78F1C1CAA5943F (void);
// 0x00000180 System.Void UnityEngine.XR.ARFoundation.ARPointCloudMeshVisualizer::set_mesh(UnityEngine.Mesh)
extern void ARPointCloudMeshVisualizer_set_mesh_mCA2DE27C102B4BCCD4B99E2FB926047FF1EACB97 (void);
// 0x00000181 System.Void UnityEngine.XR.ARFoundation.ARPointCloudMeshVisualizer::OnPointCloudChanged(UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs)
extern void ARPointCloudMeshVisualizer_OnPointCloudChanged_m7DC671156F5FCA53233A42B4089C8DEC5EEE99EB (void);
// 0x00000182 System.Void UnityEngine.XR.ARFoundation.ARPointCloudMeshVisualizer::Awake()
extern void ARPointCloudMeshVisualizer_Awake_m0A6B44AF804A7E0B27255C4EB7669984EF18A257 (void);
// 0x00000183 System.Void UnityEngine.XR.ARFoundation.ARPointCloudMeshVisualizer::OnEnable()
extern void ARPointCloudMeshVisualizer_OnEnable_m4D9E23AE2939C6744852822BC5CF5F85A5BA9A59 (void);
// 0x00000184 System.Void UnityEngine.XR.ARFoundation.ARPointCloudMeshVisualizer::OnDisable()
extern void ARPointCloudMeshVisualizer_OnDisable_mCA54B26FFE4FB41E91B214957534333C48740298 (void);
// 0x00000185 System.Void UnityEngine.XR.ARFoundation.ARPointCloudMeshVisualizer::Update()
extern void ARPointCloudMeshVisualizer_Update_m7CC89E91D9AF8548A9701521AE6C7CA243B3F69B (void);
// 0x00000186 System.Void UnityEngine.XR.ARFoundation.ARPointCloudMeshVisualizer::UpdateVisibility()
extern void ARPointCloudMeshVisualizer_UpdateVisibility_mCBF198BCE48526537B22FF32D18893EC589F5FA9 (void);
// 0x00000187 System.Void UnityEngine.XR.ARFoundation.ARPointCloudMeshVisualizer::SetVisible(System.Boolean)
extern void ARPointCloudMeshVisualizer_SetVisible_m8528922AE9887CCB8B8FA1E4B91870DBB9251838 (void);
// 0x00000188 System.Void UnityEngine.XR.ARFoundation.ARPointCloudMeshVisualizer::.ctor()
extern void ARPointCloudMeshVisualizer__ctor_mDBE2EE53EF0534363673948ABC8E3FF0F418196A (void);
// 0x00000189 System.Void UnityEngine.XR.ARFoundation.ARPointCloudMeshVisualizer::.cctor()
extern void ARPointCloudMeshVisualizer__cctor_mA2CFF704CAB1E0C4AB2C6E702B244B9DAD22708C (void);
// 0x0000018A System.Void UnityEngine.XR.ARFoundation.ARPointCloudParticleVisualizer::OnPointCloudChanged(UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs)
extern void ARPointCloudParticleVisualizer_OnPointCloudChanged_m2BDE581222E5D2FE8D6D6279C5ABD76633ACD9D0 (void);
// 0x0000018B System.Void UnityEngine.XR.ARFoundation.ARPointCloudParticleVisualizer::Awake()
extern void ARPointCloudParticleVisualizer_Awake_mB4A2A5BB3362F4C44CD92F2A9888E8DBEA527804 (void);
// 0x0000018C System.Void UnityEngine.XR.ARFoundation.ARPointCloudParticleVisualizer::OnEnable()
extern void ARPointCloudParticleVisualizer_OnEnable_m7E41D69E0949FF17E8191C9A7DDC551C5D7290AF (void);
// 0x0000018D System.Void UnityEngine.XR.ARFoundation.ARPointCloudParticleVisualizer::OnDisable()
extern void ARPointCloudParticleVisualizer_OnDisable_m883CEDCF81909D00A28CC2460C43CB539F9DCC5F (void);
// 0x0000018E System.Void UnityEngine.XR.ARFoundation.ARPointCloudParticleVisualizer::Update()
extern void ARPointCloudParticleVisualizer_Update_m288ED8AF843475DD435FFACE738CA83AEE6654E5 (void);
// 0x0000018F System.Void UnityEngine.XR.ARFoundation.ARPointCloudParticleVisualizer::UpdateVisibility()
extern void ARPointCloudParticleVisualizer_UpdateVisibility_m615D4D3A73AF2A05CEEB21578F6911B0580121DB (void);
// 0x00000190 System.Void UnityEngine.XR.ARFoundation.ARPointCloudParticleVisualizer::SetVisible(System.Boolean)
extern void ARPointCloudParticleVisualizer_SetVisible_mE63C4F71B57F739D63EE40788AA79870ABCDF0AA (void);
// 0x00000191 System.Void UnityEngine.XR.ARFoundation.ARPointCloudParticleVisualizer::.ctor()
extern void ARPointCloudParticleVisualizer__ctor_m3F48BAD9F776C522DAA0C6624B98E54B33B81C57 (void);
// 0x00000192 System.Void UnityEngine.XR.ARFoundation.ARPointCloudParticleVisualizer::.cctor()
extern void ARPointCloudParticleVisualizer__cctor_m9C79727F3A5E81150BFC5F74696068975E296608 (void);
// 0x00000193 System.Int32 UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs::GetHashCode()
extern void ARPointCloudUpdatedEventArgs_GetHashCode_mD52F0A42535E9D590F2065D60644655D80DC3C22 (void);
// 0x00000194 System.Boolean UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs::Equals(System.Object)
extern void ARPointCloudUpdatedEventArgs_Equals_mBD558C0754E1A1C6BA9864C5891A282F6B977C9E (void);
// 0x00000195 System.Boolean UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs)
extern void ARPointCloudUpdatedEventArgs_Equals_mB39450D04B879FE647C18AB10C409523B5FF4195 (void);
// 0x00000196 System.Boolean UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs::op_Equality(UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs,UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs)
extern void ARPointCloudUpdatedEventArgs_op_Equality_m65F86E115A3625AD876ECD8C6BF47E62B176F14E (void);
// 0x00000197 System.Boolean UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs::op_Inequality(UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs,UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs)
extern void ARPointCloudUpdatedEventArgs_op_Inequality_m7638C0B46DC1DD9E9A8872226A4B93E6C82EED2C (void);
// 0x00000198 System.Void UnityEngine.XR.ARFoundation.ARRaycastHit::.ctor(UnityEngine.XR.ARSubsystems.XRRaycastHit,System.Single,UnityEngine.Transform)
extern void ARRaycastHit__ctor_m7E37CAC9301AAB566F3B8751091D3E472BB5C2E8 (void);
// 0x00000199 System.Single UnityEngine.XR.ARFoundation.ARRaycastHit::get_distance()
extern void ARRaycastHit_get_distance_m14A7CD73EDA065E5E722BB48BD2FB3420BC6CFDC (void);
// 0x0000019A System.Void UnityEngine.XR.ARFoundation.ARRaycastHit::set_distance(System.Single)
extern void ARRaycastHit_set_distance_m04D1E140673EA5F48185A2C0C78D138957D7010E (void);
// 0x0000019B UnityEngine.XR.ARSubsystems.TrackableType UnityEngine.XR.ARFoundation.ARRaycastHit::get_hitType()
extern void ARRaycastHit_get_hitType_m9F24C8C20D2985ACFBBAD4F7EEA509BE470CFB00 (void);
// 0x0000019C UnityEngine.Pose UnityEngine.XR.ARFoundation.ARRaycastHit::get_pose()
extern void ARRaycastHit_get_pose_m5CCFFED6C4A101EA42083A8661956A2B4B4C4A0D (void);
// 0x0000019D UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARFoundation.ARRaycastHit::get_trackableId()
extern void ARRaycastHit_get_trackableId_m6FBBEF54882143C2EE439CE919D131CD71EBE972 (void);
// 0x0000019E UnityEngine.Pose UnityEngine.XR.ARFoundation.ARRaycastHit::get_sessionRelativePose()
extern void ARRaycastHit_get_sessionRelativePose_m0BFDEFFEE9453DE6A3EBF1F478D04C6CCD1E93AE (void);
// 0x0000019F System.Single UnityEngine.XR.ARFoundation.ARRaycastHit::get_sessionRelativeDistance()
extern void ARRaycastHit_get_sessionRelativeDistance_mDCDED72A207F7C1D59BFE84815C181A29AF8C059 (void);
// 0x000001A0 System.Int32 UnityEngine.XR.ARFoundation.ARRaycastHit::GetHashCode()
extern void ARRaycastHit_GetHashCode_m0ADC3CEFC9A9617A4F61616CE9981A98D7926525 (void);
// 0x000001A1 System.Boolean UnityEngine.XR.ARFoundation.ARRaycastHit::Equals(System.Object)
extern void ARRaycastHit_Equals_mDA8FEFBD8AB244631961E9C9EA43D3A1B5D2F894 (void);
// 0x000001A2 System.Boolean UnityEngine.XR.ARFoundation.ARRaycastHit::Equals(UnityEngine.XR.ARFoundation.ARRaycastHit)
extern void ARRaycastHit_Equals_mE086AFC2E5237C054CC2D38B22B7506E892B090E (void);
// 0x000001A3 System.Boolean UnityEngine.XR.ARFoundation.ARRaycastHit::op_Equality(UnityEngine.XR.ARFoundation.ARRaycastHit,UnityEngine.XR.ARFoundation.ARRaycastHit)
extern void ARRaycastHit_op_Equality_mB8DDC0E9E24BFF9332A78AB325989A7903C9095E (void);
// 0x000001A4 System.Boolean UnityEngine.XR.ARFoundation.ARRaycastHit::op_Inequality(UnityEngine.XR.ARFoundation.ARRaycastHit,UnityEngine.XR.ARFoundation.ARRaycastHit)
extern void ARRaycastHit_op_Inequality_mFCA5D48E720A6FE838F6AF3CE58C0D85FF7371B5 (void);
// 0x000001A5 System.Int32 UnityEngine.XR.ARFoundation.ARRaycastHit::CompareTo(UnityEngine.XR.ARFoundation.ARRaycastHit)
extern void ARRaycastHit_CompareTo_m3F5B58CE479D3DDB7503B9C288C497AF2E3AE99E (void);
// 0x000001A6 System.Boolean UnityEngine.XR.ARFoundation.ARRaycastManager::Raycast(UnityEngine.Vector2,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit>,UnityEngine.XR.ARSubsystems.TrackableType)
extern void ARRaycastManager_Raycast_m2F43B2CAF3D7C66183720D3980BD0CEB42E8F393 (void);
// 0x000001A7 System.Boolean UnityEngine.XR.ARFoundation.ARRaycastManager::Raycast(UnityEngine.Ray,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit>,UnityEngine.XR.ARSubsystems.TrackableType)
extern void ARRaycastManager_Raycast_m65B31E8B76200A372429F98D2065E0D20CD7F768 (void);
// 0x000001A8 System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::TransformAndSortRaycastResults(UnityEngine.Transform,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit>,UnityEngine.Vector3)
extern void ARRaycastManager_TransformAndSortRaycastResults_m026F42706BDE4904A69862B9562F27D01762F987 (void);
// 0x000001A9 System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::RegisterRaycaster(UnityEngine.XR.ARFoundation.IRaycaster)
extern void ARRaycastManager_RegisterRaycaster_m6A40E4C8E52CF603432C13E0048EA64521E75418 (void);
// 0x000001AA System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::UnregisterRaycaster(UnityEngine.XR.ARFoundation.IRaycaster)
extern void ARRaycastManager_UnregisterRaycaster_mEED237BC2FDEA7DCCD8AEB2C46A618C5357B22C4 (void);
// 0x000001AB System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::OnAfterStart()
extern void ARRaycastManager_OnAfterStart_m177F70E50FD2E573CF4D688773269380CA0E5C73 (void);
// 0x000001AC Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARFoundation.ARRaycastManager::RaycastViewportAsRay(UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void ARRaycastManager_RaycastViewportAsRay_m4482E0D42A28C0AB579EEA16B359897DAC247C87 (void);
// 0x000001AD Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARFoundation.ARRaycastManager::RaycastViewport(UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void ARRaycastManager_RaycastViewport_mBD7C00F6EEF1CA76E80ED9C3EA2EAFCAA2C7405D (void);
// 0x000001AE Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARFoundation.ARRaycastManager::RaycastRay(UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void ARRaycastManager_RaycastRay_m16F2A0CB579339B8E9467DB8A93BFE34804D3B22 (void);
// 0x000001AF System.Int32 UnityEngine.XR.ARFoundation.ARRaycastManager::RaycastHitComparer(UnityEngine.XR.ARFoundation.ARRaycastHit,UnityEngine.XR.ARFoundation.ARRaycastHit)
extern void ARRaycastManager_RaycastHitComparer_mF66FE66AAD0E8B1C223B8363CC5404543986AF54 (void);
// 0x000001B0 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARFoundation.ARRaycastManager::RaycastFallback(UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void ARRaycastManager_RaycastFallback_m2F2CFEBE5FC835576D03745E899818115084885D (void);
// 0x000001B1 System.Boolean UnityEngine.XR.ARFoundation.ARRaycastManager::TransformAndDisposeNativeHitResults(Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit>,UnityEngine.Vector3)
extern void ARRaycastManager_TransformAndDisposeNativeHitResults_m1E00EAF176C4264D6402C0FAA1A82AEED41CE545 (void);
// 0x000001B2 System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::ConstructIfNecessary()
extern void ARRaycastManager_ConstructIfNecessary_m6D8C34B3EBAAA21205EEF23FDC100A3CD0FDC4EE (void);
// 0x000001B3 System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::Awake()
extern void ARRaycastManager_Awake_mD449B9047D9C92A0257735EFCCD5EEFDB6B9B00A (void);
// 0x000001B4 System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::.ctor()
extern void ARRaycastManager__ctor_mF86FCE8D20CD564A99EAD6820D17A93CE0CE8906 (void);
// 0x000001B5 System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::.cctor()
extern void ARRaycastManager__cctor_mB93E1AC04873B8406D58B274ED18F89DAD31000C (void);
// 0x000001B6 System.IntPtr UnityEngine.XR.ARFoundation.ARReferencePoint::get_nativePtr()
extern void ARReferencePoint_get_nativePtr_m3A5EBB10CDB229BEDF6F9A32DB344F7D7E60C425 (void);
// 0x000001B7 System.Void UnityEngine.XR.ARFoundation.ARReferencePoint::.ctor()
extern void ARReferencePoint__ctor_mBF1A6D8BB55E72D2421479015293E6983284B492 (void);
// 0x000001B8 UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARReferencePointManager::get_referencePointPrefab()
extern void ARReferencePointManager_get_referencePointPrefab_m0B582D7592957A7B3FBB56FF9FE89023EA09A4A7 (void);
// 0x000001B9 System.Void UnityEngine.XR.ARFoundation.ARReferencePointManager::set_referencePointPrefab(UnityEngine.GameObject)
extern void ARReferencePointManager_set_referencePointPrefab_m2099BFBB205C215C5ABE952CB9FC27D7990DE980 (void);
// 0x000001BA System.Void UnityEngine.XR.ARFoundation.ARReferencePointManager::add_referencePointsChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs>)
extern void ARReferencePointManager_add_referencePointsChanged_m5BDB05B2E802FDE47B989FED8B630905E0F631AE (void);
// 0x000001BB System.Void UnityEngine.XR.ARFoundation.ARReferencePointManager::remove_referencePointsChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs>)
extern void ARReferencePointManager_remove_referencePointsChanged_mFF6485AE35BB1B27D458622285FE7B901DD934CE (void);
// 0x000001BC UnityEngine.XR.ARFoundation.ARReferencePoint UnityEngine.XR.ARFoundation.ARReferencePointManager::AddReferencePoint(UnityEngine.Pose)
extern void ARReferencePointManager_AddReferencePoint_m69E8A182F4236F19B516DB2A7533D40366F737C1 (void);
// 0x000001BD UnityEngine.XR.ARFoundation.ARReferencePoint UnityEngine.XR.ARFoundation.ARReferencePointManager::AttachReferencePoint(UnityEngine.XR.ARFoundation.ARPlane,UnityEngine.Pose)
extern void ARReferencePointManager_AttachReferencePoint_mDAB5752D0A684707D0B8A5F994D58A939482DFAF (void);
// 0x000001BE System.Boolean UnityEngine.XR.ARFoundation.ARReferencePointManager::RemoveReferencePoint(UnityEngine.XR.ARFoundation.ARReferencePoint)
extern void ARReferencePointManager_RemoveReferencePoint_m1AC3A26D9ADC6861C20DA44C66FF8C445C4031CA (void);
// 0x000001BF UnityEngine.XR.ARFoundation.ARReferencePoint UnityEngine.XR.ARFoundation.ARReferencePointManager::GetReferencePoint(UnityEngine.XR.ARSubsystems.TrackableId)
extern void ARReferencePointManager_GetReferencePoint_mB7E635DBEEB46117809F74000544885B6276C736 (void);
// 0x000001C0 UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARReferencePointManager::GetPrefab()
extern void ARReferencePointManager_GetPrefab_mBCCAE5E2794AEB9C7A71036FDE1A52E72F570E3E (void);
// 0x000001C1 System.String UnityEngine.XR.ARFoundation.ARReferencePointManager::get_gameObjectName()
extern void ARReferencePointManager_get_gameObjectName_mC6C01BD63D4E5F83A030F0719CA89FEA1597EE8D (void);
// 0x000001C2 System.Void UnityEngine.XR.ARFoundation.ARReferencePointManager::OnTrackablesChanged(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARReferencePoint>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARReferencePoint>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARReferencePoint>)
extern void ARReferencePointManager_OnTrackablesChanged_mF782BA36C8A0584FE4A43098C6250EE0A0AC49DB (void);
// 0x000001C3 System.Void UnityEngine.XR.ARFoundation.ARReferencePointManager::.ctor()
extern void ARReferencePointManager__ctor_mD7E1B733733585176F2239B8FB5BCB70AE57D027 (void);
// 0x000001C4 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARReferencePoint> UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs::get_added()
extern void ARReferencePointsChangedEventArgs_get_added_mABB62AC87103053E3FE9610B0242D97C8B6B949A (void);
// 0x000001C5 System.Void UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs::set_added(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARReferencePoint>)
extern void ARReferencePointsChangedEventArgs_set_added_mC4359784851149411C6182FD35628B46E9394BA0 (void);
// 0x000001C6 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARReferencePoint> UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs::get_updated()
extern void ARReferencePointsChangedEventArgs_get_updated_mE33F7C73AB2E3D7329A351F4CFF05A9F1651DD72 (void);
// 0x000001C7 System.Void UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs::set_updated(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARReferencePoint>)
extern void ARReferencePointsChangedEventArgs_set_updated_m31B2E7CDD0D3F91C6F816967FB894A5474C044EB (void);
// 0x000001C8 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARReferencePoint> UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs::get_removed()
extern void ARReferencePointsChangedEventArgs_get_removed_m6D5B3717AC83FB6F1CB0C332D90903E7396F142A (void);
// 0x000001C9 System.Void UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs::set_removed(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARReferencePoint>)
extern void ARReferencePointsChangedEventArgs_set_removed_m7E1839AA6025FE381D0B66153360ED5102627397 (void);
// 0x000001CA System.Void UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs::.ctor(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARReferencePoint>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARReferencePoint>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARReferencePoint>)
extern void ARReferencePointsChangedEventArgs__ctor_mDA783F2639DF702E800713983C3995142AE05894 (void);
// 0x000001CB System.Int32 UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs::GetHashCode()
extern void ARReferencePointsChangedEventArgs_GetHashCode_m64B6156588CC25C2724164B57E539D5214A3CC03 (void);
// 0x000001CC System.Boolean UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs::Equals(System.Object)
extern void ARReferencePointsChangedEventArgs_Equals_mEDD233C7AF5F3E512CBD8D5DCE01C4563BB40801 (void);
// 0x000001CD System.String UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs::ToString()
extern void ARReferencePointsChangedEventArgs_ToString_mEEA97C554FAC8D8247313ED6003B55211C503BFB (void);
// 0x000001CE System.Boolean UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs)
extern void ARReferencePointsChangedEventArgs_Equals_m872DC192FEA15EC16A6E6B6884A420ECBBF3A887 (void);
// 0x000001CF System.Boolean UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs::op_Equality(UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs,UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs)
extern void ARReferencePointsChangedEventArgs_op_Equality_m1866410B0B84DFD0CCDF36BBEA8C2D507F23C55A (void);
// 0x000001D0 System.Boolean UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs::op_Inequality(UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs,UnityEngine.XR.ARFoundation.ARReferencePointsChangedEventArgs)
extern void ARReferencePointsChangedEventArgs_op_Inequality_m86A7E7867272015EA605293E9F94EC2EC4636711 (void);
// 0x000001D1 System.Boolean UnityEngine.XR.ARFoundation.ARSession::get_attemptUpdate()
extern void ARSession_get_attemptUpdate_m0A4410AE45968D8DB68B3F3B27C19D5DE1B73D24 (void);
// 0x000001D2 System.Void UnityEngine.XR.ARFoundation.ARSession::set_attemptUpdate(System.Boolean)
extern void ARSession_set_attemptUpdate_mFF7E5FBE5A24F0B19FF23FB852D4A0563D47F273 (void);
// 0x000001D3 System.Boolean UnityEngine.XR.ARFoundation.ARSession::get_matchFrameRate()
extern void ARSession_get_matchFrameRate_m59AE94947F30496E5A38E6AB6BB51F848E0BFE04 (void);
// 0x000001D4 System.Void UnityEngine.XR.ARFoundation.ARSession::set_matchFrameRate(System.Boolean)
extern void ARSession_set_matchFrameRate_m224070636779F553E69CBD52112A7518B5FE3373 (void);
// 0x000001D5 System.Void UnityEngine.XR.ARFoundation.ARSession::add_stateChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs>)
extern void ARSession_add_stateChanged_m4D2004D8967341410C3726EE786C3ABA971D937C (void);
// 0x000001D6 System.Void UnityEngine.XR.ARFoundation.ARSession::remove_stateChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs>)
extern void ARSession_remove_stateChanged_mF32B8A10ED41FD873374EE0F3C3A8F550CD912B1 (void);
// 0x000001D7 UnityEngine.XR.ARFoundation.ARSessionState UnityEngine.XR.ARFoundation.ARSession::get_state()
extern void ARSession_get_state_m6B8AE895247F4F9A8D76749E447637BC7FBEE639 (void);
// 0x000001D8 System.Void UnityEngine.XR.ARFoundation.ARSession::set_state(UnityEngine.XR.ARFoundation.ARSessionState)
extern void ARSession_set_state_mBBCFE2FBD1A465FCBEAFAC911F648FA1C8EAE71B (void);
// 0x000001D9 UnityEngine.XR.ARSubsystems.NotTrackingReason UnityEngine.XR.ARFoundation.ARSession::get_notTrackingReason()
extern void ARSession_get_notTrackingReason_mAE083BBF91B9C076B53887EEF6C823DA8720EF1C (void);
// 0x000001DA System.Void UnityEngine.XR.ARFoundation.ARSession::Reset()
extern void ARSession_Reset_mFAD3C148FDE97C7A90B4E8C166F93C3C2D11894D (void);
// 0x000001DB System.Void UnityEngine.XR.ARFoundation.ARSession::SetMatchFrameRateEnabled(System.Boolean)
extern void ARSession_SetMatchFrameRateEnabled_m3F0CC4B76EB7EF2E963DC13E1DF154B1CB49007C (void);
// 0x000001DC System.Void UnityEngine.XR.ARFoundation.ARSession::WarnIfMultipleARSessions()
extern void ARSession_WarnIfMultipleARSessions_mFB245A2EAF514D74AB16343680013B836999994E (void);
// 0x000001DD System.Collections.IEnumerator UnityEngine.XR.ARFoundation.ARSession::CheckAvailability()
extern void ARSession_CheckAvailability_mCB651B104BBF6ED4C3041D86C625EC1178318931 (void);
// 0x000001DE System.Collections.IEnumerator UnityEngine.XR.ARFoundation.ARSession::Install()
extern void ARSession_Install_m4C15ED3F78EE143E765FE98CE54351BBBD2BBCB2 (void);
// 0x000001DF System.Void UnityEngine.XR.ARFoundation.ARSession::OnEnable()
extern void ARSession_OnEnable_m278A9E1F0715381458F24B6834AFD930C1A4E750 (void);
// 0x000001E0 System.Collections.IEnumerator UnityEngine.XR.ARFoundation.ARSession::Initialize()
extern void ARSession_Initialize_mA14AF59D418374508717C2E885A4BFFA3F4F1817 (void);
// 0x000001E1 System.Void UnityEngine.XR.ARFoundation.ARSession::StartSubsystem()
extern void ARSession_StartSubsystem_mF5BC087C1EDE948B5BC4CA6ADD47E004A31EDCC0 (void);
// 0x000001E2 System.Void UnityEngine.XR.ARFoundation.ARSession::Awake()
extern void ARSession_Awake_m0E45C4E48B5352BF288BC0392186061C0076B531 (void);
// 0x000001E3 System.Void UnityEngine.XR.ARFoundation.ARSession::Update()
extern void ARSession_Update_m5F845E6E9DACEF91167155BA894CBA73AFB5BED6 (void);
// 0x000001E4 System.Void UnityEngine.XR.ARFoundation.ARSession::OnApplicationPause(System.Boolean)
extern void ARSession_OnApplicationPause_m701633E71A1517E0D89DB0D4348CF00297A40A35 (void);
// 0x000001E5 System.Void UnityEngine.XR.ARFoundation.ARSession::OnDisable()
extern void ARSession_OnDisable_m518EAC900DFA0E9553BF08382C18A9F813DB1D14 (void);
// 0x000001E6 System.Void UnityEngine.XR.ARFoundation.ARSession::OnDestroy()
extern void ARSession_OnDestroy_m5163A3F69D7739D0BAD0046D423561BBE457F27B (void);
// 0x000001E7 System.Void UnityEngine.XR.ARFoundation.ARSession::UpdateNotTrackingReason()
extern void ARSession_UpdateNotTrackingReason_m5F58154AC1A855819FD0C952708FB85C2515170A (void);
// 0x000001E8 System.Void UnityEngine.XR.ARFoundation.ARSession::.ctor()
extern void ARSession__ctor_mE345E2948F903454DF7484566DF13BEF689215CA (void);
// 0x000001E9 UnityEngine.Camera UnityEngine.XR.ARFoundation.ARSessionOrigin::get_camera()
extern void ARSessionOrigin_get_camera_m6809A44CAB9A1E35394FB329780A6C5B56B8CEBB (void);
// 0x000001EA System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::set_camera(UnityEngine.Camera)
extern void ARSessionOrigin_set_camera_m851561DFDA4B9520A6F0EAA1D3A53128A07BD351 (void);
// 0x000001EB UnityEngine.Transform UnityEngine.XR.ARFoundation.ARSessionOrigin::get_trackablesParent()
extern void ARSessionOrigin_get_trackablesParent_m37049D7E75CF694834A140C2EACB15D2D1098505 (void);
// 0x000001EC System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::set_trackablesParent(UnityEngine.Transform)
extern void ARSessionOrigin_set_trackablesParent_m1DC880A558CB7E18D31ADFBFEFC583F807076E0E (void);
// 0x000001ED UnityEngine.Transform UnityEngine.XR.ARFoundation.ARSessionOrigin::get_contentOffsetTransform()
extern void ARSessionOrigin_get_contentOffsetTransform_m656172F5E167D7DC3C1217098B39A7368E03CA89 (void);
// 0x000001EE System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::MakeContentAppearAt(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ARSessionOrigin_MakeContentAppearAt_mBF6FBDBD3CC75ABA8C4C929EC7239B602E205A54 (void);
// 0x000001EF System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::MakeContentAppearAt(UnityEngine.Transform,UnityEngine.Vector3)
extern void ARSessionOrigin_MakeContentAppearAt_m5808AA16F1126DC24F5595C35BB949F528A3CDF3 (void);
// 0x000001F0 System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::MakeContentAppearAt(UnityEngine.Transform,UnityEngine.Quaternion)
extern void ARSessionOrigin_MakeContentAppearAt_mE6BF5A0A537C751E67AAA85C56387AF1AEF1EDFB (void);
// 0x000001F1 System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::Awake()
extern void ARSessionOrigin_Awake_m696618685A6FC1F64CE085A5A331C5E8781472BA (void);
// 0x000001F2 UnityEngine.Pose UnityEngine.XR.ARFoundation.ARSessionOrigin::GetCameraOriginPose()
extern void ARSessionOrigin_GetCameraOriginPose_m75D111317C0E96F4B80715ECAB89CB4062A915AD (void);
// 0x000001F3 System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::Update()
extern void ARSessionOrigin_Update_mD080963394C175FD90C6F945F6776D7D456BA45F (void);
// 0x000001F4 System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::.ctor()
extern void ARSessionOrigin__ctor_m8419EDC8402212489DB65B9B79D74AC3E1F2C81C (void);
// 0x000001F5 UnityEngine.XR.ARFoundation.ARSessionState UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::get_state()
extern void ARSessionStateChangedEventArgs_get_state_mF0E9D3730C795A07DDD65F845E4291D2F9B70638 (void);
// 0x000001F6 System.Void UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::set_state(UnityEngine.XR.ARFoundation.ARSessionState)
extern void ARSessionStateChangedEventArgs_set_state_m9CFA0FF2E49408B0BF09EB38F4F650A792BF6C9C (void);
// 0x000001F7 System.Void UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::.ctor(UnityEngine.XR.ARFoundation.ARSessionState)
extern void ARSessionStateChangedEventArgs__ctor_m7473A94BDBF05370C5A4EC71665D08283FE5F895 (void);
// 0x000001F8 System.Int32 UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::GetHashCode()
extern void ARSessionStateChangedEventArgs_GetHashCode_m8D2A1E07AB9A28442B186F7B68C2FB324F25468D (void);
// 0x000001F9 System.Boolean UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::Equals(System.Object)
extern void ARSessionStateChangedEventArgs_Equals_mB7F8D129E9C22D5F3B8F1DC75772AD99B5C66804 (void);
// 0x000001FA System.String UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::ToString()
extern void ARSessionStateChangedEventArgs_ToString_mEFF33F8DE367398EE4ABBF971A58E7D9B3EEF689 (void);
// 0x000001FB System.Boolean UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs)
extern void ARSessionStateChangedEventArgs_Equals_mA3A45D328EB6F00C559FE9CF5DDA1CC80FD93AA5 (void);
// 0x000001FC System.Boolean UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::op_Equality(UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs,UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs)
extern void ARSessionStateChangedEventArgs_op_Equality_mD585630AE06115C088579E1224450B294A6AAC0E (void);
// 0x000001FD System.Boolean UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::op_Inequality(UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs,UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs)
extern void ARSessionStateChangedEventArgs_op_Inequality_m6399CDE5CA3B3C993A7A315E39ACE9204248B5AB (void);
// 0x000001FE System.Boolean UnityEngine.XR.ARFoundation.ARTrackable`2::get_destroyOnRemoval()
// 0x000001FF System.Void UnityEngine.XR.ARFoundation.ARTrackable`2::set_destroyOnRemoval(System.Boolean)
// 0x00000200 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARFoundation.ARTrackable`2::get_trackableId()
// 0x00000201 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARFoundation.ARTrackable`2::get_trackingState()
// 0x00000202 System.Boolean UnityEngine.XR.ARFoundation.ARTrackable`2::get_pending()
// 0x00000203 System.Void UnityEngine.XR.ARFoundation.ARTrackable`2::set_pending(System.Boolean)
// 0x00000204 TSessionRelativeData UnityEngine.XR.ARFoundation.ARTrackable`2::get_sessionRelativeData()
// 0x00000205 System.Void UnityEngine.XR.ARFoundation.ARTrackable`2::set_sessionRelativeData(TSessionRelativeData)
// 0x00000206 System.Void UnityEngine.XR.ARFoundation.ARTrackable`2::OnAfterSetSessionRelativeData()
// 0x00000207 System.Void UnityEngine.XR.ARFoundation.ARTrackable`2::SetSessionRelativeData(TSessionRelativeData)
// 0x00000208 System.Void UnityEngine.XR.ARFoundation.ARTrackable`2::.ctor()
// 0x00000209 UnityEngine.XR.ARFoundation.TrackableCollection`1<TTrackable> UnityEngine.XR.ARFoundation.ARTrackableManager`4::get_trackables()
// 0x0000020A UnityEngine.XR.ARFoundation.ARSessionOrigin UnityEngine.XR.ARFoundation.ARTrackableManager`4::get_sessionOrigin()
// 0x0000020B System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`4::set_sessionOrigin(UnityEngine.XR.ARFoundation.ARSessionOrigin)
// 0x0000020C System.String UnityEngine.XR.ARFoundation.ARTrackableManager`4::get_gameObjectName()
// 0x0000020D UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARTrackableManager`4::GetPrefab()
// 0x0000020E System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`4::Awake()
// 0x0000020F System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`4::Update()
// 0x00000210 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`4::OnTrackablesChanged(System.Collections.Generic.List`1<TTrackable>,System.Collections.Generic.List`1<TTrackable>,System.Collections.Generic.List`1<TTrackable>)
// 0x00000211 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`4::OnCreateTrackable(TTrackable)
// 0x00000212 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`4::OnAfterSetSessionRelativeData(TTrackable,TSessionRelativeData)
// 0x00000213 TTrackable UnityEngine.XR.ARFoundation.ARTrackableManager`4::CreateTrackableImmediate(TSessionRelativeData)
// 0x00000214 System.Boolean UnityEngine.XR.ARFoundation.ARTrackableManager`4::DestroyPendingTrackable(UnityEngine.XR.ARSubsystems.TrackableId)
// 0x00000215 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`4::ClearAndSetCapacity(System.Collections.Generic.List`1<TTrackable>,System.Int32)
// 0x00000216 System.String UnityEngine.XR.ARFoundation.ARTrackableManager`4::GetTrackableName(UnityEngine.XR.ARSubsystems.TrackableId)
// 0x00000217 UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARTrackableManager`4::CreateGameObject()
// 0x00000218 UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARTrackableManager`4::CreateGameObject(System.String)
// 0x00000219 UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARTrackableManager`4::CreateGameObject(UnityEngine.XR.ARSubsystems.TrackableId)
// 0x0000021A TTrackable UnityEngine.XR.ARFoundation.ARTrackableManager`4::CreateTrackable(UnityEngine.XR.ARSubsystems.TrackableId)
// 0x0000021B TTrackable UnityEngine.XR.ARFoundation.ARTrackableManager`4::CreateOrUpdateTrackable(TSessionRelativeData)
// 0x0000021C System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`4::DestroyTrackable(TTrackable)
// 0x0000021D System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`4::.ctor()
// 0x0000021E System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`4::.cctor()
// 0x0000021F UnityEngine.Vector2 UnityEngine.XR.ARFoundation.ARTrackedImage::get_extents()
extern void ARTrackedImage_get_extents_mCC673F1F9276D44374B0B9F896D05F47F9AA5D9D (void);
// 0x00000220 UnityEngine.Vector2 UnityEngine.XR.ARFoundation.ARTrackedImage::get_size()
extern void ARTrackedImage_get_size_m8D617290A171CEB929A4453518510F8B0E63151A (void);
// 0x00000221 System.IntPtr UnityEngine.XR.ARFoundation.ARTrackedImage::get_nativePtr()
extern void ARTrackedImage_get_nativePtr_mF5EBB2FDA7853386D6833EA956AAB2F2EC8B3C98 (void);
// 0x00000222 UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARFoundation.ARTrackedImage::get_referenceImage()
extern void ARTrackedImage_get_referenceImage_m46AAAD2FF5BF6E2A9AB8192BCE198FB69F00CC7D (void);
// 0x00000223 System.Void UnityEngine.XR.ARFoundation.ARTrackedImage::set_referenceImage(UnityEngine.XR.ARSubsystems.XRReferenceImage)
extern void ARTrackedImage_set_referenceImage_m9A54F39853DE3CEA412E5615D21354ACD43FAD49 (void);
// 0x00000224 System.Void UnityEngine.XR.ARFoundation.ARTrackedImage::.ctor()
extern void ARTrackedImage__ctor_m9279D554E8FEE057263849B153821D7D5692E6FC (void);
// 0x00000225 UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary UnityEngine.XR.ARFoundation.ARTrackedImageManager::get_referenceLibrary()
extern void ARTrackedImageManager_get_referenceLibrary_mF77A49EFFEA34DA7DFCE3D1465DB95D1BC1E78F7 (void);
// 0x00000226 System.Void UnityEngine.XR.ARFoundation.ARTrackedImageManager::set_referenceLibrary(UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary)
extern void ARTrackedImageManager_set_referenceLibrary_m9041D9DB7FDA1199763F05C5EC5E8B1DAB7C16E4 (void);
// 0x00000227 System.Int32 UnityEngine.XR.ARFoundation.ARTrackedImageManager::get_maxNumberOfMovingImages()
extern void ARTrackedImageManager_get_maxNumberOfMovingImages_mF868D79ED343BF25B1EA71F66A45153250244FF4 (void);
// 0x00000228 System.Void UnityEngine.XR.ARFoundation.ARTrackedImageManager::set_maxNumberOfMovingImages(System.Int32)
extern void ARTrackedImageManager_set_maxNumberOfMovingImages_m534F24C3CA00D0E4525A5CFAC5D5D6840F2E04CD (void);
// 0x00000229 UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARTrackedImageManager::get_trackedImagePrefab()
extern void ARTrackedImageManager_get_trackedImagePrefab_m2FA5CB1B5E3288918F6E3C2641E86D02A4C7C25B (void);
// 0x0000022A System.Void UnityEngine.XR.ARFoundation.ARTrackedImageManager::set_trackedImagePrefab(UnityEngine.GameObject)
extern void ARTrackedImageManager_set_trackedImagePrefab_m8EC64DA46478F3D3CC1BC8EFDA9EAD2180B56C4E (void);
// 0x0000022B UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARTrackedImageManager::GetPrefab()
extern void ARTrackedImageManager_GetPrefab_mAC0361F8CA22F0A1DBE0378C9AAF6FF3122BF18C (void);
// 0x0000022C System.Void UnityEngine.XR.ARFoundation.ARTrackedImageManager::add_trackedImagesChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs>)
extern void ARTrackedImageManager_add_trackedImagesChanged_m7B52639FCC946FABAEEF0CB060121E8BACC58680 (void);
// 0x0000022D System.Void UnityEngine.XR.ARFoundation.ARTrackedImageManager::remove_trackedImagesChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs>)
extern void ARTrackedImageManager_remove_trackedImagesChanged_m9604E04925AF884CFD0CDD6350230837BBF3F1CE (void);
// 0x0000022E System.String UnityEngine.XR.ARFoundation.ARTrackedImageManager::get_gameObjectName()
extern void ARTrackedImageManager_get_gameObjectName_m87203D5BF386D83F3011C6A1CD05DE1A631A41CE (void);
// 0x0000022F System.Void UnityEngine.XR.ARFoundation.ARTrackedImageManager::Awake()
extern void ARTrackedImageManager_Awake_m6BBE2C67172C868880C19AB1D4E2AE28B3F02CCE (void);
// 0x00000230 System.Void UnityEngine.XR.ARFoundation.ARTrackedImageManager::OnBeforeStart()
extern void ARTrackedImageManager_OnBeforeStart_mE529D68F75923CDC23CA4188119B046F0648C35F (void);
// 0x00000231 System.Void UnityEngine.XR.ARFoundation.ARTrackedImageManager::OnAfterSetSessionRelativeData(UnityEngine.XR.ARFoundation.ARTrackedImage,UnityEngine.XR.ARSubsystems.XRTrackedImage)
extern void ARTrackedImageManager_OnAfterSetSessionRelativeData_m4F532D0C1EBABFFB2206CDC4FEE477792970EF26 (void);
// 0x00000232 System.Void UnityEngine.XR.ARFoundation.ARTrackedImageManager::OnTrackablesChanged(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage>)
extern void ARTrackedImageManager_OnTrackablesChanged_mB08CF35175DEEC1A1118DD107D8CDE2D4FC86353 (void);
// 0x00000233 System.Void UnityEngine.XR.ARFoundation.ARTrackedImageManager::UpdateReferenceImages()
extern void ARTrackedImageManager_UpdateReferenceImages_m77C55FA1B3B3F3C3492530F7CD7CE889650BDE95 (void);
// 0x00000234 System.Void UnityEngine.XR.ARFoundation.ARTrackedImageManager::SetMaxNumberOfMovingImages(System.Int32)
extern void ARTrackedImageManager_SetMaxNumberOfMovingImages_m162FD8AFB46D42230B073ACDC9F3C998069761DF (void);
// 0x00000235 System.Void UnityEngine.XR.ARFoundation.ARTrackedImageManager::.ctor()
extern void ARTrackedImageManager__ctor_m9B19F213E9517752243F7B6C6FE38B992D5EB48A (void);
// 0x00000236 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage> UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::get_added()
extern void ARTrackedImagesChangedEventArgs_get_added_mFEE1ADD4190DA3C25E6B0E05CC1BC87A76C8FDC6 (void);
// 0x00000237 System.Void UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::set_added(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage>)
extern void ARTrackedImagesChangedEventArgs_set_added_mA40CAA72DD703D1BE3C327F9DB44E56D61A39573 (void);
// 0x00000238 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage> UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::get_updated()
extern void ARTrackedImagesChangedEventArgs_get_updated_m2C4491B11EEE724FF2122574EADB018DCA6E4BAF (void);
// 0x00000239 System.Void UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::set_updated(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage>)
extern void ARTrackedImagesChangedEventArgs_set_updated_m9C6A583C91E216FD3295B1FB6FCE56AED492CD14 (void);
// 0x0000023A System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage> UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::get_removed()
extern void ARTrackedImagesChangedEventArgs_get_removed_m6BB8A2862669BC4B46FCFC2C263FAE2F7A029877 (void);
// 0x0000023B System.Void UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::set_removed(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage>)
extern void ARTrackedImagesChangedEventArgs_set_removed_m5D5E3D10D70CE63F950D9D0915DB8961E25D6A85 (void);
// 0x0000023C System.Void UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::.ctor(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage>)
extern void ARTrackedImagesChangedEventArgs__ctor_m476333D62E0654548D6EF73052FC5D081B1B0CCC (void);
// 0x0000023D System.Int32 UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::GetHashCode()
extern void ARTrackedImagesChangedEventArgs_GetHashCode_m1F5268F315485431CE66DE629BC4F15426FE5E8C (void);
// 0x0000023E System.Boolean UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::Equals(System.Object)
extern void ARTrackedImagesChangedEventArgs_Equals_mE07DEE967AAE86BE3CFFF4E7EEB2917927E88C68 (void);
// 0x0000023F System.String UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::ToString()
extern void ARTrackedImagesChangedEventArgs_ToString_mFC911B5451161F9880E9022AC963BD7F199C7CEA (void);
// 0x00000240 System.Boolean UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void ARTrackedImagesChangedEventArgs_Equals_m33FBEDB48ED3AFF8E2606FAE1D5A333662756EB2 (void);
// 0x00000241 System.Boolean UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::op_Equality(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs,UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void ARTrackedImagesChangedEventArgs_op_Equality_mEAB400A4615B80C1ABEE139ADB3981696AFFD3BB (void);
// 0x00000242 System.Boolean UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::op_Inequality(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs,UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void ARTrackedImagesChangedEventArgs_op_Inequality_mD03EE8661982F348034CEF5581B207593D961629 (void);
// 0x00000243 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARFoundation.IRaycaster::Raycast(UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
// 0x00000244 System.Int32 UnityEngine.XR.ARFoundation.MeshInfoComparer::Compare(UnityEngine.XR.MeshInfo,UnityEngine.XR.MeshInfo)
extern void MeshInfoComparer_Compare_m69E96DBA032DF793CBBA543077E28E3E7677D935 (void);
// 0x00000245 System.Void UnityEngine.XR.ARFoundation.MeshInfoComparer::.ctor()
extern void MeshInfoComparer__ctor_mAF91CAA6ECB0892B884BE071D53A449026A31872 (void);
// 0x00000246 System.Void UnityEngine.XR.ARFoundation.MeshQueue::EnqueueUnique(UnityEngine.XR.MeshInfo)
extern void MeshQueue_EnqueueUnique_mABB321C4B2F28750D316D47F3779A2EE40F0CCFA (void);
// 0x00000247 System.Int32 UnityEngine.XR.ARFoundation.MeshQueue::get_count()
extern void MeshQueue_get_count_m19DEE003D25B036B7D6C1F8CCCD0CCE70C895279 (void);
// 0x00000248 System.Boolean UnityEngine.XR.ARFoundation.MeshQueue::TryDequeue(System.Collections.Generic.IReadOnlyDictionary`2<UnityEngine.XR.MeshId,UnityEngine.XR.MeshInfo>,UnityEngine.XR.MeshInfo&)
extern void MeshQueue_TryDequeue_m0F05CB38B5EEBFC114B190E1E927BC4843083B44 (void);
// 0x00000249 System.Boolean UnityEngine.XR.ARFoundation.MeshQueue::Remove(UnityEngine.XR.MeshId)
extern void MeshQueue_Remove_m5064AE2F02E6478173A9EC39D772A33D536DCB85 (void);
// 0x0000024A System.Void UnityEngine.XR.ARFoundation.MeshQueue::InsertNew(UnityEngine.XR.MeshInfo)
extern void MeshQueue_InsertNew_m5F757162F7F6B2090078B398E914C644D74BBF27 (void);
// 0x0000024B System.Void UnityEngine.XR.ARFoundation.MeshQueue::UpdateExisting(UnityEngine.XR.MeshInfo)
extern void MeshQueue_UpdateExisting_m850816A4EEF6C6C56CB5E76AC68D4D5085F4AB4B (void);
// 0x0000024C System.Void UnityEngine.XR.ARFoundation.MeshQueue::Clear()
extern void MeshQueue_Clear_mAA94499967ABCF17CDD05BF5C2A011B1D325B996 (void);
// 0x0000024D System.Void UnityEngine.XR.ARFoundation.MeshQueue::.ctor()
extern void MeshQueue__ctor_m91C9238B2896CC30A1E8304213A597CE28531E24 (void);
// 0x0000024E System.Void UnityEngine.XR.ARFoundation.PlaneDetectionModeMaskAttribute::.ctor()
extern void PlaneDetectionModeMaskAttribute__ctor_mB7FA9E69B382217E801BA121DA5029F784055AC4 (void);
// 0x0000024F UnityEngine.Vector3 UnityEngine.XR.ARFoundation.PoseExtensions::InverseTransformPosition(UnityEngine.Pose,UnityEngine.Vector3)
extern void PoseExtensions_InverseTransformPosition_mCDC0B4E47294498D315D25A11F55A3DCB1D56B30 (void);
// 0x00000250 UnityEngine.Vector3 UnityEngine.XR.ARFoundation.PoseExtensions::InverseTransformDirection(UnityEngine.Pose,UnityEngine.Vector3)
extern void PoseExtensions_InverseTransformDirection_m30D10D1F00928AC1DC560E31DC9536E3CA77B87C (void);
// 0x00000251 System.Void UnityEngine.XR.ARFoundation.PoseExtensions::InverseTransformPositions(UnityEngine.Pose,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void PoseExtensions_InverseTransformPositions_m2DA3D0DC2772553353753870F5CC0BB39AC6FFF0 (void);
// 0x00000252 TSubsystem UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::get_subsystem()
// 0x00000253 System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::set_subsystem(TSubsystem)
// 0x00000254 TSubsystemDescriptor UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::get_descriptor()
// 0x00000255 TSubsystem UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::CreateSubsystem()
// 0x00000256 System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::CreateSubsystemIfNecessary()
// 0x00000257 System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::OnEnable()
// 0x00000258 System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::OnDisable()
// 0x00000259 TSubsystem UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::GetActiveSubsystemInstance()
// 0x0000025A System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::OnDestroy()
// 0x0000025B System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::OnBeforeStart()
// 0x0000025C System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::OnAfterStart()
// 0x0000025D System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::.ctor()
// 0x0000025E System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::.cctor()
// 0x0000025F UnityEngine.XR.ARFoundation.TrackableCollection`1/Enumerator<TTrackable> UnityEngine.XR.ARFoundation.TrackableCollection`1::GetEnumerator()
// 0x00000260 System.Void UnityEngine.XR.ARFoundation.TrackableCollection`1::.ctor(System.Collections.Generic.Dictionary`2<UnityEngine.XR.ARSubsystems.TrackableId,TTrackable>)
// 0x00000261 System.Int32 UnityEngine.XR.ARFoundation.TrackableCollection`1::get_count()
// 0x00000262 TTrackable UnityEngine.XR.ARFoundation.TrackableCollection`1::get_Item(UnityEngine.XR.ARSubsystems.TrackableId)
// 0x00000263 System.Boolean UnityEngine.XR.ARFoundation.TrackableCollection`1::TryGetTrackable(UnityEngine.XR.ARSubsystems.TrackableId,TTrackable&)
// 0x00000264 UnityEngine.Ray UnityEngine.XR.ARFoundation.TransformExtensions::TransformRay(UnityEngine.Transform,UnityEngine.Ray)
extern void TransformExtensions_TransformRay_m146923AB98EFC9BED518F02A742A628A45D3A20A (void);
// 0x00000265 UnityEngine.Ray UnityEngine.XR.ARFoundation.TransformExtensions::InverseTransformRay(UnityEngine.Transform,UnityEngine.Ray)
extern void TransformExtensions_InverseTransformRay_mD6C666D44E1BF8407A2AA50118A7C90890935503 (void);
// 0x00000266 UnityEngine.Pose UnityEngine.XR.ARFoundation.TransformExtensions::TransformPose(UnityEngine.Transform,UnityEngine.Pose)
extern void TransformExtensions_TransformPose_m677CE84C622BD23C3DDB2953DDB820E1934B0144 (void);
// 0x00000267 UnityEngine.Pose UnityEngine.XR.ARFoundation.TransformExtensions::InverseTransformPose(UnityEngine.Transform,UnityEngine.Pose)
extern void TransformExtensions_InverseTransformPose_m7508C8166573E2D0042170C4F09BFAE44A064545 (void);
// 0x00000268 System.Void UnityEngine.XR.ARFoundation.TransformExtensions::TransformPointList(UnityEngine.Transform,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void TransformExtensions_TransformPointList_m2154D9E7CE80E84A5D831D5A9037815C6BCD80B2 (void);
// 0x00000269 System.Void UnityEngine.XR.ARFoundation.TransformExtensions::InverseTransformPointList(UnityEngine.Transform,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void TransformExtensions_InverseTransformPointList_mD21F038FA2CFC45650C1D3A0B9EFAAB6F01C2A0B (void);
// 0x0000026A System.Void UnityEngine.XR.ARFoundation.TransformExtensions::SetLayerRecursively(UnityEngine.Transform,System.Int32)
extern void TransformExtensions_SetLayerRecursively_m8D28346FC588296B9DF07B68D468733228925F92 (void);
// 0x0000026B System.Void UnityEngine.XR.ARFoundation.ARCameraManager/TextureInfo::.ctor(UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void TextureInfo__ctor_mEDA31195012270B48B7C4B8C208FA60144F9F5D7 (void);
// 0x0000026C UnityEngine.XR.ARSubsystems.XRTextureDescriptor UnityEngine.XR.ARFoundation.ARCameraManager/TextureInfo::get_descriptor()
extern void TextureInfo_get_descriptor_m18FDFD0838ED7ED90257C6069B98FD62CCCEA495 (void);
// 0x0000026D UnityEngine.Texture2D UnityEngine.XR.ARFoundation.ARCameraManager/TextureInfo::get_texture()
extern void TextureInfo_get_texture_m433B9BD7E92AE791892952E3608DF9D19079B708 (void);
// 0x0000026E UnityEngine.XR.ARFoundation.ARCameraManager/TextureInfo UnityEngine.XR.ARFoundation.ARCameraManager/TextureInfo::GetUpdatedTextureInfo(UnityEngine.XR.ARFoundation.ARCameraManager/TextureInfo,UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void TextureInfo_GetUpdatedTextureInfo_m003D8CA0166E6937460A2978ECAA51EDC13CB79B (void);
// 0x0000026F System.Void UnityEngine.XR.ARFoundation.ARCameraManager/TextureInfo::DestroyTexture()
extern void TextureInfo_DestroyTexture_m050A83AF559AC94D9F42AAD9A1F278E1979AE873 (void);
// 0x00000270 UnityEngine.Texture2D UnityEngine.XR.ARFoundation.ARCameraManager/TextureInfo::CreateTexture(UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void TextureInfo_CreateTexture_mD448B5A95A8B5D7ABA21C91C5DFF3E5F720E710B (void);
// 0x00000271 System.Int32 UnityEngine.XR.ARFoundation.ARMeshManager/TrackableIdComparer::Compare(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.XR.ARSubsystems.TrackableId)
extern void TrackableIdComparer_Compare_m9E90B709F9E6A4E771A0EDDF8B0D1665ED8D6E57 (void);
// 0x00000272 System.Void UnityEngine.XR.ARFoundation.ARMeshManager/TrackableIdComparer::.ctor()
extern void TrackableIdComparer__ctor_m8B57F6CB93FD1D0614E25DF11D00B1CAAD5E81D3 (void);
// 0x00000273 System.Void UnityEngine.XR.ARFoundation.ARPointCloudManager/PointCloudRaycastJob::Execute(System.Int32)
extern void PointCloudRaycastJob_Execute_mEB5E8A731796250ED6FB44512D0FDDDD6FC380C8 (void);
// 0x00000274 System.Void UnityEngine.XR.ARFoundation.ARPointCloudManager/PointCloudRaycastCollectResultsJob::Execute()
extern void PointCloudRaycastCollectResultsJob_Execute_m3968794C96B4131517E9F464564C67AEAA02E1F2 (void);
// 0x00000275 System.Void UnityEngine.XR.ARFoundation.ARSession/<CheckAvailability>d__19::.ctor(System.Int32)
extern void U3CCheckAvailabilityU3Ed__19__ctor_m08C1CA60DCCF91B61F68BCC1336CFFDFEBBFBD4A (void);
// 0x00000276 System.Void UnityEngine.XR.ARFoundation.ARSession/<CheckAvailability>d__19::System.IDisposable.Dispose()
extern void U3CCheckAvailabilityU3Ed__19_System_IDisposable_Dispose_m4C4ADDC32E52285BA2A6896E7E0324B1BA78E370 (void);
// 0x00000277 System.Boolean UnityEngine.XR.ARFoundation.ARSession/<CheckAvailability>d__19::MoveNext()
extern void U3CCheckAvailabilityU3Ed__19_MoveNext_m0EB56BDDB1F92E89B25E4B3BC31F5EDF6BC442F4 (void);
// 0x00000278 System.Object UnityEngine.XR.ARFoundation.ARSession/<CheckAvailability>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckAvailabilityU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A56DCE0CEFEF9017E61B8FBD4402FBBF6EB2A85 (void);
// 0x00000279 System.Void UnityEngine.XR.ARFoundation.ARSession/<CheckAvailability>d__19::System.Collections.IEnumerator.Reset()
extern void U3CCheckAvailabilityU3Ed__19_System_Collections_IEnumerator_Reset_m4357AD5282BFDCE4F14D28301FC2931DC6DFD0C8 (void);
// 0x0000027A System.Object UnityEngine.XR.ARFoundation.ARSession/<CheckAvailability>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CCheckAvailabilityU3Ed__19_System_Collections_IEnumerator_get_Current_m610B26B3A0864D10B52509D509BFC342632BF32A (void);
// 0x0000027B System.Void UnityEngine.XR.ARFoundation.ARSession/<Install>d__20::.ctor(System.Int32)
extern void U3CInstallU3Ed__20__ctor_m2F75745CA503E960D14CDC1F92C5DD66B679ACB6 (void);
// 0x0000027C System.Void UnityEngine.XR.ARFoundation.ARSession/<Install>d__20::System.IDisposable.Dispose()
extern void U3CInstallU3Ed__20_System_IDisposable_Dispose_mA295F395FAA9895AE9EC4744024D06D10F28E071 (void);
// 0x0000027D System.Boolean UnityEngine.XR.ARFoundation.ARSession/<Install>d__20::MoveNext()
extern void U3CInstallU3Ed__20_MoveNext_m4F94D4CD3BD5F915B10EC9321357B6A69A654908 (void);
// 0x0000027E System.Object UnityEngine.XR.ARFoundation.ARSession/<Install>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInstallU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55478D5D86B9A5514681B9F6AE388D80CC8844F9 (void);
// 0x0000027F System.Void UnityEngine.XR.ARFoundation.ARSession/<Install>d__20::System.Collections.IEnumerator.Reset()
extern void U3CInstallU3Ed__20_System_Collections_IEnumerator_Reset_mE6463D86E8B16A8995B5E0A91041AE1190AC978E (void);
// 0x00000280 System.Object UnityEngine.XR.ARFoundation.ARSession/<Install>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CInstallU3Ed__20_System_Collections_IEnumerator_get_Current_m853254C986ED2DCADA7172E87250714975634ACB (void);
// 0x00000281 System.Void UnityEngine.XR.ARFoundation.ARSession/<Initialize>d__22::.ctor(System.Int32)
extern void U3CInitializeU3Ed__22__ctor_mA856A0CC49FA4039FE1B3520DA0545E6E99E7EA5 (void);
// 0x00000282 System.Void UnityEngine.XR.ARFoundation.ARSession/<Initialize>d__22::System.IDisposable.Dispose()
extern void U3CInitializeU3Ed__22_System_IDisposable_Dispose_m2B0131451DF62D9F01A3A543C228C7AC9D91A606 (void);
// 0x00000283 System.Boolean UnityEngine.XR.ARFoundation.ARSession/<Initialize>d__22::MoveNext()
extern void U3CInitializeU3Ed__22_MoveNext_m15B37CEB613B082CD374FD573D4CCDAFC719B280 (void);
// 0x00000284 System.Object UnityEngine.XR.ARFoundation.ARSession/<Initialize>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D493CF82413AA98CC4445F406FC2E7ECB9226B6 (void);
// 0x00000285 System.Void UnityEngine.XR.ARFoundation.ARSession/<Initialize>d__22::System.Collections.IEnumerator.Reset()
extern void U3CInitializeU3Ed__22_System_Collections_IEnumerator_Reset_m3CD8DF951E64226B6553E14B3D438D4C87E13BA8 (void);
// 0x00000286 System.Object UnityEngine.XR.ARFoundation.ARSession/<Initialize>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeU3Ed__22_System_Collections_IEnumerator_get_Current_m878C21107C0E73FA127198E15F77D49D50D9265E (void);
// 0x00000287 System.Void UnityEngine.XR.ARFoundation.TrackableCollection`1/Enumerator::.ctor(System.Collections.Generic.Dictionary`2<UnityEngine.XR.ARSubsystems.TrackableId,TTrackable>)
// 0x00000288 System.Boolean UnityEngine.XR.ARFoundation.TrackableCollection`1/Enumerator::MoveNext()
// 0x00000289 TTrackable UnityEngine.XR.ARFoundation.TrackableCollection`1/Enumerator::get_Current()
// 0x0000028A System.Void UnityEngine.XR.ARFoundation.TrackableCollection`1/Enumerator::Dispose()
static Il2CppMethodPointer s_methodPointers[650] = 
{
	NULL,
	NULL,
	NULL,
	ARBackgroundRendererAsset__ctor_m176A55D92E105E2A70E14AB312D7D24EF3B89306,
	ARCameraBackground_get_useCustomMaterial_mD74747ED7EE8105D789E8BC42E90AAA54E992896,
	ARCameraBackground_set_useCustomMaterial_m4B4941B89CD4768AE7B0388E0DF2757B09EF6977,
	ARCameraBackground_get_customMaterial_m5405AC6064562E48113BDE0A3FF52869F2AFA611,
	ARCameraBackground_set_customMaterial_m856F6BD371FB08A072539B5B6AEB35A1733F9697,
	ARCameraBackground_get_material_m3163C241851D9445E405ACFDA9200921E65288ED,
	ARCameraBackground_set_material_mD35D84A442B74B54A215CA16AA5C602BD9A6A355,
	ARCameraBackground_get_useCustomRendererAsset_mB40673385E0549AA635214EB9043E985C4C91304,
	ARCameraBackground_set_useCustomRendererAsset_mB60A83699045452C991F829CDA47938F5977C5F3,
	ARCameraBackground_get_customRendererAsset_m5F868FBA42CAB728508F1F90534100E04A6D6B27,
	ARCameraBackground_set_customRendererAsset_m3897137531E97E8DDF5EA075CCAB83995018DD52,
	ARCameraBackground_get_m_BackgroundRenderer_m1B313D75B85F24DAC3E814F8A407A16AF21ED72C,
	ARCameraBackground_set_m_BackgroundRenderer_m81BE6FDDB7346A6BEBF8C2A90637F9EABCD2928D,
	ARCameraBackground_CreateMaterialFromSubsystemShader_m91CBB645E549260938C0CC7E4E2231009106E112,
	ARCameraBackground_OnCameraFrameReceived_m3D8C38D609F7E4A2F5F829A799F6E228C00EF3B1,
	ARCameraBackground_SetupBackgroundRenderer_mA7B43BB265F49199D7CFD7BE55F4EA76F2929760,
	ARCameraBackground_Awake_m533DD57AB5745778694DFD05036953081632F7C1,
	ARCameraBackground_OnEnable_m9637B9AC38620F199D7A7282D58DFEF38912B738,
	ARCameraBackground_OnDisable_m61D075560663A9A8B9CAE93CC240F525D3785CC1,
	ARCameraBackground_OnSessionStateChanged_m140FAA76DE26615403D3FF3335EC21B615AFCB7F,
	ARCameraBackground_UpdateMaterial_mF3A8305A178B874F2B100C5C6B6AEEDB875EFAED,
	ARCameraBackground_get_subsystemMaterial_m82EA024B4F69FAF76136AB8C594CC38D0525653E,
	ARCameraBackground_get_lwrpMaterial_m876FA7A579D262F1D67F5DBF501FA63963C577BC,
	ARCameraBackground_get_mode_m7C4BDFF6F9EBBEF9074852AE08604AE6F83D464F,
	ARCameraBackground_set_mode_mF1D34C25DA3B0699B1A53217AC5290B8DF21E2D5,
	ARCameraBackground_get_useRenderPipeline_m0DFF39BBBDEE59497ADF65D7D4DBFA552134EC07,
	ARCameraBackground__ctor_m6180F629FF4C5FD28ADF5815B04D4378FB8EADDC,
	ARCameraBackground__cctor_m45FB63609D064ECDC2B90E6BE0E0B4371ED13FD5,
	ARCameraFrameEventArgs_get_lightEstimation_m4F226F18EC0E91AEA1F152B165703869D31C9600,
	ARCameraFrameEventArgs_set_lightEstimation_mE109AD7E91548FEF74DF045FB85B598210895657,
	ARCameraFrameEventArgs_get_timestampNs_m26633672AA4D433551422517ADA8F0ECED1B1A9E,
	ARCameraFrameEventArgs_set_timestampNs_mDA520DBF61A930154758C9FF620B639861EE920B,
	ARCameraFrameEventArgs_get_projectionMatrix_mEBEA65211112C8080149BB535637D3BED21EC273,
	ARCameraFrameEventArgs_set_projectionMatrix_mC6137EDADA29E8383CFFAAB4F9C839A176220142,
	ARCameraFrameEventArgs_get_displayMatrix_m9FCD5AF461B96162C2D1E9CCBEDE76974F32AAB0,
	ARCameraFrameEventArgs_set_displayMatrix_mC37DE33664D4EBA5788105502751A981E37E71AB,
	ARCameraFrameEventArgs_get_textures_m7062757F64E5497A602D2E5E55929ECE9C9CAC58,
	ARCameraFrameEventArgs_set_textures_m11E835B5FF52F8FACF26836715ED85B785FB3E79,
	ARCameraFrameEventArgs_get_propertyNameIds_m84B811AA86E7BE4E1D8D79C88F7892D35FF38946,
	ARCameraFrameEventArgs_set_propertyNameIds_m122F7477BC8728536A0C240FD62A2CDFA26743A9,
	ARCameraFrameEventArgs_GetHashCode_m406B3D310D34F27D4BEAC18178A13E57F1BF8E7E,
	ARCameraFrameEventArgs_Equals_mFB12BA2256481CDBA32F843FA14CE365E9FB1F17,
	ARCameraFrameEventArgs_ToString_m49FFD9A837C51548F9F25E341869E9CA1637C108,
	ARCameraFrameEventArgs_Equals_m3C3D3D4D19A38B553D21664F58B52A6DDF745AE8,
	ARCameraFrameEventArgs_op_Equality_m28337A36DEDAA5E9A103A862F2534FB4BE72D67C,
	ARCameraFrameEventArgs_op_Inequality_m232AF17EF5D3C1A18CA8A7D0CBBCFA5F48542112,
	ARCameraManager_get_focusMode_mF8E49519D985F5108A8417F51B53BFEFBC42D5A1,
	ARCameraManager_set_focusMode_m43E1ABCEA28C77FB476B9A81E0C869A54D167677,
	ARCameraManager_get_lightEstimationMode_m92906F93977637966A91F1096F5E36C8085E253E,
	ARCameraManager_set_lightEstimationMode_mB8099A10ECE4DCA4CA90491B7EDD98F70125B27F,
	ARCameraManager_get_permissionGranted_m53AF1AD25F10B8AD4B5FE2EF1BAFB2619C6630B9,
	ARCameraManager_add_frameReceived_mC892BC9299D54D118573AD11EC2AD7FB01FDE824,
	ARCameraManager_remove_frameReceived_m001AAE0A292720A4BFE91B4604B8151DA93ED730,
	ARCameraManager_get_shaderName_m6A2F691CCEF93426C0C6C13A514914CAC93ED46C,
	ARCameraManager_TryGetIntrinsics_mC131D22073841EA1D44B43EA1A763ED4A0712567,
	ARCameraManager_GetConfigurations_m8E6514724AE3B3FD6CD6A03303739F05DB45B7F2,
	ARCameraManager_get_currentConfiguration_m01ECFF2D814A2D6C63390866420BA18F6BE02ECF,
	ARCameraManager_set_currentConfiguration_m7C89FF4948F9C9AD0F094391795A25EB9E127E32,
	ARCameraManager_TryGetLatestImage_m100D8C01169594321DE493AB5590E71F710367CE,
	ARCameraManager_Awake_m69804098263656B4466DBD5A331F2B6B8BF377A7,
	ARCameraManager_OnBeforeStart_mBF04D69FA21C519C53D02EFCAA3F6BE9CF31B933,
	ARCameraManager_OnDisable_m8F490C3886573FCC2A48AF1E5F55058616E5541F,
	ARCameraManager_Update_m619CE64F8F0CCFA87D5E853CCF936B362FB58490,
	ARCameraManager_OnPreRender_m5945C4B5E7496C9B1F83BD6FFB5927D2C1EA8627,
	ARCameraManager_OnPostRender_m4355C0AA56022921C0DC378461B0918776EB3E11,
	ARCameraManager_UpdateTexturesInfos_mEF1F6080FD54142B25AC1BCE9A74DBF02634A4FF,
	ARCameraManager_InvokeFrameReceivedEvent_m30221EE34D16F2E319A6FBA8EA09467174D2E225,
	ARCameraManager__ctor_m230D7CC23E284E7B5E6D2130EC244BC0D8322A1C,
	ARCameraManager__cctor_m51CDF011C24CE085E7C23E09C3A301C982B5B366,
	AREnvironmentProbe_get_environmentTextureFilterMode_m2B5BA9DC0B04BAA623972475F874E1AAD252B4B7,
	AREnvironmentProbe_set_environmentTextureFilterMode_m5C00F6A4AD84EACEA6886012AD7C612CD031DE12,
	AREnvironmentProbe_get_placementType_mDE1B7634B143C16E7A6460BEA8C06428DF7EB3ED,
	AREnvironmentProbe_set_placementType_m9F0AD7D8D186E40E64EF012B7F4291FF2ED2378D,
	AREnvironmentProbe_get_size_mFE109731EA55FF9DDF82992686D57196B88AF8FC,
	AREnvironmentProbe_get_extents_mFC169B39CBD2CCCA53084795FA1F01FE7A73DEC5,
	AREnvironmentProbe_get_nativePtr_m086E391F4F2FF3B65F2025E3BE92D9421DD3BBC3,
	AREnvironmentProbe_get_textureDescriptor_m10810988B25A99EB6FED275F48C751F2BBABA5ED,
	AREnvironmentProbe_Awake_mA160B91C2BB6E98C328103B21A65AF7FD162DE59,
	AREnvironmentProbe_OnAfterSetSessionRelativeData_m3FA8BCBB7D951BBA0E3535EBDD784328FB401188,
	AREnvironmentProbe_UpdateEnvironmentTexture_mAFAA53AB561160343F832D9926C93196BDE4A9F5,
	AREnvironmentProbe_CreateEnvironmentTexture_m39D6391EA40701D6C01CA13C5533B84083988265,
	AREnvironmentProbe_ToString_m2D9DAB7EA4B6FCCC22ED1468CFDE0CDDEB18BEA6,
	AREnvironmentProbe__ctor_m395755A4E1DD4F34B33288E042C7F417A0C287C1,
	AREnvironmentProbeManager_get_automaticPlacement_m3AD52806C0CBA15EBD4D14AA89895AAD93FFDCAD,
	AREnvironmentProbeManager_set_automaticPlacement_m7DB8F9C5B456A6E3C749AFF0C74ADEF31504BEBB,
	AREnvironmentProbeManager_get_environmentTextureFilterMode_m3CFE876910AFCF994883863B11F718DC28FD2FDF,
	AREnvironmentProbeManager_set_environmentTextureFilterMode_mF837C56600DBA2FDAB9FECDB5EF200107EBE3582,
	AREnvironmentProbeManager_get_debugPrefab_m77463EEC9592EDDEB2020C934F95F1D3AD67C1E0,
	AREnvironmentProbeManager_set_debugPrefab_m89082B692DFCD9010BF6FA9B229ADF579AF9018B,
	AREnvironmentProbeManager_add_environmentProbesChanged_m2D795CC563BF0C56BE5885EDEF93451E5DFDCE7D,
	AREnvironmentProbeManager_remove_environmentProbesChanged_mCE43A47A88A4FF347D8E74B9D5FDFBAC1C3E616B,
	AREnvironmentProbeManager_GetEnvironmentProbe_m7C1863C4F116EEF962B8C4135932FD4325EBCDA6,
	AREnvironmentProbeManager_AddEnvironmentProbe_mA8B34934DB01DCBF00291EA5DC402786FF86D6C2,
	AREnvironmentProbeManager_RemoveEnvironmentProbe_m9C3FAB868462C90A3DF6F7822C83115FEC4D01C8,
	AREnvironmentProbeManager_get_gameObjectName_mDF2444D465C494D5821382055E60BF7BC160C5F0,
	AREnvironmentProbeManager_GetPrefab_mA5337D413659A58A19C53B9CDD35CF58BBE7E4E0,
	AREnvironmentProbeManager_OnBeforeStart_m6ECC81FCA226D5605382FA59BC341F7180B1177B,
	AREnvironmentProbeManager_OnDestroy_m2D98A8EA182A5A9D64D29363FF629768DA283EEC,
	AREnvironmentProbeManager_OnTrackablesChanged_m5176B9A2000F304677252914562AE7095BA1FF5A,
	AREnvironmentProbeManager_OnCreateTrackable_m29B7CA9CA37FF0311E1CA33507A20380E5C875B9,
	AREnvironmentProbeManager_SetAutomaticPlacementStateOnSubsystem_m24F2292692263F84507C48E9B98C3136701FED16,
	AREnvironmentProbeManager__ctor_mA7BFA20548EF5E203775703BD7654626062DE75A,
	AREnvironmentProbesChangedEvent_get_added_m9BB5E81DFA9C421C674F7A730155A8401E1909C4,
	AREnvironmentProbesChangedEvent_set_added_mBA9CCAEA557D0793D123E22B4DDAA8FC8B2A8F5B,
	AREnvironmentProbesChangedEvent_get_updated_m64A6BF00CB47EB9AF3D5F868254CBC3F6768681C,
	AREnvironmentProbesChangedEvent_set_updated_m1EC2DA3AD720493D88D6EE126CABCE43B1041821,
	AREnvironmentProbesChangedEvent_get_removed_mE849182B7E2F464312887B3DDCFE153E73872CCE,
	AREnvironmentProbesChangedEvent_set_removed_mBA8F20E7B9B35B23023E5513D4F908951FA96738,
	AREnvironmentProbesChangedEvent__ctor_m6B3E7444BC6E8E50407CAE8FF136B128F85B71CC,
	AREnvironmentProbesChangedEvent_GetHashCode_mB2DA47D276F5AFD887049840F016940EAF1913B7,
	AREnvironmentProbesChangedEvent_Equals_m7F5FF62122376EB1CD88F50959F6A297A36E42BC,
	AREnvironmentProbesChangedEvent_ToString_m917D72B83F15A2DAABCAE4BAA80F68F9259E4304,
	AREnvironmentProbesChangedEvent_Equals_mA2A9DD5565680439D9C22E9972C4C7582B341739,
	AREnvironmentProbesChangedEvent_op_Equality_mAA6F2EC184F4C8A908B118C4B49AF8B831CEA209,
	AREnvironmentProbesChangedEvent_op_Inequality_m97AD812132F5340870142AF0C6DD83B95F39FB09,
	ARFace_add_updated_mC76C6961391C3AA87BF12ECF7B2C09C36D2A83D5,
	ARFace_remove_updated_mC371EEDD673DC8ED0522E766CFE73CDE92B53C40,
	ARFace_get_vertices_m3ED72F651B9A0E73A11ADCF9E58A152631B779D5,
	ARFace_get_normals_mB78A18AC0EBB168A45292C9655D580A85C6E732E,
	ARFace_get_indices_m2CF54ACF97DDC886A39272B536F551A2BDE9C5BB,
	ARFace_get_uvs_m14F8F1713F862CE6306790FA417CFE42E541E8DF,
	ARFace_Update_mBBDBC4E62AA22F8086B34EB97193A5A5DD557EFA,
	ARFace_OnDestroy_m628AAE79CC879965D3574E00C95CDBA8BA2B6F34,
	NULL,
	ARFace_UpdateMesh_m3D32D18FCA504BEFE5337E902A591B74883319D0,
	ARFace__ctor_m22E819375DF72B8CDF3FAC2F4318673CB5A1B008,
	ARFaceManager_get_facePrefab_m5E7CBC1B1A124A11B0146E55B30D0012839CFBD8,
	ARFaceManager_set_facePrefab_m666350470A61048423D3175686CFCFB87B42BA18,
	ARFaceManager_get_supported_mDB9E6477890DAC79D3941357BCBFF745E711E288,
	ARFaceManager_add_facesChanged_mC09C76E089CE72D4D37CA6D06D258EF00E4F91C6,
	ARFaceManager_remove_facesChanged_mF8E16DC0EA4533D67C78EB6F7AB13685A61AAF38,
	ARFaceManager_TryGetFace_m54BDE8E1E445BAC4813D5140E8BB35175DF8599E,
	ARFaceManager_OnEnable_m4C4892859FFC657EB76A0AEA571FE79E328EEE43,
	ARFaceManager_OnAfterSetSessionRelativeData_m53DFEB2A0F56478FFEF84DF85B9986B0D7610A0B,
	ARFaceManager_OnTrackablesChanged_m3AF746DD0FD5D615CC936505BF0E936B28156CEC,
	ARFaceManager_GetPrefab_m47D3E3DFA54EB38C517D2621687759F7FBCF31BF,
	ARFaceManager_get_gameObjectName_m56E7CFE79A04D3CD478CDABA75F1B444D65685E0,
	ARFaceManager__ctor_m4CF8658707D546CDAF84C2A9F50FB46BE66B01D7,
	ARFaceMeshVisualizer_get_mesh_m235A1EDEA1D2C61A9564893316012E9D932FA6D6,
	ARFaceMeshVisualizer_set_mesh_m308FC4FCE7816F2812B97FE002BBEC88D3FD6B97,
	ARFaceMeshVisualizer_SetVisible_m905A63BC9381E7D66C70BE6F9E63C043E6836FF5,
	NULL,
	ARFaceMeshVisualizer_SetMeshTopology_mD1A3878995AF31167B80C7F8D1220D306F0759B7,
	ARFaceMeshVisualizer_UpdateVisibility_mF26DFBAE77B1E8FF366D5153AC9D0E7ABF5E616B,
	ARFaceMeshVisualizer_OnUpdated_mB54FD44BBAB032710E26DCA9096BC2ECBF3E605E,
	ARFaceMeshVisualizer_OnSessionStateChanged_m251478CC19F73057DA519059B8EC04BF309CAD9B,
	ARFaceMeshVisualizer_Awake_m62DBB1C0A7D9F70198EE0EFD0173B7F230455510,
	ARFaceMeshVisualizer_OnEnable_m55B7834B78827AFE7D120F0811357560890AC53B,
	ARFaceMeshVisualizer_OnDisable_m8882820E446917B0CF728687F98063CCB5298FAC,
	ARFaceMeshVisualizer__ctor_m32D407CBD57E5B408B593649E79BF6EE1835228D,
	ARFaceUpdatedEventArgs_get_face_mECE51067B415A317E9F43CFDAD67D17E81D93CA0,
	ARFaceUpdatedEventArgs_set_face_mB64678F0DDE0D4C5CA7F729D338EDD86C04B16C4,
	ARFaceUpdatedEventArgs__ctor_m3FB5F8EE705C0F4F9F10194CEAF835AFF04F4B81,
	ARFaceUpdatedEventArgs_GetHashCode_m941B2292502C893BCAA6808A6D71F36935B470BA,
	ARFaceUpdatedEventArgs_Equals_m08148B0B8B75DA7D44C1DFE693386D7E82959F35,
	ARFaceUpdatedEventArgs_Equals_mFFCF8F02B10115669047395AF4BC245EE74ACBB9,
	ARFaceUpdatedEventArgs_op_Equality_mC3C96286D6AF05F23D7590498FE23F5E42967F11,
	ARFaceUpdatedEventArgs_op_Inequality_m1304B2612248EBC6C6306515F024558B9138124A,
	ARFacesChangedEventArgs_get_added_m78E85E8C2A286ADA233ACB9EE04CED7F71100889,
	ARFacesChangedEventArgs_set_added_mFDB1B3EFA9E097B2AA4EBABCA139B79E89759AC5,
	ARFacesChangedEventArgs_get_updated_m7877418FAC42584DA75C820DD34928418F3336BD,
	ARFacesChangedEventArgs_set_updated_m3993021EC9472BCE3E9A993C3C4B40D5318D8352,
	ARFacesChangedEventArgs_get_removed_m2A3A1BAC623C9D00D642422772FFC5F90961A729,
	ARFacesChangedEventArgs_set_removed_m670EC88FBAF24F8F7FD8CD3E1DCF57765B538CEA,
	ARFacesChangedEventArgs__ctor_m4033277E36C6AE75A2561BE9E1B8B88652FDDE25,
	ARFacesChangedEventArgs_GetHashCode_mB888F6AF90BC4A28ABC14AE8A5CBA2080D404D42,
	ARFacesChangedEventArgs_Equals_m2869D9928B1AF6540AA012371399B69CE46ADC83,
	ARFacesChangedEventArgs_ToString_m6EE382E0E4CC5C5540D9918D346C0085E13C3C1D,
	ARFacesChangedEventArgs_Equals_mE74910D42FA40C32E16E8C9D14506277BBBF4149,
	ARFacesChangedEventArgs_op_Equality_mD3696BD92E38AA97EA668CFB238585ED73EFE1B4,
	ARFacesChangedEventArgs_op_Inequality_m4D97917B41E61822D23877DB3C6B49E9CAD9BB6D,
	ARFoundationBackgroundRenderer_ResetGlState_mF47B550F91756D479D3E93AAB9D69C674892E94E,
	ARFoundationBackgroundRenderer_add_backgroundRendererChanged_mBD685A8A649DFA20BBB66AC67C6CCD0A912436F9,
	ARFoundationBackgroundRenderer_remove_backgroundRendererChanged_m1E943EFD3D621926CE9C5A46E681D33230057EEC,
	ARFoundationBackgroundRenderer_get_backgroundMaterial_m20B5F85B3D15F8B87ED8C77ECD85E88DEA997D48,
	ARFoundationBackgroundRenderer_set_backgroundMaterial_m1D7D7F4F782DCC9B326445E46B53127CB1FC03BC,
	ARFoundationBackgroundRenderer_get_backgroundTexture_mAAD551D8041DB863224BFD36CFE22CA560D274CE,
	ARFoundationBackgroundRenderer_set_backgroundTexture_m82ABDD84C3ED41274DDF32ABDE69B955775E08BF,
	ARFoundationBackgroundRenderer_get_camera_m51C36CF275B3D4BC01BACB6D3DB24EEB98B1B066,
	ARFoundationBackgroundRenderer_set_camera_mA26857C4B8E78DBC4E2DBE9A46D144341B1A3808,
	ARFoundationBackgroundRenderer_get_mode_m592D96C6C96206856B95C594D5F7CC0F42C28050,
	ARFoundationBackgroundRenderer_set_mode_mF7A41852F4D170E441C82B2840DFF9E95142650E,
	ARFoundationBackgroundRenderer_AddOpenGLES3ResetStateCommand_mA8DCA9AEBAB9D006AD55A3C2F313FCF7BA7B4E9B,
	ARFoundationBackgroundRenderer_EnableARBackgroundRendering_m5ECC8D3D8679D45352F6A5A59B548B7F78C95FBE,
	ARFoundationBackgroundRenderer_DisableARBackgroundRendering_m50922D9BDCC3B4B21B344280245A386AEF5EB96C,
	ARFoundationBackgroundRenderer_ReapplyCommandBuffersIfNeeded_mF30006D468EE5240E4DBD3EC8AD57EA997FDB9C5,
	ARFoundationBackgroundRenderer_RemoveCommandBuffersIfNeeded_m3B40A725ED00379B6AE36D2D0501955997078C73,
	ARFoundationBackgroundRenderer__ctor_m59FF990B39B2C11FB45B3861CC81B1DC13FBA6D3,
	ARFoundationBackgroundRenderer__cctor_m797A95E8E064137DDAF76ADA0A3A1F3EC12E44FD,
	ARFoundationBackgroundRendererChangedEventArgs_GetHashCode_m8C37A3197487C438791442E94615A4158AB90C81,
	ARFoundationBackgroundRendererChangedEventArgs_Equals_mDB252BA86BC70B445DD29B5BD144B3605E764AB0,
	ARFoundationBackgroundRendererChangedEventArgs_Equals_m13499F72035E3D1175ACDB88B3520BDF83DF0B9C,
	ARFoundationBackgroundRendererChangedEventArgs_op_Equality_mED1A70ACCDF2FFD4EBA30C95B52EBC60BE198C9F,
	ARFoundationBackgroundRendererChangedEventArgs_op_Inequality_m9CE525D585E83043185B4D11CE2C673FC6FD3567,
	ARInputManager_get_subsystem_mB6B87FB0E7A0CE152C8C193EE9DE3ECFEFD450A2,
	ARInputManager_set_subsystem_m9B6400C1A5A4E932C742193CC5F0E774664A27AA,
	ARInputManager_OnEnable_m43ADB54D8FBE01265C77AAED9680C80A5B75432A,
	ARInputManager_OnDisable_m109E1AF8CAB37C2BD574D326C58BDACCFD49FDAF,
	ARInputManager_OnDestroy_mD610DE54A4C1396CF3399518496CA6CB5E8ACCA1,
	ARInputManager_CreateSubsystemIfNecessary_m8E21A700CBB9A3C694D622900047944D762A7CFE,
	ARInputManager_CreateSubsystem_mBC202793130ED485EF3ADD88055ABA3085645FBF,
	ARInputManager_GetActiveSubsystemInstance_mBC10E6AD7F1B4319B544B60C6B85F79759EDB308,
	ARInputManager__ctor_mAF16A842A222B3303CB2050409E91EC060255A47,
	ARInputManager__cctor_m83B727E8827BE46BFA66249FBE442D7124CB8464,
	ARLightEstimationData_get_averageBrightness_mB339C036AE0CBCF8DAA8FCDE4A4D04A19EFB7233,
	ARLightEstimationData_set_averageBrightness_m9C0FEF62B371B067D51E1E930A42ABE40F0EEDAB,
	ARLightEstimationData_get_averageColorTemperature_m7C37859EB80C2606270F72DEF3E292299CE81778,
	ARLightEstimationData_set_averageColorTemperature_mDCDE66CD7D7D2BD24434DE00D68B171449D9207A,
	ARLightEstimationData_get_colorCorrection_mD318D06DB8535937FD4BB5E5DEF1BDE6471A2A2D,
	ARLightEstimationData_set_colorCorrection_m7551D5F797A9E203BAB64416C4B1637848A7EBBD,
	ARLightEstimationData_GetHashCode_mE8A3FF44F64AEC6C3FCFF1629777C45EDD89B06D,
	ARLightEstimationData_Equals_mAB9336917F38BCB1ACD25EFE8B7F6C07C979F478,
	ARLightEstimationData_ToString_m4068C6C855EABAA601CB607A080453B094479DE1,
	ARLightEstimationData_Equals_m2A61749DD4B3227BE1727779503F7580C05F8495,
	ARLightEstimationData_op_Equality_m35E79A4E46AE792EEB1E0807D438062442DCF520,
	ARLightEstimationData_op_Inequality_m11532BCDCA49F6DEDD6AC3C6AFD2CDDE06197D22,
	ARMeshManager_get_meshPrefab_mE0271046EBF41B280F14B0A773666C73A3489CC7,
	ARMeshManager_set_meshPrefab_m08BFB7AC287E9B89554E750B9A0179A00D979577,
	ARMeshManager_get_density_m0EDF78AE3FB79684768E68F5D9FFD946237DAFD0,
	ARMeshManager_set_density_m485913E0ED10AF606DFE6D1095D24ADE15FE2C39,
	ARMeshManager_get_normals_m5DE2A8C09BDEDC026C46C45EA807C0D86D9AD4CF,
	ARMeshManager_set_normals_m7112C481FE146A20CBE5A2B90AC12020EE94A8BB,
	ARMeshManager_get_tangents_m3FE960BCDD1A46FC6F1040E9966CFA5F2698AEF0,
	ARMeshManager_set_tangents_mE6A70D1D30FC468DBEB157FF7D654BFFCB9E1DD9,
	ARMeshManager_get_textureCoordinates_m5F144CC814A5AE853C6CDA1819FB96FA04FF2D0C,
	ARMeshManager_set_textureCoordinates_m5AA472A4D24260428DF732B432B6BC6DE3905AA8,
	ARMeshManager_get_colors_mDE61B9163BD95DB03519E46A17E6E267C5DC1A44,
	ARMeshManager_set_colors_m6B87CDCAF924BAB0F6FAE89A312F417B7054C9A2,
	ARMeshManager_get_concurrentQueueSize_m11826C3996CC1A1F2DF7F495781DBF95AC00BA0D,
	ARMeshManager_set_concurrentQueueSize_mF7841046A6C36BD67EEEF6CB0B184A6EBD812D26,
	ARMeshManager_add_meshesChanged_mEE9E53B27ADB4427DCFA369F9D0C913B356DF4E4,
	ARMeshManager_remove_meshesChanged_mA46004B024552977D9495EBBE41A031E6CE7699C,
	ARMeshManager_get_subsystem_m75AF924F0383AEAEC0046E1D5B552314F2D86909,
	ARMeshManager_get_meshes_mDF28D58C7C7D3021F01B15A99CCC49AA754392C4,
	ARMeshManager_DestroyAllMeshes_m9DC72181A5DA9211A2B4D7776965A3F0BDE1B550,
	ARMeshManager_GetSessionOrigin_m1AA18CD54C9C3E42C4A9A38CB21E139714F79B9F,
	ARMeshManager_SetBoundingVolume_m11BC2D4DF62C5440AA4EE24526F5740453E34EED,
	ARMeshManager_OnEnable_mCA928F5B676C85E3DB01A9DA19F71EC45968B9EE,
	ARMeshManager_OnDrawGizmosSelected_mD1FD9FAB0E4653F71A230706D53A20AFC0B7099C,
	ARMeshManager_Update_mF2A044D4D9F005948F9BBEAF64159EC6D0599358,
	ARMeshManager_Generate_m239A013DAB4790B83EB74ACD9095917EBBC60D0B,
	ARMeshManager_OnMeshGenerated_m981F27D548806FD4D1F4FF74AE0431BCC3D6777E,
	ARMeshManager_UpdateMeshInfos_m2F704B4E3F5BE441508F6C52F9538D5E57749E1B,
	ARMeshManager_OnDisable_mE8C25004398E1C9EC52A22BDDCA7984CACCA76E8,
	ARMeshManager_OnDestroy_m8CB92235F351A9052FE60F59789598D57B4069A0,
	ARMeshManager_GetOrCreateMeshFilter_m2CBE90F9D53D72AD6F5E3E0CBC4D3EB000CFE54D,
	ARMeshManager_GetTrackableId_m66F6AB46495F45B31EBFFC9E4547845190631E11,
	ARMeshManager_GetLegacyMeshId_m03296A7F349BAE02872FE64406C8B82D6DE71B88,
	ARMeshManager_Awake_m415C9B21B176C501BD3E694600BF1B0679D7B912,
	ARMeshManager_CreateSubsystem_mC8DCAE337AAE273FF351430114BA866CFE3F02FA,
	ARMeshManager__ctor_m193FA3F74B01501D99FF8AE57CE68CC71650DE6C,
	ARMeshManager__cctor_m9FA9AF5610FBD1AC1254EA99140EA969D043E315,
	ARMeshesChangedEventArgs_get_added_m416B22A9406017FC5E40EB473774CE9149EB1BC7,
	ARMeshesChangedEventArgs_set_added_m07AC944AFF173C597B045BB60E40BCEE02A646FF,
	ARMeshesChangedEventArgs_get_updated_m3E49A6C32750983B744168851591634FDA4B542F,
	ARMeshesChangedEventArgs_set_updated_m2A97C612DD863055AE6CEA0D1598E687A93EEAEF,
	ARMeshesChangedEventArgs_get_removed_m88699CFBAE47A01A6C053A351E02D32B7C61E084,
	ARMeshesChangedEventArgs_set_removed_mB3D40BCE5693E2436C19CA66879C0C1F3B57756A,
	ARMeshesChangedEventArgs__ctor_m40CF8F7E678F5D03DEB1AE5AE6330F6F6B2958EE,
	ARMeshesChangedEventArgs_GetHashCode_m7E79DE6C2468873DC4CD40005D850221480E34E6,
	ARMeshesChangedEventArgs_Equals_mB538D8CA2C6C59B16D1F67E354D49FB6B47B0EE4,
	ARMeshesChangedEventArgs_ToString_m0ED4C46371B9B64DAA5956A5A90CF5933B300AFF,
	ARMeshesChangedEventArgs_Equals_m20539FBB146BA19C19AE6996CF6FD53492C57A8F,
	ARMeshesChangedEventArgs_op_Equality_m5FDE7E6EC10551B14E832F26F210BBBE9785807F,
	ARMeshesChangedEventArgs_op_Inequality_mC4CA3029E394F285609C6179EA9380EE673A65DE,
	ARPlane_get_vertexChangedThreshold_m7D0635EBA7DE0BFA18D4F9C83C74E78CB841E18F,
	ARPlane_set_vertexChangedThreshold_m1C7F8CD3037AE652731CA77B23E63826F3CA23A6,
	ARPlane_add_boundaryChanged_m29CFF978ACA0B60DE6BA14688B7A113C3EDA2E31,
	ARPlane_remove_boundaryChanged_m49F278652DDB9BE23D3CB939B061C848C1E7B06C,
	ARPlane_get_normal_mA454AC0BDC53BB29B23252CA933C2488B1D65E05,
	ARPlane_get_subsumedBy_mBE978F9E4BD0B0FE8544457CC4770E69378F66AA,
	ARPlane_set_subsumedBy_m9E1CF16AFEA932801A0F20B93577A717EF3A6278,
	ARPlane_get_alignment_m48654B659D193EEF5DB28095ABF7B1BDBB3A9BD5,
	ARPlane_get_centerInPlaneSpace_m0111B536A21E35BA1B70651EF6BC6EF3CD407529,
	ARPlane_get_center_m30B400A4510C2F9D4412E74C0ADC97DD8892EB07,
	ARPlane_get_extents_m23C7BEC8A2485EB82E7F7EE665304BF1AE431690,
	ARPlane_get_size_m716429374DDDEBE4C5D5C4EE4094F16F8A96D2BD,
	ARPlane_get_infinitePlane_m3E5A49987D9B10446EE57CA081ABF611D9151BDE,
	ARPlane_get_nativePtr_m98D11221375A16A9CA121C162F7E9D1758BAE72E,
	ARPlane_get_boundary_mE2365FCC5D0232A1F5F444844B93CCD01FFC2559,
	ARPlane_UpdateBoundary_m53446E5350109A75B9F96A3042EE037E4521D7B9,
	ARPlane_OnValidate_m4A8849A53B054ED7977324349B8275250D0C5C04,
	ARPlane_OnDestroy_m92A8E4C2946F18356B563F3B34D7ACA657F221E5,
	ARPlane_CheckForBoundaryChanges_m6E300E7748E9F1CC241347A7A519231B2BCF5D96,
	ARPlane_CopyBoundaryAndSetChangedFlag_mEBE6B4CFD45C3865D5304CD78D9F792D248375F0,
	ARPlane_Update_mCD9290EFD789F73B747101141CAD1FD944F886B2,
	ARPlane__ctor_mEFCE074188D91A380B684E00E1EB3A8633856BC9,
	ARPlaneBoundaryChangedEventArgs_get_plane_mE14F32BD989ADD2F0FBDBAB9AB293CDAB4CCB03B,
	ARPlaneBoundaryChangedEventArgs_set_plane_m5DC6678054CF729894BBE1BB359ECD6ABB21C018,
	ARPlaneBoundaryChangedEventArgs__ctor_mCF92554C8AB31E6FB30004DE9480EE749767720D,
	ARPlaneBoundaryChangedEventArgs_GetHashCode_m6380A99937DA7544630D3E7D025BCCBA75C92E2C,
	ARPlaneBoundaryChangedEventArgs_Equals_mCAE90FE42E59A12583A7EA34758578304D10BEC4,
	ARPlaneBoundaryChangedEventArgs_ToString_m459F343E2184B7AFE40965471117B25F7783F873,
	ARPlaneBoundaryChangedEventArgs_Equals_m5168B0124859C641CFE47EF454D01E1C42BBE2AA,
	ARPlaneBoundaryChangedEventArgs_op_Equality_m4E6BC2B245E0AC2361F18C7C8E9D8B39864A9A44,
	ARPlaneBoundaryChangedEventArgs_op_Inequality_m6B6C4B7437B45CE4E5E43D931E45EFB1D364AE09,
	ARPlaneManager_get_planePrefab_m420C466CEE70BB4900C7C8BB699FF4CB288DB1C9,
	ARPlaneManager_set_planePrefab_mA8D37D90B73308F931871C17C30CEAE1AD0B5922,
	ARPlaneManager_get_detectionMode_m9213906802A76764C2B75D2BC4DC3CF838EEE76B,
	ARPlaneManager_set_detectionMode_mDBF9B7E80B9785A2288AB78EF6BCF0BBE4CB2BD9,
	ARPlaneManager_add_planesChanged_m9866605669B3B910AAC344FF40D4195686404D2B,
	ARPlaneManager_remove_planesChanged_m3DDBB16626916DD0F6F0BAA1FEF5AA695B924846,
	ARPlaneManager_GetPlane_m07E47479942F326E19BEFCFF4599579637A03227,
	ARPlaneManager_Raycast_m7598B32F45C2E8612B131D13C4E1AB283F006C67,
	ARPlaneManager_GetCrossDirection_mC2F352184BDF9B212CA8E3A0A2FDDD60F8645006,
	ARPlaneManager_WindingNumber_m181C484F9F116133407CDC939D5B3898A2B5D873,
	ARPlaneManager_GetPrefab_m45F6E55A085930E2EC7D01737F6E6A4CCDE8832B,
	ARPlaneManager_OnBeforeStart_mB9D2CBA5D2B05C6A524F9692DD57FB0AE9263383,
	ARPlaneManager_OnAfterSetSessionRelativeData_m5824123176AA49FD51C6EACEA8B521D97B775502,
	ARPlaneManager_OnTrackablesChanged_mC594D8189E68C158025CD7F467D992BDA2076A6C,
	ARPlaneManager_get_gameObjectName_m5D2D94A1E80B8C7240D2717225FF4F2CA2AFA2F7,
	ARPlaneManager_OnEnable_m251D7B94FD6766A3F26806ADAAFA3AD73E142C4F,
	ARPlaneManager_OnDisable_mA42AED97FC24125D7FF7A16B8EF29361A4DF885A,
	ARPlaneManager__ctor_m3D15D9E2AF6D14B0613EABB82FDD465D7A3BA111,
	ARPlaneManager__cctor_m3F6450FDA4A33161452FB66D0998E7FF315E22CF,
	ARPlaneMeshGenerators_GenerateMesh_m31B11251353F699F456EFF5307CF35C8EA48BD76,
	ARPlaneMeshGenerators_GenerateUvs_mBC2A77E6B10BC1331B3F0036C57A532665D37173,
	ARPlaneMeshGenerators_GenerateIndices_mD8B027666DC55E2DA3A4F251D61E03E2C2D05AF1,
	ARPlaneMeshGenerators__cctor_mE3BF52F68272A91EDBF6A51E128B3E268210B0D5,
	ARPlaneMeshVisualizer_get_mesh_m74A28DC83789468390901498492ECD91A58D9D88,
	ARPlaneMeshVisualizer_set_mesh_m8B42F851E8410BED82E67F135AD0A3AB7C6A9DC7,
	ARPlaneMeshVisualizer_OnBoundaryChanged_m1787BC532ABCA0DB773DA98D6DE54D9FA840BA4B,
	ARPlaneMeshVisualizer_DisableComponents_m49A77252B75A9ABFDBF17FFE43C2E264A0CE9BF5,
	ARPlaneMeshVisualizer_SetVisible_m3278C868A00EF82B5D863CC92B1D5980C7B893BB,
	ARPlaneMeshVisualizer_UpdateVisibility_m893626BA96546175A81B5DC60EDA9AFA1BB1C725,
	ARPlaneMeshVisualizer_Awake_mDF40B09B5BCD21A6FD95B137386FA76174678107,
	ARPlaneMeshVisualizer_OnEnable_mBF3848FD5ED4106FF4B298B0459500EF28C3B0A9,
	ARPlaneMeshVisualizer_OnDisable_m2F868BBB379E9D9A34BED36C2F4192518EF69A99,
	ARPlaneMeshVisualizer_Update_m5A0802B6146884AAA3358E08CD786C208EE7B747,
	ARPlaneMeshVisualizer__ctor_mCB3EC94CAE888A2D5219D840A92FA6E0652E1955,
	ARPlanesChangedEventArgs_get_added_m26B6F1AEAD621FD8A939521235C7B45AE08FDA1D,
	ARPlanesChangedEventArgs_set_added_m363F2A1024103B4E741741E4BBB09F952293E21B,
	ARPlanesChangedEventArgs_get_updated_m674C4C76BC3C8A2D49793B254B103021A4E33C5A,
	ARPlanesChangedEventArgs_set_updated_mDB2AA3FD2A483CE633ABBDBBEC8C5193B421D5A6,
	ARPlanesChangedEventArgs_get_removed_m5F0AC740E7C61322659EC224A4EEC4DBC8B3E405,
	ARPlanesChangedEventArgs_set_removed_mD249F70F7D051CE0030B6D671DE8FDE8CEEDD860,
	ARPlanesChangedEventArgs__ctor_m88626999299D44A1F2D39060A37CCE03B6A7B51C,
	ARPlanesChangedEventArgs_GetHashCode_m12A40FC12326E1B13B615985A5CC188ADA83088B,
	ARPlanesChangedEventArgs_Equals_m004DBCB6C164D7FBAA6EA23D6FDDD7730795EFEE,
	ARPlanesChangedEventArgs_ToString_mB3CAC22890529BED6B5FA38DAE92D2DFC250DB72,
	ARPlanesChangedEventArgs_Equals_m82227BEF1235CDE33ADCE37382B7964629CFBAD9,
	ARPlanesChangedEventArgs_op_Equality_m1BD9BEF2A02C5E7EDB68A5F493C6BFD2EF5806D8,
	ARPlanesChangedEventArgs_op_Inequality_mD61DA03E0C14E646E81D6C37C5B0B9B878887F91,
	ARPointCloud_add_updated_mFA6E98BC9EE3C6E1D632E0A22FCF96C6670158A1,
	ARPointCloud_remove_updated_m9DD76FD146C7BFE6BA0AFD672700B75D71474F17,
	ARPointCloud_get_positions_m9FBA4F24BEE6BD88710890321082897B648D4813,
	ARPointCloud_get_identifiers_m6A0A77296BB198B2F42B4A14B5468A1E56F01921,
	ARPointCloud_get_confidenceValues_mD9E3378A48FA4CF0E504320C14B78255BAE989BB,
	ARPointCloud_Update_mC992FA846C53C7F28DED02D49C8BA06D3A44B198,
	ARPointCloud_OnDestroy_mDADD946B93109041238570823C7A067CC81B3119,
	NULL,
	ARPointCloud_UpdateData_m8EAB6AB87441F91F462CD2D130334177750FB7C3,
	ARPointCloud__ctor_m526C7A48B22B46189646D842885CBB84FDCD23C3,
	ARPointCloudChangedEventArgs_get_added_m9F934F1FA3322402E50F72E9B78669042367DF75,
	ARPointCloudChangedEventArgs_set_added_m21B8ABD00EFDA37DB3409B10A07F45B26C34F9B6,
	ARPointCloudChangedEventArgs_get_updated_mCE2A84457D78408052DAF251ACFC8D591D9B0FBA,
	ARPointCloudChangedEventArgs_set_updated_mDCB2376E57C283B084A04870BAAFB597989D45C3,
	ARPointCloudChangedEventArgs_get_removed_mCAFAC3A438975DE62575CB41D02A46ABD8C67842,
	ARPointCloudChangedEventArgs_set_removed_m36A3675CA4A35C7061EA7F5EEF114C56F932A076,
	ARPointCloudChangedEventArgs__ctor_m0BF590246450830BDA1923F1C79B383BD032FD40,
	ARPointCloudChangedEventArgs_GetHashCode_m28D5E34EB0E025E21935BF0FD1E46A35C49FC04C,
	ARPointCloudChangedEventArgs_Equals_m6BD50C01CB9435D9CBC7BE0F913199CAD3C38D4D,
	ARPointCloudChangedEventArgs_ToString_m96CAAE9D0926B9A3CE4FC16D942AE7571EDC83A5,
	ARPointCloudChangedEventArgs_Equals_mF2BB005AFBD3B0418D29AAC35DDE9566C23A14FE,
	ARPointCloudChangedEventArgs_op_Equality_mE45AAA1C835846CFA39B30C8B7168AC2D408D160,
	ARPointCloudChangedEventArgs_op_Inequality_mD405A9CB1E1A52C1FBBA44FE4CCF4A8529E0EDEC,
	ARPointCloudManager_get_pointCloudPrefab_m8EF0DDF8D272A4C7703411CA7E05B36DAD94DD66,
	ARPointCloudManager_set_pointCloudPrefab_m1B13F4CCD6D4F1A85783CEBC1567EC00D7AC4D5D,
	ARPointCloudManager_add_pointCloudsChanged_mA1FE3FBDADFA9B534ABBDA0668FA6FEC279D0A84,
	ARPointCloudManager_remove_pointCloudsChanged_m4AD9312BDE821627A52C95948D70D35BBD1E527E,
	ARPointCloudManager_OnEnable_m916C5325A4B95E562704DAAF6EA9D9FAA75E3CA8,
	ARPointCloudManager_OnDisable_m28A3932B1892E865675642D30EE32F5FED805662,
	ARPointCloudManager_GetPrefab_m0D7BF714EB65584D01D4D61D03AD21874B770124,
	ARPointCloudManager_get_gameObjectName_m6A0CE6B2D0F0E8ADE18FAEA2E60BEC4B234A31DD,
	ARPointCloudManager_OnAfterSetSessionRelativeData_mF7E960689FB8C1273EC77771101D5BFF39F7BFE9,
	ARPointCloudManager_OnTrackablesChanged_mAC96B361D7FF8582531603E0BEEDF7CDF96AD552,
	ARPointCloudManager_Raycast_mBCB5C701727AECBA0BA2B6BE721B8A45F1F9E105,
	NULL,
	ARPointCloudManager__ctor_m5FC517F01824F0201DA0DF05C3777CA3FAA6BA3D,
	ARPointCloudMeshVisualizer_get_mesh_mAF6BC728E087BCD55EDBB2E58E78F1C1CAA5943F,
	ARPointCloudMeshVisualizer_set_mesh_mCA2DE27C102B4BCCD4B99E2FB926047FF1EACB97,
	ARPointCloudMeshVisualizer_OnPointCloudChanged_m7DC671156F5FCA53233A42B4089C8DEC5EEE99EB,
	ARPointCloudMeshVisualizer_Awake_m0A6B44AF804A7E0B27255C4EB7669984EF18A257,
	ARPointCloudMeshVisualizer_OnEnable_m4D9E23AE2939C6744852822BC5CF5F85A5BA9A59,
	ARPointCloudMeshVisualizer_OnDisable_mCA54B26FFE4FB41E91B214957534333C48740298,
	ARPointCloudMeshVisualizer_Update_m7CC89E91D9AF8548A9701521AE6C7CA243B3F69B,
	ARPointCloudMeshVisualizer_UpdateVisibility_mCBF198BCE48526537B22FF32D18893EC589F5FA9,
	ARPointCloudMeshVisualizer_SetVisible_m8528922AE9887CCB8B8FA1E4B91870DBB9251838,
	ARPointCloudMeshVisualizer__ctor_mDBE2EE53EF0534363673948ABC8E3FF0F418196A,
	ARPointCloudMeshVisualizer__cctor_mA2CFF704CAB1E0C4AB2C6E702B244B9DAD22708C,
	ARPointCloudParticleVisualizer_OnPointCloudChanged_m2BDE581222E5D2FE8D6D6279C5ABD76633ACD9D0,
	ARPointCloudParticleVisualizer_Awake_mB4A2A5BB3362F4C44CD92F2A9888E8DBEA527804,
	ARPointCloudParticleVisualizer_OnEnable_m7E41D69E0949FF17E8191C9A7DDC551C5D7290AF,
	ARPointCloudParticleVisualizer_OnDisable_m883CEDCF81909D00A28CC2460C43CB539F9DCC5F,
	ARPointCloudParticleVisualizer_Update_m288ED8AF843475DD435FFACE738CA83AEE6654E5,
	ARPointCloudParticleVisualizer_UpdateVisibility_m615D4D3A73AF2A05CEEB21578F6911B0580121DB,
	ARPointCloudParticleVisualizer_SetVisible_mE63C4F71B57F739D63EE40788AA79870ABCDF0AA,
	ARPointCloudParticleVisualizer__ctor_m3F48BAD9F776C522DAA0C6624B98E54B33B81C57,
	ARPointCloudParticleVisualizer__cctor_m9C79727F3A5E81150BFC5F74696068975E296608,
	ARPointCloudUpdatedEventArgs_GetHashCode_mD52F0A42535E9D590F2065D60644655D80DC3C22,
	ARPointCloudUpdatedEventArgs_Equals_mBD558C0754E1A1C6BA9864C5891A282F6B977C9E,
	ARPointCloudUpdatedEventArgs_Equals_mB39450D04B879FE647C18AB10C409523B5FF4195,
	ARPointCloudUpdatedEventArgs_op_Equality_m65F86E115A3625AD876ECD8C6BF47E62B176F14E,
	ARPointCloudUpdatedEventArgs_op_Inequality_m7638C0B46DC1DD9E9A8872226A4B93E6C82EED2C,
	ARRaycastHit__ctor_m7E37CAC9301AAB566F3B8751091D3E472BB5C2E8,
	ARRaycastHit_get_distance_m14A7CD73EDA065E5E722BB48BD2FB3420BC6CFDC,
	ARRaycastHit_set_distance_m04D1E140673EA5F48185A2C0C78D138957D7010E,
	ARRaycastHit_get_hitType_m9F24C8C20D2985ACFBBAD4F7EEA509BE470CFB00,
	ARRaycastHit_get_pose_m5CCFFED6C4A101EA42083A8661956A2B4B4C4A0D,
	ARRaycastHit_get_trackableId_m6FBBEF54882143C2EE439CE919D131CD71EBE972,
	ARRaycastHit_get_sessionRelativePose_m0BFDEFFEE9453DE6A3EBF1F478D04C6CCD1E93AE,
	ARRaycastHit_get_sessionRelativeDistance_mDCDED72A207F7C1D59BFE84815C181A29AF8C059,
	ARRaycastHit_GetHashCode_m0ADC3CEFC9A9617A4F61616CE9981A98D7926525,
	ARRaycastHit_Equals_mDA8FEFBD8AB244631961E9C9EA43D3A1B5D2F894,
	ARRaycastHit_Equals_mE086AFC2E5237C054CC2D38B22B7506E892B090E,
	ARRaycastHit_op_Equality_mB8DDC0E9E24BFF9332A78AB325989A7903C9095E,
	ARRaycastHit_op_Inequality_mFCA5D48E720A6FE838F6AF3CE58C0D85FF7371B5,
	ARRaycastHit_CompareTo_m3F5B58CE479D3DDB7503B9C288C497AF2E3AE99E,
	ARRaycastManager_Raycast_m2F43B2CAF3D7C66183720D3980BD0CEB42E8F393,
	ARRaycastManager_Raycast_m65B31E8B76200A372429F98D2065E0D20CD7F768,
	ARRaycastManager_TransformAndSortRaycastResults_m026F42706BDE4904A69862B9562F27D01762F987,
	ARRaycastManager_RegisterRaycaster_m6A40E4C8E52CF603432C13E0048EA64521E75418,
	ARRaycastManager_UnregisterRaycaster_mEED237BC2FDEA7DCCD8AEB2C46A618C5357B22C4,
	ARRaycastManager_OnAfterStart_m177F70E50FD2E573CF4D688773269380CA0E5C73,
	ARRaycastManager_RaycastViewportAsRay_m4482E0D42A28C0AB579EEA16B359897DAC247C87,
	ARRaycastManager_RaycastViewport_mBD7C00F6EEF1CA76E80ED9C3EA2EAFCAA2C7405D,
	ARRaycastManager_RaycastRay_m16F2A0CB579339B8E9467DB8A93BFE34804D3B22,
	ARRaycastManager_RaycastHitComparer_mF66FE66AAD0E8B1C223B8363CC5404543986AF54,
	ARRaycastManager_RaycastFallback_m2F2CFEBE5FC835576D03745E899818115084885D,
	ARRaycastManager_TransformAndDisposeNativeHitResults_m1E00EAF176C4264D6402C0FAA1A82AEED41CE545,
	ARRaycastManager_ConstructIfNecessary_m6D8C34B3EBAAA21205EEF23FDC100A3CD0FDC4EE,
	ARRaycastManager_Awake_mD449B9047D9C92A0257735EFCCD5EEFDB6B9B00A,
	ARRaycastManager__ctor_mF86FCE8D20CD564A99EAD6820D17A93CE0CE8906,
	ARRaycastManager__cctor_mB93E1AC04873B8406D58B274ED18F89DAD31000C,
	ARReferencePoint_get_nativePtr_m3A5EBB10CDB229BEDF6F9A32DB344F7D7E60C425,
	ARReferencePoint__ctor_mBF1A6D8BB55E72D2421479015293E6983284B492,
	ARReferencePointManager_get_referencePointPrefab_m0B582D7592957A7B3FBB56FF9FE89023EA09A4A7,
	ARReferencePointManager_set_referencePointPrefab_m2099BFBB205C215C5ABE952CB9FC27D7990DE980,
	ARReferencePointManager_add_referencePointsChanged_m5BDB05B2E802FDE47B989FED8B630905E0F631AE,
	ARReferencePointManager_remove_referencePointsChanged_mFF6485AE35BB1B27D458622285FE7B901DD934CE,
	ARReferencePointManager_AddReferencePoint_m69E8A182F4236F19B516DB2A7533D40366F737C1,
	ARReferencePointManager_AttachReferencePoint_mDAB5752D0A684707D0B8A5F994D58A939482DFAF,
	ARReferencePointManager_RemoveReferencePoint_m1AC3A26D9ADC6861C20DA44C66FF8C445C4031CA,
	ARReferencePointManager_GetReferencePoint_mB7E635DBEEB46117809F74000544885B6276C736,
	ARReferencePointManager_GetPrefab_mBCCAE5E2794AEB9C7A71036FDE1A52E72F570E3E,
	ARReferencePointManager_get_gameObjectName_mC6C01BD63D4E5F83A030F0719CA89FEA1597EE8D,
	ARReferencePointManager_OnTrackablesChanged_mF782BA36C8A0584FE4A43098C6250EE0A0AC49DB,
	ARReferencePointManager__ctor_mD7E1B733733585176F2239B8FB5BCB70AE57D027,
	ARReferencePointsChangedEventArgs_get_added_mABB62AC87103053E3FE9610B0242D97C8B6B949A,
	ARReferencePointsChangedEventArgs_set_added_mC4359784851149411C6182FD35628B46E9394BA0,
	ARReferencePointsChangedEventArgs_get_updated_mE33F7C73AB2E3D7329A351F4CFF05A9F1651DD72,
	ARReferencePointsChangedEventArgs_set_updated_m31B2E7CDD0D3F91C6F816967FB894A5474C044EB,
	ARReferencePointsChangedEventArgs_get_removed_m6D5B3717AC83FB6F1CB0C332D90903E7396F142A,
	ARReferencePointsChangedEventArgs_set_removed_m7E1839AA6025FE381D0B66153360ED5102627397,
	ARReferencePointsChangedEventArgs__ctor_mDA783F2639DF702E800713983C3995142AE05894,
	ARReferencePointsChangedEventArgs_GetHashCode_m64B6156588CC25C2724164B57E539D5214A3CC03,
	ARReferencePointsChangedEventArgs_Equals_mEDD233C7AF5F3E512CBD8D5DCE01C4563BB40801,
	ARReferencePointsChangedEventArgs_ToString_mEEA97C554FAC8D8247313ED6003B55211C503BFB,
	ARReferencePointsChangedEventArgs_Equals_m872DC192FEA15EC16A6E6B6884A420ECBBF3A887,
	ARReferencePointsChangedEventArgs_op_Equality_m1866410B0B84DFD0CCDF36BBEA8C2D507F23C55A,
	ARReferencePointsChangedEventArgs_op_Inequality_m86A7E7867272015EA605293E9F94EC2EC4636711,
	ARSession_get_attemptUpdate_m0A4410AE45968D8DB68B3F3B27C19D5DE1B73D24,
	ARSession_set_attemptUpdate_mFF7E5FBE5A24F0B19FF23FB852D4A0563D47F273,
	ARSession_get_matchFrameRate_m59AE94947F30496E5A38E6AB6BB51F848E0BFE04,
	ARSession_set_matchFrameRate_m224070636779F553E69CBD52112A7518B5FE3373,
	ARSession_add_stateChanged_m4D2004D8967341410C3726EE786C3ABA971D937C,
	ARSession_remove_stateChanged_mF32B8A10ED41FD873374EE0F3C3A8F550CD912B1,
	ARSession_get_state_m6B8AE895247F4F9A8D76749E447637BC7FBEE639,
	ARSession_set_state_mBBCFE2FBD1A465FCBEAFAC911F648FA1C8EAE71B,
	ARSession_get_notTrackingReason_mAE083BBF91B9C076B53887EEF6C823DA8720EF1C,
	ARSession_Reset_mFAD3C148FDE97C7A90B4E8C166F93C3C2D11894D,
	ARSession_SetMatchFrameRateEnabled_m3F0CC4B76EB7EF2E963DC13E1DF154B1CB49007C,
	ARSession_WarnIfMultipleARSessions_mFB245A2EAF514D74AB16343680013B836999994E,
	ARSession_CheckAvailability_mCB651B104BBF6ED4C3041D86C625EC1178318931,
	ARSession_Install_m4C15ED3F78EE143E765FE98CE54351BBBD2BBCB2,
	ARSession_OnEnable_m278A9E1F0715381458F24B6834AFD930C1A4E750,
	ARSession_Initialize_mA14AF59D418374508717C2E885A4BFFA3F4F1817,
	ARSession_StartSubsystem_mF5BC087C1EDE948B5BC4CA6ADD47E004A31EDCC0,
	ARSession_Awake_m0E45C4E48B5352BF288BC0392186061C0076B531,
	ARSession_Update_m5F845E6E9DACEF91167155BA894CBA73AFB5BED6,
	ARSession_OnApplicationPause_m701633E71A1517E0D89DB0D4348CF00297A40A35,
	ARSession_OnDisable_m518EAC900DFA0E9553BF08382C18A9F813DB1D14,
	ARSession_OnDestroy_m5163A3F69D7739D0BAD0046D423561BBE457F27B,
	ARSession_UpdateNotTrackingReason_m5F58154AC1A855819FD0C952708FB85C2515170A,
	ARSession__ctor_mE345E2948F903454DF7484566DF13BEF689215CA,
	ARSessionOrigin_get_camera_m6809A44CAB9A1E35394FB329780A6C5B56B8CEBB,
	ARSessionOrigin_set_camera_m851561DFDA4B9520A6F0EAA1D3A53128A07BD351,
	ARSessionOrigin_get_trackablesParent_m37049D7E75CF694834A140C2EACB15D2D1098505,
	ARSessionOrigin_set_trackablesParent_m1DC880A558CB7E18D31ADFBFEFC583F807076E0E,
	ARSessionOrigin_get_contentOffsetTransform_m656172F5E167D7DC3C1217098B39A7368E03CA89,
	ARSessionOrigin_MakeContentAppearAt_mBF6FBDBD3CC75ABA8C4C929EC7239B602E205A54,
	ARSessionOrigin_MakeContentAppearAt_m5808AA16F1126DC24F5595C35BB949F528A3CDF3,
	ARSessionOrigin_MakeContentAppearAt_mE6BF5A0A537C751E67AAA85C56387AF1AEF1EDFB,
	ARSessionOrigin_Awake_m696618685A6FC1F64CE085A5A331C5E8781472BA,
	ARSessionOrigin_GetCameraOriginPose_m75D111317C0E96F4B80715ECAB89CB4062A915AD,
	ARSessionOrigin_Update_mD080963394C175FD90C6F945F6776D7D456BA45F,
	ARSessionOrigin__ctor_m8419EDC8402212489DB65B9B79D74AC3E1F2C81C,
	ARSessionStateChangedEventArgs_get_state_mF0E9D3730C795A07DDD65F845E4291D2F9B70638,
	ARSessionStateChangedEventArgs_set_state_m9CFA0FF2E49408B0BF09EB38F4F650A792BF6C9C,
	ARSessionStateChangedEventArgs__ctor_m7473A94BDBF05370C5A4EC71665D08283FE5F895,
	ARSessionStateChangedEventArgs_GetHashCode_m8D2A1E07AB9A28442B186F7B68C2FB324F25468D,
	ARSessionStateChangedEventArgs_Equals_mB7F8D129E9C22D5F3B8F1DC75772AD99B5C66804,
	ARSessionStateChangedEventArgs_ToString_mEFF33F8DE367398EE4ABBF971A58E7D9B3EEF689,
	ARSessionStateChangedEventArgs_Equals_mA3A45D328EB6F00C559FE9CF5DDA1CC80FD93AA5,
	ARSessionStateChangedEventArgs_op_Equality_mD585630AE06115C088579E1224450B294A6AAC0E,
	ARSessionStateChangedEventArgs_op_Inequality_m6399CDE5CA3B3C993A7A315E39ACE9204248B5AB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ARTrackedImage_get_extents_mCC673F1F9276D44374B0B9F896D05F47F9AA5D9D,
	ARTrackedImage_get_size_m8D617290A171CEB929A4453518510F8B0E63151A,
	ARTrackedImage_get_nativePtr_mF5EBB2FDA7853386D6833EA956AAB2F2EC8B3C98,
	ARTrackedImage_get_referenceImage_m46AAAD2FF5BF6E2A9AB8192BCE198FB69F00CC7D,
	ARTrackedImage_set_referenceImage_m9A54F39853DE3CEA412E5615D21354ACD43FAD49,
	ARTrackedImage__ctor_m9279D554E8FEE057263849B153821D7D5692E6FC,
	ARTrackedImageManager_get_referenceLibrary_mF77A49EFFEA34DA7DFCE3D1465DB95D1BC1E78F7,
	ARTrackedImageManager_set_referenceLibrary_m9041D9DB7FDA1199763F05C5EC5E8B1DAB7C16E4,
	ARTrackedImageManager_get_maxNumberOfMovingImages_mF868D79ED343BF25B1EA71F66A45153250244FF4,
	ARTrackedImageManager_set_maxNumberOfMovingImages_m534F24C3CA00D0E4525A5CFAC5D5D6840F2E04CD,
	ARTrackedImageManager_get_trackedImagePrefab_m2FA5CB1B5E3288918F6E3C2641E86D02A4C7C25B,
	ARTrackedImageManager_set_trackedImagePrefab_m8EC64DA46478F3D3CC1BC8EFDA9EAD2180B56C4E,
	ARTrackedImageManager_GetPrefab_mAC0361F8CA22F0A1DBE0378C9AAF6FF3122BF18C,
	ARTrackedImageManager_add_trackedImagesChanged_m7B52639FCC946FABAEEF0CB060121E8BACC58680,
	ARTrackedImageManager_remove_trackedImagesChanged_m9604E04925AF884CFD0CDD6350230837BBF3F1CE,
	ARTrackedImageManager_get_gameObjectName_m87203D5BF386D83F3011C6A1CD05DE1A631A41CE,
	ARTrackedImageManager_Awake_m6BBE2C67172C868880C19AB1D4E2AE28B3F02CCE,
	ARTrackedImageManager_OnBeforeStart_mE529D68F75923CDC23CA4188119B046F0648C35F,
	ARTrackedImageManager_OnAfterSetSessionRelativeData_m4F532D0C1EBABFFB2206CDC4FEE477792970EF26,
	ARTrackedImageManager_OnTrackablesChanged_mB08CF35175DEEC1A1118DD107D8CDE2D4FC86353,
	ARTrackedImageManager_UpdateReferenceImages_m77C55FA1B3B3F3C3492530F7CD7CE889650BDE95,
	ARTrackedImageManager_SetMaxNumberOfMovingImages_m162FD8AFB46D42230B073ACDC9F3C998069761DF,
	ARTrackedImageManager__ctor_m9B19F213E9517752243F7B6C6FE38B992D5EB48A,
	ARTrackedImagesChangedEventArgs_get_added_mFEE1ADD4190DA3C25E6B0E05CC1BC87A76C8FDC6,
	ARTrackedImagesChangedEventArgs_set_added_mA40CAA72DD703D1BE3C327F9DB44E56D61A39573,
	ARTrackedImagesChangedEventArgs_get_updated_m2C4491B11EEE724FF2122574EADB018DCA6E4BAF,
	ARTrackedImagesChangedEventArgs_set_updated_m9C6A583C91E216FD3295B1FB6FCE56AED492CD14,
	ARTrackedImagesChangedEventArgs_get_removed_m6BB8A2862669BC4B46FCFC2C263FAE2F7A029877,
	ARTrackedImagesChangedEventArgs_set_removed_m5D5E3D10D70CE63F950D9D0915DB8961E25D6A85,
	ARTrackedImagesChangedEventArgs__ctor_m476333D62E0654548D6EF73052FC5D081B1B0CCC,
	ARTrackedImagesChangedEventArgs_GetHashCode_m1F5268F315485431CE66DE629BC4F15426FE5E8C,
	ARTrackedImagesChangedEventArgs_Equals_mE07DEE967AAE86BE3CFFF4E7EEB2917927E88C68,
	ARTrackedImagesChangedEventArgs_ToString_mFC911B5451161F9880E9022AC963BD7F199C7CEA,
	ARTrackedImagesChangedEventArgs_Equals_m33FBEDB48ED3AFF8E2606FAE1D5A333662756EB2,
	ARTrackedImagesChangedEventArgs_op_Equality_mEAB400A4615B80C1ABEE139ADB3981696AFFD3BB,
	ARTrackedImagesChangedEventArgs_op_Inequality_mD03EE8661982F348034CEF5581B207593D961629,
	NULL,
	MeshInfoComparer_Compare_m69E96DBA032DF793CBBA543077E28E3E7677D935,
	MeshInfoComparer__ctor_mAF91CAA6ECB0892B884BE071D53A449026A31872,
	MeshQueue_EnqueueUnique_mABB321C4B2F28750D316D47F3779A2EE40F0CCFA,
	MeshQueue_get_count_m19DEE003D25B036B7D6C1F8CCCD0CCE70C895279,
	MeshQueue_TryDequeue_m0F05CB38B5EEBFC114B190E1E927BC4843083B44,
	MeshQueue_Remove_m5064AE2F02E6478173A9EC39D772A33D536DCB85,
	MeshQueue_InsertNew_m5F757162F7F6B2090078B398E914C644D74BBF27,
	MeshQueue_UpdateExisting_m850816A4EEF6C6C56CB5E76AC68D4D5085F4AB4B,
	MeshQueue_Clear_mAA94499967ABCF17CDD05BF5C2A011B1D325B996,
	MeshQueue__ctor_m91C9238B2896CC30A1E8304213A597CE28531E24,
	PlaneDetectionModeMaskAttribute__ctor_mB7FA9E69B382217E801BA121DA5029F784055AC4,
	PoseExtensions_InverseTransformPosition_mCDC0B4E47294498D315D25A11F55A3DCB1D56B30,
	PoseExtensions_InverseTransformDirection_m30D10D1F00928AC1DC560E31DC9536E3CA77B87C,
	PoseExtensions_InverseTransformPositions_m2DA3D0DC2772553353753870F5CC0BB39AC6FFF0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TransformExtensions_TransformRay_m146923AB98EFC9BED518F02A742A628A45D3A20A,
	TransformExtensions_InverseTransformRay_mD6C666D44E1BF8407A2AA50118A7C90890935503,
	TransformExtensions_TransformPose_m677CE84C622BD23C3DDB2953DDB820E1934B0144,
	TransformExtensions_InverseTransformPose_m7508C8166573E2D0042170C4F09BFAE44A064545,
	TransformExtensions_TransformPointList_m2154D9E7CE80E84A5D831D5A9037815C6BCD80B2,
	TransformExtensions_InverseTransformPointList_mD21F038FA2CFC45650C1D3A0B9EFAAB6F01C2A0B,
	TransformExtensions_SetLayerRecursively_m8D28346FC588296B9DF07B68D468733228925F92,
	TextureInfo__ctor_mEDA31195012270B48B7C4B8C208FA60144F9F5D7,
	TextureInfo_get_descriptor_m18FDFD0838ED7ED90257C6069B98FD62CCCEA495,
	TextureInfo_get_texture_m433B9BD7E92AE791892952E3608DF9D19079B708,
	TextureInfo_GetUpdatedTextureInfo_m003D8CA0166E6937460A2978ECAA51EDC13CB79B,
	TextureInfo_DestroyTexture_m050A83AF559AC94D9F42AAD9A1F278E1979AE873,
	TextureInfo_CreateTexture_mD448B5A95A8B5D7ABA21C91C5DFF3E5F720E710B,
	TrackableIdComparer_Compare_m9E90B709F9E6A4E771A0EDDF8B0D1665ED8D6E57,
	TrackableIdComparer__ctor_m8B57F6CB93FD1D0614E25DF11D00B1CAAD5E81D3,
	PointCloudRaycastJob_Execute_mEB5E8A731796250ED6FB44512D0FDDDD6FC380C8,
	PointCloudRaycastCollectResultsJob_Execute_m3968794C96B4131517E9F464564C67AEAA02E1F2,
	U3CCheckAvailabilityU3Ed__19__ctor_m08C1CA60DCCF91B61F68BCC1336CFFDFEBBFBD4A,
	U3CCheckAvailabilityU3Ed__19_System_IDisposable_Dispose_m4C4ADDC32E52285BA2A6896E7E0324B1BA78E370,
	U3CCheckAvailabilityU3Ed__19_MoveNext_m0EB56BDDB1F92E89B25E4B3BC31F5EDF6BC442F4,
	U3CCheckAvailabilityU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A56DCE0CEFEF9017E61B8FBD4402FBBF6EB2A85,
	U3CCheckAvailabilityU3Ed__19_System_Collections_IEnumerator_Reset_m4357AD5282BFDCE4F14D28301FC2931DC6DFD0C8,
	U3CCheckAvailabilityU3Ed__19_System_Collections_IEnumerator_get_Current_m610B26B3A0864D10B52509D509BFC342632BF32A,
	U3CInstallU3Ed__20__ctor_m2F75745CA503E960D14CDC1F92C5DD66B679ACB6,
	U3CInstallU3Ed__20_System_IDisposable_Dispose_mA295F395FAA9895AE9EC4744024D06D10F28E071,
	U3CInstallU3Ed__20_MoveNext_m4F94D4CD3BD5F915B10EC9321357B6A69A654908,
	U3CInstallU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55478D5D86B9A5514681B9F6AE388D80CC8844F9,
	U3CInstallU3Ed__20_System_Collections_IEnumerator_Reset_mE6463D86E8B16A8995B5E0A91041AE1190AC978E,
	U3CInstallU3Ed__20_System_Collections_IEnumerator_get_Current_m853254C986ED2DCADA7172E87250714975634ACB,
	U3CInitializeU3Ed__22__ctor_mA856A0CC49FA4039FE1B3520DA0545E6E99E7EA5,
	U3CInitializeU3Ed__22_System_IDisposable_Dispose_m2B0131451DF62D9F01A3A543C228C7AC9D91A606,
	U3CInitializeU3Ed__22_MoveNext_m15B37CEB613B082CD374FD573D4CCDAFC719B280,
	U3CInitializeU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D493CF82413AA98CC4445F406FC2E7ECB9226B6,
	U3CInitializeU3Ed__22_System_Collections_IEnumerator_Reset_m3CD8DF951E64226B6553E14B3D438D4C87E13BA8,
	U3CInitializeU3Ed__22_System_Collections_IEnumerator_get_Current_m878C21107C0E73FA127198E15F77D49D50D9265E,
	NULL,
	NULL,
	NULL,
	NULL,
};
extern void ARCameraFrameEventArgs_get_lightEstimation_m4F226F18EC0E91AEA1F152B165703869D31C9600_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_set_lightEstimation_mE109AD7E91548FEF74DF045FB85B598210895657_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_get_timestampNs_m26633672AA4D433551422517ADA8F0ECED1B1A9E_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_set_timestampNs_mDA520DBF61A930154758C9FF620B639861EE920B_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_get_projectionMatrix_mEBEA65211112C8080149BB535637D3BED21EC273_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_set_projectionMatrix_mC6137EDADA29E8383CFFAAB4F9C839A176220142_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_get_displayMatrix_m9FCD5AF461B96162C2D1E9CCBEDE76974F32AAB0_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_set_displayMatrix_mC37DE33664D4EBA5788105502751A981E37E71AB_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_get_textures_m7062757F64E5497A602D2E5E55929ECE9C9CAC58_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_set_textures_m11E835B5FF52F8FACF26836715ED85B785FB3E79_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_get_propertyNameIds_m84B811AA86E7BE4E1D8D79C88F7892D35FF38946_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_set_propertyNameIds_m122F7477BC8728536A0C240FD62A2CDFA26743A9_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_GetHashCode_m406B3D310D34F27D4BEAC18178A13E57F1BF8E7E_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_Equals_mFB12BA2256481CDBA32F843FA14CE365E9FB1F17_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_ToString_m49FFD9A837C51548F9F25E341869E9CA1637C108_AdjustorThunk (void);
extern void ARCameraFrameEventArgs_Equals_m3C3D3D4D19A38B553D21664F58B52A6DDF745AE8_AdjustorThunk (void);
extern void AREnvironmentProbesChangedEvent_get_added_m9BB5E81DFA9C421C674F7A730155A8401E1909C4_AdjustorThunk (void);
extern void AREnvironmentProbesChangedEvent_set_added_mBA9CCAEA557D0793D123E22B4DDAA8FC8B2A8F5B_AdjustorThunk (void);
extern void AREnvironmentProbesChangedEvent_get_updated_m64A6BF00CB47EB9AF3D5F868254CBC3F6768681C_AdjustorThunk (void);
extern void AREnvironmentProbesChangedEvent_set_updated_m1EC2DA3AD720493D88D6EE126CABCE43B1041821_AdjustorThunk (void);
extern void AREnvironmentProbesChangedEvent_get_removed_mE849182B7E2F464312887B3DDCFE153E73872CCE_AdjustorThunk (void);
extern void AREnvironmentProbesChangedEvent_set_removed_mBA8F20E7B9B35B23023E5513D4F908951FA96738_AdjustorThunk (void);
extern void AREnvironmentProbesChangedEvent__ctor_m6B3E7444BC6E8E50407CAE8FF136B128F85B71CC_AdjustorThunk (void);
extern void AREnvironmentProbesChangedEvent_GetHashCode_mB2DA47D276F5AFD887049840F016940EAF1913B7_AdjustorThunk (void);
extern void AREnvironmentProbesChangedEvent_Equals_m7F5FF62122376EB1CD88F50959F6A297A36E42BC_AdjustorThunk (void);
extern void AREnvironmentProbesChangedEvent_ToString_m917D72B83F15A2DAABCAE4BAA80F68F9259E4304_AdjustorThunk (void);
extern void AREnvironmentProbesChangedEvent_Equals_mA2A9DD5565680439D9C22E9972C4C7582B341739_AdjustorThunk (void);
extern void ARFaceUpdatedEventArgs_get_face_mECE51067B415A317E9F43CFDAD67D17E81D93CA0_AdjustorThunk (void);
extern void ARFaceUpdatedEventArgs_set_face_mB64678F0DDE0D4C5CA7F729D338EDD86C04B16C4_AdjustorThunk (void);
extern void ARFaceUpdatedEventArgs__ctor_m3FB5F8EE705C0F4F9F10194CEAF835AFF04F4B81_AdjustorThunk (void);
extern void ARFaceUpdatedEventArgs_GetHashCode_m941B2292502C893BCAA6808A6D71F36935B470BA_AdjustorThunk (void);
extern void ARFaceUpdatedEventArgs_Equals_m08148B0B8B75DA7D44C1DFE693386D7E82959F35_AdjustorThunk (void);
extern void ARFaceUpdatedEventArgs_Equals_mFFCF8F02B10115669047395AF4BC245EE74ACBB9_AdjustorThunk (void);
extern void ARFacesChangedEventArgs_get_added_m78E85E8C2A286ADA233ACB9EE04CED7F71100889_AdjustorThunk (void);
extern void ARFacesChangedEventArgs_set_added_mFDB1B3EFA9E097B2AA4EBABCA139B79E89759AC5_AdjustorThunk (void);
extern void ARFacesChangedEventArgs_get_updated_m7877418FAC42584DA75C820DD34928418F3336BD_AdjustorThunk (void);
extern void ARFacesChangedEventArgs_set_updated_m3993021EC9472BCE3E9A993C3C4B40D5318D8352_AdjustorThunk (void);
extern void ARFacesChangedEventArgs_get_removed_m2A3A1BAC623C9D00D642422772FFC5F90961A729_AdjustorThunk (void);
extern void ARFacesChangedEventArgs_set_removed_m670EC88FBAF24F8F7FD8CD3E1DCF57765B538CEA_AdjustorThunk (void);
extern void ARFacesChangedEventArgs__ctor_m4033277E36C6AE75A2561BE9E1B8B88652FDDE25_AdjustorThunk (void);
extern void ARFacesChangedEventArgs_GetHashCode_mB888F6AF90BC4A28ABC14AE8A5CBA2080D404D42_AdjustorThunk (void);
extern void ARFacesChangedEventArgs_Equals_m2869D9928B1AF6540AA012371399B69CE46ADC83_AdjustorThunk (void);
extern void ARFacesChangedEventArgs_ToString_m6EE382E0E4CC5C5540D9918D346C0085E13C3C1D_AdjustorThunk (void);
extern void ARFacesChangedEventArgs_Equals_mE74910D42FA40C32E16E8C9D14506277BBBF4149_AdjustorThunk (void);
extern void ARFoundationBackgroundRendererChangedEventArgs_GetHashCode_m8C37A3197487C438791442E94615A4158AB90C81_AdjustorThunk (void);
extern void ARFoundationBackgroundRendererChangedEventArgs_Equals_mDB252BA86BC70B445DD29B5BD144B3605E764AB0_AdjustorThunk (void);
extern void ARFoundationBackgroundRendererChangedEventArgs_Equals_m13499F72035E3D1175ACDB88B3520BDF83DF0B9C_AdjustorThunk (void);
extern void ARLightEstimationData_get_averageBrightness_mB339C036AE0CBCF8DAA8FCDE4A4D04A19EFB7233_AdjustorThunk (void);
extern void ARLightEstimationData_set_averageBrightness_m9C0FEF62B371B067D51E1E930A42ABE40F0EEDAB_AdjustorThunk (void);
extern void ARLightEstimationData_get_averageColorTemperature_m7C37859EB80C2606270F72DEF3E292299CE81778_AdjustorThunk (void);
extern void ARLightEstimationData_set_averageColorTemperature_mDCDE66CD7D7D2BD24434DE00D68B171449D9207A_AdjustorThunk (void);
extern void ARLightEstimationData_get_colorCorrection_mD318D06DB8535937FD4BB5E5DEF1BDE6471A2A2D_AdjustorThunk (void);
extern void ARLightEstimationData_set_colorCorrection_m7551D5F797A9E203BAB64416C4B1637848A7EBBD_AdjustorThunk (void);
extern void ARLightEstimationData_GetHashCode_mE8A3FF44F64AEC6C3FCFF1629777C45EDD89B06D_AdjustorThunk (void);
extern void ARLightEstimationData_Equals_mAB9336917F38BCB1ACD25EFE8B7F6C07C979F478_AdjustorThunk (void);
extern void ARLightEstimationData_ToString_m4068C6C855EABAA601CB607A080453B094479DE1_AdjustorThunk (void);
extern void ARLightEstimationData_Equals_m2A61749DD4B3227BE1727779503F7580C05F8495_AdjustorThunk (void);
extern void ARMeshesChangedEventArgs_get_added_m416B22A9406017FC5E40EB473774CE9149EB1BC7_AdjustorThunk (void);
extern void ARMeshesChangedEventArgs_set_added_m07AC944AFF173C597B045BB60E40BCEE02A646FF_AdjustorThunk (void);
extern void ARMeshesChangedEventArgs_get_updated_m3E49A6C32750983B744168851591634FDA4B542F_AdjustorThunk (void);
extern void ARMeshesChangedEventArgs_set_updated_m2A97C612DD863055AE6CEA0D1598E687A93EEAEF_AdjustorThunk (void);
extern void ARMeshesChangedEventArgs_get_removed_m88699CFBAE47A01A6C053A351E02D32B7C61E084_AdjustorThunk (void);
extern void ARMeshesChangedEventArgs_set_removed_mB3D40BCE5693E2436C19CA66879C0C1F3B57756A_AdjustorThunk (void);
extern void ARMeshesChangedEventArgs__ctor_m40CF8F7E678F5D03DEB1AE5AE6330F6F6B2958EE_AdjustorThunk (void);
extern void ARMeshesChangedEventArgs_GetHashCode_m7E79DE6C2468873DC4CD40005D850221480E34E6_AdjustorThunk (void);
extern void ARMeshesChangedEventArgs_Equals_mB538D8CA2C6C59B16D1F67E354D49FB6B47B0EE4_AdjustorThunk (void);
extern void ARMeshesChangedEventArgs_ToString_m0ED4C46371B9B64DAA5956A5A90CF5933B300AFF_AdjustorThunk (void);
extern void ARMeshesChangedEventArgs_Equals_m20539FBB146BA19C19AE6996CF6FD53492C57A8F_AdjustorThunk (void);
extern void ARPlaneBoundaryChangedEventArgs_get_plane_mE14F32BD989ADD2F0FBDBAB9AB293CDAB4CCB03B_AdjustorThunk (void);
extern void ARPlaneBoundaryChangedEventArgs_set_plane_m5DC6678054CF729894BBE1BB359ECD6ABB21C018_AdjustorThunk (void);
extern void ARPlaneBoundaryChangedEventArgs__ctor_mCF92554C8AB31E6FB30004DE9480EE749767720D_AdjustorThunk (void);
extern void ARPlaneBoundaryChangedEventArgs_GetHashCode_m6380A99937DA7544630D3E7D025BCCBA75C92E2C_AdjustorThunk (void);
extern void ARPlaneBoundaryChangedEventArgs_Equals_mCAE90FE42E59A12583A7EA34758578304D10BEC4_AdjustorThunk (void);
extern void ARPlaneBoundaryChangedEventArgs_ToString_m459F343E2184B7AFE40965471117B25F7783F873_AdjustorThunk (void);
extern void ARPlaneBoundaryChangedEventArgs_Equals_m5168B0124859C641CFE47EF454D01E1C42BBE2AA_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_get_added_m26B6F1AEAD621FD8A939521235C7B45AE08FDA1D_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_set_added_m363F2A1024103B4E741741E4BBB09F952293E21B_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_get_updated_m674C4C76BC3C8A2D49793B254B103021A4E33C5A_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_set_updated_mDB2AA3FD2A483CE633ABBDBBEC8C5193B421D5A6_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_get_removed_m5F0AC740E7C61322659EC224A4EEC4DBC8B3E405_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_set_removed_mD249F70F7D051CE0030B6D671DE8FDE8CEEDD860_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs__ctor_m88626999299D44A1F2D39060A37CCE03B6A7B51C_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_GetHashCode_m12A40FC12326E1B13B615985A5CC188ADA83088B_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_Equals_m004DBCB6C164D7FBAA6EA23D6FDDD7730795EFEE_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_ToString_mB3CAC22890529BED6B5FA38DAE92D2DFC250DB72_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_Equals_m82227BEF1235CDE33ADCE37382B7964629CFBAD9_AdjustorThunk (void);
extern void ARPointCloudChangedEventArgs_get_added_m9F934F1FA3322402E50F72E9B78669042367DF75_AdjustorThunk (void);
extern void ARPointCloudChangedEventArgs_set_added_m21B8ABD00EFDA37DB3409B10A07F45B26C34F9B6_AdjustorThunk (void);
extern void ARPointCloudChangedEventArgs_get_updated_mCE2A84457D78408052DAF251ACFC8D591D9B0FBA_AdjustorThunk (void);
extern void ARPointCloudChangedEventArgs_set_updated_mDCB2376E57C283B084A04870BAAFB597989D45C3_AdjustorThunk (void);
extern void ARPointCloudChangedEventArgs_get_removed_mCAFAC3A438975DE62575CB41D02A46ABD8C67842_AdjustorThunk (void);
extern void ARPointCloudChangedEventArgs_set_removed_m36A3675CA4A35C7061EA7F5EEF114C56F932A076_AdjustorThunk (void);
extern void ARPointCloudChangedEventArgs__ctor_m0BF590246450830BDA1923F1C79B383BD032FD40_AdjustorThunk (void);
extern void ARPointCloudChangedEventArgs_GetHashCode_m28D5E34EB0E025E21935BF0FD1E46A35C49FC04C_AdjustorThunk (void);
extern void ARPointCloudChangedEventArgs_Equals_m6BD50C01CB9435D9CBC7BE0F913199CAD3C38D4D_AdjustorThunk (void);
extern void ARPointCloudChangedEventArgs_ToString_m96CAAE9D0926B9A3CE4FC16D942AE7571EDC83A5_AdjustorThunk (void);
extern void ARPointCloudChangedEventArgs_Equals_mF2BB005AFBD3B0418D29AAC35DDE9566C23A14FE_AdjustorThunk (void);
extern void ARPointCloudUpdatedEventArgs_GetHashCode_mD52F0A42535E9D590F2065D60644655D80DC3C22_AdjustorThunk (void);
extern void ARPointCloudUpdatedEventArgs_Equals_mBD558C0754E1A1C6BA9864C5891A282F6B977C9E_AdjustorThunk (void);
extern void ARPointCloudUpdatedEventArgs_Equals_mB39450D04B879FE647C18AB10C409523B5FF4195_AdjustorThunk (void);
extern void ARRaycastHit__ctor_m7E37CAC9301AAB566F3B8751091D3E472BB5C2E8_AdjustorThunk (void);
extern void ARRaycastHit_get_distance_m14A7CD73EDA065E5E722BB48BD2FB3420BC6CFDC_AdjustorThunk (void);
extern void ARRaycastHit_set_distance_m04D1E140673EA5F48185A2C0C78D138957D7010E_AdjustorThunk (void);
extern void ARRaycastHit_get_hitType_m9F24C8C20D2985ACFBBAD4F7EEA509BE470CFB00_AdjustorThunk (void);
extern void ARRaycastHit_get_pose_m5CCFFED6C4A101EA42083A8661956A2B4B4C4A0D_AdjustorThunk (void);
extern void ARRaycastHit_get_trackableId_m6FBBEF54882143C2EE439CE919D131CD71EBE972_AdjustorThunk (void);
extern void ARRaycastHit_get_sessionRelativePose_m0BFDEFFEE9453DE6A3EBF1F478D04C6CCD1E93AE_AdjustorThunk (void);
extern void ARRaycastHit_get_sessionRelativeDistance_mDCDED72A207F7C1D59BFE84815C181A29AF8C059_AdjustorThunk (void);
extern void ARRaycastHit_GetHashCode_m0ADC3CEFC9A9617A4F61616CE9981A98D7926525_AdjustorThunk (void);
extern void ARRaycastHit_Equals_mDA8FEFBD8AB244631961E9C9EA43D3A1B5D2F894_AdjustorThunk (void);
extern void ARRaycastHit_Equals_mE086AFC2E5237C054CC2D38B22B7506E892B090E_AdjustorThunk (void);
extern void ARRaycastHit_CompareTo_m3F5B58CE479D3DDB7503B9C288C497AF2E3AE99E_AdjustorThunk (void);
extern void ARReferencePointsChangedEventArgs_get_added_mABB62AC87103053E3FE9610B0242D97C8B6B949A_AdjustorThunk (void);
extern void ARReferencePointsChangedEventArgs_set_added_mC4359784851149411C6182FD35628B46E9394BA0_AdjustorThunk (void);
extern void ARReferencePointsChangedEventArgs_get_updated_mE33F7C73AB2E3D7329A351F4CFF05A9F1651DD72_AdjustorThunk (void);
extern void ARReferencePointsChangedEventArgs_set_updated_m31B2E7CDD0D3F91C6F816967FB894A5474C044EB_AdjustorThunk (void);
extern void ARReferencePointsChangedEventArgs_get_removed_m6D5B3717AC83FB6F1CB0C332D90903E7396F142A_AdjustorThunk (void);
extern void ARReferencePointsChangedEventArgs_set_removed_m7E1839AA6025FE381D0B66153360ED5102627397_AdjustorThunk (void);
extern void ARReferencePointsChangedEventArgs__ctor_mDA783F2639DF702E800713983C3995142AE05894_AdjustorThunk (void);
extern void ARReferencePointsChangedEventArgs_GetHashCode_m64B6156588CC25C2724164B57E539D5214A3CC03_AdjustorThunk (void);
extern void ARReferencePointsChangedEventArgs_Equals_mEDD233C7AF5F3E512CBD8D5DCE01C4563BB40801_AdjustorThunk (void);
extern void ARReferencePointsChangedEventArgs_ToString_mEEA97C554FAC8D8247313ED6003B55211C503BFB_AdjustorThunk (void);
extern void ARReferencePointsChangedEventArgs_Equals_m872DC192FEA15EC16A6E6B6884A420ECBBF3A887_AdjustorThunk (void);
extern void ARSessionStateChangedEventArgs_get_state_mF0E9D3730C795A07DDD65F845E4291D2F9B70638_AdjustorThunk (void);
extern void ARSessionStateChangedEventArgs_set_state_m9CFA0FF2E49408B0BF09EB38F4F650A792BF6C9C_AdjustorThunk (void);
extern void ARSessionStateChangedEventArgs__ctor_m7473A94BDBF05370C5A4EC71665D08283FE5F895_AdjustorThunk (void);
extern void ARSessionStateChangedEventArgs_GetHashCode_m8D2A1E07AB9A28442B186F7B68C2FB324F25468D_AdjustorThunk (void);
extern void ARSessionStateChangedEventArgs_Equals_mB7F8D129E9C22D5F3B8F1DC75772AD99B5C66804_AdjustorThunk (void);
extern void ARSessionStateChangedEventArgs_ToString_mEFF33F8DE367398EE4ABBF971A58E7D9B3EEF689_AdjustorThunk (void);
extern void ARSessionStateChangedEventArgs_Equals_mA3A45D328EB6F00C559FE9CF5DDA1CC80FD93AA5_AdjustorThunk (void);
extern void ARTrackedImagesChangedEventArgs_get_added_mFEE1ADD4190DA3C25E6B0E05CC1BC87A76C8FDC6_AdjustorThunk (void);
extern void ARTrackedImagesChangedEventArgs_set_added_mA40CAA72DD703D1BE3C327F9DB44E56D61A39573_AdjustorThunk (void);
extern void ARTrackedImagesChangedEventArgs_get_updated_m2C4491B11EEE724FF2122574EADB018DCA6E4BAF_AdjustorThunk (void);
extern void ARTrackedImagesChangedEventArgs_set_updated_m9C6A583C91E216FD3295B1FB6FCE56AED492CD14_AdjustorThunk (void);
extern void ARTrackedImagesChangedEventArgs_get_removed_m6BB8A2862669BC4B46FCFC2C263FAE2F7A029877_AdjustorThunk (void);
extern void ARTrackedImagesChangedEventArgs_set_removed_m5D5E3D10D70CE63F950D9D0915DB8961E25D6A85_AdjustorThunk (void);
extern void ARTrackedImagesChangedEventArgs__ctor_m476333D62E0654548D6EF73052FC5D081B1B0CCC_AdjustorThunk (void);
extern void ARTrackedImagesChangedEventArgs_GetHashCode_m1F5268F315485431CE66DE629BC4F15426FE5E8C_AdjustorThunk (void);
extern void ARTrackedImagesChangedEventArgs_Equals_mE07DEE967AAE86BE3CFFF4E7EEB2917927E88C68_AdjustorThunk (void);
extern void ARTrackedImagesChangedEventArgs_ToString_mFC911B5451161F9880E9022AC963BD7F199C7CEA_AdjustorThunk (void);
extern void ARTrackedImagesChangedEventArgs_Equals_m33FBEDB48ED3AFF8E2606FAE1D5A333662756EB2_AdjustorThunk (void);
extern void TextureInfo__ctor_mEDA31195012270B48B7C4B8C208FA60144F9F5D7_AdjustorThunk (void);
extern void TextureInfo_get_descriptor_m18FDFD0838ED7ED90257C6069B98FD62CCCEA495_AdjustorThunk (void);
extern void TextureInfo_get_texture_m433B9BD7E92AE791892952E3608DF9D19079B708_AdjustorThunk (void);
extern void TextureInfo_DestroyTexture_m050A83AF559AC94D9F42AAD9A1F278E1979AE873_AdjustorThunk (void);
extern void PointCloudRaycastJob_Execute_mEB5E8A731796250ED6FB44512D0FDDDD6FC380C8_AdjustorThunk (void);
extern void PointCloudRaycastCollectResultsJob_Execute_m3968794C96B4131517E9F464564C67AEAA02E1F2_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[147] = 
{
	{ 0x06000020, ARCameraFrameEventArgs_get_lightEstimation_m4F226F18EC0E91AEA1F152B165703869D31C9600_AdjustorThunk },
	{ 0x06000021, ARCameraFrameEventArgs_set_lightEstimation_mE109AD7E91548FEF74DF045FB85B598210895657_AdjustorThunk },
	{ 0x06000022, ARCameraFrameEventArgs_get_timestampNs_m26633672AA4D433551422517ADA8F0ECED1B1A9E_AdjustorThunk },
	{ 0x06000023, ARCameraFrameEventArgs_set_timestampNs_mDA520DBF61A930154758C9FF620B639861EE920B_AdjustorThunk },
	{ 0x06000024, ARCameraFrameEventArgs_get_projectionMatrix_mEBEA65211112C8080149BB535637D3BED21EC273_AdjustorThunk },
	{ 0x06000025, ARCameraFrameEventArgs_set_projectionMatrix_mC6137EDADA29E8383CFFAAB4F9C839A176220142_AdjustorThunk },
	{ 0x06000026, ARCameraFrameEventArgs_get_displayMatrix_m9FCD5AF461B96162C2D1E9CCBEDE76974F32AAB0_AdjustorThunk },
	{ 0x06000027, ARCameraFrameEventArgs_set_displayMatrix_mC37DE33664D4EBA5788105502751A981E37E71AB_AdjustorThunk },
	{ 0x06000028, ARCameraFrameEventArgs_get_textures_m7062757F64E5497A602D2E5E55929ECE9C9CAC58_AdjustorThunk },
	{ 0x06000029, ARCameraFrameEventArgs_set_textures_m11E835B5FF52F8FACF26836715ED85B785FB3E79_AdjustorThunk },
	{ 0x0600002A, ARCameraFrameEventArgs_get_propertyNameIds_m84B811AA86E7BE4E1D8D79C88F7892D35FF38946_AdjustorThunk },
	{ 0x0600002B, ARCameraFrameEventArgs_set_propertyNameIds_m122F7477BC8728536A0C240FD62A2CDFA26743A9_AdjustorThunk },
	{ 0x0600002C, ARCameraFrameEventArgs_GetHashCode_m406B3D310D34F27D4BEAC18178A13E57F1BF8E7E_AdjustorThunk },
	{ 0x0600002D, ARCameraFrameEventArgs_Equals_mFB12BA2256481CDBA32F843FA14CE365E9FB1F17_AdjustorThunk },
	{ 0x0600002E, ARCameraFrameEventArgs_ToString_m49FFD9A837C51548F9F25E341869E9CA1637C108_AdjustorThunk },
	{ 0x0600002F, ARCameraFrameEventArgs_Equals_m3C3D3D4D19A38B553D21664F58B52A6DDF745AE8_AdjustorThunk },
	{ 0x0600006A, AREnvironmentProbesChangedEvent_get_added_m9BB5E81DFA9C421C674F7A730155A8401E1909C4_AdjustorThunk },
	{ 0x0600006B, AREnvironmentProbesChangedEvent_set_added_mBA9CCAEA557D0793D123E22B4DDAA8FC8B2A8F5B_AdjustorThunk },
	{ 0x0600006C, AREnvironmentProbesChangedEvent_get_updated_m64A6BF00CB47EB9AF3D5F868254CBC3F6768681C_AdjustorThunk },
	{ 0x0600006D, AREnvironmentProbesChangedEvent_set_updated_m1EC2DA3AD720493D88D6EE126CABCE43B1041821_AdjustorThunk },
	{ 0x0600006E, AREnvironmentProbesChangedEvent_get_removed_mE849182B7E2F464312887B3DDCFE153E73872CCE_AdjustorThunk },
	{ 0x0600006F, AREnvironmentProbesChangedEvent_set_removed_mBA8F20E7B9B35B23023E5513D4F908951FA96738_AdjustorThunk },
	{ 0x06000070, AREnvironmentProbesChangedEvent__ctor_m6B3E7444BC6E8E50407CAE8FF136B128F85B71CC_AdjustorThunk },
	{ 0x06000071, AREnvironmentProbesChangedEvent_GetHashCode_mB2DA47D276F5AFD887049840F016940EAF1913B7_AdjustorThunk },
	{ 0x06000072, AREnvironmentProbesChangedEvent_Equals_m7F5FF62122376EB1CD88F50959F6A297A36E42BC_AdjustorThunk },
	{ 0x06000073, AREnvironmentProbesChangedEvent_ToString_m917D72B83F15A2DAABCAE4BAA80F68F9259E4304_AdjustorThunk },
	{ 0x06000074, AREnvironmentProbesChangedEvent_Equals_mA2A9DD5565680439D9C22E9972C4C7582B341739_AdjustorThunk },
	{ 0x0600009A, ARFaceUpdatedEventArgs_get_face_mECE51067B415A317E9F43CFDAD67D17E81D93CA0_AdjustorThunk },
	{ 0x0600009B, ARFaceUpdatedEventArgs_set_face_mB64678F0DDE0D4C5CA7F729D338EDD86C04B16C4_AdjustorThunk },
	{ 0x0600009C, ARFaceUpdatedEventArgs__ctor_m3FB5F8EE705C0F4F9F10194CEAF835AFF04F4B81_AdjustorThunk },
	{ 0x0600009D, ARFaceUpdatedEventArgs_GetHashCode_m941B2292502C893BCAA6808A6D71F36935B470BA_AdjustorThunk },
	{ 0x0600009E, ARFaceUpdatedEventArgs_Equals_m08148B0B8B75DA7D44C1DFE693386D7E82959F35_AdjustorThunk },
	{ 0x0600009F, ARFaceUpdatedEventArgs_Equals_mFFCF8F02B10115669047395AF4BC245EE74ACBB9_AdjustorThunk },
	{ 0x060000A2, ARFacesChangedEventArgs_get_added_m78E85E8C2A286ADA233ACB9EE04CED7F71100889_AdjustorThunk },
	{ 0x060000A3, ARFacesChangedEventArgs_set_added_mFDB1B3EFA9E097B2AA4EBABCA139B79E89759AC5_AdjustorThunk },
	{ 0x060000A4, ARFacesChangedEventArgs_get_updated_m7877418FAC42584DA75C820DD34928418F3336BD_AdjustorThunk },
	{ 0x060000A5, ARFacesChangedEventArgs_set_updated_m3993021EC9472BCE3E9A993C3C4B40D5318D8352_AdjustorThunk },
	{ 0x060000A6, ARFacesChangedEventArgs_get_removed_m2A3A1BAC623C9D00D642422772FFC5F90961A729_AdjustorThunk },
	{ 0x060000A7, ARFacesChangedEventArgs_set_removed_m670EC88FBAF24F8F7FD8CD3E1DCF57765B538CEA_AdjustorThunk },
	{ 0x060000A8, ARFacesChangedEventArgs__ctor_m4033277E36C6AE75A2561BE9E1B8B88652FDDE25_AdjustorThunk },
	{ 0x060000A9, ARFacesChangedEventArgs_GetHashCode_mB888F6AF90BC4A28ABC14AE8A5CBA2080D404D42_AdjustorThunk },
	{ 0x060000AA, ARFacesChangedEventArgs_Equals_m2869D9928B1AF6540AA012371399B69CE46ADC83_AdjustorThunk },
	{ 0x060000AB, ARFacesChangedEventArgs_ToString_m6EE382E0E4CC5C5540D9918D346C0085E13C3C1D_AdjustorThunk },
	{ 0x060000AC, ARFacesChangedEventArgs_Equals_mE74910D42FA40C32E16E8C9D14506277BBBF4149_AdjustorThunk },
	{ 0x060000C1, ARFoundationBackgroundRendererChangedEventArgs_GetHashCode_m8C37A3197487C438791442E94615A4158AB90C81_AdjustorThunk },
	{ 0x060000C2, ARFoundationBackgroundRendererChangedEventArgs_Equals_mDB252BA86BC70B445DD29B5BD144B3605E764AB0_AdjustorThunk },
	{ 0x060000C3, ARFoundationBackgroundRendererChangedEventArgs_Equals_m13499F72035E3D1175ACDB88B3520BDF83DF0B9C_AdjustorThunk },
	{ 0x060000D0, ARLightEstimationData_get_averageBrightness_mB339C036AE0CBCF8DAA8FCDE4A4D04A19EFB7233_AdjustorThunk },
	{ 0x060000D1, ARLightEstimationData_set_averageBrightness_m9C0FEF62B371B067D51E1E930A42ABE40F0EEDAB_AdjustorThunk },
	{ 0x060000D2, ARLightEstimationData_get_averageColorTemperature_m7C37859EB80C2606270F72DEF3E292299CE81778_AdjustorThunk },
	{ 0x060000D3, ARLightEstimationData_set_averageColorTemperature_mDCDE66CD7D7D2BD24434DE00D68B171449D9207A_AdjustorThunk },
	{ 0x060000D4, ARLightEstimationData_get_colorCorrection_mD318D06DB8535937FD4BB5E5DEF1BDE6471A2A2D_AdjustorThunk },
	{ 0x060000D5, ARLightEstimationData_set_colorCorrection_m7551D5F797A9E203BAB64416C4B1637848A7EBBD_AdjustorThunk },
	{ 0x060000D6, ARLightEstimationData_GetHashCode_mE8A3FF44F64AEC6C3FCFF1629777C45EDD89B06D_AdjustorThunk },
	{ 0x060000D7, ARLightEstimationData_Equals_mAB9336917F38BCB1ACD25EFE8B7F6C07C979F478_AdjustorThunk },
	{ 0x060000D8, ARLightEstimationData_ToString_m4068C6C855EABAA601CB607A080453B094479DE1_AdjustorThunk },
	{ 0x060000D9, ARLightEstimationData_Equals_m2A61749DD4B3227BE1727779503F7580C05F8495_AdjustorThunk },
	{ 0x06000100, ARMeshesChangedEventArgs_get_added_m416B22A9406017FC5E40EB473774CE9149EB1BC7_AdjustorThunk },
	{ 0x06000101, ARMeshesChangedEventArgs_set_added_m07AC944AFF173C597B045BB60E40BCEE02A646FF_AdjustorThunk },
	{ 0x06000102, ARMeshesChangedEventArgs_get_updated_m3E49A6C32750983B744168851591634FDA4B542F_AdjustorThunk },
	{ 0x06000103, ARMeshesChangedEventArgs_set_updated_m2A97C612DD863055AE6CEA0D1598E687A93EEAEF_AdjustorThunk },
	{ 0x06000104, ARMeshesChangedEventArgs_get_removed_m88699CFBAE47A01A6C053A351E02D32B7C61E084_AdjustorThunk },
	{ 0x06000105, ARMeshesChangedEventArgs_set_removed_mB3D40BCE5693E2436C19CA66879C0C1F3B57756A_AdjustorThunk },
	{ 0x06000106, ARMeshesChangedEventArgs__ctor_m40CF8F7E678F5D03DEB1AE5AE6330F6F6B2958EE_AdjustorThunk },
	{ 0x06000107, ARMeshesChangedEventArgs_GetHashCode_m7E79DE6C2468873DC4CD40005D850221480E34E6_AdjustorThunk },
	{ 0x06000108, ARMeshesChangedEventArgs_Equals_mB538D8CA2C6C59B16D1F67E354D49FB6B47B0EE4_AdjustorThunk },
	{ 0x06000109, ARMeshesChangedEventArgs_ToString_m0ED4C46371B9B64DAA5956A5A90CF5933B300AFF_AdjustorThunk },
	{ 0x0600010A, ARMeshesChangedEventArgs_Equals_m20539FBB146BA19C19AE6996CF6FD53492C57A8F_AdjustorThunk },
	{ 0x06000123, ARPlaneBoundaryChangedEventArgs_get_plane_mE14F32BD989ADD2F0FBDBAB9AB293CDAB4CCB03B_AdjustorThunk },
	{ 0x06000124, ARPlaneBoundaryChangedEventArgs_set_plane_m5DC6678054CF729894BBE1BB359ECD6ABB21C018_AdjustorThunk },
	{ 0x06000125, ARPlaneBoundaryChangedEventArgs__ctor_mCF92554C8AB31E6FB30004DE9480EE749767720D_AdjustorThunk },
	{ 0x06000126, ARPlaneBoundaryChangedEventArgs_GetHashCode_m6380A99937DA7544630D3E7D025BCCBA75C92E2C_AdjustorThunk },
	{ 0x06000127, ARPlaneBoundaryChangedEventArgs_Equals_mCAE90FE42E59A12583A7EA34758578304D10BEC4_AdjustorThunk },
	{ 0x06000128, ARPlaneBoundaryChangedEventArgs_ToString_m459F343E2184B7AFE40965471117B25F7783F873_AdjustorThunk },
	{ 0x06000129, ARPlaneBoundaryChangedEventArgs_Equals_m5168B0124859C641CFE47EF454D01E1C42BBE2AA_AdjustorThunk },
	{ 0x0600014E, ARPlanesChangedEventArgs_get_added_m26B6F1AEAD621FD8A939521235C7B45AE08FDA1D_AdjustorThunk },
	{ 0x0600014F, ARPlanesChangedEventArgs_set_added_m363F2A1024103B4E741741E4BBB09F952293E21B_AdjustorThunk },
	{ 0x06000150, ARPlanesChangedEventArgs_get_updated_m674C4C76BC3C8A2D49793B254B103021A4E33C5A_AdjustorThunk },
	{ 0x06000151, ARPlanesChangedEventArgs_set_updated_mDB2AA3FD2A483CE633ABBDBBEC8C5193B421D5A6_AdjustorThunk },
	{ 0x06000152, ARPlanesChangedEventArgs_get_removed_m5F0AC740E7C61322659EC224A4EEC4DBC8B3E405_AdjustorThunk },
	{ 0x06000153, ARPlanesChangedEventArgs_set_removed_mD249F70F7D051CE0030B6D671DE8FDE8CEEDD860_AdjustorThunk },
	{ 0x06000154, ARPlanesChangedEventArgs__ctor_m88626999299D44A1F2D39060A37CCE03B6A7B51C_AdjustorThunk },
	{ 0x06000155, ARPlanesChangedEventArgs_GetHashCode_m12A40FC12326E1B13B615985A5CC188ADA83088B_AdjustorThunk },
	{ 0x06000156, ARPlanesChangedEventArgs_Equals_m004DBCB6C164D7FBAA6EA23D6FDDD7730795EFEE_AdjustorThunk },
	{ 0x06000157, ARPlanesChangedEventArgs_ToString_mB3CAC22890529BED6B5FA38DAE92D2DFC250DB72_AdjustorThunk },
	{ 0x06000158, ARPlanesChangedEventArgs_Equals_m82227BEF1235CDE33ADCE37382B7964629CFBAD9_AdjustorThunk },
	{ 0x06000165, ARPointCloudChangedEventArgs_get_added_m9F934F1FA3322402E50F72E9B78669042367DF75_AdjustorThunk },
	{ 0x06000166, ARPointCloudChangedEventArgs_set_added_m21B8ABD00EFDA37DB3409B10A07F45B26C34F9B6_AdjustorThunk },
	{ 0x06000167, ARPointCloudChangedEventArgs_get_updated_mCE2A84457D78408052DAF251ACFC8D591D9B0FBA_AdjustorThunk },
	{ 0x06000168, ARPointCloudChangedEventArgs_set_updated_mDCB2376E57C283B084A04870BAAFB597989D45C3_AdjustorThunk },
	{ 0x06000169, ARPointCloudChangedEventArgs_get_removed_mCAFAC3A438975DE62575CB41D02A46ABD8C67842_AdjustorThunk },
	{ 0x0600016A, ARPointCloudChangedEventArgs_set_removed_m36A3675CA4A35C7061EA7F5EEF114C56F932A076_AdjustorThunk },
	{ 0x0600016B, ARPointCloudChangedEventArgs__ctor_m0BF590246450830BDA1923F1C79B383BD032FD40_AdjustorThunk },
	{ 0x0600016C, ARPointCloudChangedEventArgs_GetHashCode_m28D5E34EB0E025E21935BF0FD1E46A35C49FC04C_AdjustorThunk },
	{ 0x0600016D, ARPointCloudChangedEventArgs_Equals_m6BD50C01CB9435D9CBC7BE0F913199CAD3C38D4D_AdjustorThunk },
	{ 0x0600016E, ARPointCloudChangedEventArgs_ToString_m96CAAE9D0926B9A3CE4FC16D942AE7571EDC83A5_AdjustorThunk },
	{ 0x0600016F, ARPointCloudChangedEventArgs_Equals_mF2BB005AFBD3B0418D29AAC35DDE9566C23A14FE_AdjustorThunk },
	{ 0x06000193, ARPointCloudUpdatedEventArgs_GetHashCode_mD52F0A42535E9D590F2065D60644655D80DC3C22_AdjustorThunk },
	{ 0x06000194, ARPointCloudUpdatedEventArgs_Equals_mBD558C0754E1A1C6BA9864C5891A282F6B977C9E_AdjustorThunk },
	{ 0x06000195, ARPointCloudUpdatedEventArgs_Equals_mB39450D04B879FE647C18AB10C409523B5FF4195_AdjustorThunk },
	{ 0x06000198, ARRaycastHit__ctor_m7E37CAC9301AAB566F3B8751091D3E472BB5C2E8_AdjustorThunk },
	{ 0x06000199, ARRaycastHit_get_distance_m14A7CD73EDA065E5E722BB48BD2FB3420BC6CFDC_AdjustorThunk },
	{ 0x0600019A, ARRaycastHit_set_distance_m04D1E140673EA5F48185A2C0C78D138957D7010E_AdjustorThunk },
	{ 0x0600019B, ARRaycastHit_get_hitType_m9F24C8C20D2985ACFBBAD4F7EEA509BE470CFB00_AdjustorThunk },
	{ 0x0600019C, ARRaycastHit_get_pose_m5CCFFED6C4A101EA42083A8661956A2B4B4C4A0D_AdjustorThunk },
	{ 0x0600019D, ARRaycastHit_get_trackableId_m6FBBEF54882143C2EE439CE919D131CD71EBE972_AdjustorThunk },
	{ 0x0600019E, ARRaycastHit_get_sessionRelativePose_m0BFDEFFEE9453DE6A3EBF1F478D04C6CCD1E93AE_AdjustorThunk },
	{ 0x0600019F, ARRaycastHit_get_sessionRelativeDistance_mDCDED72A207F7C1D59BFE84815C181A29AF8C059_AdjustorThunk },
	{ 0x060001A0, ARRaycastHit_GetHashCode_m0ADC3CEFC9A9617A4F61616CE9981A98D7926525_AdjustorThunk },
	{ 0x060001A1, ARRaycastHit_Equals_mDA8FEFBD8AB244631961E9C9EA43D3A1B5D2F894_AdjustorThunk },
	{ 0x060001A2, ARRaycastHit_Equals_mE086AFC2E5237C054CC2D38B22B7506E892B090E_AdjustorThunk },
	{ 0x060001A5, ARRaycastHit_CompareTo_m3F5B58CE479D3DDB7503B9C288C497AF2E3AE99E_AdjustorThunk },
	{ 0x060001C4, ARReferencePointsChangedEventArgs_get_added_mABB62AC87103053E3FE9610B0242D97C8B6B949A_AdjustorThunk },
	{ 0x060001C5, ARReferencePointsChangedEventArgs_set_added_mC4359784851149411C6182FD35628B46E9394BA0_AdjustorThunk },
	{ 0x060001C6, ARReferencePointsChangedEventArgs_get_updated_mE33F7C73AB2E3D7329A351F4CFF05A9F1651DD72_AdjustorThunk },
	{ 0x060001C7, ARReferencePointsChangedEventArgs_set_updated_m31B2E7CDD0D3F91C6F816967FB894A5474C044EB_AdjustorThunk },
	{ 0x060001C8, ARReferencePointsChangedEventArgs_get_removed_m6D5B3717AC83FB6F1CB0C332D90903E7396F142A_AdjustorThunk },
	{ 0x060001C9, ARReferencePointsChangedEventArgs_set_removed_m7E1839AA6025FE381D0B66153360ED5102627397_AdjustorThunk },
	{ 0x060001CA, ARReferencePointsChangedEventArgs__ctor_mDA783F2639DF702E800713983C3995142AE05894_AdjustorThunk },
	{ 0x060001CB, ARReferencePointsChangedEventArgs_GetHashCode_m64B6156588CC25C2724164B57E539D5214A3CC03_AdjustorThunk },
	{ 0x060001CC, ARReferencePointsChangedEventArgs_Equals_mEDD233C7AF5F3E512CBD8D5DCE01C4563BB40801_AdjustorThunk },
	{ 0x060001CD, ARReferencePointsChangedEventArgs_ToString_mEEA97C554FAC8D8247313ED6003B55211C503BFB_AdjustorThunk },
	{ 0x060001CE, ARReferencePointsChangedEventArgs_Equals_m872DC192FEA15EC16A6E6B6884A420ECBBF3A887_AdjustorThunk },
	{ 0x060001F5, ARSessionStateChangedEventArgs_get_state_mF0E9D3730C795A07DDD65F845E4291D2F9B70638_AdjustorThunk },
	{ 0x060001F6, ARSessionStateChangedEventArgs_set_state_m9CFA0FF2E49408B0BF09EB38F4F650A792BF6C9C_AdjustorThunk },
	{ 0x060001F7, ARSessionStateChangedEventArgs__ctor_m7473A94BDBF05370C5A4EC71665D08283FE5F895_AdjustorThunk },
	{ 0x060001F8, ARSessionStateChangedEventArgs_GetHashCode_m8D2A1E07AB9A28442B186F7B68C2FB324F25468D_AdjustorThunk },
	{ 0x060001F9, ARSessionStateChangedEventArgs_Equals_mB7F8D129E9C22D5F3B8F1DC75772AD99B5C66804_AdjustorThunk },
	{ 0x060001FA, ARSessionStateChangedEventArgs_ToString_mEFF33F8DE367398EE4ABBF971A58E7D9B3EEF689_AdjustorThunk },
	{ 0x060001FB, ARSessionStateChangedEventArgs_Equals_mA3A45D328EB6F00C559FE9CF5DDA1CC80FD93AA5_AdjustorThunk },
	{ 0x06000236, ARTrackedImagesChangedEventArgs_get_added_mFEE1ADD4190DA3C25E6B0E05CC1BC87A76C8FDC6_AdjustorThunk },
	{ 0x06000237, ARTrackedImagesChangedEventArgs_set_added_mA40CAA72DD703D1BE3C327F9DB44E56D61A39573_AdjustorThunk },
	{ 0x06000238, ARTrackedImagesChangedEventArgs_get_updated_m2C4491B11EEE724FF2122574EADB018DCA6E4BAF_AdjustorThunk },
	{ 0x06000239, ARTrackedImagesChangedEventArgs_set_updated_m9C6A583C91E216FD3295B1FB6FCE56AED492CD14_AdjustorThunk },
	{ 0x0600023A, ARTrackedImagesChangedEventArgs_get_removed_m6BB8A2862669BC4B46FCFC2C263FAE2F7A029877_AdjustorThunk },
	{ 0x0600023B, ARTrackedImagesChangedEventArgs_set_removed_m5D5E3D10D70CE63F950D9D0915DB8961E25D6A85_AdjustorThunk },
	{ 0x0600023C, ARTrackedImagesChangedEventArgs__ctor_m476333D62E0654548D6EF73052FC5D081B1B0CCC_AdjustorThunk },
	{ 0x0600023D, ARTrackedImagesChangedEventArgs_GetHashCode_m1F5268F315485431CE66DE629BC4F15426FE5E8C_AdjustorThunk },
	{ 0x0600023E, ARTrackedImagesChangedEventArgs_Equals_mE07DEE967AAE86BE3CFFF4E7EEB2917927E88C68_AdjustorThunk },
	{ 0x0600023F, ARTrackedImagesChangedEventArgs_ToString_mFC911B5451161F9880E9022AC963BD7F199C7CEA_AdjustorThunk },
	{ 0x06000240, ARTrackedImagesChangedEventArgs_Equals_m33FBEDB48ED3AFF8E2606FAE1D5A333662756EB2_AdjustorThunk },
	{ 0x0600026B, TextureInfo__ctor_mEDA31195012270B48B7C4B8C208FA60144F9F5D7_AdjustorThunk },
	{ 0x0600026C, TextureInfo_get_descriptor_m18FDFD0838ED7ED90257C6069B98FD62CCCEA495_AdjustorThunk },
	{ 0x0600026D, TextureInfo_get_texture_m433B9BD7E92AE791892952E3608DF9D19079B708_AdjustorThunk },
	{ 0x0600026F, TextureInfo_DestroyTexture_m050A83AF559AC94D9F42AAD9A1F278E1979AE873_AdjustorThunk },
	{ 0x06000273, PointCloudRaycastJob_Execute_mEB5E8A731796250ED6FB44512D0FDDDD6FC380C8_AdjustorThunk },
	{ 0x06000274, PointCloudRaycastCollectResultsJob_Execute_m3968794C96B4131517E9F464564C67AEAA02E1F2_AdjustorThunk },
};
static const int32_t s_InvokerIndices[650] = 
{
	14,
	26,
	14,
	23,
	89,
	31,
	14,
	26,
	14,
	26,
	89,
	31,
	14,
	26,
	14,
	26,
	14,
	2634,
	23,
	23,
	23,
	23,
	2635,
	23,
	14,
	14,
	10,
	32,
	89,
	23,
	3,
	2636,
	2637,
	2638,
	2426,
	2639,
	2640,
	2639,
	2640,
	14,
	26,
	14,
	26,
	10,
	9,
	14,
	2641,
	2642,
	2642,
	10,
	32,
	10,
	32,
	89,
	26,
	26,
	14,
	842,
	2166,
	2167,
	2168,
	842,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2643,
	23,
	3,
	10,
	32,
	10,
	32,
	1539,
	1539,
	15,
	2200,
	23,
	23,
	2644,
	2647,
	14,
	23,
	89,
	31,
	10,
	32,
	14,
	26,
	26,
	26,
	2648,
	2649,
	9,
	14,
	14,
	23,
	23,
	197,
	26,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	197,
	10,
	9,
	14,
	2650,
	2651,
	2651,
	26,
	26,
	2191,
	2191,
	2212,
	2213,
	23,
	23,
	-1,
	26,
	23,
	14,
	26,
	89,
	26,
	26,
	2648,
	23,
	2652,
	197,
	14,
	14,
	23,
	14,
	26,
	31,
	-1,
	23,
	23,
	2653,
	2635,
	23,
	23,
	23,
	23,
	14,
	26,
	26,
	10,
	9,
	2654,
	2655,
	2655,
	14,
	26,
	14,
	26,
	14,
	26,
	197,
	10,
	9,
	14,
	2656,
	2657,
	2657,
	164,
	26,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	10,
	32,
	154,
	89,
	23,
	89,
	89,
	23,
	3,
	10,
	9,
	2658,
	2659,
	2659,
	14,
	26,
	23,
	23,
	23,
	23,
	14,
	14,
	23,
	3,
	2660,
	2422,
	2660,
	2422,
	2661,
	2662,
	10,
	9,
	14,
	2663,
	2664,
	2664,
	14,
	26,
	726,
	334,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	10,
	32,
	26,
	26,
	14,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	2665,
	23,
	23,
	23,
	2648,
	2666,
	2667,
	23,
	14,
	23,
	3,
	14,
	26,
	14,
	26,
	14,
	26,
	197,
	10,
	9,
	14,
	2669,
	2670,
	2670,
	726,
	334,
	26,
	26,
	1539,
	14,
	26,
	10,
	1549,
	1539,
	1549,
	1549,
	2671,
	15,
	2213,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	26,
	10,
	9,
	14,
	2672,
	2673,
	2673,
	14,
	26,
	10,
	32,
	26,
	26,
	2648,
	2242,
	1657,
	2674,
	14,
	23,
	2675,
	197,
	14,
	23,
	23,
	23,
	3,
	2676,
	2677,
	2678,
	3,
	14,
	26,
	2679,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	197,
	10,
	9,
	14,
	2680,
	2681,
	2681,
	26,
	26,
	2191,
	2194,
	2193,
	23,
	23,
	-1,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	197,
	10,
	9,
	14,
	2682,
	2683,
	2683,
	14,
	26,
	26,
	26,
	23,
	23,
	14,
	14,
	2684,
	197,
	2242,
	-1,
	23,
	14,
	26,
	2685,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	3,
	2685,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	3,
	10,
	9,
	2686,
	2687,
	2687,
	2688,
	726,
	334,
	10,
	2189,
	2188,
	2189,
	726,
	10,
	9,
	2689,
	2690,
	2690,
	2691,
	2692,
	2693,
	2694,
	26,
	26,
	23,
	2243,
	2243,
	2242,
	2695,
	2242,
	2696,
	23,
	23,
	23,
	3,
	15,
	23,
	14,
	26,
	26,
	26,
	2697,
	2698,
	9,
	2648,
	14,
	14,
	197,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	197,
	10,
	9,
	14,
	2699,
	2700,
	2700,
	89,
	31,
	89,
	31,
	154,
	154,
	106,
	164,
	106,
	23,
	31,
	23,
	4,
	4,
	23,
	14,
	23,
	23,
	23,
	31,
	23,
	23,
	3,
	23,
	14,
	26,
	14,
	26,
	14,
	2701,
	2702,
	2703,
	23,
	2189,
	23,
	23,
	10,
	32,
	32,
	10,
	9,
	14,
	2704,
	2705,
	2705,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1549,
	1549,
	15,
	2706,
	2707,
	23,
	14,
	26,
	10,
	32,
	14,
	26,
	14,
	26,
	26,
	14,
	23,
	23,
	2708,
	197,
	23,
	32,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	197,
	10,
	9,
	14,
	2709,
	2710,
	2710,
	2242,
	2711,
	23,
	2712,
	10,
	796,
	2146,
	2712,
	2712,
	23,
	23,
	23,
	2713,
	2713,
	2714,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2715,
	2715,
	2716,
	2716,
	137,
	137,
	372,
	2644,
	2200,
	14,
	2645,
	23,
	2646,
	2668,
	23,
	32,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[1] = 
{
	{ 0x060000AF, 8,  (void**)&ARFoundationBackgroundRenderer_ResetGlState_mF47B550F91756D479D3E93AAB9D69C674892E94E_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[9] = 
{
	{ 0x0200002B, { 22, 3 } },
	{ 0x0200002C, { 25, 57 } },
	{ 0x02000036, { 82, 24 } },
	{ 0x02000037, { 106, 5 } },
	{ 0x02000041, { 111, 5 } },
	{ 0x0600007F, { 0, 4 } },
	{ 0x06000091, { 4, 8 } },
	{ 0x06000162, { 12, 4 } },
	{ 0x0600017D, { 16, 6 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[116] = 
{
	{ (Il2CppRGCTXDataType)3, 20991 },
	{ (Il2CppRGCTXDataType)3, 20992 },
	{ (Il2CppRGCTXDataType)3, 20993 },
	{ (Il2CppRGCTXDataType)3, 20994 },
	{ (Il2CppRGCTXDataType)3, 20995 },
	{ (Il2CppRGCTXDataType)3, 20996 },
	{ (Il2CppRGCTXDataType)3, 20997 },
	{ (Il2CppRGCTXDataType)3, 20998 },
	{ (Il2CppRGCTXDataType)3, 20999 },
	{ (Il2CppRGCTXDataType)3, 21000 },
	{ (Il2CppRGCTXDataType)3, 21001 },
	{ (Il2CppRGCTXDataType)2, 33218 },
	{ (Il2CppRGCTXDataType)3, 21002 },
	{ (Il2CppRGCTXDataType)3, 21003 },
	{ (Il2CppRGCTXDataType)3, 21004 },
	{ (Il2CppRGCTXDataType)3, 21005 },
	{ (Il2CppRGCTXDataType)3, 21006 },
	{ (Il2CppRGCTXDataType)3, 21007 },
	{ (Il2CppRGCTXDataType)3, 21008 },
	{ (Il2CppRGCTXDataType)2, 31531 },
	{ (Il2CppRGCTXDataType)3, 21009 },
	{ (Il2CppRGCTXDataType)3, 21010 },
	{ (Il2CppRGCTXDataType)3, 21011 },
	{ (Il2CppRGCTXDataType)2, 31590 },
	{ (Il2CppRGCTXDataType)3, 21012 },
	{ (Il2CppRGCTXDataType)2, 31599 },
	{ (Il2CppRGCTXDataType)3, 21013 },
	{ (Il2CppRGCTXDataType)2, 33219 },
	{ (Il2CppRGCTXDataType)3, 21014 },
	{ (Il2CppRGCTXDataType)3, 21015 },
	{ (Il2CppRGCTXDataType)3, 21016 },
	{ (Il2CppRGCTXDataType)2, 31597 },
	{ (Il2CppRGCTXDataType)3, 21017 },
	{ (Il2CppRGCTXDataType)2, 33220 },
	{ (Il2CppRGCTXDataType)2, 31596 },
	{ (Il2CppRGCTXDataType)3, 21018 },
	{ (Il2CppRGCTXDataType)3, 21019 },
	{ (Il2CppRGCTXDataType)3, 21020 },
	{ (Il2CppRGCTXDataType)3, 21021 },
	{ (Il2CppRGCTXDataType)3, 21022 },
	{ (Il2CppRGCTXDataType)3, 21023 },
	{ (Il2CppRGCTXDataType)3, 21024 },
	{ (Il2CppRGCTXDataType)3, 21025 },
	{ (Il2CppRGCTXDataType)2, 33221 },
	{ (Il2CppRGCTXDataType)3, 21026 },
	{ (Il2CppRGCTXDataType)3, 21027 },
	{ (Il2CppRGCTXDataType)3, 21028 },
	{ (Il2CppRGCTXDataType)3, 21029 },
	{ (Il2CppRGCTXDataType)2, 33222 },
	{ (Il2CppRGCTXDataType)3, 21030 },
	{ (Il2CppRGCTXDataType)3, 21031 },
	{ (Il2CppRGCTXDataType)3, 21032 },
	{ (Il2CppRGCTXDataType)3, 21033 },
	{ (Il2CppRGCTXDataType)3, 21034 },
	{ (Il2CppRGCTXDataType)3, 21035 },
	{ (Il2CppRGCTXDataType)2, 33223 },
	{ (Il2CppRGCTXDataType)2, 31600 },
	{ (Il2CppRGCTXDataType)3, 21036 },
	{ (Il2CppRGCTXDataType)3, 21037 },
	{ (Il2CppRGCTXDataType)3, 21038 },
	{ (Il2CppRGCTXDataType)3, 21039 },
	{ (Il2CppRGCTXDataType)3, 21040 },
	{ (Il2CppRGCTXDataType)3, 21041 },
	{ (Il2CppRGCTXDataType)3, 21042 },
	{ (Il2CppRGCTXDataType)3, 21043 },
	{ (Il2CppRGCTXDataType)3, 21044 },
	{ (Il2CppRGCTXDataType)3, 21045 },
	{ (Il2CppRGCTXDataType)3, 21046 },
	{ (Il2CppRGCTXDataType)3, 21047 },
	{ (Il2CppRGCTXDataType)3, 21048 },
	{ (Il2CppRGCTXDataType)3, 21049 },
	{ (Il2CppRGCTXDataType)3, 21050 },
	{ (Il2CppRGCTXDataType)2, 31602 },
	{ (Il2CppRGCTXDataType)3, 21051 },
	{ (Il2CppRGCTXDataType)3, 21052 },
	{ (Il2CppRGCTXDataType)3, 21053 },
	{ (Il2CppRGCTXDataType)3, 21054 },
	{ (Il2CppRGCTXDataType)3, 21055 },
	{ (Il2CppRGCTXDataType)3, 21056 },
	{ (Il2CppRGCTXDataType)3, 21057 },
	{ (Il2CppRGCTXDataType)2, 31601 },
	{ (Il2CppRGCTXDataType)3, 21058 },
	{ (Il2CppRGCTXDataType)3, 21059 },
	{ (Il2CppRGCTXDataType)2, 31633 },
	{ (Il2CppRGCTXDataType)3, 21060 },
	{ (Il2CppRGCTXDataType)2, 33224 },
	{ (Il2CppRGCTXDataType)3, 21061 },
	{ (Il2CppRGCTXDataType)3, 21062 },
	{ (Il2CppRGCTXDataType)3, 21063 },
	{ (Il2CppRGCTXDataType)1, 31633 },
	{ (Il2CppRGCTXDataType)2, 31634 },
	{ (Il2CppRGCTXDataType)3, 21064 },
	{ (Il2CppRGCTXDataType)3, 21065 },
	{ (Il2CppRGCTXDataType)3, 21066 },
	{ (Il2CppRGCTXDataType)3, 21067 },
	{ (Il2CppRGCTXDataType)3, 21068 },
	{ (Il2CppRGCTXDataType)3, 21069 },
	{ (Il2CppRGCTXDataType)3, 21070 },
	{ (Il2CppRGCTXDataType)3, 21071 },
	{ (Il2CppRGCTXDataType)2, 33225 },
	{ (Il2CppRGCTXDataType)3, 21072 },
	{ (Il2CppRGCTXDataType)3, 21073 },
	{ (Il2CppRGCTXDataType)3, 21074 },
	{ (Il2CppRGCTXDataType)3, 21075 },
	{ (Il2CppRGCTXDataType)2, 33226 },
	{ (Il2CppRGCTXDataType)3, 21076 },
	{ (Il2CppRGCTXDataType)2, 31641 },
	{ (Il2CppRGCTXDataType)3, 21077 },
	{ (Il2CppRGCTXDataType)3, 21078 },
	{ (Il2CppRGCTXDataType)3, 21079 },
	{ (Il2CppRGCTXDataType)3, 21080 },
	{ (Il2CppRGCTXDataType)3, 21081 },
	{ (Il2CppRGCTXDataType)3, 21082 },
	{ (Il2CppRGCTXDataType)3, 21083 },
	{ (Il2CppRGCTXDataType)3, 21084 },
	{ (Il2CppRGCTXDataType)3, 21085 },
};
extern const Il2CppCodeGenModule g_Unity_XR_ARFoundationCodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_ARFoundationCodeGenModule = 
{
	"Unity.XR.ARFoundation.dll",
	650,
	s_methodPointers,
	147,
	s_adjustorThunks,
	s_InvokerIndices,
	1,
	s_reversePInvokeIndices,
	9,
	s_rgctxIndices,
	116,
	s_rgctxValues,
	NULL,
};
