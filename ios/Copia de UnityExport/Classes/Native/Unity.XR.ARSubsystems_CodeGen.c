﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* XRCameraImage_OnAsyncConversionComplete_m8C475A6334B19FAC20896F0DFEFBDEAC6E5EE53B_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraConfiguration::get_width()
extern void XRCameraConfiguration_get_width_m998994AB66DE43B180D472B2B703B524C69497B4 (void);
// 0x00000002 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraConfiguration::get_height()
extern void XRCameraConfiguration_get_height_m13F96FED610F582C624331BD6394B88E4D055140 (void);
// 0x00000003 System.Nullable`1<System.Int32> UnityEngine.XR.ARSubsystems.XRCameraConfiguration::get_framerate()
extern void XRCameraConfiguration_get_framerate_m97E323885AEFFC7E8FCAC41CBE3F72A34400440B (void);
// 0x00000004 System.String UnityEngine.XR.ARSubsystems.XRCameraConfiguration::ToString()
extern void XRCameraConfiguration_ToString_m423574318A784468BA1055A28EC192FC34D4FA30 (void);
// 0x00000005 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraConfiguration::GetHashCode()
extern void XRCameraConfiguration_GetHashCode_m0826525E55CF9BD9E693F2D359F46A36E7FB08EC (void);
// 0x00000006 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraConfiguration::Equals(System.Object)
extern void XRCameraConfiguration_Equals_m04BF698F8D3E743BEF15107AFF74AAE1347CE82C (void);
// 0x00000007 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraConfiguration::Equals(UnityEngine.XR.ARSubsystems.XRCameraConfiguration)
extern void XRCameraConfiguration_Equals_m40024812F8E6090F4F18A6F0EBC055E9ED1B50BC (void);
// 0x00000008 System.Int64 UnityEngine.XR.ARSubsystems.XRCameraFrame::get_timestampNs()
extern void XRCameraFrame_get_timestampNs_m490E96B453EB3AA7416F98013B47B551C003268C (void);
// 0x00000009 System.Single UnityEngine.XR.ARSubsystems.XRCameraFrame::get_averageBrightness()
extern void XRCameraFrame_get_averageBrightness_mA23D9FB95046E89792F9F70750E000FF30E2959C (void);
// 0x0000000A System.Single UnityEngine.XR.ARSubsystems.XRCameraFrame::get_averageColorTemperature()
extern void XRCameraFrame_get_averageColorTemperature_m2AA0B1BE3B939E9221507D9AFB1CB28AE9FF0234 (void);
// 0x0000000B UnityEngine.Color UnityEngine.XR.ARSubsystems.XRCameraFrame::get_colorCorrection()
extern void XRCameraFrame_get_colorCorrection_m21EA821139C87463A369E9D308D3794A398D7A2C (void);
// 0x0000000C UnityEngine.Matrix4x4 UnityEngine.XR.ARSubsystems.XRCameraFrame::get_projectionMatrix()
extern void XRCameraFrame_get_projectionMatrix_m1AFF19568E4F29BDC16B943B34A8C988CA8382A1 (void);
// 0x0000000D UnityEngine.Matrix4x4 UnityEngine.XR.ARSubsystems.XRCameraFrame::get_displayMatrix()
extern void XRCameraFrame_get_displayMatrix_mE7C0D233A017D72EA36CC4879F49BC872D49B2A5 (void);
// 0x0000000E System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasTimestamp()
extern void XRCameraFrame_get_hasTimestamp_m9463BC747994BF7E6AEFDA339D88F5EDB02AA83B (void);
// 0x0000000F System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasAverageBrightness()
extern void XRCameraFrame_get_hasAverageBrightness_m86A574F4A162689E55C8B2C0F04B9528CFB738A5 (void);
// 0x00000010 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasAverageColorTemperature()
extern void XRCameraFrame_get_hasAverageColorTemperature_mB177764FC2D9592E4CA15F5BEFD710EF5510DB15 (void);
// 0x00000011 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasColorCorrection()
extern void XRCameraFrame_get_hasColorCorrection_m8FE25191C55FF1FA09EE7EB0D1AC1ABB1CD87597 (void);
// 0x00000012 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasProjectionMatrix()
extern void XRCameraFrame_get_hasProjectionMatrix_mC15F4AD61C07287736EA10A7C7BE5F66EE0258D1 (void);
// 0x00000013 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasDisplayMatrix()
extern void XRCameraFrame_get_hasDisplayMatrix_mD63FE4D87F9D23F40217F3DA69CEF5D01F45B8A9 (void);
// 0x00000014 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::Equals(UnityEngine.XR.ARSubsystems.XRCameraFrame)
extern void XRCameraFrame_Equals_mD750801D16ED7C39E75B31B663A97F300513FE3E (void);
// 0x00000015 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::Equals(System.Object)
extern void XRCameraFrame_Equals_mA3BFB0E406F27C0A40D0A8B79E02A17B8C06AA61 (void);
// 0x00000016 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraFrame::GetHashCode()
extern void XRCameraFrame_GetHashCode_mB60ADF713E8CA46B1629DA9EB361ED34127C358C (void);
// 0x00000017 System.String UnityEngine.XR.ARSubsystems.XRCameraFrame::ToString()
extern void XRCameraFrame_ToString_mE362631B14B4302E3AED73932E73C1D6AB5684FA (void);
// 0x00000018 UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRCameraImage::get_dimensions()
extern void XRCameraImage_get_dimensions_m396844BB3871C3CF359299C935A286F7EC9E07CC (void);
// 0x00000019 System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::set_dimensions(UnityEngine.Vector2Int)
extern void XRCameraImage_set_dimensions_m0EB351A10EB73AEB3BA66DEBB92CF6A482DF0FDD (void);
// 0x0000001A System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImage::get_width()
extern void XRCameraImage_get_width_m9540469A8631057CDBA693ACA5B39F11F7F8F85A (void);
// 0x0000001B System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImage::get_height()
extern void XRCameraImage_get_height_m3DFB851BE8723A821A3AB2A7F08F38E5C910A2BE (void);
// 0x0000001C System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImage::get_planeCount()
extern void XRCameraImage_get_planeCount_mE12B1F437995559D932808E1D5CA4838243802D6 (void);
// 0x0000001D System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::set_planeCount(System.Int32)
extern void XRCameraImage_set_planeCount_mBC04A59810816A0E2B8F173F17A17627CB2177C5 (void);
// 0x0000001E UnityEngine.XR.ARSubsystems.CameraImageFormat UnityEngine.XR.ARSubsystems.XRCameraImage::get_format()
extern void XRCameraImage_get_format_m363F742D003794B51229FEF58C6D24EF2E4174D9 (void);
// 0x0000001F System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::set_format(UnityEngine.XR.ARSubsystems.CameraImageFormat)
extern void XRCameraImage_set_format_m587247101073030F7122D794E2C7C4ED3208D3EC (void);
// 0x00000020 System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::set_timestamp(System.Double)
extern void XRCameraImage_set_timestamp_m817C23EC056A13E82BC33FD115C5DC4A40569C5F (void);
// 0x00000021 System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::.cctor()
extern void XRCameraImage__cctor_mCE4936C86EAC70BB3FBC1690BE39DED4D063FFFA (void);
// 0x00000022 System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::.ctor(UnityEngine.XR.ARSubsystems.XRCameraSubsystem,System.Int32,UnityEngine.Vector2Int,System.Int32,System.Double,UnityEngine.XR.ARSubsystems.CameraImageFormat)
extern void XRCameraImage__ctor_m13AB438F2246EEA666CEB1A6C7CE9C32B7F867C0 (void);
// 0x00000023 System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::OnAsyncConversionComplete(UnityEngine.XR.ARSubsystems.AsyncCameraImageConversionStatus,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,System.IntPtr,System.Int32,System.IntPtr)
extern void XRCameraImage_OnAsyncConversionComplete_m8C475A6334B19FAC20896F0DFEFBDEAC6E5EE53B (void);
// 0x00000024 System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::Dispose()
extern void XRCameraImage_Dispose_m01A1F3830038FD9ED9B35B6640A6CA57D383615A (void);
// 0x00000025 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImage::GetHashCode()
extern void XRCameraImage_GetHashCode_m27BEDB780E2AF59CA888ED4A8A0DC2B120BC756E (void);
// 0x00000026 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImage::Equals(System.Object)
extern void XRCameraImage_Equals_mE4A7D2162B33C795268692D0B745F9610B58D4F6 (void);
// 0x00000027 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImage::Equals(UnityEngine.XR.ARSubsystems.XRCameraImage)
extern void XRCameraImage_Equals_m9408E719265D28710EF012AA4E2E63AF320C7431 (void);
// 0x00000028 System.String UnityEngine.XR.ARSubsystems.XRCameraImage::ToString()
extern void XRCameraImage_ToString_m2E0D10A11B0FAE6A42A572D9C23320587B53C9E0 (void);
// 0x00000029 UnityEngine.RectInt UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::get_inputRect()
extern void XRCameraImageConversionParams_get_inputRect_m6F91AAA4D0844E9A9E1391A2FA73F99CEADAC833 (void);
// 0x0000002A UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::get_outputDimensions()
extern void XRCameraImageConversionParams_get_outputDimensions_mE4BFDDB1E03C9024E392ABD2A0EF90398F1188D2 (void);
// 0x0000002B UnityEngine.TextureFormat UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::get_outputFormat()
extern void XRCameraImageConversionParams_get_outputFormat_m7960F36DE5418DCFCBD0E780C483EDA42AB9405F (void);
// 0x0000002C UnityEngine.XR.ARSubsystems.CameraImageTransformation UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::get_transformation()
extern void XRCameraImageConversionParams_get_transformation_m2DA4133406E43882B1B52CDB3867F2E29A70C013 (void);
// 0x0000002D System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::GetHashCode()
extern void XRCameraImageConversionParams_GetHashCode_mE4B9237F3C69745C28C8D0D958B353D80F33A876 (void);
// 0x0000002E System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::Equals(UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams)
extern void XRCameraImageConversionParams_Equals_m7E25A42871B1C10084E685AC78F4A2E53D42652E (void);
// 0x0000002F System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::Equals(System.Object)
extern void XRCameraImageConversionParams_Equals_mAB0C49294DC3FB284CB470DDC298B696C4B28A06 (void);
// 0x00000030 System.String UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::ToString()
extern void XRCameraImageConversionParams_ToString_mCD58F5C2B04509ADEDDB374DA4492E22F3D70E97 (void);
// 0x00000031 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::Equals(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics)
extern void XRCameraIntrinsics_Equals_mEA26D3BF6A90B7DC9E2FF626BEA72EB6C098D5D2 (void);
// 0x00000032 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::Equals(System.Object)
extern void XRCameraIntrinsics_Equals_mB82F3C59386B2169809615700C79BF3DC519C45F (void);
// 0x00000033 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::GetHashCode()
extern void XRCameraIntrinsics_GetHashCode_mC38995C37469CCF20C3A155F7F033BD16EE36D98 (void);
// 0x00000034 System.String UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::ToString()
extern void XRCameraIntrinsics_ToString_m6A86B906A1EB79BDC75582D45311E75323054983 (void);
// 0x00000035 System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_zNear(System.Single)
extern void XRCameraParams_set_zNear_m0592AF26BE6AD3149462E71FB77B3838ACC3E9AB (void);
// 0x00000036 System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_zFar(System.Single)
extern void XRCameraParams_set_zFar_m85FE4877910BD92BFE4F308F73EA2BE35F3538BF (void);
// 0x00000037 System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_screenWidth(System.Single)
extern void XRCameraParams_set_screenWidth_mF256E58C15E4E73B3675323C93D1F38E06438919 (void);
// 0x00000038 System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_screenHeight(System.Single)
extern void XRCameraParams_set_screenHeight_mFFFBD063E1AA590D9B5055287965EF5A0A0B92A8 (void);
// 0x00000039 System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_screenOrientation(UnityEngine.ScreenOrientation)
extern void XRCameraParams_set_screenOrientation_m2F3BC04E753E945AD1AE69F2643301C5FDEA3117 (void);
// 0x0000003A System.Boolean UnityEngine.XR.ARSubsystems.XRCameraParams::Equals(UnityEngine.XR.ARSubsystems.XRCameraParams)
extern void XRCameraParams_Equals_m0E44D78DF6343B56235F52CC885DC36ABD69F586 (void);
// 0x0000003B System.Boolean UnityEngine.XR.ARSubsystems.XRCameraParams::Equals(System.Object)
extern void XRCameraParams_Equals_m2AF39FA9619DC2B0C470AAEEDF35A05E85BD0A1E (void);
// 0x0000003C System.Int32 UnityEngine.XR.ARSubsystems.XRCameraParams::GetHashCode()
extern void XRCameraParams_GetHashCode_m4E01EC262AD8F277BB0793D9782904D78F3B46D4 (void);
// 0x0000003D System.String UnityEngine.XR.ARSubsystems.XRCameraParams::ToString()
extern void XRCameraParams_ToString_mBE56D42E3C4F978B3A61B67C346E4CB58BD293CD (void);
// 0x0000003E System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_running()
extern void XRCameraSubsystem_get_running_m48C0F3AC90563B18389076B33BA7505096C42C2D (void);
// 0x0000003F System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::.ctor()
extern void XRCameraSubsystem__ctor_m4DB65C1288A29F049A4A362B9CD81B60970A73AE (void);
// 0x00000040 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::set_focusMode(UnityEngine.XR.ARSubsystems.CameraFocusMode)
extern void XRCameraSubsystem_set_focusMode_m4230E264ADA524A210DDE27D926F0DE66918C07A (void);
// 0x00000041 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::set_lightEstimationMode(UnityEngine.XR.ARSubsystems.LightEstimationMode)
extern void XRCameraSubsystem_set_lightEstimationMode_m0B04B0E3D3E8F73CA5FC6868DE86D6CFFF2263B5 (void);
// 0x00000042 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::Start()
extern void XRCameraSubsystem_Start_mA8F48C52695FA1A13DF12B8FF09E919A467D14DE (void);
// 0x00000043 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::Stop()
extern void XRCameraSubsystem_Stop_m8E0BEA2C6E44306AC10FA18EADC19ABBFCD70136 (void);
// 0x00000044 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::OnDestroy()
extern void XRCameraSubsystem_OnDestroy_m4B56BEECBCFD8BBB8B67D38DA084D1AE41EE29EB (void);
// 0x00000045 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTextureDescriptor> UnityEngine.XR.ARSubsystems.XRCameraSubsystem::GetTextureDescriptors(Unity.Collections.Allocator)
extern void XRCameraSubsystem_GetTextureDescriptors_mD7CFEDA2DDB138789A1E96CE71672F9C12FE4D21 (void);
// 0x00000046 System.String UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_shaderName()
extern void XRCameraSubsystem_get_shaderName_m1E02E577015568DA7CF93F4C55FB6094118BC014 (void);
// 0x00000047 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::TryGetIntrinsics(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics&)
extern void XRCameraSubsystem_TryGetIntrinsics_mA8696FD477463BD57E5EF328AA276439C556BD3F (void);
// 0x00000048 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARSubsystems.XRCameraSubsystem::GetConfigurations(Unity.Collections.Allocator)
extern void XRCameraSubsystem_GetConfigurations_m89E371017BA4612ECF7B4D472A0F71D290738BFA (void);
// 0x00000049 System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_currentConfiguration()
extern void XRCameraSubsystem_get_currentConfiguration_m95C8ED2F04B9AD10A1C2498C5D32DF03821A1DBF (void);
// 0x0000004A System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::set_currentConfiguration(System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration>)
extern void XRCameraSubsystem_set_currentConfiguration_mC6AAFA4CCF55F2293DB2F8E0A16A9239438ED70D (void);
// 0x0000004B System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_invertCulling()
extern void XRCameraSubsystem_get_invertCulling_mB2F2CD061D9065757EDFB6EAD361FB0B1782BAF4 (void);
// 0x0000004C UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider UnityEngine.XR.ARSubsystems.XRCameraSubsystem::CreateProvider()
// 0x0000004D System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::TryGetLatestFrame(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraFrame&)
extern void XRCameraSubsystem_TryGetLatestFrame_m7BDBD516A8A2832ECBD9334E4D7B0D38ED2D191F (void);
// 0x0000004E System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_permissionGranted()
extern void XRCameraSubsystem_get_permissionGranted_m4BE44093A85FE550B9DEFAA70E21CD84C3E99BFF (void);
// 0x0000004F System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::TryGetLatestImage(UnityEngine.XR.ARSubsystems.XRCameraImage&)
extern void XRCameraSubsystem_TryGetLatestImage_mA9EEBC8B2BC3D9F1DC25513B25BFECA07CE45ABF (void);
// 0x00000050 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::Register(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystem_Register_m471B8039B86BACC07C48A926544DF6C1415C1DA7 (void);
// 0x00000051 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::DisposeImage(System.Int32)
extern void XRCameraSubsystem_DisposeImage_m45A6B78356D402A5528F4F023FA33BD395F1E958 (void);
// 0x00000052 System.String UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::get_shaderName()
extern void IProvider_get_shaderName_mFB5F6E61A7112591AC17C13CB11552195D25ACC3 (void);
// 0x00000053 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::get_permissionGranted()
extern void IProvider_get_permissionGranted_m05E6ECEFF8068CD11404F2C8C658074EEA0C821E (void);
// 0x00000054 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::get_invertCulling()
extern void IProvider_get_invertCulling_mFAA294FC4DC9EFF1BFCA03BEBFA732F99D79906D (void);
// 0x00000055 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::Start()
extern void IProvider_Start_m65624A0EA48A4A41D9006C465CCF9CD9D5D642C6 (void);
// 0x00000056 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::Stop()
extern void IProvider_Stop_m047FB312180DAEE8120B88A6203D1A26E7025C1F (void);
// 0x00000057 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::Destroy()
extern void IProvider_Destroy_m40500F97E9831F72386920BF30B7E98E695CCD50 (void);
// 0x00000058 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::TryGetFrame(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraFrame&)
extern void IProvider_TryGetFrame_m51A0B9C95A46E751D76CD1F03B34C8787DDF3FFE (void);
// 0x00000059 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::TrySetFocusMode(UnityEngine.XR.ARSubsystems.CameraFocusMode)
extern void IProvider_TrySetFocusMode_mA8FFA007764B1FBC79A7A85329C9D43CE932F047 (void);
// 0x0000005A System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::TrySetLightEstimationMode(UnityEngine.XR.ARSubsystems.LightEstimationMode)
extern void IProvider_TrySetLightEstimationMode_mAA191807E6EDAA9848296BA3673E70642A25B645 (void);
// 0x0000005B System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::TryGetIntrinsics(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics&)
extern void IProvider_TryGetIntrinsics_m65D0B51511B516DB5B9D7E5AAA5B13CF62A22897 (void);
// 0x0000005C Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::GetConfigurations(UnityEngine.XR.ARSubsystems.XRCameraConfiguration,Unity.Collections.Allocator)
extern void IProvider_GetConfigurations_m485213213F2035799374E29F1F0E05DD90373A9F (void);
// 0x0000005D System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::get_currentConfiguration()
extern void IProvider_get_currentConfiguration_m7F1319DB358EE8CA535D2E91C2BD16E91829A9B4 (void);
// 0x0000005E System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::set_currentConfiguration(System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration>)
extern void IProvider_set_currentConfiguration_m8C98EB142400102A3EF7008A03F0BD35C82C06D3 (void);
// 0x0000005F Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTextureDescriptor> UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::GetTextureDescriptors(UnityEngine.XR.ARSubsystems.XRTextureDescriptor,Unity.Collections.Allocator)
extern void IProvider_GetTextureDescriptors_m79A1CA1945679FED96EAEAD126B23C6FDD8D4576 (void);
// 0x00000060 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::TryAcquireLatestImage(UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImageCinfo&)
extern void IProvider_TryAcquireLatestImage_m9D3691E0B7F486BFE505000AE6C48C8923196C3E (void);
// 0x00000061 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::DisposeImage(System.Int32)
extern void IProvider_DisposeImage_m7C58E8C74C44F8D8B43DFF4677064F51B92E7995 (void);
// 0x00000062 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider::.ctor()
extern void IProvider__ctor_m2709637AF8148063803D9E41C66EC75B4D713AC1 (void);
// 0x00000063 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem/OnImageRequestCompleteDelegate::.ctor(System.Object,System.IntPtr)
extern void OnImageRequestCompleteDelegate__ctor_mD9BE79ADAAEE0A44060E897728EA37196AF5ED47 (void);
// 0x00000064 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem/OnImageRequestCompleteDelegate::Invoke(UnityEngine.XR.ARSubsystems.AsyncCameraImageConversionStatus,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,System.IntPtr,System.Int32,System.IntPtr)
extern void OnImageRequestCompleteDelegate_Invoke_m835EE17741E50584491577BD5B9061FB12EB95F0 (void);
// 0x00000065 System.IAsyncResult UnityEngine.XR.ARSubsystems.XRCameraSubsystem/OnImageRequestCompleteDelegate::BeginInvoke(UnityEngine.XR.ARSubsystems.AsyncCameraImageConversionStatus,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,System.IntPtr,System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern void OnImageRequestCompleteDelegate_BeginInvoke_m3102FE931688E2734845C0CB9FED184CA15B48AC (void);
// 0x00000066 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem/OnImageRequestCompleteDelegate::EndInvoke(System.IAsyncResult)
extern void OnImageRequestCompleteDelegate_EndInvoke_m17DDA25840DD080989CEC9304CBD369D65AC82AD (void);
// 0x00000067 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImageCinfo::get_nativeHandle()
extern void CameraImageCinfo_get_nativeHandle_mD1CE0F2F44CFBA90188A3DD71970DC947D548419 (void);
// 0x00000068 UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImageCinfo::get_dimensions()
extern void CameraImageCinfo_get_dimensions_m55D78C58843F9F6AB11D634BBD0A4F17FB4CBFED (void);
// 0x00000069 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImageCinfo::get_planeCount()
extern void CameraImageCinfo_get_planeCount_m261902854A879D48561867655A57295A2B7E44D6 (void);
// 0x0000006A System.Double UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImageCinfo::get_timestamp()
extern void CameraImageCinfo_get_timestamp_m482C0B93963CCBB4953A1F375D5CF5862F758ED2 (void);
// 0x0000006B UnityEngine.XR.ARSubsystems.CameraImageFormat UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImageCinfo::get_format()
extern void CameraImageCinfo_get_format_mE92FD0C4F73255F6CDCCDC1CACBCAEF1E5E5605F (void);
// 0x0000006C System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImageCinfo::Equals(UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImageCinfo)
extern void CameraImageCinfo_Equals_mED7B9DECD14313D8D33586A50B22F2B3CFD81EC8 (void);
// 0x0000006D System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImageCinfo::Equals(System.Object)
extern void CameraImageCinfo_Equals_m5410D9B5B20D2CD87049EB0A89362E8777B02DF8 (void);
// 0x0000006E System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImageCinfo::GetHashCode()
extern void CameraImageCinfo_GetHashCode_m9E0E4A3A6091C9FF9D72E70AEB5820DD1A0959FE (void);
// 0x0000006F System.String UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImageCinfo::ToString()
extern void CameraImageCinfo_ToString_mD15A4DF77EC11F2BFC6E4A94AC781CDDF14A7BB7 (void);
// 0x00000070 System.IntPtr UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImagePlaneCinfo::get_dataPtr()
extern void CameraImagePlaneCinfo_get_dataPtr_m0C0144ED8D52B8456B579FA31C3159780E5161D5 (void);
// 0x00000071 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImagePlaneCinfo::get_dataLength()
extern void CameraImagePlaneCinfo_get_dataLength_m6CD246242017D808B54EC7E57C276CCA724AFA22 (void);
// 0x00000072 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImagePlaneCinfo::get_rowStride()
extern void CameraImagePlaneCinfo_get_rowStride_m5594C13CC656157844AF9045800D49868C1A65B3 (void);
// 0x00000073 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImagePlaneCinfo::get_pixelStride()
extern void CameraImagePlaneCinfo_get_pixelStride_m83C1A6151625091937C7DD696C10C39CBE4C084A (void);
// 0x00000074 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImagePlaneCinfo::Equals(UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImagePlaneCinfo)
extern void CameraImagePlaneCinfo_Equals_mA5382E8A5C97636DBB423D83FDBDC6E6C4AD2F6A (void);
// 0x00000075 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImagePlaneCinfo::Equals(System.Object)
extern void CameraImagePlaneCinfo_Equals_mFA864D2AFF1CCC3F62ED3B107F6B50A8D66466CE (void);
// 0x00000076 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImagePlaneCinfo::GetHashCode()
extern void CameraImagePlaneCinfo_GetHashCode_mBB8FCE75397290CF3F692965D989A11927B16162 (void);
// 0x00000077 System.String UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImagePlaneCinfo::ToString()
extern void CameraImagePlaneCinfo_ToString_mD7AEB170BA8CAA9EABF9245793A993DB435584BD (void);
// 0x00000078 System.String UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_id()
extern void XRCameraSubsystemCinfo_get_id_m8C63E6A41979D1A0201ADACA9F984088D06F488C (void);
// 0x00000079 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_id(System.String)
extern void XRCameraSubsystemCinfo_set_id_mEA5E0B21781D8AAF0FB30E9E506AA4D7C392E2A8 (void);
// 0x0000007A System.Type UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_implementationType()
extern void XRCameraSubsystemCinfo_get_implementationType_m6F9012A33C47D026F5070C2F64B444A93709B5C5 (void);
// 0x0000007B System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_implementationType(System.Type)
extern void XRCameraSubsystemCinfo_set_implementationType_mAEA2151AEC9F31C5726795200B63D4BA53F2721E (void);
// 0x0000007C System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsAverageBrightness()
extern void XRCameraSubsystemCinfo_get_supportsAverageBrightness_m1DDB2CC0BE14F5C77E7ECC2FF1BA5C712C08CCA5 (void);
// 0x0000007D System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsAverageBrightness(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsAverageBrightness_m0851BD298973A23FCE8D87B5B8AB389562D255FA (void);
// 0x0000007E System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsAverageColorTemperature()
extern void XRCameraSubsystemCinfo_get_supportsAverageColorTemperature_m16E2A9E1B564001C9A7D2F5EA20C0101DC97218D (void);
// 0x0000007F System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsAverageColorTemperature(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsAverageColorTemperature_m427080856A860B42B3FC21139B990F19BE0AD87E (void);
// 0x00000080 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsColorCorrection(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsColorCorrection_mFC3AED27787017D69ABA73FF60D1E20DDF5E674F (void);
// 0x00000081 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsDisplayMatrix()
extern void XRCameraSubsystemCinfo_get_supportsDisplayMatrix_mE79B1E6401467F3320D1AFF9678041FC2B5EC36C (void);
// 0x00000082 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsDisplayMatrix(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsDisplayMatrix_mB5BF43F49F4D64AA3DFE174574D386D99A96F92F (void);
// 0x00000083 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsProjectionMatrix()
extern void XRCameraSubsystemCinfo_get_supportsProjectionMatrix_m820DB18EBD58445C4976C05E25C3E2A8B04B388E (void);
// 0x00000084 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsProjectionMatrix(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsProjectionMatrix_m4CC64D264746A394D8186CCDD583CFCC637C8E66 (void);
// 0x00000085 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsTimestamp()
extern void XRCameraSubsystemCinfo_get_supportsTimestamp_mC9A551933743EC5171B9EAC41D5222067B56CAEA (void);
// 0x00000086 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsTimestamp(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsTimestamp_m901DA9F41D9CEE062F7A054738E5382E2A825F28 (void);
// 0x00000087 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsCameraConfigurations()
extern void XRCameraSubsystemCinfo_get_supportsCameraConfigurations_m125606F11D5A7521099E70525019DA50BBB4334E (void);
// 0x00000088 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsCameraConfigurations(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsCameraConfigurations_mCCA48E46B902EEABAA94FB7A2A668097E06D4906 (void);
// 0x00000089 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsCameraImage()
extern void XRCameraSubsystemCinfo_get_supportsCameraImage_m247ED6F4E54DC010F7506E353AD06E5D62EB4445 (void);
// 0x0000008A System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsCameraImage(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsCameraImage_m9B592584A2C27917CC80AB290F0A2600FD275951 (void);
// 0x0000008B System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::Equals(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystemCinfo_Equals_mC0F2554FBD6C4944FE9956C0843718D3D423D86F (void);
// 0x0000008C System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::Equals(System.Object)
extern void XRCameraSubsystemCinfo_Equals_mDC6FD25003FEA123FF4D83BAAED03854B0DF2B15 (void);
// 0x0000008D System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::GetHashCode()
extern void XRCameraSubsystemCinfo_GetHashCode_m41B5C3CD9B0D9101488D0D63E8557909BE239EE3 (void);
// 0x0000008E System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystemDescriptor__ctor_m218E5DC6846EF28CD00B48704E59A327949C3CAE (void);
// 0x0000008F System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsAverageBrightness(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsAverageBrightness_m90F7E1B3733B5D1975217E7FBEEFC2EC6DFDFBF6 (void);
// 0x00000090 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsAverageColorTemperature(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsAverageColorTemperature_m617CC63B166A095A8F873C73BAF39F86D579CD69 (void);
// 0x00000091 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsDisplayMatrix(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsDisplayMatrix_m67FC7675BE452E50E68679516708CEF60B36C061 (void);
// 0x00000092 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsProjectionMatrix(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsProjectionMatrix_mC5EBFF2790C6672050DD1781F0026AA82CDE0A3C (void);
// 0x00000093 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsTimestamp(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsTimestamp_m675F2A889C4A694BB5F72569DC697BDE0455B3E1 (void);
// 0x00000094 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsCameraConfigurations(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsCameraConfigurations_m2EFBEC5D6FC11ED5D546B7B75090B56A960F57F7 (void);
// 0x00000095 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsCameraImage(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsCameraImage_m84810E8929E44D5C62EEC38D5AAA7ED2F8434979 (void);
// 0x00000096 UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystemDescriptor_Create_mD1CD2F7DDCCF8702EEDE082BAFCAFAC8ECE3DEA3 (void);
// 0x00000097 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem::.ctor()
extern void XRDepthSubsystem__ctor_m24423F4A0EF54A1EDA98684496E5973E192C097B (void);
// 0x00000098 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem::Start()
extern void XRDepthSubsystem_Start_m68450EB9B783A9BB64B1B5B42083FDFA6B5709DD (void);
// 0x00000099 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem::OnDestroy()
extern void XRDepthSubsystem_OnDestroy_m121B6BB3B5074B632F3439D0888CA6AC728BBCB6 (void);
// 0x0000009A System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem::Stop()
extern void XRDepthSubsystem_Stop_m4E407A66FD6841B8AC70B906B5358F0C85EC85C9 (void);
// 0x0000009B UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud> UnityEngine.XR.ARSubsystems.XRDepthSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRDepthSubsystem_GetChanges_m7B42781E43AFE126FBE6DFE2C8A3AF76DC53EEB3 (void);
// 0x0000009C UnityEngine.XR.ARSubsystems.XRPointCloudData UnityEngine.XR.ARSubsystems.XRDepthSubsystem::GetPointCloudData(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator)
extern void XRDepthSubsystem_GetPointCloudData_m49BEF4047DEED6FC3E885AF893387ED347971BB8 (void);
// 0x0000009D UnityEngine.XR.ARSubsystems.XRDepthSubsystem/IDepthApi UnityEngine.XR.ARSubsystems.XRDepthSubsystem::GetInterface()
// 0x0000009E System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem/IDepthApi::Start()
extern void IDepthApi_Start_m4FFE14EF1808D84F83A805786BB5D17F0F66C779 (void);
// 0x0000009F System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem/IDepthApi::Stop()
extern void IDepthApi_Stop_m9D7185DBB1129C64160889FE3D569FDE887F2E44 (void);
// 0x000000A0 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem/IDepthApi::Destroy()
extern void IDepthApi_Destroy_m04DAAED019DFD454F222CE747809E1D95765D662 (void);
// 0x000000A1 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud> UnityEngine.XR.ARSubsystems.XRDepthSubsystem/IDepthApi::GetChanges(UnityEngine.XR.ARSubsystems.XRPointCloud,Unity.Collections.Allocator)
extern void IDepthApi_GetChanges_m5B950E3E0BE9BEDF1172AB25C46F8F7BADC40F18 (void);
// 0x000000A2 UnityEngine.XR.ARSubsystems.XRPointCloudData UnityEngine.XR.ARSubsystems.XRDepthSubsystem/IDepthApi::GetPointCloudData(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator)
extern void IDepthApi_GetPointCloudData_m92DD9FE13BBC60B6686809061B6809BC6B8622CF (void);
// 0x000000A3 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem/IDepthApi::.ctor()
extern void IDepthApi__ctor_m4B44FB5141C363FF42F5703404626927B89C222A (void);
// 0x000000A4 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Cinfo)
extern void XRDepthSubsystemDescriptor__ctor_mA74AF85046BBA65B77FA333055F82C2E3F6AD05D (void);
// 0x000000A5 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::set_supportsFeaturePoints(System.Boolean)
extern void XRDepthSubsystemDescriptor_set_supportsFeaturePoints_m7974B23484377F9C2698B1981BCE830C7BEC6C73 (void);
// 0x000000A6 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::set_supportsUniqueIds(System.Boolean)
extern void XRDepthSubsystemDescriptor_set_supportsUniqueIds_mF89B9E95F36EB311A0C512A7C20BD7CCADB1489B (void);
// 0x000000A7 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::set_supportsConfidence(System.Boolean)
extern void XRDepthSubsystemDescriptor_set_supportsConfidence_mBC66AFF12E25475139457FA6DEB1093A57065068 (void);
// 0x000000A8 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::RegisterDescriptor(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Cinfo)
extern void XRDepthSubsystemDescriptor_RegisterDescriptor_m9F40B303586BE45F7AACB8B0AA408D242B34F4EC (void);
// 0x000000A9 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Cinfo::get_supportsFeaturePoints()
extern void Cinfo_get_supportsFeaturePoints_m167B1DCD4173C6CB2AAC5E8FA35759AE4D4BF385 (void);
// 0x000000AA System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Cinfo::set_supportsFeaturePoints(System.Boolean)
extern void Cinfo_set_supportsFeaturePoints_mB3633125ACFBA430C6EC66F3FF8E5BFEC72EC360 (void);
// 0x000000AB System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Cinfo::get_supportsConfidence()
extern void Cinfo_get_supportsConfidence_m2AB397C1B754341CC18399C796866FDC8FF597F0 (void);
// 0x000000AC System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Cinfo::set_supportsConfidence(System.Boolean)
extern void Cinfo_set_supportsConfidence_mD7DE3DC81C6783C66AAE15A10301DE202520605C (void);
// 0x000000AD System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Cinfo::get_supportsUniqueIds()
extern void Cinfo_get_supportsUniqueIds_m72987F3CF2CB4081D548EF34F13876D607CBD807 (void);
// 0x000000AE System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Cinfo::set_supportsUniqueIds(System.Boolean)
extern void Cinfo_set_supportsUniqueIds_m416FF5EC15306E37DC3436BBB02B4998D64B62C4 (void);
// 0x000000AF UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Capabilities UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Cinfo::get_capabilities()
extern void Cinfo_get_capabilities_m6B6CF2B89609939E9E7313A3AAD56C543E4445AB (void);
// 0x000000B0 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Cinfo::set_capabilities(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Capabilities)
extern void Cinfo_set_capabilities_m2FFCFE5025136EFB71DDF302E4B81E9287D9A39E (void);
// 0x000000B1 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Cinfo)
extern void Cinfo_Equals_mDAB1544CED67FAD91D9F273A32EBD01B18EC87ED (void);
// 0x000000B2 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Cinfo::Equals(System.Object)
extern void Cinfo_Equals_mF847824268A42C4CC910934F8454B95DD30C4C1E (void);
// 0x000000B3 System.Int32 UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor/Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m114731847A78B883C2EC5BC32DE8C412D4684A58 (void);
// 0x000000B4 UnityEngine.XR.ARSubsystems.XRPointCloud UnityEngine.XR.ARSubsystems.XRPointCloud::GetDefault()
extern void XRPointCloud_GetDefault_m217CAB3C5FEF4FF479DFDE0D019B806EB2BB2FE8 (void);
// 0x000000B5 System.Void UnityEngine.XR.ARSubsystems.XRPointCloud::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr)
extern void XRPointCloud__ctor_m049E9ED3F776B9F25BD217D0B2D714464A5F6C9F (void);
// 0x000000B6 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRPointCloud::get_trackableId()
extern void XRPointCloud_get_trackableId_mA394197EAD026665FC02A1118CBBB46FF6873EF1 (void);
// 0x000000B7 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRPointCloud::get_pose()
extern void XRPointCloud_get_pose_m09C2DF1AD7F1220B547BD2EBCCA6E35F85A87EB0 (void);
// 0x000000B8 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRPointCloud::get_trackingState()
extern void XRPointCloud_get_trackingState_m0CE633649849B59E4AEA7875F62F0B34CA61FC96 (void);
// 0x000000B9 System.Int32 UnityEngine.XR.ARSubsystems.XRPointCloud::GetHashCode()
extern void XRPointCloud_GetHashCode_mC2934047C0D733F372E51C52E2837DAE9E13259C (void);
// 0x000000BA System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloud::Equals(UnityEngine.XR.ARSubsystems.XRPointCloud)
extern void XRPointCloud_Equals_mC56FA4F7B07E704C529E144B073920A79E599CC1 (void);
// 0x000000BB System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloud::Equals(System.Object)
extern void XRPointCloud_Equals_m66CC3D8FEDF0226F8D4F0B6574449E334D849F57 (void);
// 0x000000BC Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARSubsystems.XRPointCloudData::get_positions()
extern void XRPointCloudData_get_positions_m2BDA572054D639DB35E9FDA3D15AEF3B7B39D40C (void);
// 0x000000BD System.Void UnityEngine.XR.ARSubsystems.XRPointCloudData::set_positions(Unity.Collections.NativeArray`1<UnityEngine.Vector3>)
extern void XRPointCloudData_set_positions_mC99C23E8AE61A1A3333C1A2F7E0F9DBD6C9F771C (void);
// 0x000000BE Unity.Collections.NativeArray`1<System.Single> UnityEngine.XR.ARSubsystems.XRPointCloudData::get_confidenceValues()
extern void XRPointCloudData_get_confidenceValues_mE672D23FB62CF42CA475CF82A678DA16729D618C (void);
// 0x000000BF Unity.Collections.NativeArray`1<System.UInt64> UnityEngine.XR.ARSubsystems.XRPointCloudData::get_identifiers()
extern void XRPointCloudData_get_identifiers_m6F0A88EEB7DD58C82662346098A4E20CA111C479 (void);
// 0x000000C0 System.Void UnityEngine.XR.ARSubsystems.XRPointCloudData::set_identifiers(Unity.Collections.NativeArray`1<System.UInt64>)
extern void XRPointCloudData_set_identifiers_m3CDA83EC60EC5AAB982B3C5E0F9AC9E94D41992B (void);
// 0x000000C1 System.Void UnityEngine.XR.ARSubsystems.XRPointCloudData::Dispose()
extern void XRPointCloudData_Dispose_mB2C769355B6385CE3F8F47E720087F7B7C726259 (void);
// 0x000000C2 System.Int32 UnityEngine.XR.ARSubsystems.XRPointCloudData::GetHashCode()
extern void XRPointCloudData_GetHashCode_m0ACAD17C55503EE73995A9DF2A1110FBEF0145DC (void);
// 0x000000C3 System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloudData::Equals(System.Object)
extern void XRPointCloudData_Equals_mBDA7D40FB197B84AF2E8EB4C5CF6015D09E74357 (void);
// 0x000000C4 System.String UnityEngine.XR.ARSubsystems.XRPointCloudData::ToString()
extern void XRPointCloudData_ToString_m91E31F94CB96601D2BA320BE4B993728D1A8DE20 (void);
// 0x000000C5 System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloudData::Equals(UnityEngine.XR.ARSubsystems.XRPointCloudData)
extern void XRPointCloudData_Equals_mBE92CAA314FFE99803718F2EABEA3A1B7AE8A99C (void);
// 0x000000C6 UnityEngine.XR.ARSubsystems.XREnvironmentProbe UnityEngine.XR.ARSubsystems.XREnvironmentProbe::GetDefault()
extern void XREnvironmentProbe_GetDefault_mE837280E7ECD29A7313DE019DD7A4FFEA0FECE3B (void);
// 0x000000C7 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_trackableId()
extern void XREnvironmentProbe_get_trackableId_m2F7F8DCE954C099E60807742B6A4B27DA2F30085 (void);
// 0x000000C8 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_trackableId(UnityEngine.XR.ARSubsystems.TrackableId)
extern void XREnvironmentProbe_set_trackableId_m8E02AF983995D8D544C83C7F170989AFABC13AA2 (void);
// 0x000000C9 UnityEngine.Vector3 UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_scale()
extern void XREnvironmentProbe_get_scale_m2346CDBC2FEF9CC85761BF11829E10D2992E0004 (void);
// 0x000000CA UnityEngine.Pose UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_pose()
extern void XREnvironmentProbe_get_pose_m2CF6BF7E554B1225E99947B620D2C029499E7996 (void);
// 0x000000CB System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_pose(UnityEngine.Pose)
extern void XREnvironmentProbe_set_pose_m19121B0DDCFC795C1541A378F506B899BD2F8D0E (void);
// 0x000000CC UnityEngine.Vector3 UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_size()
extern void XREnvironmentProbe_get_size_mCFA1F9B8C8BE0138585D2BE4C2059E0153133C11 (void);
// 0x000000CD UnityEngine.XR.ARSubsystems.XRTextureDescriptor UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_textureDescriptor()
extern void XREnvironmentProbe_get_textureDescriptor_mA926C0701B8D774E5C0A1F51DC54546CB71ECFFA (void);
// 0x000000CE UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_trackingState()
extern void XREnvironmentProbe_get_trackingState_m48BAAE58BF028382D00D5F49BA1F6023E53B0AF9 (void);
// 0x000000CF System.IntPtr UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_nativePtr()
extern void XREnvironmentProbe_get_nativePtr_mC9CB253F77A64FCD5D1ADC64590E91A793DC8D66 (void);
// 0x000000D0 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbe::Equals(UnityEngine.XR.ARSubsystems.XREnvironmentProbe)
extern void XREnvironmentProbe_Equals_mABEF3AB481CB2191DE4C790E3A5A245DE1D347D0 (void);
// 0x000000D1 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbe::Equals(System.Object)
extern void XREnvironmentProbe_Equals_m8DFEE5B51820BCC164FDFA7F5F4996074A9C5170 (void);
// 0x000000D2 System.Int32 UnityEngine.XR.ARSubsystems.XREnvironmentProbe::GetHashCode()
extern void XREnvironmentProbe_GetHashCode_m638CB5F2CF52A8ABA8778B6B5EB7F13E57CD7B1B (void);
// 0x000000D3 System.String UnityEngine.XR.ARSubsystems.XREnvironmentProbe::ToString()
extern void XREnvironmentProbe_ToString_m8B856D8579587102C1F500A2F2180361CD7770D2 (void);
// 0x000000D4 System.String UnityEngine.XR.ARSubsystems.XREnvironmentProbe::ToString(System.String)
extern void XREnvironmentProbe_ToString_m20D40F2265F40337C75C536779CC935885222B19 (void);
// 0x000000D5 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::.ctor()
extern void XREnvironmentProbeSubsystem__ctor_m7A3AE7794DA58FE53C8EE9F47F8B84F3D5DF47B6 (void);
// 0x000000D6 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::get_automaticPlacement()
extern void XREnvironmentProbeSubsystem_get_automaticPlacement_m4D1AC97BF886DFA4851A85797CE2FB34246BAA43 (void);
// 0x000000D7 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::set_automaticPlacement(System.Boolean)
extern void XREnvironmentProbeSubsystem_set_automaticPlacement_m1B62556B219E960E694D903D797ED5D9E1E998C4 (void);
// 0x000000D8 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XREnvironmentProbe> UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XREnvironmentProbeSubsystem_GetChanges_mA100F4697822F10AAAD8506683629996239BD7C5 (void);
// 0x000000D9 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::Start()
extern void XREnvironmentProbeSubsystem_Start_m697A7A5789BFE9B17A0668268E97D0245ED12306 (void);
// 0x000000DA System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::Stop()
extern void XREnvironmentProbeSubsystem_Stop_m45CA3860BD9B913D3CA2FE9905CF0232FBD2B95F (void);
// 0x000000DB System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::OnDestroy()
extern void XREnvironmentProbeSubsystem_OnDestroy_m39FD526B93D9252DED22732011B818F0DFDC0E24 (void);
// 0x000000DC System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::TryAddEnvironmentProbe(UnityEngine.Pose,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.XR.ARSubsystems.XREnvironmentProbe&)
extern void XREnvironmentProbeSubsystem_TryAddEnvironmentProbe_mC5653C998195D72D7A048BE3F4CC07BE81B6438B (void);
// 0x000000DD System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::RemoveEnvironmentProbe(UnityEngine.XR.ARSubsystems.TrackableId)
extern void XREnvironmentProbeSubsystem_RemoveEnvironmentProbe_m045B4A7307B5CBAFFEF3DBAFC78DF05F551B6801 (void);
// 0x000000DE UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/IProvider UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::CreateProvider()
// 0x000000DF System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::Register(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystem_Register_mF52AA36EB4EAA59C932C43E45DA567A3EA6D55FD (void);
// 0x000000E0 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/IProvider::Start()
extern void IProvider_Start_mC5843EBBA0BF44B4631E433F6BB73D4013EEE473 (void);
// 0x000000E1 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/IProvider::Stop()
extern void IProvider_Stop_m6CCB471415A9BAF0B24863D71F952EB76C707B29 (void);
// 0x000000E2 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/IProvider::Destroy()
extern void IProvider_Destroy_m391A202D94ACD3C96BC9483C5213FFB3D40F9B84 (void);
// 0x000000E3 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/IProvider::SetAutomaticPlacement(System.Boolean)
extern void IProvider_SetAutomaticPlacement_mE563FEC72D1C1CCE511C20F92AB2804F06BA8AB7 (void);
// 0x000000E4 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/IProvider::TryAddEnvironmentProbe(UnityEngine.Pose,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.XR.ARSubsystems.XREnvironmentProbe&)
extern void IProvider_TryAddEnvironmentProbe_mF5F8B5978164B3CD13F5B0542DA04EE650FEB1F2 (void);
// 0x000000E5 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/IProvider::RemoveEnvironmentProbe(UnityEngine.XR.ARSubsystems.TrackableId)
extern void IProvider_RemoveEnvironmentProbe_mA00B3FCBDE45170B3BC9A12E835D8B11299BC1C0 (void);
// 0x000000E6 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XREnvironmentProbe> UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/IProvider::GetChanges(UnityEngine.XR.ARSubsystems.XREnvironmentProbe,Unity.Collections.Allocator)
// 0x000000E7 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/IProvider::.ctor()
extern void IProvider__ctor_m5A111BAA2E41E225EEF73921AB5C4A279E7AC337 (void);
// 0x000000E8 System.String UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_id()
extern void XREnvironmentProbeSubsystemCinfo_get_id_mADBD2988DA174EE595955008050CB74CB19C4882 (void);
// 0x000000E9 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_id(System.String)
extern void XREnvironmentProbeSubsystemCinfo_set_id_m65F71E8D97413215944F75C52F6F9F2088644E24 (void);
// 0x000000EA System.Type UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_implementationType()
extern void XREnvironmentProbeSubsystemCinfo_get_implementationType_m2AAB6F75B1588A46DC09034244ED3C4CEF0BDD22 (void);
// 0x000000EB System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_implementationType(System.Type)
extern void XREnvironmentProbeSubsystemCinfo_set_implementationType_m5CDE58834E022AEB4B9E6FD826D2A6140D3D1B3E (void);
// 0x000000EC System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsManualPlacement()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsManualPlacement_m38F2FBCF91D735F7ACD339C2C6FD013B62C0DC0A (void);
// 0x000000ED System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsManualPlacement(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsManualPlacement_mF3AA42AAE10CC81DF831404F415BD34694B08C59 (void);
// 0x000000EE System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsRemovalOfManual()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfManual_m1F2CFC423F2D37975488C822E76CD175A589E8AE (void);
// 0x000000EF System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsRemovalOfManual(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfManual_m200BBBC11A1580CAA151ED498A8B24E27BAB646F (void);
// 0x000000F0 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsAutomaticPlacement()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsAutomaticPlacement_mFA0D6FB52DF9C8ACCF6DC3B9C8A11FDB37877C67 (void);
// 0x000000F1 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsAutomaticPlacement(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsAutomaticPlacement_mBC81F9BC67A3FF73D0EB679BEDDFE1D3DA918582 (void);
// 0x000000F2 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsRemovalOfAutomatic()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfAutomatic_mF9F46814201602562ACCF4E7AD63E20B0B7C1645 (void);
// 0x000000F3 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsRemovalOfAutomatic(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfAutomatic_mC43F8E59BF70D73AC0EB8EE5A9B2D6F92966B3B3 (void);
// 0x000000F4 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsEnvironmentTexture()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsEnvironmentTexture_m444E17E86B43838E6BC1279C1793F40E21E0C210 (void);
// 0x000000F5 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsEnvironmentTexture(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsEnvironmentTexture_m9C630C891056E5E1187AA2433DDC8D7E0F3FF662 (void);
// 0x000000F6 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::Equals(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystemCinfo_Equals_m87EE67B01CB7C502E31CBBB818CA4C6D12DAB809 (void);
// 0x000000F7 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::Equals(System.Object)
extern void XREnvironmentProbeSubsystemCinfo_Equals_m79E504F27913015A7C97F1E64E16A02DC1C4D73E (void);
// 0x000000F8 System.Int32 UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::GetHashCode()
extern void XREnvironmentProbeSubsystemCinfo_GetHashCode_mFCD281E4EF9FD74889EF9754EF53341C79988F1B (void);
// 0x000000F9 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystemDescriptor__ctor_m91F1A02FF56AC51ABA801B94687E393C9AB82F74 (void);
// 0x000000FA System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsManualPlacement()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsManualPlacement_mE198AF9A486EED050CB81241F91BE6F7DA074632 (void);
// 0x000000FB System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsManualPlacement(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsManualPlacement_mFB3D77F497F4DF6722A1B7C66DE6F4D9505763A7 (void);
// 0x000000FC System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsRemovalOfManual()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsRemovalOfManual_m1FCEBFF75F2F521CC101940F5F8A1022B45B64E1 (void);
// 0x000000FD System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsRemovalOfManual(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsRemovalOfManual_m4E909B728829BACB3845873C9D0A2F41ED14D531 (void);
// 0x000000FE System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsAutomaticPlacement()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsAutomaticPlacement_mA944EEE25188EC82048232D81023516D1298080A (void);
// 0x000000FF System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsAutomaticPlacement(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsAutomaticPlacement_m2374B2B4F3CF3959F87F0DD13C8EE4895BC9AE25 (void);
// 0x00000100 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsRemovalOfAutomatic()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsRemovalOfAutomatic_m25ACC17AE99D2D056B11C7BAD34981D0A7444550 (void);
// 0x00000101 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsRemovalOfAutomatic(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsRemovalOfAutomatic_mECFC5E6081D92D77590B981F055E86AE6557873B (void);
// 0x00000102 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsEnvironmentTexture(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsEnvironmentTexture_m31201D249DB3E60DA61A739A66D19BEFB89B4B7B (void);
// 0x00000103 UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystemDescriptor_Create_mB7EBDC47BFF343F6B7B621B5D5E5EE9600EF767E (void);
// 0x00000104 UnityEngine.XR.ARSubsystems.XRFace UnityEngine.XR.ARSubsystems.XRFace::GetDefault()
extern void XRFace_GetDefault_m769DBA5C20BEF8E9F2C7B197AFBAA92527946E3C (void);
// 0x00000105 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRFace::get_trackableId()
extern void XRFace_get_trackableId_mC7AA3B622C6B16A9E8B5A3BEA524C7ED54A6188D (void);
// 0x00000106 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRFace::get_pose()
extern void XRFace_get_pose_m3792AF11CBB24361529B7291ED46B9DD2970AC54 (void);
// 0x00000107 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRFace::get_trackingState()
extern void XRFace_get_trackingState_m29CA0D89354ACC65EF8A18C09201BCBB3F732E3B (void);
// 0x00000108 System.IntPtr UnityEngine.XR.ARSubsystems.XRFace::get_nativePtr()
extern void XRFace_get_nativePtr_m1EDCB59CD67423A2951DBA0DD0C98AB848183F06 (void);
// 0x00000109 System.Boolean UnityEngine.XR.ARSubsystems.XRFace::Equals(System.Object)
extern void XRFace_Equals_mB68B812AD74B02F7B8B82DE62EAE30146238DD3A (void);
// 0x0000010A System.Int32 UnityEngine.XR.ARSubsystems.XRFace::GetHashCode()
extern void XRFace_GetHashCode_mB3AC95DBCA3308827747473BF3A6044FCD2DD7B0 (void);
// 0x0000010B System.Boolean UnityEngine.XR.ARSubsystems.XRFace::Equals(UnityEngine.XR.ARSubsystems.XRFace)
extern void XRFace_Equals_m1FF3F979A7F289C6CA77DD3290F4A50CAF889ED6 (void);
// 0x0000010C Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARSubsystems.XRFaceMesh::get_vertices()
extern void XRFaceMesh_get_vertices_mD6CE3C180851CA10DE10A7B6C7BF0817E73609E9 (void);
// 0x0000010D Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARSubsystems.XRFaceMesh::get_normals()
extern void XRFaceMesh_get_normals_m8CF3D0395943001F6A681154DF7A75FA045CE6AE (void);
// 0x0000010E Unity.Collections.NativeArray`1<System.Int32> UnityEngine.XR.ARSubsystems.XRFaceMesh::get_indices()
extern void XRFaceMesh_get_indices_m7EA9FB6B6CE3484262F74546455CB08BA5B5B00D (void);
// 0x0000010F Unity.Collections.NativeArray`1<UnityEngine.Vector2> UnityEngine.XR.ARSubsystems.XRFaceMesh::get_uvs()
extern void XRFaceMesh_get_uvs_mECB939F9E262D22AED47311D6985116FC0CEEA9B (void);
// 0x00000110 System.Void UnityEngine.XR.ARSubsystems.XRFaceMesh::Dispose()
extern void XRFaceMesh_Dispose_m3E7A416718B532DFD7D100D5D0F1F3A9AED96F7E (void);
// 0x00000111 System.Int32 UnityEngine.XR.ARSubsystems.XRFaceMesh::GetHashCode()
extern void XRFaceMesh_GetHashCode_mBB4E68260D980EAFD29333F2B0EE9B10FAA8040E (void);
// 0x00000112 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceMesh::Equals(System.Object)
extern void XRFaceMesh_Equals_m466E0A0D30D307B956CB677D1103AD4F4C86E3BC (void);
// 0x00000113 System.String UnityEngine.XR.ARSubsystems.XRFaceMesh::ToString()
extern void XRFaceMesh_ToString_mC0D08232897F11B687916F6B1975927411A783B3 (void);
// 0x00000114 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceMesh::Equals(UnityEngine.XR.ARSubsystems.XRFaceMesh)
extern void XRFaceMesh_Equals_m7F90AA84BD56C74C8C6472CE390C706E8AB8120D (void);
// 0x00000115 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::.ctor()
extern void XRFaceSubsystem__ctor_m69A0FE81F7D83567E0B1F70FE0CC9B37AE5BB7EB (void);
// 0x00000116 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::Start()
extern void XRFaceSubsystem_Start_mFD2DC7D5B1E73266D3A752056B6E7771DCB16AA6 (void);
// 0x00000117 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::OnDestroy()
extern void XRFaceSubsystem_OnDestroy_m9D447A629CA2CFCDBD38F8B38C1EC42C747BA029 (void);
// 0x00000118 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::Stop()
extern void XRFaceSubsystem_Stop_m3761285C8E1E72D253F42E390921B3395515626B (void);
// 0x00000119 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceSubsystem::get_supported()
extern void XRFaceSubsystem_get_supported_mF71AD1931835B24E79D1F762CDC9A5B12B5753CA (void);
// 0x0000011A UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace> UnityEngine.XR.ARSubsystems.XRFaceSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRFaceSubsystem_GetChanges_m7A0FF036C2DF93EBD2B39EB65AA87335601C9AA7 (void);
// 0x0000011B System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::GetFaceMesh(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,UnityEngine.XR.ARSubsystems.XRFaceMesh&)
extern void XRFaceSubsystem_GetFaceMesh_m4E1417F6B00F84DB66EFECFE29A8FA2D3ECBBD12 (void);
// 0x0000011C UnityEngine.XR.ARSubsystems.XRFaceSubsystem/IProvider UnityEngine.XR.ARSubsystems.XRFaceSubsystem::CreateProvider()
// 0x0000011D System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem/IProvider::Start()
extern void IProvider_Start_mCC319513AA6093EC38BEF32EE228DE2A0ECF86D8 (void);
// 0x0000011E System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem/IProvider::Stop()
extern void IProvider_Stop_m5773A5F49083C4FEE1A84F701D8D20C33A1F8786 (void);
// 0x0000011F System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem/IProvider::Destroy()
extern void IProvider_Destroy_m510EDD2FBD76DB2082AC958420287E544742299A (void);
// 0x00000120 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceSubsystem/IProvider::get_supported()
extern void IProvider_get_supported_m488CA2BFBFFF8C9C6E8D1AAD4C0CA7324AD79BB4 (void);
// 0x00000121 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem/IProvider::GetFaceMesh(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,UnityEngine.XR.ARSubsystems.XRFaceMesh&)
extern void IProvider_GetFaceMesh_mA90B7C66A3FC5259F31005616C40430E75E32F88 (void);
// 0x00000122 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace> UnityEngine.XR.ARSubsystems.XRFaceSubsystem/IProvider::GetChanges(UnityEngine.XR.ARSubsystems.XRFace,Unity.Collections.Allocator)
extern void IProvider_GetChanges_mB14296C0CDA96C92523BA2C7DC1927A0E8B20C8C (void);
// 0x00000123 System.Guid UnityEngine.XR.ARSubsystems.GuidUtil::Compose(System.UInt64,System.UInt64)
extern void GuidUtil_Compose_m23689A8CCFCDF3904D23BE11760E58DC662E35C1 (void);
// 0x00000124 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::.ctor()
extern void XRImageTrackingSubsystem__ctor_mEE7E27E4FDC18721F1D9CDDAAC8FFAACC782D4CF (void);
// 0x00000125 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::Start()
extern void XRImageTrackingSubsystem_Start_m91F8C046524DE309DC7C3A7886D570BE35C177D1 (void);
// 0x00000126 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::Stop()
extern void XRImageTrackingSubsystem_Stop_m23F39DDCB8A8DEEC9D2EBCCC3676D248F263BDEF (void);
// 0x00000127 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::OnDestroy()
extern void XRImageTrackingSubsystem_OnDestroy_mC6226FC0BE61C75F945FE96D36C4F983A29D2477 (void);
// 0x00000128 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::set_imageLibrary(UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary)
extern void XRImageTrackingSubsystem_set_imageLibrary_mF42A5F9DE0F684462B4113BAC91A2FC86F89E069 (void);
// 0x00000129 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage> UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRImageTrackingSubsystem_GetChanges_m84DB25AC8DB44AE84050A755C823960BABC2CFA0 (void);
// 0x0000012A System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::set_maxNumberOfMovingImages(System.Int32)
extern void XRImageTrackingSubsystem_set_maxNumberOfMovingImages_m59B8A966406E06A35D55565FE6628158006A32BB (void);
// 0x0000012B UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/IProvider UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::CreateProvider()
// 0x0000012C System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/IProvider::Destroy()
extern void IProvider_Destroy_mA90D2D8E662D193A6E53790B1C6C6F2C036EF977 (void);
// 0x0000012D UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage> UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/IProvider::GetChanges(UnityEngine.XR.ARSubsystems.XRTrackedImage,Unity.Collections.Allocator)
extern void IProvider_GetChanges_m8A5C85F9184615748807A531CBB5C49CC8FE3648 (void);
// 0x0000012E System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/IProvider::set_imageLibrary(UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary)
extern void IProvider_set_imageLibrary_mDF0EA87D8EDDE21B17DEAEE4E914200802E35DF2 (void);
// 0x0000012F System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/IProvider::set_maxNumberOfMovingImages(System.Int32)
extern void IProvider_set_maxNumberOfMovingImages_mDFC808999A3DFCFA90D6EFFF36A39451CBE371D3 (void);
// 0x00000130 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/IProvider::.ctor()
extern void IProvider__ctor_mE3F278B6BD96AAE5265BB88C2F2F7B0AA5C1D210 (void);
// 0x00000131 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::get_supportsMovingImages()
extern void XRImageTrackingSubsystemDescriptor_get_supportsMovingImages_mB0B37BABA6FEECB44860DFFD2A388BBFF48F4F20 (void);
// 0x00000132 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::set_supportsMovingImages(System.Boolean)
extern void XRImageTrackingSubsystemDescriptor_set_supportsMovingImages_mB13C74324DB6B9D7E3A69826726BFEBA2E403D69 (void);
// 0x00000133 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor/Cinfo)
extern void XRImageTrackingSubsystemDescriptor_Create_m1049DA7C21F27833846D6C6E699DD2DA964522A9 (void);
// 0x00000134 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor/Cinfo)
extern void XRImageTrackingSubsystemDescriptor__ctor_mCD5CADD6B43C92838FA1A87926CC23D53A9715C4 (void);
// 0x00000135 System.String UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor/Cinfo::get_id()
extern void Cinfo_get_id_m146AEBFC1F34906EF7CB853DB90B28AAA8449D98 (void);
// 0x00000136 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor/Cinfo::set_id(System.String)
extern void Cinfo_set_id_mFA87FA52172846CBD4587F2E207D65A097B842E6 (void);
// 0x00000137 System.Type UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor/Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_m3715508999808DEC6A812A1D228A2472ECEA48C4 (void);
// 0x00000138 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor/Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_mBBC866C6F4207A69F7351857F66D683E0EAF8FAF (void);
// 0x00000139 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor/Cinfo::get_supportsMovingImages()
extern void Cinfo_get_supportsMovingImages_mAB2286D926C059743ACE6E6DABCFAAB7939AC80F (void);
// 0x0000013A System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor/Cinfo::set_supportsMovingImages(System.Boolean)
extern void Cinfo_set_supportsMovingImages_m2898F758BCCE6DDC222E828D855CEFC89C58EF2C (void);
// 0x0000013B System.Int32 UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor/Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m30FEF663E95BCA50174085CAE5EC3E6B207CF094 (void);
// 0x0000013C System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor/Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor/Cinfo)
extern void Cinfo_Equals_mB17F6F32907C286C2B660B6E9E1BCC20A651F941 (void);
// 0x0000013D System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor/Cinfo::Equals(System.Object)
extern void Cinfo_Equals_mB207ED3E11BCF5AEC1A56818611A0082753E8DBC (void);
// 0x0000013E System.Guid UnityEngine.XR.ARSubsystems.XRReferenceImage::get_guid()
extern void XRReferenceImage_get_guid_m646CE46068C4BA601BC23772D3D807D18836B80C (void);
// 0x0000013F System.String UnityEngine.XR.ARSubsystems.XRReferenceImage::ToString()
extern void XRReferenceImage_ToString_m6BC181F25F28FCFB70B2035D7AAEFB1E6DDC6FAA (void);
// 0x00000140 System.Int32 UnityEngine.XR.ARSubsystems.XRReferenceImage::GetHashCode()
extern void XRReferenceImage_GetHashCode_m5178E851EB53F2D106ACBB7C8330F889E1581C09 (void);
// 0x00000141 System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceImage::Equals(System.Object)
extern void XRReferenceImage_Equals_mE3432D5A1D715669F9AEC65EB524B925549F4726 (void);
// 0x00000142 System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceImage::Equals(UnityEngine.XR.ARSubsystems.XRReferenceImage)
extern void XRReferenceImage_Equals_m001D4B708546DF448C4243079684F6FDEE76FBC6 (void);
// 0x00000143 System.Int32 UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::get_count()
extern void XRReferenceImageLibrary_get_count_mFC2EABE3C3D8966005C0AB2E74836BDC998995DD (void);
// 0x00000144 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.ARSubsystems.XRReferenceImage> UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::GetEnumerator()
extern void XRReferenceImageLibrary_GetEnumerator_mE48D64D91D797FCFFA8D582B720C68F14D521710 (void);
// 0x00000145 UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::get_Item(System.Int32)
extern void XRReferenceImageLibrary_get_Item_mD672D0FB305F5209E867F2361EAA542524E3A199 (void);
// 0x00000146 System.Int32 UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::indexOf(UnityEngine.XR.ARSubsystems.XRReferenceImage)
extern void XRReferenceImageLibrary_indexOf_mCF228BB01B5658DAD82FF40FE4B0086C2FE2AC52 (void);
// 0x00000147 System.Guid UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::get_guid()
extern void XRReferenceImageLibrary_get_guid_m101D8AFC1E328EBF5DBDED74F7EA8863A3468418 (void);
// 0x00000148 System.Void UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::.ctor()
extern void XRReferenceImageLibrary__ctor_mF39B0A6A9C1B2777ADD98CAFF08762B601EA5691 (void);
// 0x00000149 System.Void UnityEngine.XR.ARSubsystems.XRTrackedImage::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,System.Guid,UnityEngine.Pose,UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr)
extern void XRTrackedImage__ctor_m0D4DB0925EB1FBEC466A9D19C627F747A400408F (void);
// 0x0000014A UnityEngine.XR.ARSubsystems.XRTrackedImage UnityEngine.XR.ARSubsystems.XRTrackedImage::GetDefault()
extern void XRTrackedImage_GetDefault_m5F66138594FB5852F148CD6F830086B62B906B84 (void);
// 0x0000014B UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRTrackedImage::get_trackableId()
extern void XRTrackedImage_get_trackableId_m6EB6DBACC95E5EE2AFEE3CE421F4C123F32E9CB8 (void);
// 0x0000014C System.Guid UnityEngine.XR.ARSubsystems.XRTrackedImage::get_sourceImageId()
extern void XRTrackedImage_get_sourceImageId_mFEBFE1A21956E0CBF6828407DE0F2209610BF60A (void);
// 0x0000014D UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRTrackedImage::get_pose()
extern void XRTrackedImage_get_pose_m0566E087CA2DC99DF749E80277510C61DCF13186 (void);
// 0x0000014E UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.XRTrackedImage::get_size()
extern void XRTrackedImage_get_size_m746034D0E2FD28C9E48A90965E4FCD9137988906 (void);
// 0x0000014F UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRTrackedImage::get_trackingState()
extern void XRTrackedImage_get_trackingState_mA7177B042E8F9F9B584582970BC5FF0377CE94DB (void);
// 0x00000150 System.IntPtr UnityEngine.XR.ARSubsystems.XRTrackedImage::get_nativePtr()
extern void XRTrackedImage_get_nativePtr_mE90A65D3EDE7F0190F36BD4BDF2E06FEAD113DAF (void);
// 0x00000151 System.Int32 UnityEngine.XR.ARSubsystems.XRTrackedImage::GetHashCode()
extern void XRTrackedImage_GetHashCode_mFF30CD39BC82F7A636BF9E0ACF96967C46F07B5D (void);
// 0x00000152 System.Boolean UnityEngine.XR.ARSubsystems.XRTrackedImage::Equals(UnityEngine.XR.ARSubsystems.XRTrackedImage)
extern void XRTrackedImage_Equals_m626B512ECA4BFBB14918EF13969F8789C3A8A069 (void);
// 0x00000153 System.Boolean UnityEngine.XR.ARSubsystems.XRTrackedImage::Equals(System.Object)
extern void XRTrackedImage_Equals_mF94BFA9B373C9899F29EBD1F01A15ADA2D6E47AF (void);
// 0x00000154 Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.NativeCopyUtility::PtrToNativeArrayWithDefault(T,System.Void*,System.Int32,System.Int32,Unity.Collections.Allocator)
// 0x00000155 UnityEngine.XR.ARSubsystems.BoundedPlane UnityEngine.XR.ARSubsystems.BoundedPlane::GetDefault()
extern void BoundedPlane_GetDefault_mDC1F2DC14F5C53295558B28FA102CE5CD774660A (void);
// 0x00000156 System.Void UnityEngine.XR.ARSubsystems.BoundedPlane::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.PlaneAlignment,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr)
extern void BoundedPlane__ctor_mC6A5A4A0E3417618DAC201CE213664B9791BFACC (void);
// 0x00000157 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.BoundedPlane::get_trackableId()
extern void BoundedPlane_get_trackableId_mD2A3DCDA61898E637ACAE0A0F4A832931378071A (void);
// 0x00000158 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.BoundedPlane::get_subsumedById()
extern void BoundedPlane_get_subsumedById_m77895AF3F9E056F4816CB9177209586E98967889 (void);
// 0x00000159 UnityEngine.Pose UnityEngine.XR.ARSubsystems.BoundedPlane::get_pose()
extern void BoundedPlane_get_pose_m30B084D1F307CB46894BE4FAD448788001A0F0DF (void);
// 0x0000015A UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.BoundedPlane::get_center()
extern void BoundedPlane_get_center_m1AB8ED2CBDC8F4A85358F98A8DD832436818505F (void);
// 0x0000015B UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.BoundedPlane::get_extents()
extern void BoundedPlane_get_extents_mC0F005CBFDA55C68EF9CC52FBC8C0FC07854CBBB (void);
// 0x0000015C UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.BoundedPlane::get_size()
extern void BoundedPlane_get_size_mC49BFA58CD7CE92C2470C82F30EA6F8A4F38B854 (void);
// 0x0000015D UnityEngine.XR.ARSubsystems.PlaneAlignment UnityEngine.XR.ARSubsystems.BoundedPlane::get_alignment()
extern void BoundedPlane_get_alignment_m85C46D81426435F0B9399ABAF80AC5342A2F7137 (void);
// 0x0000015E UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.BoundedPlane::get_trackingState()
extern void BoundedPlane_get_trackingState_m9B6467E433564ABE4BED9328F7B23AB05905B008 (void);
// 0x0000015F System.IntPtr UnityEngine.XR.ARSubsystems.BoundedPlane::get_nativePtr()
extern void BoundedPlane_get_nativePtr_mF0C7299B1CD00C40972DE1BE13C411594A59D361 (void);
// 0x00000160 System.String UnityEngine.XR.ARSubsystems.BoundedPlane::ToString()
extern void BoundedPlane_ToString_m0EE069A6B9579B564EFD59C300277FB7D824E7D1 (void);
// 0x00000161 System.Boolean UnityEngine.XR.ARSubsystems.BoundedPlane::Equals(System.Object)
extern void BoundedPlane_Equals_m74FDA713E8EBBF9256546B4B04A1CCF214ED7D8E (void);
// 0x00000162 System.Int32 UnityEngine.XR.ARSubsystems.BoundedPlane::GetHashCode()
extern void BoundedPlane_GetHashCode_m39BDD70727BE818F86E2DEEF4E264FE13368B637 (void);
// 0x00000163 System.Boolean UnityEngine.XR.ARSubsystems.BoundedPlane::Equals(UnityEngine.XR.ARSubsystems.BoundedPlane)
extern void BoundedPlane_Equals_mF06B1E1B2C53F0BF0FA541CD4828DFD16E8D789D (void);
// 0x00000164 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::.ctor()
extern void XRPlaneSubsystem__ctor_mC6CCE81B1FE634A34E37D1595EC6189A6D5B28E1 (void);
// 0x00000165 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::Start()
extern void XRPlaneSubsystem_Start_m5B5037483FC16EC86C70B67994234236932EDC18 (void);
// 0x00000166 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::OnDestroy()
extern void XRPlaneSubsystem_OnDestroy_mC839A429BEE5C7B303DC6C463A7F5DCE0E827639 (void);
// 0x00000167 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::Stop()
extern void XRPlaneSubsystem_Stop_mBA856FFA3F7680F9B86A915CD28E457369A1975B (void);
// 0x00000168 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::set_planeDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void XRPlaneSubsystem_set_planeDetectionMode_mC7B2B3A8A0FB7853FBA4227F3A4DFD8A155E14DD (void);
// 0x00000169 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.BoundedPlane> UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRPlaneSubsystem_GetChanges_m0487B4AE994BA3CE0DD7D9FA365856F4C9F5710B (void);
// 0x0000016A System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::GetBoundary(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<UnityEngine.Vector2>&)
extern void XRPlaneSubsystem_GetBoundary_mB724A7CF46B3AF8C7E01A0A56854F46C595314CC (void);
// 0x0000016B UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/IProvider UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::CreateProvider()
// 0x0000016C System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::CreateOrResizeNativeArrayIfNecessary(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<T>&)
// 0x0000016D System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/IProvider::Start()
extern void IProvider_Start_mB6468DFD7AD773660A0D1C4D65AD4E184DBE66FE (void);
// 0x0000016E System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/IProvider::Stop()
extern void IProvider_Stop_mB7E116A8377F666DBB09C768E7A33B3FAE4FE888 (void);
// 0x0000016F System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/IProvider::Destroy()
extern void IProvider_Destroy_m409156BDA5D114282D0DCC2491A3BF2586B8DEBF (void);
// 0x00000170 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/IProvider::GetBoundary(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<UnityEngine.Vector2>&)
extern void IProvider_GetBoundary_m821E610D30EFBF9DF2ABAE2DF19DA3334D7EEE9D (void);
// 0x00000171 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.BoundedPlane> UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/IProvider::GetChanges(UnityEngine.XR.ARSubsystems.BoundedPlane,Unity.Collections.Allocator)
extern void IProvider_GetChanges_m565968265ACB120BC15D83E38E4C1AA87796AE6F (void);
// 0x00000172 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/IProvider::set_planeDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void IProvider_set_planeDetectionMode_m56414E4307411B77962ADC47BBB1ED641D339367 (void);
// 0x00000173 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/IProvider::.ctor()
extern void IProvider__ctor_mB2FE77AE4D29B62B76F877BB9761B2CFFBF5A46A (void);
// 0x00000174 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::set_supportsHorizontalPlaneDetection(System.Boolean)
extern void XRPlaneSubsystemDescriptor_set_supportsHorizontalPlaneDetection_m834174F0D747E2D8D8C34EB20D879FFA3E607849 (void);
// 0x00000175 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::set_supportsVerticalPlaneDetection(System.Boolean)
extern void XRPlaneSubsystemDescriptor_set_supportsVerticalPlaneDetection_mF2E7FC43C5A6D048BE8A13F7824CC76543C28AB4 (void);
// 0x00000176 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::set_supportsArbitraryPlaneDetection(System.Boolean)
extern void XRPlaneSubsystemDescriptor_set_supportsArbitraryPlaneDetection_mCC9B359F247DFD0928E3D968CBD7713C744403F6 (void);
// 0x00000177 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::get_supportsBoundaryVertices()
extern void XRPlaneSubsystemDescriptor_get_supportsBoundaryVertices_m455A7A695D7C4F238405B4B50472BE0A8118F611 (void);
// 0x00000178 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::set_supportsBoundaryVertices(System.Boolean)
extern void XRPlaneSubsystemDescriptor_set_supportsBoundaryVertices_m79DA28E3ED06B14E1FEC3EEB72986EA1A420A15E (void);
// 0x00000179 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo)
extern void XRPlaneSubsystemDescriptor_Create_mE7A8E8E49F7EB078CE4D76C9F0D883634157EC9C (void);
// 0x0000017A System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo)
extern void XRPlaneSubsystemDescriptor__ctor_mFF66A6FD68D7DBC951F675E15E230CEADC37B42C (void);
// 0x0000017B System.String UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::get_id()
extern void Cinfo_get_id_m68FCB4621B357B690E4C43846221E241455A1D99 (void);
// 0x0000017C System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::set_id(System.String)
extern void Cinfo_set_id_m9211F9ADC4DCFA1AAB5AA9F662EE6510D6FE01EF (void);
// 0x0000017D System.Type UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_mF924CAAE546DCF3AB899CD4ED8AC2B75B4B0706C (void);
// 0x0000017E System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_m74075897385685A6E0753F2EE29CD77A90A22E6B (void);
// 0x0000017F System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::get_supportsHorizontalPlaneDetection()
extern void Cinfo_get_supportsHorizontalPlaneDetection_m0FB9CC965AD3A99988175C8D1669D341BFB72214 (void);
// 0x00000180 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::set_supportsHorizontalPlaneDetection(System.Boolean)
extern void Cinfo_set_supportsHorizontalPlaneDetection_m6BA5B6FD1C2FDF236AEE15957FD1F1837C394304 (void);
// 0x00000181 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::get_supportsVerticalPlaneDetection()
extern void Cinfo_get_supportsVerticalPlaneDetection_m3C7470BF80CEB7F507540383C2BB02A5B2201AAE (void);
// 0x00000182 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::set_supportsVerticalPlaneDetection(System.Boolean)
extern void Cinfo_set_supportsVerticalPlaneDetection_m386B3816E8C1538AB58318D55D9C64D1113C1B3B (void);
// 0x00000183 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::get_supportsArbitraryPlaneDetection()
extern void Cinfo_get_supportsArbitraryPlaneDetection_mC75F86CCCBA2ED8AD6283EF458F3897CB78DAB3C (void);
// 0x00000184 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::set_supportsArbitraryPlaneDetection(System.Boolean)
extern void Cinfo_set_supportsArbitraryPlaneDetection_m625EDF8616A904C1D2C3B9DB1B52A28A0D3EAF06 (void);
// 0x00000185 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::get_supportsBoundaryVertices()
extern void Cinfo_get_supportsBoundaryVertices_m6E514112FA9F617953047AF6C2CD98895D587E84 (void);
// 0x00000186 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::set_supportsBoundaryVertices(System.Boolean)
extern void Cinfo_set_supportsBoundaryVertices_mFC986523905272E58728731CEE06B47DD4ECAC3D (void);
// 0x00000187 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo)
extern void Cinfo_Equals_mE08723A87EF2FC94FD64BDD93A176F3497518F7D (void);
// 0x00000188 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::Equals(System.Object)
extern void Cinfo_Equals_m3B3C974BB1055B352CAAE2961A7374DA559049F8 (void);
// 0x00000189 System.Int32 UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor/Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m9F3BA097C011CC6998676138C52EF6306C09BBA0 (void);
// 0x0000018A System.Boolean UnityEngine.XR.ARSubsystems.Promise`1::get_keepWaiting()
// 0x0000018B T UnityEngine.XR.ARSubsystems.Promise`1::get_result()
// 0x0000018C System.Void UnityEngine.XR.ARSubsystems.Promise`1::set_result(T)
// 0x0000018D UnityEngine.XR.ARSubsystems.Promise`1<T> UnityEngine.XR.ARSubsystems.Promise`1::CreateResolvedPromise(T)
// 0x0000018E System.Void UnityEngine.XR.ARSubsystems.Promise`1::Resolve(T)
// 0x0000018F System.Void UnityEngine.XR.ARSubsystems.Promise`1::OnKeepWaiting()
// 0x00000190 System.Void UnityEngine.XR.ARSubsystems.Promise`1::.ctor()
// 0x00000191 System.Void UnityEngine.XR.ARSubsystems.Promise`1/ImmediatePromise::OnKeepWaiting()
// 0x00000192 System.Void UnityEngine.XR.ARSubsystems.Promise`1/ImmediatePromise::.ctor(T)
// 0x00000193 UnityEngine.XR.ARSubsystems.XRRaycastHit UnityEngine.XR.ARSubsystems.XRRaycastHit::GetDefault()
extern void XRRaycastHit_GetDefault_mB7D3A8CFE226FD38DDDA4B1C2DE3162FD3B6BE2F (void);
// 0x00000194 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRRaycastHit::get_trackableId()
extern void XRRaycastHit_get_trackableId_mAECCB1BE08FB0B5A48CB27D955250FE2068492CF (void);
// 0x00000195 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRRaycastHit::get_pose()
extern void XRRaycastHit_get_pose_mE0B0A754E818C6FF3675A41CA95185A3E608C8C3 (void);
// 0x00000196 System.Single UnityEngine.XR.ARSubsystems.XRRaycastHit::get_distance()
extern void XRRaycastHit_get_distance_mCD38ECEDD0FA6EAFEFEC71DB7EE3CF1B82B5CEFE (void);
// 0x00000197 UnityEngine.XR.ARSubsystems.TrackableType UnityEngine.XR.ARSubsystems.XRRaycastHit::get_hitType()
extern void XRRaycastHit_get_hitType_m52BBF5DBDE1B3E7E01571EE029F68EB29E240DA6 (void);
// 0x00000198 System.Void UnityEngine.XR.ARSubsystems.XRRaycastHit::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,System.Single,UnityEngine.XR.ARSubsystems.TrackableType)
extern void XRRaycastHit__ctor_mFE2DF7F0A38F4507B3C5684B78F0694266BB76B5 (void);
// 0x00000199 System.Int32 UnityEngine.XR.ARSubsystems.XRRaycastHit::GetHashCode()
extern void XRRaycastHit_GetHashCode_mB0A7A65C634E1CA9C70DC17D2B31A6E082D349EA (void);
// 0x0000019A System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastHit::Equals(System.Object)
extern void XRRaycastHit_Equals_m80D2CC8EEC73127B553C601D9B6A3CEDCFBCF862 (void);
// 0x0000019B System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastHit::Equals(UnityEngine.XR.ARSubsystems.XRRaycastHit)
extern void XRRaycastHit_Equals_mD3774307DB6D9200AE2C4703CF2CB2D90616051C (void);
// 0x0000019C System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::get_running()
extern void XRRaycastSubsystem_get_running_m83B87B0DE729E3A19DB0F30AC0A6918B61E4F0EC (void);
// 0x0000019D System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::.ctor()
extern void XRRaycastSubsystem__ctor_mD6FC049FC72B869A2A78EE56093819D989AD4021 (void);
// 0x0000019E System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::Start()
extern void XRRaycastSubsystem_Start_m088682FAFE45FE3605823014221A92F1FC2459DF (void);
// 0x0000019F System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::Stop()
extern void XRRaycastSubsystem_Stop_m296F12121C158E681FF8ED1575CA06D6D62D039D (void);
// 0x000001A0 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::OnDestroy()
extern void XRRaycastSubsystem_OnDestroy_m6F220962FC615CA40238CDD470B5568DE88C8E02 (void);
// 0x000001A1 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::Raycast(UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void XRRaycastSubsystem_Raycast_mD6335AB75E7AD15295138215F593EAB71754E6FA (void);
// 0x000001A2 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::Raycast(UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void XRRaycastSubsystem_Raycast_m46598C4ACA7D6AC6B6DA53A92ED1349F327EC6BF (void);
// 0x000001A3 UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/IProvider UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::CreateProvider()
// 0x000001A4 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/IProvider::Start()
extern void IProvider_Start_m07598B8B5FF4458B29EDB05DF332F71038D16045 (void);
// 0x000001A5 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/IProvider::Stop()
extern void IProvider_Stop_mD42463E875792E02D298C03F49E4CF5410880909 (void);
// 0x000001A6 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/IProvider::Destroy()
extern void IProvider_Destroy_mDD7686E47514FA1FB4D69FE55A66A5795514EB72 (void);
// 0x000001A7 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/IProvider::Raycast(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void IProvider_Raycast_m70674ED49690ED8304135E4E57023D6F4EF031E6 (void);
// 0x000001A8 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/IProvider::Raycast(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void IProvider_Raycast_m82D63262EEC3A07B14B897B9D2549340D4FB4E82 (void);
// 0x000001A9 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/IProvider::.ctor()
extern void IProvider__ctor_m6ED48B63231AFC2B55F4CAD72EAE1D9C624165DF (void);
// 0x000001AA System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::get_supportsViewportBasedRaycast()
extern void XRRaycastSubsystemDescriptor_get_supportsViewportBasedRaycast_m58C8F3A796EEB498A31A9BEE387A37E935121388 (void);
// 0x000001AB System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::set_supportsViewportBasedRaycast(System.Boolean)
extern void XRRaycastSubsystemDescriptor_set_supportsViewportBasedRaycast_mD39B9FA29B589E0DF23DE7A21012058C0505402C (void);
// 0x000001AC System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::get_supportsWorldBasedRaycast()
extern void XRRaycastSubsystemDescriptor_get_supportsWorldBasedRaycast_mA8C8F4A9E9B0B85E6BE432488CBCF9A97A5E5F4A (void);
// 0x000001AD System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::set_supportsWorldBasedRaycast(System.Boolean)
extern void XRRaycastSubsystemDescriptor_set_supportsWorldBasedRaycast_m4D35B87B7D4284B470A41F46E5261FE953B75035 (void);
// 0x000001AE System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::set_supportedTrackableTypes(UnityEngine.XR.ARSubsystems.TrackableType)
extern void XRRaycastSubsystemDescriptor_set_supportedTrackableTypes_mE17AF0F588A87E741576518F5C851286937BFE27 (void);
// 0x000001AF System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::RegisterDescriptor(UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo)
extern void XRRaycastSubsystemDescriptor_RegisterDescriptor_mA164B987D51AD208D957753220E5B1D1A2DB0650 (void);
// 0x000001B0 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo)
extern void XRRaycastSubsystemDescriptor__ctor_m69D7B8410396340E3D54A19B7EE73B4D24562C3C (void);
// 0x000001B1 System.String UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo::get_id()
extern void Cinfo_get_id_mCCD575E1E7E6E7E7166A4B7F0AF9E7F023FC3FCA (void);
// 0x000001B2 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo::set_id(System.String)
extern void Cinfo_set_id_mDBC061879B3E989FF064E7E31CFC85ACD142199B (void);
// 0x000001B3 System.Type UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_m65BDDAA14217AD17F0928287A8428D7B33A3634F (void);
// 0x000001B4 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_m9670297F5DC91608B606E2B8A7E4C2643236D65A (void);
// 0x000001B5 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo::get_supportsViewportBasedRaycast()
extern void Cinfo_get_supportsViewportBasedRaycast_m8D0A14E0E43F99FCB89A3D87F954D657ADC0C514 (void);
// 0x000001B6 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo::set_supportsViewportBasedRaycast(System.Boolean)
extern void Cinfo_set_supportsViewportBasedRaycast_m42B64A1095C52F16217EBF1D5ABFD7353DA35233 (void);
// 0x000001B7 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo::get_supportsWorldBasedRaycast()
extern void Cinfo_get_supportsWorldBasedRaycast_m327E8B0EE9C3103FBF490CA62FE5E1A51EF62C9F (void);
// 0x000001B8 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo::set_supportsWorldBasedRaycast(System.Boolean)
extern void Cinfo_set_supportsWorldBasedRaycast_mBF04DD8B3208A7D9C98419FEDC8CB012F7253DF5 (void);
// 0x000001B9 UnityEngine.XR.ARSubsystems.TrackableType UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo::get_supportedTrackableTypes()
extern void Cinfo_get_supportedTrackableTypes_mCC57E28DFCE93ECA772B1DE2E3E49AD030D79424 (void);
// 0x000001BA System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo::set_supportedTrackableTypes(UnityEngine.XR.ARSubsystems.TrackableType)
extern void Cinfo_set_supportedTrackableTypes_m13138A57079E692472B33A4B216D5568852BE652 (void);
// 0x000001BB System.Int32 UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m155C084A788BBE99BBD85B2B06D7CC74DF76D636 (void);
// 0x000001BC System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo::Equals(System.Object)
extern void Cinfo_Equals_m434EDD8246018244C8CBBD881D91CCDBF5437EF6 (void);
// 0x000001BD System.String UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo::ToString()
extern void Cinfo_ToString_m2DFF2387C99F28931C7E802878564A3694D51453 (void);
// 0x000001BE System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor/Cinfo)
extern void Cinfo_Equals_m21B31135C0E69990482BE0286436FF51748CF92B (void);
// 0x000001BF UnityEngine.XR.ARSubsystems.XRReferencePoint UnityEngine.XR.ARSubsystems.XRReferencePoint::GetDefault()
extern void XRReferencePoint_GetDefault_m2C8B9BC8378BD9F7C636DCD1C90D4793F9C80BF8 (void);
// 0x000001C0 System.Void UnityEngine.XR.ARSubsystems.XRReferencePoint::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr)
extern void XRReferencePoint__ctor_mD4597D205E31CBA88C523E616E7A45D78C95BF3E (void);
// 0x000001C1 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRReferencePoint::get_trackableId()
extern void XRReferencePoint_get_trackableId_m6D53542802F2444CE58861B8868274F9A8296D88 (void);
// 0x000001C2 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRReferencePoint::get_pose()
extern void XRReferencePoint_get_pose_mA4320629B8C7AE23D97FCD8E2C5FB9C9FB6AED9C (void);
// 0x000001C3 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRReferencePoint::get_trackingState()
extern void XRReferencePoint_get_trackingState_mBA0DB4050B734039D22D0ECF69CD6E8896DF52B8 (void);
// 0x000001C4 System.IntPtr UnityEngine.XR.ARSubsystems.XRReferencePoint::get_nativePtr()
extern void XRReferencePoint_get_nativePtr_m51D5CA830D54C1D67656E683BE81A53D479E71A1 (void);
// 0x000001C5 System.Int32 UnityEngine.XR.ARSubsystems.XRReferencePoint::GetHashCode()
extern void XRReferencePoint_GetHashCode_mAA9EDDD897E7DDD88AA10A7B9E10FC2025E4F7CA (void);
// 0x000001C6 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePoint::Equals(UnityEngine.XR.ARSubsystems.XRReferencePoint)
extern void XRReferencePoint_Equals_m4783954B81A2A4496B718B1940FEB8D9BD99C15D (void);
// 0x000001C7 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePoint::Equals(System.Object)
extern void XRReferencePoint_Equals_m63D1CD2AE3191F649B2791970AB507AA379B78FA (void);
// 0x000001C8 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::.ctor()
extern void XRReferencePointSubsystem__ctor_m00BC1D2247754D96678FBCC67FE14BBC2107D275 (void);
// 0x000001C9 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::Start()
extern void XRReferencePointSubsystem_Start_m92D4BE486883048A2C181BB0D4C3E75A10F834BE (void);
// 0x000001CA System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::Stop()
extern void XRReferencePointSubsystem_Stop_mFAFD07F36E8E1ADDB46CEE80E65A0A65CFFB0E99 (void);
// 0x000001CB System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::OnDestroy()
extern void XRReferencePointSubsystem_OnDestroy_m49B0CE290FA4181799996BB3343CE6412D58E4C5 (void);
// 0x000001CC UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint> UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRReferencePointSubsystem_GetChanges_mE0EC8049CED1EA604A751066DB97430E803BE487 (void);
// 0x000001CD System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::TryAddReferencePoint(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRReferencePoint&)
extern void XRReferencePointSubsystem_TryAddReferencePoint_mEC8446DC54875A2954FFAA796B3BA6D671BCC064 (void);
// 0x000001CE System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::TryAttachReferencePoint(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRReferencePoint&)
extern void XRReferencePointSubsystem_TryAttachReferencePoint_m71CC28796BB1087039C1F0F067A19C0836649FF5 (void);
// 0x000001CF System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::TryRemoveReferencePoint(UnityEngine.XR.ARSubsystems.TrackableId)
extern void XRReferencePointSubsystem_TryRemoveReferencePoint_m212CA9D300500BDFFEBC9B99F8F3C29C8D2BE0D9 (void);
// 0x000001D0 UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem/IProvider UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::CreateProvider()
// 0x000001D1 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem/IProvider::Start()
extern void IProvider_Start_m36529D200576FC422EF0E0815D4CE54C9452B51D (void);
// 0x000001D2 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem/IProvider::Stop()
extern void IProvider_Stop_m6E16DE8C2AB51A51A05D3DE786D0720FBE2607BF (void);
// 0x000001D3 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem/IProvider::Destroy()
extern void IProvider_Destroy_mDE0559C0896A464F033F677A73CD0A16CB082A88 (void);
// 0x000001D4 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint> UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem/IProvider::GetChanges(UnityEngine.XR.ARSubsystems.XRReferencePoint,Unity.Collections.Allocator)
extern void IProvider_GetChanges_m7F99F4F7722D3F7761B2144E01E5964B90A0D579 (void);
// 0x000001D5 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem/IProvider::TryAddReferencePoint(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRReferencePoint&)
extern void IProvider_TryAddReferencePoint_m272B550B20E72D235C3E9347E87BBBEA456415A8 (void);
// 0x000001D6 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem/IProvider::TryAttachReferencePoint(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRReferencePoint&)
extern void IProvider_TryAttachReferencePoint_m4F7742B7C3CB7418C12C23B91BA6807748BC8F61 (void);
// 0x000001D7 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem/IProvider::TryRemoveReferencePoint(UnityEngine.XR.ARSubsystems.TrackableId)
extern void IProvider_TryRemoveReferencePoint_mFF0DA84C79423650C0D155402B7D69EF0F8D436B (void);
// 0x000001D8 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem/IProvider::.ctor()
extern void IProvider__ctor_m7D69D152ED0A017FF02C31CDD297F546148C9DD5 (void);
// 0x000001D9 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor::set_supportsTrackableAttachments(System.Boolean)
extern void XRReferencePointSubsystemDescriptor_set_supportsTrackableAttachments_m3FBD1AAB8470EFCF3A9F31271331B1BE0DAA93BF (void);
// 0x000001DA System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor/Cinfo)
extern void XRReferencePointSubsystemDescriptor_Create_m6560F038F7DD64D1FEE3924A57AD4DBC79D29988 (void);
// 0x000001DB System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor/Cinfo)
extern void XRReferencePointSubsystemDescriptor__ctor_m549A60C3584AADA3A0DE752C0F363113A61FD0A2 (void);
// 0x000001DC System.String UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor/Cinfo::get_id()
extern void Cinfo_get_id_m915E41A74906A167A3AD5FA288FC098F301167D9 (void);
// 0x000001DD System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor/Cinfo::set_id(System.String)
extern void Cinfo_set_id_m022C41DDBCD030680C14FA11F178C96FCE67D687 (void);
// 0x000001DE System.Type UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor/Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_m66047A62536BD79CB4452C2FE4878FFE035293C2 (void);
// 0x000001DF System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor/Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_m713DB30ECC2AFC3D45F60BE6910713C6009A99E0 (void);
// 0x000001E0 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor/Cinfo::get_supportsTrackableAttachments()
extern void Cinfo_get_supportsTrackableAttachments_mE8629D11414D8E706864D0F179E39CA063035137 (void);
// 0x000001E1 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor/Cinfo::set_supportsTrackableAttachments(System.Boolean)
extern void Cinfo_set_supportsTrackableAttachments_m1A6C2F5CE68A65944F1631E0E32FFC6BC04ECB03 (void);
// 0x000001E2 System.Int32 UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor/Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m44A7F0571AF0F4CEFECFDF9347EA517EA62F8E5D (void);
// 0x000001E3 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor/Cinfo::Equals(System.Object)
extern void Cinfo_Equals_m48D047F332BBAC4B19A45B966678D92CADC6F0F2 (void);
// 0x000001E4 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor/Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor/Cinfo)
extern void Cinfo_Equals_m62C82D4C3A17A329222BC38370B76CDB219ED6F3 (void);
// 0x000001E5 System.Guid UnityEngine.XR.ARSubsystems.SerializableGuid::get_guid()
extern void SerializableGuid_get_guid_mBBAACA6CC4257BB65BB6178DB4B9B8273462D71B (void);
// 0x000001E6 System.Int32 UnityEngine.XR.ARSubsystems.SerializableGuid::GetHashCode()
extern void SerializableGuid_GetHashCode_m2220A2C721DACD85F1271B8D17D334323B0CAD2D (void);
// 0x000001E7 System.Boolean UnityEngine.XR.ARSubsystems.SerializableGuid::Equals(System.Object)
extern void SerializableGuid_Equals_m0775AC30655B576CF3FC2E92FCD210A864264994 (void);
// 0x000001E8 System.String UnityEngine.XR.ARSubsystems.SerializableGuid::ToString()
extern void SerializableGuid_ToString_mDC25F3F328C27C046B6BAD13E5839C363B4BF98C (void);
// 0x000001E9 System.Boolean UnityEngine.XR.ARSubsystems.SerializableGuid::Equals(UnityEngine.XR.ARSubsystems.SerializableGuid)
extern void SerializableGuid_Equals_mE819A7AFF3AC4EE0DBF8AC4811E3E930367DDFEC (void);
// 0x000001EA System.Boolean UnityEngine.XR.ARSubsystems.SessionAvailabilityExtensions::IsSupported(UnityEngine.XR.ARSubsystems.SessionAvailability)
extern void SessionAvailabilityExtensions_IsSupported_mE7271B43B00DD99C98FA00AAAE3A948E0822D624 (void);
// 0x000001EB System.Boolean UnityEngine.XR.ARSubsystems.SessionAvailabilityExtensions::IsInstalled(UnityEngine.XR.ARSubsystems.SessionAvailability)
extern void SessionAvailabilityExtensions_IsInstalled_m199B1D6F5729DE0A1BC7144BE38DEF9896B68A33 (void);
// 0x000001EC System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_running()
extern void XRSessionSubsystem_get_running_m0AF2183320BAA9C43DD4B9E12B11514E579487E6 (void);
// 0x000001ED UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionAvailability> UnityEngine.XR.ARSubsystems.XRSessionSubsystem::GetAvailabilityAsync()
extern void XRSessionSubsystem_GetAvailabilityAsync_mE1444BD33C0A1EAD4982FC0AE64D1251635487ED (void);
// 0x000001EE UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionInstallationStatus> UnityEngine.XR.ARSubsystems.XRSessionSubsystem::InstallAsync()
extern void XRSessionSubsystem_InstallAsync_m35E08EF7130491F2E498C990109FA7323A2ABCCC (void);
// 0x000001EF System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::.ctor()
extern void XRSessionSubsystem__ctor_m0E7B9E65E53B03A65C53F87CF55E76528E9AF62A (void);
// 0x000001F0 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::Start()
extern void XRSessionSubsystem_Start_m4309ED22C582729B7E0CE7593EA4F05E5D01E80C (void);
// 0x000001F1 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::Reset()
extern void XRSessionSubsystem_Reset_m09A1C906E574CD27377602632572058C88958773 (void);
// 0x000001F2 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::Stop()
extern void XRSessionSubsystem_Stop_m660C2C790C22C34B49054B5AC0033BD1B2993A6D (void);
// 0x000001F3 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::OnDestroy()
extern void XRSessionSubsystem_OnDestroy_m6E173EA064A32E49BF31E67245A70BFB0970890C (void);
// 0x000001F4 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::Update(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void XRSessionSubsystem_Update_m40F8405ECB47FDC56B0B203F09655E3E5F637EFB (void);
// 0x000001F5 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::OnApplicationPause()
extern void XRSessionSubsystem_OnApplicationPause_m5030BDDD5E7BB722D7259D98C988911335751945 (void);
// 0x000001F6 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::OnApplicationResume()
extern void XRSessionSubsystem_OnApplicationResume_m47B8B29B90F5DDF27221EE1E32D5DBA22C347FBD (void);
// 0x000001F7 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_trackingState()
extern void XRSessionSubsystem_get_trackingState_m6CEDC16CB9B224A0302A83BC2C22FC4C0905EB30 (void);
// 0x000001F8 UnityEngine.XR.ARSubsystems.NotTrackingReason UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_notTrackingReason()
extern void XRSessionSubsystem_get_notTrackingReason_m2425113BCCDD44CEF92AA9A045C002CAF981B6D7 (void);
// 0x000001F9 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_matchFrameRate()
extern void XRSessionSubsystem_get_matchFrameRate_mC750A3D6F4EA06E45880B76305178E568E04C82A (void);
// 0x000001FA System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::set_matchFrameRate(System.Boolean)
extern void XRSessionSubsystem_set_matchFrameRate_mAE3C92CA2CA959912C6AF7AB1BD10B2B9A9FC54C (void);
// 0x000001FB System.Int32 UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_frameRate()
extern void XRSessionSubsystem_get_frameRate_m0E723AB90D900D24D7DA2F5A2A8009CB4946A62B (void);
// 0x000001FC UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider UnityEngine.XR.ARSubsystems.XRSessionSubsystem::CreateProvider()
// 0x000001FD System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::Resume()
extern void IProvider_Resume_mE7AD058BDE3BD98A07B26F97C71FB5AB0BD85CA3 (void);
// 0x000001FE System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::Pause()
extern void IProvider_Pause_mE01D418A52EA40CB3BDDACC95052A68BF4646C32 (void);
// 0x000001FF System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::Update(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void IProvider_Update_m5547CB709CDEBCAC2330B3F5EBBF9758B4C3EC89 (void);
// 0x00000200 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::Destroy()
extern void IProvider_Destroy_m2284C5D0C971DFE6960647F4876A139CCE3BCC97 (void);
// 0x00000201 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::Reset()
extern void IProvider_Reset_m4A833C1AC1AEEF1926652298879EC04C355B3506 (void);
// 0x00000202 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::OnApplicationPause()
extern void IProvider_OnApplicationPause_m225EDBE7D139FAA2ACD88CFEC0159C060DFA197C (void);
// 0x00000203 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::OnApplicationResume()
extern void IProvider_OnApplicationResume_m7214879F26973B5CCB17D476416DD9345B13E0D5 (void);
// 0x00000204 UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionAvailability> UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::GetAvailabilityAsync()
extern void IProvider_GetAvailabilityAsync_mBEB3A0B9542D7FFC86148CDA981B9D7A79E6ECAF (void);
// 0x00000205 UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionInstallationStatus> UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::InstallAsync()
extern void IProvider_InstallAsync_m89483BB3B7B4D4E481B1F73CC1E2C0DAABAAC2BE (void);
// 0x00000206 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::get_trackingState()
extern void IProvider_get_trackingState_m3D36F03D7C209523EDDFEF23BAFDBD41B56E1128 (void);
// 0x00000207 UnityEngine.XR.ARSubsystems.NotTrackingReason UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::get_notTrackingReason()
extern void IProvider_get_notTrackingReason_mD52FF69EFF5BAB7432DDF25F883C86FFA9BF2D3F (void);
// 0x00000208 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::get_matchFrameRate()
extern void IProvider_get_matchFrameRate_m7D043BC7FAC35BB9E54EFB6035864487B775624C (void);
// 0x00000209 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::set_matchFrameRate(System.Boolean)
extern void IProvider_set_matchFrameRate_mFD6D9C6FD79B16E999A0E7650260F954F1CA6B66 (void);
// 0x0000020A System.Int32 UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::get_frameRate()
extern void IProvider_get_frameRate_m157CFC505E86907CB821238B45B2BA6FA4983813 (void);
// 0x0000020B System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider::.ctor()
extern void IProvider__ctor_m44F52AA14885F4899E67E09988D8B08C827DEFED (void);
// 0x0000020C System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::get_supportsInstall()
extern void XRSessionSubsystemDescriptor_get_supportsInstall_m6A46D829025B00AFFCEE06A8A66A3D383AF2757E (void);
// 0x0000020D System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::set_supportsInstall(System.Boolean)
extern void XRSessionSubsystemDescriptor_set_supportsInstall_m8660DC05AA837607AC160D4300E7D1D204BD1E18 (void);
// 0x0000020E System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::get_supportsMatchFrameRate()
extern void XRSessionSubsystemDescriptor_get_supportsMatchFrameRate_m331D3B1192D04F6EF98D9172D70EC35A5B24A02B (void);
// 0x0000020F System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::set_supportsMatchFrameRate(System.Boolean)
extern void XRSessionSubsystemDescriptor_set_supportsMatchFrameRate_m9371FEB427307794FC0EB7DF361E2F47E6E3F378 (void);
// 0x00000210 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::RegisterDescriptor(UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor/Cinfo)
extern void XRSessionSubsystemDescriptor_RegisterDescriptor_m3EF9E7985B16FFF8FE15FBEDFC87FF1BB811D49E (void);
// 0x00000211 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor/Cinfo)
extern void XRSessionSubsystemDescriptor__ctor_m1956ED91DA6DA8D4EAA62906C378B69F5F3E8C1E (void);
// 0x00000212 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor/Cinfo::get_supportsInstall()
extern void Cinfo_get_supportsInstall_m010F6B6254015F5F477114A35C4F11F4A3334E2E (void);
// 0x00000213 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor/Cinfo::set_supportsInstall(System.Boolean)
extern void Cinfo_set_supportsInstall_m4295AB46C19802B003C61D7EB79DC8D02CF14B80 (void);
// 0x00000214 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor/Cinfo::get_supportsMatchFrameRate()
extern void Cinfo_get_supportsMatchFrameRate_m7DD61A2A7B767E475C97DF33FA4785C2DC12B6E3 (void);
// 0x00000215 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor/Cinfo::set_supportsMatchFrameRate(System.Boolean)
extern void Cinfo_set_supportsMatchFrameRate_mE43FF83622414EA44D02418EC98B1DA8DDFFDBD6 (void);
// 0x00000216 System.String UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor/Cinfo::get_id()
extern void Cinfo_get_id_m038FCFC448004E0F21D8DF6F22FBFA0F5AE14870 (void);
// 0x00000217 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor/Cinfo::set_id(System.String)
extern void Cinfo_set_id_m8E2A1220FE77B46B870237AE788DFEE34F6C29CB (void);
// 0x00000218 System.Type UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor/Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_m27F97E21F5CC1903B5358E2C386343D53F3379BF (void);
// 0x00000219 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor/Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_m9591600428F1364957BEDD8C12C1B734BBA2BF85 (void);
// 0x0000021A System.Int32 UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor/Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m10FAF407C61975E8F03E5F4B961BC2583EA369A9 (void);
// 0x0000021B System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor/Cinfo::Equals(System.Object)
extern void Cinfo_Equals_m2AB456C7BFFE6923D76AE087F4548493143B1B7B (void);
// 0x0000021C System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor/Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor/Cinfo)
extern void Cinfo_Equals_m1FEB5A86DE5F73249471CC686193B327ED687B67 (void);
// 0x0000021D UnityEngine.ScreenOrientation UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::get_screenOrientation()
extern void XRSessionUpdateParams_get_screenOrientation_m111C145EA6A683F025DF48C6EA355E37D8974183 (void);
// 0x0000021E System.Void UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::set_screenOrientation(UnityEngine.ScreenOrientation)
extern void XRSessionUpdateParams_set_screenOrientation_m977BF9AC1B8FF7224144F0979A8A30325256EE12 (void);
// 0x0000021F UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::get_screenDimensions()
extern void XRSessionUpdateParams_get_screenDimensions_m61A9722E272D6292B9C7C093BF7792FB007BF21E (void);
// 0x00000220 System.Void UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::set_screenDimensions(UnityEngine.Vector2Int)
extern void XRSessionUpdateParams_set_screenDimensions_m74048D3192BAF559FEFAB878921C3EBB68ACB635 (void);
// 0x00000221 System.Int32 UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::GetHashCode()
extern void XRSessionUpdateParams_GetHashCode_m5D06AEAC2DD5497E37C5C8A1E06952186ACDCC70 (void);
// 0x00000222 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::Equals(System.Object)
extern void XRSessionUpdateParams_Equals_m0D01CBAE986724E42B3FF0EBE51808915F873B92 (void);
// 0x00000223 System.String UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::ToString()
extern void XRSessionUpdateParams_ToString_mC2C61A95F598C42B879A6E20982DA96B16E23B6D (void);
// 0x00000224 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::Equals(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void XRSessionUpdateParams_Equals_mAA0877F7CE8BCEC50F568C257794F50C3A4BFDB8 (void);
// 0x00000225 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.TrackableId::get_invalidId()
extern void TrackableId_get_invalidId_mBE9FA1EC8F2EC1575C1B31666EA928A3382DF1CD (void);
// 0x00000226 System.UInt64 UnityEngine.XR.ARSubsystems.TrackableId::get_subId1()
extern void TrackableId_get_subId1_mC31404D1705A36B5554158D187B8E91107425D4D (void);
// 0x00000227 System.UInt64 UnityEngine.XR.ARSubsystems.TrackableId::get_subId2()
extern void TrackableId_get_subId2_m42D794430D6C6163D2F5F839A58383F5BB628AC0 (void);
// 0x00000228 System.Void UnityEngine.XR.ARSubsystems.TrackableId::.ctor(System.UInt64,System.UInt64)
extern void TrackableId__ctor_m620606AC246BEEC637A748B22FAB588CB93120B8 (void);
// 0x00000229 System.String UnityEngine.XR.ARSubsystems.TrackableId::ToString()
extern void TrackableId_ToString_m9C04F12E2DA81C481BAABC989B14E8B3509DADB2 (void);
// 0x0000022A System.Int32 UnityEngine.XR.ARSubsystems.TrackableId::GetHashCode()
extern void TrackableId_GetHashCode_m6F1171936847F6A193255FABDCA3772D7AE57328 (void);
// 0x0000022B System.Boolean UnityEngine.XR.ARSubsystems.TrackableId::Equals(System.Object)
extern void TrackableId_Equals_m513A2978A536C42A1AFE1A4D42BEE78EE056BD28 (void);
// 0x0000022C System.Boolean UnityEngine.XR.ARSubsystems.TrackableId::Equals(UnityEngine.XR.ARSubsystems.TrackableId)
extern void TrackableId_Equals_mB8FF48A7F895DEE1E826EDD6126475258BE9ADC2 (void);
// 0x0000022D System.Boolean UnityEngine.XR.ARSubsystems.TrackableId::op_Equality(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.XR.ARSubsystems.TrackableId)
extern void TrackableId_op_Equality_mDC7BF74A9CA0E8F34E4BD3C584918380A624D3E8 (void);
// 0x0000022E System.Void UnityEngine.XR.ARSubsystems.TrackableId::.cctor()
extern void TrackableId__cctor_m874A4E0F53AF39A7D95AEA9A0E8164BA2B6EA32F (void);
// 0x0000022F UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.ITrackable::get_trackableId()
// 0x00000230 UnityEngine.Pose UnityEngine.XR.ARSubsystems.ITrackable::get_pose()
// 0x00000231 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.ITrackable::get_trackingState()
// 0x00000232 Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::get_added()
// 0x00000233 Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::get_updated()
// 0x00000234 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1::get_removed()
// 0x00000235 System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1::get_isCreated()
// 0x00000236 System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::set_isCreated(System.Boolean)
// 0x00000237 System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
// 0x00000238 System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::Dispose()
// 0x00000239 System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::get_running()
// 0x0000023A UnityEngine.XR.ARSubsystems.TrackableChanges`1<TTrackable> UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::GetChanges(Unity.Collections.Allocator)
// 0x0000023B System.Void UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::.ctor()
// 0x0000023C System.IntPtr UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_nativeTexture()
extern void XRTextureDescriptor_get_nativeTexture_mC7FFC8C9D5E8C5BBD93F7A7E95B29253FD59770B (void);
// 0x0000023D System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_width()
extern void XRTextureDescriptor_get_width_m66B9E821EBE5FEBAB7A9B589A056462FD2E35D04 (void);
// 0x0000023E System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_height()
extern void XRTextureDescriptor_get_height_mFC30414502C03B7BDD149DFFC374ACE0BD472755 (void);
// 0x0000023F System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_mipmapCount()
extern void XRTextureDescriptor_get_mipmapCount_m206E924935A23EB8B06CE8FC1E98BC74B09247F3 (void);
// 0x00000240 UnityEngine.TextureFormat UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_format()
extern void XRTextureDescriptor_get_format_mA2DA22DC1DEBCAD27A9C69F3374D614DF1C3FA2B (void);
// 0x00000241 System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_propertyNameId()
extern void XRTextureDescriptor_get_propertyNameId_mCF70D57DDDB24C2F3521DA86DFC1131DB29B8D80 (void);
// 0x00000242 System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_valid()
extern void XRTextureDescriptor_get_valid_mBDA97DA3C73C0B8282A29606BFD70F1C29C0B4AE (void);
// 0x00000243 System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::hasIdenticalTextureMetadata(UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void XRTextureDescriptor_hasIdenticalTextureMetadata_m7EF6E9887C77831839D7AECC3B8E51022821A5A8 (void);
// 0x00000244 System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::Equals(UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void XRTextureDescriptor_Equals_m8198CAFC2D9A7FA7941C2B587F6FB1B9FE5918EB (void);
// 0x00000245 System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::Equals(System.Object)
extern void XRTextureDescriptor_Equals_m162675AC01545EA2A8149CE27A70E811C9A7B3D5 (void);
// 0x00000246 System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::GetHashCode()
extern void XRTextureDescriptor_GetHashCode_m686D69173CDCFB2CB96E32C948A0EEC176ED19AD (void);
// 0x00000247 System.String UnityEngine.XR.ARSubsystems.XRTextureDescriptor::ToString()
extern void XRTextureDescriptor_ToString_m8F80DF64DD7FC44FBFB6FFD42FA3B7265FB016C2 (void);
static Il2CppMethodPointer s_methodPointers[583] = 
{
	XRCameraConfiguration_get_width_m998994AB66DE43B180D472B2B703B524C69497B4,
	XRCameraConfiguration_get_height_m13F96FED610F582C624331BD6394B88E4D055140,
	XRCameraConfiguration_get_framerate_m97E323885AEFFC7E8FCAC41CBE3F72A34400440B,
	XRCameraConfiguration_ToString_m423574318A784468BA1055A28EC192FC34D4FA30,
	XRCameraConfiguration_GetHashCode_m0826525E55CF9BD9E693F2D359F46A36E7FB08EC,
	XRCameraConfiguration_Equals_m04BF698F8D3E743BEF15107AFF74AAE1347CE82C,
	XRCameraConfiguration_Equals_m40024812F8E6090F4F18A6F0EBC055E9ED1B50BC,
	XRCameraFrame_get_timestampNs_m490E96B453EB3AA7416F98013B47B551C003268C,
	XRCameraFrame_get_averageBrightness_mA23D9FB95046E89792F9F70750E000FF30E2959C,
	XRCameraFrame_get_averageColorTemperature_m2AA0B1BE3B939E9221507D9AFB1CB28AE9FF0234,
	XRCameraFrame_get_colorCorrection_m21EA821139C87463A369E9D308D3794A398D7A2C,
	XRCameraFrame_get_projectionMatrix_m1AFF19568E4F29BDC16B943B34A8C988CA8382A1,
	XRCameraFrame_get_displayMatrix_mE7C0D233A017D72EA36CC4879F49BC872D49B2A5,
	XRCameraFrame_get_hasTimestamp_m9463BC747994BF7E6AEFDA339D88F5EDB02AA83B,
	XRCameraFrame_get_hasAverageBrightness_m86A574F4A162689E55C8B2C0F04B9528CFB738A5,
	XRCameraFrame_get_hasAverageColorTemperature_mB177764FC2D9592E4CA15F5BEFD710EF5510DB15,
	XRCameraFrame_get_hasColorCorrection_m8FE25191C55FF1FA09EE7EB0D1AC1ABB1CD87597,
	XRCameraFrame_get_hasProjectionMatrix_mC15F4AD61C07287736EA10A7C7BE5F66EE0258D1,
	XRCameraFrame_get_hasDisplayMatrix_mD63FE4D87F9D23F40217F3DA69CEF5D01F45B8A9,
	XRCameraFrame_Equals_mD750801D16ED7C39E75B31B663A97F300513FE3E,
	XRCameraFrame_Equals_mA3BFB0E406F27C0A40D0A8B79E02A17B8C06AA61,
	XRCameraFrame_GetHashCode_mB60ADF713E8CA46B1629DA9EB361ED34127C358C,
	XRCameraFrame_ToString_mE362631B14B4302E3AED73932E73C1D6AB5684FA,
	XRCameraImage_get_dimensions_m396844BB3871C3CF359299C935A286F7EC9E07CC,
	XRCameraImage_set_dimensions_m0EB351A10EB73AEB3BA66DEBB92CF6A482DF0FDD,
	XRCameraImage_get_width_m9540469A8631057CDBA693ACA5B39F11F7F8F85A,
	XRCameraImage_get_height_m3DFB851BE8723A821A3AB2A7F08F38E5C910A2BE,
	XRCameraImage_get_planeCount_mE12B1F437995559D932808E1D5CA4838243802D6,
	XRCameraImage_set_planeCount_mBC04A59810816A0E2B8F173F17A17627CB2177C5,
	XRCameraImage_get_format_m363F742D003794B51229FEF58C6D24EF2E4174D9,
	XRCameraImage_set_format_m587247101073030F7122D794E2C7C4ED3208D3EC,
	XRCameraImage_set_timestamp_m817C23EC056A13E82BC33FD115C5DC4A40569C5F,
	XRCameraImage__cctor_mCE4936C86EAC70BB3FBC1690BE39DED4D063FFFA,
	XRCameraImage__ctor_m13AB438F2246EEA666CEB1A6C7CE9C32B7F867C0,
	XRCameraImage_OnAsyncConversionComplete_m8C475A6334B19FAC20896F0DFEFBDEAC6E5EE53B,
	XRCameraImage_Dispose_m01A1F3830038FD9ED9B35B6640A6CA57D383615A,
	XRCameraImage_GetHashCode_m27BEDB780E2AF59CA888ED4A8A0DC2B120BC756E,
	XRCameraImage_Equals_mE4A7D2162B33C795268692D0B745F9610B58D4F6,
	XRCameraImage_Equals_m9408E719265D28710EF012AA4E2E63AF320C7431,
	XRCameraImage_ToString_m2E0D10A11B0FAE6A42A572D9C23320587B53C9E0,
	XRCameraImageConversionParams_get_inputRect_m6F91AAA4D0844E9A9E1391A2FA73F99CEADAC833,
	XRCameraImageConversionParams_get_outputDimensions_mE4BFDDB1E03C9024E392ABD2A0EF90398F1188D2,
	XRCameraImageConversionParams_get_outputFormat_m7960F36DE5418DCFCBD0E780C483EDA42AB9405F,
	XRCameraImageConversionParams_get_transformation_m2DA4133406E43882B1B52CDB3867F2E29A70C013,
	XRCameraImageConversionParams_GetHashCode_mE4B9237F3C69745C28C8D0D958B353D80F33A876,
	XRCameraImageConversionParams_Equals_m7E25A42871B1C10084E685AC78F4A2E53D42652E,
	XRCameraImageConversionParams_Equals_mAB0C49294DC3FB284CB470DDC298B696C4B28A06,
	XRCameraImageConversionParams_ToString_mCD58F5C2B04509ADEDDB374DA4492E22F3D70E97,
	XRCameraIntrinsics_Equals_mEA26D3BF6A90B7DC9E2FF626BEA72EB6C098D5D2,
	XRCameraIntrinsics_Equals_mB82F3C59386B2169809615700C79BF3DC519C45F,
	XRCameraIntrinsics_GetHashCode_mC38995C37469CCF20C3A155F7F033BD16EE36D98,
	XRCameraIntrinsics_ToString_m6A86B906A1EB79BDC75582D45311E75323054983,
	XRCameraParams_set_zNear_m0592AF26BE6AD3149462E71FB77B3838ACC3E9AB,
	XRCameraParams_set_zFar_m85FE4877910BD92BFE4F308F73EA2BE35F3538BF,
	XRCameraParams_set_screenWidth_mF256E58C15E4E73B3675323C93D1F38E06438919,
	XRCameraParams_set_screenHeight_mFFFBD063E1AA590D9B5055287965EF5A0A0B92A8,
	XRCameraParams_set_screenOrientation_m2F3BC04E753E945AD1AE69F2643301C5FDEA3117,
	XRCameraParams_Equals_m0E44D78DF6343B56235F52CC885DC36ABD69F586,
	XRCameraParams_Equals_m2AF39FA9619DC2B0C470AAEEDF35A05E85BD0A1E,
	XRCameraParams_GetHashCode_m4E01EC262AD8F277BB0793D9782904D78F3B46D4,
	XRCameraParams_ToString_mBE56D42E3C4F978B3A61B67C346E4CB58BD293CD,
	XRCameraSubsystem_get_running_m48C0F3AC90563B18389076B33BA7505096C42C2D,
	XRCameraSubsystem__ctor_m4DB65C1288A29F049A4A362B9CD81B60970A73AE,
	XRCameraSubsystem_set_focusMode_m4230E264ADA524A210DDE27D926F0DE66918C07A,
	XRCameraSubsystem_set_lightEstimationMode_m0B04B0E3D3E8F73CA5FC6868DE86D6CFFF2263B5,
	XRCameraSubsystem_Start_mA8F48C52695FA1A13DF12B8FF09E919A467D14DE,
	XRCameraSubsystem_Stop_m8E0BEA2C6E44306AC10FA18EADC19ABBFCD70136,
	XRCameraSubsystem_OnDestroy_m4B56BEECBCFD8BBB8B67D38DA084D1AE41EE29EB,
	XRCameraSubsystem_GetTextureDescriptors_mD7CFEDA2DDB138789A1E96CE71672F9C12FE4D21,
	XRCameraSubsystem_get_shaderName_m1E02E577015568DA7CF93F4C55FB6094118BC014,
	XRCameraSubsystem_TryGetIntrinsics_mA8696FD477463BD57E5EF328AA276439C556BD3F,
	XRCameraSubsystem_GetConfigurations_m89E371017BA4612ECF7B4D472A0F71D290738BFA,
	XRCameraSubsystem_get_currentConfiguration_m95C8ED2F04B9AD10A1C2498C5D32DF03821A1DBF,
	XRCameraSubsystem_set_currentConfiguration_mC6AAFA4CCF55F2293DB2F8E0A16A9239438ED70D,
	XRCameraSubsystem_get_invertCulling_mB2F2CD061D9065757EDFB6EAD361FB0B1782BAF4,
	NULL,
	XRCameraSubsystem_TryGetLatestFrame_m7BDBD516A8A2832ECBD9334E4D7B0D38ED2D191F,
	XRCameraSubsystem_get_permissionGranted_m4BE44093A85FE550B9DEFAA70E21CD84C3E99BFF,
	XRCameraSubsystem_TryGetLatestImage_mA9EEBC8B2BC3D9F1DC25513B25BFECA07CE45ABF,
	XRCameraSubsystem_Register_m471B8039B86BACC07C48A926544DF6C1415C1DA7,
	XRCameraSubsystem_DisposeImage_m45A6B78356D402A5528F4F023FA33BD395F1E958,
	IProvider_get_shaderName_mFB5F6E61A7112591AC17C13CB11552195D25ACC3,
	IProvider_get_permissionGranted_m05E6ECEFF8068CD11404F2C8C658074EEA0C821E,
	IProvider_get_invertCulling_mFAA294FC4DC9EFF1BFCA03BEBFA732F99D79906D,
	IProvider_Start_m65624A0EA48A4A41D9006C465CCF9CD9D5D642C6,
	IProvider_Stop_m047FB312180DAEE8120B88A6203D1A26E7025C1F,
	IProvider_Destroy_m40500F97E9831F72386920BF30B7E98E695CCD50,
	IProvider_TryGetFrame_m51A0B9C95A46E751D76CD1F03B34C8787DDF3FFE,
	IProvider_TrySetFocusMode_mA8FFA007764B1FBC79A7A85329C9D43CE932F047,
	IProvider_TrySetLightEstimationMode_mAA191807E6EDAA9848296BA3673E70642A25B645,
	IProvider_TryGetIntrinsics_m65D0B51511B516DB5B9D7E5AAA5B13CF62A22897,
	IProvider_GetConfigurations_m485213213F2035799374E29F1F0E05DD90373A9F,
	IProvider_get_currentConfiguration_m7F1319DB358EE8CA535D2E91C2BD16E91829A9B4,
	IProvider_set_currentConfiguration_m8C98EB142400102A3EF7008A03F0BD35C82C06D3,
	IProvider_GetTextureDescriptors_m79A1CA1945679FED96EAEAD126B23C6FDD8D4576,
	IProvider_TryAcquireLatestImage_m9D3691E0B7F486BFE505000AE6C48C8923196C3E,
	IProvider_DisposeImage_m7C58E8C74C44F8D8B43DFF4677064F51B92E7995,
	IProvider__ctor_m2709637AF8148063803D9E41C66EC75B4D713AC1,
	OnImageRequestCompleteDelegate__ctor_mD9BE79ADAAEE0A44060E897728EA37196AF5ED47,
	OnImageRequestCompleteDelegate_Invoke_m835EE17741E50584491577BD5B9061FB12EB95F0,
	OnImageRequestCompleteDelegate_BeginInvoke_m3102FE931688E2734845C0CB9FED184CA15B48AC,
	OnImageRequestCompleteDelegate_EndInvoke_m17DDA25840DD080989CEC9304CBD369D65AC82AD,
	CameraImageCinfo_get_nativeHandle_mD1CE0F2F44CFBA90188A3DD71970DC947D548419,
	CameraImageCinfo_get_dimensions_m55D78C58843F9F6AB11D634BBD0A4F17FB4CBFED,
	CameraImageCinfo_get_planeCount_m261902854A879D48561867655A57295A2B7E44D6,
	CameraImageCinfo_get_timestamp_m482C0B93963CCBB4953A1F375D5CF5862F758ED2,
	CameraImageCinfo_get_format_mE92FD0C4F73255F6CDCCDC1CACBCAEF1E5E5605F,
	CameraImageCinfo_Equals_mED7B9DECD14313D8D33586A50B22F2B3CFD81EC8,
	CameraImageCinfo_Equals_m5410D9B5B20D2CD87049EB0A89362E8777B02DF8,
	CameraImageCinfo_GetHashCode_m9E0E4A3A6091C9FF9D72E70AEB5820DD1A0959FE,
	CameraImageCinfo_ToString_mD15A4DF77EC11F2BFC6E4A94AC781CDDF14A7BB7,
	CameraImagePlaneCinfo_get_dataPtr_m0C0144ED8D52B8456B579FA31C3159780E5161D5,
	CameraImagePlaneCinfo_get_dataLength_m6CD246242017D808B54EC7E57C276CCA724AFA22,
	CameraImagePlaneCinfo_get_rowStride_m5594C13CC656157844AF9045800D49868C1A65B3,
	CameraImagePlaneCinfo_get_pixelStride_m83C1A6151625091937C7DD696C10C39CBE4C084A,
	CameraImagePlaneCinfo_Equals_mA5382E8A5C97636DBB423D83FDBDC6E6C4AD2F6A,
	CameraImagePlaneCinfo_Equals_mFA864D2AFF1CCC3F62ED3B107F6B50A8D66466CE,
	CameraImagePlaneCinfo_GetHashCode_mBB8FCE75397290CF3F692965D989A11927B16162,
	CameraImagePlaneCinfo_ToString_mD7AEB170BA8CAA9EABF9245793A993DB435584BD,
	XRCameraSubsystemCinfo_get_id_m8C63E6A41979D1A0201ADACA9F984088D06F488C,
	XRCameraSubsystemCinfo_set_id_mEA5E0B21781D8AAF0FB30E9E506AA4D7C392E2A8,
	XRCameraSubsystemCinfo_get_implementationType_m6F9012A33C47D026F5070C2F64B444A93709B5C5,
	XRCameraSubsystemCinfo_set_implementationType_mAEA2151AEC9F31C5726795200B63D4BA53F2721E,
	XRCameraSubsystemCinfo_get_supportsAverageBrightness_m1DDB2CC0BE14F5C77E7ECC2FF1BA5C712C08CCA5,
	XRCameraSubsystemCinfo_set_supportsAverageBrightness_m0851BD298973A23FCE8D87B5B8AB389562D255FA,
	XRCameraSubsystemCinfo_get_supportsAverageColorTemperature_m16E2A9E1B564001C9A7D2F5EA20C0101DC97218D,
	XRCameraSubsystemCinfo_set_supportsAverageColorTemperature_m427080856A860B42B3FC21139B990F19BE0AD87E,
	XRCameraSubsystemCinfo_set_supportsColorCorrection_mFC3AED27787017D69ABA73FF60D1E20DDF5E674F,
	XRCameraSubsystemCinfo_get_supportsDisplayMatrix_mE79B1E6401467F3320D1AFF9678041FC2B5EC36C,
	XRCameraSubsystemCinfo_set_supportsDisplayMatrix_mB5BF43F49F4D64AA3DFE174574D386D99A96F92F,
	XRCameraSubsystemCinfo_get_supportsProjectionMatrix_m820DB18EBD58445C4976C05E25C3E2A8B04B388E,
	XRCameraSubsystemCinfo_set_supportsProjectionMatrix_m4CC64D264746A394D8186CCDD583CFCC637C8E66,
	XRCameraSubsystemCinfo_get_supportsTimestamp_mC9A551933743EC5171B9EAC41D5222067B56CAEA,
	XRCameraSubsystemCinfo_set_supportsTimestamp_m901DA9F41D9CEE062F7A054738E5382E2A825F28,
	XRCameraSubsystemCinfo_get_supportsCameraConfigurations_m125606F11D5A7521099E70525019DA50BBB4334E,
	XRCameraSubsystemCinfo_set_supportsCameraConfigurations_mCCA48E46B902EEABAA94FB7A2A668097E06D4906,
	XRCameraSubsystemCinfo_get_supportsCameraImage_m247ED6F4E54DC010F7506E353AD06E5D62EB4445,
	XRCameraSubsystemCinfo_set_supportsCameraImage_m9B592584A2C27917CC80AB290F0A2600FD275951,
	XRCameraSubsystemCinfo_Equals_mC0F2554FBD6C4944FE9956C0843718D3D423D86F,
	XRCameraSubsystemCinfo_Equals_mDC6FD25003FEA123FF4D83BAAED03854B0DF2B15,
	XRCameraSubsystemCinfo_GetHashCode_m41B5C3CD9B0D9101488D0D63E8557909BE239EE3,
	XRCameraSubsystemDescriptor__ctor_m218E5DC6846EF28CD00B48704E59A327949C3CAE,
	XRCameraSubsystemDescriptor_set_supportsAverageBrightness_m90F7E1B3733B5D1975217E7FBEEFC2EC6DFDFBF6,
	XRCameraSubsystemDescriptor_set_supportsAverageColorTemperature_m617CC63B166A095A8F873C73BAF39F86D579CD69,
	XRCameraSubsystemDescriptor_set_supportsDisplayMatrix_m67FC7675BE452E50E68679516708CEF60B36C061,
	XRCameraSubsystemDescriptor_set_supportsProjectionMatrix_mC5EBFF2790C6672050DD1781F0026AA82CDE0A3C,
	XRCameraSubsystemDescriptor_set_supportsTimestamp_m675F2A889C4A694BB5F72569DC697BDE0455B3E1,
	XRCameraSubsystemDescriptor_set_supportsCameraConfigurations_m2EFBEC5D6FC11ED5D546B7B75090B56A960F57F7,
	XRCameraSubsystemDescriptor_set_supportsCameraImage_m84810E8929E44D5C62EEC38D5AAA7ED2F8434979,
	XRCameraSubsystemDescriptor_Create_mD1CD2F7DDCCF8702EEDE082BAFCAFAC8ECE3DEA3,
	XRDepthSubsystem__ctor_m24423F4A0EF54A1EDA98684496E5973E192C097B,
	XRDepthSubsystem_Start_m68450EB9B783A9BB64B1B5B42083FDFA6B5709DD,
	XRDepthSubsystem_OnDestroy_m121B6BB3B5074B632F3439D0888CA6AC728BBCB6,
	XRDepthSubsystem_Stop_m4E407A66FD6841B8AC70B906B5358F0C85EC85C9,
	XRDepthSubsystem_GetChanges_m7B42781E43AFE126FBE6DFE2C8A3AF76DC53EEB3,
	XRDepthSubsystem_GetPointCloudData_m49BEF4047DEED6FC3E885AF893387ED347971BB8,
	NULL,
	IDepthApi_Start_m4FFE14EF1808D84F83A805786BB5D17F0F66C779,
	IDepthApi_Stop_m9D7185DBB1129C64160889FE3D569FDE887F2E44,
	IDepthApi_Destroy_m04DAAED019DFD454F222CE747809E1D95765D662,
	IDepthApi_GetChanges_m5B950E3E0BE9BEDF1172AB25C46F8F7BADC40F18,
	IDepthApi_GetPointCloudData_m92DD9FE13BBC60B6686809061B6809BC6B8622CF,
	IDepthApi__ctor_m4B44FB5141C363FF42F5703404626927B89C222A,
	XRDepthSubsystemDescriptor__ctor_mA74AF85046BBA65B77FA333055F82C2E3F6AD05D,
	XRDepthSubsystemDescriptor_set_supportsFeaturePoints_m7974B23484377F9C2698B1981BCE830C7BEC6C73,
	XRDepthSubsystemDescriptor_set_supportsUniqueIds_mF89B9E95F36EB311A0C512A7C20BD7CCADB1489B,
	XRDepthSubsystemDescriptor_set_supportsConfidence_mBC66AFF12E25475139457FA6DEB1093A57065068,
	XRDepthSubsystemDescriptor_RegisterDescriptor_m9F40B303586BE45F7AACB8B0AA408D242B34F4EC,
	Cinfo_get_supportsFeaturePoints_m167B1DCD4173C6CB2AAC5E8FA35759AE4D4BF385,
	Cinfo_set_supportsFeaturePoints_mB3633125ACFBA430C6EC66F3FF8E5BFEC72EC360,
	Cinfo_get_supportsConfidence_m2AB397C1B754341CC18399C796866FDC8FF597F0,
	Cinfo_set_supportsConfidence_mD7DE3DC81C6783C66AAE15A10301DE202520605C,
	Cinfo_get_supportsUniqueIds_m72987F3CF2CB4081D548EF34F13876D607CBD807,
	Cinfo_set_supportsUniqueIds_m416FF5EC15306E37DC3436BBB02B4998D64B62C4,
	Cinfo_get_capabilities_m6B6CF2B89609939E9E7313A3AAD56C543E4445AB,
	Cinfo_set_capabilities_m2FFCFE5025136EFB71DDF302E4B81E9287D9A39E,
	Cinfo_Equals_mDAB1544CED67FAD91D9F273A32EBD01B18EC87ED,
	Cinfo_Equals_mF847824268A42C4CC910934F8454B95DD30C4C1E,
	Cinfo_GetHashCode_m114731847A78B883C2EC5BC32DE8C412D4684A58,
	XRPointCloud_GetDefault_m217CAB3C5FEF4FF479DFDE0D019B806EB2BB2FE8,
	XRPointCloud__ctor_m049E9ED3F776B9F25BD217D0B2D714464A5F6C9F,
	XRPointCloud_get_trackableId_mA394197EAD026665FC02A1118CBBB46FF6873EF1,
	XRPointCloud_get_pose_m09C2DF1AD7F1220B547BD2EBCCA6E35F85A87EB0,
	XRPointCloud_get_trackingState_m0CE633649849B59E4AEA7875F62F0B34CA61FC96,
	XRPointCloud_GetHashCode_mC2934047C0D733F372E51C52E2837DAE9E13259C,
	XRPointCloud_Equals_mC56FA4F7B07E704C529E144B073920A79E599CC1,
	XRPointCloud_Equals_m66CC3D8FEDF0226F8D4F0B6574449E334D849F57,
	XRPointCloudData_get_positions_m2BDA572054D639DB35E9FDA3D15AEF3B7B39D40C,
	XRPointCloudData_set_positions_mC99C23E8AE61A1A3333C1A2F7E0F9DBD6C9F771C,
	XRPointCloudData_get_confidenceValues_mE672D23FB62CF42CA475CF82A678DA16729D618C,
	XRPointCloudData_get_identifiers_m6F0A88EEB7DD58C82662346098A4E20CA111C479,
	XRPointCloudData_set_identifiers_m3CDA83EC60EC5AAB982B3C5E0F9AC9E94D41992B,
	XRPointCloudData_Dispose_mB2C769355B6385CE3F8F47E720087F7B7C726259,
	XRPointCloudData_GetHashCode_m0ACAD17C55503EE73995A9DF2A1110FBEF0145DC,
	XRPointCloudData_Equals_mBDA7D40FB197B84AF2E8EB4C5CF6015D09E74357,
	XRPointCloudData_ToString_m91E31F94CB96601D2BA320BE4B993728D1A8DE20,
	XRPointCloudData_Equals_mBE92CAA314FFE99803718F2EABEA3A1B7AE8A99C,
	XREnvironmentProbe_GetDefault_mE837280E7ECD29A7313DE019DD7A4FFEA0FECE3B,
	XREnvironmentProbe_get_trackableId_m2F7F8DCE954C099E60807742B6A4B27DA2F30085,
	XREnvironmentProbe_set_trackableId_m8E02AF983995D8D544C83C7F170989AFABC13AA2,
	XREnvironmentProbe_get_scale_m2346CDBC2FEF9CC85761BF11829E10D2992E0004,
	XREnvironmentProbe_get_pose_m2CF6BF7E554B1225E99947B620D2C029499E7996,
	XREnvironmentProbe_set_pose_m19121B0DDCFC795C1541A378F506B899BD2F8D0E,
	XREnvironmentProbe_get_size_mCFA1F9B8C8BE0138585D2BE4C2059E0153133C11,
	XREnvironmentProbe_get_textureDescriptor_mA926C0701B8D774E5C0A1F51DC54546CB71ECFFA,
	XREnvironmentProbe_get_trackingState_m48BAAE58BF028382D00D5F49BA1F6023E53B0AF9,
	XREnvironmentProbe_get_nativePtr_mC9CB253F77A64FCD5D1ADC64590E91A793DC8D66,
	XREnvironmentProbe_Equals_mABEF3AB481CB2191DE4C790E3A5A245DE1D347D0,
	XREnvironmentProbe_Equals_m8DFEE5B51820BCC164FDFA7F5F4996074A9C5170,
	XREnvironmentProbe_GetHashCode_m638CB5F2CF52A8ABA8778B6B5EB7F13E57CD7B1B,
	XREnvironmentProbe_ToString_m8B856D8579587102C1F500A2F2180361CD7770D2,
	XREnvironmentProbe_ToString_m20D40F2265F40337C75C536779CC935885222B19,
	XREnvironmentProbeSubsystem__ctor_m7A3AE7794DA58FE53C8EE9F47F8B84F3D5DF47B6,
	XREnvironmentProbeSubsystem_get_automaticPlacement_m4D1AC97BF886DFA4851A85797CE2FB34246BAA43,
	XREnvironmentProbeSubsystem_set_automaticPlacement_m1B62556B219E960E694D903D797ED5D9E1E998C4,
	XREnvironmentProbeSubsystem_GetChanges_mA100F4697822F10AAAD8506683629996239BD7C5,
	XREnvironmentProbeSubsystem_Start_m697A7A5789BFE9B17A0668268E97D0245ED12306,
	XREnvironmentProbeSubsystem_Stop_m45CA3860BD9B913D3CA2FE9905CF0232FBD2B95F,
	XREnvironmentProbeSubsystem_OnDestroy_m39FD526B93D9252DED22732011B818F0DFDC0E24,
	XREnvironmentProbeSubsystem_TryAddEnvironmentProbe_mC5653C998195D72D7A048BE3F4CC07BE81B6438B,
	XREnvironmentProbeSubsystem_RemoveEnvironmentProbe_m045B4A7307B5CBAFFEF3DBAFC78DF05F551B6801,
	NULL,
	XREnvironmentProbeSubsystem_Register_mF52AA36EB4EAA59C932C43E45DA567A3EA6D55FD,
	IProvider_Start_mC5843EBBA0BF44B4631E433F6BB73D4013EEE473,
	IProvider_Stop_m6CCB471415A9BAF0B24863D71F952EB76C707B29,
	IProvider_Destroy_m391A202D94ACD3C96BC9483C5213FFB3D40F9B84,
	IProvider_SetAutomaticPlacement_mE563FEC72D1C1CCE511C20F92AB2804F06BA8AB7,
	IProvider_TryAddEnvironmentProbe_mF5F8B5978164B3CD13F5B0542DA04EE650FEB1F2,
	IProvider_RemoveEnvironmentProbe_mA00B3FCBDE45170B3BC9A12E835D8B11299BC1C0,
	NULL,
	IProvider__ctor_m5A111BAA2E41E225EEF73921AB5C4A279E7AC337,
	XREnvironmentProbeSubsystemCinfo_get_id_mADBD2988DA174EE595955008050CB74CB19C4882,
	XREnvironmentProbeSubsystemCinfo_set_id_m65F71E8D97413215944F75C52F6F9F2088644E24,
	XREnvironmentProbeSubsystemCinfo_get_implementationType_m2AAB6F75B1588A46DC09034244ED3C4CEF0BDD22,
	XREnvironmentProbeSubsystemCinfo_set_implementationType_m5CDE58834E022AEB4B9E6FD826D2A6140D3D1B3E,
	XREnvironmentProbeSubsystemCinfo_get_supportsManualPlacement_m38F2FBCF91D735F7ACD339C2C6FD013B62C0DC0A,
	XREnvironmentProbeSubsystemCinfo_set_supportsManualPlacement_mF3AA42AAE10CC81DF831404F415BD34694B08C59,
	XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfManual_m1F2CFC423F2D37975488C822E76CD175A589E8AE,
	XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfManual_m200BBBC11A1580CAA151ED498A8B24E27BAB646F,
	XREnvironmentProbeSubsystemCinfo_get_supportsAutomaticPlacement_mFA0D6FB52DF9C8ACCF6DC3B9C8A11FDB37877C67,
	XREnvironmentProbeSubsystemCinfo_set_supportsAutomaticPlacement_mBC81F9BC67A3FF73D0EB679BEDDFE1D3DA918582,
	XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfAutomatic_mF9F46814201602562ACCF4E7AD63E20B0B7C1645,
	XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfAutomatic_mC43F8E59BF70D73AC0EB8EE5A9B2D6F92966B3B3,
	XREnvironmentProbeSubsystemCinfo_get_supportsEnvironmentTexture_m444E17E86B43838E6BC1279C1793F40E21E0C210,
	XREnvironmentProbeSubsystemCinfo_set_supportsEnvironmentTexture_m9C630C891056E5E1187AA2433DDC8D7E0F3FF662,
	XREnvironmentProbeSubsystemCinfo_Equals_m87EE67B01CB7C502E31CBBB818CA4C6D12DAB809,
	XREnvironmentProbeSubsystemCinfo_Equals_m79E504F27913015A7C97F1E64E16A02DC1C4D73E,
	XREnvironmentProbeSubsystemCinfo_GetHashCode_mFCD281E4EF9FD74889EF9754EF53341C79988F1B,
	XREnvironmentProbeSubsystemDescriptor__ctor_m91F1A02FF56AC51ABA801B94687E393C9AB82F74,
	XREnvironmentProbeSubsystemDescriptor_get_supportsManualPlacement_mE198AF9A486EED050CB81241F91BE6F7DA074632,
	XREnvironmentProbeSubsystemDescriptor_set_supportsManualPlacement_mFB3D77F497F4DF6722A1B7C66DE6F4D9505763A7,
	XREnvironmentProbeSubsystemDescriptor_get_supportsRemovalOfManual_m1FCEBFF75F2F521CC101940F5F8A1022B45B64E1,
	XREnvironmentProbeSubsystemDescriptor_set_supportsRemovalOfManual_m4E909B728829BACB3845873C9D0A2F41ED14D531,
	XREnvironmentProbeSubsystemDescriptor_get_supportsAutomaticPlacement_mA944EEE25188EC82048232D81023516D1298080A,
	XREnvironmentProbeSubsystemDescriptor_set_supportsAutomaticPlacement_m2374B2B4F3CF3959F87F0DD13C8EE4895BC9AE25,
	XREnvironmentProbeSubsystemDescriptor_get_supportsRemovalOfAutomatic_m25ACC17AE99D2D056B11C7BAD34981D0A7444550,
	XREnvironmentProbeSubsystemDescriptor_set_supportsRemovalOfAutomatic_mECFC5E6081D92D77590B981F055E86AE6557873B,
	XREnvironmentProbeSubsystemDescriptor_set_supportsEnvironmentTexture_m31201D249DB3E60DA61A739A66D19BEFB89B4B7B,
	XREnvironmentProbeSubsystemDescriptor_Create_mB7EBDC47BFF343F6B7B621B5D5E5EE9600EF767E,
	XRFace_GetDefault_m769DBA5C20BEF8E9F2C7B197AFBAA92527946E3C,
	XRFace_get_trackableId_mC7AA3B622C6B16A9E8B5A3BEA524C7ED54A6188D,
	XRFace_get_pose_m3792AF11CBB24361529B7291ED46B9DD2970AC54,
	XRFace_get_trackingState_m29CA0D89354ACC65EF8A18C09201BCBB3F732E3B,
	XRFace_get_nativePtr_m1EDCB59CD67423A2951DBA0DD0C98AB848183F06,
	XRFace_Equals_mB68B812AD74B02F7B8B82DE62EAE30146238DD3A,
	XRFace_GetHashCode_mB3AC95DBCA3308827747473BF3A6044FCD2DD7B0,
	XRFace_Equals_m1FF3F979A7F289C6CA77DD3290F4A50CAF889ED6,
	XRFaceMesh_get_vertices_mD6CE3C180851CA10DE10A7B6C7BF0817E73609E9,
	XRFaceMesh_get_normals_m8CF3D0395943001F6A681154DF7A75FA045CE6AE,
	XRFaceMesh_get_indices_m7EA9FB6B6CE3484262F74546455CB08BA5B5B00D,
	XRFaceMesh_get_uvs_mECB939F9E262D22AED47311D6985116FC0CEEA9B,
	XRFaceMesh_Dispose_m3E7A416718B532DFD7D100D5D0F1F3A9AED96F7E,
	XRFaceMesh_GetHashCode_mBB4E68260D980EAFD29333F2B0EE9B10FAA8040E,
	XRFaceMesh_Equals_m466E0A0D30D307B956CB677D1103AD4F4C86E3BC,
	XRFaceMesh_ToString_mC0D08232897F11B687916F6B1975927411A783B3,
	XRFaceMesh_Equals_m7F90AA84BD56C74C8C6472CE390C706E8AB8120D,
	XRFaceSubsystem__ctor_m69A0FE81F7D83567E0B1F70FE0CC9B37AE5BB7EB,
	XRFaceSubsystem_Start_mFD2DC7D5B1E73266D3A752056B6E7771DCB16AA6,
	XRFaceSubsystem_OnDestroy_m9D447A629CA2CFCDBD38F8B38C1EC42C747BA029,
	XRFaceSubsystem_Stop_m3761285C8E1E72D253F42E390921B3395515626B,
	XRFaceSubsystem_get_supported_mF71AD1931835B24E79D1F762CDC9A5B12B5753CA,
	XRFaceSubsystem_GetChanges_m7A0FF036C2DF93EBD2B39EB65AA87335601C9AA7,
	XRFaceSubsystem_GetFaceMesh_m4E1417F6B00F84DB66EFECFE29A8FA2D3ECBBD12,
	NULL,
	IProvider_Start_mCC319513AA6093EC38BEF32EE228DE2A0ECF86D8,
	IProvider_Stop_m5773A5F49083C4FEE1A84F701D8D20C33A1F8786,
	IProvider_Destroy_m510EDD2FBD76DB2082AC958420287E544742299A,
	IProvider_get_supported_m488CA2BFBFFF8C9C6E8D1AAD4C0CA7324AD79BB4,
	IProvider_GetFaceMesh_mA90B7C66A3FC5259F31005616C40430E75E32F88,
	IProvider_GetChanges_mB14296C0CDA96C92523BA2C7DC1927A0E8B20C8C,
	GuidUtil_Compose_m23689A8CCFCDF3904D23BE11760E58DC662E35C1,
	XRImageTrackingSubsystem__ctor_mEE7E27E4FDC18721F1D9CDDAAC8FFAACC782D4CF,
	XRImageTrackingSubsystem_Start_m91F8C046524DE309DC7C3A7886D570BE35C177D1,
	XRImageTrackingSubsystem_Stop_m23F39DDCB8A8DEEC9D2EBCCC3676D248F263BDEF,
	XRImageTrackingSubsystem_OnDestroy_mC6226FC0BE61C75F945FE96D36C4F983A29D2477,
	XRImageTrackingSubsystem_set_imageLibrary_mF42A5F9DE0F684462B4113BAC91A2FC86F89E069,
	XRImageTrackingSubsystem_GetChanges_m84DB25AC8DB44AE84050A755C823960BABC2CFA0,
	XRImageTrackingSubsystem_set_maxNumberOfMovingImages_m59B8A966406E06A35D55565FE6628158006A32BB,
	NULL,
	IProvider_Destroy_mA90D2D8E662D193A6E53790B1C6C6F2C036EF977,
	IProvider_GetChanges_m8A5C85F9184615748807A531CBB5C49CC8FE3648,
	IProvider_set_imageLibrary_mDF0EA87D8EDDE21B17DEAEE4E914200802E35DF2,
	IProvider_set_maxNumberOfMovingImages_mDFC808999A3DFCFA90D6EFFF36A39451CBE371D3,
	IProvider__ctor_mE3F278B6BD96AAE5265BB88C2F2F7B0AA5C1D210,
	XRImageTrackingSubsystemDescriptor_get_supportsMovingImages_mB0B37BABA6FEECB44860DFFD2A388BBFF48F4F20,
	XRImageTrackingSubsystemDescriptor_set_supportsMovingImages_mB13C74324DB6B9D7E3A69826726BFEBA2E403D69,
	XRImageTrackingSubsystemDescriptor_Create_m1049DA7C21F27833846D6C6E699DD2DA964522A9,
	XRImageTrackingSubsystemDescriptor__ctor_mCD5CADD6B43C92838FA1A87926CC23D53A9715C4,
	Cinfo_get_id_m146AEBFC1F34906EF7CB853DB90B28AAA8449D98,
	Cinfo_set_id_mFA87FA52172846CBD4587F2E207D65A097B842E6,
	Cinfo_get_subsystemImplementationType_m3715508999808DEC6A812A1D228A2472ECEA48C4,
	Cinfo_set_subsystemImplementationType_mBBC866C6F4207A69F7351857F66D683E0EAF8FAF,
	Cinfo_get_supportsMovingImages_mAB2286D926C059743ACE6E6DABCFAAB7939AC80F,
	Cinfo_set_supportsMovingImages_m2898F758BCCE6DDC222E828D855CEFC89C58EF2C,
	Cinfo_GetHashCode_m30FEF663E95BCA50174085CAE5EC3E6B207CF094,
	Cinfo_Equals_mB17F6F32907C286C2B660B6E9E1BCC20A651F941,
	Cinfo_Equals_mB207ED3E11BCF5AEC1A56818611A0082753E8DBC,
	XRReferenceImage_get_guid_m646CE46068C4BA601BC23772D3D807D18836B80C,
	XRReferenceImage_ToString_m6BC181F25F28FCFB70B2035D7AAEFB1E6DDC6FAA,
	XRReferenceImage_GetHashCode_m5178E851EB53F2D106ACBB7C8330F889E1581C09,
	XRReferenceImage_Equals_mE3432D5A1D715669F9AEC65EB524B925549F4726,
	XRReferenceImage_Equals_m001D4B708546DF448C4243079684F6FDEE76FBC6,
	XRReferenceImageLibrary_get_count_mFC2EABE3C3D8966005C0AB2E74836BDC998995DD,
	XRReferenceImageLibrary_GetEnumerator_mE48D64D91D797FCFFA8D582B720C68F14D521710,
	XRReferenceImageLibrary_get_Item_mD672D0FB305F5209E867F2361EAA542524E3A199,
	XRReferenceImageLibrary_indexOf_mCF228BB01B5658DAD82FF40FE4B0086C2FE2AC52,
	XRReferenceImageLibrary_get_guid_m101D8AFC1E328EBF5DBDED74F7EA8863A3468418,
	XRReferenceImageLibrary__ctor_mF39B0A6A9C1B2777ADD98CAFF08762B601EA5691,
	XRTrackedImage__ctor_m0D4DB0925EB1FBEC466A9D19C627F747A400408F,
	XRTrackedImage_GetDefault_m5F66138594FB5852F148CD6F830086B62B906B84,
	XRTrackedImage_get_trackableId_m6EB6DBACC95E5EE2AFEE3CE421F4C123F32E9CB8,
	XRTrackedImage_get_sourceImageId_mFEBFE1A21956E0CBF6828407DE0F2209610BF60A,
	XRTrackedImage_get_pose_m0566E087CA2DC99DF749E80277510C61DCF13186,
	XRTrackedImage_get_size_m746034D0E2FD28C9E48A90965E4FCD9137988906,
	XRTrackedImage_get_trackingState_mA7177B042E8F9F9B584582970BC5FF0377CE94DB,
	XRTrackedImage_get_nativePtr_mE90A65D3EDE7F0190F36BD4BDF2E06FEAD113DAF,
	XRTrackedImage_GetHashCode_mFF30CD39BC82F7A636BF9E0ACF96967C46F07B5D,
	XRTrackedImage_Equals_m626B512ECA4BFBB14918EF13969F8789C3A8A069,
	XRTrackedImage_Equals_mF94BFA9B373C9899F29EBD1F01A15ADA2D6E47AF,
	NULL,
	BoundedPlane_GetDefault_mDC1F2DC14F5C53295558B28FA102CE5CD774660A,
	BoundedPlane__ctor_mC6A5A4A0E3417618DAC201CE213664B9791BFACC,
	BoundedPlane_get_trackableId_mD2A3DCDA61898E637ACAE0A0F4A832931378071A,
	BoundedPlane_get_subsumedById_m77895AF3F9E056F4816CB9177209586E98967889,
	BoundedPlane_get_pose_m30B084D1F307CB46894BE4FAD448788001A0F0DF,
	BoundedPlane_get_center_m1AB8ED2CBDC8F4A85358F98A8DD832436818505F,
	BoundedPlane_get_extents_mC0F005CBFDA55C68EF9CC52FBC8C0FC07854CBBB,
	BoundedPlane_get_size_mC49BFA58CD7CE92C2470C82F30EA6F8A4F38B854,
	BoundedPlane_get_alignment_m85C46D81426435F0B9399ABAF80AC5342A2F7137,
	BoundedPlane_get_trackingState_m9B6467E433564ABE4BED9328F7B23AB05905B008,
	BoundedPlane_get_nativePtr_mF0C7299B1CD00C40972DE1BE13C411594A59D361,
	BoundedPlane_ToString_m0EE069A6B9579B564EFD59C300277FB7D824E7D1,
	BoundedPlane_Equals_m74FDA713E8EBBF9256546B4B04A1CCF214ED7D8E,
	BoundedPlane_GetHashCode_m39BDD70727BE818F86E2DEEF4E264FE13368B637,
	BoundedPlane_Equals_mF06B1E1B2C53F0BF0FA541CD4828DFD16E8D789D,
	XRPlaneSubsystem__ctor_mC6CCE81B1FE634A34E37D1595EC6189A6D5B28E1,
	XRPlaneSubsystem_Start_m5B5037483FC16EC86C70B67994234236932EDC18,
	XRPlaneSubsystem_OnDestroy_mC839A429BEE5C7B303DC6C463A7F5DCE0E827639,
	XRPlaneSubsystem_Stop_mBA856FFA3F7680F9B86A915CD28E457369A1975B,
	XRPlaneSubsystem_set_planeDetectionMode_mC7B2B3A8A0FB7853FBA4227F3A4DFD8A155E14DD,
	XRPlaneSubsystem_GetChanges_m0487B4AE994BA3CE0DD7D9FA365856F4C9F5710B,
	XRPlaneSubsystem_GetBoundary_mB724A7CF46B3AF8C7E01A0A56854F46C595314CC,
	NULL,
	NULL,
	IProvider_Start_mB6468DFD7AD773660A0D1C4D65AD4E184DBE66FE,
	IProvider_Stop_mB7E116A8377F666DBB09C768E7A33B3FAE4FE888,
	IProvider_Destroy_m409156BDA5D114282D0DCC2491A3BF2586B8DEBF,
	IProvider_GetBoundary_m821E610D30EFBF9DF2ABAE2DF19DA3334D7EEE9D,
	IProvider_GetChanges_m565968265ACB120BC15D83E38E4C1AA87796AE6F,
	IProvider_set_planeDetectionMode_m56414E4307411B77962ADC47BBB1ED641D339367,
	IProvider__ctor_mB2FE77AE4D29B62B76F877BB9761B2CFFBF5A46A,
	XRPlaneSubsystemDescriptor_set_supportsHorizontalPlaneDetection_m834174F0D747E2D8D8C34EB20D879FFA3E607849,
	XRPlaneSubsystemDescriptor_set_supportsVerticalPlaneDetection_mF2E7FC43C5A6D048BE8A13F7824CC76543C28AB4,
	XRPlaneSubsystemDescriptor_set_supportsArbitraryPlaneDetection_mCC9B359F247DFD0928E3D968CBD7713C744403F6,
	XRPlaneSubsystemDescriptor_get_supportsBoundaryVertices_m455A7A695D7C4F238405B4B50472BE0A8118F611,
	XRPlaneSubsystemDescriptor_set_supportsBoundaryVertices_m79DA28E3ED06B14E1FEC3EEB72986EA1A420A15E,
	XRPlaneSubsystemDescriptor_Create_mE7A8E8E49F7EB078CE4D76C9F0D883634157EC9C,
	XRPlaneSubsystemDescriptor__ctor_mFF66A6FD68D7DBC951F675E15E230CEADC37B42C,
	Cinfo_get_id_m68FCB4621B357B690E4C43846221E241455A1D99,
	Cinfo_set_id_m9211F9ADC4DCFA1AAB5AA9F662EE6510D6FE01EF,
	Cinfo_get_subsystemImplementationType_mF924CAAE546DCF3AB899CD4ED8AC2B75B4B0706C,
	Cinfo_set_subsystemImplementationType_m74075897385685A6E0753F2EE29CD77A90A22E6B,
	Cinfo_get_supportsHorizontalPlaneDetection_m0FB9CC965AD3A99988175C8D1669D341BFB72214,
	Cinfo_set_supportsHorizontalPlaneDetection_m6BA5B6FD1C2FDF236AEE15957FD1F1837C394304,
	Cinfo_get_supportsVerticalPlaneDetection_m3C7470BF80CEB7F507540383C2BB02A5B2201AAE,
	Cinfo_set_supportsVerticalPlaneDetection_m386B3816E8C1538AB58318D55D9C64D1113C1B3B,
	Cinfo_get_supportsArbitraryPlaneDetection_mC75F86CCCBA2ED8AD6283EF458F3897CB78DAB3C,
	Cinfo_set_supportsArbitraryPlaneDetection_m625EDF8616A904C1D2C3B9DB1B52A28A0D3EAF06,
	Cinfo_get_supportsBoundaryVertices_m6E514112FA9F617953047AF6C2CD98895D587E84,
	Cinfo_set_supportsBoundaryVertices_mFC986523905272E58728731CEE06B47DD4ECAC3D,
	Cinfo_Equals_mE08723A87EF2FC94FD64BDD93A176F3497518F7D,
	Cinfo_Equals_m3B3C974BB1055B352CAAE2961A7374DA559049F8,
	Cinfo_GetHashCode_m9F3BA097C011CC6998676138C52EF6306C09BBA0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	XRRaycastHit_GetDefault_mB7D3A8CFE226FD38DDDA4B1C2DE3162FD3B6BE2F,
	XRRaycastHit_get_trackableId_mAECCB1BE08FB0B5A48CB27D955250FE2068492CF,
	XRRaycastHit_get_pose_mE0B0A754E818C6FF3675A41CA95185A3E608C8C3,
	XRRaycastHit_get_distance_mCD38ECEDD0FA6EAFEFEC71DB7EE3CF1B82B5CEFE,
	XRRaycastHit_get_hitType_m52BBF5DBDE1B3E7E01571EE029F68EB29E240DA6,
	XRRaycastHit__ctor_mFE2DF7F0A38F4507B3C5684B78F0694266BB76B5,
	XRRaycastHit_GetHashCode_mB0A7A65C634E1CA9C70DC17D2B31A6E082D349EA,
	XRRaycastHit_Equals_m80D2CC8EEC73127B553C601D9B6A3CEDCFBCF862,
	XRRaycastHit_Equals_mD3774307DB6D9200AE2C4703CF2CB2D90616051C,
	XRRaycastSubsystem_get_running_m83B87B0DE729E3A19DB0F30AC0A6918B61E4F0EC,
	XRRaycastSubsystem__ctor_mD6FC049FC72B869A2A78EE56093819D989AD4021,
	XRRaycastSubsystem_Start_m088682FAFE45FE3605823014221A92F1FC2459DF,
	XRRaycastSubsystem_Stop_m296F12121C158E681FF8ED1575CA06D6D62D039D,
	XRRaycastSubsystem_OnDestroy_m6F220962FC615CA40238CDD470B5568DE88C8E02,
	XRRaycastSubsystem_Raycast_mD6335AB75E7AD15295138215F593EAB71754E6FA,
	XRRaycastSubsystem_Raycast_m46598C4ACA7D6AC6B6DA53A92ED1349F327EC6BF,
	NULL,
	IProvider_Start_m07598B8B5FF4458B29EDB05DF332F71038D16045,
	IProvider_Stop_mD42463E875792E02D298C03F49E4CF5410880909,
	IProvider_Destroy_mDD7686E47514FA1FB4D69FE55A66A5795514EB72,
	IProvider_Raycast_m70674ED49690ED8304135E4E57023D6F4EF031E6,
	IProvider_Raycast_m82D63262EEC3A07B14B897B9D2549340D4FB4E82,
	IProvider__ctor_m6ED48B63231AFC2B55F4CAD72EAE1D9C624165DF,
	XRRaycastSubsystemDescriptor_get_supportsViewportBasedRaycast_m58C8F3A796EEB498A31A9BEE387A37E935121388,
	XRRaycastSubsystemDescriptor_set_supportsViewportBasedRaycast_mD39B9FA29B589E0DF23DE7A21012058C0505402C,
	XRRaycastSubsystemDescriptor_get_supportsWorldBasedRaycast_mA8C8F4A9E9B0B85E6BE432488CBCF9A97A5E5F4A,
	XRRaycastSubsystemDescriptor_set_supportsWorldBasedRaycast_m4D35B87B7D4284B470A41F46E5261FE953B75035,
	XRRaycastSubsystemDescriptor_set_supportedTrackableTypes_mE17AF0F588A87E741576518F5C851286937BFE27,
	XRRaycastSubsystemDescriptor_RegisterDescriptor_mA164B987D51AD208D957753220E5B1D1A2DB0650,
	XRRaycastSubsystemDescriptor__ctor_m69D7B8410396340E3D54A19B7EE73B4D24562C3C,
	Cinfo_get_id_mCCD575E1E7E6E7E7166A4B7F0AF9E7F023FC3FCA,
	Cinfo_set_id_mDBC061879B3E989FF064E7E31CFC85ACD142199B,
	Cinfo_get_subsystemImplementationType_m65BDDAA14217AD17F0928287A8428D7B33A3634F,
	Cinfo_set_subsystemImplementationType_m9670297F5DC91608B606E2B8A7E4C2643236D65A,
	Cinfo_get_supportsViewportBasedRaycast_m8D0A14E0E43F99FCB89A3D87F954D657ADC0C514,
	Cinfo_set_supportsViewportBasedRaycast_m42B64A1095C52F16217EBF1D5ABFD7353DA35233,
	Cinfo_get_supportsWorldBasedRaycast_m327E8B0EE9C3103FBF490CA62FE5E1A51EF62C9F,
	Cinfo_set_supportsWorldBasedRaycast_mBF04DD8B3208A7D9C98419FEDC8CB012F7253DF5,
	Cinfo_get_supportedTrackableTypes_mCC57E28DFCE93ECA772B1DE2E3E49AD030D79424,
	Cinfo_set_supportedTrackableTypes_m13138A57079E692472B33A4B216D5568852BE652,
	Cinfo_GetHashCode_m155C084A788BBE99BBD85B2B06D7CC74DF76D636,
	Cinfo_Equals_m434EDD8246018244C8CBBD881D91CCDBF5437EF6,
	Cinfo_ToString_m2DFF2387C99F28931C7E802878564A3694D51453,
	Cinfo_Equals_m21B31135C0E69990482BE0286436FF51748CF92B,
	XRReferencePoint_GetDefault_m2C8B9BC8378BD9F7C636DCD1C90D4793F9C80BF8,
	XRReferencePoint__ctor_mD4597D205E31CBA88C523E616E7A45D78C95BF3E,
	XRReferencePoint_get_trackableId_m6D53542802F2444CE58861B8868274F9A8296D88,
	XRReferencePoint_get_pose_mA4320629B8C7AE23D97FCD8E2C5FB9C9FB6AED9C,
	XRReferencePoint_get_trackingState_mBA0DB4050B734039D22D0ECF69CD6E8896DF52B8,
	XRReferencePoint_get_nativePtr_m51D5CA830D54C1D67656E683BE81A53D479E71A1,
	XRReferencePoint_GetHashCode_mAA9EDDD897E7DDD88AA10A7B9E10FC2025E4F7CA,
	XRReferencePoint_Equals_m4783954B81A2A4496B718B1940FEB8D9BD99C15D,
	XRReferencePoint_Equals_m63D1CD2AE3191F649B2791970AB507AA379B78FA,
	XRReferencePointSubsystem__ctor_m00BC1D2247754D96678FBCC67FE14BBC2107D275,
	XRReferencePointSubsystem_Start_m92D4BE486883048A2C181BB0D4C3E75A10F834BE,
	XRReferencePointSubsystem_Stop_mFAFD07F36E8E1ADDB46CEE80E65A0A65CFFB0E99,
	XRReferencePointSubsystem_OnDestroy_m49B0CE290FA4181799996BB3343CE6412D58E4C5,
	XRReferencePointSubsystem_GetChanges_mE0EC8049CED1EA604A751066DB97430E803BE487,
	XRReferencePointSubsystem_TryAddReferencePoint_mEC8446DC54875A2954FFAA796B3BA6D671BCC064,
	XRReferencePointSubsystem_TryAttachReferencePoint_m71CC28796BB1087039C1F0F067A19C0836649FF5,
	XRReferencePointSubsystem_TryRemoveReferencePoint_m212CA9D300500BDFFEBC9B99F8F3C29C8D2BE0D9,
	NULL,
	IProvider_Start_m36529D200576FC422EF0E0815D4CE54C9452B51D,
	IProvider_Stop_m6E16DE8C2AB51A51A05D3DE786D0720FBE2607BF,
	IProvider_Destroy_mDE0559C0896A464F033F677A73CD0A16CB082A88,
	IProvider_GetChanges_m7F99F4F7722D3F7761B2144E01E5964B90A0D579,
	IProvider_TryAddReferencePoint_m272B550B20E72D235C3E9347E87BBBEA456415A8,
	IProvider_TryAttachReferencePoint_m4F7742B7C3CB7418C12C23B91BA6807748BC8F61,
	IProvider_TryRemoveReferencePoint_mFF0DA84C79423650C0D155402B7D69EF0F8D436B,
	IProvider__ctor_m7D69D152ED0A017FF02C31CDD297F546148C9DD5,
	XRReferencePointSubsystemDescriptor_set_supportsTrackableAttachments_m3FBD1AAB8470EFCF3A9F31271331B1BE0DAA93BF,
	XRReferencePointSubsystemDescriptor_Create_m6560F038F7DD64D1FEE3924A57AD4DBC79D29988,
	XRReferencePointSubsystemDescriptor__ctor_m549A60C3584AADA3A0DE752C0F363113A61FD0A2,
	Cinfo_get_id_m915E41A74906A167A3AD5FA288FC098F301167D9,
	Cinfo_set_id_m022C41DDBCD030680C14FA11F178C96FCE67D687,
	Cinfo_get_subsystemImplementationType_m66047A62536BD79CB4452C2FE4878FFE035293C2,
	Cinfo_set_subsystemImplementationType_m713DB30ECC2AFC3D45F60BE6910713C6009A99E0,
	Cinfo_get_supportsTrackableAttachments_mE8629D11414D8E706864D0F179E39CA063035137,
	Cinfo_set_supportsTrackableAttachments_m1A6C2F5CE68A65944F1631E0E32FFC6BC04ECB03,
	Cinfo_GetHashCode_m44A7F0571AF0F4CEFECFDF9347EA517EA62F8E5D,
	Cinfo_Equals_m48D047F332BBAC4B19A45B966678D92CADC6F0F2,
	Cinfo_Equals_m62C82D4C3A17A329222BC38370B76CDB219ED6F3,
	SerializableGuid_get_guid_mBBAACA6CC4257BB65BB6178DB4B9B8273462D71B,
	SerializableGuid_GetHashCode_m2220A2C721DACD85F1271B8D17D334323B0CAD2D,
	SerializableGuid_Equals_m0775AC30655B576CF3FC2E92FCD210A864264994,
	SerializableGuid_ToString_mDC25F3F328C27C046B6BAD13E5839C363B4BF98C,
	SerializableGuid_Equals_mE819A7AFF3AC4EE0DBF8AC4811E3E930367DDFEC,
	SessionAvailabilityExtensions_IsSupported_mE7271B43B00DD99C98FA00AAAE3A948E0822D624,
	SessionAvailabilityExtensions_IsInstalled_m199B1D6F5729DE0A1BC7144BE38DEF9896B68A33,
	XRSessionSubsystem_get_running_m0AF2183320BAA9C43DD4B9E12B11514E579487E6,
	XRSessionSubsystem_GetAvailabilityAsync_mE1444BD33C0A1EAD4982FC0AE64D1251635487ED,
	XRSessionSubsystem_InstallAsync_m35E08EF7130491F2E498C990109FA7323A2ABCCC,
	XRSessionSubsystem__ctor_m0E7B9E65E53B03A65C53F87CF55E76528E9AF62A,
	XRSessionSubsystem_Start_m4309ED22C582729B7E0CE7593EA4F05E5D01E80C,
	XRSessionSubsystem_Reset_m09A1C906E574CD27377602632572058C88958773,
	XRSessionSubsystem_Stop_m660C2C790C22C34B49054B5AC0033BD1B2993A6D,
	XRSessionSubsystem_OnDestroy_m6E173EA064A32E49BF31E67245A70BFB0970890C,
	XRSessionSubsystem_Update_m40F8405ECB47FDC56B0B203F09655E3E5F637EFB,
	XRSessionSubsystem_OnApplicationPause_m5030BDDD5E7BB722D7259D98C988911335751945,
	XRSessionSubsystem_OnApplicationResume_m47B8B29B90F5DDF27221EE1E32D5DBA22C347FBD,
	XRSessionSubsystem_get_trackingState_m6CEDC16CB9B224A0302A83BC2C22FC4C0905EB30,
	XRSessionSubsystem_get_notTrackingReason_m2425113BCCDD44CEF92AA9A045C002CAF981B6D7,
	XRSessionSubsystem_get_matchFrameRate_mC750A3D6F4EA06E45880B76305178E568E04C82A,
	XRSessionSubsystem_set_matchFrameRate_mAE3C92CA2CA959912C6AF7AB1BD10B2B9A9FC54C,
	XRSessionSubsystem_get_frameRate_m0E723AB90D900D24D7DA2F5A2A8009CB4946A62B,
	NULL,
	IProvider_Resume_mE7AD058BDE3BD98A07B26F97C71FB5AB0BD85CA3,
	IProvider_Pause_mE01D418A52EA40CB3BDDACC95052A68BF4646C32,
	IProvider_Update_m5547CB709CDEBCAC2330B3F5EBBF9758B4C3EC89,
	IProvider_Destroy_m2284C5D0C971DFE6960647F4876A139CCE3BCC97,
	IProvider_Reset_m4A833C1AC1AEEF1926652298879EC04C355B3506,
	IProvider_OnApplicationPause_m225EDBE7D139FAA2ACD88CFEC0159C060DFA197C,
	IProvider_OnApplicationResume_m7214879F26973B5CCB17D476416DD9345B13E0D5,
	IProvider_GetAvailabilityAsync_mBEB3A0B9542D7FFC86148CDA981B9D7A79E6ECAF,
	IProvider_InstallAsync_m89483BB3B7B4D4E481B1F73CC1E2C0DAABAAC2BE,
	IProvider_get_trackingState_m3D36F03D7C209523EDDFEF23BAFDBD41B56E1128,
	IProvider_get_notTrackingReason_mD52FF69EFF5BAB7432DDF25F883C86FFA9BF2D3F,
	IProvider_get_matchFrameRate_m7D043BC7FAC35BB9E54EFB6035864487B775624C,
	IProvider_set_matchFrameRate_mFD6D9C6FD79B16E999A0E7650260F954F1CA6B66,
	IProvider_get_frameRate_m157CFC505E86907CB821238B45B2BA6FA4983813,
	IProvider__ctor_m44F52AA14885F4899E67E09988D8B08C827DEFED,
	XRSessionSubsystemDescriptor_get_supportsInstall_m6A46D829025B00AFFCEE06A8A66A3D383AF2757E,
	XRSessionSubsystemDescriptor_set_supportsInstall_m8660DC05AA837607AC160D4300E7D1D204BD1E18,
	XRSessionSubsystemDescriptor_get_supportsMatchFrameRate_m331D3B1192D04F6EF98D9172D70EC35A5B24A02B,
	XRSessionSubsystemDescriptor_set_supportsMatchFrameRate_m9371FEB427307794FC0EB7DF361E2F47E6E3F378,
	XRSessionSubsystemDescriptor_RegisterDescriptor_m3EF9E7985B16FFF8FE15FBEDFC87FF1BB811D49E,
	XRSessionSubsystemDescriptor__ctor_m1956ED91DA6DA8D4EAA62906C378B69F5F3E8C1E,
	Cinfo_get_supportsInstall_m010F6B6254015F5F477114A35C4F11F4A3334E2E,
	Cinfo_set_supportsInstall_m4295AB46C19802B003C61D7EB79DC8D02CF14B80,
	Cinfo_get_supportsMatchFrameRate_m7DD61A2A7B767E475C97DF33FA4785C2DC12B6E3,
	Cinfo_set_supportsMatchFrameRate_mE43FF83622414EA44D02418EC98B1DA8DDFFDBD6,
	Cinfo_get_id_m038FCFC448004E0F21D8DF6F22FBFA0F5AE14870,
	Cinfo_set_id_m8E2A1220FE77B46B870237AE788DFEE34F6C29CB,
	Cinfo_get_subsystemImplementationType_m27F97E21F5CC1903B5358E2C386343D53F3379BF,
	Cinfo_set_subsystemImplementationType_m9591600428F1364957BEDD8C12C1B734BBA2BF85,
	Cinfo_GetHashCode_m10FAF407C61975E8F03E5F4B961BC2583EA369A9,
	Cinfo_Equals_m2AB456C7BFFE6923D76AE087F4548493143B1B7B,
	Cinfo_Equals_m1FEB5A86DE5F73249471CC686193B327ED687B67,
	XRSessionUpdateParams_get_screenOrientation_m111C145EA6A683F025DF48C6EA355E37D8974183,
	XRSessionUpdateParams_set_screenOrientation_m977BF9AC1B8FF7224144F0979A8A30325256EE12,
	XRSessionUpdateParams_get_screenDimensions_m61A9722E272D6292B9C7C093BF7792FB007BF21E,
	XRSessionUpdateParams_set_screenDimensions_m74048D3192BAF559FEFAB878921C3EBB68ACB635,
	XRSessionUpdateParams_GetHashCode_m5D06AEAC2DD5497E37C5C8A1E06952186ACDCC70,
	XRSessionUpdateParams_Equals_m0D01CBAE986724E42B3FF0EBE51808915F873B92,
	XRSessionUpdateParams_ToString_mC2C61A95F598C42B879A6E20982DA96B16E23B6D,
	XRSessionUpdateParams_Equals_mAA0877F7CE8BCEC50F568C257794F50C3A4BFDB8,
	TrackableId_get_invalidId_mBE9FA1EC8F2EC1575C1B31666EA928A3382DF1CD,
	TrackableId_get_subId1_mC31404D1705A36B5554158D187B8E91107425D4D,
	TrackableId_get_subId2_m42D794430D6C6163D2F5F839A58383F5BB628AC0,
	TrackableId__ctor_m620606AC246BEEC637A748B22FAB588CB93120B8,
	TrackableId_ToString_m9C04F12E2DA81C481BAABC989B14E8B3509DADB2,
	TrackableId_GetHashCode_m6F1171936847F6A193255FABDCA3772D7AE57328,
	TrackableId_Equals_m513A2978A536C42A1AFE1A4D42BEE78EE056BD28,
	TrackableId_Equals_mB8FF48A7F895DEE1E826EDD6126475258BE9ADC2,
	TrackableId_op_Equality_mDC7BF74A9CA0E8F34E4BD3C584918380A624D3E8,
	TrackableId__cctor_m874A4E0F53AF39A7D95AEA9A0E8164BA2B6EA32F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	XRTextureDescriptor_get_nativeTexture_mC7FFC8C9D5E8C5BBD93F7A7E95B29253FD59770B,
	XRTextureDescriptor_get_width_m66B9E821EBE5FEBAB7A9B589A056462FD2E35D04,
	XRTextureDescriptor_get_height_mFC30414502C03B7BDD149DFFC374ACE0BD472755,
	XRTextureDescriptor_get_mipmapCount_m206E924935A23EB8B06CE8FC1E98BC74B09247F3,
	XRTextureDescriptor_get_format_mA2DA22DC1DEBCAD27A9C69F3374D614DF1C3FA2B,
	XRTextureDescriptor_get_propertyNameId_mCF70D57DDDB24C2F3521DA86DFC1131DB29B8D80,
	XRTextureDescriptor_get_valid_mBDA97DA3C73C0B8282A29606BFD70F1C29C0B4AE,
	XRTextureDescriptor_hasIdenticalTextureMetadata_m7EF6E9887C77831839D7AECC3B8E51022821A5A8,
	XRTextureDescriptor_Equals_m8198CAFC2D9A7FA7941C2B587F6FB1B9FE5918EB,
	XRTextureDescriptor_Equals_m162675AC01545EA2A8149CE27A70E811C9A7B3D5,
	XRTextureDescriptor_GetHashCode_m686D69173CDCFB2CB96E32C948A0EEC176ED19AD,
	XRTextureDescriptor_ToString_m8F80DF64DD7FC44FBFB6FFD42FA3B7265FB016C2,
};
extern void XRCameraConfiguration_get_width_m998994AB66DE43B180D472B2B703B524C69497B4_AdjustorThunk (void);
extern void XRCameraConfiguration_get_height_m13F96FED610F582C624331BD6394B88E4D055140_AdjustorThunk (void);
extern void XRCameraConfiguration_get_framerate_m97E323885AEFFC7E8FCAC41CBE3F72A34400440B_AdjustorThunk (void);
extern void XRCameraConfiguration_ToString_m423574318A784468BA1055A28EC192FC34D4FA30_AdjustorThunk (void);
extern void XRCameraConfiguration_GetHashCode_m0826525E55CF9BD9E693F2D359F46A36E7FB08EC_AdjustorThunk (void);
extern void XRCameraConfiguration_Equals_m04BF698F8D3E743BEF15107AFF74AAE1347CE82C_AdjustorThunk (void);
extern void XRCameraConfiguration_Equals_m40024812F8E6090F4F18A6F0EBC055E9ED1B50BC_AdjustorThunk (void);
extern void XRCameraFrame_get_timestampNs_m490E96B453EB3AA7416F98013B47B551C003268C_AdjustorThunk (void);
extern void XRCameraFrame_get_averageBrightness_mA23D9FB95046E89792F9F70750E000FF30E2959C_AdjustorThunk (void);
extern void XRCameraFrame_get_averageColorTemperature_m2AA0B1BE3B939E9221507D9AFB1CB28AE9FF0234_AdjustorThunk (void);
extern void XRCameraFrame_get_colorCorrection_m21EA821139C87463A369E9D308D3794A398D7A2C_AdjustorThunk (void);
extern void XRCameraFrame_get_projectionMatrix_m1AFF19568E4F29BDC16B943B34A8C988CA8382A1_AdjustorThunk (void);
extern void XRCameraFrame_get_displayMatrix_mE7C0D233A017D72EA36CC4879F49BC872D49B2A5_AdjustorThunk (void);
extern void XRCameraFrame_get_hasTimestamp_m9463BC747994BF7E6AEFDA339D88F5EDB02AA83B_AdjustorThunk (void);
extern void XRCameraFrame_get_hasAverageBrightness_m86A574F4A162689E55C8B2C0F04B9528CFB738A5_AdjustorThunk (void);
extern void XRCameraFrame_get_hasAverageColorTemperature_mB177764FC2D9592E4CA15F5BEFD710EF5510DB15_AdjustorThunk (void);
extern void XRCameraFrame_get_hasColorCorrection_m8FE25191C55FF1FA09EE7EB0D1AC1ABB1CD87597_AdjustorThunk (void);
extern void XRCameraFrame_get_hasProjectionMatrix_mC15F4AD61C07287736EA10A7C7BE5F66EE0258D1_AdjustorThunk (void);
extern void XRCameraFrame_get_hasDisplayMatrix_mD63FE4D87F9D23F40217F3DA69CEF5D01F45B8A9_AdjustorThunk (void);
extern void XRCameraFrame_Equals_mD750801D16ED7C39E75B31B663A97F300513FE3E_AdjustorThunk (void);
extern void XRCameraFrame_Equals_mA3BFB0E406F27C0A40D0A8B79E02A17B8C06AA61_AdjustorThunk (void);
extern void XRCameraFrame_GetHashCode_mB60ADF713E8CA46B1629DA9EB361ED34127C358C_AdjustorThunk (void);
extern void XRCameraFrame_ToString_mE362631B14B4302E3AED73932E73C1D6AB5684FA_AdjustorThunk (void);
extern void XRCameraImage_get_dimensions_m396844BB3871C3CF359299C935A286F7EC9E07CC_AdjustorThunk (void);
extern void XRCameraImage_set_dimensions_m0EB351A10EB73AEB3BA66DEBB92CF6A482DF0FDD_AdjustorThunk (void);
extern void XRCameraImage_get_width_m9540469A8631057CDBA693ACA5B39F11F7F8F85A_AdjustorThunk (void);
extern void XRCameraImage_get_height_m3DFB851BE8723A821A3AB2A7F08F38E5C910A2BE_AdjustorThunk (void);
extern void XRCameraImage_get_planeCount_mE12B1F437995559D932808E1D5CA4838243802D6_AdjustorThunk (void);
extern void XRCameraImage_set_planeCount_mBC04A59810816A0E2B8F173F17A17627CB2177C5_AdjustorThunk (void);
extern void XRCameraImage_get_format_m363F742D003794B51229FEF58C6D24EF2E4174D9_AdjustorThunk (void);
extern void XRCameraImage_set_format_m587247101073030F7122D794E2C7C4ED3208D3EC_AdjustorThunk (void);
extern void XRCameraImage_set_timestamp_m817C23EC056A13E82BC33FD115C5DC4A40569C5F_AdjustorThunk (void);
extern void XRCameraImage__ctor_m13AB438F2246EEA666CEB1A6C7CE9C32B7F867C0_AdjustorThunk (void);
extern void XRCameraImage_Dispose_m01A1F3830038FD9ED9B35B6640A6CA57D383615A_AdjustorThunk (void);
extern void XRCameraImage_GetHashCode_m27BEDB780E2AF59CA888ED4A8A0DC2B120BC756E_AdjustorThunk (void);
extern void XRCameraImage_Equals_mE4A7D2162B33C795268692D0B745F9610B58D4F6_AdjustorThunk (void);
extern void XRCameraImage_Equals_m9408E719265D28710EF012AA4E2E63AF320C7431_AdjustorThunk (void);
extern void XRCameraImage_ToString_m2E0D10A11B0FAE6A42A572D9C23320587B53C9E0_AdjustorThunk (void);
extern void XRCameraImageConversionParams_get_inputRect_m6F91AAA4D0844E9A9E1391A2FA73F99CEADAC833_AdjustorThunk (void);
extern void XRCameraImageConversionParams_get_outputDimensions_mE4BFDDB1E03C9024E392ABD2A0EF90398F1188D2_AdjustorThunk (void);
extern void XRCameraImageConversionParams_get_outputFormat_m7960F36DE5418DCFCBD0E780C483EDA42AB9405F_AdjustorThunk (void);
extern void XRCameraImageConversionParams_get_transformation_m2DA4133406E43882B1B52CDB3867F2E29A70C013_AdjustorThunk (void);
extern void XRCameraImageConversionParams_GetHashCode_mE4B9237F3C69745C28C8D0D958B353D80F33A876_AdjustorThunk (void);
extern void XRCameraImageConversionParams_Equals_m7E25A42871B1C10084E685AC78F4A2E53D42652E_AdjustorThunk (void);
extern void XRCameraImageConversionParams_Equals_mAB0C49294DC3FB284CB470DDC298B696C4B28A06_AdjustorThunk (void);
extern void XRCameraImageConversionParams_ToString_mCD58F5C2B04509ADEDDB374DA4492E22F3D70E97_AdjustorThunk (void);
extern void XRCameraIntrinsics_Equals_mEA26D3BF6A90B7DC9E2FF626BEA72EB6C098D5D2_AdjustorThunk (void);
extern void XRCameraIntrinsics_Equals_mB82F3C59386B2169809615700C79BF3DC519C45F_AdjustorThunk (void);
extern void XRCameraIntrinsics_GetHashCode_mC38995C37469CCF20C3A155F7F033BD16EE36D98_AdjustorThunk (void);
extern void XRCameraIntrinsics_ToString_m6A86B906A1EB79BDC75582D45311E75323054983_AdjustorThunk (void);
extern void XRCameraParams_set_zNear_m0592AF26BE6AD3149462E71FB77B3838ACC3E9AB_AdjustorThunk (void);
extern void XRCameraParams_set_zFar_m85FE4877910BD92BFE4F308F73EA2BE35F3538BF_AdjustorThunk (void);
extern void XRCameraParams_set_screenWidth_mF256E58C15E4E73B3675323C93D1F38E06438919_AdjustorThunk (void);
extern void XRCameraParams_set_screenHeight_mFFFBD063E1AA590D9B5055287965EF5A0A0B92A8_AdjustorThunk (void);
extern void XRCameraParams_set_screenOrientation_m2F3BC04E753E945AD1AE69F2643301C5FDEA3117_AdjustorThunk (void);
extern void XRCameraParams_Equals_m0E44D78DF6343B56235F52CC885DC36ABD69F586_AdjustorThunk (void);
extern void XRCameraParams_Equals_m2AF39FA9619DC2B0C470AAEEDF35A05E85BD0A1E_AdjustorThunk (void);
extern void XRCameraParams_GetHashCode_m4E01EC262AD8F277BB0793D9782904D78F3B46D4_AdjustorThunk (void);
extern void XRCameraParams_ToString_mBE56D42E3C4F978B3A61B67C346E4CB58BD293CD_AdjustorThunk (void);
extern void CameraImageCinfo_get_nativeHandle_mD1CE0F2F44CFBA90188A3DD71970DC947D548419_AdjustorThunk (void);
extern void CameraImageCinfo_get_dimensions_m55D78C58843F9F6AB11D634BBD0A4F17FB4CBFED_AdjustorThunk (void);
extern void CameraImageCinfo_get_planeCount_m261902854A879D48561867655A57295A2B7E44D6_AdjustorThunk (void);
extern void CameraImageCinfo_get_timestamp_m482C0B93963CCBB4953A1F375D5CF5862F758ED2_AdjustorThunk (void);
extern void CameraImageCinfo_get_format_mE92FD0C4F73255F6CDCCDC1CACBCAEF1E5E5605F_AdjustorThunk (void);
extern void CameraImageCinfo_Equals_mED7B9DECD14313D8D33586A50B22F2B3CFD81EC8_AdjustorThunk (void);
extern void CameraImageCinfo_Equals_m5410D9B5B20D2CD87049EB0A89362E8777B02DF8_AdjustorThunk (void);
extern void CameraImageCinfo_GetHashCode_m9E0E4A3A6091C9FF9D72E70AEB5820DD1A0959FE_AdjustorThunk (void);
extern void CameraImageCinfo_ToString_mD15A4DF77EC11F2BFC6E4A94AC781CDDF14A7BB7_AdjustorThunk (void);
extern void CameraImagePlaneCinfo_get_dataPtr_m0C0144ED8D52B8456B579FA31C3159780E5161D5_AdjustorThunk (void);
extern void CameraImagePlaneCinfo_get_dataLength_m6CD246242017D808B54EC7E57C276CCA724AFA22_AdjustorThunk (void);
extern void CameraImagePlaneCinfo_get_rowStride_m5594C13CC656157844AF9045800D49868C1A65B3_AdjustorThunk (void);
extern void CameraImagePlaneCinfo_get_pixelStride_m83C1A6151625091937C7DD696C10C39CBE4C084A_AdjustorThunk (void);
extern void CameraImagePlaneCinfo_Equals_mA5382E8A5C97636DBB423D83FDBDC6E6C4AD2F6A_AdjustorThunk (void);
extern void CameraImagePlaneCinfo_Equals_mFA864D2AFF1CCC3F62ED3B107F6B50A8D66466CE_AdjustorThunk (void);
extern void CameraImagePlaneCinfo_GetHashCode_mBB8FCE75397290CF3F692965D989A11927B16162_AdjustorThunk (void);
extern void CameraImagePlaneCinfo_ToString_mD7AEB170BA8CAA9EABF9245793A993DB435584BD_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_get_id_m8C63E6A41979D1A0201ADACA9F984088D06F488C_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_set_id_mEA5E0B21781D8AAF0FB30E9E506AA4D7C392E2A8_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_get_implementationType_m6F9012A33C47D026F5070C2F64B444A93709B5C5_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_set_implementationType_mAEA2151AEC9F31C5726795200B63D4BA53F2721E_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_get_supportsAverageBrightness_m1DDB2CC0BE14F5C77E7ECC2FF1BA5C712C08CCA5_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_set_supportsAverageBrightness_m0851BD298973A23FCE8D87B5B8AB389562D255FA_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_get_supportsAverageColorTemperature_m16E2A9E1B564001C9A7D2F5EA20C0101DC97218D_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_set_supportsAverageColorTemperature_m427080856A860B42B3FC21139B990F19BE0AD87E_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_set_supportsColorCorrection_mFC3AED27787017D69ABA73FF60D1E20DDF5E674F_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_get_supportsDisplayMatrix_mE79B1E6401467F3320D1AFF9678041FC2B5EC36C_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_set_supportsDisplayMatrix_mB5BF43F49F4D64AA3DFE174574D386D99A96F92F_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_get_supportsProjectionMatrix_m820DB18EBD58445C4976C05E25C3E2A8B04B388E_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_set_supportsProjectionMatrix_m4CC64D264746A394D8186CCDD583CFCC637C8E66_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_get_supportsTimestamp_mC9A551933743EC5171B9EAC41D5222067B56CAEA_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_set_supportsTimestamp_m901DA9F41D9CEE062F7A054738E5382E2A825F28_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_get_supportsCameraConfigurations_m125606F11D5A7521099E70525019DA50BBB4334E_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_set_supportsCameraConfigurations_mCCA48E46B902EEABAA94FB7A2A668097E06D4906_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_get_supportsCameraImage_m247ED6F4E54DC010F7506E353AD06E5D62EB4445_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_set_supportsCameraImage_m9B592584A2C27917CC80AB290F0A2600FD275951_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_Equals_mC0F2554FBD6C4944FE9956C0843718D3D423D86F_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_Equals_mDC6FD25003FEA123FF4D83BAAED03854B0DF2B15_AdjustorThunk (void);
extern void XRCameraSubsystemCinfo_GetHashCode_m41B5C3CD9B0D9101488D0D63E8557909BE239EE3_AdjustorThunk (void);
extern void Cinfo_get_supportsFeaturePoints_m167B1DCD4173C6CB2AAC5E8FA35759AE4D4BF385_AdjustorThunk (void);
extern void Cinfo_set_supportsFeaturePoints_mB3633125ACFBA430C6EC66F3FF8E5BFEC72EC360_AdjustorThunk (void);
extern void Cinfo_get_supportsConfidence_m2AB397C1B754341CC18399C796866FDC8FF597F0_AdjustorThunk (void);
extern void Cinfo_set_supportsConfidence_mD7DE3DC81C6783C66AAE15A10301DE202520605C_AdjustorThunk (void);
extern void Cinfo_get_supportsUniqueIds_m72987F3CF2CB4081D548EF34F13876D607CBD807_AdjustorThunk (void);
extern void Cinfo_set_supportsUniqueIds_m416FF5EC15306E37DC3436BBB02B4998D64B62C4_AdjustorThunk (void);
extern void Cinfo_get_capabilities_m6B6CF2B89609939E9E7313A3AAD56C543E4445AB_AdjustorThunk (void);
extern void Cinfo_set_capabilities_m2FFCFE5025136EFB71DDF302E4B81E9287D9A39E_AdjustorThunk (void);
extern void Cinfo_Equals_mDAB1544CED67FAD91D9F273A32EBD01B18EC87ED_AdjustorThunk (void);
extern void Cinfo_Equals_mF847824268A42C4CC910934F8454B95DD30C4C1E_AdjustorThunk (void);
extern void Cinfo_GetHashCode_m114731847A78B883C2EC5BC32DE8C412D4684A58_AdjustorThunk (void);
extern void XRPointCloud__ctor_m049E9ED3F776B9F25BD217D0B2D714464A5F6C9F_AdjustorThunk (void);
extern void XRPointCloud_get_trackableId_mA394197EAD026665FC02A1118CBBB46FF6873EF1_AdjustorThunk (void);
extern void XRPointCloud_get_pose_m09C2DF1AD7F1220B547BD2EBCCA6E35F85A87EB0_AdjustorThunk (void);
extern void XRPointCloud_get_trackingState_m0CE633649849B59E4AEA7875F62F0B34CA61FC96_AdjustorThunk (void);
extern void XRPointCloud_GetHashCode_mC2934047C0D733F372E51C52E2837DAE9E13259C_AdjustorThunk (void);
extern void XRPointCloud_Equals_mC56FA4F7B07E704C529E144B073920A79E599CC1_AdjustorThunk (void);
extern void XRPointCloud_Equals_m66CC3D8FEDF0226F8D4F0B6574449E334D849F57_AdjustorThunk (void);
extern void XRPointCloudData_get_positions_m2BDA572054D639DB35E9FDA3D15AEF3B7B39D40C_AdjustorThunk (void);
extern void XRPointCloudData_set_positions_mC99C23E8AE61A1A3333C1A2F7E0F9DBD6C9F771C_AdjustorThunk (void);
extern void XRPointCloudData_get_confidenceValues_mE672D23FB62CF42CA475CF82A678DA16729D618C_AdjustorThunk (void);
extern void XRPointCloudData_get_identifiers_m6F0A88EEB7DD58C82662346098A4E20CA111C479_AdjustorThunk (void);
extern void XRPointCloudData_set_identifiers_m3CDA83EC60EC5AAB982B3C5E0F9AC9E94D41992B_AdjustorThunk (void);
extern void XRPointCloudData_Dispose_mB2C769355B6385CE3F8F47E720087F7B7C726259_AdjustorThunk (void);
extern void XRPointCloudData_GetHashCode_m0ACAD17C55503EE73995A9DF2A1110FBEF0145DC_AdjustorThunk (void);
extern void XRPointCloudData_Equals_mBDA7D40FB197B84AF2E8EB4C5CF6015D09E74357_AdjustorThunk (void);
extern void XRPointCloudData_ToString_m91E31F94CB96601D2BA320BE4B993728D1A8DE20_AdjustorThunk (void);
extern void XRPointCloudData_Equals_mBE92CAA314FFE99803718F2EABEA3A1B7AE8A99C_AdjustorThunk (void);
extern void XREnvironmentProbe_get_trackableId_m2F7F8DCE954C099E60807742B6A4B27DA2F30085_AdjustorThunk (void);
extern void XREnvironmentProbe_set_trackableId_m8E02AF983995D8D544C83C7F170989AFABC13AA2_AdjustorThunk (void);
extern void XREnvironmentProbe_get_scale_m2346CDBC2FEF9CC85761BF11829E10D2992E0004_AdjustorThunk (void);
extern void XREnvironmentProbe_get_pose_m2CF6BF7E554B1225E99947B620D2C029499E7996_AdjustorThunk (void);
extern void XREnvironmentProbe_set_pose_m19121B0DDCFC795C1541A378F506B899BD2F8D0E_AdjustorThunk (void);
extern void XREnvironmentProbe_get_size_mCFA1F9B8C8BE0138585D2BE4C2059E0153133C11_AdjustorThunk (void);
extern void XREnvironmentProbe_get_textureDescriptor_mA926C0701B8D774E5C0A1F51DC54546CB71ECFFA_AdjustorThunk (void);
extern void XREnvironmentProbe_get_trackingState_m48BAAE58BF028382D00D5F49BA1F6023E53B0AF9_AdjustorThunk (void);
extern void XREnvironmentProbe_get_nativePtr_mC9CB253F77A64FCD5D1ADC64590E91A793DC8D66_AdjustorThunk (void);
extern void XREnvironmentProbe_Equals_mABEF3AB481CB2191DE4C790E3A5A245DE1D347D0_AdjustorThunk (void);
extern void XREnvironmentProbe_Equals_m8DFEE5B51820BCC164FDFA7F5F4996074A9C5170_AdjustorThunk (void);
extern void XREnvironmentProbe_GetHashCode_m638CB5F2CF52A8ABA8778B6B5EB7F13E57CD7B1B_AdjustorThunk (void);
extern void XREnvironmentProbe_ToString_m8B856D8579587102C1F500A2F2180361CD7770D2_AdjustorThunk (void);
extern void XREnvironmentProbe_ToString_m20D40F2265F40337C75C536779CC935885222B19_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_get_id_mADBD2988DA174EE595955008050CB74CB19C4882_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_set_id_m65F71E8D97413215944F75C52F6F9F2088644E24_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_get_implementationType_m2AAB6F75B1588A46DC09034244ED3C4CEF0BDD22_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_set_implementationType_m5CDE58834E022AEB4B9E6FD826D2A6140D3D1B3E_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_get_supportsManualPlacement_m38F2FBCF91D735F7ACD339C2C6FD013B62C0DC0A_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_set_supportsManualPlacement_mF3AA42AAE10CC81DF831404F415BD34694B08C59_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfManual_m1F2CFC423F2D37975488C822E76CD175A589E8AE_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfManual_m200BBBC11A1580CAA151ED498A8B24E27BAB646F_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_get_supportsAutomaticPlacement_mFA0D6FB52DF9C8ACCF6DC3B9C8A11FDB37877C67_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_set_supportsAutomaticPlacement_mBC81F9BC67A3FF73D0EB679BEDDFE1D3DA918582_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfAutomatic_mF9F46814201602562ACCF4E7AD63E20B0B7C1645_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfAutomatic_mC43F8E59BF70D73AC0EB8EE5A9B2D6F92966B3B3_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_get_supportsEnvironmentTexture_m444E17E86B43838E6BC1279C1793F40E21E0C210_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_set_supportsEnvironmentTexture_m9C630C891056E5E1187AA2433DDC8D7E0F3FF662_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_Equals_m87EE67B01CB7C502E31CBBB818CA4C6D12DAB809_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_Equals_m79E504F27913015A7C97F1E64E16A02DC1C4D73E_AdjustorThunk (void);
extern void XREnvironmentProbeSubsystemCinfo_GetHashCode_mFCD281E4EF9FD74889EF9754EF53341C79988F1B_AdjustorThunk (void);
extern void XRFace_get_trackableId_mC7AA3B622C6B16A9E8B5A3BEA524C7ED54A6188D_AdjustorThunk (void);
extern void XRFace_get_pose_m3792AF11CBB24361529B7291ED46B9DD2970AC54_AdjustorThunk (void);
extern void XRFace_get_trackingState_m29CA0D89354ACC65EF8A18C09201BCBB3F732E3B_AdjustorThunk (void);
extern void XRFace_get_nativePtr_m1EDCB59CD67423A2951DBA0DD0C98AB848183F06_AdjustorThunk (void);
extern void XRFace_Equals_mB68B812AD74B02F7B8B82DE62EAE30146238DD3A_AdjustorThunk (void);
extern void XRFace_GetHashCode_mB3AC95DBCA3308827747473BF3A6044FCD2DD7B0_AdjustorThunk (void);
extern void XRFace_Equals_m1FF3F979A7F289C6CA77DD3290F4A50CAF889ED6_AdjustorThunk (void);
extern void XRFaceMesh_get_vertices_mD6CE3C180851CA10DE10A7B6C7BF0817E73609E9_AdjustorThunk (void);
extern void XRFaceMesh_get_normals_m8CF3D0395943001F6A681154DF7A75FA045CE6AE_AdjustorThunk (void);
extern void XRFaceMesh_get_indices_m7EA9FB6B6CE3484262F74546455CB08BA5B5B00D_AdjustorThunk (void);
extern void XRFaceMesh_get_uvs_mECB939F9E262D22AED47311D6985116FC0CEEA9B_AdjustorThunk (void);
extern void XRFaceMesh_Dispose_m3E7A416718B532DFD7D100D5D0F1F3A9AED96F7E_AdjustorThunk (void);
extern void XRFaceMesh_GetHashCode_mBB4E68260D980EAFD29333F2B0EE9B10FAA8040E_AdjustorThunk (void);
extern void XRFaceMesh_Equals_m466E0A0D30D307B956CB677D1103AD4F4C86E3BC_AdjustorThunk (void);
extern void XRFaceMesh_ToString_mC0D08232897F11B687916F6B1975927411A783B3_AdjustorThunk (void);
extern void XRFaceMesh_Equals_m7F90AA84BD56C74C8C6472CE390C706E8AB8120D_AdjustorThunk (void);
extern void Cinfo_get_id_m146AEBFC1F34906EF7CB853DB90B28AAA8449D98_AdjustorThunk (void);
extern void Cinfo_set_id_mFA87FA52172846CBD4587F2E207D65A097B842E6_AdjustorThunk (void);
extern void Cinfo_get_subsystemImplementationType_m3715508999808DEC6A812A1D228A2472ECEA48C4_AdjustorThunk (void);
extern void Cinfo_set_subsystemImplementationType_mBBC866C6F4207A69F7351857F66D683E0EAF8FAF_AdjustorThunk (void);
extern void Cinfo_get_supportsMovingImages_mAB2286D926C059743ACE6E6DABCFAAB7939AC80F_AdjustorThunk (void);
extern void Cinfo_set_supportsMovingImages_m2898F758BCCE6DDC222E828D855CEFC89C58EF2C_AdjustorThunk (void);
extern void Cinfo_GetHashCode_m30FEF663E95BCA50174085CAE5EC3E6B207CF094_AdjustorThunk (void);
extern void Cinfo_Equals_mB17F6F32907C286C2B660B6E9E1BCC20A651F941_AdjustorThunk (void);
extern void Cinfo_Equals_mB207ED3E11BCF5AEC1A56818611A0082753E8DBC_AdjustorThunk (void);
extern void XRReferenceImage_get_guid_m646CE46068C4BA601BC23772D3D807D18836B80C_AdjustorThunk (void);
extern void XRReferenceImage_ToString_m6BC181F25F28FCFB70B2035D7AAEFB1E6DDC6FAA_AdjustorThunk (void);
extern void XRReferenceImage_GetHashCode_m5178E851EB53F2D106ACBB7C8330F889E1581C09_AdjustorThunk (void);
extern void XRReferenceImage_Equals_mE3432D5A1D715669F9AEC65EB524B925549F4726_AdjustorThunk (void);
extern void XRReferenceImage_Equals_m001D4B708546DF448C4243079684F6FDEE76FBC6_AdjustorThunk (void);
extern void XRTrackedImage__ctor_m0D4DB0925EB1FBEC466A9D19C627F747A400408F_AdjustorThunk (void);
extern void XRTrackedImage_get_trackableId_m6EB6DBACC95E5EE2AFEE3CE421F4C123F32E9CB8_AdjustorThunk (void);
extern void XRTrackedImage_get_sourceImageId_mFEBFE1A21956E0CBF6828407DE0F2209610BF60A_AdjustorThunk (void);
extern void XRTrackedImage_get_pose_m0566E087CA2DC99DF749E80277510C61DCF13186_AdjustorThunk (void);
extern void XRTrackedImage_get_size_m746034D0E2FD28C9E48A90965E4FCD9137988906_AdjustorThunk (void);
extern void XRTrackedImage_get_trackingState_mA7177B042E8F9F9B584582970BC5FF0377CE94DB_AdjustorThunk (void);
extern void XRTrackedImage_get_nativePtr_mE90A65D3EDE7F0190F36BD4BDF2E06FEAD113DAF_AdjustorThunk (void);
extern void XRTrackedImage_GetHashCode_mFF30CD39BC82F7A636BF9E0ACF96967C46F07B5D_AdjustorThunk (void);
extern void XRTrackedImage_Equals_m626B512ECA4BFBB14918EF13969F8789C3A8A069_AdjustorThunk (void);
extern void XRTrackedImage_Equals_mF94BFA9B373C9899F29EBD1F01A15ADA2D6E47AF_AdjustorThunk (void);
extern void BoundedPlane__ctor_mC6A5A4A0E3417618DAC201CE213664B9791BFACC_AdjustorThunk (void);
extern void BoundedPlane_get_trackableId_mD2A3DCDA61898E637ACAE0A0F4A832931378071A_AdjustorThunk (void);
extern void BoundedPlane_get_subsumedById_m77895AF3F9E056F4816CB9177209586E98967889_AdjustorThunk (void);
extern void BoundedPlane_get_pose_m30B084D1F307CB46894BE4FAD448788001A0F0DF_AdjustorThunk (void);
extern void BoundedPlane_get_center_m1AB8ED2CBDC8F4A85358F98A8DD832436818505F_AdjustorThunk (void);
extern void BoundedPlane_get_extents_mC0F005CBFDA55C68EF9CC52FBC8C0FC07854CBBB_AdjustorThunk (void);
extern void BoundedPlane_get_size_mC49BFA58CD7CE92C2470C82F30EA6F8A4F38B854_AdjustorThunk (void);
extern void BoundedPlane_get_alignment_m85C46D81426435F0B9399ABAF80AC5342A2F7137_AdjustorThunk (void);
extern void BoundedPlane_get_trackingState_m9B6467E433564ABE4BED9328F7B23AB05905B008_AdjustorThunk (void);
extern void BoundedPlane_get_nativePtr_mF0C7299B1CD00C40972DE1BE13C411594A59D361_AdjustorThunk (void);
extern void BoundedPlane_ToString_m0EE069A6B9579B564EFD59C300277FB7D824E7D1_AdjustorThunk (void);
extern void BoundedPlane_Equals_m74FDA713E8EBBF9256546B4B04A1CCF214ED7D8E_AdjustorThunk (void);
extern void BoundedPlane_GetHashCode_m39BDD70727BE818F86E2DEEF4E264FE13368B637_AdjustorThunk (void);
extern void BoundedPlane_Equals_mF06B1E1B2C53F0BF0FA541CD4828DFD16E8D789D_AdjustorThunk (void);
extern void Cinfo_get_id_m68FCB4621B357B690E4C43846221E241455A1D99_AdjustorThunk (void);
extern void Cinfo_set_id_m9211F9ADC4DCFA1AAB5AA9F662EE6510D6FE01EF_AdjustorThunk (void);
extern void Cinfo_get_subsystemImplementationType_mF924CAAE546DCF3AB899CD4ED8AC2B75B4B0706C_AdjustorThunk (void);
extern void Cinfo_set_subsystemImplementationType_m74075897385685A6E0753F2EE29CD77A90A22E6B_AdjustorThunk (void);
extern void Cinfo_get_supportsHorizontalPlaneDetection_m0FB9CC965AD3A99988175C8D1669D341BFB72214_AdjustorThunk (void);
extern void Cinfo_set_supportsHorizontalPlaneDetection_m6BA5B6FD1C2FDF236AEE15957FD1F1837C394304_AdjustorThunk (void);
extern void Cinfo_get_supportsVerticalPlaneDetection_m3C7470BF80CEB7F507540383C2BB02A5B2201AAE_AdjustorThunk (void);
extern void Cinfo_set_supportsVerticalPlaneDetection_m386B3816E8C1538AB58318D55D9C64D1113C1B3B_AdjustorThunk (void);
extern void Cinfo_get_supportsArbitraryPlaneDetection_mC75F86CCCBA2ED8AD6283EF458F3897CB78DAB3C_AdjustorThunk (void);
extern void Cinfo_set_supportsArbitraryPlaneDetection_m625EDF8616A904C1D2C3B9DB1B52A28A0D3EAF06_AdjustorThunk (void);
extern void Cinfo_get_supportsBoundaryVertices_m6E514112FA9F617953047AF6C2CD98895D587E84_AdjustorThunk (void);
extern void Cinfo_set_supportsBoundaryVertices_mFC986523905272E58728731CEE06B47DD4ECAC3D_AdjustorThunk (void);
extern void Cinfo_Equals_mE08723A87EF2FC94FD64BDD93A176F3497518F7D_AdjustorThunk (void);
extern void Cinfo_Equals_m3B3C974BB1055B352CAAE2961A7374DA559049F8_AdjustorThunk (void);
extern void Cinfo_GetHashCode_m9F3BA097C011CC6998676138C52EF6306C09BBA0_AdjustorThunk (void);
extern void XRRaycastHit_get_trackableId_mAECCB1BE08FB0B5A48CB27D955250FE2068492CF_AdjustorThunk (void);
extern void XRRaycastHit_get_pose_mE0B0A754E818C6FF3675A41CA95185A3E608C8C3_AdjustorThunk (void);
extern void XRRaycastHit_get_distance_mCD38ECEDD0FA6EAFEFEC71DB7EE3CF1B82B5CEFE_AdjustorThunk (void);
extern void XRRaycastHit_get_hitType_m52BBF5DBDE1B3E7E01571EE029F68EB29E240DA6_AdjustorThunk (void);
extern void XRRaycastHit__ctor_mFE2DF7F0A38F4507B3C5684B78F0694266BB76B5_AdjustorThunk (void);
extern void XRRaycastHit_GetHashCode_mB0A7A65C634E1CA9C70DC17D2B31A6E082D349EA_AdjustorThunk (void);
extern void XRRaycastHit_Equals_m80D2CC8EEC73127B553C601D9B6A3CEDCFBCF862_AdjustorThunk (void);
extern void XRRaycastHit_Equals_mD3774307DB6D9200AE2C4703CF2CB2D90616051C_AdjustorThunk (void);
extern void Cinfo_get_id_mCCD575E1E7E6E7E7166A4B7F0AF9E7F023FC3FCA_AdjustorThunk (void);
extern void Cinfo_set_id_mDBC061879B3E989FF064E7E31CFC85ACD142199B_AdjustorThunk (void);
extern void Cinfo_get_subsystemImplementationType_m65BDDAA14217AD17F0928287A8428D7B33A3634F_AdjustorThunk (void);
extern void Cinfo_set_subsystemImplementationType_m9670297F5DC91608B606E2B8A7E4C2643236D65A_AdjustorThunk (void);
extern void Cinfo_get_supportsViewportBasedRaycast_m8D0A14E0E43F99FCB89A3D87F954D657ADC0C514_AdjustorThunk (void);
extern void Cinfo_set_supportsViewportBasedRaycast_m42B64A1095C52F16217EBF1D5ABFD7353DA35233_AdjustorThunk (void);
extern void Cinfo_get_supportsWorldBasedRaycast_m327E8B0EE9C3103FBF490CA62FE5E1A51EF62C9F_AdjustorThunk (void);
extern void Cinfo_set_supportsWorldBasedRaycast_mBF04DD8B3208A7D9C98419FEDC8CB012F7253DF5_AdjustorThunk (void);
extern void Cinfo_get_supportedTrackableTypes_mCC57E28DFCE93ECA772B1DE2E3E49AD030D79424_AdjustorThunk (void);
extern void Cinfo_set_supportedTrackableTypes_m13138A57079E692472B33A4B216D5568852BE652_AdjustorThunk (void);
extern void Cinfo_GetHashCode_m155C084A788BBE99BBD85B2B06D7CC74DF76D636_AdjustorThunk (void);
extern void Cinfo_Equals_m434EDD8246018244C8CBBD881D91CCDBF5437EF6_AdjustorThunk (void);
extern void Cinfo_ToString_m2DFF2387C99F28931C7E802878564A3694D51453_AdjustorThunk (void);
extern void Cinfo_Equals_m21B31135C0E69990482BE0286436FF51748CF92B_AdjustorThunk (void);
extern void XRReferencePoint__ctor_mD4597D205E31CBA88C523E616E7A45D78C95BF3E_AdjustorThunk (void);
extern void XRReferencePoint_get_trackableId_m6D53542802F2444CE58861B8868274F9A8296D88_AdjustorThunk (void);
extern void XRReferencePoint_get_pose_mA4320629B8C7AE23D97FCD8E2C5FB9C9FB6AED9C_AdjustorThunk (void);
extern void XRReferencePoint_get_trackingState_mBA0DB4050B734039D22D0ECF69CD6E8896DF52B8_AdjustorThunk (void);
extern void XRReferencePoint_get_nativePtr_m51D5CA830D54C1D67656E683BE81A53D479E71A1_AdjustorThunk (void);
extern void XRReferencePoint_GetHashCode_mAA9EDDD897E7DDD88AA10A7B9E10FC2025E4F7CA_AdjustorThunk (void);
extern void XRReferencePoint_Equals_m4783954B81A2A4496B718B1940FEB8D9BD99C15D_AdjustorThunk (void);
extern void XRReferencePoint_Equals_m63D1CD2AE3191F649B2791970AB507AA379B78FA_AdjustorThunk (void);
extern void Cinfo_get_id_m915E41A74906A167A3AD5FA288FC098F301167D9_AdjustorThunk (void);
extern void Cinfo_set_id_m022C41DDBCD030680C14FA11F178C96FCE67D687_AdjustorThunk (void);
extern void Cinfo_get_subsystemImplementationType_m66047A62536BD79CB4452C2FE4878FFE035293C2_AdjustorThunk (void);
extern void Cinfo_set_subsystemImplementationType_m713DB30ECC2AFC3D45F60BE6910713C6009A99E0_AdjustorThunk (void);
extern void Cinfo_get_supportsTrackableAttachments_mE8629D11414D8E706864D0F179E39CA063035137_AdjustorThunk (void);
extern void Cinfo_set_supportsTrackableAttachments_m1A6C2F5CE68A65944F1631E0E32FFC6BC04ECB03_AdjustorThunk (void);
extern void Cinfo_GetHashCode_m44A7F0571AF0F4CEFECFDF9347EA517EA62F8E5D_AdjustorThunk (void);
extern void Cinfo_Equals_m48D047F332BBAC4B19A45B966678D92CADC6F0F2_AdjustorThunk (void);
extern void Cinfo_Equals_m62C82D4C3A17A329222BC38370B76CDB219ED6F3_AdjustorThunk (void);
extern void SerializableGuid_get_guid_mBBAACA6CC4257BB65BB6178DB4B9B8273462D71B_AdjustorThunk (void);
extern void SerializableGuid_GetHashCode_m2220A2C721DACD85F1271B8D17D334323B0CAD2D_AdjustorThunk (void);
extern void SerializableGuid_Equals_m0775AC30655B576CF3FC2E92FCD210A864264994_AdjustorThunk (void);
extern void SerializableGuid_ToString_mDC25F3F328C27C046B6BAD13E5839C363B4BF98C_AdjustorThunk (void);
extern void SerializableGuid_Equals_mE819A7AFF3AC4EE0DBF8AC4811E3E930367DDFEC_AdjustorThunk (void);
extern void Cinfo_get_supportsInstall_m010F6B6254015F5F477114A35C4F11F4A3334E2E_AdjustorThunk (void);
extern void Cinfo_set_supportsInstall_m4295AB46C19802B003C61D7EB79DC8D02CF14B80_AdjustorThunk (void);
extern void Cinfo_get_supportsMatchFrameRate_m7DD61A2A7B767E475C97DF33FA4785C2DC12B6E3_AdjustorThunk (void);
extern void Cinfo_set_supportsMatchFrameRate_mE43FF83622414EA44D02418EC98B1DA8DDFFDBD6_AdjustorThunk (void);
extern void Cinfo_get_id_m038FCFC448004E0F21D8DF6F22FBFA0F5AE14870_AdjustorThunk (void);
extern void Cinfo_set_id_m8E2A1220FE77B46B870237AE788DFEE34F6C29CB_AdjustorThunk (void);
extern void Cinfo_get_subsystemImplementationType_m27F97E21F5CC1903B5358E2C386343D53F3379BF_AdjustorThunk (void);
extern void Cinfo_set_subsystemImplementationType_m9591600428F1364957BEDD8C12C1B734BBA2BF85_AdjustorThunk (void);
extern void Cinfo_GetHashCode_m10FAF407C61975E8F03E5F4B961BC2583EA369A9_AdjustorThunk (void);
extern void Cinfo_Equals_m2AB456C7BFFE6923D76AE087F4548493143B1B7B_AdjustorThunk (void);
extern void Cinfo_Equals_m1FEB5A86DE5F73249471CC686193B327ED687B67_AdjustorThunk (void);
extern void XRSessionUpdateParams_get_screenOrientation_m111C145EA6A683F025DF48C6EA355E37D8974183_AdjustorThunk (void);
extern void XRSessionUpdateParams_set_screenOrientation_m977BF9AC1B8FF7224144F0979A8A30325256EE12_AdjustorThunk (void);
extern void XRSessionUpdateParams_get_screenDimensions_m61A9722E272D6292B9C7C093BF7792FB007BF21E_AdjustorThunk (void);
extern void XRSessionUpdateParams_set_screenDimensions_m74048D3192BAF559FEFAB878921C3EBB68ACB635_AdjustorThunk (void);
extern void XRSessionUpdateParams_GetHashCode_m5D06AEAC2DD5497E37C5C8A1E06952186ACDCC70_AdjustorThunk (void);
extern void XRSessionUpdateParams_Equals_m0D01CBAE986724E42B3FF0EBE51808915F873B92_AdjustorThunk (void);
extern void XRSessionUpdateParams_ToString_mC2C61A95F598C42B879A6E20982DA96B16E23B6D_AdjustorThunk (void);
extern void XRSessionUpdateParams_Equals_mAA0877F7CE8BCEC50F568C257794F50C3A4BFDB8_AdjustorThunk (void);
extern void TrackableId_get_subId1_mC31404D1705A36B5554158D187B8E91107425D4D_AdjustorThunk (void);
extern void TrackableId_get_subId2_m42D794430D6C6163D2F5F839A58383F5BB628AC0_AdjustorThunk (void);
extern void TrackableId__ctor_m620606AC246BEEC637A748B22FAB588CB93120B8_AdjustorThunk (void);
extern void TrackableId_ToString_m9C04F12E2DA81C481BAABC989B14E8B3509DADB2_AdjustorThunk (void);
extern void TrackableId_GetHashCode_m6F1171936847F6A193255FABDCA3772D7AE57328_AdjustorThunk (void);
extern void TrackableId_Equals_m513A2978A536C42A1AFE1A4D42BEE78EE056BD28_AdjustorThunk (void);
extern void TrackableId_Equals_mB8FF48A7F895DEE1E826EDD6126475258BE9ADC2_AdjustorThunk (void);
extern void XRTextureDescriptor_get_nativeTexture_mC7FFC8C9D5E8C5BBD93F7A7E95B29253FD59770B_AdjustorThunk (void);
extern void XRTextureDescriptor_get_width_m66B9E821EBE5FEBAB7A9B589A056462FD2E35D04_AdjustorThunk (void);
extern void XRTextureDescriptor_get_height_mFC30414502C03B7BDD149DFFC374ACE0BD472755_AdjustorThunk (void);
extern void XRTextureDescriptor_get_mipmapCount_m206E924935A23EB8B06CE8FC1E98BC74B09247F3_AdjustorThunk (void);
extern void XRTextureDescriptor_get_format_mA2DA22DC1DEBCAD27A9C69F3374D614DF1C3FA2B_AdjustorThunk (void);
extern void XRTextureDescriptor_get_propertyNameId_mCF70D57DDDB24C2F3521DA86DFC1131DB29B8D80_AdjustorThunk (void);
extern void XRTextureDescriptor_get_valid_mBDA97DA3C73C0B8282A29606BFD70F1C29C0B4AE_AdjustorThunk (void);
extern void XRTextureDescriptor_hasIdenticalTextureMetadata_m7EF6E9887C77831839D7AECC3B8E51022821A5A8_AdjustorThunk (void);
extern void XRTextureDescriptor_Equals_m8198CAFC2D9A7FA7941C2B587F6FB1B9FE5918EB_AdjustorThunk (void);
extern void XRTextureDescriptor_Equals_m162675AC01545EA2A8149CE27A70E811C9A7B3D5_AdjustorThunk (void);
extern void XRTextureDescriptor_GetHashCode_m686D69173CDCFB2CB96E32C948A0EEC176ED19AD_AdjustorThunk (void);
extern void XRTextureDescriptor_ToString_m8F80DF64DD7FC44FBFB6FFD42FA3B7265FB016C2_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[308] = 
{
	{ 0x06000001, XRCameraConfiguration_get_width_m998994AB66DE43B180D472B2B703B524C69497B4_AdjustorThunk },
	{ 0x06000002, XRCameraConfiguration_get_height_m13F96FED610F582C624331BD6394B88E4D055140_AdjustorThunk },
	{ 0x06000003, XRCameraConfiguration_get_framerate_m97E323885AEFFC7E8FCAC41CBE3F72A34400440B_AdjustorThunk },
	{ 0x06000004, XRCameraConfiguration_ToString_m423574318A784468BA1055A28EC192FC34D4FA30_AdjustorThunk },
	{ 0x06000005, XRCameraConfiguration_GetHashCode_m0826525E55CF9BD9E693F2D359F46A36E7FB08EC_AdjustorThunk },
	{ 0x06000006, XRCameraConfiguration_Equals_m04BF698F8D3E743BEF15107AFF74AAE1347CE82C_AdjustorThunk },
	{ 0x06000007, XRCameraConfiguration_Equals_m40024812F8E6090F4F18A6F0EBC055E9ED1B50BC_AdjustorThunk },
	{ 0x06000008, XRCameraFrame_get_timestampNs_m490E96B453EB3AA7416F98013B47B551C003268C_AdjustorThunk },
	{ 0x06000009, XRCameraFrame_get_averageBrightness_mA23D9FB95046E89792F9F70750E000FF30E2959C_AdjustorThunk },
	{ 0x0600000A, XRCameraFrame_get_averageColorTemperature_m2AA0B1BE3B939E9221507D9AFB1CB28AE9FF0234_AdjustorThunk },
	{ 0x0600000B, XRCameraFrame_get_colorCorrection_m21EA821139C87463A369E9D308D3794A398D7A2C_AdjustorThunk },
	{ 0x0600000C, XRCameraFrame_get_projectionMatrix_m1AFF19568E4F29BDC16B943B34A8C988CA8382A1_AdjustorThunk },
	{ 0x0600000D, XRCameraFrame_get_displayMatrix_mE7C0D233A017D72EA36CC4879F49BC872D49B2A5_AdjustorThunk },
	{ 0x0600000E, XRCameraFrame_get_hasTimestamp_m9463BC747994BF7E6AEFDA339D88F5EDB02AA83B_AdjustorThunk },
	{ 0x0600000F, XRCameraFrame_get_hasAverageBrightness_m86A574F4A162689E55C8B2C0F04B9528CFB738A5_AdjustorThunk },
	{ 0x06000010, XRCameraFrame_get_hasAverageColorTemperature_mB177764FC2D9592E4CA15F5BEFD710EF5510DB15_AdjustorThunk },
	{ 0x06000011, XRCameraFrame_get_hasColorCorrection_m8FE25191C55FF1FA09EE7EB0D1AC1ABB1CD87597_AdjustorThunk },
	{ 0x06000012, XRCameraFrame_get_hasProjectionMatrix_mC15F4AD61C07287736EA10A7C7BE5F66EE0258D1_AdjustorThunk },
	{ 0x06000013, XRCameraFrame_get_hasDisplayMatrix_mD63FE4D87F9D23F40217F3DA69CEF5D01F45B8A9_AdjustorThunk },
	{ 0x06000014, XRCameraFrame_Equals_mD750801D16ED7C39E75B31B663A97F300513FE3E_AdjustorThunk },
	{ 0x06000015, XRCameraFrame_Equals_mA3BFB0E406F27C0A40D0A8B79E02A17B8C06AA61_AdjustorThunk },
	{ 0x06000016, XRCameraFrame_GetHashCode_mB60ADF713E8CA46B1629DA9EB361ED34127C358C_AdjustorThunk },
	{ 0x06000017, XRCameraFrame_ToString_mE362631B14B4302E3AED73932E73C1D6AB5684FA_AdjustorThunk },
	{ 0x06000018, XRCameraImage_get_dimensions_m396844BB3871C3CF359299C935A286F7EC9E07CC_AdjustorThunk },
	{ 0x06000019, XRCameraImage_set_dimensions_m0EB351A10EB73AEB3BA66DEBB92CF6A482DF0FDD_AdjustorThunk },
	{ 0x0600001A, XRCameraImage_get_width_m9540469A8631057CDBA693ACA5B39F11F7F8F85A_AdjustorThunk },
	{ 0x0600001B, XRCameraImage_get_height_m3DFB851BE8723A821A3AB2A7F08F38E5C910A2BE_AdjustorThunk },
	{ 0x0600001C, XRCameraImage_get_planeCount_mE12B1F437995559D932808E1D5CA4838243802D6_AdjustorThunk },
	{ 0x0600001D, XRCameraImage_set_planeCount_mBC04A59810816A0E2B8F173F17A17627CB2177C5_AdjustorThunk },
	{ 0x0600001E, XRCameraImage_get_format_m363F742D003794B51229FEF58C6D24EF2E4174D9_AdjustorThunk },
	{ 0x0600001F, XRCameraImage_set_format_m587247101073030F7122D794E2C7C4ED3208D3EC_AdjustorThunk },
	{ 0x06000020, XRCameraImage_set_timestamp_m817C23EC056A13E82BC33FD115C5DC4A40569C5F_AdjustorThunk },
	{ 0x06000022, XRCameraImage__ctor_m13AB438F2246EEA666CEB1A6C7CE9C32B7F867C0_AdjustorThunk },
	{ 0x06000024, XRCameraImage_Dispose_m01A1F3830038FD9ED9B35B6640A6CA57D383615A_AdjustorThunk },
	{ 0x06000025, XRCameraImage_GetHashCode_m27BEDB780E2AF59CA888ED4A8A0DC2B120BC756E_AdjustorThunk },
	{ 0x06000026, XRCameraImage_Equals_mE4A7D2162B33C795268692D0B745F9610B58D4F6_AdjustorThunk },
	{ 0x06000027, XRCameraImage_Equals_m9408E719265D28710EF012AA4E2E63AF320C7431_AdjustorThunk },
	{ 0x06000028, XRCameraImage_ToString_m2E0D10A11B0FAE6A42A572D9C23320587B53C9E0_AdjustorThunk },
	{ 0x06000029, XRCameraImageConversionParams_get_inputRect_m6F91AAA4D0844E9A9E1391A2FA73F99CEADAC833_AdjustorThunk },
	{ 0x0600002A, XRCameraImageConversionParams_get_outputDimensions_mE4BFDDB1E03C9024E392ABD2A0EF90398F1188D2_AdjustorThunk },
	{ 0x0600002B, XRCameraImageConversionParams_get_outputFormat_m7960F36DE5418DCFCBD0E780C483EDA42AB9405F_AdjustorThunk },
	{ 0x0600002C, XRCameraImageConversionParams_get_transformation_m2DA4133406E43882B1B52CDB3867F2E29A70C013_AdjustorThunk },
	{ 0x0600002D, XRCameraImageConversionParams_GetHashCode_mE4B9237F3C69745C28C8D0D958B353D80F33A876_AdjustorThunk },
	{ 0x0600002E, XRCameraImageConversionParams_Equals_m7E25A42871B1C10084E685AC78F4A2E53D42652E_AdjustorThunk },
	{ 0x0600002F, XRCameraImageConversionParams_Equals_mAB0C49294DC3FB284CB470DDC298B696C4B28A06_AdjustorThunk },
	{ 0x06000030, XRCameraImageConversionParams_ToString_mCD58F5C2B04509ADEDDB374DA4492E22F3D70E97_AdjustorThunk },
	{ 0x06000031, XRCameraIntrinsics_Equals_mEA26D3BF6A90B7DC9E2FF626BEA72EB6C098D5D2_AdjustorThunk },
	{ 0x06000032, XRCameraIntrinsics_Equals_mB82F3C59386B2169809615700C79BF3DC519C45F_AdjustorThunk },
	{ 0x06000033, XRCameraIntrinsics_GetHashCode_mC38995C37469CCF20C3A155F7F033BD16EE36D98_AdjustorThunk },
	{ 0x06000034, XRCameraIntrinsics_ToString_m6A86B906A1EB79BDC75582D45311E75323054983_AdjustorThunk },
	{ 0x06000035, XRCameraParams_set_zNear_m0592AF26BE6AD3149462E71FB77B3838ACC3E9AB_AdjustorThunk },
	{ 0x06000036, XRCameraParams_set_zFar_m85FE4877910BD92BFE4F308F73EA2BE35F3538BF_AdjustorThunk },
	{ 0x06000037, XRCameraParams_set_screenWidth_mF256E58C15E4E73B3675323C93D1F38E06438919_AdjustorThunk },
	{ 0x06000038, XRCameraParams_set_screenHeight_mFFFBD063E1AA590D9B5055287965EF5A0A0B92A8_AdjustorThunk },
	{ 0x06000039, XRCameraParams_set_screenOrientation_m2F3BC04E753E945AD1AE69F2643301C5FDEA3117_AdjustorThunk },
	{ 0x0600003A, XRCameraParams_Equals_m0E44D78DF6343B56235F52CC885DC36ABD69F586_AdjustorThunk },
	{ 0x0600003B, XRCameraParams_Equals_m2AF39FA9619DC2B0C470AAEEDF35A05E85BD0A1E_AdjustorThunk },
	{ 0x0600003C, XRCameraParams_GetHashCode_m4E01EC262AD8F277BB0793D9782904D78F3B46D4_AdjustorThunk },
	{ 0x0600003D, XRCameraParams_ToString_mBE56D42E3C4F978B3A61B67C346E4CB58BD293CD_AdjustorThunk },
	{ 0x06000067, CameraImageCinfo_get_nativeHandle_mD1CE0F2F44CFBA90188A3DD71970DC947D548419_AdjustorThunk },
	{ 0x06000068, CameraImageCinfo_get_dimensions_m55D78C58843F9F6AB11D634BBD0A4F17FB4CBFED_AdjustorThunk },
	{ 0x06000069, CameraImageCinfo_get_planeCount_m261902854A879D48561867655A57295A2B7E44D6_AdjustorThunk },
	{ 0x0600006A, CameraImageCinfo_get_timestamp_m482C0B93963CCBB4953A1F375D5CF5862F758ED2_AdjustorThunk },
	{ 0x0600006B, CameraImageCinfo_get_format_mE92FD0C4F73255F6CDCCDC1CACBCAEF1E5E5605F_AdjustorThunk },
	{ 0x0600006C, CameraImageCinfo_Equals_mED7B9DECD14313D8D33586A50B22F2B3CFD81EC8_AdjustorThunk },
	{ 0x0600006D, CameraImageCinfo_Equals_m5410D9B5B20D2CD87049EB0A89362E8777B02DF8_AdjustorThunk },
	{ 0x0600006E, CameraImageCinfo_GetHashCode_m9E0E4A3A6091C9FF9D72E70AEB5820DD1A0959FE_AdjustorThunk },
	{ 0x0600006F, CameraImageCinfo_ToString_mD15A4DF77EC11F2BFC6E4A94AC781CDDF14A7BB7_AdjustorThunk },
	{ 0x06000070, CameraImagePlaneCinfo_get_dataPtr_m0C0144ED8D52B8456B579FA31C3159780E5161D5_AdjustorThunk },
	{ 0x06000071, CameraImagePlaneCinfo_get_dataLength_m6CD246242017D808B54EC7E57C276CCA724AFA22_AdjustorThunk },
	{ 0x06000072, CameraImagePlaneCinfo_get_rowStride_m5594C13CC656157844AF9045800D49868C1A65B3_AdjustorThunk },
	{ 0x06000073, CameraImagePlaneCinfo_get_pixelStride_m83C1A6151625091937C7DD696C10C39CBE4C084A_AdjustorThunk },
	{ 0x06000074, CameraImagePlaneCinfo_Equals_mA5382E8A5C97636DBB423D83FDBDC6E6C4AD2F6A_AdjustorThunk },
	{ 0x06000075, CameraImagePlaneCinfo_Equals_mFA864D2AFF1CCC3F62ED3B107F6B50A8D66466CE_AdjustorThunk },
	{ 0x06000076, CameraImagePlaneCinfo_GetHashCode_mBB8FCE75397290CF3F692965D989A11927B16162_AdjustorThunk },
	{ 0x06000077, CameraImagePlaneCinfo_ToString_mD7AEB170BA8CAA9EABF9245793A993DB435584BD_AdjustorThunk },
	{ 0x06000078, XRCameraSubsystemCinfo_get_id_m8C63E6A41979D1A0201ADACA9F984088D06F488C_AdjustorThunk },
	{ 0x06000079, XRCameraSubsystemCinfo_set_id_mEA5E0B21781D8AAF0FB30E9E506AA4D7C392E2A8_AdjustorThunk },
	{ 0x0600007A, XRCameraSubsystemCinfo_get_implementationType_m6F9012A33C47D026F5070C2F64B444A93709B5C5_AdjustorThunk },
	{ 0x0600007B, XRCameraSubsystemCinfo_set_implementationType_mAEA2151AEC9F31C5726795200B63D4BA53F2721E_AdjustorThunk },
	{ 0x0600007C, XRCameraSubsystemCinfo_get_supportsAverageBrightness_m1DDB2CC0BE14F5C77E7ECC2FF1BA5C712C08CCA5_AdjustorThunk },
	{ 0x0600007D, XRCameraSubsystemCinfo_set_supportsAverageBrightness_m0851BD298973A23FCE8D87B5B8AB389562D255FA_AdjustorThunk },
	{ 0x0600007E, XRCameraSubsystemCinfo_get_supportsAverageColorTemperature_m16E2A9E1B564001C9A7D2F5EA20C0101DC97218D_AdjustorThunk },
	{ 0x0600007F, XRCameraSubsystemCinfo_set_supportsAverageColorTemperature_m427080856A860B42B3FC21139B990F19BE0AD87E_AdjustorThunk },
	{ 0x06000080, XRCameraSubsystemCinfo_set_supportsColorCorrection_mFC3AED27787017D69ABA73FF60D1E20DDF5E674F_AdjustorThunk },
	{ 0x06000081, XRCameraSubsystemCinfo_get_supportsDisplayMatrix_mE79B1E6401467F3320D1AFF9678041FC2B5EC36C_AdjustorThunk },
	{ 0x06000082, XRCameraSubsystemCinfo_set_supportsDisplayMatrix_mB5BF43F49F4D64AA3DFE174574D386D99A96F92F_AdjustorThunk },
	{ 0x06000083, XRCameraSubsystemCinfo_get_supportsProjectionMatrix_m820DB18EBD58445C4976C05E25C3E2A8B04B388E_AdjustorThunk },
	{ 0x06000084, XRCameraSubsystemCinfo_set_supportsProjectionMatrix_m4CC64D264746A394D8186CCDD583CFCC637C8E66_AdjustorThunk },
	{ 0x06000085, XRCameraSubsystemCinfo_get_supportsTimestamp_mC9A551933743EC5171B9EAC41D5222067B56CAEA_AdjustorThunk },
	{ 0x06000086, XRCameraSubsystemCinfo_set_supportsTimestamp_m901DA9F41D9CEE062F7A054738E5382E2A825F28_AdjustorThunk },
	{ 0x06000087, XRCameraSubsystemCinfo_get_supportsCameraConfigurations_m125606F11D5A7521099E70525019DA50BBB4334E_AdjustorThunk },
	{ 0x06000088, XRCameraSubsystemCinfo_set_supportsCameraConfigurations_mCCA48E46B902EEABAA94FB7A2A668097E06D4906_AdjustorThunk },
	{ 0x06000089, XRCameraSubsystemCinfo_get_supportsCameraImage_m247ED6F4E54DC010F7506E353AD06E5D62EB4445_AdjustorThunk },
	{ 0x0600008A, XRCameraSubsystemCinfo_set_supportsCameraImage_m9B592584A2C27917CC80AB290F0A2600FD275951_AdjustorThunk },
	{ 0x0600008B, XRCameraSubsystemCinfo_Equals_mC0F2554FBD6C4944FE9956C0843718D3D423D86F_AdjustorThunk },
	{ 0x0600008C, XRCameraSubsystemCinfo_Equals_mDC6FD25003FEA123FF4D83BAAED03854B0DF2B15_AdjustorThunk },
	{ 0x0600008D, XRCameraSubsystemCinfo_GetHashCode_m41B5C3CD9B0D9101488D0D63E8557909BE239EE3_AdjustorThunk },
	{ 0x060000A9, Cinfo_get_supportsFeaturePoints_m167B1DCD4173C6CB2AAC5E8FA35759AE4D4BF385_AdjustorThunk },
	{ 0x060000AA, Cinfo_set_supportsFeaturePoints_mB3633125ACFBA430C6EC66F3FF8E5BFEC72EC360_AdjustorThunk },
	{ 0x060000AB, Cinfo_get_supportsConfidence_m2AB397C1B754341CC18399C796866FDC8FF597F0_AdjustorThunk },
	{ 0x060000AC, Cinfo_set_supportsConfidence_mD7DE3DC81C6783C66AAE15A10301DE202520605C_AdjustorThunk },
	{ 0x060000AD, Cinfo_get_supportsUniqueIds_m72987F3CF2CB4081D548EF34F13876D607CBD807_AdjustorThunk },
	{ 0x060000AE, Cinfo_set_supportsUniqueIds_m416FF5EC15306E37DC3436BBB02B4998D64B62C4_AdjustorThunk },
	{ 0x060000AF, Cinfo_get_capabilities_m6B6CF2B89609939E9E7313A3AAD56C543E4445AB_AdjustorThunk },
	{ 0x060000B0, Cinfo_set_capabilities_m2FFCFE5025136EFB71DDF302E4B81E9287D9A39E_AdjustorThunk },
	{ 0x060000B1, Cinfo_Equals_mDAB1544CED67FAD91D9F273A32EBD01B18EC87ED_AdjustorThunk },
	{ 0x060000B2, Cinfo_Equals_mF847824268A42C4CC910934F8454B95DD30C4C1E_AdjustorThunk },
	{ 0x060000B3, Cinfo_GetHashCode_m114731847A78B883C2EC5BC32DE8C412D4684A58_AdjustorThunk },
	{ 0x060000B5, XRPointCloud__ctor_m049E9ED3F776B9F25BD217D0B2D714464A5F6C9F_AdjustorThunk },
	{ 0x060000B6, XRPointCloud_get_trackableId_mA394197EAD026665FC02A1118CBBB46FF6873EF1_AdjustorThunk },
	{ 0x060000B7, XRPointCloud_get_pose_m09C2DF1AD7F1220B547BD2EBCCA6E35F85A87EB0_AdjustorThunk },
	{ 0x060000B8, XRPointCloud_get_trackingState_m0CE633649849B59E4AEA7875F62F0B34CA61FC96_AdjustorThunk },
	{ 0x060000B9, XRPointCloud_GetHashCode_mC2934047C0D733F372E51C52E2837DAE9E13259C_AdjustorThunk },
	{ 0x060000BA, XRPointCloud_Equals_mC56FA4F7B07E704C529E144B073920A79E599CC1_AdjustorThunk },
	{ 0x060000BB, XRPointCloud_Equals_m66CC3D8FEDF0226F8D4F0B6574449E334D849F57_AdjustorThunk },
	{ 0x060000BC, XRPointCloudData_get_positions_m2BDA572054D639DB35E9FDA3D15AEF3B7B39D40C_AdjustorThunk },
	{ 0x060000BD, XRPointCloudData_set_positions_mC99C23E8AE61A1A3333C1A2F7E0F9DBD6C9F771C_AdjustorThunk },
	{ 0x060000BE, XRPointCloudData_get_confidenceValues_mE672D23FB62CF42CA475CF82A678DA16729D618C_AdjustorThunk },
	{ 0x060000BF, XRPointCloudData_get_identifiers_m6F0A88EEB7DD58C82662346098A4E20CA111C479_AdjustorThunk },
	{ 0x060000C0, XRPointCloudData_set_identifiers_m3CDA83EC60EC5AAB982B3C5E0F9AC9E94D41992B_AdjustorThunk },
	{ 0x060000C1, XRPointCloudData_Dispose_mB2C769355B6385CE3F8F47E720087F7B7C726259_AdjustorThunk },
	{ 0x060000C2, XRPointCloudData_GetHashCode_m0ACAD17C55503EE73995A9DF2A1110FBEF0145DC_AdjustorThunk },
	{ 0x060000C3, XRPointCloudData_Equals_mBDA7D40FB197B84AF2E8EB4C5CF6015D09E74357_AdjustorThunk },
	{ 0x060000C4, XRPointCloudData_ToString_m91E31F94CB96601D2BA320BE4B993728D1A8DE20_AdjustorThunk },
	{ 0x060000C5, XRPointCloudData_Equals_mBE92CAA314FFE99803718F2EABEA3A1B7AE8A99C_AdjustorThunk },
	{ 0x060000C7, XREnvironmentProbe_get_trackableId_m2F7F8DCE954C099E60807742B6A4B27DA2F30085_AdjustorThunk },
	{ 0x060000C8, XREnvironmentProbe_set_trackableId_m8E02AF983995D8D544C83C7F170989AFABC13AA2_AdjustorThunk },
	{ 0x060000C9, XREnvironmentProbe_get_scale_m2346CDBC2FEF9CC85761BF11829E10D2992E0004_AdjustorThunk },
	{ 0x060000CA, XREnvironmentProbe_get_pose_m2CF6BF7E554B1225E99947B620D2C029499E7996_AdjustorThunk },
	{ 0x060000CB, XREnvironmentProbe_set_pose_m19121B0DDCFC795C1541A378F506B899BD2F8D0E_AdjustorThunk },
	{ 0x060000CC, XREnvironmentProbe_get_size_mCFA1F9B8C8BE0138585D2BE4C2059E0153133C11_AdjustorThunk },
	{ 0x060000CD, XREnvironmentProbe_get_textureDescriptor_mA926C0701B8D774E5C0A1F51DC54546CB71ECFFA_AdjustorThunk },
	{ 0x060000CE, XREnvironmentProbe_get_trackingState_m48BAAE58BF028382D00D5F49BA1F6023E53B0AF9_AdjustorThunk },
	{ 0x060000CF, XREnvironmentProbe_get_nativePtr_mC9CB253F77A64FCD5D1ADC64590E91A793DC8D66_AdjustorThunk },
	{ 0x060000D0, XREnvironmentProbe_Equals_mABEF3AB481CB2191DE4C790E3A5A245DE1D347D0_AdjustorThunk },
	{ 0x060000D1, XREnvironmentProbe_Equals_m8DFEE5B51820BCC164FDFA7F5F4996074A9C5170_AdjustorThunk },
	{ 0x060000D2, XREnvironmentProbe_GetHashCode_m638CB5F2CF52A8ABA8778B6B5EB7F13E57CD7B1B_AdjustorThunk },
	{ 0x060000D3, XREnvironmentProbe_ToString_m8B856D8579587102C1F500A2F2180361CD7770D2_AdjustorThunk },
	{ 0x060000D4, XREnvironmentProbe_ToString_m20D40F2265F40337C75C536779CC935885222B19_AdjustorThunk },
	{ 0x060000E8, XREnvironmentProbeSubsystemCinfo_get_id_mADBD2988DA174EE595955008050CB74CB19C4882_AdjustorThunk },
	{ 0x060000E9, XREnvironmentProbeSubsystemCinfo_set_id_m65F71E8D97413215944F75C52F6F9F2088644E24_AdjustorThunk },
	{ 0x060000EA, XREnvironmentProbeSubsystemCinfo_get_implementationType_m2AAB6F75B1588A46DC09034244ED3C4CEF0BDD22_AdjustorThunk },
	{ 0x060000EB, XREnvironmentProbeSubsystemCinfo_set_implementationType_m5CDE58834E022AEB4B9E6FD826D2A6140D3D1B3E_AdjustorThunk },
	{ 0x060000EC, XREnvironmentProbeSubsystemCinfo_get_supportsManualPlacement_m38F2FBCF91D735F7ACD339C2C6FD013B62C0DC0A_AdjustorThunk },
	{ 0x060000ED, XREnvironmentProbeSubsystemCinfo_set_supportsManualPlacement_mF3AA42AAE10CC81DF831404F415BD34694B08C59_AdjustorThunk },
	{ 0x060000EE, XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfManual_m1F2CFC423F2D37975488C822E76CD175A589E8AE_AdjustorThunk },
	{ 0x060000EF, XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfManual_m200BBBC11A1580CAA151ED498A8B24E27BAB646F_AdjustorThunk },
	{ 0x060000F0, XREnvironmentProbeSubsystemCinfo_get_supportsAutomaticPlacement_mFA0D6FB52DF9C8ACCF6DC3B9C8A11FDB37877C67_AdjustorThunk },
	{ 0x060000F1, XREnvironmentProbeSubsystemCinfo_set_supportsAutomaticPlacement_mBC81F9BC67A3FF73D0EB679BEDDFE1D3DA918582_AdjustorThunk },
	{ 0x060000F2, XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfAutomatic_mF9F46814201602562ACCF4E7AD63E20B0B7C1645_AdjustorThunk },
	{ 0x060000F3, XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfAutomatic_mC43F8E59BF70D73AC0EB8EE5A9B2D6F92966B3B3_AdjustorThunk },
	{ 0x060000F4, XREnvironmentProbeSubsystemCinfo_get_supportsEnvironmentTexture_m444E17E86B43838E6BC1279C1793F40E21E0C210_AdjustorThunk },
	{ 0x060000F5, XREnvironmentProbeSubsystemCinfo_set_supportsEnvironmentTexture_m9C630C891056E5E1187AA2433DDC8D7E0F3FF662_AdjustorThunk },
	{ 0x060000F6, XREnvironmentProbeSubsystemCinfo_Equals_m87EE67B01CB7C502E31CBBB818CA4C6D12DAB809_AdjustorThunk },
	{ 0x060000F7, XREnvironmentProbeSubsystemCinfo_Equals_m79E504F27913015A7C97F1E64E16A02DC1C4D73E_AdjustorThunk },
	{ 0x060000F8, XREnvironmentProbeSubsystemCinfo_GetHashCode_mFCD281E4EF9FD74889EF9754EF53341C79988F1B_AdjustorThunk },
	{ 0x06000105, XRFace_get_trackableId_mC7AA3B622C6B16A9E8B5A3BEA524C7ED54A6188D_AdjustorThunk },
	{ 0x06000106, XRFace_get_pose_m3792AF11CBB24361529B7291ED46B9DD2970AC54_AdjustorThunk },
	{ 0x06000107, XRFace_get_trackingState_m29CA0D89354ACC65EF8A18C09201BCBB3F732E3B_AdjustorThunk },
	{ 0x06000108, XRFace_get_nativePtr_m1EDCB59CD67423A2951DBA0DD0C98AB848183F06_AdjustorThunk },
	{ 0x06000109, XRFace_Equals_mB68B812AD74B02F7B8B82DE62EAE30146238DD3A_AdjustorThunk },
	{ 0x0600010A, XRFace_GetHashCode_mB3AC95DBCA3308827747473BF3A6044FCD2DD7B0_AdjustorThunk },
	{ 0x0600010B, XRFace_Equals_m1FF3F979A7F289C6CA77DD3290F4A50CAF889ED6_AdjustorThunk },
	{ 0x0600010C, XRFaceMesh_get_vertices_mD6CE3C180851CA10DE10A7B6C7BF0817E73609E9_AdjustorThunk },
	{ 0x0600010D, XRFaceMesh_get_normals_m8CF3D0395943001F6A681154DF7A75FA045CE6AE_AdjustorThunk },
	{ 0x0600010E, XRFaceMesh_get_indices_m7EA9FB6B6CE3484262F74546455CB08BA5B5B00D_AdjustorThunk },
	{ 0x0600010F, XRFaceMesh_get_uvs_mECB939F9E262D22AED47311D6985116FC0CEEA9B_AdjustorThunk },
	{ 0x06000110, XRFaceMesh_Dispose_m3E7A416718B532DFD7D100D5D0F1F3A9AED96F7E_AdjustorThunk },
	{ 0x06000111, XRFaceMesh_GetHashCode_mBB4E68260D980EAFD29333F2B0EE9B10FAA8040E_AdjustorThunk },
	{ 0x06000112, XRFaceMesh_Equals_m466E0A0D30D307B956CB677D1103AD4F4C86E3BC_AdjustorThunk },
	{ 0x06000113, XRFaceMesh_ToString_mC0D08232897F11B687916F6B1975927411A783B3_AdjustorThunk },
	{ 0x06000114, XRFaceMesh_Equals_m7F90AA84BD56C74C8C6472CE390C706E8AB8120D_AdjustorThunk },
	{ 0x06000135, Cinfo_get_id_m146AEBFC1F34906EF7CB853DB90B28AAA8449D98_AdjustorThunk },
	{ 0x06000136, Cinfo_set_id_mFA87FA52172846CBD4587F2E207D65A097B842E6_AdjustorThunk },
	{ 0x06000137, Cinfo_get_subsystemImplementationType_m3715508999808DEC6A812A1D228A2472ECEA48C4_AdjustorThunk },
	{ 0x06000138, Cinfo_set_subsystemImplementationType_mBBC866C6F4207A69F7351857F66D683E0EAF8FAF_AdjustorThunk },
	{ 0x06000139, Cinfo_get_supportsMovingImages_mAB2286D926C059743ACE6E6DABCFAAB7939AC80F_AdjustorThunk },
	{ 0x0600013A, Cinfo_set_supportsMovingImages_m2898F758BCCE6DDC222E828D855CEFC89C58EF2C_AdjustorThunk },
	{ 0x0600013B, Cinfo_GetHashCode_m30FEF663E95BCA50174085CAE5EC3E6B207CF094_AdjustorThunk },
	{ 0x0600013C, Cinfo_Equals_mB17F6F32907C286C2B660B6E9E1BCC20A651F941_AdjustorThunk },
	{ 0x0600013D, Cinfo_Equals_mB207ED3E11BCF5AEC1A56818611A0082753E8DBC_AdjustorThunk },
	{ 0x0600013E, XRReferenceImage_get_guid_m646CE46068C4BA601BC23772D3D807D18836B80C_AdjustorThunk },
	{ 0x0600013F, XRReferenceImage_ToString_m6BC181F25F28FCFB70B2035D7AAEFB1E6DDC6FAA_AdjustorThunk },
	{ 0x06000140, XRReferenceImage_GetHashCode_m5178E851EB53F2D106ACBB7C8330F889E1581C09_AdjustorThunk },
	{ 0x06000141, XRReferenceImage_Equals_mE3432D5A1D715669F9AEC65EB524B925549F4726_AdjustorThunk },
	{ 0x06000142, XRReferenceImage_Equals_m001D4B708546DF448C4243079684F6FDEE76FBC6_AdjustorThunk },
	{ 0x06000149, XRTrackedImage__ctor_m0D4DB0925EB1FBEC466A9D19C627F747A400408F_AdjustorThunk },
	{ 0x0600014B, XRTrackedImage_get_trackableId_m6EB6DBACC95E5EE2AFEE3CE421F4C123F32E9CB8_AdjustorThunk },
	{ 0x0600014C, XRTrackedImage_get_sourceImageId_mFEBFE1A21956E0CBF6828407DE0F2209610BF60A_AdjustorThunk },
	{ 0x0600014D, XRTrackedImage_get_pose_m0566E087CA2DC99DF749E80277510C61DCF13186_AdjustorThunk },
	{ 0x0600014E, XRTrackedImage_get_size_m746034D0E2FD28C9E48A90965E4FCD9137988906_AdjustorThunk },
	{ 0x0600014F, XRTrackedImage_get_trackingState_mA7177B042E8F9F9B584582970BC5FF0377CE94DB_AdjustorThunk },
	{ 0x06000150, XRTrackedImage_get_nativePtr_mE90A65D3EDE7F0190F36BD4BDF2E06FEAD113DAF_AdjustorThunk },
	{ 0x06000151, XRTrackedImage_GetHashCode_mFF30CD39BC82F7A636BF9E0ACF96967C46F07B5D_AdjustorThunk },
	{ 0x06000152, XRTrackedImage_Equals_m626B512ECA4BFBB14918EF13969F8789C3A8A069_AdjustorThunk },
	{ 0x06000153, XRTrackedImage_Equals_mF94BFA9B373C9899F29EBD1F01A15ADA2D6E47AF_AdjustorThunk },
	{ 0x06000156, BoundedPlane__ctor_mC6A5A4A0E3417618DAC201CE213664B9791BFACC_AdjustorThunk },
	{ 0x06000157, BoundedPlane_get_trackableId_mD2A3DCDA61898E637ACAE0A0F4A832931378071A_AdjustorThunk },
	{ 0x06000158, BoundedPlane_get_subsumedById_m77895AF3F9E056F4816CB9177209586E98967889_AdjustorThunk },
	{ 0x06000159, BoundedPlane_get_pose_m30B084D1F307CB46894BE4FAD448788001A0F0DF_AdjustorThunk },
	{ 0x0600015A, BoundedPlane_get_center_m1AB8ED2CBDC8F4A85358F98A8DD832436818505F_AdjustorThunk },
	{ 0x0600015B, BoundedPlane_get_extents_mC0F005CBFDA55C68EF9CC52FBC8C0FC07854CBBB_AdjustorThunk },
	{ 0x0600015C, BoundedPlane_get_size_mC49BFA58CD7CE92C2470C82F30EA6F8A4F38B854_AdjustorThunk },
	{ 0x0600015D, BoundedPlane_get_alignment_m85C46D81426435F0B9399ABAF80AC5342A2F7137_AdjustorThunk },
	{ 0x0600015E, BoundedPlane_get_trackingState_m9B6467E433564ABE4BED9328F7B23AB05905B008_AdjustorThunk },
	{ 0x0600015F, BoundedPlane_get_nativePtr_mF0C7299B1CD00C40972DE1BE13C411594A59D361_AdjustorThunk },
	{ 0x06000160, BoundedPlane_ToString_m0EE069A6B9579B564EFD59C300277FB7D824E7D1_AdjustorThunk },
	{ 0x06000161, BoundedPlane_Equals_m74FDA713E8EBBF9256546B4B04A1CCF214ED7D8E_AdjustorThunk },
	{ 0x06000162, BoundedPlane_GetHashCode_m39BDD70727BE818F86E2DEEF4E264FE13368B637_AdjustorThunk },
	{ 0x06000163, BoundedPlane_Equals_mF06B1E1B2C53F0BF0FA541CD4828DFD16E8D789D_AdjustorThunk },
	{ 0x0600017B, Cinfo_get_id_m68FCB4621B357B690E4C43846221E241455A1D99_AdjustorThunk },
	{ 0x0600017C, Cinfo_set_id_m9211F9ADC4DCFA1AAB5AA9F662EE6510D6FE01EF_AdjustorThunk },
	{ 0x0600017D, Cinfo_get_subsystemImplementationType_mF924CAAE546DCF3AB899CD4ED8AC2B75B4B0706C_AdjustorThunk },
	{ 0x0600017E, Cinfo_set_subsystemImplementationType_m74075897385685A6E0753F2EE29CD77A90A22E6B_AdjustorThunk },
	{ 0x0600017F, Cinfo_get_supportsHorizontalPlaneDetection_m0FB9CC965AD3A99988175C8D1669D341BFB72214_AdjustorThunk },
	{ 0x06000180, Cinfo_set_supportsHorizontalPlaneDetection_m6BA5B6FD1C2FDF236AEE15957FD1F1837C394304_AdjustorThunk },
	{ 0x06000181, Cinfo_get_supportsVerticalPlaneDetection_m3C7470BF80CEB7F507540383C2BB02A5B2201AAE_AdjustorThunk },
	{ 0x06000182, Cinfo_set_supportsVerticalPlaneDetection_m386B3816E8C1538AB58318D55D9C64D1113C1B3B_AdjustorThunk },
	{ 0x06000183, Cinfo_get_supportsArbitraryPlaneDetection_mC75F86CCCBA2ED8AD6283EF458F3897CB78DAB3C_AdjustorThunk },
	{ 0x06000184, Cinfo_set_supportsArbitraryPlaneDetection_m625EDF8616A904C1D2C3B9DB1B52A28A0D3EAF06_AdjustorThunk },
	{ 0x06000185, Cinfo_get_supportsBoundaryVertices_m6E514112FA9F617953047AF6C2CD98895D587E84_AdjustorThunk },
	{ 0x06000186, Cinfo_set_supportsBoundaryVertices_mFC986523905272E58728731CEE06B47DD4ECAC3D_AdjustorThunk },
	{ 0x06000187, Cinfo_Equals_mE08723A87EF2FC94FD64BDD93A176F3497518F7D_AdjustorThunk },
	{ 0x06000188, Cinfo_Equals_m3B3C974BB1055B352CAAE2961A7374DA559049F8_AdjustorThunk },
	{ 0x06000189, Cinfo_GetHashCode_m9F3BA097C011CC6998676138C52EF6306C09BBA0_AdjustorThunk },
	{ 0x06000194, XRRaycastHit_get_trackableId_mAECCB1BE08FB0B5A48CB27D955250FE2068492CF_AdjustorThunk },
	{ 0x06000195, XRRaycastHit_get_pose_mE0B0A754E818C6FF3675A41CA95185A3E608C8C3_AdjustorThunk },
	{ 0x06000196, XRRaycastHit_get_distance_mCD38ECEDD0FA6EAFEFEC71DB7EE3CF1B82B5CEFE_AdjustorThunk },
	{ 0x06000197, XRRaycastHit_get_hitType_m52BBF5DBDE1B3E7E01571EE029F68EB29E240DA6_AdjustorThunk },
	{ 0x06000198, XRRaycastHit__ctor_mFE2DF7F0A38F4507B3C5684B78F0694266BB76B5_AdjustorThunk },
	{ 0x06000199, XRRaycastHit_GetHashCode_mB0A7A65C634E1CA9C70DC17D2B31A6E082D349EA_AdjustorThunk },
	{ 0x0600019A, XRRaycastHit_Equals_m80D2CC8EEC73127B553C601D9B6A3CEDCFBCF862_AdjustorThunk },
	{ 0x0600019B, XRRaycastHit_Equals_mD3774307DB6D9200AE2C4703CF2CB2D90616051C_AdjustorThunk },
	{ 0x060001B1, Cinfo_get_id_mCCD575E1E7E6E7E7166A4B7F0AF9E7F023FC3FCA_AdjustorThunk },
	{ 0x060001B2, Cinfo_set_id_mDBC061879B3E989FF064E7E31CFC85ACD142199B_AdjustorThunk },
	{ 0x060001B3, Cinfo_get_subsystemImplementationType_m65BDDAA14217AD17F0928287A8428D7B33A3634F_AdjustorThunk },
	{ 0x060001B4, Cinfo_set_subsystemImplementationType_m9670297F5DC91608B606E2B8A7E4C2643236D65A_AdjustorThunk },
	{ 0x060001B5, Cinfo_get_supportsViewportBasedRaycast_m8D0A14E0E43F99FCB89A3D87F954D657ADC0C514_AdjustorThunk },
	{ 0x060001B6, Cinfo_set_supportsViewportBasedRaycast_m42B64A1095C52F16217EBF1D5ABFD7353DA35233_AdjustorThunk },
	{ 0x060001B7, Cinfo_get_supportsWorldBasedRaycast_m327E8B0EE9C3103FBF490CA62FE5E1A51EF62C9F_AdjustorThunk },
	{ 0x060001B8, Cinfo_set_supportsWorldBasedRaycast_mBF04DD8B3208A7D9C98419FEDC8CB012F7253DF5_AdjustorThunk },
	{ 0x060001B9, Cinfo_get_supportedTrackableTypes_mCC57E28DFCE93ECA772B1DE2E3E49AD030D79424_AdjustorThunk },
	{ 0x060001BA, Cinfo_set_supportedTrackableTypes_m13138A57079E692472B33A4B216D5568852BE652_AdjustorThunk },
	{ 0x060001BB, Cinfo_GetHashCode_m155C084A788BBE99BBD85B2B06D7CC74DF76D636_AdjustorThunk },
	{ 0x060001BC, Cinfo_Equals_m434EDD8246018244C8CBBD881D91CCDBF5437EF6_AdjustorThunk },
	{ 0x060001BD, Cinfo_ToString_m2DFF2387C99F28931C7E802878564A3694D51453_AdjustorThunk },
	{ 0x060001BE, Cinfo_Equals_m21B31135C0E69990482BE0286436FF51748CF92B_AdjustorThunk },
	{ 0x060001C0, XRReferencePoint__ctor_mD4597D205E31CBA88C523E616E7A45D78C95BF3E_AdjustorThunk },
	{ 0x060001C1, XRReferencePoint_get_trackableId_m6D53542802F2444CE58861B8868274F9A8296D88_AdjustorThunk },
	{ 0x060001C2, XRReferencePoint_get_pose_mA4320629B8C7AE23D97FCD8E2C5FB9C9FB6AED9C_AdjustorThunk },
	{ 0x060001C3, XRReferencePoint_get_trackingState_mBA0DB4050B734039D22D0ECF69CD6E8896DF52B8_AdjustorThunk },
	{ 0x060001C4, XRReferencePoint_get_nativePtr_m51D5CA830D54C1D67656E683BE81A53D479E71A1_AdjustorThunk },
	{ 0x060001C5, XRReferencePoint_GetHashCode_mAA9EDDD897E7DDD88AA10A7B9E10FC2025E4F7CA_AdjustorThunk },
	{ 0x060001C6, XRReferencePoint_Equals_m4783954B81A2A4496B718B1940FEB8D9BD99C15D_AdjustorThunk },
	{ 0x060001C7, XRReferencePoint_Equals_m63D1CD2AE3191F649B2791970AB507AA379B78FA_AdjustorThunk },
	{ 0x060001DC, Cinfo_get_id_m915E41A74906A167A3AD5FA288FC098F301167D9_AdjustorThunk },
	{ 0x060001DD, Cinfo_set_id_m022C41DDBCD030680C14FA11F178C96FCE67D687_AdjustorThunk },
	{ 0x060001DE, Cinfo_get_subsystemImplementationType_m66047A62536BD79CB4452C2FE4878FFE035293C2_AdjustorThunk },
	{ 0x060001DF, Cinfo_set_subsystemImplementationType_m713DB30ECC2AFC3D45F60BE6910713C6009A99E0_AdjustorThunk },
	{ 0x060001E0, Cinfo_get_supportsTrackableAttachments_mE8629D11414D8E706864D0F179E39CA063035137_AdjustorThunk },
	{ 0x060001E1, Cinfo_set_supportsTrackableAttachments_m1A6C2F5CE68A65944F1631E0E32FFC6BC04ECB03_AdjustorThunk },
	{ 0x060001E2, Cinfo_GetHashCode_m44A7F0571AF0F4CEFECFDF9347EA517EA62F8E5D_AdjustorThunk },
	{ 0x060001E3, Cinfo_Equals_m48D047F332BBAC4B19A45B966678D92CADC6F0F2_AdjustorThunk },
	{ 0x060001E4, Cinfo_Equals_m62C82D4C3A17A329222BC38370B76CDB219ED6F3_AdjustorThunk },
	{ 0x060001E5, SerializableGuid_get_guid_mBBAACA6CC4257BB65BB6178DB4B9B8273462D71B_AdjustorThunk },
	{ 0x060001E6, SerializableGuid_GetHashCode_m2220A2C721DACD85F1271B8D17D334323B0CAD2D_AdjustorThunk },
	{ 0x060001E7, SerializableGuid_Equals_m0775AC30655B576CF3FC2E92FCD210A864264994_AdjustorThunk },
	{ 0x060001E8, SerializableGuid_ToString_mDC25F3F328C27C046B6BAD13E5839C363B4BF98C_AdjustorThunk },
	{ 0x060001E9, SerializableGuid_Equals_mE819A7AFF3AC4EE0DBF8AC4811E3E930367DDFEC_AdjustorThunk },
	{ 0x06000212, Cinfo_get_supportsInstall_m010F6B6254015F5F477114A35C4F11F4A3334E2E_AdjustorThunk },
	{ 0x06000213, Cinfo_set_supportsInstall_m4295AB46C19802B003C61D7EB79DC8D02CF14B80_AdjustorThunk },
	{ 0x06000214, Cinfo_get_supportsMatchFrameRate_m7DD61A2A7B767E475C97DF33FA4785C2DC12B6E3_AdjustorThunk },
	{ 0x06000215, Cinfo_set_supportsMatchFrameRate_mE43FF83622414EA44D02418EC98B1DA8DDFFDBD6_AdjustorThunk },
	{ 0x06000216, Cinfo_get_id_m038FCFC448004E0F21D8DF6F22FBFA0F5AE14870_AdjustorThunk },
	{ 0x06000217, Cinfo_set_id_m8E2A1220FE77B46B870237AE788DFEE34F6C29CB_AdjustorThunk },
	{ 0x06000218, Cinfo_get_subsystemImplementationType_m27F97E21F5CC1903B5358E2C386343D53F3379BF_AdjustorThunk },
	{ 0x06000219, Cinfo_set_subsystemImplementationType_m9591600428F1364957BEDD8C12C1B734BBA2BF85_AdjustorThunk },
	{ 0x0600021A, Cinfo_GetHashCode_m10FAF407C61975E8F03E5F4B961BC2583EA369A9_AdjustorThunk },
	{ 0x0600021B, Cinfo_Equals_m2AB456C7BFFE6923D76AE087F4548493143B1B7B_AdjustorThunk },
	{ 0x0600021C, Cinfo_Equals_m1FEB5A86DE5F73249471CC686193B327ED687B67_AdjustorThunk },
	{ 0x0600021D, XRSessionUpdateParams_get_screenOrientation_m111C145EA6A683F025DF48C6EA355E37D8974183_AdjustorThunk },
	{ 0x0600021E, XRSessionUpdateParams_set_screenOrientation_m977BF9AC1B8FF7224144F0979A8A30325256EE12_AdjustorThunk },
	{ 0x0600021F, XRSessionUpdateParams_get_screenDimensions_m61A9722E272D6292B9C7C093BF7792FB007BF21E_AdjustorThunk },
	{ 0x06000220, XRSessionUpdateParams_set_screenDimensions_m74048D3192BAF559FEFAB878921C3EBB68ACB635_AdjustorThunk },
	{ 0x06000221, XRSessionUpdateParams_GetHashCode_m5D06AEAC2DD5497E37C5C8A1E06952186ACDCC70_AdjustorThunk },
	{ 0x06000222, XRSessionUpdateParams_Equals_m0D01CBAE986724E42B3FF0EBE51808915F873B92_AdjustorThunk },
	{ 0x06000223, XRSessionUpdateParams_ToString_mC2C61A95F598C42B879A6E20982DA96B16E23B6D_AdjustorThunk },
	{ 0x06000224, XRSessionUpdateParams_Equals_mAA0877F7CE8BCEC50F568C257794F50C3A4BFDB8_AdjustorThunk },
	{ 0x06000226, TrackableId_get_subId1_mC31404D1705A36B5554158D187B8E91107425D4D_AdjustorThunk },
	{ 0x06000227, TrackableId_get_subId2_m42D794430D6C6163D2F5F839A58383F5BB628AC0_AdjustorThunk },
	{ 0x06000228, TrackableId__ctor_m620606AC246BEEC637A748B22FAB588CB93120B8_AdjustorThunk },
	{ 0x06000229, TrackableId_ToString_m9C04F12E2DA81C481BAABC989B14E8B3509DADB2_AdjustorThunk },
	{ 0x0600022A, TrackableId_GetHashCode_m6F1171936847F6A193255FABDCA3772D7AE57328_AdjustorThunk },
	{ 0x0600022B, TrackableId_Equals_m513A2978A536C42A1AFE1A4D42BEE78EE056BD28_AdjustorThunk },
	{ 0x0600022C, TrackableId_Equals_mB8FF48A7F895DEE1E826EDD6126475258BE9ADC2_AdjustorThunk },
	{ 0x0600023C, XRTextureDescriptor_get_nativeTexture_mC7FFC8C9D5E8C5BBD93F7A7E95B29253FD59770B_AdjustorThunk },
	{ 0x0600023D, XRTextureDescriptor_get_width_m66B9E821EBE5FEBAB7A9B589A056462FD2E35D04_AdjustorThunk },
	{ 0x0600023E, XRTextureDescriptor_get_height_mFC30414502C03B7BDD149DFFC374ACE0BD472755_AdjustorThunk },
	{ 0x0600023F, XRTextureDescriptor_get_mipmapCount_m206E924935A23EB8B06CE8FC1E98BC74B09247F3_AdjustorThunk },
	{ 0x06000240, XRTextureDescriptor_get_format_mA2DA22DC1DEBCAD27A9C69F3374D614DF1C3FA2B_AdjustorThunk },
	{ 0x06000241, XRTextureDescriptor_get_propertyNameId_mCF70D57DDDB24C2F3521DA86DFC1131DB29B8D80_AdjustorThunk },
	{ 0x06000242, XRTextureDescriptor_get_valid_mBDA97DA3C73C0B8282A29606BFD70F1C29C0B4AE_AdjustorThunk },
	{ 0x06000243, XRTextureDescriptor_hasIdenticalTextureMetadata_m7EF6E9887C77831839D7AECC3B8E51022821A5A8_AdjustorThunk },
	{ 0x06000244, XRTextureDescriptor_Equals_m8198CAFC2D9A7FA7941C2B587F6FB1B9FE5918EB_AdjustorThunk },
	{ 0x06000245, XRTextureDescriptor_Equals_m162675AC01545EA2A8149CE27A70E811C9A7B3D5_AdjustorThunk },
	{ 0x06000246, XRTextureDescriptor_GetHashCode_m686D69173CDCFB2CB96E32C948A0EEC176ED19AD_AdjustorThunk },
	{ 0x06000247, XRTextureDescriptor_ToString_m8F80DF64DD7FC44FBFB6FFD42FA3B7265FB016C2_AdjustorThunk },
};
static const int32_t s_InvokerIndices[583] = 
{
	10,
	10,
	772,
	14,
	10,
	9,
	2154,
	172,
	726,
	726,
	1569,
	1706,
	1706,
	89,
	89,
	89,
	89,
	89,
	89,
	2155,
	9,
	10,
	14,
	2156,
	2157,
	10,
	10,
	10,
	32,
	10,
	32,
	335,
	3,
	2158,
	2159,
	23,
	10,
	9,
	2160,
	14,
	2161,
	2156,
	10,
	10,
	10,
	2162,
	9,
	14,
	2163,
	9,
	10,
	14,
	334,
	334,
	334,
	334,
	32,
	2164,
	9,
	10,
	14,
	89,
	23,
	32,
	32,
	23,
	23,
	23,
	2165,
	14,
	842,
	2166,
	2167,
	2168,
	89,
	14,
	2169,
	89,
	842,
	2170,
	32,
	14,
	89,
	89,
	23,
	23,
	23,
	2169,
	30,
	30,
	842,
	2171,
	2167,
	2168,
	2172,
	842,
	32,
	23,
	124,
	2173,
	2174,
	26,
	10,
	2156,
	10,
	463,
	10,
	2175,
	9,
	10,
	14,
	15,
	10,
	10,
	10,
	2176,
	9,
	10,
	14,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	2177,
	9,
	10,
	2178,
	31,
	31,
	31,
	31,
	31,
	31,
	31,
	2179,
	23,
	23,
	23,
	23,
	2180,
	2181,
	14,
	23,
	23,
	23,
	2182,
	2181,
	23,
	2183,
	31,
	31,
	31,
	2184,
	89,
	31,
	89,
	31,
	89,
	31,
	10,
	32,
	2185,
	9,
	10,
	2186,
	2187,
	2188,
	2189,
	10,
	10,
	2190,
	9,
	2191,
	2192,
	2193,
	2194,
	2195,
	23,
	10,
	9,
	14,
	2196,
	2197,
	2188,
	2198,
	1539,
	2189,
	2199,
	1539,
	2200,
	10,
	15,
	2201,
	9,
	10,
	14,
	28,
	23,
	89,
	31,
	2202,
	23,
	23,
	23,
	2203,
	2204,
	14,
	2205,
	23,
	23,
	23,
	31,
	2203,
	2204,
	2206,
	23,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	2207,
	9,
	10,
	2208,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	31,
	2209,
	2210,
	2188,
	2189,
	10,
	15,
	9,
	10,
	2211,
	2191,
	2191,
	2212,
	2213,
	23,
	10,
	9,
	14,
	2214,
	23,
	23,
	23,
	23,
	89,
	2215,
	2216,
	14,
	23,
	23,
	23,
	89,
	2216,
	2217,
	2218,
	23,
	23,
	23,
	23,
	26,
	2219,
	32,
	14,
	23,
	2220,
	26,
	32,
	23,
	89,
	31,
	2221,
	2222,
	14,
	26,
	14,
	26,
	89,
	31,
	10,
	2223,
	9,
	714,
	14,
	10,
	9,
	2224,
	10,
	2225,
	2226,
	2227,
	714,
	23,
	2228,
	2229,
	2188,
	714,
	2189,
	1549,
	10,
	15,
	10,
	2230,
	9,
	-1,
	2231,
	2232,
	2188,
	2188,
	2189,
	1549,
	1549,
	1549,
	10,
	10,
	15,
	14,
	9,
	10,
	2233,
	23,
	23,
	23,
	23,
	32,
	2234,
	2216,
	14,
	-1,
	23,
	23,
	23,
	2216,
	2235,
	32,
	23,
	31,
	31,
	31,
	89,
	31,
	2236,
	2237,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	2238,
	9,
	10,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2239,
	2188,
	2189,
	726,
	10,
	2240,
	10,
	9,
	2241,
	89,
	23,
	23,
	23,
	23,
	2242,
	2243,
	14,
	23,
	23,
	23,
	2244,
	2245,
	23,
	89,
	31,
	89,
	31,
	32,
	2246,
	2247,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	10,
	32,
	10,
	9,
	14,
	2248,
	2249,
	2187,
	2188,
	2189,
	10,
	15,
	10,
	2250,
	9,
	23,
	23,
	23,
	23,
	2251,
	2252,
	2253,
	2204,
	14,
	23,
	23,
	23,
	2254,
	2252,
	2253,
	2204,
	23,
	31,
	2255,
	2256,
	14,
	26,
	14,
	26,
	89,
	31,
	10,
	9,
	2257,
	714,
	10,
	9,
	14,
	2258,
	46,
	46,
	89,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	2259,
	23,
	23,
	10,
	10,
	89,
	31,
	10,
	14,
	23,
	23,
	2259,
	23,
	23,
	23,
	23,
	14,
	14,
	10,
	10,
	89,
	31,
	10,
	23,
	89,
	31,
	89,
	31,
	2260,
	2261,
	89,
	31,
	89,
	31,
	14,
	26,
	14,
	26,
	10,
	9,
	2262,
	10,
	32,
	2156,
	2157,
	10,
	9,
	14,
	2263,
	2264,
	172,
	172,
	973,
	14,
	10,
	9,
	2204,
	2265,
	3,
	2188,
	2189,
	10,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	15,
	10,
	10,
	10,
	10,
	10,
	89,
	2266,
	2266,
	9,
	10,
	14,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[1] = 
{
	{ 0x06000023, 7,  (void**)&XRCameraImage_OnAsyncConversionComplete_m8C475A6334B19FAC20896F0DFEFBDEAC6E5EE53B_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[6] = 
{
	{ 0x02000037, { 9, 4 } },
	{ 0x02000038, { 13, 3 } },
	{ 0x02000050, { 16, 4 } },
	{ 0x02000051, { 20, 2 } },
	{ 0x06000154, { 0, 4 } },
	{ 0x0600016C, { 4, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[22] = 
{
	{ (Il2CppRGCTXDataType)3, 20722 },
	{ (Il2CppRGCTXDataType)3, 20723 },
	{ (Il2CppRGCTXDataType)3, 20724 },
	{ (Il2CppRGCTXDataType)3, 20725 },
	{ (Il2CppRGCTXDataType)3, 20726 },
	{ (Il2CppRGCTXDataType)3, 20727 },
	{ (Il2CppRGCTXDataType)3, 20728 },
	{ (Il2CppRGCTXDataType)2, 28838 },
	{ (Il2CppRGCTXDataType)3, 20729 },
	{ (Il2CppRGCTXDataType)3, 20730 },
	{ (Il2CppRGCTXDataType)2, 33135 },
	{ (Il2CppRGCTXDataType)3, 20731 },
	{ (Il2CppRGCTXDataType)3, 20732 },
	{ (Il2CppRGCTXDataType)3, 20733 },
	{ (Il2CppRGCTXDataType)2, 28852 },
	{ (Il2CppRGCTXDataType)3, 20734 },
	{ (Il2CppRGCTXDataType)3, 20735 },
	{ (Il2CppRGCTXDataType)3, 20736 },
	{ (Il2CppRGCTXDataType)3, 20737 },
	{ (Il2CppRGCTXDataType)3, 20738 },
	{ (Il2CppRGCTXDataType)3, 20739 },
	{ (Il2CppRGCTXDataType)2, 28902 },
};
extern const Il2CppCodeGenModule g_Unity_XR_ARSubsystemsCodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_ARSubsystemsCodeGenModule = 
{
	"Unity.XR.ARSubsystems.dll",
	583,
	s_methodPointers,
	308,
	s_adjustorThunks,
	s_InvokerIndices,
	1,
	s_reversePInvokeIndices,
	6,
	s_rgctxIndices,
	22,
	s_rgctxValues,
	NULL,
};
