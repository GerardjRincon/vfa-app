﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* NativeRecorder_OnRecording_m6E2D200EE07CE507951155CB24CC8107AFE8D13B_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 <id>j__TPar <>f__AnonymousType0`4::get_id()
// 0x00000002 <seq>j__TPar <>f__AnonymousType0`4::get_seq()
// 0x00000003 <name>j__TPar <>f__AnonymousType0`4::get_name()
// 0x00000004 <data>j__TPar <>f__AnonymousType0`4::get_data()
// 0x00000005 System.Void <>f__AnonymousType0`4::.ctor(<id>j__TPar,<seq>j__TPar,<name>j__TPar,<data>j__TPar)
// 0x00000006 System.Boolean <>f__AnonymousType0`4::Equals(System.Object)
// 0x00000007 System.Int32 <>f__AnonymousType0`4::GetHashCode()
// 0x00000008 System.String <>f__AnonymousType0`4::ToString()
// 0x00000009 System.Void CipherTool::Start()
extern void CipherTool_Start_m1BC1C61A2276818DBBDA04CAA330964DEF75A708 (void);
// 0x0000000A System.Void CipherTool::LoadAnimation()
extern void CipherTool_LoadAnimation_mCEA8D22339C99D33C752FE2DA2A1E5543CEF18BE (void);
// 0x0000000B System.Void CipherTool::SaveAnimation()
extern void CipherTool_SaveAnimation_m1D1BA906F27739FFECEDD64F63D3914BD93CAEE7 (void);
// 0x0000000C System.Void CipherTool::.ctor()
extern void CipherTool__ctor_m414219A0986AFE3A20D1784AC1345F7E664F1DB7 (void);
// 0x0000000D System.Void DataBundle::.ctor()
extern void DataBundle__ctor_mAFACFD9897E0F84D3EE54444119373F23EE89203 (void);
// 0x0000000E System.Void AudioLoader::Start()
extern void AudioLoader_Start_m1D06BEC87E773193385644E6D12EB693B4420BE7 (void);
// 0x0000000F System.Void AudioLoader::LoadAudio()
extern void AudioLoader_LoadAudio_m666D6550C8EDF3AE4420245437966C09BC448856 (void);
// 0x00000010 System.Collections.IEnumerator AudioLoader::LoadHelper(System.String)
extern void AudioLoader_LoadHelper_m3736883064E9C22D0889185745442FA473D2FEE4 (void);
// 0x00000011 System.Void AudioLoader::Update()
extern void AudioLoader_Update_m817CF291BC2BA4E9BE0C351CB1C1ADEED6911DB4 (void);
// 0x00000012 System.Void AudioLoader::.ctor()
extern void AudioLoader__ctor_m578F6C13779E91F8FE265716882F39D91789EBC0 (void);
// 0x00000013 System.Void OldGUIExamplesCS::Start()
extern void OldGUIExamplesCS_Start_m5C679AD607A16C85DDFAE78B996120E58E8989E9 (void);
// 0x00000014 System.Void OldGUIExamplesCS::catMoved()
extern void OldGUIExamplesCS_catMoved_m844C01EC2655FB6AEA6261F74BE3AFE380B195A7 (void);
// 0x00000015 System.Void OldGUIExamplesCS::OnGUI()
extern void OldGUIExamplesCS_OnGUI_mE08A1CA5C406551B5D66A4A6BB86E00285F97C30 (void);
// 0x00000016 System.Void OldGUIExamplesCS::.ctor()
extern void OldGUIExamplesCS__ctor_m626C9BA19A539E78953626625960526EDB180F4A (void);
// 0x00000017 System.Void TestingPunch::Start()
extern void TestingPunch_Start_mB88A524025639F5F9A18D9B82E98E30EB3356D06 (void);
// 0x00000018 System.Void TestingPunch::Update()
extern void TestingPunch_Update_m9B5977F7EA9B1FC0F7D69A7E202DC2FA38BBEAA8 (void);
// 0x00000019 System.Void TestingPunch::tweenStatically(UnityEngine.GameObject)
extern void TestingPunch_tweenStatically_m8E1F7A46E2EAAB6AEDD62E0C2428EC4EBAD0C7F3 (void);
// 0x0000001A System.Void TestingPunch::enterMiniGameStart(System.Object)
extern void TestingPunch_enterMiniGameStart_m2FAE7161DDC3665930335EABF53A611D1DDA65A1 (void);
// 0x0000001B System.Void TestingPunch::updateColor(UnityEngine.Color)
extern void TestingPunch_updateColor_mFA69FF82ECDF6CD2EC12354370879F019E15D477 (void);
// 0x0000001C System.Void TestingPunch::delayedMethod(System.Object)
extern void TestingPunch_delayedMethod_mF8E7A6CE3054ACE65057D43047ECAB6E458C4E84 (void);
// 0x0000001D System.Void TestingPunch::destroyOnComp(System.Object)
extern void TestingPunch_destroyOnComp_m0D7E0E3DA9113B3B5F265EBC69AEB3CC70D63E13 (void);
// 0x0000001E System.String TestingPunch::curveToString(UnityEngine.AnimationCurve)
extern void TestingPunch_curveToString_mDE53974600DB58372C612CB078B663510C667F61 (void);
// 0x0000001F System.Void TestingPunch::.ctor()
extern void TestingPunch__ctor_m9A8492FDD64928449DB0DB92CD16061D642E4A6A (void);
// 0x00000020 System.Void TestingPunch::<Update>b__4_0()
extern void TestingPunch_U3CUpdateU3Eb__4_0_m8FB81D468DBC883BE134CF8314A05D3482BE566C (void);
// 0x00000021 System.Void TestingPunch::<Update>b__4_3()
extern void TestingPunch_U3CUpdateU3Eb__4_3_m542BB1341B4AECC188C74DCBE4F49CA2AB8AC4BE (void);
// 0x00000022 System.Void TestingPunch::<Update>b__4_7(UnityEngine.Vector2)
extern void TestingPunch_U3CUpdateU3Eb__4_7_m0D94A53B95AF073898CBDEAD58DC696E6E060066 (void);
// 0x00000023 System.Void TestingRigidbodyCS::Start()
extern void TestingRigidbodyCS_Start_m936906C87BBDA6BF06D51D430EF08BB8D14E0376 (void);
// 0x00000024 System.Void TestingRigidbodyCS::Update()
extern void TestingRigidbodyCS_Update_mFA7EB03B37C1B2DC3C7ABA84488484149155828D (void);
// 0x00000025 System.Void TestingRigidbodyCS::.ctor()
extern void TestingRigidbodyCS__ctor_mA1DCB32BECE6491D4E8B9F8E06979C6F2B775516 (void);
// 0x00000026 System.Void Following::Start()
extern void Following_Start_mFEEA46D15CD9B280B5E7F418BAB4456C730EF556 (void);
// 0x00000027 System.Void Following::Update()
extern void Following_Update_m0909311BA8C83F7F2448084BAFC093C62C012B76 (void);
// 0x00000028 System.Void Following::moveArrow()
extern void Following_moveArrow_m6B308968FAA0842DE5E36D17DFF4CFADC87BDE64 (void);
// 0x00000029 System.Void Following::.ctor()
extern void Following__ctor_m47A2E77D28030BA448B3705FBD95D3EF1C0EF5DE (void);
// 0x0000002A System.Void GeneralAdvancedTechniques::Start()
extern void GeneralAdvancedTechniques_Start_mA1FF0FADF625671F926CC2E4D0CB77B979301564 (void);
// 0x0000002B System.Void GeneralAdvancedTechniques::.ctor()
extern void GeneralAdvancedTechniques__ctor_m54D6A82A08BC2E749DA9363763E710312B79E899 (void);
// 0x0000002C System.Void GeneralAdvancedTechniques::<Start>b__10_0(System.Single)
extern void GeneralAdvancedTechniques_U3CStartU3Eb__10_0_mBEBB18A1DC341C34F99BEF02138E30A577DC63C0 (void);
// 0x0000002D System.Void GeneralBasic::Start()
extern void GeneralBasic_Start_m722ABC7DD46AD33613ED9A99550009B1388A2478 (void);
// 0x0000002E System.Void GeneralBasic::advancedExamples()
extern void GeneralBasic_advancedExamples_mC9210C18842768B7D2E99936EC766E18D85C8773 (void);
// 0x0000002F System.Void GeneralBasic::.ctor()
extern void GeneralBasic__ctor_mA0A6D418EBB951CF5EF97DCDE5E62CCC6D85D31F (void);
// 0x00000030 System.Void GeneralBasic::<advancedExamples>b__2_0()
extern void GeneralBasic_U3CadvancedExamplesU3Eb__2_0_m6A1F674D4183516100E64513ACC50585EBA731E8 (void);
// 0x00000031 System.Void GeneralBasics2d::Start()
extern void GeneralBasics2d_Start_mE3215316598A87E2CF0D9833F686D13FDC86DF2C (void);
// 0x00000032 UnityEngine.GameObject GeneralBasics2d::createSpriteDude(System.String,UnityEngine.Vector3,System.Boolean)
extern void GeneralBasics2d_createSpriteDude_mBB652175A3D6AE7952BBE8C0E1226864D79928EA (void);
// 0x00000033 System.Void GeneralBasics2d::advancedExamples()
extern void GeneralBasics2d_advancedExamples_mAC58CC5F094D9E634ACE4F04656C6A484C6D52C3 (void);
// 0x00000034 System.Void GeneralBasics2d::.ctor()
extern void GeneralBasics2d__ctor_mFEB91E190B0738BC50845B33FAE252A2B41D55C7 (void);
// 0x00000035 System.Void GeneralBasics2d::<advancedExamples>b__4_0()
extern void GeneralBasics2d_U3CadvancedExamplesU3Eb__4_0_m05957C9477E9A908C2EAF3BD0AD001EE3DC0D881 (void);
// 0x00000036 System.Void GeneralCameraShake::Start()
extern void GeneralCameraShake_Start_mB1EE5CBBF521F4D697F8EAF2F53600826F54DABA (void);
// 0x00000037 System.Void GeneralCameraShake::bigGuyJump()
extern void GeneralCameraShake_bigGuyJump_mE82096DC9C47AFAB293E2B8BAD0B7F2153063681 (void);
// 0x00000038 System.Void GeneralCameraShake::.ctor()
extern void GeneralCameraShake__ctor_mA22AEAD53E34A074E7DBC77BC3164C64814270D8 (void);
// 0x00000039 System.Void GeneralEasingTypes::Start()
extern void GeneralEasingTypes_Start_mBDA1C8799607E8C3D6052D0B88E5E96ED8892184 (void);
// 0x0000003A System.Void GeneralEasingTypes::demoEaseTypes()
extern void GeneralEasingTypes_demoEaseTypes_mAD63F64427B735F85C75265777B1EA2C6038B422 (void);
// 0x0000003B System.Void GeneralEasingTypes::resetLines()
extern void GeneralEasingTypes_resetLines_m46F8B9B5ADFB7348E099993918DB8EAA1108B746 (void);
// 0x0000003C System.Void GeneralEasingTypes::.ctor()
extern void GeneralEasingTypes__ctor_mB2B0932931E070FA4BA5C24CFCFBBBF894E534BB (void);
// 0x0000003D System.Void GeneralEventsListeners::Awake()
extern void GeneralEventsListeners_Awake_mBAFA4AE2A5056FBFED34D12A4776985220D66AF7 (void);
// 0x0000003E System.Void GeneralEventsListeners::Start()
extern void GeneralEventsListeners_Start_m6B12B5EE52523BBA2008F17CA57A49B0ECCB619A (void);
// 0x0000003F System.Void GeneralEventsListeners::jumpUp(LTEvent)
extern void GeneralEventsListeners_jumpUp_mC8BCDFB7169C9F7EBC42A5860DBB0BAF05612E70 (void);
// 0x00000040 System.Void GeneralEventsListeners::changeColor(LTEvent)
extern void GeneralEventsListeners_changeColor_m6FBDF19D2457EA40303B510C9B9C4206943A8D09 (void);
// 0x00000041 System.Void GeneralEventsListeners::OnCollisionEnter(UnityEngine.Collision)
extern void GeneralEventsListeners_OnCollisionEnter_m43F8FDFAC97EC09E53E98849F68469FCA7881F5F (void);
// 0x00000042 System.Void GeneralEventsListeners::OnCollisionStay(UnityEngine.Collision)
extern void GeneralEventsListeners_OnCollisionStay_mC15D1654A132C57A85C90F0A9D1A0489481B20D2 (void);
// 0x00000043 System.Void GeneralEventsListeners::FixedUpdate()
extern void GeneralEventsListeners_FixedUpdate_mF30FB01A225EFB50EB4714A0110DD6668F9715D0 (void);
// 0x00000044 System.Void GeneralEventsListeners::OnMouseDown()
extern void GeneralEventsListeners_OnMouseDown_m7961A64655D9C0BC7401ACCD7A54741F5440C659 (void);
// 0x00000045 System.Void GeneralEventsListeners::.ctor()
extern void GeneralEventsListeners__ctor_mFE5DA13A6BE436986ADB09018AC3C641A1DD5A3A (void);
// 0x00000046 System.Void GeneralEventsListeners::<changeColor>b__8_0(UnityEngine.Color)
extern void GeneralEventsListeners_U3CchangeColorU3Eb__8_0_mF7603717CE44B39A97A3F80DC96F80E43A2DD33B (void);
// 0x00000047 System.Void GeneralSequencer::Start()
extern void GeneralSequencer_Start_mE22252603584E9595014C8CEAC0006FD9B55DBE7 (void);
// 0x00000048 System.Void GeneralSequencer::.ctor()
extern void GeneralSequencer__ctor_mCF1E29ADF0F70414A63D0F950D0FAB09AB1F0C8E (void);
// 0x00000049 System.Void GeneralSequencer::<Start>b__4_0()
extern void GeneralSequencer_U3CStartU3Eb__4_0_mCC4F15FF69FEEA0BFB73E8E2542C8910D2FAA220 (void);
// 0x0000004A System.Void GeneralSimpleUI::Start()
extern void GeneralSimpleUI_Start_mEEF7BDC999733A706FE7C03F54C8B4B83243C221 (void);
// 0x0000004B System.Void GeneralSimpleUI::.ctor()
extern void GeneralSimpleUI__ctor_m9A7A8639A7F2EE5FEAF39E2B6232FA533F58E1CC (void);
// 0x0000004C System.Void GeneralSimpleUI::<Start>b__1_0(UnityEngine.Vector2)
extern void GeneralSimpleUI_U3CStartU3Eb__1_0_mD28AC7A7223ECEB398951A2AAA561A4B9DC6FB66 (void);
// 0x0000004D System.Void GeneralSimpleUI::<Start>b__1_2(UnityEngine.Vector3)
extern void GeneralSimpleUI_U3CStartU3Eb__1_2_mA798C3D21C7EDE1BF5F133F6946478FDCE1B5CA8 (void);
// 0x0000004E System.Void GeneralSimpleUI::<Start>b__1_3(UnityEngine.Color)
extern void GeneralSimpleUI_U3CStartU3Eb__1_3_m759D56CF0CDCA8E27EDEB8BF968FD00AD9932824 (void);
// 0x0000004F System.Void GeneralUISpace::Start()
extern void GeneralUISpace_Start_mA7F9726366918F04A038C11E04A5230F0C4AA41D (void);
// 0x00000050 System.Void GeneralUISpace::.ctor()
extern void GeneralUISpace__ctor_mD9DD0C8F2377084F9DC2EA074C24E93DF1A3518B (void);
// 0x00000051 System.Void LogoCinematic::Awake()
extern void LogoCinematic_Awake_m72CA9A35497E16D50121250F9BCF15F3BDF65138 (void);
// 0x00000052 System.Void LogoCinematic::Start()
extern void LogoCinematic_Start_m5DB42CBB808A9C41D32B440B65FEE9F35E1FF86A (void);
// 0x00000053 System.Void LogoCinematic::playBoom()
extern void LogoCinematic_playBoom_m1359C13981E4254A26839968D26F0F96E6033E4E (void);
// 0x00000054 System.Void LogoCinematic::.ctor()
extern void LogoCinematic__ctor_m87A3D83E1047FD2818C5319C7462F2475468743E (void);
// 0x00000055 System.Void PathBezier2d::Start()
extern void PathBezier2d_Start_m6101865E419D574B55D90AFC87D59C61E2FE6A5A (void);
// 0x00000056 System.Void PathBezier2d::OnDrawGizmos()
extern void PathBezier2d_OnDrawGizmos_m5AF15DD02CC758B94C6988CA1084C7D5C03FC721 (void);
// 0x00000057 System.Void PathBezier2d::.ctor()
extern void PathBezier2d__ctor_m57F2349CACC84E24CFC8D242C81E3DE948CC357E (void);
// 0x00000058 System.Void ExampleSpline::Start()
extern void ExampleSpline_Start_mAD9D6848447333E450D9991F68BDD989B6AE6BFC (void);
// 0x00000059 System.Void ExampleSpline::Update()
extern void ExampleSpline_Update_m0C06D2C4C359A2AA1E1EEE9C4A7CE76B10A774F7 (void);
// 0x0000005A System.Void ExampleSpline::OnDrawGizmos()
extern void ExampleSpline_OnDrawGizmos_m1EC31ED5019C6CAAF409DE171F1D46631C4FEB4B (void);
// 0x0000005B System.Void ExampleSpline::.ctor()
extern void ExampleSpline__ctor_mA777C76A3F034C55804819D09FAD803C22B6945A (void);
// 0x0000005C System.Void PathSpline2d::Start()
extern void PathSpline2d_Start_m322D034A9E1AE896E48FD9C3A83D81E298B101F2 (void);
// 0x0000005D System.Void PathSpline2d::OnDrawGizmos()
extern void PathSpline2d_OnDrawGizmos_m45503986AD7ABD0B14F69C276092F5E99F3508E0 (void);
// 0x0000005E System.Void PathSpline2d::.ctor()
extern void PathSpline2d__ctor_m0C4B7813D0D578B5CAAAF5D2963ADF0E699583B8 (void);
// 0x0000005F System.Void PathSplineEndless::Start()
extern void PathSplineEndless_Start_m0D63B5D9F0A5A8953829DBFF1AB80AD1699E5858 (void);
// 0x00000060 System.Void PathSplineEndless::Update()
extern void PathSplineEndless_Update_m2168C1E36EB175C86685B1D6438244B9905B0D55 (void);
// 0x00000061 UnityEngine.GameObject PathSplineEndless::objectQueue(UnityEngine.GameObject[],System.Int32&)
extern void PathSplineEndless_objectQueue_mDF89CDC0D51CA6B9A84F700BFD011EA5D53D0B3C (void);
// 0x00000062 System.Void PathSplineEndless::addRandomTrackPoint()
extern void PathSplineEndless_addRandomTrackPoint_m2A6DF9D7DC969FC9E3E269C8F6815BDB1622552E (void);
// 0x00000063 System.Void PathSplineEndless::refreshSpline()
extern void PathSplineEndless_refreshSpline_mE7C260A36EBDE07ED250D752CC2AE13F9640BF84 (void);
// 0x00000064 System.Void PathSplineEndless::playSwish()
extern void PathSplineEndless_playSwish_m8DF90B3209DEC3201AD432B013B8CE3D665E70F3 (void);
// 0x00000065 System.Void PathSplineEndless::.ctor()
extern void PathSplineEndless__ctor_m936283AAD1B2ACB8A975A6C5F44E5C49F235D889 (void);
// 0x00000066 System.Void PathSplineEndless::<Start>b__17_0(System.Single)
extern void PathSplineEndless_U3CStartU3Eb__17_0_m6457B9A5A1966F3A5EB50724D4F867D6AB05BDD5 (void);
// 0x00000067 System.Void PathSplinePerformance::Start()
extern void PathSplinePerformance_Start_m1DA9D6816079EA25341277C76A317D94B4168EF3 (void);
// 0x00000068 System.Void PathSplinePerformance::Update()
extern void PathSplinePerformance_Update_m440225D51242DD97344316E466389BB633099B3F (void);
// 0x00000069 System.Void PathSplinePerformance::OnDrawGizmos()
extern void PathSplinePerformance_OnDrawGizmos_m963F417D81A92D1D1260EB172050FEB2C95B9E06 (void);
// 0x0000006A System.Void PathSplinePerformance::playSwish()
extern void PathSplinePerformance_playSwish_mCDD5986D88F473C4C789EBB08013D5C4371B8934 (void);
// 0x0000006B System.Void PathSplinePerformance::.ctor()
extern void PathSplinePerformance__ctor_mF91B2042C6950B108ACFC3CDB4FA0F2F6545377E (void);
// 0x0000006C System.Void PathSplineTrack::Start()
extern void PathSplineTrack_Start_m281BC1BE6C44BADFA80DA3D5C0679D34FEBADE1B (void);
// 0x0000006D System.Void PathSplineTrack::Update()
extern void PathSplineTrack_Update_m789CE9A3C91D635B04AE45C77ADC552CEEB0BE3A (void);
// 0x0000006E System.Void PathSplineTrack::OnDrawGizmos()
extern void PathSplineTrack_OnDrawGizmos_mF8A6C592C8E177043453FAFA09F3917D133E82FC (void);
// 0x0000006F System.Void PathSplineTrack::playSwish()
extern void PathSplineTrack_playSwish_m166E8F61FFC8D4DE30A55839EE95413669489D99 (void);
// 0x00000070 System.Void PathSplineTrack::.ctor()
extern void PathSplineTrack__ctor_m3D4ECADCEDAC59010FC4B6576F49BCC3083E8DBA (void);
// 0x00000071 System.Void PathSplines::OnEnable()
extern void PathSplines_OnEnable_m9A31A2680F7A3A5A786B007758F6B68DEFD47CBF (void);
// 0x00000072 System.Void PathSplines::Start()
extern void PathSplines_Start_mBB7ABDB218B114C164C2E61B9BD469CB8D5E76D6 (void);
// 0x00000073 System.Void PathSplines::Update()
extern void PathSplines_Update_mFAFAB73B81EA5981B4EEC5C507B72A52D49E593C (void);
// 0x00000074 System.Void PathSplines::OnDrawGizmos()
extern void PathSplines_OnDrawGizmos_mD3B3EE01513F4BBF82A00F87DBA40C283384C5EB (void);
// 0x00000075 System.Void PathSplines::.ctor()
extern void PathSplines__ctor_mFFF59F5DECB94D08ECA1765F4AF70F15F1D0F807 (void);
// 0x00000076 System.Void PathSplines::<Start>b__4_0()
extern void PathSplines_U3CStartU3Eb__4_0_mCDFF93FF0B96E66451EDB0D037E46B7F6A0B0D04 (void);
// 0x00000077 System.Void TestingZLegacy::Awake()
extern void TestingZLegacy_Awake_mBCE69D28EF4B5C8DD9623C93150A62690FF6F560 (void);
// 0x00000078 System.Void TestingZLegacy::Start()
extern void TestingZLegacy_Start_mF7B3C1D0359654FACF1A636ED276DA2D7AC2F238 (void);
// 0x00000079 System.Void TestingZLegacy::pauseNow()
extern void TestingZLegacy_pauseNow_m655EADF071BB3053B444C84C0BE35675AD971405 (void);
// 0x0000007A System.Void TestingZLegacy::OnGUI()
extern void TestingZLegacy_OnGUI_m8736E68E13F34D38E820CBDF166DB67989878F22 (void);
// 0x0000007B System.Void TestingZLegacy::endlessCallback()
extern void TestingZLegacy_endlessCallback_m9AEBB3556FEF851B1F6DB08DB8050E8D89D45D17 (void);
// 0x0000007C System.Void TestingZLegacy::cycleThroughExamples()
extern void TestingZLegacy_cycleThroughExamples_mC9AF8FB1FED800DF92B00E6C482380B1ED2EF4A3 (void);
// 0x0000007D System.Void TestingZLegacy::updateValue3Example()
extern void TestingZLegacy_updateValue3Example_mCA94016385AF00299FD43C7778501DB3F51E8786 (void);
// 0x0000007E System.Void TestingZLegacy::updateValue3ExampleUpdate(UnityEngine.Vector3)
extern void TestingZLegacy_updateValue3ExampleUpdate_mC0566B31227122FC6140F7704C48065E4AC046C3 (void);
// 0x0000007F System.Void TestingZLegacy::updateValue3ExampleCallback(UnityEngine.Vector3)
extern void TestingZLegacy_updateValue3ExampleCallback_mEA31AFEE5C74647804BDE02889A38307D8096B4B (void);
// 0x00000080 System.Void TestingZLegacy::loopTestClamp()
extern void TestingZLegacy_loopTestClamp_mEC3D64101ACA122E2257ED726DA36D5E7E2EE6DD (void);
// 0x00000081 System.Void TestingZLegacy::loopTestPingPong()
extern void TestingZLegacy_loopTestPingPong_mC9FC3BFBB6EB66050EB24EA3623777201C131F91 (void);
// 0x00000082 System.Void TestingZLegacy::colorExample()
extern void TestingZLegacy_colorExample_mDCABB46692619BF22DD41425D03EA835F6024AC2 (void);
// 0x00000083 System.Void TestingZLegacy::moveOnACurveExample()
extern void TestingZLegacy_moveOnACurveExample_m643B475C72457BFC0580809D25206A474786A1AA (void);
// 0x00000084 System.Void TestingZLegacy::customTweenExample()
extern void TestingZLegacy_customTweenExample_mE863F3298241D782EBCB333A93B882C572C8F9E6 (void);
// 0x00000085 System.Void TestingZLegacy::moveExample()
extern void TestingZLegacy_moveExample_m8CB97B6FAD6C66EC740EB31D539F78BC5930F02A (void);
// 0x00000086 System.Void TestingZLegacy::rotateExample()
extern void TestingZLegacy_rotateExample_m09C24A6D46978C652A6D8F321F9A071247462DD3 (void);
// 0x00000087 System.Void TestingZLegacy::rotateOnUpdate(System.Single)
extern void TestingZLegacy_rotateOnUpdate_mDB551FEC7A3BC50E5BFCB3A4D86EDBD71343FE97 (void);
// 0x00000088 System.Void TestingZLegacy::rotateFinished(System.Object)
extern void TestingZLegacy_rotateFinished_mEE1B541D31B1E9388A575AEF8FECC8D8FDD3605F (void);
// 0x00000089 System.Void TestingZLegacy::scaleExample()
extern void TestingZLegacy_scaleExample_m8683640627D1DF303D1A55A59C476198A6D5D7D8 (void);
// 0x0000008A System.Void TestingZLegacy::updateValueExample()
extern void TestingZLegacy_updateValueExample_mE0A9F76605CEA59FA59357CE4C5313A32801AAF4 (void);
// 0x0000008B System.Void TestingZLegacy::updateValueExampleCallback(System.Single,System.Object)
extern void TestingZLegacy_updateValueExampleCallback_m4CB6A988D0383234B578C9465B40085A962D92EC (void);
// 0x0000008C System.Void TestingZLegacy::delayedCallExample()
extern void TestingZLegacy_delayedCallExample_mB11B32B256519D1ECFBB54216CEB46E7719BE5E9 (void);
// 0x0000008D System.Void TestingZLegacy::delayedCallExampleCallback()
extern void TestingZLegacy_delayedCallExampleCallback_mE8E45FAD4B3C8A46BA856C0250AF6A4F6606F20C (void);
// 0x0000008E System.Void TestingZLegacy::alphaExample()
extern void TestingZLegacy_alphaExample_m7DEE863B73235D62D7FACC183D9B69A9C22C68F8 (void);
// 0x0000008F System.Void TestingZLegacy::moveLocalExample()
extern void TestingZLegacy_moveLocalExample_mD7173FDBB36229E28B108F6AD5195F253326F3B2 (void);
// 0x00000090 System.Void TestingZLegacy::rotateAroundExample()
extern void TestingZLegacy_rotateAroundExample_m2D08EC29B26084F4B7D22AAD9D8A4453FFB9659B (void);
// 0x00000091 System.Void TestingZLegacy::loopPause()
extern void TestingZLegacy_loopPause_m55A880DF0CB7F8737EAB6CB0202121B258759EE7 (void);
// 0x00000092 System.Void TestingZLegacy::loopResume()
extern void TestingZLegacy_loopResume_mE7636F1996095F5B17B155C5BA0350DDEF222F8D (void);
// 0x00000093 System.Void TestingZLegacy::punchTest()
extern void TestingZLegacy_punchTest_mE2E20734BDEE2A30E5F643FDF84FE56BD99ACB49 (void);
// 0x00000094 System.Void TestingZLegacy::.ctor()
extern void TestingZLegacy__ctor_m54DC8DE5EF69E156FDDF1C34916397E68B0B9F2E (void);
// 0x00000095 System.Void TestingZLegacyExt::Awake()
extern void TestingZLegacyExt_Awake_m97F85D3DD812AE6BFA4E59E5BDB074A11DEFB74D (void);
// 0x00000096 System.Void TestingZLegacyExt::Start()
extern void TestingZLegacyExt_Start_m3FC22E42B4D1C512B8F76260CF90F7584CDE78C9 (void);
// 0x00000097 System.Void TestingZLegacyExt::pauseNow()
extern void TestingZLegacyExt_pauseNow_m6FA2ACC29A279AFFEAC9C1117C615E692109E47E (void);
// 0x00000098 System.Void TestingZLegacyExt::OnGUI()
extern void TestingZLegacyExt_OnGUI_mE335B55E2688BCF03B84575D64CE6905E9EB390C (void);
// 0x00000099 System.Void TestingZLegacyExt::endlessCallback()
extern void TestingZLegacyExt_endlessCallback_mC91F80F402003198552BB5A47773AE91C1CDF3E4 (void);
// 0x0000009A System.Void TestingZLegacyExt::cycleThroughExamples()
extern void TestingZLegacyExt_cycleThroughExamples_m7ADA1FA87F2373C6CA2F710EE09E07C6E6B48DA7 (void);
// 0x0000009B System.Void TestingZLegacyExt::updateValue3Example()
extern void TestingZLegacyExt_updateValue3Example_m56A718E3C7178DB0C59E3E030EF2007D60168950 (void);
// 0x0000009C System.Void TestingZLegacyExt::updateValue3ExampleUpdate(UnityEngine.Vector3)
extern void TestingZLegacyExt_updateValue3ExampleUpdate_mBA7E2C03C01B5CDA6C4DF5EE4E5521FF72644235 (void);
// 0x0000009D System.Void TestingZLegacyExt::updateValue3ExampleCallback(UnityEngine.Vector3)
extern void TestingZLegacyExt_updateValue3ExampleCallback_m83AD25E3BEB33C7F57C68FCD9C1EA9E27F762651 (void);
// 0x0000009E System.Void TestingZLegacyExt::loopTestClamp()
extern void TestingZLegacyExt_loopTestClamp_m402BD2B45CDF06FA5F47E43EE139E3D593D3837E (void);
// 0x0000009F System.Void TestingZLegacyExt::loopTestPingPong()
extern void TestingZLegacyExt_loopTestPingPong_m88A111C6F9F4C97A89F6E141862BD22F1723F1A8 (void);
// 0x000000A0 System.Void TestingZLegacyExt::colorExample()
extern void TestingZLegacyExt_colorExample_mD487DAAD856F9B4E044869A56A9E83F0E5CE686D (void);
// 0x000000A1 System.Void TestingZLegacyExt::moveOnACurveExample()
extern void TestingZLegacyExt_moveOnACurveExample_m76FBF8744FDD1EDFA144DF0B05F91F341DDE7C86 (void);
// 0x000000A2 System.Void TestingZLegacyExt::customTweenExample()
extern void TestingZLegacyExt_customTweenExample_m7FD53EA42C543F5CDE51BE17B7B1714B078A3671 (void);
// 0x000000A3 System.Void TestingZLegacyExt::moveExample()
extern void TestingZLegacyExt_moveExample_m877A1E1840D36A34C7655F995471BA952B64FC2D (void);
// 0x000000A4 System.Void TestingZLegacyExt::rotateExample()
extern void TestingZLegacyExt_rotateExample_mB882491A5D47DE36B743560863B366C88CF69A2D (void);
// 0x000000A5 System.Void TestingZLegacyExt::rotateOnUpdate(System.Single)
extern void TestingZLegacyExt_rotateOnUpdate_m60EEE824C1EBA3FA12C60B3B23B6144739697A25 (void);
// 0x000000A6 System.Void TestingZLegacyExt::rotateFinished(System.Object)
extern void TestingZLegacyExt_rotateFinished_m6885AB18A26592B3844455D756D70BDC20F257E8 (void);
// 0x000000A7 System.Void TestingZLegacyExt::scaleExample()
extern void TestingZLegacyExt_scaleExample_m3D54AA1EEFBA37C30889E34AFBB350316CEAB41B (void);
// 0x000000A8 System.Void TestingZLegacyExt::updateValueExample()
extern void TestingZLegacyExt_updateValueExample_m0A08F0B235AD41CC0C4153B431862904C81799C6 (void);
// 0x000000A9 System.Void TestingZLegacyExt::updateValueExampleCallback(System.Single,System.Object)
extern void TestingZLegacyExt_updateValueExampleCallback_mD07695C332FF263FCFF89F4B305C96A02A786729 (void);
// 0x000000AA System.Void TestingZLegacyExt::delayedCallExample()
extern void TestingZLegacyExt_delayedCallExample_m530FFA75198C880599CE65E105D061B133E172F9 (void);
// 0x000000AB System.Void TestingZLegacyExt::delayedCallExampleCallback()
extern void TestingZLegacyExt_delayedCallExampleCallback_m0F8E4A9EC78F99065D1B519DA641FEB9B4A686A5 (void);
// 0x000000AC System.Void TestingZLegacyExt::alphaExample()
extern void TestingZLegacyExt_alphaExample_m6FEFC52F975614F51524FB7B91547C791FC1A982 (void);
// 0x000000AD System.Void TestingZLegacyExt::moveLocalExample()
extern void TestingZLegacyExt_moveLocalExample_mD509C90ADEA3546144652A770965A5C98F88F3C2 (void);
// 0x000000AE System.Void TestingZLegacyExt::rotateAroundExample()
extern void TestingZLegacyExt_rotateAroundExample_m4507C9CD41E678846AE918C6D796A4BFC43EF056 (void);
// 0x000000AF System.Void TestingZLegacyExt::loopPause()
extern void TestingZLegacyExt_loopPause_mC23D0D95589F74EA510A9E79D35F28C7418F514F (void);
// 0x000000B0 System.Void TestingZLegacyExt::loopResume()
extern void TestingZLegacyExt_loopResume_mA3FBE26AE1243FE2178C55BEDB45AC6763C42FF9 (void);
// 0x000000B1 System.Void TestingZLegacyExt::punchTest()
extern void TestingZLegacyExt_punchTest_mBC5D8A5FFF359CFAFF16BFD89387B56B571DA001 (void);
// 0x000000B2 System.Void TestingZLegacyExt::.ctor()
extern void TestingZLegacyExt__ctor_m918A02678AF6D9E398E17E98FC792A73E8205542 (void);
// 0x000000B3 UnityEngine.Vector3 LTDescr::get_from()
extern void LTDescr_get_from_m8E7FF7FB5FE65C313FE2CD26C8B1BBAC00BFBDC2 (void);
// 0x000000B4 System.Void LTDescr::set_from(UnityEngine.Vector3)
extern void LTDescr_set_from_m0C7AA03B3869C1A3B6EEB8ECD16FF0311A19B5FD (void);
// 0x000000B5 UnityEngine.Vector3 LTDescr::get_to()
extern void LTDescr_get_to_m842DE271937A3E21367D57D4C1A963E98E0AF5E9 (void);
// 0x000000B6 System.Void LTDescr::set_to(UnityEngine.Vector3)
extern void LTDescr_set_to_m720DB590EE8641E7091375EE626ED38CCA36C63D (void);
// 0x000000B7 LTDescr/ActionMethodDelegate LTDescr::get_easeInternal()
extern void LTDescr_get_easeInternal_mEDF89A6F240F4770536F1FBC0FBB79A6A55C7BBD (void);
// 0x000000B8 System.Void LTDescr::set_easeInternal(LTDescr/ActionMethodDelegate)
extern void LTDescr_set_easeInternal_m16C7B8635E5D2FB3E31EBE721E954967EFBF30B7 (void);
// 0x000000B9 LTDescr/ActionMethodDelegate LTDescr::get_initInternal()
extern void LTDescr_get_initInternal_mE336562DA52E756130ADBB07B3CEF2D7E7EB01D4 (void);
// 0x000000BA System.Void LTDescr::set_initInternal(LTDescr/ActionMethodDelegate)
extern void LTDescr_set_initInternal_m0753D24B8455857620589DBBE1EF095A2AFE489B (void);
// 0x000000BB UnityEngine.Transform LTDescr::get_toTrans()
extern void LTDescr_get_toTrans_m2B06D495FA4BC4F44D1EAD32F163D62BCC457B5F (void);
// 0x000000BC System.String LTDescr::ToString()
extern void LTDescr_ToString_mA312106BDDBB7E356B88C05BF8C767C63964CCC6 (void);
// 0x000000BD System.Void LTDescr::.ctor()
extern void LTDescr__ctor_m975E0C19B34E8078C70AFAE95A581777B8D6F4D8 (void);
// 0x000000BE LTDescr LTDescr::cancel(UnityEngine.GameObject)
extern void LTDescr_cancel_m944E66783EA1A1BD7781AFE7C7EF7A6117B56930 (void);
// 0x000000BF System.Int32 LTDescr::get_uniqueId()
extern void LTDescr_get_uniqueId_m11CA85564A0B0C9BE7FC551ECA3A05111888C7B2 (void);
// 0x000000C0 System.Int32 LTDescr::get_id()
extern void LTDescr_get_id_m76232B8A21BCE44149F9014942BD1C63F5C6B4E7 (void);
// 0x000000C1 LTDescrOptional LTDescr::get_optional()
extern void LTDescr_get_optional_mEA6CD7BE31135EB1F2310490F9FAC346767B3020 (void);
// 0x000000C2 System.Void LTDescr::set_optional(LTDescrOptional)
extern void LTDescr_set_optional_mF78587FE46E7E26D9742F3F156DC92DA51AEA2EE (void);
// 0x000000C3 System.Void LTDescr::reset()
extern void LTDescr_reset_m16BED571328E24D5385BDA9E4E478508EEB74664 (void);
// 0x000000C4 LTDescr LTDescr::setFollow()
extern void LTDescr_setFollow_mD35310187FD79F81851CD3635C0376BAAC9EF9FB (void);
// 0x000000C5 LTDescr LTDescr::setMoveX()
extern void LTDescr_setMoveX_m3B1BB06BE81F8DF47CE145148ADE5D2E63B7D630 (void);
// 0x000000C6 LTDescr LTDescr::setMoveY()
extern void LTDescr_setMoveY_m62E79AA722E091918379FDA5255DB3F517D447F3 (void);
// 0x000000C7 LTDescr LTDescr::setMoveZ()
extern void LTDescr_setMoveZ_m5846FCA035E0BC391028178FEAEF74B0C31C998B (void);
// 0x000000C8 LTDescr LTDescr::setMoveLocalX()
extern void LTDescr_setMoveLocalX_m1DAB1F3CFCF599184B907273B7EEDB58C319177D (void);
// 0x000000C9 LTDescr LTDescr::setMoveLocalY()
extern void LTDescr_setMoveLocalY_m3EFB302B1C5A842539CCC7D972836D4AA5BEF9C1 (void);
// 0x000000CA LTDescr LTDescr::setMoveLocalZ()
extern void LTDescr_setMoveLocalZ_mBCD523EEE6FDE11FBDF9B291FA827EFCAADC055F (void);
// 0x000000CB System.Void LTDescr::initFromInternal()
extern void LTDescr_initFromInternal_mA4604276862B5C16B4D0A1F323C5031D37CBE125 (void);
// 0x000000CC LTDescr LTDescr::setOffset(UnityEngine.Vector3)
extern void LTDescr_setOffset_m29ED0A18B591990BBCC94E36A22426FEFF0B6F5F (void);
// 0x000000CD LTDescr LTDescr::setMoveCurved()
extern void LTDescr_setMoveCurved_mD2FC79FD2FBC09320F29D253187B0FD5E1E3F242 (void);
// 0x000000CE LTDescr LTDescr::setMoveCurvedLocal()
extern void LTDescr_setMoveCurvedLocal_m5B19FE68C93FBE105E3D02DA4AABB28183218DA4 (void);
// 0x000000CF LTDescr LTDescr::setMoveSpline()
extern void LTDescr_setMoveSpline_m483C9060295F816DDB9F363B2B7D3C7EB2BC709A (void);
// 0x000000D0 LTDescr LTDescr::setMoveSplineLocal()
extern void LTDescr_setMoveSplineLocal_mE23B6AEEBD002D6D811758CB415DBB8559505DB2 (void);
// 0x000000D1 LTDescr LTDescr::setScaleX()
extern void LTDescr_setScaleX_mC50D44B615017A9DEC47B21746DA49E6907C2106 (void);
// 0x000000D2 LTDescr LTDescr::setScaleY()
extern void LTDescr_setScaleY_m3980093C790F98A85A697A6CB76A08CE258A1831 (void);
// 0x000000D3 LTDescr LTDescr::setScaleZ()
extern void LTDescr_setScaleZ_m1E3D4F892EFB3F884EB006D6314CEAAE2E86B788 (void);
// 0x000000D4 LTDescr LTDescr::setRotateX()
extern void LTDescr_setRotateX_m9710079249D050C4709F38FF212A8C5A064141A6 (void);
// 0x000000D5 LTDescr LTDescr::setRotateY()
extern void LTDescr_setRotateY_m0A6206F1BAC1EB1E74373C866BB993A17AC474C7 (void);
// 0x000000D6 LTDescr LTDescr::setRotateZ()
extern void LTDescr_setRotateZ_m88EB19296F3A5A255B0DDAD63F5556854A3AA6D5 (void);
// 0x000000D7 LTDescr LTDescr::setRotateAround()
extern void LTDescr_setRotateAround_m5C916E21674683F454CF66A50F1B52688EC5A72F (void);
// 0x000000D8 LTDescr LTDescr::setRotateAroundLocal()
extern void LTDescr_setRotateAroundLocal_m26621B8F8199CF50CFD97CF32E6DF271543CE142 (void);
// 0x000000D9 LTDescr LTDescr::setAlpha()
extern void LTDescr_setAlpha_mA82AF444875CEBDC11AF76A6DA33F8832070A7C9 (void);
// 0x000000DA LTDescr LTDescr::setTextAlpha()
extern void LTDescr_setTextAlpha_m140059336BABDBE97F7B9EB26E7B07A6A2FECCCF (void);
// 0x000000DB LTDescr LTDescr::setAlphaVertex()
extern void LTDescr_setAlphaVertex_m4BE3949864A804651A8E884317090AC0BE9BA543 (void);
// 0x000000DC LTDescr LTDescr::setColor()
extern void LTDescr_setColor_m677D553334BCD65A976646538D2E3FB901FEB2F8 (void);
// 0x000000DD LTDescr LTDescr::setCallbackColor()
extern void LTDescr_setCallbackColor_m8ED344DF8DA4A35A9323090A9705F338CCEFA6BF (void);
// 0x000000DE LTDescr LTDescr::setTextColor()
extern void LTDescr_setTextColor_mB0E1342E1C5604929D621039E74EB5C968E63CA5 (void);
// 0x000000DF LTDescr LTDescr::setCanvasAlpha()
extern void LTDescr_setCanvasAlpha_m883942EF674A4FCF4C0621D8BB36F1EB78D9465E (void);
// 0x000000E0 LTDescr LTDescr::setCanvasGroupAlpha()
extern void LTDescr_setCanvasGroupAlpha_mA08B850710279DF7259A9EA457614BABD29EF28C (void);
// 0x000000E1 LTDescr LTDescr::setCanvasColor()
extern void LTDescr_setCanvasColor_m8D0A659A765604171C1ECE12ACB617319E179015 (void);
// 0x000000E2 LTDescr LTDescr::setCanvasMoveX()
extern void LTDescr_setCanvasMoveX_m012C7901BCEC98126783ACDB01B088297D30EB69 (void);
// 0x000000E3 LTDescr LTDescr::setCanvasMoveY()
extern void LTDescr_setCanvasMoveY_m8124C7761145134D07245BB3031857695501B9EB (void);
// 0x000000E4 LTDescr LTDescr::setCanvasMoveZ()
extern void LTDescr_setCanvasMoveZ_mFDFC0D71E22EE1097FDA8A2A6F0A9990C5403D6C (void);
// 0x000000E5 System.Void LTDescr::initCanvasRotateAround()
extern void LTDescr_initCanvasRotateAround_m1205028161B594D223E69013F8C92B766CE75959 (void);
// 0x000000E6 LTDescr LTDescr::setCanvasRotateAround()
extern void LTDescr_setCanvasRotateAround_m7E04711CE56F7BB9D43E27A0102E0C3C7FDCDDF9 (void);
// 0x000000E7 LTDescr LTDescr::setCanvasRotateAroundLocal()
extern void LTDescr_setCanvasRotateAroundLocal_m7BA851F76908BF978A682D4E49CD2DB770F27F55 (void);
// 0x000000E8 LTDescr LTDescr::setCanvasPlaySprite()
extern void LTDescr_setCanvasPlaySprite_m3CDD391E0DC1AB5B60FC4A01F1527184062E0202 (void);
// 0x000000E9 LTDescr LTDescr::setCanvasMove()
extern void LTDescr_setCanvasMove_m84559A096BC7A140C8CA25DE9DB766D537C9A148 (void);
// 0x000000EA LTDescr LTDescr::setCanvasScale()
extern void LTDescr_setCanvasScale_mF06739A6DC21077E3EB7704B3997CB495302F134 (void);
// 0x000000EB LTDescr LTDescr::setCanvasSizeDelta()
extern void LTDescr_setCanvasSizeDelta_m6C41930BF6F585DCDDF31EB4B540F3C3163DF0E3 (void);
// 0x000000EC System.Void LTDescr::callback()
extern void LTDescr_callback_m9B45B1DCD83B9880A61C16C6C21F0D05C7C6AFA7 (void);
// 0x000000ED LTDescr LTDescr::setCallback()
extern void LTDescr_setCallback_m7BD2A7CFAC15C8D9D992E0521B819B41FD9A6CB3 (void);
// 0x000000EE LTDescr LTDescr::setValue3()
extern void LTDescr_setValue3_m553FBFB157A9765053F9FB7CF08B59A1E3D976D7 (void);
// 0x000000EF LTDescr LTDescr::setMove()
extern void LTDescr_setMove_mDEC044497DD6398395A17A83EDC32ABEB3F8DA88 (void);
// 0x000000F0 LTDescr LTDescr::setMoveLocal()
extern void LTDescr_setMoveLocal_m4B75F54B72038AF2971A73A8A6E905DE94AA6346 (void);
// 0x000000F1 LTDescr LTDescr::setMoveToTransform()
extern void LTDescr_setMoveToTransform_m9C07DB063E64D2F9A91054DFA08B1C2594D22F88 (void);
// 0x000000F2 LTDescr LTDescr::setRotate()
extern void LTDescr_setRotate_m37FB3B6088196BB2A0DD48105125989468400A5B (void);
// 0x000000F3 LTDescr LTDescr::setRotateLocal()
extern void LTDescr_setRotateLocal_m94A88F95DB9D3BD078305AB3A25039966CF4147C (void);
// 0x000000F4 LTDescr LTDescr::setScale()
extern void LTDescr_setScale_m003B5242EE6667C4F5AC57C5C06A9A32007F0FBD (void);
// 0x000000F5 LTDescr LTDescr::setGUIMove()
extern void LTDescr_setGUIMove_mC25E34C2B27930D956DB92F12FF79F25F7D78E9E (void);
// 0x000000F6 LTDescr LTDescr::setGUIMoveMargin()
extern void LTDescr_setGUIMoveMargin_m96C670A2423CC1E4A2D02021CF0F5CFA51A8E14B (void);
// 0x000000F7 LTDescr LTDescr::setGUIScale()
extern void LTDescr_setGUIScale_mE8BA06B69FFA9FF14F0C7CD3FB76AE2F12283B0D (void);
// 0x000000F8 LTDescr LTDescr::setGUIAlpha()
extern void LTDescr_setGUIAlpha_m964F89CFFC7D20A97EE78E6340C9F8236AC2DE6A (void);
// 0x000000F9 LTDescr LTDescr::setGUIRotate()
extern void LTDescr_setGUIRotate_m9B46A83C43BAFD1DB0C5629D850F1D3424B3AA7B (void);
// 0x000000FA LTDescr LTDescr::setDelayedSound()
extern void LTDescr_setDelayedSound_m6223261128689E6768A694E089DE42B67A2BC026 (void);
// 0x000000FB LTDescr LTDescr::setTarget(UnityEngine.Transform)
extern void LTDescr_setTarget_m96F2398CBF82B65C2BD373807C75C0E04A39DD29 (void);
// 0x000000FC System.Void LTDescr::init()
extern void LTDescr_init_mB964AC9BD3406D19362D8675474F4DC99B33347B (void);
// 0x000000FD System.Void LTDescr::initSpeed()
extern void LTDescr_initSpeed_m97D3A943AA4B2F25ABC91FC5C5C055611799AE16 (void);
// 0x000000FE LTDescr LTDescr::updateNow()
extern void LTDescr_updateNow_mB7468B5F37D19C21184A2A885C1DF0A55E1F1109 (void);
// 0x000000FF System.Boolean LTDescr::updateInternal()
extern void LTDescr_updateInternal_m2295EAEC09FDEF9AD71EB618734DE0DF5570D2A5 (void);
// 0x00000100 System.Void LTDescr::callOnCompletes()
extern void LTDescr_callOnCompletes_mE047FE764250E4071E38197BC1B29221B87B8B4B (void);
// 0x00000101 LTDescr LTDescr::setFromColor(UnityEngine.Color)
extern void LTDescr_setFromColor_m84DCC218D18266CA0D5167AE546CF54EBF2D38B0 (void);
// 0x00000102 System.Void LTDescr::alphaRecursive(UnityEngine.Transform,System.Single,System.Boolean)
extern void LTDescr_alphaRecursive_mA15152C74BCA8D8CC498BF7A84A0481A35AA29EC (void);
// 0x00000103 System.Void LTDescr::colorRecursive(UnityEngine.Transform,UnityEngine.Color,System.Boolean)
extern void LTDescr_colorRecursive_mA38B1CB2A51FECCCF49F1D1F50D98312A2296E0E (void);
// 0x00000104 System.Void LTDescr::alphaRecursive(UnityEngine.RectTransform,System.Single,System.Int32)
extern void LTDescr_alphaRecursive_m6BAD882CBD0900B2ED3ACD96B49E1AE50DEA791D (void);
// 0x00000105 System.Void LTDescr::alphaRecursiveSprite(UnityEngine.Transform,System.Single)
extern void LTDescr_alphaRecursiveSprite_m64AA415027FB1434EFB1E006721B206F47CB1ECA (void);
// 0x00000106 System.Void LTDescr::colorRecursiveSprite(UnityEngine.Transform,UnityEngine.Color)
extern void LTDescr_colorRecursiveSprite_mFE805A4F826EC3CFF84CB6E86E43E857B934897D (void);
// 0x00000107 System.Void LTDescr::colorRecursive(UnityEngine.RectTransform,UnityEngine.Color)
extern void LTDescr_colorRecursive_mE714E3CB2CB79B4F6C40A0C0A5CF648A5C1E3F94 (void);
// 0x00000108 System.Void LTDescr::textAlphaChildrenRecursive(UnityEngine.Transform,System.Single,System.Boolean)
extern void LTDescr_textAlphaChildrenRecursive_m99FA1EC22F4CF7A7743038CF8369298B1F3ADAF2 (void);
// 0x00000109 System.Void LTDescr::textAlphaRecursive(UnityEngine.Transform,System.Single,System.Boolean)
extern void LTDescr_textAlphaRecursive_m8EC23765F7904CBA0C35734A7D63F58C879C577C (void);
// 0x0000010A System.Void LTDescr::textColorRecursive(UnityEngine.Transform,UnityEngine.Color)
extern void LTDescr_textColorRecursive_mE39D07578A5FCAEE8336AE9D1DE9E841B98FD90C (void);
// 0x0000010B UnityEngine.Color LTDescr::tweenColor(LTDescr,System.Single)
extern void LTDescr_tweenColor_mD7F8129343D2677EC17D5740E160A88540685140 (void);
// 0x0000010C LTDescr LTDescr::pause()
extern void LTDescr_pause_m516CADEFB418399C0A796883DB35D183F8FBD9D3 (void);
// 0x0000010D LTDescr LTDescr::resume()
extern void LTDescr_resume_m1D572322CCB20396E715EED46A1692A6D4F81E80 (void);
// 0x0000010E LTDescr LTDescr::setAxis(UnityEngine.Vector3)
extern void LTDescr_setAxis_m6785FFA919508809C7A0A5FB7A998E46E1CB22D1 (void);
// 0x0000010F LTDescr LTDescr::setDelay(System.Single)
extern void LTDescr_setDelay_m78E34545BB3EEA43ABF9CD5D29C8FB1CC3D4F464 (void);
// 0x00000110 LTDescr LTDescr::setEase(LeanTweenType)
extern void LTDescr_setEase_mCF284E5CA17BB1BECBB5B1CC1013571B108580A3 (void);
// 0x00000111 LTDescr LTDescr::setEaseLinear()
extern void LTDescr_setEaseLinear_mB1944DC5601584DF8F68F2FD1E844526B4B57D0A (void);
// 0x00000112 LTDescr LTDescr::setEaseSpring()
extern void LTDescr_setEaseSpring_mCD7F60BDFCE5BFF617264B8775C4E80ABED4A465 (void);
// 0x00000113 LTDescr LTDescr::setEaseInQuad()
extern void LTDescr_setEaseInQuad_mC03167FBB2CD3CB171D87440DC20604F51248C6E (void);
// 0x00000114 LTDescr LTDescr::setEaseOutQuad()
extern void LTDescr_setEaseOutQuad_mC87008E2DA94BFA521BB12A74F4FC0F160ED1291 (void);
// 0x00000115 LTDescr LTDescr::setEaseInOutQuad()
extern void LTDescr_setEaseInOutQuad_m22C85D4E11EA5B77869864113758310F097CFDDD (void);
// 0x00000116 LTDescr LTDescr::setEaseInCubic()
extern void LTDescr_setEaseInCubic_m0D93D024F3F116E6A7887F86F0F5203D74FAF5FF (void);
// 0x00000117 LTDescr LTDescr::setEaseOutCubic()
extern void LTDescr_setEaseOutCubic_mEEAA2F960FB30E6B84B3D52BEB173E406A0A8A6F (void);
// 0x00000118 LTDescr LTDescr::setEaseInOutCubic()
extern void LTDescr_setEaseInOutCubic_mACAC52E920C0602EC0D90E6019B3EAE9034D25F9 (void);
// 0x00000119 LTDescr LTDescr::setEaseInQuart()
extern void LTDescr_setEaseInQuart_mEFE37EB2D9BB775BAC45A1D55CABB0D41FA679F0 (void);
// 0x0000011A LTDescr LTDescr::setEaseOutQuart()
extern void LTDescr_setEaseOutQuart_mEAD18F5546A6BE77FE48294B5BB316769338A461 (void);
// 0x0000011B LTDescr LTDescr::setEaseInOutQuart()
extern void LTDescr_setEaseInOutQuart_mAFC9F8155E7348004D99107EE10C10AD51C526B4 (void);
// 0x0000011C LTDescr LTDescr::setEaseInQuint()
extern void LTDescr_setEaseInQuint_m8C116A004899207382401BCADC6F39F4EE3B75B7 (void);
// 0x0000011D LTDescr LTDescr::setEaseOutQuint()
extern void LTDescr_setEaseOutQuint_mA59F24802CD0E1007E659D34F533F8F1CA7FAD13 (void);
// 0x0000011E LTDescr LTDescr::setEaseInOutQuint()
extern void LTDescr_setEaseInOutQuint_mF815B5687F9162E45C9D1AD031C317840712FE19 (void);
// 0x0000011F LTDescr LTDescr::setEaseInSine()
extern void LTDescr_setEaseInSine_mF481D539F3DA2FF9AE13F373BB79B2CC93AE82B6 (void);
// 0x00000120 LTDescr LTDescr::setEaseOutSine()
extern void LTDescr_setEaseOutSine_mD8439D9AD162893667913F82632A087BB7BF66D0 (void);
// 0x00000121 LTDescr LTDescr::setEaseInOutSine()
extern void LTDescr_setEaseInOutSine_m1647757426F728F0A076E73DCB939C469B77E1A9 (void);
// 0x00000122 LTDescr LTDescr::setEaseInExpo()
extern void LTDescr_setEaseInExpo_mE27C2F90670FA473FC8D27DD70D3E4F47A4935C2 (void);
// 0x00000123 LTDescr LTDescr::setEaseOutExpo()
extern void LTDescr_setEaseOutExpo_m9220EBCED76070AE7B72E997FD5711A68D35371F (void);
// 0x00000124 LTDescr LTDescr::setEaseInOutExpo()
extern void LTDescr_setEaseInOutExpo_mA4B0D2B2F98FDD2C5864ECA9000CADFBCE38C8CC (void);
// 0x00000125 LTDescr LTDescr::setEaseInCirc()
extern void LTDescr_setEaseInCirc_m75ABE78F120AC863C1BFCE3426ED6940CA9B30BD (void);
// 0x00000126 LTDescr LTDescr::setEaseOutCirc()
extern void LTDescr_setEaseOutCirc_m2728C6194974BE6C02932FB34B991EE2110FB14D (void);
// 0x00000127 LTDescr LTDescr::setEaseInOutCirc()
extern void LTDescr_setEaseInOutCirc_m75B555F1A463C9A3B59D2DA7642B9A7AD36770F2 (void);
// 0x00000128 LTDescr LTDescr::setEaseInBounce()
extern void LTDescr_setEaseInBounce_mFC166FCC2D1359368A01D60AD95F87880E89E22B (void);
// 0x00000129 LTDescr LTDescr::setEaseOutBounce()
extern void LTDescr_setEaseOutBounce_mF118AA6B5601E83ED52CCDE9A0EA4C92F30A5626 (void);
// 0x0000012A LTDescr LTDescr::setEaseInOutBounce()
extern void LTDescr_setEaseInOutBounce_m1268040122DB6FA1269D298171FADB03DDE8BA9E (void);
// 0x0000012B LTDescr LTDescr::setEaseInBack()
extern void LTDescr_setEaseInBack_m3220F0CC60207776341D8612292803E58A8D2175 (void);
// 0x0000012C LTDescr LTDescr::setEaseOutBack()
extern void LTDescr_setEaseOutBack_mF300DC1C059B5BB6CCD5BFC2CC71B665334AD717 (void);
// 0x0000012D LTDescr LTDescr::setEaseInOutBack()
extern void LTDescr_setEaseInOutBack_mCEA5FB6CF2A0A6335CF1F9F06177A4B663CE1F47 (void);
// 0x0000012E LTDescr LTDescr::setEaseInElastic()
extern void LTDescr_setEaseInElastic_m1FE80F0CD5ABAA68172CE15DB1280788BB53B440 (void);
// 0x0000012F LTDescr LTDescr::setEaseOutElastic()
extern void LTDescr_setEaseOutElastic_m0AED6CBCC243E3EE10DBCE0973F32BF5DD16A873 (void);
// 0x00000130 LTDescr LTDescr::setEaseInOutElastic()
extern void LTDescr_setEaseInOutElastic_m494EE77E1C98A7356847D0B44E4D088A5ACC1F6A (void);
// 0x00000131 LTDescr LTDescr::setEasePunch()
extern void LTDescr_setEasePunch_m7B72691172462D818F06010F316087DBF5EDEB49 (void);
// 0x00000132 LTDescr LTDescr::setEaseShake()
extern void LTDescr_setEaseShake_m6AE6595FBDF8FFB68838D6D20B29C64EBAC941E9 (void);
// 0x00000133 UnityEngine.Vector3 LTDescr::tweenOnCurve()
extern void LTDescr_tweenOnCurve_m84B46DFEB19FDB6D772DE2D47B4366E0A706C4DB (void);
// 0x00000134 UnityEngine.Vector3 LTDescr::easeInOutQuad()
extern void LTDescr_easeInOutQuad_m36065001F18BC0B29C29EBA965B08989B028234F (void);
// 0x00000135 UnityEngine.Vector3 LTDescr::easeInQuad()
extern void LTDescr_easeInQuad_m802EB621FB9F8EF34C4DD060972E506C7D6CD1F0 (void);
// 0x00000136 UnityEngine.Vector3 LTDescr::easeOutQuad()
extern void LTDescr_easeOutQuad_mA2A1E36372772F90ED90F59E97062FE128B6794D (void);
// 0x00000137 UnityEngine.Vector3 LTDescr::easeLinear()
extern void LTDescr_easeLinear_mAD6CECCB459D27BE9B3ACEBDC2F7473F6A688A17 (void);
// 0x00000138 UnityEngine.Vector3 LTDescr::easeSpring()
extern void LTDescr_easeSpring_m9D0A0D6942FF7A2D9DE855C19487D7E9ED6F26E7 (void);
// 0x00000139 UnityEngine.Vector3 LTDescr::easeInCubic()
extern void LTDescr_easeInCubic_mEF13536017E3D96BA908FA0C37028D3BB7FD7304 (void);
// 0x0000013A UnityEngine.Vector3 LTDescr::easeOutCubic()
extern void LTDescr_easeOutCubic_mD5A8610D69B8116750EA64A02DFAB3314EC11C78 (void);
// 0x0000013B UnityEngine.Vector3 LTDescr::easeInOutCubic()
extern void LTDescr_easeInOutCubic_m2F441E0BDA04412B6746865E4D810E30E830C0E3 (void);
// 0x0000013C UnityEngine.Vector3 LTDescr::easeInQuart()
extern void LTDescr_easeInQuart_m865902CA1856CE060535F26E68FC3E48A1B9EC0C (void);
// 0x0000013D UnityEngine.Vector3 LTDescr::easeOutQuart()
extern void LTDescr_easeOutQuart_mE87CA7D4DF0135B45C8536A8FC74D4CDE8EE1357 (void);
// 0x0000013E UnityEngine.Vector3 LTDescr::easeInOutQuart()
extern void LTDescr_easeInOutQuart_m6D290AB4B5EDF285E1A4AD1945B87864B810E672 (void);
// 0x0000013F UnityEngine.Vector3 LTDescr::easeInQuint()
extern void LTDescr_easeInQuint_m3A8922E83FB7FD5921895D0C7C180B251E0D8D87 (void);
// 0x00000140 UnityEngine.Vector3 LTDescr::easeOutQuint()
extern void LTDescr_easeOutQuint_mA84CCBDB25A1E58D82C7A446850ADDA258592A0D (void);
// 0x00000141 UnityEngine.Vector3 LTDescr::easeInOutQuint()
extern void LTDescr_easeInOutQuint_m73C03ABA976E0D1D40E8835BE9A1FE5DBF7F640B (void);
// 0x00000142 UnityEngine.Vector3 LTDescr::easeInSine()
extern void LTDescr_easeInSine_m0608A3B0E9C83EC723B9FB55149DB9837F51AB41 (void);
// 0x00000143 UnityEngine.Vector3 LTDescr::easeOutSine()
extern void LTDescr_easeOutSine_mD364D0BBA67752AE94B5752213507D91A41C3CF2 (void);
// 0x00000144 UnityEngine.Vector3 LTDescr::easeInOutSine()
extern void LTDescr_easeInOutSine_m5817005F731481A8FB1B8B88E63319B0C08E27AA (void);
// 0x00000145 UnityEngine.Vector3 LTDescr::easeInExpo()
extern void LTDescr_easeInExpo_m8975809F06D63CBAC26760C7243779393D7FCCC5 (void);
// 0x00000146 UnityEngine.Vector3 LTDescr::easeOutExpo()
extern void LTDescr_easeOutExpo_m1793556A051CCF34AD78DCC5FEE153CFEDC857F2 (void);
// 0x00000147 UnityEngine.Vector3 LTDescr::easeInOutExpo()
extern void LTDescr_easeInOutExpo_mF6009DCEAA7D84D5E219F93448BF38340917DF47 (void);
// 0x00000148 UnityEngine.Vector3 LTDescr::easeInCirc()
extern void LTDescr_easeInCirc_m7F525DEA40803C0025FAC510774C26F86AB100DC (void);
// 0x00000149 UnityEngine.Vector3 LTDescr::easeOutCirc()
extern void LTDescr_easeOutCirc_m78859138DEAF3005D845A924841F0EA8BBEAD585 (void);
// 0x0000014A UnityEngine.Vector3 LTDescr::easeInOutCirc()
extern void LTDescr_easeInOutCirc_m16338C8496BAE07CDE37BDA8FCF813BFBA7F19FE (void);
// 0x0000014B UnityEngine.Vector3 LTDescr::easeInBounce()
extern void LTDescr_easeInBounce_m16D1EE3AB1D5E67FEBE2EA6DBE9C95A92F084C3F (void);
// 0x0000014C UnityEngine.Vector3 LTDescr::easeOutBounce()
extern void LTDescr_easeOutBounce_mC4DFF3C3FD156884B92838BBABB56570EFDBADFD (void);
// 0x0000014D UnityEngine.Vector3 LTDescr::easeInOutBounce()
extern void LTDescr_easeInOutBounce_mFD18B378E248EB26725EEE7267C20BD3FBED946C (void);
// 0x0000014E UnityEngine.Vector3 LTDescr::easeInBack()
extern void LTDescr_easeInBack_mD2D30FEF3F06538124A2AB1775BCA2CED07256A8 (void);
// 0x0000014F UnityEngine.Vector3 LTDescr::easeOutBack()
extern void LTDescr_easeOutBack_m744F4CCCD32F8F556B68C9577E67B0468BEA3DE8 (void);
// 0x00000150 UnityEngine.Vector3 LTDescr::easeInOutBack()
extern void LTDescr_easeInOutBack_m57DB03AEDB85C66A5925B8FF6F15B7C97E2DDFC2 (void);
// 0x00000151 UnityEngine.Vector3 LTDescr::easeInElastic()
extern void LTDescr_easeInElastic_mDF8A4B5CDAAA652D432B99F536FDE5F12D29CC13 (void);
// 0x00000152 UnityEngine.Vector3 LTDescr::easeOutElastic()
extern void LTDescr_easeOutElastic_m8D73182BD8A48500C5FB89BF4AA84094CCB62F85 (void);
// 0x00000153 UnityEngine.Vector3 LTDescr::easeInOutElastic()
extern void LTDescr_easeInOutElastic_m3BFEB7C3B52B2C061544C8C2849BB7AD000E341C (void);
// 0x00000154 LTDescr LTDescr::setOvershoot(System.Single)
extern void LTDescr_setOvershoot_m0E935F8EBE21ED62F29A62DD2FC4E7C1378E0544 (void);
// 0x00000155 LTDescr LTDescr::setPeriod(System.Single)
extern void LTDescr_setPeriod_m6D8399405BD7E4C3CE73E90B1DF55991A6EEF6B6 (void);
// 0x00000156 LTDescr LTDescr::setScale(System.Single)
extern void LTDescr_setScale_m75F4440E58BAAACBE7205441A2FDB96E61AFE2AB (void);
// 0x00000157 LTDescr LTDescr::setEase(UnityEngine.AnimationCurve)
extern void LTDescr_setEase_mBF09981854FEEF635C7CD97C0CBAB2A941193DC6 (void);
// 0x00000158 LTDescr LTDescr::setTo(UnityEngine.Vector3)
extern void LTDescr_setTo_m829E53D1E4FA8D3C11D35CC8E736AAAC151A8234 (void);
// 0x00000159 LTDescr LTDescr::setTo(UnityEngine.Transform)
extern void LTDescr_setTo_mB3C80993607B61B638F5EC5D773CA42AE33CDB3A (void);
// 0x0000015A LTDescr LTDescr::setFrom(UnityEngine.Vector3)
extern void LTDescr_setFrom_m869544CF9DE9E93780E6ADAED4B99CD3580058AC (void);
// 0x0000015B LTDescr LTDescr::setFrom(System.Single)
extern void LTDescr_setFrom_m916CEBF6049D5ACC8C00337FF34DF141461951A2 (void);
// 0x0000015C LTDescr LTDescr::setDiff(UnityEngine.Vector3)
extern void LTDescr_setDiff_m4E9CA4C17BEAC2C76720E33912216FE25D816E5D (void);
// 0x0000015D LTDescr LTDescr::setHasInitialized(System.Boolean)
extern void LTDescr_setHasInitialized_mDCBCCB7087C97B20C58181593EB1C7A8587C84C6 (void);
// 0x0000015E LTDescr LTDescr::setId(System.UInt32,System.UInt32)
extern void LTDescr_setId_m4F5E561450C9DD9297F9C0A4920E7F56A57BA8F6 (void);
// 0x0000015F LTDescr LTDescr::setPassed(System.Single)
extern void LTDescr_setPassed_m629829107456CBBEA75E743A244D5A8156872ED4 (void);
// 0x00000160 LTDescr LTDescr::setTime(System.Single)
extern void LTDescr_setTime_m9DB98DF42D1DE55F45346D9F4F0C0B836EF5BE82 (void);
// 0x00000161 LTDescr LTDescr::setSpeed(System.Single)
extern void LTDescr_setSpeed_mD51B284F6FEC68EACC2216BCC1CAE20588B88596 (void);
// 0x00000162 LTDescr LTDescr::setRepeat(System.Int32)
extern void LTDescr_setRepeat_m39C1FFE0F3BE7AEA0AB2D3D09B8C5E93471FD528 (void);
// 0x00000163 LTDescr LTDescr::setLoopType(LeanTweenType)
extern void LTDescr_setLoopType_m432119442AFE9EC9DCA026A196ACB390CB6E490A (void);
// 0x00000164 LTDescr LTDescr::setUseEstimatedTime(System.Boolean)
extern void LTDescr_setUseEstimatedTime_m1DCE69547A9FDA123D41FC468334AAB464A9E27A (void);
// 0x00000165 LTDescr LTDescr::setIgnoreTimeScale(System.Boolean)
extern void LTDescr_setIgnoreTimeScale_m74876A4244E7C476990FD9F49A32F2077733D10E (void);
// 0x00000166 LTDescr LTDescr::setUseFrames(System.Boolean)
extern void LTDescr_setUseFrames_mC5C200981C6D66E22A194C46E4E68F4DEE93A583 (void);
// 0x00000167 LTDescr LTDescr::setUseManualTime(System.Boolean)
extern void LTDescr_setUseManualTime_mA583DBD31325B81D677BFDE10848D522F3B13917 (void);
// 0x00000168 LTDescr LTDescr::setLoopCount(System.Int32)
extern void LTDescr_setLoopCount_m5F4204D6F059EAB72ECF645AFFF1909CE328E9A7 (void);
// 0x00000169 LTDescr LTDescr::setLoopOnce()
extern void LTDescr_setLoopOnce_mC7EF69D620AE0723DBF4F7786DDE722A9CC46914 (void);
// 0x0000016A LTDescr LTDescr::setLoopClamp()
extern void LTDescr_setLoopClamp_m26C0A8ECA2F70ADA1C48A2C92B13B47B58E2B1B0 (void);
// 0x0000016B LTDescr LTDescr::setLoopClamp(System.Int32)
extern void LTDescr_setLoopClamp_m45FC70613BB57B5B72A397242071368CCC539E35 (void);
// 0x0000016C LTDescr LTDescr::setLoopPingPong()
extern void LTDescr_setLoopPingPong_m53E03879D46B899429F463427A5EACEE747F9636 (void);
// 0x0000016D LTDescr LTDescr::setLoopPingPong(System.Int32)
extern void LTDescr_setLoopPingPong_m21667D1F3685ED55D706C025D48849E61D655B0E (void);
// 0x0000016E LTDescr LTDescr::setOnComplete(System.Action)
extern void LTDescr_setOnComplete_mDCB57CB9AFEC6C5FFD63A9B89962DEE80BD1E72C (void);
// 0x0000016F LTDescr LTDescr::setOnComplete(System.Action`1<System.Object>)
extern void LTDescr_setOnComplete_m63F5635BC23F15B3B64960290B0D723D36BC1030 (void);
// 0x00000170 LTDescr LTDescr::setOnComplete(System.Action`1<System.Object>,System.Object)
extern void LTDescr_setOnComplete_m2D493BA5F77149765D519E685C05ADEC2F9EE5E6 (void);
// 0x00000171 LTDescr LTDescr::setOnCompleteParam(System.Object)
extern void LTDescr_setOnCompleteParam_m4172EF1D7B63D5B2BA5B4358BB8282FF55A5D9C2 (void);
// 0x00000172 LTDescr LTDescr::setOnUpdate(System.Action`1<System.Single>)
extern void LTDescr_setOnUpdate_m437DAC29C60F0FFBE61C926313DF48FBB1BBDDE7 (void);
// 0x00000173 LTDescr LTDescr::setOnUpdateRatio(System.Action`2<System.Single,System.Single>)
extern void LTDescr_setOnUpdateRatio_m004D6332C968E0F0059FF807E34770989AA009F6 (void);
// 0x00000174 LTDescr LTDescr::setOnUpdateObject(System.Action`2<System.Single,System.Object>)
extern void LTDescr_setOnUpdateObject_m770A74E823CD5DB5B6448F7A66DED37B968893D1 (void);
// 0x00000175 LTDescr LTDescr::setOnUpdateVector2(System.Action`1<UnityEngine.Vector2>)
extern void LTDescr_setOnUpdateVector2_m7FD189610C25124F725477B4DC948F11A69455B1 (void);
// 0x00000176 LTDescr LTDescr::setOnUpdateVector3(System.Action`1<UnityEngine.Vector3>)
extern void LTDescr_setOnUpdateVector3_m0DDC2E6AEDC0E0EACE3947E4E67A6A6803EEFABA (void);
// 0x00000177 LTDescr LTDescr::setOnUpdateColor(System.Action`1<UnityEngine.Color>)
extern void LTDescr_setOnUpdateColor_mDAB53EF324EB204A7038AC70A88978C5E93D8009 (void);
// 0x00000178 LTDescr LTDescr::setOnUpdateColor(System.Action`2<UnityEngine.Color,System.Object>)
extern void LTDescr_setOnUpdateColor_mF0B148249135750B5B772E6FA0EFC59A9925E69D (void);
// 0x00000179 LTDescr LTDescr::setOnUpdate(System.Action`1<UnityEngine.Color>)
extern void LTDescr_setOnUpdate_mDFE9026BDBF9479028A6D2DF03C3F9E1C391E354 (void);
// 0x0000017A LTDescr LTDescr::setOnUpdate(System.Action`2<UnityEngine.Color,System.Object>)
extern void LTDescr_setOnUpdate_mA5CB1382CE68571CE5DB619406AC5C6EB22729B9 (void);
// 0x0000017B LTDescr LTDescr::setOnUpdate(System.Action`2<System.Single,System.Object>,System.Object)
extern void LTDescr_setOnUpdate_mEFD57B3855E8C131BA76DADD3FD13C41E867F58A (void);
// 0x0000017C LTDescr LTDescr::setOnUpdate(System.Action`2<UnityEngine.Vector3,System.Object>,System.Object)
extern void LTDescr_setOnUpdate_mEFD26172DE9342C6C0FB34248E3EA7F6267A1968 (void);
// 0x0000017D LTDescr LTDescr::setOnUpdate(System.Action`1<UnityEngine.Vector2>,System.Object)
extern void LTDescr_setOnUpdate_m73910A7E30781DFDE8149BF06376A3CB98DDF74B (void);
// 0x0000017E LTDescr LTDescr::setOnUpdate(System.Action`1<UnityEngine.Vector3>,System.Object)
extern void LTDescr_setOnUpdate_mAFDE1872327547292A82B3D393F4B7007913EFDB (void);
// 0x0000017F LTDescr LTDescr::setOnUpdateParam(System.Object)
extern void LTDescr_setOnUpdateParam_mB449256AC309484AE7FF7E1E685A8B6B5727D4D0 (void);
// 0x00000180 LTDescr LTDescr::setOrientToPath(System.Boolean)
extern void LTDescr_setOrientToPath_m6458FAA1A9AACC5BA13A1C83AEB87A63883CFBE4 (void);
// 0x00000181 LTDescr LTDescr::setOrientToPath2d(System.Boolean)
extern void LTDescr_setOrientToPath2d_m135B485E55660A23D7FE3A51AA1C5D3E38804E27 (void);
// 0x00000182 LTDescr LTDescr::setRect(LTRect)
extern void LTDescr_setRect_mDE3387640B652E3851AA62B4483FE0150067BCF6 (void);
// 0x00000183 LTDescr LTDescr::setRect(UnityEngine.Rect)
extern void LTDescr_setRect_mFF955D0F4C0518F219B3DF1063035FF8AD8B5A4B (void);
// 0x00000184 LTDescr LTDescr::setPath(LTBezierPath)
extern void LTDescr_setPath_mD75B1338F2117AEDC0E243C925038014EEC86974 (void);
// 0x00000185 LTDescr LTDescr::setPoint(UnityEngine.Vector3)
extern void LTDescr_setPoint_mE041B263E9365DE9D2C8A12537A69E59DE02D311 (void);
// 0x00000186 LTDescr LTDescr::setDestroyOnComplete(System.Boolean)
extern void LTDescr_setDestroyOnComplete_mD7468721AACBFC786485DC873912EF6DC442D831 (void);
// 0x00000187 LTDescr LTDescr::setAudio(System.Object)
extern void LTDescr_setAudio_m5AA3E45C784E79CC21DB55C4789C4C3B8DB5FE9B (void);
// 0x00000188 LTDescr LTDescr::setOnCompleteOnRepeat(System.Boolean)
extern void LTDescr_setOnCompleteOnRepeat_m6BB648BEEBC71B54A8EA1CD68E4C09C1BD0ED414 (void);
// 0x00000189 LTDescr LTDescr::setOnCompleteOnStart(System.Boolean)
extern void LTDescr_setOnCompleteOnStart_mEF3D29199EA20722C91FD0FA1663A279D46036F0 (void);
// 0x0000018A LTDescr LTDescr::setRect(UnityEngine.RectTransform)
extern void LTDescr_setRect_m24CBF3848B9211ED00193D668308C5849294191D (void);
// 0x0000018B LTDescr LTDescr::setSprites(UnityEngine.Sprite[])
extern void LTDescr_setSprites_mCA611B87878CCED8217DD98E25AC53FB8874B5BA (void);
// 0x0000018C LTDescr LTDescr::setFrameRate(System.Single)
extern void LTDescr_setFrameRate_mA3F0847615F3E5A97C192C548D40BDC1B6BC48F2 (void);
// 0x0000018D LTDescr LTDescr::setOnStart(System.Action)
extern void LTDescr_setOnStart_mAA21647EAE7FBD188227754F8F8E175985CD9379 (void);
// 0x0000018E LTDescr LTDescr::setDirection(System.Single)
extern void LTDescr_setDirection_m1C23D5D973D7CB4391403DFEFBDF12F15A1E75B4 (void);
// 0x0000018F LTDescr LTDescr::setRecursive(System.Boolean)
extern void LTDescr_setRecursive_mB6A214753796545145520D40C7AF9C7B85B8DC97 (void);
// 0x00000190 System.Void LTDescr::<setMoveX>b__73_0()
extern void LTDescr_U3CsetMoveXU3Eb__73_0_mC1B3A6D25B73B3945BA6BAE2A0B8048B2E7706F4 (void);
// 0x00000191 System.Void LTDescr::<setMoveX>b__73_1()
extern void LTDescr_U3CsetMoveXU3Eb__73_1_mB298DE15C613EDB42361E42D3935981846884621 (void);
// 0x00000192 System.Void LTDescr::<setMoveY>b__74_0()
extern void LTDescr_U3CsetMoveYU3Eb__74_0_mB45AE75CE0B616D21D397C4600C3FA7E19608E21 (void);
// 0x00000193 System.Void LTDescr::<setMoveY>b__74_1()
extern void LTDescr_U3CsetMoveYU3Eb__74_1_mC7C72E8D96740B0F4F476545FF9A9CE6579D773C (void);
// 0x00000194 System.Void LTDescr::<setMoveZ>b__75_0()
extern void LTDescr_U3CsetMoveZU3Eb__75_0_m2F90BBD09164CCA80404F98AEBE39E0C7C49E140 (void);
// 0x00000195 System.Void LTDescr::<setMoveZ>b__75_1()
extern void LTDescr_U3CsetMoveZU3Eb__75_1_mB0FCBC7EDEE189C9FC90E38F51C5F36FFB1C6E75 (void);
// 0x00000196 System.Void LTDescr::<setMoveLocalX>b__76_0()
extern void LTDescr_U3CsetMoveLocalXU3Eb__76_0_m2E2D10CEC52049D66A18E61B7A6820F5693D1000 (void);
// 0x00000197 System.Void LTDescr::<setMoveLocalX>b__76_1()
extern void LTDescr_U3CsetMoveLocalXU3Eb__76_1_m2B451B55022DFA5A2149080DCF5EFEA51DD1EF15 (void);
// 0x00000198 System.Void LTDescr::<setMoveLocalY>b__77_0()
extern void LTDescr_U3CsetMoveLocalYU3Eb__77_0_mD773679AD1B3F289E97B8A97FA3C9816B43B0F97 (void);
// 0x00000199 System.Void LTDescr::<setMoveLocalY>b__77_1()
extern void LTDescr_U3CsetMoveLocalYU3Eb__77_1_mD806F3C4BD68BD6A88B7E47DD52B9E8EF67516DA (void);
// 0x0000019A System.Void LTDescr::<setMoveLocalZ>b__78_0()
extern void LTDescr_U3CsetMoveLocalZU3Eb__78_0_mDFB2967D5068D7DA7D1FBD20D8F621C8A7E4A5C7 (void);
// 0x0000019B System.Void LTDescr::<setMoveLocalZ>b__78_1()
extern void LTDescr_U3CsetMoveLocalZU3Eb__78_1_m9FE503FE722E8A8877BF3559FFA4EFF806841A0E (void);
// 0x0000019C System.Void LTDescr::<setMoveCurved>b__81_0()
extern void LTDescr_U3CsetMoveCurvedU3Eb__81_0_m93E47D28F6BC0CAEF79EC1B39091B66D2ABFF67F (void);
// 0x0000019D System.Void LTDescr::<setMoveCurvedLocal>b__82_0()
extern void LTDescr_U3CsetMoveCurvedLocalU3Eb__82_0_m7AB4E3AF580B52EE389B4264B1C3DC08035631EF (void);
// 0x0000019E System.Void LTDescr::<setMoveSpline>b__83_0()
extern void LTDescr_U3CsetMoveSplineU3Eb__83_0_mB704A557E71BAE0EEADA2DBDC7C85EBDE842174D (void);
// 0x0000019F System.Void LTDescr::<setMoveSplineLocal>b__84_0()
extern void LTDescr_U3CsetMoveSplineLocalU3Eb__84_0_m71CA556831D55D2294212AF2E49D1C38ACB38C73 (void);
// 0x000001A0 System.Void LTDescr::<setScaleX>b__85_0()
extern void LTDescr_U3CsetScaleXU3Eb__85_0_mFF1273FC3FC6E6955FDF2804FB5894109E1E6877 (void);
// 0x000001A1 System.Void LTDescr::<setScaleX>b__85_1()
extern void LTDescr_U3CsetScaleXU3Eb__85_1_mB8660436928C1ABFE1AAA2988DC2AC0BD6CEDFAB (void);
// 0x000001A2 System.Void LTDescr::<setScaleY>b__86_0()
extern void LTDescr_U3CsetScaleYU3Eb__86_0_m86846124370FA864FD9E0A6F84F3ACFFB73946DB (void);
// 0x000001A3 System.Void LTDescr::<setScaleY>b__86_1()
extern void LTDescr_U3CsetScaleYU3Eb__86_1_m2A79BFC6964E50B4602F3C4D17A372BA1712F4E2 (void);
// 0x000001A4 System.Void LTDescr::<setScaleZ>b__87_0()
extern void LTDescr_U3CsetScaleZU3Eb__87_0_m5BB4E20E122944640691C42C328283ACB0E779AA (void);
// 0x000001A5 System.Void LTDescr::<setScaleZ>b__87_1()
extern void LTDescr_U3CsetScaleZU3Eb__87_1_mDF138908ABE364C4CA46DA854F9C1A01F3A115F8 (void);
// 0x000001A6 System.Void LTDescr::<setRotateX>b__88_0()
extern void LTDescr_U3CsetRotateXU3Eb__88_0_m493F46830D9AE2D265D85AECD9CC07542CB2D164 (void);
// 0x000001A7 System.Void LTDescr::<setRotateX>b__88_1()
extern void LTDescr_U3CsetRotateXU3Eb__88_1_mC1D2B816B4365D56F639D50DDA16AE00A3E6FF5F (void);
// 0x000001A8 System.Void LTDescr::<setRotateY>b__89_0()
extern void LTDescr_U3CsetRotateYU3Eb__89_0_m0C043D7BDF837A3C3FDA107395C85D181B66F13F (void);
// 0x000001A9 System.Void LTDescr::<setRotateY>b__89_1()
extern void LTDescr_U3CsetRotateYU3Eb__89_1_m4E38375358C4BC49BD7BF69476F9B0FA5525BACF (void);
// 0x000001AA System.Void LTDescr::<setRotateZ>b__90_0()
extern void LTDescr_U3CsetRotateZU3Eb__90_0_mA1C23C492D18C89ED39A5E0B2B24F14930DAB66A (void);
// 0x000001AB System.Void LTDescr::<setRotateZ>b__90_1()
extern void LTDescr_U3CsetRotateZU3Eb__90_1_mD77CC83BA388F7F3A8E101D31AC5662901F347B3 (void);
// 0x000001AC System.Void LTDescr::<setRotateAround>b__91_0()
extern void LTDescr_U3CsetRotateAroundU3Eb__91_0_mC30EB5DCA1B236C2D2BFB1BC8C2F620729399547 (void);
// 0x000001AD System.Void LTDescr::<setRotateAround>b__91_1()
extern void LTDescr_U3CsetRotateAroundU3Eb__91_1_m54A375FE4EE6BB07CB382CDA4B91D655E30D8177 (void);
// 0x000001AE System.Void LTDescr::<setRotateAroundLocal>b__92_0()
extern void LTDescr_U3CsetRotateAroundLocalU3Eb__92_0_m71580817297D4C28EF66A991FE629314AD3FE249 (void);
// 0x000001AF System.Void LTDescr::<setRotateAroundLocal>b__92_1()
extern void LTDescr_U3CsetRotateAroundLocalU3Eb__92_1_mEA423F4B08C78D3F9319DF0CF2C2EA95CFA40595 (void);
// 0x000001B0 System.Void LTDescr::<setAlpha>b__93_0()
extern void LTDescr_U3CsetAlphaU3Eb__93_0_m62840E0A1F518EEED15E89CA1B8FE51800CB6ACA (void);
// 0x000001B1 System.Void LTDescr::<setAlpha>b__93_2()
extern void LTDescr_U3CsetAlphaU3Eb__93_2_m74F5E55F3656D02C098B8D5F51444822BCBF619E (void);
// 0x000001B2 System.Void LTDescr::<setAlpha>b__93_1()
extern void LTDescr_U3CsetAlphaU3Eb__93_1_m5E1833D9D31C62DF5234D7AD9CB657BE909F10C0 (void);
// 0x000001B3 System.Void LTDescr::<setTextAlpha>b__94_0()
extern void LTDescr_U3CsetTextAlphaU3Eb__94_0_m8DFC387E69EA4F5D71017CA03EBBB3B865041251 (void);
// 0x000001B4 System.Void LTDescr::<setTextAlpha>b__94_1()
extern void LTDescr_U3CsetTextAlphaU3Eb__94_1_mF29634A706AB1DFF06D7ED9D5314C9CC10998676 (void);
// 0x000001B5 System.Void LTDescr::<setAlphaVertex>b__95_0()
extern void LTDescr_U3CsetAlphaVertexU3Eb__95_0_m38DB9282A06B18A985B4FA2A3051210DB0796FA3 (void);
// 0x000001B6 System.Void LTDescr::<setAlphaVertex>b__95_1()
extern void LTDescr_U3CsetAlphaVertexU3Eb__95_1_m9B16A2B73E285ABB14E7AECE55CACB8C04BE2ECB (void);
// 0x000001B7 System.Void LTDescr::<setColor>b__96_0()
extern void LTDescr_U3CsetColorU3Eb__96_0_mC4C335EE7D3765075D4287E9C5C4E185A010B463 (void);
// 0x000001B8 System.Void LTDescr::<setColor>b__96_1()
extern void LTDescr_U3CsetColorU3Eb__96_1_mF45002C64332B080B32EE1056139487352147496 (void);
// 0x000001B9 System.Void LTDescr::<setCallbackColor>b__97_0()
extern void LTDescr_U3CsetCallbackColorU3Eb__97_0_m0C6A0800CAAB148669FB87B0E686A60E9E0E1E50 (void);
// 0x000001BA System.Void LTDescr::<setCallbackColor>b__97_1()
extern void LTDescr_U3CsetCallbackColorU3Eb__97_1_mD30A488146A9F8DE2876C79404BF0A69361CFFC8 (void);
// 0x000001BB System.Void LTDescr::<setTextColor>b__98_0()
extern void LTDescr_U3CsetTextColorU3Eb__98_0_m3369FE91DC4472C45068C9B348E308991CDB0D92 (void);
// 0x000001BC System.Void LTDescr::<setTextColor>b__98_1()
extern void LTDescr_U3CsetTextColorU3Eb__98_1_mAD33FF9EE92E06980CBEB4C7759E32DA69F625EB (void);
// 0x000001BD System.Void LTDescr::<setCanvasAlpha>b__99_0()
extern void LTDescr_U3CsetCanvasAlphaU3Eb__99_0_m4AC161231D4D1B57CA69B7BA0323F4E6CF0F4CA2 (void);
// 0x000001BE System.Void LTDescr::<setCanvasAlpha>b__99_1()
extern void LTDescr_U3CsetCanvasAlphaU3Eb__99_1_m2B6D509A77B2396A304997DC39DCB3455E4D198F (void);
// 0x000001BF System.Void LTDescr::<setCanvasGroupAlpha>b__100_0()
extern void LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_0_mF33DE2956D5D901DC51B5D9924EFD4FD3E38ED57 (void);
// 0x000001C0 System.Void LTDescr::<setCanvasGroupAlpha>b__100_1()
extern void LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_1_mBA6DC3BCE7F96E4D726A0AC809397EE35FFFF222 (void);
// 0x000001C1 System.Void LTDescr::<setCanvasColor>b__101_0()
extern void LTDescr_U3CsetCanvasColorU3Eb__101_0_m7054A2F4C59932B279ACCE46880C9832B8A9D889 (void);
// 0x000001C2 System.Void LTDescr::<setCanvasColor>b__101_1()
extern void LTDescr_U3CsetCanvasColorU3Eb__101_1_m6460741C9E869E7B10CB86D933E4BDCA18D737ED (void);
// 0x000001C3 System.Void LTDescr::<setCanvasMoveX>b__102_0()
extern void LTDescr_U3CsetCanvasMoveXU3Eb__102_0_mFE234BD5C94E07BCF7414CB6D52994226A53EF0C (void);
// 0x000001C4 System.Void LTDescr::<setCanvasMoveX>b__102_1()
extern void LTDescr_U3CsetCanvasMoveXU3Eb__102_1_m2DA6E431C160C39E876CD86BFF777B6D8D0C0894 (void);
// 0x000001C5 System.Void LTDescr::<setCanvasMoveY>b__103_0()
extern void LTDescr_U3CsetCanvasMoveYU3Eb__103_0_m54D0560CC7DACB6DF5B711C9DFF85C1A08524104 (void);
// 0x000001C6 System.Void LTDescr::<setCanvasMoveY>b__103_1()
extern void LTDescr_U3CsetCanvasMoveYU3Eb__103_1_m25A736629C0A77D4B89641F916444588968FA12B (void);
// 0x000001C7 System.Void LTDescr::<setCanvasMoveZ>b__104_0()
extern void LTDescr_U3CsetCanvasMoveZU3Eb__104_0_m334C0650BE3F004D3246A37F9703EE58F310AFEF (void);
// 0x000001C8 System.Void LTDescr::<setCanvasMoveZ>b__104_1()
extern void LTDescr_U3CsetCanvasMoveZU3Eb__104_1_m093E0527B343F64B8D424AFFF2FA73EFF4CD23AC (void);
// 0x000001C9 System.Void LTDescr::<setCanvasRotateAround>b__106_0()
extern void LTDescr_U3CsetCanvasRotateAroundU3Eb__106_0_m77AE4A4821E0D9DA232D32B3F85AA6D17B102CE1 (void);
// 0x000001CA System.Void LTDescr::<setCanvasRotateAroundLocal>b__107_0()
extern void LTDescr_U3CsetCanvasRotateAroundLocalU3Eb__107_0_m5B22A810186C6A02EEA6C3D1F9E7823A58F48A8A (void);
// 0x000001CB System.Void LTDescr::<setCanvasPlaySprite>b__108_0()
extern void LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_0_mCCC3A883C06689CE1AF4E56F599E17E6A08063D9 (void);
// 0x000001CC System.Void LTDescr::<setCanvasPlaySprite>b__108_1()
extern void LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_1_mEEA14A276656406AA550A39A45E0EF0DBCFCAD00 (void);
// 0x000001CD System.Void LTDescr::<setCanvasMove>b__109_0()
extern void LTDescr_U3CsetCanvasMoveU3Eb__109_0_m9B67812E976E616B40413119359E8A358B779DD9 (void);
// 0x000001CE System.Void LTDescr::<setCanvasMove>b__109_1()
extern void LTDescr_U3CsetCanvasMoveU3Eb__109_1_m515B75E2D9C1099D35D29062A747D6881F3FD22A (void);
// 0x000001CF System.Void LTDescr::<setCanvasScale>b__110_0()
extern void LTDescr_U3CsetCanvasScaleU3Eb__110_0_m66C78DAA27E664E37107383D3F72D9EAE85E93B5 (void);
// 0x000001D0 System.Void LTDescr::<setCanvasScale>b__110_1()
extern void LTDescr_U3CsetCanvasScaleU3Eb__110_1_m7DED18C9319531415228CB9DAD9E40A0578BA7D6 (void);
// 0x000001D1 System.Void LTDescr::<setCanvasSizeDelta>b__111_0()
extern void LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_0_m622448B8EA313708FCB496103613BE9368191CFA (void);
// 0x000001D2 System.Void LTDescr::<setCanvasSizeDelta>b__111_1()
extern void LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_1_mEB78ECCD2C73EDA02A10E61A43F44B0522D99FA8 (void);
// 0x000001D3 System.Void LTDescr::<setMove>b__115_0()
extern void LTDescr_U3CsetMoveU3Eb__115_0_mA9FC21145770DFE3CD83595E33E2A3F9CBA088A5 (void);
// 0x000001D4 System.Void LTDescr::<setMove>b__115_1()
extern void LTDescr_U3CsetMoveU3Eb__115_1_m268228A84C3365C7DFD41C66717C908DD0408766 (void);
// 0x000001D5 System.Void LTDescr::<setMoveLocal>b__116_0()
extern void LTDescr_U3CsetMoveLocalU3Eb__116_0_m81C2B5508318FB8C79015D00599694265975859F (void);
// 0x000001D6 System.Void LTDescr::<setMoveLocal>b__116_1()
extern void LTDescr_U3CsetMoveLocalU3Eb__116_1_m4FC9E996AA295A46BEAF965E85DC9372A8A1C081 (void);
// 0x000001D7 System.Void LTDescr::<setMoveToTransform>b__117_0()
extern void LTDescr_U3CsetMoveToTransformU3Eb__117_0_m4A1FC4A154E19A07F743509A9044C9447DB2A24E (void);
// 0x000001D8 System.Void LTDescr::<setMoveToTransform>b__117_1()
extern void LTDescr_U3CsetMoveToTransformU3Eb__117_1_m1CED29CFFC5D0BE430F5DCC24367B64CEE4E43F3 (void);
// 0x000001D9 System.Void LTDescr::<setRotate>b__118_0()
extern void LTDescr_U3CsetRotateU3Eb__118_0_m23A4A1AD7B915AC5EA95B9AE5231809F4E7A7D97 (void);
// 0x000001DA System.Void LTDescr::<setRotate>b__118_1()
extern void LTDescr_U3CsetRotateU3Eb__118_1_m575493C2DA420D01FEA96EE0AA7E4871430EBD55 (void);
// 0x000001DB System.Void LTDescr::<setRotateLocal>b__119_0()
extern void LTDescr_U3CsetRotateLocalU3Eb__119_0_m8A85181B21112A32CE5CCE169692DEDB501B4E23 (void);
// 0x000001DC System.Void LTDescr::<setRotateLocal>b__119_1()
extern void LTDescr_U3CsetRotateLocalU3Eb__119_1_m41757196871546467230FABFFB37075B85251B1F (void);
// 0x000001DD System.Void LTDescr::<setScale>b__120_0()
extern void LTDescr_U3CsetScaleU3Eb__120_0_mC7315F796459D639557D6B0F54514AEED391BA35 (void);
// 0x000001DE System.Void LTDescr::<setScale>b__120_1()
extern void LTDescr_U3CsetScaleU3Eb__120_1_mE6F7F8EAEC18A9A19D4D698E0FD8034E91F763F6 (void);
// 0x000001DF System.Void LTDescr::<setGUIMove>b__121_0()
extern void LTDescr_U3CsetGUIMoveU3Eb__121_0_m1224EB2264D882E8A0055B5AE36CEEE740B3F21C (void);
// 0x000001E0 System.Void LTDescr::<setGUIMove>b__121_1()
extern void LTDescr_U3CsetGUIMoveU3Eb__121_1_m1A995AAE2505372C605DC64A13E5F8D9282307FE (void);
// 0x000001E1 System.Void LTDescr::<setGUIMoveMargin>b__122_0()
extern void LTDescr_U3CsetGUIMoveMarginU3Eb__122_0_mA08F3D978D14B8FE43A13FD6C51C6EA287D70276 (void);
// 0x000001E2 System.Void LTDescr::<setGUIMoveMargin>b__122_1()
extern void LTDescr_U3CsetGUIMoveMarginU3Eb__122_1_m6C40E08EF41DE0B32C19DF4483E1D153D1CA04AF (void);
// 0x000001E3 System.Void LTDescr::<setGUIScale>b__123_0()
extern void LTDescr_U3CsetGUIScaleU3Eb__123_0_m713651C4163C0D725D46C2CE175BEA1E8A541770 (void);
// 0x000001E4 System.Void LTDescr::<setGUIScale>b__123_1()
extern void LTDescr_U3CsetGUIScaleU3Eb__123_1_mD45B431D66ED1E7AA392547A69C2F14698FC31DD (void);
// 0x000001E5 System.Void LTDescr::<setGUIAlpha>b__124_0()
extern void LTDescr_U3CsetGUIAlphaU3Eb__124_0_mB1E87936B50CEA5B8066511633A64DC06C437B7D (void);
// 0x000001E6 System.Void LTDescr::<setGUIAlpha>b__124_1()
extern void LTDescr_U3CsetGUIAlphaU3Eb__124_1_m8E111699E7BB33552DF1284E5B6E793D53A0E32A (void);
// 0x000001E7 System.Void LTDescr::<setGUIRotate>b__125_0()
extern void LTDescr_U3CsetGUIRotateU3Eb__125_0_m608C2F49CC8E321754F2F2C67186893EC1BCF121 (void);
// 0x000001E8 System.Void LTDescr::<setGUIRotate>b__125_1()
extern void LTDescr_U3CsetGUIRotateU3Eb__125_1_m3CF50F3F6A1980ECB3712F1A437DF9A3EE9A7A17 (void);
// 0x000001E9 System.Void LTDescr::<setDelayedSound>b__126_0()
extern void LTDescr_U3CsetDelayedSoundU3Eb__126_0_mC6AFAF26799C7A75AE029A2F1B6ACCFC0EC9C714 (void);
// 0x000001EA UnityEngine.Transform LTDescrOptional::get_toTrans()
extern void LTDescrOptional_get_toTrans_mB530A713C24C3F06F8B42AF31CA3FF977AB7EE0C (void);
// 0x000001EB System.Void LTDescrOptional::set_toTrans(UnityEngine.Transform)
extern void LTDescrOptional_set_toTrans_mD1A169A39C828FA5536708E3A19F1A1D24370ADA (void);
// 0x000001EC UnityEngine.Vector3 LTDescrOptional::get_point()
extern void LTDescrOptional_get_point_m684081D83B6A9895107F41A78E6FB2EF4F871A62 (void);
// 0x000001ED System.Void LTDescrOptional::set_point(UnityEngine.Vector3)
extern void LTDescrOptional_set_point_m699F8960E9420CC80E624E3A914294836144D84B (void);
// 0x000001EE UnityEngine.Vector3 LTDescrOptional::get_axis()
extern void LTDescrOptional_get_axis_mB136A693C94957550FB4A493D8055AF2A8498950 (void);
// 0x000001EF System.Void LTDescrOptional::set_axis(UnityEngine.Vector3)
extern void LTDescrOptional_set_axis_m3B316ED04F0DD39AA46549611DD2A87CB09563A3 (void);
// 0x000001F0 System.Single LTDescrOptional::get_lastVal()
extern void LTDescrOptional_get_lastVal_mF32405DF5E4967E0526173E12A1D49ED459DEE96 (void);
// 0x000001F1 System.Void LTDescrOptional::set_lastVal(System.Single)
extern void LTDescrOptional_set_lastVal_m10DF605FAB025AE13E603C2272486F8A4CB90A92 (void);
// 0x000001F2 UnityEngine.Quaternion LTDescrOptional::get_origRotation()
extern void LTDescrOptional_get_origRotation_m07382A5C09A8A47B7D9C9DE129BD7CF56E59FDBF (void);
// 0x000001F3 System.Void LTDescrOptional::set_origRotation(UnityEngine.Quaternion)
extern void LTDescrOptional_set_origRotation_m72912AB74F93857BB64706F91607E176B7C5B084 (void);
// 0x000001F4 LTBezierPath LTDescrOptional::get_path()
extern void LTDescrOptional_get_path_m70C0472D3F2618618EA9A0EA152799AD814CBF56 (void);
// 0x000001F5 System.Void LTDescrOptional::set_path(LTBezierPath)
extern void LTDescrOptional_set_path_mBA7489CB0E86A582174D0A5E183CC0BE9FF859A0 (void);
// 0x000001F6 LTSpline LTDescrOptional::get_spline()
extern void LTDescrOptional_get_spline_m8DF792E3FA917F95827962B53715396B14B58CFE (void);
// 0x000001F7 System.Void LTDescrOptional::set_spline(LTSpline)
extern void LTDescrOptional_set_spline_mE5EEFABD4DE99672B93F6A46B09B90C272EEE01F (void);
// 0x000001F8 LTRect LTDescrOptional::get_ltRect()
extern void LTDescrOptional_get_ltRect_m6906D85E922AD1856AEEDE0E2736AA2CAD13401C (void);
// 0x000001F9 System.Void LTDescrOptional::set_ltRect(LTRect)
extern void LTDescrOptional_set_ltRect_m16894F8919730156E369DB2273FDA7EFF3736C1E (void);
// 0x000001FA System.Action`1<System.Single> LTDescrOptional::get_onUpdateFloat()
extern void LTDescrOptional_get_onUpdateFloat_m22C332D592FA821F4A74034B3F77061500B9728E (void);
// 0x000001FB System.Void LTDescrOptional::set_onUpdateFloat(System.Action`1<System.Single>)
extern void LTDescrOptional_set_onUpdateFloat_m2396684827436DC9B10053C9AC2C10A53DA8AECD (void);
// 0x000001FC System.Action`2<System.Single,System.Single> LTDescrOptional::get_onUpdateFloatRatio()
extern void LTDescrOptional_get_onUpdateFloatRatio_m6B22F53C31109E96E7E48E43A9B44651838F7945 (void);
// 0x000001FD System.Void LTDescrOptional::set_onUpdateFloatRatio(System.Action`2<System.Single,System.Single>)
extern void LTDescrOptional_set_onUpdateFloatRatio_m94CE022ED233DA9D79AA3F18C8E6054C2D8E6573 (void);
// 0x000001FE System.Action`2<System.Single,System.Object> LTDescrOptional::get_onUpdateFloatObject()
extern void LTDescrOptional_get_onUpdateFloatObject_mBCB3D7865F5F9ABAC4C5A03DD91241BF2B1AB601 (void);
// 0x000001FF System.Void LTDescrOptional::set_onUpdateFloatObject(System.Action`2<System.Single,System.Object>)
extern void LTDescrOptional_set_onUpdateFloatObject_m81041094C42F5FF531D4C26135A74444B4EAC4EC (void);
// 0x00000200 System.Action`1<UnityEngine.Vector2> LTDescrOptional::get_onUpdateVector2()
extern void LTDescrOptional_get_onUpdateVector2_m310F0582519C2897287E219E5AE5EB4A034F3FD1 (void);
// 0x00000201 System.Void LTDescrOptional::set_onUpdateVector2(System.Action`1<UnityEngine.Vector2>)
extern void LTDescrOptional_set_onUpdateVector2_m843CED0BD5F49E0B9CADEE1E0D5C1F418A77F5BB (void);
// 0x00000202 System.Action`1<UnityEngine.Vector3> LTDescrOptional::get_onUpdateVector3()
extern void LTDescrOptional_get_onUpdateVector3_m1EAF8967B52F428EF0BB00792489D5F9DBC2C959 (void);
// 0x00000203 System.Void LTDescrOptional::set_onUpdateVector3(System.Action`1<UnityEngine.Vector3>)
extern void LTDescrOptional_set_onUpdateVector3_m8AB5DC8AFCF8FBA6132342A8E662D0BAD10B22E0 (void);
// 0x00000204 System.Action`2<UnityEngine.Vector3,System.Object> LTDescrOptional::get_onUpdateVector3Object()
extern void LTDescrOptional_get_onUpdateVector3Object_m1BC1054A2FBE8FABCE6F6DDB453A60685C8B86B8 (void);
// 0x00000205 System.Void LTDescrOptional::set_onUpdateVector3Object(System.Action`2<UnityEngine.Vector3,System.Object>)
extern void LTDescrOptional_set_onUpdateVector3Object_m98BFC4AA1096210EC4D9374E8AD0DC61E927FBEB (void);
// 0x00000206 System.Action`1<UnityEngine.Color> LTDescrOptional::get_onUpdateColor()
extern void LTDescrOptional_get_onUpdateColor_m2ED4389F5DCBCE5D9157F6DB12EEFE229D279006 (void);
// 0x00000207 System.Void LTDescrOptional::set_onUpdateColor(System.Action`1<UnityEngine.Color>)
extern void LTDescrOptional_set_onUpdateColor_m71FBD24255ADA5C0CBA8E473AFB71F18D4A958CE (void);
// 0x00000208 System.Action`2<UnityEngine.Color,System.Object> LTDescrOptional::get_onUpdateColorObject()
extern void LTDescrOptional_get_onUpdateColorObject_m8942669D7FE94243BCF809D11BAF52E289CF3065 (void);
// 0x00000209 System.Void LTDescrOptional::set_onUpdateColorObject(System.Action`2<UnityEngine.Color,System.Object>)
extern void LTDescrOptional_set_onUpdateColorObject_m9556E2C730376300992A2388E38161C59B7B87DD (void);
// 0x0000020A System.Action LTDescrOptional::get_onComplete()
extern void LTDescrOptional_get_onComplete_mFF2D0179EF228E49F57BE663851C6BFDB45044B6 (void);
// 0x0000020B System.Void LTDescrOptional::set_onComplete(System.Action)
extern void LTDescrOptional_set_onComplete_m5337F3EF337F442B0CBE09C1721EAC212FE306BC (void);
// 0x0000020C System.Action`1<System.Object> LTDescrOptional::get_onCompleteObject()
extern void LTDescrOptional_get_onCompleteObject_m6CC2CEEEEE1FA541AEC4D71580593EB4A603CDF0 (void);
// 0x0000020D System.Void LTDescrOptional::set_onCompleteObject(System.Action`1<System.Object>)
extern void LTDescrOptional_set_onCompleteObject_mB19BF5653BE9F370A1AE846055F9B546C4D74D7B (void);
// 0x0000020E System.Object LTDescrOptional::get_onCompleteParam()
extern void LTDescrOptional_get_onCompleteParam_m428D156539D89C6007E0BA22E368A7B8252FD512 (void);
// 0x0000020F System.Void LTDescrOptional::set_onCompleteParam(System.Object)
extern void LTDescrOptional_set_onCompleteParam_mEDC4259A922981661AEAF55E9B845B4514BF0FBD (void);
// 0x00000210 System.Object LTDescrOptional::get_onUpdateParam()
extern void LTDescrOptional_get_onUpdateParam_mBFC0491E0705696A41C4DCB044F7ED0D70AB1BEC (void);
// 0x00000211 System.Void LTDescrOptional::set_onUpdateParam(System.Object)
extern void LTDescrOptional_set_onUpdateParam_m795BC6412556889BC4503D1E571DC2FF722643E0 (void);
// 0x00000212 System.Action LTDescrOptional::get_onStart()
extern void LTDescrOptional_get_onStart_mC70E09328F589CFB5965B061923AD4FEB6D4B2C5 (void);
// 0x00000213 System.Void LTDescrOptional::set_onStart(System.Action)
extern void LTDescrOptional_set_onStart_m529CC6B3EE612FF8C1F736BC185A090C1F04C2FA (void);
// 0x00000214 System.Void LTDescrOptional::reset()
extern void LTDescrOptional_reset_mAE8172FF1FFB9054153167751ACB4A3756853159 (void);
// 0x00000215 System.Void LTDescrOptional::callOnUpdate(System.Single,System.Single)
extern void LTDescrOptional_callOnUpdate_m10CF5AF6AE9BDA730C97538C3E164DF2961274AB (void);
// 0x00000216 System.Void LTDescrOptional::.ctor()
extern void LTDescrOptional__ctor_m50C4F69D364DD967ACF09DE82F78E20D3B15AB0C (void);
// 0x00000217 System.Int32 LTSeq::get_id()
extern void LTSeq_get_id_mF7BDB8790E2BC1B542C0F5A4260AADDF48795D3F (void);
// 0x00000218 System.Void LTSeq::reset()
extern void LTSeq_reset_m342DA923D9419644BF3A4793A0606AF0A5C133B1 (void);
// 0x00000219 System.Void LTSeq::init(System.UInt32,System.UInt32)
extern void LTSeq_init_m5315CCEC0CACF7895E5B1D5F617F589D41741A1B (void);
// 0x0000021A LTSeq LTSeq::addOn()
extern void LTSeq_addOn_mD55953C7985C4A08B02742D62A7E010367F68C2A (void);
// 0x0000021B System.Single LTSeq::addPreviousDelays()
extern void LTSeq_addPreviousDelays_mB6198AF3CABBB0D35D0AC220991530A5DD2155EB (void);
// 0x0000021C LTSeq LTSeq::append(System.Single)
extern void LTSeq_append_m0B12C0E6C61C2780F758FD94D903C716E72F68C0 (void);
// 0x0000021D LTSeq LTSeq::append(System.Action)
extern void LTSeq_append_mC0F7F9A6570CBE8BAA7112CF7217D6E164A413F4 (void);
// 0x0000021E LTSeq LTSeq::append(System.Action`1<System.Object>,System.Object)
extern void LTSeq_append_mB7C3B091C37DBD0042777E6F099687007BAF5D17 (void);
// 0x0000021F LTSeq LTSeq::append(UnityEngine.GameObject,System.Action)
extern void LTSeq_append_mDF7F824A764228060EFF8BBA69F903D1795D499F (void);
// 0x00000220 LTSeq LTSeq::append(UnityEngine.GameObject,System.Action`1<System.Object>,System.Object)
extern void LTSeq_append_mD32A66D2490D4B839B6AAA8B0B577895941E61EE (void);
// 0x00000221 LTSeq LTSeq::append(LTDescr)
extern void LTSeq_append_m76F5F63AB647BB18EF33454A98F105D75E9E9B80 (void);
// 0x00000222 LTSeq LTSeq::insert(LTDescr)
extern void LTSeq_insert_m2B024BE54107A8CAF3AE6B271FFB3CE86441DD9A (void);
// 0x00000223 LTSeq LTSeq::setScale(System.Single)
extern void LTSeq_setScale_m896BA18CF4C685EF34A26175CED8C6906043568E (void);
// 0x00000224 System.Void LTSeq::setScaleRecursive(LTSeq,System.Single,System.Int32)
extern void LTSeq_setScaleRecursive_m78C901915A35EB641BDBD9FB639199463C49639B (void);
// 0x00000225 LTSeq LTSeq::reverse()
extern void LTSeq_reverse_m777F21FCED971A010923D62CD068626ECAAD9DC5 (void);
// 0x00000226 System.Void LTSeq::.ctor()
extern void LTSeq__ctor_mB6D4797A9F32BE64240BC1D4FB9A0A8B6D824E2C (void);
// 0x00000227 System.Void LeanAudioStream::.ctor(System.Single[])
extern void LeanAudioStream__ctor_m531CEA7A157EA4A83F540265CA7160894005CFC1 (void);
// 0x00000228 System.Void LeanAudioStream::OnAudioRead(System.Single[])
extern void LeanAudioStream_OnAudioRead_m97338215EAA44081B43443314379D323FA30C308 (void);
// 0x00000229 System.Void LeanAudioStream::OnAudioSetPosition(System.Int32)
extern void LeanAudioStream_OnAudioSetPosition_m5B845BECDEBA740CB23D0725D335BBAE1B2FA4B4 (void);
// 0x0000022A LeanAudioOptions LeanAudio::options()
extern void LeanAudio_options_mB74094BFE3791F34AD4614CCB425AEE5228AAC83 (void);
// 0x0000022B LeanAudioStream LeanAudio::createAudioStream(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,LeanAudioOptions)
extern void LeanAudio_createAudioStream_m86F3E14A3FF84DC88892A8B7F16D92224BD15D8A (void);
// 0x0000022C UnityEngine.AudioClip LeanAudio::createAudio(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,LeanAudioOptions)
extern void LeanAudio_createAudio_mB693792EC007530C3DCA798A57ECCD7BFE67D426 (void);
// 0x0000022D System.Int32 LeanAudio::createAudioWave(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,LeanAudioOptions)
extern void LeanAudio_createAudioWave_m3D9DFFC3319332471839B2D76C5266629AAEED04 (void);
// 0x0000022E UnityEngine.AudioClip LeanAudio::createAudioFromWave(System.Int32,LeanAudioOptions)
extern void LeanAudio_createAudioFromWave_m8FFF705E1832195B4E0EAA977C9F4CF61FA33058 (void);
// 0x0000022F System.Void LeanAudio::OnAudioSetPosition(System.Int32)
extern void LeanAudio_OnAudioSetPosition_mFF60A18D3230037BCEB08585DDDA7B27F8B71C4F (void);
// 0x00000230 UnityEngine.AudioClip LeanAudio::generateAudioFromCurve(UnityEngine.AnimationCurve,System.Int32)
extern void LeanAudio_generateAudioFromCurve_m7E8EFFD1CC78766F82416FD21A47889DC8D31923 (void);
// 0x00000231 UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip,System.Single)
extern void LeanAudio_play_mB81F9043651130080D3F0CE28033BED5EB39E0CB (void);
// 0x00000232 UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip)
extern void LeanAudio_play_m32A1254BE2452ED5367119EFFF686D9595E4D57B (void);
// 0x00000233 UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip,UnityEngine.Vector3)
extern void LeanAudio_play_m3A30E48BBA88B9E8881968633686928B854544F9 (void);
// 0x00000234 UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern void LeanAudio_play_mCD89719473F04DF4A0CBDE0FACBBD831A8DD2ACF (void);
// 0x00000235 UnityEngine.AudioSource LeanAudio::playClipAt(UnityEngine.AudioClip,UnityEngine.Vector3)
extern void LeanAudio_playClipAt_mC5C4CD778BDCF8D897740C2B3EA8415C631BCF3F (void);
// 0x00000236 System.Void LeanAudio::printOutAudioClip(UnityEngine.AudioClip,UnityEngine.AnimationCurve&,System.Single)
extern void LeanAudio_printOutAudioClip_m2783A6C26F9A85D837099B9A4010A3523C9BDA90 (void);
// 0x00000237 System.Void LeanAudio::.ctor()
extern void LeanAudio__ctor_m88410F3D66A6EDDF53D657CF52DBBCBD40E7E8D8 (void);
// 0x00000238 System.Void LeanAudio::.cctor()
extern void LeanAudio__cctor_m5647818341FC7B5272CFCDD6966CA0407EC9D131 (void);
// 0x00000239 System.Void LeanAudioOptions::.ctor()
extern void LeanAudioOptions__ctor_mC4A118B38AA350E5502615E56400F51A43B3B60B (void);
// 0x0000023A LeanAudioOptions LeanAudioOptions::setFrequency(System.Int32)
extern void LeanAudioOptions_setFrequency_m2534EC3E99ECCB13667B5703931133E97CCB6459 (void);
// 0x0000023B LeanAudioOptions LeanAudioOptions::setVibrato(UnityEngine.Vector3[])
extern void LeanAudioOptions_setVibrato_m493C38946EC65F6B1C2E811A1C232CF8C40E8876 (void);
// 0x0000023C LeanAudioOptions LeanAudioOptions::setWaveSine()
extern void LeanAudioOptions_setWaveSine_mDADF8446B8A6BB14326F15C6FD010E43A6B5A84B (void);
// 0x0000023D LeanAudioOptions LeanAudioOptions::setWaveSquare()
extern void LeanAudioOptions_setWaveSquare_m694508FC09511CF1BF371B7FA456BE92D503322F (void);
// 0x0000023E LeanAudioOptions LeanAudioOptions::setWaveSawtooth()
extern void LeanAudioOptions_setWaveSawtooth_mC60BB12911D96EA79544EBF3780588CCB0858EF7 (void);
// 0x0000023F LeanAudioOptions LeanAudioOptions::setWaveNoise()
extern void LeanAudioOptions_setWaveNoise_m92B623A5316643EACFC1CCEF761D463EA7F53C15 (void);
// 0x00000240 LeanAudioOptions LeanAudioOptions::setWaveStyle(LeanAudioOptions/LeanAudioWaveStyle)
extern void LeanAudioOptions_setWaveStyle_mD446529C32C1D2809CE0BD19558B9B9A1CCC7381 (void);
// 0x00000241 LeanAudioOptions LeanAudioOptions::setWaveNoiseScale(System.Single)
extern void LeanAudioOptions_setWaveNoiseScale_mFE6B538697857F28C0077DC7C7F3C34CC00DD5B3 (void);
// 0x00000242 LeanAudioOptions LeanAudioOptions::setWaveNoiseInfluence(System.Single)
extern void LeanAudioOptions_setWaveNoiseInfluence_m321FC18AC3DB87F3F22658CB134088F6AE8B0A5D (void);
// 0x00000243 System.Single LeanSmooth::damp(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern void LeanSmooth_damp_mC64F76E7BEB749AE0C6620500B8F7A124C8B2281 (void);
// 0x00000244 UnityEngine.Vector3 LeanSmooth::damp(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single,System.Single,System.Single)
extern void LeanSmooth_damp_m2AE19609C34606AAC949DD5737F7C905D6D1FAEC (void);
// 0x00000245 UnityEngine.Color LeanSmooth::damp(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color&,System.Single,System.Single,System.Single)
extern void LeanSmooth_damp_m7155F328C1DE25635DFE88A26F308E83D559C084 (void);
// 0x00000246 System.Single LeanSmooth::spring(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_spring_m13E4D3125D99326FA237E391E6AFC87321D643A7 (void);
// 0x00000247 UnityEngine.Vector3 LeanSmooth::spring(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_spring_mB9D231C3690876EE5299B0261A5B101C0299E3C6 (void);
// 0x00000248 UnityEngine.Color LeanSmooth::spring(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color&,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_spring_m93FC4C89467835E4367BE781881F47A7E7416B36 (void);
// 0x00000249 System.Single LeanSmooth::linear(System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_linear_mE0319D93C18D8C6230B69BB6914BC2171D9991FC (void);
// 0x0000024A UnityEngine.Vector3 LeanSmooth::linear(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanSmooth_linear_mE1E0D6A877174839F0D805E0E23AB7B9869F99BE (void);
// 0x0000024B UnityEngine.Color LeanSmooth::linear(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanSmooth_linear_m5F142DE9C4019A6163ED38F547F8E26D6FF4C1DF (void);
// 0x0000024C System.Single LeanSmooth::bounceOut(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_bounceOut_mAF91D979E058FDC80D6E7BB35FDBDF5D1C584930 (void);
// 0x0000024D UnityEngine.Vector3 LeanSmooth::bounceOut(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_bounceOut_m213E8992275BA3F7E5DF6DCB584C2E4EAE055163 (void);
// 0x0000024E UnityEngine.Color LeanSmooth::bounceOut(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color&,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_bounceOut_mC0BF320EBCFCF7EAD5CF449073C707968FC27F1C (void);
// 0x0000024F System.Void LeanSmooth::.ctor()
extern void LeanSmooth__ctor_m14E227157D0426A2F47C06DE30CB17982B72BF66 (void);
// 0x00000250 System.Void LeanTester::Start()
extern void LeanTester_Start_m9A9E920B5F046E3372B0366E0BC9DFC84A919F58 (void);
// 0x00000251 System.Collections.IEnumerator LeanTester::timeoutCheck()
extern void LeanTester_timeoutCheck_mC313EB95692CB44EBD8BA1A87AA815BD5416B150 (void);
// 0x00000252 System.Void LeanTester::.ctor()
extern void LeanTester__ctor_mC69E2DD7669788DEA00909ED2351EAAA6CF317C3 (void);
// 0x00000253 System.Void LeanTest::debug(System.String,System.Boolean,System.String)
extern void LeanTest_debug_m6C2E9AD4476177558EF713F252CB2FCEDB14DEE3 (void);
// 0x00000254 System.Void LeanTest::expect(System.Boolean,System.String,System.String)
extern void LeanTest_expect_mD39E3D849F6BB08F68658A3EA543DABC2ECC9B77 (void);
// 0x00000255 System.String LeanTest::padRight(System.Int32)
extern void LeanTest_padRight_mD8E69C06CFE450F91AE8B44FD304BAECDCB25F91 (void);
// 0x00000256 System.Single LeanTest::printOutLength(System.String)
extern void LeanTest_printOutLength_m340E6C2B8CE237DD562D42857AA79E56724407C6 (void);
// 0x00000257 System.String LeanTest::formatBC(System.String,System.String)
extern void LeanTest_formatBC_m61FCBB26428987CBC8EAD380847104E662D747CE (void);
// 0x00000258 System.String LeanTest::formatB(System.String)
extern void LeanTest_formatB_m3A16FF8B95ABDFCFEAF5C5B3591F5D84867A7954 (void);
// 0x00000259 System.String LeanTest::formatC(System.String,System.String)
extern void LeanTest_formatC_mF76064C060FB981E2535F8A487393A849D5DD292 (void);
// 0x0000025A System.Void LeanTest::overview()
extern void LeanTest_overview_m54D7E1E1324A5D63C8834BCBDCD9627490C9FC37 (void);
// 0x0000025B System.Void LeanTest::.ctor()
extern void LeanTest__ctor_m08E8F412E4EBF00D54D3AB5D8EE7E0015C3A04EF (void);
// 0x0000025C System.Void LeanTest::.cctor()
extern void LeanTest__cctor_m589610FAD222BE5A0012463E6AF9D88514317EBF (void);
// 0x0000025D System.Void LeanTween::init()
extern void LeanTween_init_m742C83A1FC52BA7DC1A45D77418E2A20C6EA3FA4 (void);
// 0x0000025E System.Int32 LeanTween::get_maxSearch()
extern void LeanTween_get_maxSearch_mCBB6EBACEB7810A13B6BFE5E46FF93D9B576F8DB (void);
// 0x0000025F System.Int32 LeanTween::get_maxSimulataneousTweens()
extern void LeanTween_get_maxSimulataneousTweens_m29E755842BECB453B666A30118106AC8FA3ECDA9 (void);
// 0x00000260 System.Int32 LeanTween::get_tweensRunning()
extern void LeanTween_get_tweensRunning_m7DAE5C7327AF47F20CEAE9B5D7BE7460C53CDC46 (void);
// 0x00000261 System.Void LeanTween::init(System.Int32)
extern void LeanTween_init_mF9EBB839A07AFBCAEBB72C052B6F8EC4CD48D7BA (void);
// 0x00000262 System.Void LeanTween::init(System.Int32,System.Int32)
extern void LeanTween_init_m876B8AA3523AB02C0246A87E9E37F5388FFFB35B (void);
// 0x00000263 System.Void LeanTween::reset()
extern void LeanTween_reset_m239840212FEA6441ED109FC1E312A2BBE2534AF9 (void);
// 0x00000264 System.Void LeanTween::Update()
extern void LeanTween_Update_mF15DAA93C9C32D4BA1645DB4B03AC67F6C3D8F8D (void);
// 0x00000265 System.Void LeanTween::onLevelWasLoaded54(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void LeanTween_onLevelWasLoaded54_m30E5953BBD22DF6BFAD61B4B62081D26ED1E0046 (void);
// 0x00000266 System.Void LeanTween::internalOnLevelWasLoaded(System.Int32)
extern void LeanTween_internalOnLevelWasLoaded_mF01B04BD6D4CCB7D5C896B0C9381BACE2A6996F1 (void);
// 0x00000267 System.Void LeanTween::update()
extern void LeanTween_update_mE2EEEF001DE108CFA101BB549033B6022655C0E8 (void);
// 0x00000268 System.Void LeanTween::removeTween(System.Int32,System.Int32)
extern void LeanTween_removeTween_m47036DA44D4E0AF9443A2A459B10F51C039BC835 (void);
// 0x00000269 System.Void LeanTween::removeTween(System.Int32)
extern void LeanTween_removeTween_m5183FCF6228B6149D948D3175B1906EDE9EB5A2F (void);
// 0x0000026A UnityEngine.Vector3[] LeanTween::add(UnityEngine.Vector3[],UnityEngine.Vector3)
extern void LeanTween_add_m94B30FC92F9702D1C000E53F0481E2547BCEA152 (void);
// 0x0000026B System.Single LeanTween::closestRot(System.Single,System.Single)
extern void LeanTween_closestRot_m425E4814D09E1E33641F79E253706B42A8677DCA (void);
// 0x0000026C System.Void LeanTween::cancelAll()
extern void LeanTween_cancelAll_m57047A3005FF66C0137640192B7BDB3DA6CD2F27 (void);
// 0x0000026D System.Void LeanTween::cancelAll(System.Boolean)
extern void LeanTween_cancelAll_mDFE02D9737D5CFFBBBAB9B7ADEFF4EC8A2EC6BA7 (void);
// 0x0000026E System.Void LeanTween::cancel(UnityEngine.GameObject)
extern void LeanTween_cancel_m5FAAE217AED5E47A774964AD4B49CBF06BB4CFAE (void);
// 0x0000026F System.Void LeanTween::cancel(UnityEngine.GameObject,System.Boolean)
extern void LeanTween_cancel_m8C912896B48485DEF6C233BDE9515DD9BCB37F4D (void);
// 0x00000270 System.Void LeanTween::cancel(UnityEngine.RectTransform)
extern void LeanTween_cancel_m83465350000117665934A8D6556E5759C60E4BA3 (void);
// 0x00000271 System.Void LeanTween::cancel(UnityEngine.GameObject,System.Int32,System.Boolean)
extern void LeanTween_cancel_mC1789BEE750E0F49BBB9A5427837EAAED0F6B362 (void);
// 0x00000272 System.Void LeanTween::cancel(LTRect,System.Int32)
extern void LeanTween_cancel_mF06045394FD8DB00302C26CBB06F791FC20BE002 (void);
// 0x00000273 System.Void LeanTween::cancel(System.Int32)
extern void LeanTween_cancel_mBDB0EE47F9FEA5CDC99DAE5AB071A317D1646E73 (void);
// 0x00000274 System.Void LeanTween::cancel(System.Int32,System.Boolean)
extern void LeanTween_cancel_mFBB7196E53B68C2337C13F0AA1F6F96A9E652995 (void);
// 0x00000275 LTDescr LeanTween::descr(System.Int32)
extern void LeanTween_descr_m673151C22BCD383E070208DC886A48EA5682354E (void);
// 0x00000276 LTDescr LeanTween::description(System.Int32)
extern void LeanTween_description_m614B276C8B85841618BC11CC53EC3F9F998B12CD (void);
// 0x00000277 LTDescr[] LeanTween::descriptions(UnityEngine.GameObject)
extern void LeanTween_descriptions_m15F1EE30DA28ED509BED8B1208D21C6A305D0F7C (void);
// 0x00000278 System.Void LeanTween::pause(UnityEngine.GameObject,System.Int32)
extern void LeanTween_pause_m544CC2BEF9E44BF1FA9C2B02FC8C8D8D840303F5 (void);
// 0x00000279 System.Void LeanTween::pause(System.Int32)
extern void LeanTween_pause_mD334E7016A1280D8612923E19FE0EAC3A99213C7 (void);
// 0x0000027A System.Void LeanTween::pause(UnityEngine.GameObject)
extern void LeanTween_pause_m34D1984074FEAD898CB5A732E153230C05BC0F8A (void);
// 0x0000027B System.Void LeanTween::pauseAll()
extern void LeanTween_pauseAll_m9A08262908FA3B89F629A53310A615960E25681F (void);
// 0x0000027C System.Void LeanTween::resumeAll()
extern void LeanTween_resumeAll_m0CD4B24182F95AB48670CAA51B31356DA0298F02 (void);
// 0x0000027D System.Void LeanTween::resume(UnityEngine.GameObject,System.Int32)
extern void LeanTween_resume_mA7A5417B25090825B935A5A53BBCBC0DD2CA60FA (void);
// 0x0000027E System.Void LeanTween::resume(System.Int32)
extern void LeanTween_resume_m9A7648F170B3EF8AD8CF448578A1423E5AC57DC0 (void);
// 0x0000027F System.Void LeanTween::resume(UnityEngine.GameObject)
extern void LeanTween_resume_mED2ED2E734874013D07AB1CA160FA30098BB93CA (void);
// 0x00000280 System.Boolean LeanTween::isPaused(UnityEngine.GameObject)
extern void LeanTween_isPaused_mEA3B9A77082F25532A58E01A3B16F0EB6AD3C9D1 (void);
// 0x00000281 System.Boolean LeanTween::isPaused(UnityEngine.RectTransform)
extern void LeanTween_isPaused_mF0B834CE3C9278DF00216045E11ACE5EE9D66354 (void);
// 0x00000282 System.Boolean LeanTween::isPaused(System.Int32)
extern void LeanTween_isPaused_mB1A22DBCB84A1FAD2A2D5E0B7F2307A77D1C630A (void);
// 0x00000283 System.Boolean LeanTween::isTweening(UnityEngine.GameObject)
extern void LeanTween_isTweening_m8565F710EC2593893864BDC02B0F286E58D77928 (void);
// 0x00000284 System.Boolean LeanTween::isTweening(UnityEngine.RectTransform)
extern void LeanTween_isTweening_mD5E04C094ED73605C74804ADCF9B13FADDAC9470 (void);
// 0x00000285 System.Boolean LeanTween::isTweening(System.Int32)
extern void LeanTween_isTweening_m7300B17390DAF00D71AF9837EFAE524A0F067607 (void);
// 0x00000286 System.Boolean LeanTween::isTweening(LTRect)
extern void LeanTween_isTweening_mBEB3331D582A3CF1DFC989B9538934CBC8B29F39 (void);
// 0x00000287 System.Void LeanTween::drawBezierPath(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Transform)
extern void LeanTween_drawBezierPath_mFC7D2D0AF1632D71D25BF9B390285DAFB57A1FBA (void);
// 0x00000288 System.Object LeanTween::logError(System.String)
extern void LeanTween_logError_m37F7E5A9E9028A282D3FB22858AE9E751B5B7E17 (void);
// 0x00000289 LTDescr LeanTween::options(LTDescr)
extern void LeanTween_options_mA6EBBE759321A9ED63E09FCA3E19257A5732399C (void);
// 0x0000028A LTDescr LeanTween::options()
extern void LeanTween_options_m96D5FD6CD10BC38781B78A7ABBAFD40FD6896C10 (void);
// 0x0000028B UnityEngine.GameObject LeanTween::get_tweenEmpty()
extern void LeanTween_get_tweenEmpty_mAD2C44A742A9DBD681D325C9EC5708FB4B2DDAD9 (void);
// 0x0000028C LTDescr LeanTween::pushNewTween(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,LTDescr)
extern void LeanTween_pushNewTween_m5B8A1F2059CF7A5AA5A994254FB6FF2325FDB5E8 (void);
// 0x0000028D LTDescr LeanTween::play(UnityEngine.RectTransform,UnityEngine.Sprite[])
extern void LeanTween_play_mCEA864283E853E9E7086662D30B2B35229601255 (void);
// 0x0000028E LTSeq LeanTween::sequence(System.Boolean)
extern void LeanTween_sequence_mF0E50A4160E6E03ED0B1B6A57B25552E195F1E60 (void);
// 0x0000028F LTDescr LeanTween::alpha(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_alpha_m03563AAF9FE673DAEAD06EC87B702F6633009459 (void);
// 0x00000290 LTDescr LeanTween::alpha(LTRect,System.Single,System.Single)
extern void LeanTween_alpha_m8BD4F21C3DA8E6057BA72EB4500336A8AD2D23D0 (void);
// 0x00000291 LTDescr LeanTween::textAlpha(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_textAlpha_m56966F76D08E1D2F771F68FAC336CD26A3789C4B (void);
// 0x00000292 LTDescr LeanTween::alphaText(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_alphaText_mDF874DA494D7B483C1AD62D2459795583C87EA50 (void);
// 0x00000293 LTDescr LeanTween::alphaCanvas(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void LeanTween_alphaCanvas_mD7760E3B438F098F8E52C0E19BE66AB042700482 (void);
// 0x00000294 LTDescr LeanTween::alphaVertex(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_alphaVertex_m9C46F3D3D5156962E5E6DAFD6D3B91D87A86CD77 (void);
// 0x00000295 LTDescr LeanTween::color(UnityEngine.GameObject,UnityEngine.Color,System.Single)
extern void LeanTween_color_m37AE0DBE5DBE82AB6CCF14D76DF1EFC01D95B69F (void);
// 0x00000296 LTDescr LeanTween::textColor(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTween_textColor_mEF9FEF414B74FE47A423CF17CA79712A0D1FB658 (void);
// 0x00000297 LTDescr LeanTween::colorText(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTween_colorText_m15B3D867D6A42D0D7770B97CB2E7E200F0D661B2 (void);
// 0x00000298 LTDescr LeanTween::delayedCall(System.Single,System.Action)
extern void LeanTween_delayedCall_m04427AEA4FBB41C4A3CCB898D64BB4AC740BF03C (void);
// 0x00000299 LTDescr LeanTween::delayedCall(System.Single,System.Action`1<System.Object>)
extern void LeanTween_delayedCall_m899A2AE8C3C19487CFA5AB41977F67D01F9DD4E6 (void);
// 0x0000029A LTDescr LeanTween::delayedCall(UnityEngine.GameObject,System.Single,System.Action)
extern void LeanTween_delayedCall_mA05A41E42F1921AB3867C1AA9B43100FC5F57F2E (void);
// 0x0000029B LTDescr LeanTween::delayedCall(UnityEngine.GameObject,System.Single,System.Action`1<System.Object>)
extern void LeanTween_delayedCall_m7A1A97BE2037CF29C9C68D5DC41476904215AAA0 (void);
// 0x0000029C LTDescr LeanTween::destroyAfter(LTRect,System.Single)
extern void LeanTween_destroyAfter_m7641087D50C872EB2A5BEC69287673C0AA63A6C1 (void);
// 0x0000029D LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_move_m1191EC5B42C368CBBAA12E7EF8EFC8378980F2A5 (void);
// 0x0000029E LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Vector2,System.Single)
extern void LeanTween_move_m0AFE5CA5D827F49074E9ACA6C73E2E6D6E78E79E (void);
// 0x0000029F LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_move_m69E063649877CD64A4755003DB27E1608A41C08B (void);
// 0x000002A0 LTDescr LeanTween::move(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTween_move_mEDCFBFB4014A966F52A3DC08FE2E952DCDC53E95 (void);
// 0x000002A1 LTDescr LeanTween::move(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTween_move_m8058A34EDCE00BFEE3336DE5698A3927B796A796 (void);
// 0x000002A2 LTDescr LeanTween::moveSpline(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_moveSpline_mA1A07399D0522C220E7C062CCA41CA1A5F07781B (void);
// 0x000002A3 LTDescr LeanTween::moveSpline(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTween_moveSpline_mBC72A4210E5EBE1678828E39AA926A53D72C2EEC (void);
// 0x000002A4 LTDescr LeanTween::moveSplineLocal(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_moveSplineLocal_mC1BF1287479F66BEADB1E325D9DED97A6E69E67A (void);
// 0x000002A5 LTDescr LeanTween::move(LTRect,UnityEngine.Vector2,System.Single)
extern void LeanTween_move_m3ABE07EDBD7AD98100B8545E5AAA864F03A4F6A4 (void);
// 0x000002A6 LTDescr LeanTween::moveMargin(LTRect,UnityEngine.Vector2,System.Single)
extern void LeanTween_moveMargin_mCFF341DFEC796404C5DC2979392988C15AA31620 (void);
// 0x000002A7 LTDescr LeanTween::moveX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveX_mC528E208B5A163AA4C5400294A2D03C8E5448DED (void);
// 0x000002A8 LTDescr LeanTween::moveY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveY_m9E9F3F2DD9FC9791FB42E952BC71A32858E9D894 (void);
// 0x000002A9 LTDescr LeanTween::moveZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveZ_mA8D3635AAD59B698E00B6AD6981BE2B8E2A381D4 (void);
// 0x000002AA LTDescr LeanTween::moveLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_moveLocal_m4628772CED24062B5B96C041E6C3FE3B64DE8252 (void);
// 0x000002AB LTDescr LeanTween::moveLocal(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_moveLocal_m6B50D6459C312416C631F3C391C76022ED19C5D8 (void);
// 0x000002AC LTDescr LeanTween::moveLocalX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveLocalX_mA9353C4CDB6A49FC6F7B9568E70D4D3C210033A1 (void);
// 0x000002AD LTDescr LeanTween::moveLocalY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveLocalY_m605F33473A6318A1BC2719D5032D048B2206ABA9 (void);
// 0x000002AE LTDescr LeanTween::moveLocalZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveLocalZ_m0B7FEA9EDE5CDEB065F4187E11F174465D7E8EF6 (void);
// 0x000002AF LTDescr LeanTween::moveLocal(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTween_moveLocal_m52BCB9FDEEFBB74988883648CA96FBA390F991A1 (void);
// 0x000002B0 LTDescr LeanTween::moveLocal(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTween_moveLocal_mE6FF69270695A740FF6950ABB1C389BD87336445 (void);
// 0x000002B1 LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Transform,System.Single)
extern void LeanTween_move_mB930433417E02C8708DA33ABFDCB478397AA070B (void);
// 0x000002B2 LTDescr LeanTween::rotate(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_rotate_mA9190F8C72DBB9FF8A0998C936208C97D18236AE (void);
// 0x000002B3 LTDescr LeanTween::rotate(LTRect,System.Single,System.Single)
extern void LeanTween_rotate_m3D39319C8CA729B58884DED8AAB29903BAD76541 (void);
// 0x000002B4 LTDescr LeanTween::rotateLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_rotateLocal_mB981D5E76B2EA75987333079E8EEEDE7F78740A7 (void);
// 0x000002B5 LTDescr LeanTween::rotateX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_rotateX_m39C9891518C637C4DFEC9C6E455DF5562AAB1050 (void);
// 0x000002B6 LTDescr LeanTween::rotateY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_rotateY_mF66A36114A32D24BFE92D94B7944838A19847B90 (void);
// 0x000002B7 LTDescr LeanTween::rotateZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_rotateZ_mDBBBFF6BFBE5B724FCE59D3A6EF40F377431C1C8 (void);
// 0x000002B8 LTDescr LeanTween::rotateAround(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAround_m061DF6B0D1F4FB9C9A635C2B5C8D5F0D0486DCF9 (void);
// 0x000002B9 LTDescr LeanTween::rotateAroundLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAroundLocal_m8498C0D937E0D12B6EF6C43F5F1AA7F4FD721726 (void);
// 0x000002BA LTDescr LeanTween::scale(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_scale_m4CE3CCD970CF75664CF7FCA1E6286F418FDE2A0F (void);
// 0x000002BB LTDescr LeanTween::scale(LTRect,UnityEngine.Vector2,System.Single)
extern void LeanTween_scale_m209552C2E41F5D6B0B1525A25540FE40A33F4ADE (void);
// 0x000002BC LTDescr LeanTween::scaleX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_scaleX_mAA4702337267BB96770EA128A25EB894E4C7E2AB (void);
// 0x000002BD LTDescr LeanTween::scaleY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_scaleY_m664993B4D0F2296E8BE7BF8168D17A90EEEBB3D5 (void);
// 0x000002BE LTDescr LeanTween::scaleZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_scaleZ_m4F475E490D515FAF13E36488615B3C9972EAC2F7 (void);
// 0x000002BF LTDescr LeanTween::value(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern void LeanTween_value_m915028F98333F18A1326931DFAC8106B6301D23F (void);
// 0x000002C0 LTDescr LeanTween::value(System.Single,System.Single,System.Single)
extern void LeanTween_value_m589DDED951D44371EB0589C01EBB679977F47556 (void);
// 0x000002C1 LTDescr LeanTween::value(UnityEngine.GameObject,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTween_value_m4887473E05B0379914BD3795CA6697D274C54606 (void);
// 0x000002C2 LTDescr LeanTween::value(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTween_value_m3EF8D28045541C4387176018135B82C81F1FD38E (void);
// 0x000002C3 LTDescr LeanTween::value(UnityEngine.GameObject,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTween_value_m3F8D4D5F95CA382EA0D770B07EC74C270B7EC526 (void);
// 0x000002C4 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<System.Single>,System.Single,System.Single,System.Single)
extern void LeanTween_value_m115667D1977F3E7BF517B9EA3EA3EE161A8D9D39 (void);
// 0x000002C5 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`2<System.Single,System.Single>,System.Single,System.Single,System.Single)
extern void LeanTween_value_m169AE41FDE8360E78E0B637CEA5A114E112BA45A (void);
// 0x000002C6 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<UnityEngine.Color>,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTween_value_m1709E165F0E2491A98561574CE72092EF202C1F5 (void);
// 0x000002C7 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`2<UnityEngine.Color,System.Object>,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTween_value_m931E3256745BE0E7AF60C79DEE155EC421C55EC6 (void);
// 0x000002C8 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector2>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTween_value_m4FEF278D52CEFC694E29730BC229D3C52F7070C5 (void);
// 0x000002C9 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector3>,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTween_value_m0846E512D96A497507FAA08EC3747DA89CD6FB7D (void);
// 0x000002CA LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`2<System.Single,System.Object>,System.Single,System.Single,System.Single)
extern void LeanTween_value_m0E16AF4A36148D3831CBEF54C0DFD01CE08475DD (void);
// 0x000002CB LTDescr LeanTween::delayedSound(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern void LeanTween_delayedSound_mD225DC785D5C479E00DF03A9406C5EF1A145446A (void);
// 0x000002CC LTDescr LeanTween::delayedSound(UnityEngine.GameObject,UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern void LeanTween_delayedSound_m17C87E524E93E54A7AC33CDF95F5560831E0A6CA (void);
// 0x000002CD LTDescr LeanTween::move(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTween_move_m9EDD3EF2EDA02B6524829C56199400A302CC7BD3 (void);
// 0x000002CE LTDescr LeanTween::moveX(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_moveX_mAE925994CB0FAE76F0768A0595CCEEB5C440031B (void);
// 0x000002CF LTDescr LeanTween::moveY(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_moveY_m7FC0800EAC2943A5DF5F0652CD254D40EBA15BD1 (void);
// 0x000002D0 LTDescr LeanTween::moveZ(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_moveZ_m3AF920F4CDB4AE4E359A577F223341EF73CA9F04 (void);
// 0x000002D1 LTDescr LeanTween::rotate(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_rotate_m15EB5A2DF0974570B758B596AF032A42A1EB5F6A (void);
// 0x000002D2 LTDescr LeanTween::rotate(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTween_rotate_m362D5FBBA0847D5E7A4C8D0037355D7192F10ABB (void);
// 0x000002D3 LTDescr LeanTween::rotateAround(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAround_m6149AA8E86FFC604EB792197923D69BA3BF46823 (void);
// 0x000002D4 LTDescr LeanTween::rotateAroundLocal(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAroundLocal_m20B862CE01E52B5C4533D2512D718EDA1437B57A (void);
// 0x000002D5 LTDescr LeanTween::scale(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTween_scale_m47628FC09C70C5B77089F8AA97B7682F849EC0A9 (void);
// 0x000002D6 LTDescr LeanTween::size(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void LeanTween_size_mEC9D2E7E59F8EBFC0CA941CADD467CD1A5491B43 (void);
// 0x000002D7 LTDescr LeanTween::alpha(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_alpha_m640B7551150854D49EC0BA41B7CA866B7A6CBF6D (void);
// 0x000002D8 LTDescr LeanTween::color(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTween_color_mD14B3C4D1453119045FC7D74FB6746287E9BA037 (void);
// 0x000002D9 System.Single LeanTween::tweenOnCurve(LTDescr,System.Single)
extern void LeanTween_tweenOnCurve_mF41225AC36913842DA74788977F04689BB139920 (void);
// 0x000002DA UnityEngine.Vector3 LeanTween::tweenOnCurveVector(LTDescr,System.Single)
extern void LeanTween_tweenOnCurveVector_m517A4B35EE61BAE6ADB0DF2D84863D280540DA59 (void);
// 0x000002DB System.Single LeanTween::easeOutQuadOpt(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuadOpt_m3C04BFEED580755A923F99E89B4F023F62450868 (void);
// 0x000002DC System.Single LeanTween::easeInQuadOpt(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuadOpt_mCBC13EA35ADC87B69182C6DDAB43904CE2649BC6 (void);
// 0x000002DD System.Single LeanTween::easeInOutQuadOpt(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuadOpt_m430D10EAD4D0A1FEC9AAD1E33F67B477EE72FD31 (void);
// 0x000002DE UnityEngine.Vector3 LeanTween::easeInOutQuadOpt(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTween_easeInOutQuadOpt_m6496C1F3A68C113F8798961D6F0E357BAA178A34 (void);
// 0x000002DF System.Single LeanTween::linear(System.Single,System.Single,System.Single)
extern void LeanTween_linear_mC1ED9F0ADBDA8939C75B3BC0914E50DC34CB4F6C (void);
// 0x000002E0 System.Single LeanTween::clerp(System.Single,System.Single,System.Single)
extern void LeanTween_clerp_mE62F3DDE55042FAFC1484ECAFC2243E82B86AE62 (void);
// 0x000002E1 System.Single LeanTween::spring(System.Single,System.Single,System.Single)
extern void LeanTween_spring_mF690BE574938A3A8F8EC8248642F31ACD7785F32 (void);
// 0x000002E2 System.Single LeanTween::easeInQuad(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuad_m4D2FD34AC16BDD31B2838E3A5D80916469833DF7 (void);
// 0x000002E3 System.Single LeanTween::easeOutQuad(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuad_mB929599F71CEDB7C7C8579B9C8BA97500166584F (void);
// 0x000002E4 System.Single LeanTween::easeInOutQuad(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuad_m584E55A5AF9BF428B143857479D7D790CA38745C (void);
// 0x000002E5 System.Single LeanTween::easeInOutQuadOpt2(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuadOpt2_m6F3DE20B7BFF5E0F5B34C1246C40F9AF115F7799 (void);
// 0x000002E6 System.Single LeanTween::easeInCubic(System.Single,System.Single,System.Single)
extern void LeanTween_easeInCubic_mE4683158D3ECF4DB9FBBDF6187759886DB83F853 (void);
// 0x000002E7 System.Single LeanTween::easeOutCubic(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutCubic_m42408FA585BA84351F5CD74AD1A3ECED10FD8B91 (void);
// 0x000002E8 System.Single LeanTween::easeInOutCubic(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutCubic_mFB2A765901D685FA00D1C16E6133E53047B7FE71 (void);
// 0x000002E9 System.Single LeanTween::easeInQuart(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuart_m1C37EFBC471397640865AF91771EA21DA0ED492F (void);
// 0x000002EA System.Single LeanTween::easeOutQuart(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuart_m1A24432DC39B7ADD6D87960129D2FDE0C6D03C12 (void);
// 0x000002EB System.Single LeanTween::easeInOutQuart(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuart_mBB2A5F122AC70BF79993F1180AC9A0D77D9C139F (void);
// 0x000002EC System.Single LeanTween::easeInQuint(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuint_m1104B28EFA33850E5CB9BDC867D4C67B4D90457B (void);
// 0x000002ED System.Single LeanTween::easeOutQuint(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuint_m9900891B73DD3AC15F28155E1EF340B260C8C67F (void);
// 0x000002EE System.Single LeanTween::easeInOutQuint(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuint_mC67EDFA9A9953B9AC7C073D7211D115B37BC7B41 (void);
// 0x000002EF System.Single LeanTween::easeInSine(System.Single,System.Single,System.Single)
extern void LeanTween_easeInSine_m81F6144AC64B5E5A4ED990C48A36F5A3D275525C (void);
// 0x000002F0 System.Single LeanTween::easeOutSine(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutSine_mD6871A62B4F306845FD33F26B9B82E5CC43DDECC (void);
// 0x000002F1 System.Single LeanTween::easeInOutSine(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutSine_m74089B5D05EA9EA3FB89A14DC48F1D310939320F (void);
// 0x000002F2 System.Single LeanTween::easeInExpo(System.Single,System.Single,System.Single)
extern void LeanTween_easeInExpo_m3B47C6876FA3DDCDBA8E27516A9D5A92AB8A0F96 (void);
// 0x000002F3 System.Single LeanTween::easeOutExpo(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutExpo_m90B7C8EFAC973AFD5B0FBC48AC5277F3ED95BA4D (void);
// 0x000002F4 System.Single LeanTween::easeInOutExpo(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutExpo_m1818C9E2CA4DB499749B6E701C523FE50C57CEB7 (void);
// 0x000002F5 System.Single LeanTween::easeInCirc(System.Single,System.Single,System.Single)
extern void LeanTween_easeInCirc_m32983AB18E1A8F9632D5D3350FE72AAC235F9DF9 (void);
// 0x000002F6 System.Single LeanTween::easeOutCirc(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutCirc_m9C4FF7DD334929D7993925BBCBB8FD8D2D198134 (void);
// 0x000002F7 System.Single LeanTween::easeInOutCirc(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutCirc_m4AF54D1824BB627E59856E1EB3FEEE9801A24926 (void);
// 0x000002F8 System.Single LeanTween::easeInBounce(System.Single,System.Single,System.Single)
extern void LeanTween_easeInBounce_m4D853643882AA66C8BA82904BF06C8FD89172577 (void);
// 0x000002F9 System.Single LeanTween::easeOutBounce(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutBounce_m1790E9DEA28BAE58ED7FD4606E999056A1074136 (void);
// 0x000002FA System.Single LeanTween::easeInOutBounce(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutBounce_mDDC1155A0C82DE93397F30C80DF6059685229413 (void);
// 0x000002FB System.Single LeanTween::easeInBack(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInBack_mF3612B1794D308B659FBF9FF3DFB1BE11C4C5C3E (void);
// 0x000002FC System.Single LeanTween::easeOutBack(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeOutBack_m7B845AD4DA44D5C0B79DFFB66BFE8D1EA78BB57D (void);
// 0x000002FD System.Single LeanTween::easeInOutBack(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutBack_m1A651107C2734CD9259AE6F3422D1544D9C445E2 (void);
// 0x000002FE System.Single LeanTween::easeInElastic(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInElastic_mC0C2B9BD7D1ED6B6624F52DC5D8F6D2E1C1C575D (void);
// 0x000002FF System.Single LeanTween::easeOutElastic(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeOutElastic_mDD7A06B6C97A24B25C34A4191312D6F8E64DED7A (void);
// 0x00000300 System.Single LeanTween::easeInOutElastic(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutElastic_mC2E44F37BF3623A004AD1B60EE3AF7968DB21D2B (void);
// 0x00000301 LTDescr LeanTween::followDamp(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single,System.Single)
extern void LeanTween_followDamp_mA9C52B1FBD62D1491E62714A18E092E939948A12 (void);
// 0x00000302 LTDescr LeanTween::followSpring(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_followSpring_m6A4518BC770FBA7041F96B2D93910A874D8DE95D (void);
// 0x00000303 LTDescr LeanTween::followBounceOut(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_followBounceOut_m30E47CECD44D0928C51E4868BD229F8C3DE5939C (void);
// 0x00000304 LTDescr LeanTween::followLinear(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single)
extern void LeanTween_followLinear_mEF3E3F81DB95C8FC7512D9EB79B1A29818537333 (void);
// 0x00000305 System.Void LeanTween::addListener(System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_addListener_mEC31B40DB9ED344E2F2BAE152DA564537AB16754 (void);
// 0x00000306 System.Void LeanTween::addListener(UnityEngine.GameObject,System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_addListener_m34C97CB48F8B906351A79F49A6CECC6334BBD3C8 (void);
// 0x00000307 System.Boolean LeanTween::removeListener(System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_removeListener_m982E421FADD5A0991EBDFBD169F5BE77740ECE3A (void);
// 0x00000308 System.Boolean LeanTween::removeListener(System.Int32)
extern void LeanTween_removeListener_m39242CA2654CC5FC27C4D08B45F0F71888164F41 (void);
// 0x00000309 System.Boolean LeanTween::removeListener(UnityEngine.GameObject,System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_removeListener_mE054AB523D46CA58B0C59101C79AB1C9E680072A (void);
// 0x0000030A System.Void LeanTween::dispatchEvent(System.Int32)
extern void LeanTween_dispatchEvent_m2996C15CD7A7C91A70A896DACBD85AD50596EE74 (void);
// 0x0000030B System.Void LeanTween::dispatchEvent(System.Int32,System.Object)
extern void LeanTween_dispatchEvent_m746D9A2E728E4018D7156A8C05A2B2113151E475 (void);
// 0x0000030C System.Void LeanTween::.ctor()
extern void LeanTween__ctor_m29640AFA36A452E41D7D7FCE837930805FE524D4 (void);
// 0x0000030D System.Void LeanTween::.cctor()
extern void LeanTween__cctor_m4A2CF2E2F8D1A20278F8B86DCF870EBE903F9AD5 (void);
// 0x0000030E UnityEngine.Vector3[] LTUtility::reverse(UnityEngine.Vector3[])
extern void LTUtility_reverse_mFA541E145D5DB2CAD1B20BA78057C7A3495556C0 (void);
// 0x0000030F System.Void LTUtility::.ctor()
extern void LTUtility__ctor_m04573B783EECEFDD4E2AF1FFD4D7B62E914D0F4D (void);
// 0x00000310 System.Void LTBezier::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LTBezier__ctor_m97D899A3D06174612D033E42F804FB14684814D9 (void);
// 0x00000311 System.Single LTBezier::map(System.Single)
extern void LTBezier_map_m3DD6B1382C93E0204A339F62ACCFAB5C3A29DD2A (void);
// 0x00000312 UnityEngine.Vector3 LTBezier::bezierPoint(System.Single)
extern void LTBezier_bezierPoint_mEB0E0EF802E14A7FD9AB49F23936260D8B0AFC45 (void);
// 0x00000313 UnityEngine.Vector3 LTBezier::point(System.Single)
extern void LTBezier_point_m017F4A7A1923793B57C7A8A76682609CCFD0E8D4 (void);
// 0x00000314 System.Void LTBezierPath::.ctor()
extern void LTBezierPath__ctor_m3180C1A051F04229804D0BB0F4ECCAE916AA6845 (void);
// 0x00000315 System.Void LTBezierPath::.ctor(UnityEngine.Vector3[])
extern void LTBezierPath__ctor_m431529CBFCA2BEBB9158B0E80E9AFDBB2F9EF7C4 (void);
// 0x00000316 System.Void LTBezierPath::setPoints(UnityEngine.Vector3[])
extern void LTBezierPath_setPoints_m6717D57148D9DD5B0B3B9F2C67F5060423CC62BF (void);
// 0x00000317 System.Single LTBezierPath::get_distance()
extern void LTBezierPath_get_distance_mE03E0C6CAC8CB0D3DC5CD4F21CCE6AF6A2B5615E (void);
// 0x00000318 UnityEngine.Vector3 LTBezierPath::point(System.Single)
extern void LTBezierPath_point_m7DD32C57F1078C21BFF31EF88109EF188FBBEA89 (void);
// 0x00000319 System.Void LTBezierPath::place2d(UnityEngine.Transform,System.Single)
extern void LTBezierPath_place2d_mA08CEFF1862BCD64FBF0D6DD7821822BED4F56A7 (void);
// 0x0000031A System.Void LTBezierPath::placeLocal2d(UnityEngine.Transform,System.Single)
extern void LTBezierPath_placeLocal2d_mBDE2E3BB035F85D3A500AC0B0E36E5038CF3B27A (void);
// 0x0000031B System.Void LTBezierPath::place(UnityEngine.Transform,System.Single)
extern void LTBezierPath_place_m080C91568428A12F6CAA5EA5787D0A1EE78569E3 (void);
// 0x0000031C System.Void LTBezierPath::place(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTBezierPath_place_mA0099B55C6D850148EF3F139A3AAA21115FF6395 (void);
// 0x0000031D System.Void LTBezierPath::placeLocal(UnityEngine.Transform,System.Single)
extern void LTBezierPath_placeLocal_m0D2D81B70292CE5B7D595E8E1FECB865E2617F88 (void);
// 0x0000031E System.Void LTBezierPath::placeLocal(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTBezierPath_placeLocal_mE7093E9870F33F6B93BB03F804B14011A7C0542A (void);
// 0x0000031F System.Void LTBezierPath::gizmoDraw(System.Single)
extern void LTBezierPath_gizmoDraw_m32B996E9870AA9A63DD800B3E406C54F697B23ED (void);
// 0x00000320 System.Single LTBezierPath::ratioAtPoint(UnityEngine.Vector3,System.Single)
extern void LTBezierPath_ratioAtPoint_mDBAB488E78E8993C759918B979CCCCD9DFDAED19 (void);
// 0x00000321 System.Void LTSpline::.ctor(UnityEngine.Vector3[])
extern void LTSpline__ctor_m89DAF8F9A02B744F14DC951BF26998ABE636079E (void);
// 0x00000322 System.Void LTSpline::.ctor(UnityEngine.Vector3[],System.Boolean)
extern void LTSpline__ctor_mB9F6B9D5E8250215A98C318B775D0862716E2F62 (void);
// 0x00000323 System.Void LTSpline::init(UnityEngine.Vector3[],System.Boolean)
extern void LTSpline_init_m5F8F0ED8EE3B00DFB2020C9B8E7F0589D764296E (void);
// 0x00000324 UnityEngine.Vector3 LTSpline::map(System.Single)
extern void LTSpline_map_m8586FE88C55DFE6D7CD897651AEBB8E161D4AAD3 (void);
// 0x00000325 UnityEngine.Vector3 LTSpline::interp(System.Single)
extern void LTSpline_interp_m07E5A5D423EC7092EF3CCF7FD65FFD65DEF7704D (void);
// 0x00000326 System.Single LTSpline::ratioAtPoint(UnityEngine.Vector3)
extern void LTSpline_ratioAtPoint_m6BE44439D18029959983FA5D960C319BAA0F5EC9 (void);
// 0x00000327 UnityEngine.Vector3 LTSpline::point(System.Single)
extern void LTSpline_point_mDCF2C08D8F3F338D5749D4478D652DDE9B2949F9 (void);
// 0x00000328 System.Void LTSpline::place2d(UnityEngine.Transform,System.Single)
extern void LTSpline_place2d_m7715D476897D521AC679B2007254773606199FAF (void);
// 0x00000329 System.Void LTSpline::placeLocal2d(UnityEngine.Transform,System.Single)
extern void LTSpline_placeLocal2d_m1264CC1B0A061E5B1D399B46F1E6BEE91BE49926 (void);
// 0x0000032A System.Void LTSpline::place(UnityEngine.Transform,System.Single)
extern void LTSpline_place_mDBCF580928976DB68CFA586B31B88C23C6D9FD52 (void);
// 0x0000032B System.Void LTSpline::place(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTSpline_place_m4F75114ECFDE9711D5BC6AA012180139DB21136F (void);
// 0x0000032C System.Void LTSpline::placeLocal(UnityEngine.Transform,System.Single)
extern void LTSpline_placeLocal_m47E0DF17D96F80E0EF9E7B2809828BECF1063660 (void);
// 0x0000032D System.Void LTSpline::placeLocal(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTSpline_placeLocal_mA60E9587B94103C8BD655ADD566F2218D4D2B1B5 (void);
// 0x0000032E System.Void LTSpline::gizmoDraw(System.Single)
extern void LTSpline_gizmoDraw_mA538B509F29ED2A1583D44FDC86935B98364FA88 (void);
// 0x0000032F System.Void LTSpline::drawGizmo(UnityEngine.Color)
extern void LTSpline_drawGizmo_mD374863782B673CE885A2855910958A2DF60A95E (void);
// 0x00000330 System.Void LTSpline::drawGizmo(UnityEngine.Transform[],UnityEngine.Color)
extern void LTSpline_drawGizmo_m3C4516917996D8F6D1B40A5017937C2B2BADB1F1 (void);
// 0x00000331 System.Void LTSpline::drawLine(UnityEngine.Transform[],System.Single,UnityEngine.Color)
extern void LTSpline_drawLine_mB44BF9E463BE732E1FC8A178E92FE3230FA79588 (void);
// 0x00000332 System.Void LTSpline::drawLinesGLLines(UnityEngine.Material,UnityEngine.Color,System.Single)
extern void LTSpline_drawLinesGLLines_m7E7A763E4DEF72CFF387EAE1B80A1A6D26905590 (void);
// 0x00000333 UnityEngine.Vector3[] LTSpline::generateVectors()
extern void LTSpline_generateVectors_m85FEDD714CEEAD04670F854D11CED89775F05AB3 (void);
// 0x00000334 System.Void LTSpline::.cctor()
extern void LTSpline__cctor_m6739314E6052E4C78709F02047E8DC2DC9096AAF (void);
// 0x00000335 System.Void LTRect::.ctor()
extern void LTRect__ctor_mD2DD65C62E8EDAC7B218018C87391F7E2F36FEF9 (void);
// 0x00000336 System.Void LTRect::.ctor(UnityEngine.Rect)
extern void LTRect__ctor_m47F4B741D47E9809C1B03A566F06863E246A2696 (void);
// 0x00000337 System.Void LTRect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void LTRect__ctor_mBE81F7E8AAD5FB8A7E8AFCEAECCD66DA9A30F345 (void);
// 0x00000338 System.Void LTRect::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LTRect__ctor_m108E96CEDBA3D65FEFBE787F4B17F844CF3638AE (void);
// 0x00000339 System.Void LTRect::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LTRect__ctor_m6A1374116E6DC9E18821247FAEDDD60F74FA3758 (void);
// 0x0000033A System.Boolean LTRect::get_hasInitiliazed()
extern void LTRect_get_hasInitiliazed_m2D51CC6C1FCF712275E919DFA2E44F1E860DF216 (void);
// 0x0000033B System.Int32 LTRect::get_id()
extern void LTRect_get_id_m6F74EEF30562B278C561253B7591297CD389C30A (void);
// 0x0000033C System.Void LTRect::setId(System.Int32,System.Int32)
extern void LTRect_setId_mF1BD1C3E2DB5CA883A47FB9E8F644EB16A5B294F (void);
// 0x0000033D System.Void LTRect::reset()
extern void LTRect_reset_m53A78C5EA3E9E78CC06DB2F25D1C71A588B06F2D (void);
// 0x0000033E System.Void LTRect::resetForRotation()
extern void LTRect_resetForRotation_mA8B3D468DCD205AE911118E6EED6C8A479A674A0 (void);
// 0x0000033F System.Single LTRect::get_x()
extern void LTRect_get_x_mD70B7C3B314D4FCDF6AF3BE29F6397C47426BFC6 (void);
// 0x00000340 System.Void LTRect::set_x(System.Single)
extern void LTRect_set_x_m640BF975EE3C8DFC498BEAFC41742A094684ACAB (void);
// 0x00000341 System.Single LTRect::get_y()
extern void LTRect_get_y_m168437019BB9D70389C282B93AF269634C80EB5A (void);
// 0x00000342 System.Void LTRect::set_y(System.Single)
extern void LTRect_set_y_mAD963E12C76DC233063AFACED81EAF3D03F06D27 (void);
// 0x00000343 System.Single LTRect::get_width()
extern void LTRect_get_width_m622A9922E21390846F78DA4E37A7EE0675D9F5CE (void);
// 0x00000344 System.Void LTRect::set_width(System.Single)
extern void LTRect_set_width_m03AEDBAC9CA96B53E086CDD5DAB57A394B1417B5 (void);
// 0x00000345 System.Single LTRect::get_height()
extern void LTRect_get_height_m1C9CB0CBAEC6888479CDF92240484145730F0E30 (void);
// 0x00000346 System.Void LTRect::set_height(System.Single)
extern void LTRect_set_height_m34DC6F5C00E4A2C16382539CE21524603F6EAB89 (void);
// 0x00000347 UnityEngine.Rect LTRect::get_rect()
extern void LTRect_get_rect_m6C78FB7176F49F9F8FBB80160FDF37C57EEEEA79 (void);
// 0x00000348 System.Void LTRect::set_rect(UnityEngine.Rect)
extern void LTRect_set_rect_m7FBDEB9CFFC6BE529A76996FCA83F69517F0280D (void);
// 0x00000349 LTRect LTRect::setStyle(UnityEngine.GUIStyle)
extern void LTRect_setStyle_m73FC3A26735BB8712D8B2D92357035D9FB8FA1C5 (void);
// 0x0000034A LTRect LTRect::setFontScaleToFit(System.Boolean)
extern void LTRect_setFontScaleToFit_m39043C9072BB81E3DFF6C7697404EFB2C3090984 (void);
// 0x0000034B LTRect LTRect::setColor(UnityEngine.Color)
extern void LTRect_setColor_m260F2239C224794A8C49B970E5F5DBF5CA594779 (void);
// 0x0000034C LTRect LTRect::setAlpha(System.Single)
extern void LTRect_setAlpha_m2B25F81A596DFA252D4C2D0090FAF8ED224AD2CD (void);
// 0x0000034D LTRect LTRect::setLabel(System.String)
extern void LTRect_setLabel_m1FFCB5B75A7AA46B894883AD569AF4ECE8662D5F (void);
// 0x0000034E LTRect LTRect::setUseSimpleScale(System.Boolean,UnityEngine.Rect)
extern void LTRect_setUseSimpleScale_mFD395CF7FAF8EF1E65A4697817CA41A1E4DF22D5 (void);
// 0x0000034F LTRect LTRect::setUseSimpleScale(System.Boolean)
extern void LTRect_setUseSimpleScale_m319F90C955999F948E57D8FEFFB3706E9446EDE4 (void);
// 0x00000350 LTRect LTRect::setSizeByHeight(System.Boolean)
extern void LTRect_setSizeByHeight_m17196A565268B33273FE50DE6A9D89D71B2C7AB3 (void);
// 0x00000351 System.String LTRect::ToString()
extern void LTRect_ToString_m30F891F0B46B40656CD5BC9265D6D2F0AAADEA33 (void);
// 0x00000352 System.Void LTEvent::.ctor(System.Int32,System.Object)
extern void LTEvent__ctor_mF1824A2419396C340A300E02A11BD3C710931494 (void);
// 0x00000353 System.Void LTGUI::init()
extern void LTGUI_init_m56E76FF99D2B3BA59DC04B0CD796B8672BAD16CB (void);
// 0x00000354 System.Void LTGUI::initRectCheck()
extern void LTGUI_initRectCheck_mD1D857FC17862BBF0171D8B30C2629B9FD84D4D8 (void);
// 0x00000355 System.Void LTGUI::reset()
extern void LTGUI_reset_m7CE773801CF53E0AFCA45D4C470DA38930F518F0 (void);
// 0x00000356 System.Void LTGUI::update(System.Int32)
extern void LTGUI_update_m00A3E17E130357C2B8F88B29572E767536C3C097 (void);
// 0x00000357 System.Boolean LTGUI::checkOnScreen(UnityEngine.Rect)
extern void LTGUI_checkOnScreen_m0B39FD2B120D5FB49801098E46F8705837445E6A (void);
// 0x00000358 System.Void LTGUI::destroy(System.Int32)
extern void LTGUI_destroy_m59B5A127738FD610340C642E74C5FC5ED8CDF2C0 (void);
// 0x00000359 System.Void LTGUI::destroyAll(System.Int32)
extern void LTGUI_destroyAll_mEE7BC1026BAC0D8132D6AD8CAAE8CD4B5249807F (void);
// 0x0000035A LTRect LTGUI::label(UnityEngine.Rect,System.String,System.Int32)
extern void LTGUI_label_mA4BFF45B5BC97C4227CC0E47BC2183470B42C5F4 (void);
// 0x0000035B LTRect LTGUI::label(LTRect,System.String,System.Int32)
extern void LTGUI_label_m9016D43E1833A398F9DE235F106DB4B83E4A3EF9 (void);
// 0x0000035C LTRect LTGUI::texture(UnityEngine.Rect,UnityEngine.Texture,System.Int32)
extern void LTGUI_texture_m4EAA8E004AB27F16AF3F70E001F32086C34DF6A6 (void);
// 0x0000035D LTRect LTGUI::texture(LTRect,UnityEngine.Texture,System.Int32)
extern void LTGUI_texture_m6D73FEC3838D61DB4B92333120AF15139EEB2615 (void);
// 0x0000035E LTRect LTGUI::element(LTRect,System.Int32)
extern void LTGUI_element_m5933C737FE6DB8B7F4AC87B9EBEBF338BE4DD456 (void);
// 0x0000035F System.Boolean LTGUI::hasNoOverlap(UnityEngine.Rect,System.Int32)
extern void LTGUI_hasNoOverlap_m1174D1999AEEC77DAB75CC5456C4A28268D72663 (void);
// 0x00000360 System.Boolean LTGUI::pressedWithinRect(UnityEngine.Rect)
extern void LTGUI_pressedWithinRect_m585FA74045ACAFFF9EDAEC8469B6C61F4191C6BF (void);
// 0x00000361 System.Boolean LTGUI::checkWithinRect(UnityEngine.Vector2,UnityEngine.Rect)
extern void LTGUI_checkWithinRect_mE13CE64B1EA92D1287B6B090A75E99832C45E4E8 (void);
// 0x00000362 UnityEngine.Vector2 LTGUI::firstTouch()
extern void LTGUI_firstTouch_mF464F4A8A19F65CBC9EA2BF2111236BEBB9E618C (void);
// 0x00000363 System.Void LTGUI::.ctor()
extern void LTGUI__ctor_m914864F7BFF6D41D79693EED54116EA572A41429 (void);
// 0x00000364 System.Void LTGUI::.cctor()
extern void LTGUI__cctor_m01C5B6F47B1C4B45CA1D6B77B5720BDBAFB049AA (void);
// 0x00000365 LTDescr LeanTweenExt::LeanAlpha(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanAlpha_m461C89B7ADA172C00449BE049ACF50F3CE55D766 (void);
// 0x00000366 LTDescr LeanTweenExt::LeanAlphaVertex(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanAlphaVertex_m78E0D1E1E6D229BB56EAC9F208119463298D79D7 (void);
// 0x00000367 LTDescr LeanTweenExt::LeanAlpha(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanAlpha_m62EE99D970FEDB18E72E1CECC8BF3C3C806B8897 (void);
// 0x00000368 LTDescr LeanTweenExt::LeanAlpha(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void LeanTweenExt_LeanAlpha_mDC6296AC255B03B668D291C60FCC3F49788DF8EB (void);
// 0x00000369 LTDescr LeanTweenExt::LeanAlphaText(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanAlphaText_m8289E2F78CE20F51FD72D83399F535A6ECE4DCD0 (void);
// 0x0000036A System.Void LeanTweenExt::LeanCancel(UnityEngine.GameObject)
extern void LeanTweenExt_LeanCancel_m258065657CE313E3C76F55578F793097140D9F19 (void);
// 0x0000036B System.Void LeanTweenExt::LeanCancel(UnityEngine.GameObject,System.Boolean)
extern void LeanTweenExt_LeanCancel_m3EAAA4F94BA9D26442BF5503CF1C13F56B136E41 (void);
// 0x0000036C System.Void LeanTweenExt::LeanCancel(UnityEngine.GameObject,System.Int32,System.Boolean)
extern void LeanTweenExt_LeanCancel_m0EBACDA55B64F6124F9863947D3EDFF41FE5BC91 (void);
// 0x0000036D System.Void LeanTweenExt::LeanCancel(UnityEngine.RectTransform)
extern void LeanTweenExt_LeanCancel_m16FFCC4C3A923A8261181F5D40AF48369B766112 (void);
// 0x0000036E LTDescr LeanTweenExt::LeanColor(UnityEngine.GameObject,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanColor_mA1A81C489F98EB1520DE95136AB29B46E5EB5D1F (void);
// 0x0000036F LTDescr LeanTweenExt::LeanColorText(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanColorText_m23602453BAB11CF15AA4ED5E4F1B3962CF6836C5 (void);
// 0x00000370 LTDescr LeanTweenExt::LeanDelayedCall(UnityEngine.GameObject,System.Single,System.Action)
extern void LeanTweenExt_LeanDelayedCall_m67EE573EEA0027172A59DC8E6810F2643050D72F (void);
// 0x00000371 LTDescr LeanTweenExt::LeanDelayedCall(UnityEngine.GameObject,System.Single,System.Action`1<System.Object>)
extern void LeanTweenExt_LeanDelayedCall_mB1FF6EE6E33AEAFE689676F8674FDF2F8BC8D395 (void);
// 0x00000372 System.Boolean LeanTweenExt::LeanIsPaused(UnityEngine.GameObject)
extern void LeanTweenExt_LeanIsPaused_mE920863734EE45312ECA09EA8B998C524E26BFE1 (void);
// 0x00000373 System.Boolean LeanTweenExt::LeanIsPaused(UnityEngine.RectTransform)
extern void LeanTweenExt_LeanIsPaused_m40DD7C17AFF3F2B488C94B529A62F0EC3773B532 (void);
// 0x00000374 System.Boolean LeanTweenExt::LeanIsTweening(UnityEngine.GameObject)
extern void LeanTweenExt_LeanIsTweening_m11D59C75569AD0E04DA5CAA351D2F68F38C8E1D6 (void);
// 0x00000375 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMove_mBCF028720C25916836835CBFDDEFBBAE0F747730 (void);
// 0x00000376 LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMove_m090CF72208FA48F140219041C8062A46EF3F3ADE (void);
// 0x00000377 LTDescr LeanTweenExt::LeanMove(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMove_m3D682F2EABA9624478F95DC984962451F4C72335 (void);
// 0x00000378 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanMove_mDE73F1D58AAC128EBF331B493A9B836851AC1589 (void);
// 0x00000379 LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanMove_m38E623C91DB8CBB6FDAB138979719D2D1837EA1E (void);
// 0x0000037A LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMove_mCB11792AADB6F2D82EF43C90709B1CBD79FDDB83 (void);
// 0x0000037B LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMove_m3CC6348A9E1270D6703F193F8CC9B1C2CE12384B (void);
// 0x0000037C LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTweenExt_LeanMove_mDD7DB276339BE449389D687CCF4793E275393331 (void);
// 0x0000037D LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMove_m1D02677FF9D2509C2FA77A3BE7C3C69718AA191B (void);
// 0x0000037E LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMove_m1FC68173632416D0BBB72EFC74D82348B0523D80 (void);
// 0x0000037F LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,LTSpline,System.Single)
extern void LeanTweenExt_LeanMove_m87B72A7F9F763586108B9F5B648D48428B1F8FFD (void);
// 0x00000380 LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMoveLocal_mBD123664B63D8DE32BF36F0ED100E9B87996267E (void);
// 0x00000381 LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMoveLocal_m3149C4DF54C63DB2384A197A0AC8831EC0845703 (void);
// 0x00000382 LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveLocal_m97955CB5D282428BC2D2CBE129F6A401B482A8F8 (void);
// 0x00000383 LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMoveLocal_m616AE133C599BAF61CE3159CF579C3DB0EFE7B66 (void);
// 0x00000384 LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.Transform,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMoveLocal_mFDF99A832F9B857E1B16A2E744F9F383C1F90375 (void);
// 0x00000385 LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.Transform,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveLocal_mA80CD3FC03494A1744FB551747A732908D9B2D05 (void);
// 0x00000386 LTDescr LeanTweenExt::LeanMoveLocalX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalX_m5C894E1B3E98732358670D3837F1F8D3C4867AB6 (void);
// 0x00000387 LTDescr LeanTweenExt::LeanMoveLocalY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalY_mC4D2C4DFD2A41D0988F10D78AB782834A2D7AEA3 (void);
// 0x00000388 LTDescr LeanTweenExt::LeanMoveLocalZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalZ_m2523332A42D3C35D0EB0983B50290B260BE971D0 (void);
// 0x00000389 LTDescr LeanTweenExt::LeanMoveLocalX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalX_m8AF36480056D23742446C3D011AB3462FF6E4DE7 (void);
// 0x0000038A LTDescr LeanTweenExt::LeanMoveLocalY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalY_m402602941FDB4A2F4E4F52723678149A63D28742 (void);
// 0x0000038B LTDescr LeanTweenExt::LeanMoveLocalZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalZ_m277E9019194B31C9D2F5FACD5CA5BDB78D14031F (void);
// 0x0000038C LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSpline_m1146A15795C039B9B2B03ED4C80AD8D232A3C4F9 (void);
// 0x0000038D LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveSpline_m0C9AE2BAD998E3BC3CD0C3F1671F47B2F769D955 (void);
// 0x0000038E LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.Transform,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSpline_m65F76F58B8003E56FCB6C2EDF7044D467CE61D4C (void);
// 0x0000038F LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.Transform,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveSpline_m54CFB8DA58EB1DC4557E6456579F9D3DA7F28C8A (void);
// 0x00000390 LTDescr LeanTweenExt::LeanMoveSplineLocal(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSplineLocal_mA26C4FC628C8DBE7393FCEFAEABDDE769F3BA36A (void);
// 0x00000391 LTDescr LeanTweenExt::LeanMoveSplineLocal(UnityEngine.Transform,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSplineLocal_m1E899C48EA516A34C9929D7FDF44EF2632AA0D1E (void);
// 0x00000392 LTDescr LeanTweenExt::LeanMoveX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveX_m82773527741CA6AE6E241C12A1D8634788BEEB4A (void);
// 0x00000393 LTDescr LeanTweenExt::LeanMoveX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveX_m96FB87B93DDACF291202E912E981B3760B44215D (void);
// 0x00000394 LTDescr LeanTweenExt::LeanMoveX(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveX_m3E49D1A15D798CF0DEE8FB9A1D7F2932D636371D (void);
// 0x00000395 LTDescr LeanTweenExt::LeanMoveY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveY_m0372D8F81E13E6B32BCBA2DBE800598DCA71866B (void);
// 0x00000396 LTDescr LeanTweenExt::LeanMoveY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveY_m821E5633762A108AF0D80528ED060EB0E62DA9E5 (void);
// 0x00000397 LTDescr LeanTweenExt::LeanMoveY(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveY_m1AA13E442CA147D6B062BE8424D669317537F6F5 (void);
// 0x00000398 LTDescr LeanTweenExt::LeanMoveZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveZ_m9939145DDF1A83F4D8547D53A4538912466C008B (void);
// 0x00000399 LTDescr LeanTweenExt::LeanMoveZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveZ_m79A272207A6FBEA47E355DE22A106896A0941ECC (void);
// 0x0000039A LTDescr LeanTweenExt::LeanMoveZ(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveZ_m94F3AB61DB4EBFE4F39B405E5EFE8CD3EC9F2A36 (void);
// 0x0000039B System.Void LeanTweenExt::LeanPause(UnityEngine.GameObject)
extern void LeanTweenExt_LeanPause_m46A64B42348CB04917830211A8C40548EED616E6 (void);
// 0x0000039C LTDescr LeanTweenExt::LeanPlay(UnityEngine.RectTransform,UnityEngine.Sprite[])
extern void LeanTweenExt_LeanPlay_m635E84DBAF3658181BD11102F39B17126695612C (void);
// 0x0000039D System.Void LeanTweenExt::LeanResume(UnityEngine.GameObject)
extern void LeanTweenExt_LeanResume_m3499A30358E3318BB0F73B0B1A07AC9C00E463A4 (void);
// 0x0000039E LTDescr LeanTweenExt::LeanRotate(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanRotate_m3663A0A9B43101BE83E0113F198EDE1DC7A61A4E (void);
// 0x0000039F LTDescr LeanTweenExt::LeanRotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanRotate_m0050CFBB76AE4B345C300EE00F8F13584DAF39A9 (void);
// 0x000003A0 LTDescr LeanTweenExt::LeanRotate(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanRotate_m8B46191A435295E93DB9D3F3F982A468D020E54C (void);
// 0x000003A1 LTDescr LeanTweenExt::LeanRotateAround(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAround_m94E627C88BD4C138F9FD7ADE41FA86F614D4F26B (void);
// 0x000003A2 LTDescr LeanTweenExt::LeanRotateAround(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAround_m8D2DDA35FF67D85798119A863ED498C97980575A (void);
// 0x000003A3 LTDescr LeanTweenExt::LeanRotateAround(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAround_m1C2F12BDAD3FB54F4DB76AD02D759ECBCD15289E (void);
// 0x000003A4 LTDescr LeanTweenExt::LeanRotateAroundLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAroundLocal_m6939D0BB618449C6DB007BD42F9A4C898BE97618 (void);
// 0x000003A5 LTDescr LeanTweenExt::LeanRotateAroundLocal(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAroundLocal_m3977508F1548419CF11A64412C95D747F1C5BE71 (void);
// 0x000003A6 LTDescr LeanTweenExt::LeanRotateAroundLocal(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAroundLocal_m83B8A190A95128CD31886544515865EE7553C0E0 (void);
// 0x000003A7 LTDescr LeanTweenExt::LeanRotateX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateX_m1945A6153A434154D6ADB55FDDECAC9CCC46BDB7 (void);
// 0x000003A8 LTDescr LeanTweenExt::LeanRotateX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateX_mD17F3CCE10C947CB82E9EC222B0810EEA727FCFA (void);
// 0x000003A9 LTDescr LeanTweenExt::LeanRotateY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateY_mA27D8888248D81C3B8EB606E0E5DBD47716C3A7A (void);
// 0x000003AA LTDescr LeanTweenExt::LeanRotateY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateY_m9984A206D938B5D53BAC015F467C919DA595BEDA (void);
// 0x000003AB LTDescr LeanTweenExt::LeanRotateZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateZ_mA27C015E2FEA84D212B0C3D03D6E511035E133CB (void);
// 0x000003AC LTDescr LeanTweenExt::LeanRotateZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateZ_m8CAA7A563407C9E73FA9B514AC08A9B2C0225A97 (void);
// 0x000003AD LTDescr LeanTweenExt::LeanScale(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanScale_mB0084B9F5FC69561F247954CBED4BA70511D1F96 (void);
// 0x000003AE LTDescr LeanTweenExt::LeanScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanScale_mAE75E6B1234A072C02D7354461EAA9EB3D485E76 (void);
// 0x000003AF LTDescr LeanTweenExt::LeanScale(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanScale_m4ED118613DFC434764355605FD977578EC755910 (void);
// 0x000003B0 LTDescr LeanTweenExt::LeanScaleX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleX_mB4F38759A8A24E581E837A8A696D9AB275172103 (void);
// 0x000003B1 LTDescr LeanTweenExt::LeanScaleX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleX_mB89FF0BBCE3BE85F916A6A7B1D8E6AE9E9E88A7A (void);
// 0x000003B2 LTDescr LeanTweenExt::LeanScaleY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleY_m64BF74A2E7E6B81DA3DA69F7F29AD5EB6DC894C4 (void);
// 0x000003B3 LTDescr LeanTweenExt::LeanScaleY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleY_mBCBD993A50FC95AF46ADD7992EB634D05875AF55 (void);
// 0x000003B4 LTDescr LeanTweenExt::LeanScaleZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleZ_m42B1B4A160D482743F667A0E398885BB3C976787 (void);
// 0x000003B5 LTDescr LeanTweenExt::LeanScaleZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleZ_m821101BF3F56706E65FE11C856D03C1F4C02393E (void);
// 0x000003B6 LTDescr LeanTweenExt::LeanSize(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanSize_m99C3564D6FB42626360A9BB25BD965F40F61B376 (void);
// 0x000003B7 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanValue_m0ABC90AD97B6278EBAE75887B295D8EB1A4CA6E1 (void);
// 0x000003B8 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_m6192DF3CB7E8ABD9827718DF1C26F4892DCCA3FA (void);
// 0x000003B9 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanValue_mC3261ED9FF392F50DD1713982ADD612BBAD5266E (void);
// 0x000003BA LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanValue_m5757F21E978B62AF23CC5E142AC222BB4A081784 (void);
// 0x000003BB LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<System.Single>,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_m49CCF82C65BDB92A503F5C61B0EA0121BEEA646B (void);
// 0x000003BC LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`2<System.Single,System.Single>,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_mD59F8CFB5C81AF1061A856CDB0F42244708BC1D6 (void);
// 0x000003BD LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`2<System.Single,System.Object>,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_mB5C28F3E39E0871176731D189EF33CE24E7A6DD3 (void);
// 0x000003BE LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<UnityEngine.Color>,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanValue_m6062D935C55251AC83C7D7D71706FBB350CD375B (void);
// 0x000003BF LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector2>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanValue_m7F3A44B64D4EB8824AF830E9BCEDA6B5463099AB (void);
// 0x000003C0 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector3>,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanValue_m0663B7A32820E3685F4447EFC55951C507170CF5 (void);
// 0x000003C1 System.Void LeanTweenExt::LeanSetPosX(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetPosX_mB614BC76AA5B0934B2F156A9A781DE7DB2A9AEB2 (void);
// 0x000003C2 System.Void LeanTweenExt::LeanSetPosY(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetPosY_m8935780D650702F87FB47C62CB3BA667F4153BAE (void);
// 0x000003C3 System.Void LeanTweenExt::LeanSetPosZ(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetPosZ_m2D663CFB259143ECA22EEF0957396362A0648A7F (void);
// 0x000003C4 System.Void LeanTweenExt::LeanSetLocalPosX(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetLocalPosX_mC5B1B50F60706CF6FF542A63287C1B4DB534691E (void);
// 0x000003C5 System.Void LeanTweenExt::LeanSetLocalPosY(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetLocalPosY_m9D85F225670E5804C0E9E944161F520536DD81C8 (void);
// 0x000003C6 System.Void LeanTweenExt::LeanSetLocalPosZ(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetLocalPosZ_mDEF497EF11DC45905E787A89A835EDC0BB24CE40 (void);
// 0x000003C7 UnityEngine.Color LeanTweenExt::LeanColor(UnityEngine.Transform)
extern void LeanTweenExt_LeanColor_m3C101DA3D605DD4228FD3E6BD67DA7B2C6D61438 (void);
// 0x000003C8 System.Void ReactData::.ctor()
extern void ReactData__ctor_m17087AC6A13423903F1853349D20C6E9C99303BB (void);
// 0x000003C9 System.Void ReactController::Awake()
extern void ReactController_Awake_m913CBEA8EF07DE6F3670DB4D731313C5D255FEB7 (void);
// 0x000003CA System.Void ReactController::onDestroy()
extern void ReactController_onDestroy_mAB9768E447FB391AA7E2773706449AA63AEADA9E (void);
// 0x000003CB System.Void ReactController::SendMessageToReact(System.String,System.String)
extern void ReactController_SendMessageToReact_m43250135A3A5994D2255F3037A8272308F8F3DD8 (void);
// 0x000003CC System.Void ReactController::ShowAR()
extern void ReactController_ShowAR_m25E41FB6D16D381F84877E33E701A35DFE8506C7 (void);
// 0x000003CD System.Void ReactController::HideAR()
extern void ReactController_HideAR_mE9D19346DFC619E808F3D79A2A140AB6EC9CD9B4 (void);
// 0x000003CE System.Void ReactController::Clear3D()
extern void ReactController_Clear3D_mD4FDCCD9C42BDD634CA99C70B56E1A86728E8105 (void);
// 0x000003CF System.Void ReactController::GetReactMessage(System.String)
extern void ReactController_GetReactMessage_m558C33E7FB302D7E53033F957D2DB243F3F9B480 (void);
// 0x000003D0 System.Void ReactController::EmergencyBreak()
extern void ReactController_EmergencyBreak_m16CFFDD939BDFE747A1FE827F2CADFFFAC1C6584 (void);
// 0x000003D1 System.Collections.IEnumerator ReactController::DownloadAnimationRequest(System.Int32,System.String)
extern void ReactController_DownloadAnimationRequest_m01D3184285B7A75BF0F5BA663D5D045394155DBD (void);
// 0x000003D2 System.Void ReactController::.ctor()
extern void ReactController__ctor_m007DE9D24E2057495560F62158F0A8CDD53A2DBE (void);
// 0x000003D3 MessageHandler MessageHandler::Deserialize(System.String)
extern void MessageHandler_Deserialize_mE6CDB21EAEA2491B135D4DDC69091312F68ED426 (void);
// 0x000003D4 T MessageHandler::getData()
// 0x000003D5 System.Void MessageHandler::.ctor(System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JToken)
extern void MessageHandler__ctor_m9B9239E75ED95C1778288A2C33692FABD066647E (void);
// 0x000003D6 System.Void MessageHandler::send(System.Object)
extern void MessageHandler_send_m6E4C9DE3BD5C18B8DD958734711E4FA0803B895C (void);
// 0x000003D7 System.Void UnityMessage::.ctor()
extern void UnityMessage__ctor_m418530AF3414A53A37F1433AEE0C8A83206D44A6 (void);
// 0x000003D8 System.Void UnityMessageManager::onUnityMessage(System.String)
extern void UnityMessageManager_onUnityMessage_m8A904C01129EA827FDD76B08B34FAF9A6C4B5E36 (void);
// 0x000003D9 System.Int32 UnityMessageManager::generateId()
extern void UnityMessageManager_generateId_m105F25DED7848A21DD040716977EF8B8785C3846 (void);
// 0x000003DA UnityMessageManager UnityMessageManager::get_Instance()
extern void UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16 (void);
// 0x000003DB System.Void UnityMessageManager::set_Instance(UnityMessageManager)
extern void UnityMessageManager_set_Instance_mFD260C3BC291C3924C61F0724C6969AB0336716F (void);
// 0x000003DC System.Void UnityMessageManager::add_OnMessage(UnityMessageManager/MessageDelegate)
extern void UnityMessageManager_add_OnMessage_m2725B086DA28C9E73814BE3D96E21BC3F3E3BAA4 (void);
// 0x000003DD System.Void UnityMessageManager::remove_OnMessage(UnityMessageManager/MessageDelegate)
extern void UnityMessageManager_remove_OnMessage_m67B6DA617E39FEA4523E49A8D40590E6F6F19B07 (void);
// 0x000003DE System.Void UnityMessageManager::add_OnRNMessage(UnityMessageManager/MessageHandlerDelegate)
extern void UnityMessageManager_add_OnRNMessage_mCE5F29BD032CE5A7F4106AB17E31CF4AD8538EAE (void);
// 0x000003DF System.Void UnityMessageManager::remove_OnRNMessage(UnityMessageManager/MessageHandlerDelegate)
extern void UnityMessageManager_remove_OnRNMessage_m511526C94D646292B680A2F6448062D3EFB819C9 (void);
// 0x000003E0 System.Void UnityMessageManager::.cctor()
extern void UnityMessageManager__cctor_mA16D91AB3FC363648DDA304555ECCFFF699591E5 (void);
// 0x000003E1 System.Void UnityMessageManager::Awake()
extern void UnityMessageManager_Awake_m1C1E9A8AFB3258EB62A404F13644CBB101AC02DA (void);
// 0x000003E2 System.Void UnityMessageManager::SendMessageToRN(System.String)
extern void UnityMessageManager_SendMessageToRN_m04A9CB5DD296CC74889F72AF6B1E62664438F728 (void);
// 0x000003E3 System.Void UnityMessageManager::SendMessageToRN(UnityMessage)
extern void UnityMessageManager_SendMessageToRN_mB4A20BB4D59ED754C40DA2C5CC09FF853E517887 (void);
// 0x000003E4 System.Void UnityMessageManager::onMessage(System.String)
extern void UnityMessageManager_onMessage_m6E60A5C3A00B83B070276B32B727847F9CEEF413 (void);
// 0x000003E5 System.Void UnityMessageManager::onRNMessage(System.String)
extern void UnityMessageManager_onRNMessage_m57E23694F75F5223BC44C0CFA568FE2A59118EB2 (void);
// 0x000003E6 System.Void UnityMessageManager::.ctor()
extern void UnityMessageManager__ctor_mE89F6AEB4A75A1E52A8DF59747ACC13D3858C7C2 (void);
// 0x000003E7 System.Void ARObject::Setskill(System.Int32)
extern void ARObject_Setskill_mE17AD5A903DC9B3039AAE716AED6717D7E8F2B66 (void);
// 0x000003E8 System.Void ARObject::SetCameraStatus(System.Boolean)
extern void ARObject_SetCameraStatus_m824678FE34CA38612CAABE52EE6EF1E8B6FBE51B (void);
// 0x000003E9 System.Void ARObject::Start()
extern void ARObject_Start_mB3DA5B76805B4E7243A80B9359152E88758F4362 (void);
// 0x000003EA System.Void ARObject::Update()
extern void ARObject_Update_m7AEEB1F52448E77CCE90B1E13335C5F4CA10678E (void);
// 0x000003EB System.Void ARObject::.ctor()
extern void ARObject__ctor_m78CD5BEDC1A8C65B55D53C1E01F52E8016157C50 (void);
// 0x000003EC System.Void ARObjectManager::Reset()
extern void ARObjectManager_Reset_m9AB20BA849B03DC8372BA5B152D626DEE446274C (void);
// 0x000003ED System.Void ARObjectManager::SetARControl(System.Boolean)
extern void ARObjectManager_SetARControl_m806D32457C69D1F834DBF402C038E2BA63E76B2B (void);
// 0x000003EE System.Void ARObjectManager::ResetAnimation()
extern void ARObjectManager_ResetAnimation_m0471D070157C1E91499E54C57F1A3A6BF7A96001 (void);
// 0x000003EF System.Void ARObjectManager::SetFoot()
extern void ARObjectManager_SetFoot_m3D09494940D211D4D8BBD1DFE668D20D60FF651E (void);
// 0x000003F0 System.Void ARObjectManager::SetARSpeed()
extern void ARObjectManager_SetARSpeed_mE18074E3BB1A679571EB39EBAB0D5279D8F7036A (void);
// 0x000003F1 System.Void ARObjectManager::.ctor()
extern void ARObjectManager__ctor_mC207298C17C8F99EE271A398F093E329638301A0 (void);
// 0x000003F2 System.Void ARViewManager::SetSkill(System.Int32,System.Int32,System.String,System.String,System.String,System.Int32)
extern void ARViewManager_SetSkill_mB511EB85FF255073E75B8AF1F893C6758ADEA69C (void);
// 0x000003F3 System.Void ARViewManager::ShowMessage()
extern void ARViewManager_ShowMessage_mDDAEDBD866F97E03B175D0252332F29C6D39364F (void);
// 0x000003F4 System.Void ARViewManager::HideARView()
extern void ARViewManager_HideARView_m1AE02B33559C8C76FA3F1A3506C24E65D58A8870 (void);
// 0x000003F5 System.Void ARViewManager::.ctor()
extern void ARViewManager__ctor_m509908C0DBB609958F6C6FAB655C5694B2E1EE95 (void);
// 0x000003F6 System.Void RecordManager::SetToRecord(System.Boolean)
extern void RecordManager_SetToRecord_mEE79C055C17D2BDD5CA2239E72B44815E1DF7C21 (void);
// 0x000003F7 System.Void RecordManager::StartRecording()
extern void RecordManager_StartRecording_m6056BBCB3076486C933FDDB321B22DC8EF88AE9B (void);
// 0x000003F8 System.Void RecordManager::StopRecording(System.String)
extern void RecordManager_StopRecording_m2480416D3FF7F9FB286904B639D869E86B083477 (void);
// 0x000003F9 System.Void RecordManager::ResetInterface(System.Boolean)
extern void RecordManager_ResetInterface_m7FB8517FE4CD674889E4251AD6CE970B18DCA492 (void);
// 0x000003FA System.Collections.IEnumerator RecordManager::ManageHideObject(System.Boolean,System.Int32)
extern void RecordManager_ManageHideObject_m4645284829BA86723605822892E5339B4113D6BB (void);
// 0x000003FB System.Void RecordManager::.ctor()
extern void RecordManager__ctor_m4A4A202FA7FC57936A4944E88F05601238A4F677 (void);
// 0x000003FC System.Void AnimationGIF::OnEnable()
extern void AnimationGIF_OnEnable_m07A9A38224CD91EE9DD25F7F7FF29C82F060C460 (void);
// 0x000003FD System.Void AnimationGIF::Change()
extern void AnimationGIF_Change_m4A59ADB7EBA9CB202D38DF6DFBFC8EE7C57F7AFC (void);
// 0x000003FE System.Void AnimationGIF::OnDisable()
extern void AnimationGIF_OnDisable_mF28EBA3D1374C41679302930715ECF4C6428BE1E (void);
// 0x000003FF System.Void AnimationGIF::.ctor()
extern void AnimationGIF__ctor_mF21E90099C57C49BC57B739EE573E5F61A62DDB9 (void);
// 0x00000400 System.Void AnimationManager::AnimationCompleteHandler()
extern void AnimationManager_AnimationCompleteHandler_m00A096BD951B015BCF3A4DA645D98FADA0506C5D (void);
// 0x00000401 System.Void AnimationManager::AnimationStartedHandler()
extern void AnimationManager_AnimationStartedHandler_mF3001108FE9B26A85A3BEE23B12C520B2B6686E0 (void);
// 0x00000402 System.Void AnimationManager::OnDisable()
extern void AnimationManager_OnDisable_m0957B0989BF2D1C1BBD27A3CDFF7484B6930519F (void);
// 0x00000403 System.Void AnimationManager::.ctor()
extern void AnimationManager__ctor_mD0BF8A23C4E13B5F7427A1CB25DE31267832D578 (void);
// 0x00000404 System.Void AnimationSimpleController::Start()
extern void AnimationSimpleController_Start_mEDA07690FFEB97B7E484A17DD92D2CD94ABE37C1 (void);
// 0x00000405 System.Void AnimationSimpleController::PlayAnimation(System.Boolean)
extern void AnimationSimpleController_PlayAnimation_m99196AC322C8C22201A17F199BB2B9C58C34F62D (void);
// 0x00000406 System.Void AnimationSimpleController::.ctor()
extern void AnimationSimpleController__ctor_m5D13FDE6D9460838A9620FDB3369810BDA559E0D (void);
// 0x00000407 System.Void CameraControl::SetCameraStatus(System.Boolean)
extern void CameraControl_SetCameraStatus_m03156EB1D5F26527D6C70B80BEA477C9C74E0257 (void);
// 0x00000408 System.Void CameraControl::OnEnable()
extern void CameraControl_OnEnable_mF7C9358B55CE44CFA7BB472CC0D0F4668C2F81A3 (void);
// 0x00000409 System.Void CameraControl::ResetCamera()
extern void CameraControl_ResetCamera_mD93197DF31358AACA48E4CD4BC3F283F8C7DEFB5 (void);
// 0x0000040A System.Void CameraControl::ZoomOnPC(System.Int32)
extern void CameraControl_ZoomOnPC_m10F73839A3FC2B9F4F858B8E3432E5A46C29D8E4 (void);
// 0x0000040B System.Void CameraControl::Update()
extern void CameraControl_Update_mADA15FA22CFBD70CC2D84EB6270E470B770C253C (void);
// 0x0000040C System.Void CameraControl::.ctor()
extern void CameraControl__ctor_m40CDAD0D4D59CE6C6EC7BD8A5EA9048B5DE4C162 (void);
// 0x0000040D System.Void CameraControllersManager::SetCurrentCameras(System.String[])
extern void CameraControllersManager_SetCurrentCameras_mA10220ECC7D63F26C2E68798621033F34F660211 (void);
// 0x0000040E System.Void CameraControllersManager::SetCameraIndex(System.Int32)
extern void CameraControllersManager_SetCameraIndex_m1B4E82952A0509C9DAB842AED6E5F4F7E5C54675 (void);
// 0x0000040F System.Void CameraControllersManager::ChangeCamera()
extern void CameraControllersManager_ChangeCamera_mFC9042999E5F537B442141F4ED0420A19D400247 (void);
// 0x00000410 System.Void CameraControllersManager::ActivateCamera(System.String)
extern void CameraControllersManager_ActivateCamera_m39415E903B3516F33D7AC2189646BACB1CD032E1 (void);
// 0x00000411 System.Void CameraControllersManager::.ctor()
extern void CameraControllersManager__ctor_m43DE492CADC50D6097A72ED0740A6493A472A63B (void);
// 0x00000412 System.Void SoundTester::ResetAudioPlayer()
extern void SoundTester_ResetAudioPlayer_m6226A766AC08A48ABCF0B7EB61A9A51A2A76F939 (void);
// 0x00000413 System.Void SoundTester::OnEnable()
extern void SoundTester_OnEnable_m74E345E5C1DCB0E55F1D378AA58D6DF2C878FB73 (void);
// 0x00000414 System.Void SoundTester::SetLinkToSound(System.Int32)
extern void SoundTester_SetLinkToSound_mD3F06D31FD237135F0C896E8D5D928D4A532F252 (void);
// 0x00000415 System.Void SoundTester::HideSoundButton()
extern void SoundTester_HideSoundButton_m112A9ED037AD7D2624032B3182C40D3AF332BD78 (void);
// 0x00000416 System.Void SoundTester::ChangeLanguage()
extern void SoundTester_ChangeLanguage_m1AC6AD1139051B0DA4E6CB53EA67ACECB46443B3 (void);
// 0x00000417 System.Void SoundTester::SoundButton()
extern void SoundTester_SoundButton_m8CFC5E9EAB950C1255D8832F28A3418FEA889C68 (void);
// 0x00000418 System.Void SoundTester::PlaySound()
extern void SoundTester_PlaySound_m4F04A6FCCE7133D1341CF15300808663D4DC9C0F (void);
// 0x00000419 System.Void SoundTester::StopSound()
extern void SoundTester_StopSound_m9DAA3B76B2170ACA3B2C3F6FB8CCEF391F78693D (void);
// 0x0000041A System.Collections.IEnumerator SoundTester::StreamAudioOnMobile(System.Int32,System.Boolean,System.Boolean)
extern void SoundTester_StreamAudioOnMobile_m0B08846F432195C0BC09FC4C509AC74848770B45 (void);
// 0x0000041B System.String SoundTester::GetLinkToSound(System.Int32,System.Boolean,System.Boolean)
extern void SoundTester_GetLinkToSound_mDC680A8F598DCAA7F3F87AE85040ED5E2D35EC18 (void);
// 0x0000041C System.Void SoundTester::.ctor()
extern void SoundTester__ctor_m3B4813C3108B39E2F9E416D5905BE281FC4A8542 (void);
// 0x0000041D System.Void TimeLineControlller::OnEnable()
extern void TimeLineControlller_OnEnable_m08A28AE7057C806227062C57209CF40F46EBE3CA (void);
// 0x0000041E System.Void TimeLineControlller::MirrorEffect()
extern void TimeLineControlller_MirrorEffect_m854F9D86CC36E07F4523E7FBC1EA9F11B9186A18 (void);
// 0x0000041F System.Void TimeLineControlller::SetSpeedToAnimator(System.Single)
extern void TimeLineControlller_SetSpeedToAnimator_m46F91DE95AF693E44C6B25740842EDC6B9A00932 (void);
// 0x00000420 System.Void TimeLineControlller::ResetAnimationTimeLine()
extern void TimeLineControlller_ResetAnimationTimeLine_mE217205349F484ED2A716AD3F3981CE9790374C2 (void);
// 0x00000421 System.Void TimeLineControlller::Play_StopAnimation()
extern void TimeLineControlller_Play_StopAnimation_m10A664842571FD45AA6C3035526D75199A38FCFA (void);
// 0x00000422 System.Void TimeLineControlller::SetPlayImage()
extern void TimeLineControlller_SetPlayImage_m7A76F43800C5C5985F5379422B42596BF3275AAF (void);
// 0x00000423 System.Void TimeLineControlller::ChangeAnimatorSpeed()
extern void TimeLineControlller_ChangeAnimatorSpeed_m6C677FF71FE1B20A39896557ADDDFEFE440587C0 (void);
// 0x00000424 System.Void TimeLineControlller::SwitchPlayer()
extern void TimeLineControlller_SwitchPlayer_m453D1CFFA3B66FD4BB15F78C58E7AC73AA91F9AF (void);
// 0x00000425 System.Void TimeLineControlller::LockTime()
extern void TimeLineControlller_LockTime_mB260E03FF5F97CB811B8E2B25BF7F66E939E98A6 (void);
// 0x00000426 System.Void TimeLineControlller::SetTime()
extern void TimeLineControlller_SetTime_m99D190D519B557BA777B454DB26C41EA83DE15C0 (void);
// 0x00000427 System.Void TimeLineControlller::FixedUpdate()
extern void TimeLineControlller_FixedUpdate_mA37A71AEDF4DD9E68ED6CEB9F4ADC696AC282E54 (void);
// 0x00000428 System.Void TimeLineControlller::BeginStep()
extern void TimeLineControlller_BeginStep_m760367395B28B54F4A1DCB5617EAE98827D3521F (void);
// 0x00000429 System.Void TimeLineControlller::SkipStep()
extern void TimeLineControlller_SkipStep_mCDF5A1B93B75A31843CEF6BDE48A325B68F1EC7A (void);
// 0x0000042A System.Void TimeLineControlller::ClearAnimation()
extern void TimeLineControlller_ClearAnimation_m4361933B27352FB53959803ADC818265E335205C (void);
// 0x0000042B System.Void TimeLineControlller::SetSkillAnimation(System.Int32)
extern void TimeLineControlller_SetSkillAnimation_mCE35EFC39A63249CBE40D30F53776C3DC230DAAB (void);
// 0x0000042C System.Void TimeLineControlller::SwitchAnimation()
extern void TimeLineControlller_SwitchAnimation_mAFD403BBA0BF6A6A2CAAAB85E2CA6671C8863867 (void);
// 0x0000042D System.Void TimeLineControlller::CheckAnimation()
extern void TimeLineControlller_CheckAnimation_m21E91967BA17764F81938FC35552EDF996773B0A (void);
// 0x0000042E System.Void TimeLineControlller::StartAnimation()
extern void TimeLineControlller_StartAnimation_mB642B4F3476D9082EF678D069D3146C7980235C0 (void);
// 0x0000042F System.Void TimeLineControlller::StopTimeLine()
extern void TimeLineControlller_StopTimeLine_mA31B81020F5D5A42A7A5FD489676651CFA6DF089 (void);
// 0x00000430 System.Void TimeLineControlller::SetAnimationToObject(UnityEngine.Animator,UnityEngine.AnimationClip)
extern void TimeLineControlller_SetAnimationToObject_m259DB3D7CA2EE3C2C31D101354295DD90E561D44 (void);
// 0x00000431 System.Void TimeLineControlller::SetPositionToObjects(UnityEngine.Vector3,System.Boolean)
extern void TimeLineControlller_SetPositionToObjects_m2DC42022267BE715246785E5FCD8CA17047D906E (void);
// 0x00000432 System.Void TimeLineControlller::.ctor()
extern void TimeLineControlller__ctor_m9BE6CB570DFDC5850609F49B3F43034E22074D4B (void);
// 0x00000433 System.Void TipManager::Awake()
extern void TipManager_Awake_m000FB3B4CA0443C8C68DD78A9331160A994DA418 (void);
// 0x00000434 System.Void TipManager::ResetAudioPlayer()
extern void TipManager_ResetAudioPlayer_mEFCC944999CBADA77A24E0FF3F0250E231CD3F91 (void);
// 0x00000435 System.Void TipManager::SetLinkToSound(System.Int32)
extern void TipManager_SetLinkToSound_m496CFEA39BDCDE7F1912547FB5F547DB34E359E7 (void);
// 0x00000436 System.Void TipManager::HideSoundButton()
extern void TipManager_HideSoundButton_m819440CFE82E32C6C6B62B0C6AB74748F8A0F960 (void);
// 0x00000437 System.Void TipManager::ResetAudioPosition()
extern void TipManager_ResetAudioPosition_m5E7D7C9D53708211C1666349760F99020C8635CB (void);
// 0x00000438 System.Void TipManager::ChangeLanguage()
extern void TipManager_ChangeLanguage_m798AB21D10C430095AD9ED812133E9D7862B584C (void);
// 0x00000439 System.Void TipManager::SoundButton()
extern void TipManager_SoundButton_m1257563C8BA365E6DEA723E91334F5C696D69E24 (void);
// 0x0000043A System.Void TipManager::CheckMirrorChange()
extern void TipManager_CheckMirrorChange_mB58EBE19191FB24BD2E13D7575717637E466595F (void);
// 0x0000043B System.Void TipManager::PlaySound()
extern void TipManager_PlaySound_mB3830875E147C9C4775F53B20B49089A54AAD8B4 (void);
// 0x0000043C System.Void TipManager::HideMessage()
extern void TipManager_HideMessage_mBE52A9E50DFB01683658A1EF0DBF79C3829D4E86 (void);
// 0x0000043D System.Void TipManager::PlayOrPause(System.Boolean)
extern void TipManager_PlayOrPause_m433443D5D965DB13617EBD7B4DDAB27F7C6EA313 (void);
// 0x0000043E System.Void TipManager::StopSound()
extern void TipManager_StopSound_mB4A783532642F7AA9185FBAB1CE64A72CDB30B6E (void);
// 0x0000043F System.Collections.IEnumerator TipManager::StreamAudioOnWeb(System.Int32,System.Boolean,System.Boolean)
extern void TipManager_StreamAudioOnWeb_mDBA347C9693F5398AA9950F75AFAF3CF1CFB35C0 (void);
// 0x00000440 System.Collections.IEnumerator TipManager::StreamAudioOnMobile(System.Int32,System.Boolean,System.Boolean)
extern void TipManager_StreamAudioOnMobile_mC699C40E3B6DBA260C3E3D376754D2186ECA0973 (void);
// 0x00000441 System.String TipManager::GetSoundID(System.Int32)
extern void TipManager_GetSoundID_m0A32F3BDD0FA1DBAA989ACB1932D17C8F59DD144 (void);
// 0x00000442 System.String TipManager::GetLinkToSound(System.Int32,System.Boolean,System.Boolean)
extern void TipManager_GetLinkToSound_m3959D9148C188F8D88168C57FDF586640EA0B47C (void);
// 0x00000443 System.Void TipManager::.ctor()
extern void TipManager__ctor_mD574B22018D873BDBD9D408C03EF4F662BED4E18 (void);
// 0x00000444 System.Void BackButton_Manager::Update()
extern void BackButton_Manager_Update_mEFD4086ECF498471B9BFEEDCB584942BAE69ED7B (void);
// 0x00000445 System.Void BackButton_Manager::DeleteButtonOnList(UnityEngine.UI.Button)
extern void BackButton_Manager_DeleteButtonOnList_mC4DDE0069BF9D4DBC02060E182DB9ABB76E79A5C (void);
// 0x00000446 System.Void BackButton_Manager::ClickOnReturn()
extern void BackButton_Manager_ClickOnReturn_mF0CA908D751FE44F36BB64962EF364FB691F7B44 (void);
// 0x00000447 System.Void BackButton_Manager::CheckForBackButton(System.String,System.Boolean)
extern void BackButton_Manager_CheckForBackButton_mEA1B2FD7A2DBE6424AEFF64C807A326E474E0CE3 (void);
// 0x00000448 System.Void BackButton_Manager::.ctor()
extern void BackButton_Manager__ctor_mECB7D3ED4CC60BA22056634381403C1FC5B8D543 (void);
// 0x00000449 System.Void CanvasScalerUpdate::Start()
extern void CanvasScalerUpdate_Start_m3D671E88705C5DC400AA47145325066B344297DD (void);
// 0x0000044A System.Void CanvasScalerUpdate::.ctor()
extern void CanvasScalerUpdate__ctor_m283118746556B9A04D6A291ECF690CE5AB64E970 (void);
// 0x0000044B System.Void CoursePanelManager::Show()
extern void CoursePanelManager_Show_m3A8DA9906705EB9DDED1330FACDB3515011AF70A (void);
// 0x0000044C System.Void CoursePanelManager::Hide()
extern void CoursePanelManager_Hide_m424A304923888994DEE24086A3ACB3E10AA6F207 (void);
// 0x0000044D System.Void CoursePanelManager::.ctor()
extern void CoursePanelManager__ctor_m891889CCBAF85190CDC1A057C0A4048135E1968B (void);
// 0x0000044E System.Void FollowingCamera::Start()
extern void FollowingCamera_Start_mB1E40300679345FD55773B7597D937257BCF1509 (void);
// 0x0000044F System.Void FollowingCamera::OnEnable()
extern void FollowingCamera_OnEnable_mD2E66B6E35D34BF42F511B24406ACEC0EB234B81 (void);
// 0x00000450 System.Void FollowingCamera::FixedUpdate()
extern void FollowingCamera_FixedUpdate_mAA1A98F01A343E41B562E8A957B8BFC30D7AC348 (void);
// 0x00000451 System.Void FollowingCamera::.ctor()
extern void FollowingCamera__ctor_m1BE8AED0308D1E26C647C6A9990476E90C31B7F6 (void);
// 0x00000452 System.Void HideViewsAtStart::OnEnable()
extern void HideViewsAtStart_OnEnable_m5CF173FE4EB509546CA9F22EA5B7D49F1C5A7DDA (void);
// 0x00000453 System.Void HideViewsAtStart::ResetContent()
extern void HideViewsAtStart_ResetContent_m3B3AB2AF4EEAA2548D4AAC9C8377B890BAFBEC9C (void);
// 0x00000454 System.Void HideViewsAtStart::HideObjects(System.Int32)
extern void HideViewsAtStart_HideObjects_m56E1536B0E19197CA92E5F077671F5EBF1B22539 (void);
// 0x00000455 System.Void HideViewsAtStart::.ctor()
extern void HideViewsAtStart__ctor_m956D74DAA9AF42CDA8F4CB8B0E4DF432FD8C8407 (void);
// 0x00000456 System.Void LayoutDelay::OnDisable()
extern void LayoutDelay_OnDisable_mC58599D98A58687482376BD413616EFFDCBB0A9E (void);
// 0x00000457 System.Collections.IEnumerator LayoutDelay::SetStatus()
extern void LayoutDelay_SetStatus_m7AAC055DFCFD1171F13C93C390E4AFA45ABDBEAA (void);
// 0x00000458 System.Void LayoutDelay::OnEnable()
extern void LayoutDelay_OnEnable_mDEAE0EB8DBBFCE6076487DC96DFA56D0367ACE3D (void);
// 0x00000459 System.Void LayoutDelay::.ctor()
extern void LayoutDelay__ctor_m0A4BBB551FF423171F0AD78B5AC266719BCB99B2 (void);
// 0x0000045A System.Void LogoScaler::Start()
extern void LogoScaler_Start_m559E4F99342FCB9C518939551CF14AB1BD0E382C (void);
// 0x0000045B System.Void LogoScaler::.ctor()
extern void LogoScaler__ctor_mC9928E011E1DF1B65071C90FD02D7CED2F66179D (void);
// 0x0000045C System.Void PanelSwitch::SetPanelSwitch(CoursePanelManager)
extern void PanelSwitch_SetPanelSwitch_mFD70D297431440643B091DE7FB4DA68720370A68 (void);
// 0x0000045D System.Void PanelSwitch::HidePanel()
extern void PanelSwitch_HidePanel_mCC491BDF1471DABB5954A047459F6E0966A08F88 (void);
// 0x0000045E System.Void PanelSwitch::.ctor()
extern void PanelSwitch__ctor_m83D98FB5A26FD31AD5B5E8AF6679D4073663E439 (void);
// 0x0000045F System.Void PlayerWeekItem::SetDay(System.String,System.Boolean)
extern void PlayerWeekItem_SetDay_m2296AD540E4EBFEB63A11D5330438B676B844AB3 (void);
// 0x00000460 System.Void PlayerWeekItem::SetBackGround(System.Boolean)
extern void PlayerWeekItem_SetBackGround_m6F99A4318D11D7D0FFCD97D237C952046E05A354 (void);
// 0x00000461 System.Void PlayerWeekItem::.ctor()
extern void PlayerWeekItem__ctor_m447F1E9099EA31FD21023F4F82EE3AA2ADDACDD0 (void);
// 0x00000462 System.Void ScaleToCanvasHeight::Start()
extern void ScaleToCanvasHeight_Start_m222E7B3EF8D6BCFF15D2E307A9C685B1489D6D5E (void);
// 0x00000463 System.Void ScaleToCanvasHeight::.ctor()
extern void ScaleToCanvasHeight__ctor_mDABF8CEFAF59856242AA6F85607998E36A9D725C (void);
// 0x00000464 System.Void ScreenSpaceAdapter::Start()
extern void ScreenSpaceAdapter_Start_mE8BBBD581E56E9DB300E05CD226D2C01527BD474 (void);
// 0x00000465 System.Void ScreenSpaceAdapter::.ctor()
extern void ScreenSpaceAdapter__ctor_mD0DE7AAB253B2AF984FB03EA06435BC6AA4450B5 (void);
// 0x00000466 System.Void Tester::Start()
extern void Tester_Start_m149A96941BB9C65ED600258DA659F0443B5DDB0F (void);
// 0x00000467 System.Collections.IEnumerator Tester::LoginToAccount()
extern void Tester_LoginToAccount_mB7358E36C1ED5FD543B8A6827197E251173B5B89 (void);
// 0x00000468 System.String Tester::ComputeHash(System.String)
extern void Tester_ComputeHash_m67D993E45D772E00CC8418A2722022E0784F4601 (void);
// 0x00000469 System.Void Tester::.ctor()
extern void Tester__ctor_m893C374AFBD19F9D3738B19D4140A8227FF029E6 (void);
// 0x0000046A System.Void UICanvasMessages::Awake()
extern void UICanvasMessages_Awake_mAF30192C1FE23040860F608268118C6BE5CD2373 (void);
// 0x0000046B System.Void UICanvasMessages::ShowMessage(System.String,System.Boolean)
extern void UICanvasMessages_ShowMessage_m78FF2BF77D46AFB54E31068F0F8FAF1299AA2851 (void);
// 0x0000046C System.Void UICanvasMessages::HideMessage()
extern void UICanvasMessages_HideMessage_m01394AF240938014D1EEBC13165D31A879E33A49 (void);
// 0x0000046D System.Void UICanvasMessages::.ctor()
extern void UICanvasMessages__ctor_m795898ABD17D85F9BB5EA922CBEFB1C65D5EAD53 (void);
// 0x0000046E System.Void ServerUserData::.ctor()
extern void ServerUserData__ctor_m31D14ACE9D3A9126F4F3D423FBC5E7D9F74D4136 (void);
// 0x0000046F System.Void ServerTeamsData::.ctor()
extern void ServerTeamsData__ctor_m3016A1B4A67C65940168969CB0F28482C6C08ED4 (void);
// 0x00000470 System.Void ServerTeamData::.ctor()
extern void ServerTeamData__ctor_mE4537499959C59EDD8E8B1BCA8E699316DBD5308 (void);
// 0x00000471 System.Void Data::.ctor()
extern void Data__ctor_mCD3AD6E8D195C4D9716A6478E83258C452C89C07 (void);
// 0x00000472 System.Void UserData::.ctor()
extern void UserData__ctor_m17E413FB2279BF05C1465329FF401F61253AFA54 (void);
// 0x00000473 System.Void User::.ctor()
extern void User__ctor_mB5B1D42F0980D69CF10E95B3BF591E010A500A53 (void);
// 0x00000474 System.Void Team::.ctor()
extern void Team__ctor_m3E77144218E6D3D1EC63590741559CD75000ED50 (void);
// 0x00000475 System.Void CoachSkillsList::.ctor()
extern void CoachSkillsList__ctor_m8B4F78A4CEDEDFFC6BCB9EE6EEFD11272C33D613 (void);
// 0x00000476 System.Void CoachSkillsItem::.ctor()
extern void CoachSkillsItem__ctor_mE72F6373D41E753598E4A93B8B40B72F4E812600 (void);
// 0x00000477 System.Void PlayerSkillsResponse::.ctor()
extern void PlayerSkillsResponse__ctor_m5E65AE1A8225B9161BFC30B6739F83963AD87529 (void);
// 0x00000478 System.Void PlayerSkillsData::.ctor()
extern void PlayerSkillsData__ctor_mE84299CA58CA5455E1667EC91FC6E2BCD769C178 (void);
// 0x00000479 System.Void PivotData::.ctor()
extern void PivotData__ctor_m186643368771B95059BFBB9356D6291FFF29533A (void);
// 0x0000047A System.Void PlayerSkillsItem::.ctor()
extern void PlayerSkillsItem__ctor_mF973C6B8AB4158D4FEE142AA98C7CAE0ED1D2C6E (void);
// 0x0000047B System.Void SkillsReviewResponse::.ctor()
extern void SkillsReviewResponse__ctor_m49478E3F00DE2050E957C6B6F1D90E4748398B2F (void);
// 0x0000047C System.Void ReviewsBlock::.ctor()
extern void ReviewsBlock__ctor_m9D8965DF8CF9E140AEDA09CE9ABC0C895E0CAF85 (void);
// 0x0000047D System.Void PlayerReviewData::.ctor()
extern void PlayerReviewData__ctor_m24F116E9BFCA37D887ECE3B374408FCA217D91FF (void);
// 0x0000047E System.Void ListResponse::.ctor()
extern void ListResponse__ctor_mEB4ACDB4111C7848142EDEA70E16B096B241FFD6 (void);
// 0x0000047F System.Void PlayerData::.ctor()
extern void PlayerData__ctor_m8A621C041AB02CB7E2C83D0DE68FEDF629440AE3 (void);
// 0x00000480 System.Void BundleData::.ctor()
extern void BundleData__ctor_mDDCAE043BA4D4DA0D85205F3795AF803BD28FDBB (void);
// 0x00000481 System.Void CoachSkillDetails::.ctor()
extern void CoachSkillDetails__ctor_m3F882A64C493695E3B9CBE66580FFA9F86017485 (void);
// 0x00000482 System.Void CoachSkillDetailsData::.ctor()
extern void CoachSkillDetailsData__ctor_m8DEC32A8A603D7F8AC17804CD8E65B1DB8576187 (void);
// 0x00000483 System.Void RankingResponse::.ctor()
extern void RankingResponse__ctor_m86376EA957E23C2C64D3363CA7CC06ECEA4C821C (void);
// 0x00000484 System.Void RankingData::.ctor()
extern void RankingData__ctor_mB215EB5586492DDADB91F0D2E413AC81647A9FAC (void);
// 0x00000485 System.Void NotificationsResponse::.ctor()
extern void NotificationsResponse__ctor_mBE83CB743CFAB01067DCD09F58403B52368E718A (void);
// 0x00000486 System.Void Notifications::.ctor()
extern void Notifications__ctor_mF59746E75E6D494784F4CA3E0C59B00C91C11EC8 (void);
// 0x00000487 System.Void ChatNotification::.ctor()
extern void ChatNotification__ctor_mBD750D07BC336211D12DBF38EFC515735F840782 (void);
// 0x00000488 System.Void SimpleResponse::.ctor()
extern void SimpleResponse__ctor_m9E0C8C49526B50FB5661488BA97A83EE373F4269 (void);
// 0x00000489 System.Void OneSignalResponse::.ctor()
extern void OneSignalResponse__ctor_m8E21C7F5B7D45980750C44CC358116215BA9AA9C (void);
// 0x0000048A System.Void CoachDataRequest::.ctor()
extern void CoachDataRequest__ctor_mD742AD2B243E69EB295A808F4BB43E963B60A637 (void);
// 0x0000048B System.Void CoachData::.ctor()
extern void CoachData__ctor_m7942B8E537E4B13B26A8BBF80A2770235C969703 (void);
// 0x0000048C System.Void ChatResponse::.ctor()
extern void ChatResponse__ctor_mC1F4A089130332367BED09FAEDA70C04A1E3AEAA (void);
// 0x0000048D System.Void Chat::.ctor()
extern void Chat__ctor_m05D44117EACDBF7B3E6CC07D06D2C2CA5FC46E9F (void);
// 0x0000048E System.Void PusherResponse::.ctor()
extern void PusherResponse__ctor_m9F510102109179A667947647AB092F88CAC41704 (void);
// 0x0000048F System.Void PusherMessage::.ctor()
extern void PusherMessage__ctor_m87591149AEB9EF2CBAAA2928038CA4ADBF271E21 (void);
// 0x00000490 System.Void GeneralTeamDataResponse::.ctor()
extern void GeneralTeamDataResponse__ctor_mD8BFFA5D1EC73FCF7AC5315A72CC201B00A02E37 (void);
// 0x00000491 System.Void GeneralTeamData::.ctor()
extern void GeneralTeamData__ctor_m1991DD58A9A0BD27C61B0300A068F16DC5F73FAC (void);
// 0x00000492 System.Void EventsDataResponse::.ctor()
extern void EventsDataResponse__ctor_mC5D63B2C2D25F194747F2691918B128A1F534088 (void);
// 0x00000493 System.Void EventsData::.ctor()
extern void EventsData__ctor_mDF785F2C16C0AF770CF8131232EB5B6380AB8CD0 (void);
// 0x00000494 System.Void MessageToDelete::.ctor()
extern void MessageToDelete__ctor_m4E4209D2A3A421F03A06C4B212B7AF9B75FDA73C (void);
// 0x00000495 System.Void PlayersNotVideos::.ctor()
extern void PlayersNotVideos__ctor_mF018C5C94D7CCA54E560DC3EBEDFE27DBB4577BC (void);
// 0x00000496 System.Void SkillNotVideos::.ctor()
extern void SkillNotVideos__ctor_mF0773660B2B69FDABA5B825007D7E423271FBA03 (void);
// 0x00000497 System.String Utils::GetCleanData(System.String)
extern void Utils_GetCleanData_mE7063B99E9BC89CAC79A186FE7812AC5F5581A9A (void);
// 0x00000498 System.String Utils::ComputeHash(System.String)
extern void Utils_ComputeHash_m3F021E5385B1199A03FF84CE0E38CF559F58C498 (void);
// 0x00000499 System.String Utils::GetCurrentPlataform()
extern void Utils_GetCurrentPlataform_m2C379C3FCABE1614B436B7C7E9BECB40317443FC (void);
// 0x0000049A System.String Utils::GetTimeAgo(System.String)
extern void Utils_GetTimeAgo_mD443EB772566421784C838FF25B44B62571B2D4B (void);
// 0x0000049B UnityEngine.GameObject[] Utils::GetChildsInContainer(UnityEngine.Transform)
extern void Utils_GetChildsInContainer_mB2A3205244D62C03C1141F29B489B0566673AEA0 (void);
// 0x0000049C System.Void Utils::ClearChildsInContainer(UnityEngine.Transform)
extern void Utils_ClearChildsInContainer_mBE88354E3CDD6DE3644CCDA0816B2E05A0C904D4 (void);
// 0x0000049D System.Int32 Utils::GetCourseID(System.String)
extern void Utils_GetCourseID_m25DDEFFB89C417B87B7B4BDA45C716B8982E2AA6 (void);
// 0x0000049E System.String Utils::GetWorkingTypeDisplay(System.String)
extern void Utils_GetWorkingTypeDisplay_mCCB053E0A1E5AC672F9C97D8EFB7681A8C4438D9 (void);
// 0x0000049F System.String Utils::UpperCaseFirstLevel(System.String)
extern void Utils_UpperCaseFirstLevel_m35D3848467D6F69AF04311DCE12B4D1E6A2A2421 (void);
// 0x000004A0 System.Void Utils::.cctor()
extern void Utils__cctor_m3F050F239F46DED13E183D1F52F0E57D46665693 (void);
// 0x000004A1 System.Void SkillAnimationPreviewManager::SetCone(System.String)
extern void SkillAnimationPreviewManager_SetCone_m3E768683BDA02DEAB490C6CF11BDC60199468FFB (void);
// 0x000004A2 System.Void SkillAnimationPreviewManager::ChangeCone()
extern void SkillAnimationPreviewManager_ChangeCone_mC2084EAD2438D267E663861E2A764E4F041AD98A (void);
// 0x000004A3 System.Void SkillAnimationPreviewManager::SetSkill(System.Int32,System.Int32,System.Boolean,System.String)
extern void SkillAnimationPreviewManager_SetSkill_mD497872AAE5788680505126EB3D332C993F17527 (void);
// 0x000004A4 System.Void SkillAnimationPreviewManager::SetExercise(System.Int32,System.Int32,System.String,System.String)
extern void SkillAnimationPreviewManager_SetExercise_mC0528DED9B0E49E6357A7A88EF8056949F43C035 (void);
// 0x000004A5 System.Void SkillAnimationPreviewManager::ActivateCameraControl()
extern void SkillAnimationPreviewManager_ActivateCameraControl_m1CB3C6B6677CF5E6A778B6978048F4ED1F957974 (void);
// 0x000004A6 System.Void SkillAnimationPreviewManager::HideAnimationPreview()
extern void SkillAnimationPreviewManager_HideAnimationPreview_m5BCC7526280E5FCE3B4766258A9D74BC9CBD61C5 (void);
// 0x000004A7 System.Void SkillAnimationPreviewManager::ResetAnimation()
extern void SkillAnimationPreviewManager_ResetAnimation_m73A5539DCD802A6A280A677F9F75208013EEFA99 (void);
// 0x000004A8 System.Void SkillAnimationPreviewManager::LoadARCanvas()
extern void SkillAnimationPreviewManager_LoadARCanvas_m821F284EFFC518A99C49176B01D31BB286F29331 (void);
// 0x000004A9 System.Void SkillAnimationPreviewManager::.ctor()
extern void SkillAnimationPreviewManager__ctor_m631D29CDD3E0714C88BA43A5C00DFF8894341F58 (void);
// 0x000004AA System.Void SkillsAnimationsManager::ClearList()
extern void SkillsAnimationsManager_ClearList_m804B19E1538CC452B87E7AA3F32522A080688427 (void);
// 0x000004AB System.Void SkillsAnimationsManager::CheckAnimation(UnityEngine.Transform,System.Int32)
extern void SkillsAnimationsManager_CheckAnimation_mA2E84388486F36C04A09426B66855DFACCDD91D1 (void);
// 0x000004AC UnityEngine.AnimationClip[] SkillsAnimationsManager::GetAnimation(System.Int32)
extern void SkillsAnimationsManager_GetAnimation_mFA003D9F11B15D3166016B1A36C8D63B873A4797 (void);
// 0x000004AD System.Void SkillsAnimationsManager::AddAnimationToList(System.Int32,UnityEngine.AnimationClip[])
extern void SkillsAnimationsManager_AddAnimationToList_m1696113D86C04B7F3DA4BE07DA420DC01BCB9976 (void);
// 0x000004AE System.Void SkillsAnimationsManager::.ctor()
extern void SkillsAnimationsManager__ctor_m2D5A4E351D2BAD3FCCE776E09D1F53FBA7740BEB (void);
// 0x000004AF System.String SkillsDictionary::GetName(System.String)
extern void SkillsDictionary_GetName_m579596021C007D4CC5FF5CDDB01AC6AE5F14E85A (void);
// 0x000004B0 System.Void SkillsDictionary::Awake()
extern void SkillsDictionary_Awake_m2252A10894656F7990C7F8B312F242FFF879E21C (void);
// 0x000004B1 System.Int32 SkillsDictionary::GetExcelCodeByID(System.Int32)
extern void SkillsDictionary_GetExcelCodeByID_mF1CBE106C6E5C7EBE74DE2804FEC7DFD3344410B (void);
// 0x000004B2 System.Int32 SkillsDictionary::GetExcercisePlayerCount(System.Int32)
extern void SkillsDictionary_GetExcercisePlayerCount_m5909209C29BC0F8033B389280858A0B6BCA95F1A (void);
// 0x000004B3 SkillsDictionary/ExerciseDataItem SkillsDictionary::GetExcerciseByID(System.Int32)
extern void SkillsDictionary_GetExcerciseByID_m4C846EE4A7AF7FAE293DE376BC48B45B3F1048C6 (void);
// 0x000004B4 System.Int32 SkillsDictionary::GetIDByExcelcode(System.Int32)
extern void SkillsDictionary_GetIDByExcelcode_mFA29ED3A9F1C9CA012939D3557E54B14A5BD909F (void);
// 0x000004B5 System.String[] SkillsDictionary::GetExerciseSteps(System.Int32)
extern void SkillsDictionary_GetExerciseSteps_m5CE551B195588EC7FE6DBAEC625518C6717EBB01 (void);
// 0x000004B6 System.String SkillsDictionary::GetExercisesName(System.Int32)
extern void SkillsDictionary_GetExercisesName_mC4D88A3AFE6007630BEA750A49505AC456995639 (void);
// 0x000004B7 System.Int32 SkillsDictionary::GetExercisesNumber(System.Int32)
extern void SkillsDictionary_GetExercisesNumber_mEED389C5526CFF26499D088AA4EE29BA50557E01 (void);
// 0x000004B8 System.String SkillsDictionary::GetSkillName(System.Int32)
extern void SkillsDictionary_GetSkillName_m13E573D637E3F5A3091948643D6CD8DC33DC794D (void);
// 0x000004B9 UnityEngine.Sprite SkillsDictionary::GetPictureSkill(System.Int32)
extern void SkillsDictionary_GetPictureSkill_m4D24FCB9795AB756BF4B397432591B2C0B43A37F (void);
// 0x000004BA UnityEngine.Sprite SkillsDictionary::GetPictureExercise(System.Int32)
extern void SkillsDictionary_GetPictureExercise_m1017F7B6CACB12FA8B246CC31267EAFEBDEC9DF5 (void);
// 0x000004BB System.String SkillsDictionary::GetSkillCategory(System.Int32)
extern void SkillsDictionary_GetSkillCategory_m9E5CD913A7483131039A104F93CD3204D42ABCD0 (void);
// 0x000004BC System.String SkillsDictionary::GetAudioFolderName(System.Int32)
extern void SkillsDictionary_GetAudioFolderName_m01C59C0314D7EF6A15EDAE9249A5E013A2B2919E (void);
// 0x000004BD System.Int32 SkillsDictionary::GetSkillCategoryID(System.Int32)
extern void SkillsDictionary_GetSkillCategoryID_m58E126A0EAFABC7BB95807E92B8677F266D1B0CC (void);
// 0x000004BE System.Int32 SkillsDictionary::GetCategoryIndex(System.String)
extern void SkillsDictionary_GetCategoryIndex_m8AAF38A446E1C042721CE228F6A8D5AB682131AE (void);
// 0x000004BF System.String SkillsDictionary::GetSkillLevel(System.Int32)
extern void SkillsDictionary_GetSkillLevel_m6D78D1522F72FF3A649007B73EB79D693818FF2E (void);
// 0x000004C0 System.Int32 SkillsDictionary::GetCategorySkillsCount(System.String,System.Int32)
extern void SkillsDictionary_GetCategorySkillsCount_m4599F3D64FD1F5A0566281EA5726A5CED4A6DD24 (void);
// 0x000004C1 System.Void SkillsDictionary::.ctor()
extern void SkillsDictionary__ctor_mB16D28E4B9F93A949565AF6F98DCA26BD182D4FD (void);
// 0x000004C2 System.Void TranslationDropDownItem::.ctor()
extern void TranslationDropDownItem__ctor_mF4E35B3958038DCD0ACC590EFEAB975491F7E3B4 (void);
// 0x000004C3 System.Void TranslationInputItem::Start()
extern void TranslationInputItem_Start_mC71BE956F92A2BF96CCAA9BBC086795F5BA7F0FC (void);
// 0x000004C4 System.Void TranslationInputItem::.ctor()
extern void TranslationInputItem__ctor_m038A24B857584A9CBBB1114B3141A2E63CD710F9 (void);
// 0x000004C5 System.Void TranslationItem::Awake()
extern void TranslationItem_Awake_m6D9135A9FE36371914D0B314BA157AAF49A3D1F8 (void);
// 0x000004C6 System.Void TranslationItem::Start()
extern void TranslationItem_Start_m41270ABB11EC0D93284BD80988591962CBC73CA7 (void);
// 0x000004C7 System.Void TranslationItem::Check()
extern void TranslationItem_Check_m7B6206719C37A0FBA33A65DEACB0A097506D851D (void);
// 0x000004C8 System.Void TranslationItem::.ctor()
extern void TranslationItem__ctor_m389D38D92E3249BDFF73FBA2B1D0B16A27508D3B (void);
// 0x000004C9 System.Void TranslationManager::Awake()
extern void TranslationManager_Awake_m0287A8E51604439F789686A56F940D85820BFF87 (void);
// 0x000004CA System.String TranslationManager::GetTranslation(System.String,TextStyle)
extern void TranslationManager_GetTranslation_m1228F93C6240D0B4A1C540A59A35345B4A0F36E3 (void);
// 0x000004CB System.String TranslationManager::CheckWordInList(System.String,System.Collections.Generic.List`1<TranslationManager/MyDictionary>)
extern void TranslationManager_CheckWordInList_m43AAB95197DD6EBED05648DCEE210A988507AC23 (void);
// 0x000004CC System.String TranslationManager::SetResultTranslation(System.String,TextStyle)
extern void TranslationManager_SetResultTranslation_mEF01ED25DD903A08F69D75DF27DF81E1E422CE3F (void);
// 0x000004CD System.Void TranslationManager::FillChineseDictionary()
extern void TranslationManager_FillChineseDictionary_mB4B1EB420E74479D9C96993AA91A6021D86F5ADA (void);
// 0x000004CE System.Void TranslationManager::FillSpanishDictionary()
extern void TranslationManager_FillSpanishDictionary_mCB68D538A20F4F27EE36ABA10431CD87511CC9F0 (void);
// 0x000004CF System.Void TranslationManager::.ctor()
extern void TranslationManager__ctor_m36189DCD06C73713B72C42ABFE2579B044A4109A (void);
// 0x000004D0 System.Void UIControl::Start()
extern void UIControl_Start_m29A668948A3DABDD046DC4CF5F12DFEA8656AA00 (void);
// 0x000004D1 System.Void UIControl::SetPreviewElement()
extern void UIControl_SetPreviewElement_mF829A395F7867A94C4378BBCBF274A99D36B3880 (void);
// 0x000004D2 System.Void UIControl::PreviewElementState(System.Boolean)
extern void UIControl_PreviewElementState_m0DA18655474B520E52288A8092480F5D1C4A602F (void);
// 0x000004D3 System.Void UIControl::ShowPreviewCanvas()
extern void UIControl_ShowPreviewCanvas_m98C3B51E6E4ABF7BA73CD4DB609F6E9947D86383 (void);
// 0x000004D4 System.Void UIControl::ShowMainCanvas()
extern void UIControl_ShowMainCanvas_mA6E4D2A61ADE7F47CB5E78DFA10616D6EE5BCEAF (void);
// 0x000004D5 System.Collections.IEnumerator UIControl::SetPreviewScale(System.Single,System.Single)
extern void UIControl_SetPreviewScale_mA468EC06328AD0953BD0681374B9D0A78A29B2D7 (void);
// 0x000004D6 System.Void UIControl::HideMainCanvas()
extern void UIControl_HideMainCanvas_mCBE08EA82F760AFA366C953ADC055D06A38830A4 (void);
// 0x000004D7 System.Collections.IEnumerator UIControl::CheckARCapacity()
extern void UIControl_CheckARCapacity_m76ADF9D6688E9DE7490DA28B9E8CB6424B828023 (void);
// 0x000004D8 System.Void UIControl::HideARCanvas()
extern void UIControl_HideARCanvas_m44FD87263B0A70629D4987933286C52DE14CDD07 (void);
// 0x000004D9 System.Boolean UIControl::HasPermissionForAR()
extern void UIControl_HasPermissionForAR_m956B0D950D0144FD9102BCA0165625182EEA9190 (void);
// 0x000004DA System.Boolean UIControl::CheckAndroidAudio()
extern void UIControl_CheckAndroidAudio_m979DFEB0EDD1A3E42227C5AFCE6236C4785C21B7 (void);
// 0x000004DB System.Void UIControl::ShowARCanvas()
extern void UIControl_ShowARCanvas_m8C90624ED0B02DB5FAE62E02F8820D723107ECF1 (void);
// 0x000004DC System.Void UIControl::ShowCanvas(UnityEngine.RectTransform)
extern void UIControl_ShowCanvas_m1DBFDC7C797C43087F1FC8151AD7319FED172D4E (void);
// 0x000004DD System.Void UIControl::HideCanvas(UnityEngine.RectTransform)
extern void UIControl_HideCanvas_m3CFFD3EEE63588BBE44AF2FF63E24539DC950FDF (void);
// 0x000004DE System.Void UIControl::HideCanvasInverse(UnityEngine.RectTransform)
extern void UIControl_HideCanvasInverse_m55583D87411CDAB3BA086E6012F06EE900B14A39 (void);
// 0x000004DF System.Collections.IEnumerator UIControl::SetActiveState(System.Single,UnityEngine.RectTransform)
extern void UIControl_SetActiveState_mBECDC194AD2E9498401D5E48EF6B00281CD6EF60 (void);
// 0x000004E0 System.Void UIControl::.ctor()
extern void UIControl__ctor_m4AAD1A34B5E78D619B2A13D4427B3A3DF54254C2 (void);
// 0x000004E1 System.Void VideoPlayerController::Awake()
extern void VideoPlayerController_Awake_mF6E1AA703F1554EB66A033C575655370E4493C59 (void);
// 0x000004E2 System.Void VideoPlayerController::StopVideo()
extern void VideoPlayerController_StopVideo_m1CDEAB2485DA294C8F8B61694B3C6DBC57103926 (void);
// 0x000004E3 System.Void VideoPlayerController::PlayPauseVideo()
extern void VideoPlayerController_PlayPauseVideo_m2C4B0C90F5C62F3CD76F713BDD3558CAC6E02D82 (void);
// 0x000004E4 System.Void VideoPlayerController::FixedUpdate()
extern void VideoPlayerController_FixedUpdate_m1D939733CB33C4E4843A2C76D1914726D44033D1 (void);
// 0x000004E5 System.Void VideoPlayerController::ClearVideoPlayer()
extern void VideoPlayerController_ClearVideoPlayer_m4C6508F4F2E52523894B1F80B79A6AAB52D1AB79 (void);
// 0x000004E6 System.Void VideoPlayerController::OnDisable()
extern void VideoPlayerController_OnDisable_mA3EAE527C98EC4BACAD95FA0F51BEAC4AE0112FF (void);
// 0x000004E7 System.Void VideoPlayerController::SetVideoOnPause(System.String)
extern void VideoPlayerController_SetVideoOnPause_m3ACF8F8502CAC3980175FFAD44A5607DADED7624 (void);
// 0x000004E8 System.Collections.IEnumerator VideoPlayerController::PauseOnFirstFrame()
extern void VideoPlayerController_PauseOnFirstFrame_mF9F7006247F442FE443068D65A57E1EF9CAC1FA4 (void);
// 0x000004E9 System.Void VideoPlayerController::SetVideo(System.String)
extern void VideoPlayerController_SetVideo_mFBCA0C702139D1AD62A3F19B51DFDB798DAC16FB (void);
// 0x000004EA System.Collections.IEnumerator VideoPlayerController::GetFirstFrame()
extern void VideoPlayerController_GetFirstFrame_m7E5F7E23EC8A78726B52DBB1FD1D6C284F0F265F (void);
// 0x000004EB System.Void VideoPlayerController::.ctor()
extern void VideoPlayerController__ctor_m7F79CC84BE3EAD9D76CBFE8B13AA3711A573244D (void);
// 0x000004EC System.Void VideoPlayerView::SetVideoView(System.String,System.String,System.Int32,System.String,System.Int32)
extern void VideoPlayerView_SetVideoView_m8CE9F2AD1B556D92F92ED01820E6DEC5062E00B5 (void);
// 0x000004ED System.Void VideoPlayerView::.ctor()
extern void VideoPlayerView__ctor_m93E08BE6AF15C299166586166B0D81234EEAECC5 (void);
// 0x000004EE System.Collections.IEnumerator VideoUploader::VideoUploadRequest()
extern void VideoUploader_VideoUploadRequest_m16F30ACD3F84DA549990BABF008AC2E262197F02 (void);
// 0x000004EF System.Void VideoUploader::.ctor()
extern void VideoUploader__ctor_m6728F827B620AAC67D8D57B6A7C876B345B89981 (void);
// 0x000004F0 System.String CommonTools.CryptoTools::cifrar(System.String,System.String,System.String)
extern void CryptoTools_cifrar_mDE04EFF48265C24BEDBA3F0686820E527B1542A3 (void);
// 0x000004F1 System.String CommonTools.CryptoTools::descifrar1(System.String,System.String)
extern void CryptoTools_descifrar1_m4DA6E0B1DF0751B09CD77816E8887BAA439B6B16 (void);
// 0x000004F2 System.String CommonTools.CryptoTools::descifrar(System.String,System.String)
extern void CryptoTools_descifrar_m92DB500742AA4D25B6A87F22ED6A48028C64261F (void);
// 0x000004F3 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::get_placedPrefab()
extern void PlaceOnPlane_get_placedPrefab_mB16E4C48DB7636A8947E69FC174B8ADDE72C9A03 (void);
// 0x000004F4 System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::set_placedPrefab(UnityEngine.GameObject)
extern void PlaceOnPlane_set_placedPrefab_mCDE62C5196B4ED0382F261FA28670EC75A0BFA03 (void);
// 0x000004F5 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::get_spawnedObject()
extern void PlaceOnPlane_get_spawnedObject_mD0CF235CF8E541887A9FFB2C17E371EFB1EEB566 (void);
// 0x000004F6 System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::set_spawnedObject(UnityEngine.GameObject)
extern void PlaceOnPlane_set_spawnedObject_mF5EF3DC967889CD70BD8745BFA4DD34C14F68AB9 (void);
// 0x000004F7 System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::Awake()
extern void PlaceOnPlane_Awake_mADC207DE2F5C12243E95D153CD46C810DA7DB142 (void);
// 0x000004F8 System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::OnDisable()
extern void PlaceOnPlane_OnDisable_m3C9D6C8DB875B36A3C8685E8A7584574F9BE05E3 (void);
// 0x000004F9 System.Boolean UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::TryGetTouchPosition(UnityEngine.Vector2&)
extern void PlaceOnPlane_TryGetTouchPosition_m1FFA9158FDEB61C7BB6319C588CDDD0B390FBFE8 (void);
// 0x000004FA System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::ResetAR()
extern void PlaceOnPlane_ResetAR_m688C0ADF99DF3722A526D4BAF45DF16F5D8588DB (void);
// 0x000004FB System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::StopAR(System.Boolean,System.String)
extern void PlaceOnPlane_StopAR_m2B95DE99D8B5213B68F398AE82F7CABBB399C6C0 (void);
// 0x000004FC System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::TestSetARObject()
extern void PlaceOnPlane_TestSetARObject_m5EDD424845836D39DCD5C8FF6FDA3C676D1FAA2D (void);
// 0x000004FD System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::ResetclickDelay()
extern void PlaceOnPlane_ResetclickDelay_mA5A6951A3A554F988ED02BF1C979D9A9B5E8BA2D (void);
// 0x000004FE System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::Update()
extern void PlaceOnPlane_Update_mD220C63CD54DDBAE6C8B6862BEEC4654F956910B (void);
// 0x000004FF System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::.ctor()
extern void PlaceOnPlane__ctor_mF199E5F3E123D8F1C8E111E0773093C8E2E0A2E5 (void);
// 0x00000500 System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::.cctor()
extern void PlaceOnPlane__cctor_m797D1E1F09619F492C3330746975202822EC387B (void);
// 0x00000501 System.ValueTuple`2<System.Int32,System.Int32> NatSuite.Recorders.GIFRecorder::get_frameSize()
extern void GIFRecorder_get_frameSize_mAF322B62FC3A6D89C8A96AF422C33C042DCB5F19 (void);
// 0x00000502 System.Void NatSuite.Recorders.GIFRecorder::.ctor(System.Int32,System.Int32,System.Single)
extern void GIFRecorder__ctor_m7877546D7323542EE5B85A7BB154F7CF770D0B24 (void);
// 0x00000503 System.Void NatSuite.Recorders.GIFRecorder::CommitFrame(T[],System.Int64)
// 0x00000504 System.Void NatSuite.Recorders.GIFRecorder::CommitFrame(System.IntPtr,System.Int64)
extern void GIFRecorder_CommitFrame_m3C8F811EAE575642F6C44D49630D51B40E257968 (void);
// 0x00000505 System.Void NatSuite.Recorders.GIFRecorder::CommitSamples(System.Single[],System.Int64)
extern void GIFRecorder_CommitSamples_mAE812C381DA1714761F257D00EA25FB766D6A6E3 (void);
// 0x00000506 System.Threading.Tasks.Task`1<System.String> NatSuite.Recorders.GIFRecorder::FinishWriting()
extern void GIFRecorder_FinishWriting_m60E9D529C5E9DD5997D9741CC1F6756338B6EB43 (void);
// 0x00000507 System.ValueTuple`2<System.Int32,System.Int32> NatSuite.Recorders.HEVCRecorder::get_frameSize()
extern void HEVCRecorder_get_frameSize_m94F4A45264A73D19410AE8DC0061974307A1F29B (void);
// 0x00000508 System.Void NatSuite.Recorders.HEVCRecorder::.ctor(System.Int32,System.Int32,System.Single,System.Int32,System.Int32,System.Int32,System.Int32)
extern void HEVCRecorder__ctor_mF6EE5B8778DA065775FBD166ABDFF189429FFA3B (void);
// 0x00000509 System.Void NatSuite.Recorders.HEVCRecorder::CommitFrame(T[],System.Int64)
// 0x0000050A System.Void NatSuite.Recorders.HEVCRecorder::CommitFrame(System.IntPtr,System.Int64)
extern void HEVCRecorder_CommitFrame_mC8B7C1C1FF5E7B361BCE109240AE04E1C30E2DDD (void);
// 0x0000050B System.Void NatSuite.Recorders.HEVCRecorder::CommitSamples(System.Single[],System.Int64)
extern void HEVCRecorder_CommitSamples_m6B9585012799677A81CE10DDAF53E3EA08FAE795 (void);
// 0x0000050C System.Threading.Tasks.Task`1<System.String> NatSuite.Recorders.HEVCRecorder::FinishWriting()
extern void HEVCRecorder_FinishWriting_mF0DCDA4A632D6CB1FB9B4E78888F367F03EA77F2 (void);
// 0x0000050D System.ValueTuple`2<System.Int32,System.Int32> NatSuite.Recorders.IMediaRecorder::get_frameSize()
// 0x0000050E System.Void NatSuite.Recorders.IMediaRecorder::CommitFrame(T[],System.Int64)
// 0x0000050F System.Void NatSuite.Recorders.IMediaRecorder::CommitFrame(System.IntPtr,System.Int64)
// 0x00000510 System.Void NatSuite.Recorders.IMediaRecorder::CommitSamples(System.Single[],System.Int64)
// 0x00000511 System.Threading.Tasks.Task`1<System.String> NatSuite.Recorders.IMediaRecorder::FinishWriting()
// 0x00000512 System.ValueTuple`2<System.Int32,System.Int32> NatSuite.Recorders.JPGRecorder::get_frameSize()
extern void JPGRecorder_get_frameSize_mA2FB0F92242454DC8FF8870CB0B41B639CCD939C (void);
// 0x00000513 System.Void NatSuite.Recorders.JPGRecorder::.ctor(System.Int32,System.Int32)
extern void JPGRecorder__ctor_m9D6F75AF0202F8747BAA5D53BF56789EF9811F44 (void);
// 0x00000514 System.Void NatSuite.Recorders.JPGRecorder::CommitFrame(T[],System.Int64)
// 0x00000515 System.Void NatSuite.Recorders.JPGRecorder::CommitFrame(System.IntPtr,System.Int64)
extern void JPGRecorder_CommitFrame_m5B4D55C02454FF2FED0BB874788F531DA08630DF (void);
// 0x00000516 System.Void NatSuite.Recorders.JPGRecorder::CommitSamples(System.Single[],System.Int64)
extern void JPGRecorder_CommitSamples_mCDB906AA2C0DF6D034E8BB7AE6C18622B27140E8 (void);
// 0x00000517 System.Threading.Tasks.Task`1<System.String> NatSuite.Recorders.JPGRecorder::FinishWriting()
extern void JPGRecorder_FinishWriting_m2C76C18768813218838577D6CF0FC36C7EA77AEE (void);
// 0x00000518 System.String NatSuite.Recorders.JPGRecorder::<FinishWriting>b__6_0(System.Threading.Tasks.Task)
extern void JPGRecorder_U3CFinishWritingU3Eb__6_0_m37DE8E330FF45AC97764BC0EACB5CC53DEC85644 (void);
// 0x00000519 System.ValueTuple`2<System.Int32,System.Int32> NatSuite.Recorders.MP4Recorder::get_frameSize()
extern void MP4Recorder_get_frameSize_m19577C451FE6E3F1A654BB11D1F0ABA7DCBF033E (void);
// 0x0000051A System.Void NatSuite.Recorders.MP4Recorder::.ctor(System.Int32,System.Int32,System.Single,System.Int32,System.Int32,System.Int32,System.Int32)
extern void MP4Recorder__ctor_m0D64C114C1434E483486F0794E482E9B4FCF431D (void);
// 0x0000051B System.Void NatSuite.Recorders.MP4Recorder::CommitFrame(T[],System.Int64)
// 0x0000051C System.Void NatSuite.Recorders.MP4Recorder::CommitFrame(System.IntPtr,System.Int64)
extern void MP4Recorder_CommitFrame_m9BB983BD9C296E7AB8784B4543ECF884FFDC702D (void);
// 0x0000051D System.Void NatSuite.Recorders.MP4Recorder::CommitSamples(System.Single[],System.Int64)
extern void MP4Recorder_CommitSamples_m040DED0BEA13E3257637BE6192868A2EF81153E9 (void);
// 0x0000051E System.Threading.Tasks.Task`1<System.String> NatSuite.Recorders.MP4Recorder::FinishWriting()
extern void MP4Recorder_FinishWriting_m2DF6A9BEBDF38C66E0413084BA873FE68FCB92F9 (void);
// 0x0000051F System.ValueTuple`2<System.Int32,System.Int32> NatSuite.Recorders.WAVRecorder::get_frameSize()
extern void WAVRecorder_get_frameSize_mD98B044EB00E32D8C3A79D00151CF2C0321DFDB5 (void);
// 0x00000520 System.Void NatSuite.Recorders.WAVRecorder::.ctor(System.Int32,System.Int32)
extern void WAVRecorder__ctor_m8FDACDDCDEFEF8EA49DFE4F39A047201D35FB633 (void);
// 0x00000521 System.Void NatSuite.Recorders.WAVRecorder::CommitFrame(T[],System.Int64)
// 0x00000522 System.Void NatSuite.Recorders.WAVRecorder::CommitFrame(System.IntPtr,System.Int64)
extern void WAVRecorder_CommitFrame_m4DE08233AA6FD2DF73EF87D5095FF143E613190F (void);
// 0x00000523 System.Void NatSuite.Recorders.WAVRecorder::CommitSamples(System.Single[],System.Int64)
extern void WAVRecorder_CommitSamples_mAEBB994CDA4628B4CA680C918FCFF61A0660F881 (void);
// 0x00000524 System.Threading.Tasks.Task`1<System.String> NatSuite.Recorders.WAVRecorder::FinishWriting()
extern void WAVRecorder_FinishWriting_m96367BC5B7D51B42E8793F082317DD714536F6A1 (void);
// 0x00000525 System.IntPtr NatSuite.Recorders.Internal.Bridge::CreateMP4Recorder(System.Int32,System.Int32,System.Single,System.Int32,System.Int32,System.Int32,System.Int32,System.String,NatSuite.Recorders.Internal.Bridge/RecordingHandler,System.IntPtr)
extern void Bridge_CreateMP4Recorder_mA4D58809A1CF0F3C399CC88501E6200000FEEA79 (void);
// 0x00000526 System.IntPtr NatSuite.Recorders.Internal.Bridge::CreateHEVCRecorder(System.Int32,System.Int32,System.Single,System.Int32,System.Int32,System.Int32,System.Int32,System.String,NatSuite.Recorders.Internal.Bridge/RecordingHandler,System.IntPtr)
extern void Bridge_CreateHEVCRecorder_m62B4CEDCFCA39E5549A2FC337F639D886BFF39DA (void);
// 0x00000527 System.IntPtr NatSuite.Recorders.Internal.Bridge::CreateGIFRecorder(System.Int32,System.Int32,System.Single,System.String,NatSuite.Recorders.Internal.Bridge/RecordingHandler,System.IntPtr)
extern void Bridge_CreateGIFRecorder_mD63D11381D872AD072A0C176DD72E9DCD1A18273 (void);
// 0x00000528 System.Void NatSuite.Recorders.Internal.Bridge::FrameSize(System.IntPtr,System.Int32&,System.Int32&)
extern void Bridge_FrameSize_m085AE9D1EB78FBFA20DE33D36866C18195F1EC2D (void);
// 0x00000529 System.Void NatSuite.Recorders.Internal.Bridge::CommitFrame(System.IntPtr,System.IntPtr,System.Int64)
extern void Bridge_CommitFrame_mB0B9821E7984177FD2A536A3F0F63337939C197B (void);
// 0x0000052A System.Void NatSuite.Recorders.Internal.Bridge::CommitSamples(System.IntPtr,System.Single[],System.Int32,System.Int64)
extern void Bridge_CommitSamples_mF543FBE6F6681F1B24C176423EEF45E45AE24750 (void);
// 0x0000052B System.Void NatSuite.Recorders.Internal.Bridge::FinishWriting(System.IntPtr)
extern void Bridge_FinishWriting_m46DEAE40768F2585AA1C9592D09D4FE15E6A2242 (void);
// 0x0000052C System.ValueTuple`2<System.Int32,System.Int32> NatSuite.Recorders.Internal.NativeRecorder::get_frameSize()
extern void NativeRecorder_get_frameSize_m6A2EEB68481BA1116A448E01DCC86FB0E52BCD52 (void);
// 0x0000052D System.Void NatSuite.Recorders.Internal.NativeRecorder::.ctor(System.Func`3<NatSuite.Recorders.Internal.Bridge/RecordingHandler,System.IntPtr,System.IntPtr>)
extern void NativeRecorder__ctor_m5A1D1E64B4134B06E6971A27B02C5A603737D319 (void);
// 0x0000052E System.Void NatSuite.Recorders.Internal.NativeRecorder::CommitFrame(T[],System.Int64)
// 0x0000052F System.Void NatSuite.Recorders.Internal.NativeRecorder::CommitFrame(System.IntPtr,System.Int64)
extern void NativeRecorder_CommitFrame_m24C7A6DFB0BCFE3AE7DEE1DBC501B77917572773 (void);
// 0x00000530 System.Void NatSuite.Recorders.Internal.NativeRecorder::CommitSamples(System.Single[],System.Int64)
extern void NativeRecorder_CommitSamples_m884990214FA2D83654CB2067148128BFAB23F54A (void);
// 0x00000531 System.Threading.Tasks.Task`1<System.String> NatSuite.Recorders.Internal.NativeRecorder::FinishWriting()
extern void NativeRecorder_FinishWriting_m2CC5461B4C949B310AE8B096816E81C13CC0A6A4 (void);
// 0x00000532 System.Void NatSuite.Recorders.Internal.NativeRecorder::OnRecording(System.IntPtr,System.IntPtr)
extern void NativeRecorder_OnRecording_m6E2D200EE07CE507951155CB24CC8107AFE8D13B (void);
// 0x00000533 System.String NatSuite.Recorders.Internal.Utility::GetPath(System.String)
extern void Utility_GetPath_m9311C5984418D9EEFB0C1ED622A14626FAE58C39 (void);
// 0x00000534 System.Void NatSuite.Recorders.Inputs.AudioInput::.ctor(NatSuite.Recorders.IMediaRecorder,NatSuite.Recorders.Clocks.IClock,UnityEngine.AudioListener)
extern void AudioInput__ctor_mA3FD59F98FE959EBB21E6C0D579BAB4663805122 (void);
// 0x00000535 System.Void NatSuite.Recorders.Inputs.AudioInput::.ctor(NatSuite.Recorders.IMediaRecorder,NatSuite.Recorders.Clocks.IClock,UnityEngine.AudioSource,System.Boolean)
extern void AudioInput__ctor_mAA11A2C5840D9129AF876501389E584F0D731725 (void);
// 0x00000536 System.Void NatSuite.Recorders.Inputs.AudioInput::Dispose()
extern void AudioInput_Dispose_mE1741B608B54087C1B26B3C12574D7DE4335FC79 (void);
// 0x00000537 System.Void NatSuite.Recorders.Inputs.AudioInput::.ctor(NatSuite.Recorders.IMediaRecorder,NatSuite.Recorders.Clocks.IClock,UnityEngine.GameObject,System.Boolean)
extern void AudioInput__ctor_m1E7A78E02D9B62BF0A53D7494A11EFA09A0A53DD (void);
// 0x00000538 System.Void NatSuite.Recorders.Inputs.AudioInput::OnSampleBuffer(System.Single[])
extern void AudioInput_OnSampleBuffer_m9FC2F8C4DB48899A9542D1938175CC8DDB3DDA3D (void);
// 0x00000539 System.Void NatSuite.Recorders.Inputs.CameraInput::.ctor(NatSuite.Recorders.IMediaRecorder,NatSuite.Recorders.Clocks.IClock,UnityEngine.Camera[])
extern void CameraInput__ctor_m4BEA8A9CD3DC78C65B73FAF449D39D0437B87BDF (void);
// 0x0000053A System.Void NatSuite.Recorders.Inputs.CameraInput::Dispose()
extern void CameraInput_Dispose_m6CF1EE3A4CB113950E8E15B3A5B4FCAFC84B6E5F (void);
// 0x0000053B System.Collections.IEnumerator NatSuite.Recorders.Inputs.CameraInput::OnFrame()
extern void CameraInput_OnFrame_mE39BF5A7C297AB119889F937CC639CA79614C3E1 (void);
// 0x0000053C System.Double NatSuite.Recorders.Clocks.FixedIntervalClock::get_interval()
extern void FixedIntervalClock_get_interval_m7629C2CDC1961FE469AD24EC047D3150AA6F50BB (void);
// 0x0000053D System.Void NatSuite.Recorders.Clocks.FixedIntervalClock::set_interval(System.Double)
extern void FixedIntervalClock_set_interval_m4E4B64B782129CB871EF55BDB7023A2D1D15E531 (void);
// 0x0000053E System.Int64 NatSuite.Recorders.Clocks.FixedIntervalClock::get_timestamp()
extern void FixedIntervalClock_get_timestamp_m950563A0D70E959160C27CCF7F760DE408CE13F4 (void);
// 0x0000053F System.Void NatSuite.Recorders.Clocks.FixedIntervalClock::.ctor(System.Int32,System.Boolean)
extern void FixedIntervalClock__ctor_m7448F63B6235668B8F4B682239500CA14F5FCE58 (void);
// 0x00000540 System.Void NatSuite.Recorders.Clocks.FixedIntervalClock::.ctor(System.Double,System.Boolean)
extern void FixedIntervalClock__ctor_m9D109F10A6215B3E24795FE5BC5F2709E2F703A7 (void);
// 0x00000541 System.Void NatSuite.Recorders.Clocks.FixedIntervalClock::Tick()
extern void FixedIntervalClock_Tick_m17067E2F79EE296B3DADA632973515686450E085 (void);
// 0x00000542 System.Int64 NatSuite.Recorders.Clocks.IClock::get_timestamp()
// 0x00000543 System.Int64 NatSuite.Recorders.Clocks.RealtimeClock::get_timestamp()
extern void RealtimeClock_get_timestamp_mB99369062BEDDF34284318C4A0F3CB49059C0C81 (void);
// 0x00000544 System.Boolean NatSuite.Recorders.Clocks.RealtimeClock::get_paused()
extern void RealtimeClock_get_paused_mA2DF19C0093B9113982809B0759FD67C0C531189 (void);
// 0x00000545 System.Void NatSuite.Recorders.Clocks.RealtimeClock::set_paused(System.Boolean)
extern void RealtimeClock_set_paused_m8FFEF530250DF267554F59DA87BF6F576E95AA15 (void);
// 0x00000546 System.Void NatSuite.Recorders.Clocks.RealtimeClock::.ctor()
extern void RealtimeClock__ctor_m020229BB99250085023A9D2B1354656862C937B5 (void);
// 0x00000547 System.Void NatSuite.Examples.Giffy::StartRecording()
extern void Giffy_StartRecording_m85B30F8AAB7611332CBCDF523445A0785FEBA41F (void);
// 0x00000548 System.Void NatSuite.Examples.Giffy::StopRecording()
extern void Giffy_StopRecording_m32BFD9C6A6A4BE5345EEF2A9A445A9B9C13F5EC5 (void);
// 0x00000549 System.Void NatSuite.Examples.Giffy::.ctor()
extern void Giffy__ctor_m72758AF6110585484B5DA7F5C9892B6611D847B0 (void);
// 0x0000054A System.Collections.IEnumerator NatSuite.Examples.ReplayCam::Start()
extern void ReplayCam_Start_mFE062B76895DCE7C9E9CBD3CFDE34A33855DEC89 (void);
// 0x0000054B System.Void NatSuite.Examples.ReplayCam::SetToWrite()
extern void ReplayCam_SetToWrite_mE349A2F35FE0E21184A6A9FBBEA078BB8F6EF045 (void);
// 0x0000054C System.Void NatSuite.Examples.ReplayCam::OnDestroy()
extern void ReplayCam_OnDestroy_mA747A2C64F7189EB9696A8ACFCCD2480986EB63E (void);
// 0x0000054D System.Void NatSuite.Examples.ReplayCam::StartRecording()
extern void ReplayCam_StartRecording_m0748785F004077E5690F3973DA07ECD80DC9CBA6 (void);
// 0x0000054E System.Void NatSuite.Examples.ReplayCam::FinishRecording()
extern void ReplayCam_FinishRecording_mEFD5EC691EFA02898A4308B8BF480B9A756456FA (void);
// 0x0000054F System.Void NatSuite.Examples.ReplayCam::StopRecording()
extern void ReplayCam_StopRecording_m84CAB6D17FF50FA7B730A58B0D95792C7358DAB5 (void);
// 0x00000550 System.Void NatSuite.Examples.ReplayCam::.ctor()
extern void ReplayCam__ctor_m6BA6E989AEE2294EFDFBA2C7313441030DEC8019 (void);
// 0x00000551 UnityEngine.WebCamTexture NatSuite.Examples.Components.CameraPreview::get_cameraTexture()
extern void CameraPreview_get_cameraTexture_m9BEDA6FA7C02BCFF7BC2618C4A8DAFC783A93DF2 (void);
// 0x00000552 System.Void NatSuite.Examples.Components.CameraPreview::set_cameraTexture(UnityEngine.WebCamTexture)
extern void CameraPreview_set_cameraTexture_m58D885DA978EEA3754899E1D750C8CF911778B08 (void);
// 0x00000553 System.Collections.IEnumerator NatSuite.Examples.Components.CameraPreview::Start()
extern void CameraPreview_Start_m2DA911FDB8E31958749F15562F46B193E2C5E1EF (void);
// 0x00000554 System.Void NatSuite.Examples.Components.CameraPreview::.ctor()
extern void CameraPreview__ctor_m5F6E72C6C0780A2FD0020884F8DD6642717BABC0 (void);
// 0x00000555 System.Boolean NatSuite.Examples.Components.CameraPreview::<Start>b__6_1()
extern void CameraPreview_U3CStartU3Eb__6_1_mE72436C867270099B3AE9FCDE9FE400FEA5AB02C (void);
// 0x00000556 System.Void NatSuite.Examples.Components.RecordButton::Start()
extern void RecordButton_Start_mB17D2C0A02D5C7BCA97CED224BA88627FB167572 (void);
// 0x00000557 System.Void NatSuite.Examples.Components.RecordButton::Reset()
extern void RecordButton_Reset_mE67AA7E8CDF5B27D3938D441DB2986AB68204F1C (void);
// 0x00000558 System.Void NatSuite.Examples.Components.RecordButton::UnityEngine.EventSystems.IPointerDownHandler.OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void RecordButton_UnityEngine_EventSystems_IPointerDownHandler_OnPointerDown_m98470DC813D8CE0E525BA5BF94633E7289C1FCEF (void);
// 0x00000559 System.Void NatSuite.Examples.Components.RecordButton::UnityEngine.EventSystems.IPointerUpHandler.OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void RecordButton_UnityEngine_EventSystems_IPointerUpHandler_OnPointerUp_m47DDA104F10D93EFA358ABDCE593E42E0A092EF8 (void);
// 0x0000055A System.Collections.IEnumerator NatSuite.Examples.Components.RecordButton::Countdown()
extern void RecordButton_Countdown_mC4F140B46C883162C87733942011BB476CF151C0 (void);
// 0x0000055B System.Void NatSuite.Examples.Components.RecordButton::.ctor()
extern void RecordButton__ctor_m4B6521954A3A7A08F6E2FD40C55D1990D0BBE9FA (void);
// 0x0000055C System.Void DentedPixel.LeanDummy::.ctor()
extern void LeanDummy__ctor_mAD9437EB3765999702C790EAC2DE9FEB4442E092 (void);
// 0x0000055D System.Void DentedPixel.LTExamples.PathBezier::OnEnable()
extern void PathBezier_OnEnable_m56E18FAFF5DFB0ED61856F90EAFEF6BDD116E2DD (void);
// 0x0000055E System.Void DentedPixel.LTExamples.PathBezier::Start()
extern void PathBezier_Start_mDAF11682189F6675D4FDD93D652A898A3347B50F (void);
// 0x0000055F System.Void DentedPixel.LTExamples.PathBezier::Update()
extern void PathBezier_Update_m3CC2AC4CACBB72CE7640DC38DFC4F19689EAB564 (void);
// 0x00000560 System.Void DentedPixel.LTExamples.PathBezier::OnDrawGizmos()
extern void PathBezier_OnDrawGizmos_m345FFC89DBF05FCF81AA56654D2764EB6199F256 (void);
// 0x00000561 System.Void DentedPixel.LTExamples.PathBezier::.ctor()
extern void PathBezier__ctor_mB9E6AE4BEEE953C926C93944334E5F099FC262CA (void);
// 0x00000562 System.Void DentedPixel.LTExamples.TestingUnitTests::Awake()
extern void TestingUnitTests_Awake_m3084D275E0537EAC8B54FE41937CF217BB332504 (void);
// 0x00000563 System.Void DentedPixel.LTExamples.TestingUnitTests::Start()
extern void TestingUnitTests_Start_mE8442EF073840E58B91FD7047EC0D8708D70D928 (void);
// 0x00000564 UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cubeNamed(System.String)
extern void TestingUnitTests_cubeNamed_m46762B53A8B1B8EC91E95B1B886E0BEDA7F59270 (void);
// 0x00000565 System.Collections.IEnumerator DentedPixel.LTExamples.TestingUnitTests::timeBasedTesting()
extern void TestingUnitTests_timeBasedTesting_m5510CA65F550F87AAD701A457ADAF778B46B5758 (void);
// 0x00000566 System.Collections.IEnumerator DentedPixel.LTExamples.TestingUnitTests::lotsOfCancels()
extern void TestingUnitTests_lotsOfCancels_mF5CDB314DC950F82FF777A507914DAA972E57D08 (void);
// 0x00000567 System.Collections.IEnumerator DentedPixel.LTExamples.TestingUnitTests::pauseTimeNow()
extern void TestingUnitTests_pauseTimeNow_m038EABEB7FE1D5E01BAD36F26EDAD62253864249 (void);
// 0x00000568 System.Void DentedPixel.LTExamples.TestingUnitTests::rotateRepeatFinished()
extern void TestingUnitTests_rotateRepeatFinished_m75177B46DFB956A9AC0870AC6E1E767A09E41A65 (void);
// 0x00000569 System.Void DentedPixel.LTExamples.TestingUnitTests::rotateRepeatAllFinished()
extern void TestingUnitTests_rotateRepeatAllFinished_mE2C3707783D21F0E24E390907EC41AAD46537D53 (void);
// 0x0000056A System.Void DentedPixel.LTExamples.TestingUnitTests::eventGameObjectCalled(LTEvent)
extern void TestingUnitTests_eventGameObjectCalled_m696EE506C9313CDC2955B76C394C7D8DBAFA10D6 (void);
// 0x0000056B System.Void DentedPixel.LTExamples.TestingUnitTests::eventGeneralCalled(LTEvent)
extern void TestingUnitTests_eventGeneralCalled_m2ACEC83AC54AF7D79C1BFB26F9D63F2201886540 (void);
// 0x0000056C System.Void DentedPixel.LTExamples.TestingUnitTests::.ctor()
extern void TestingUnitTests__ctor_m1551B3D3BCD78DA068595A4F9CA1934061711DA2 (void);
// 0x0000056D System.Void DentedPixel.LTExamples.TestingUnitTests::<lotsOfCancels>b__25_0()
extern void TestingUnitTests_U3ClotsOfCancelsU3Eb__25_0_m22BEE01EA5BDFFF8A1DBF6DB397911D91B74FABA (void);
// 0x0000056E System.Void DentedPixel.LTExamples.TestingUnitTests::<pauseTimeNow>b__26_1()
extern void TestingUnitTests_U3CpauseTimeNowU3Eb__26_1_m4F3F229D6D22317DC30FF68B332B22A3CB6CFE68 (void);
// 0x0000056F System.Int32 NLayer.IMpegFrame::get_SampleRate()
// 0x00000570 System.Int32 NLayer.IMpegFrame::get_SampleRateIndex()
// 0x00000571 System.Int32 NLayer.IMpegFrame::get_FrameLength()
// 0x00000572 System.Int32 NLayer.IMpegFrame::get_BitRate()
// 0x00000573 NLayer.MpegVersion NLayer.IMpegFrame::get_Version()
// 0x00000574 NLayer.MpegLayer NLayer.IMpegFrame::get_Layer()
// 0x00000575 NLayer.MpegChannelMode NLayer.IMpegFrame::get_ChannelMode()
// 0x00000576 System.Int32 NLayer.IMpegFrame::get_ChannelModeExtension()
// 0x00000577 System.Int32 NLayer.IMpegFrame::get_SampleCount()
// 0x00000578 System.Int32 NLayer.IMpegFrame::get_BitRateIndex()
// 0x00000579 System.Boolean NLayer.IMpegFrame::get_IsCopyrighted()
// 0x0000057A System.Boolean NLayer.IMpegFrame::get_HasCrc()
// 0x0000057B System.Boolean NLayer.IMpegFrame::get_IsCorrupted()
// 0x0000057C System.Void NLayer.IMpegFrame::Reset()
// 0x0000057D System.Int32 NLayer.IMpegFrame::ReadBits(System.Int32)
// 0x0000057E System.Void NLayer.MpegFile::.ctor(System.String)
extern void MpegFile__ctor_mFB15E79DE265EEC45B668518410A6FEABBC06028 (void);
// 0x0000057F System.Void NLayer.MpegFile::.ctor(System.IO.Stream)
extern void MpegFile__ctor_mA74201D73EA2201136F5D7CB1DC439F04448431C (void);
// 0x00000580 System.Void NLayer.MpegFile::Init(System.IO.Stream,System.Boolean)
extern void MpegFile_Init_m6FBBC8ACD222EC8B750B52059D82824F12B20541 (void);
// 0x00000581 System.Void NLayer.MpegFile::Dispose()
extern void MpegFile_Dispose_mFEA66175E3182F36DE298BF10C21953FAC1201EA (void);
// 0x00000582 System.Int32 NLayer.MpegFile::get_SampleRate()
extern void MpegFile_get_SampleRate_m8B5D45191008ED6EAD554DFC6B2286935468A3F1 (void);
// 0x00000583 System.Int32 NLayer.MpegFile::get_Channels()
extern void MpegFile_get_Channels_m585D4CE88E9981B196394E711792A045EFDDF3EE (void);
// 0x00000584 System.Boolean NLayer.MpegFile::get_CanSeek()
extern void MpegFile_get_CanSeek_m33A9736A5B4A99E6B75965767B1B7682ED8B4C80 (void);
// 0x00000585 System.Int64 NLayer.MpegFile::get_Length()
extern void MpegFile_get_Length_mD157C580874BF121659678AA740ED7669D99AAB3 (void);
// 0x00000586 System.TimeSpan NLayer.MpegFile::get_Duration()
extern void MpegFile_get_Duration_mE98347B59C07FAE86C52F8A30BF73BDACD54C96A (void);
// 0x00000587 System.Int64 NLayer.MpegFile::get_Position()
extern void MpegFile_get_Position_mF5299BAEFEEEACC7433D2F195357CD1BC7751617 (void);
// 0x00000588 System.Void NLayer.MpegFile::set_Position(System.Int64)
extern void MpegFile_set_Position_mFC180DC8A925684483FAC01426BAB2AE441B693E (void);
// 0x00000589 System.TimeSpan NLayer.MpegFile::get_Time()
extern void MpegFile_get_Time_mECAE917124E89D00DB2DFDB92D8B551898920450 (void);
// 0x0000058A System.Void NLayer.MpegFile::set_Time(System.TimeSpan)
extern void MpegFile_set_Time_mEFB4D4FC0C086D782801BD9532331B277771063B (void);
// 0x0000058B System.Void NLayer.MpegFile::SetEQ(System.Single[])
extern void MpegFile_SetEQ_m05FEE93494C2E7F9817A87B764583A50CFDEE72F (void);
// 0x0000058C NLayer.StereoMode NLayer.MpegFile::get_StereoMode()
extern void MpegFile_get_StereoMode_m08794FC310CFB0DA19236B772EF1C4957A19B3E5 (void);
// 0x0000058D System.Void NLayer.MpegFile::set_StereoMode(NLayer.StereoMode)
extern void MpegFile_set_StereoMode_m7AADBDF4CF454A1295AEB0E50D01C50F2B674D3C (void);
// 0x0000058E System.Int32 NLayer.MpegFile::ReadSamples(System.Byte[],System.Int32,System.Int32)
extern void MpegFile_ReadSamples_mEB5411FDE75024631E0E50BF18B14AB98E756370 (void);
// 0x0000058F System.Int32 NLayer.MpegFile::ReadSamples(System.Single[],System.Int32,System.Int32)
extern void MpegFile_ReadSamples_m198848186BC5B41FC179B69035B1E8E4D00DBBBE (void);
// 0x00000590 System.Int32 NLayer.MpegFile::ReadSamplesImpl(System.Array,System.Int32,System.Int32)
extern void MpegFile_ReadSamplesImpl_mBCCB01C47CF5EA05A5F60F7D662C2EA28F481F2E (void);
// 0x00000591 System.Void NLayer.MpegFrameDecoder::.ctor()
extern void MpegFrameDecoder__ctor_mBB627C81E59763B314F303391EBFB3B0BEBE9288 (void);
// 0x00000592 System.Void NLayer.MpegFrameDecoder::SetEQ(System.Single[])
extern void MpegFrameDecoder_SetEQ_mB5BCD4538E3168ABB562663B24B1CDEAB01C8233 (void);
// 0x00000593 NLayer.StereoMode NLayer.MpegFrameDecoder::get_StereoMode()
extern void MpegFrameDecoder_get_StereoMode_mB5F3AA3369522D108193283455B38361B4CA6D5A (void);
// 0x00000594 System.Void NLayer.MpegFrameDecoder::set_StereoMode(NLayer.StereoMode)
extern void MpegFrameDecoder_set_StereoMode_mFF9AEB280D043F79C0CDF71F3CDFD46249833C8D (void);
// 0x00000595 System.Int32 NLayer.MpegFrameDecoder::DecodeFrame(NLayer.IMpegFrame,System.Byte[],System.Int32)
extern void MpegFrameDecoder_DecodeFrame_m3A78C8BCBB40C95CA9B1FEDC0D462AF4651FA1AC (void);
// 0x00000596 System.Int32 NLayer.MpegFrameDecoder::DecodeFrame(NLayer.IMpegFrame,System.Single[],System.Int32)
extern void MpegFrameDecoder_DecodeFrame_m1EE49D1829C38E005B233CE734218012C6FC3AC7 (void);
// 0x00000597 System.Int32 NLayer.MpegFrameDecoder::DecodeFrameImpl(NLayer.IMpegFrame,System.Array,System.Int32)
extern void MpegFrameDecoder_DecodeFrameImpl_m105DAD491B3AAAF670334277440254A2268A3A0E (void);
// 0x00000598 System.Void NLayer.MpegFrameDecoder::Reset()
extern void MpegFrameDecoder_Reset_m7A3309B349B79B7A91067DA43E5AF23127A95BC2 (void);
// 0x00000599 System.Int32 NLayer.Decoder.BitReservoir::GetSlots(NLayer.IMpegFrame)
extern void BitReservoir_GetSlots_m032F3C5A05CB2F1F15E3E878358E1C7B0A46F8D4 (void);
// 0x0000059A System.Boolean NLayer.Decoder.BitReservoir::AddBits(NLayer.IMpegFrame,System.Int32)
extern void BitReservoir_AddBits_m469E338C830BB8C3010A68DC57423BBD2783CDF7 (void);
// 0x0000059B System.Int32 NLayer.Decoder.BitReservoir::GetBits(System.Int32)
extern void BitReservoir_GetBits_m073222CC5C65DAE676E64DADF70BBEACA3349005 (void);
// 0x0000059C System.Int32 NLayer.Decoder.BitReservoir::Get1Bit()
extern void BitReservoir_Get1Bit_m436BEBD334302CB6F69955143400FE328C601045 (void);
// 0x0000059D System.Int32 NLayer.Decoder.BitReservoir::TryPeekBits(System.Int32,System.Int32&)
extern void BitReservoir_TryPeekBits_m182CF2486C83737707AE35845F7C5B41B5C08D94 (void);
// 0x0000059E System.Int32 NLayer.Decoder.BitReservoir::get_BitsAvailable()
extern void BitReservoir_get_BitsAvailable_m1B4A1627734B83C5A7C49DE8736B774D71648CCD (void);
// 0x0000059F System.Int64 NLayer.Decoder.BitReservoir::get_BitsRead()
extern void BitReservoir_get_BitsRead_mF0BE03D1C23C59AE833A9AAFDFF47E708C9B41DA (void);
// 0x000005A0 System.Void NLayer.Decoder.BitReservoir::SkipBits(System.Int32)
extern void BitReservoir_SkipBits_m065D078B44A93BF0B491D5D0A3185E362ABA9A9B (void);
// 0x000005A1 System.Void NLayer.Decoder.BitReservoir::RewindBits(System.Int32)
extern void BitReservoir_RewindBits_m8C790EAC08D378EE9CBDBB4CF666A2BB02CE80B8 (void);
// 0x000005A2 System.Void NLayer.Decoder.BitReservoir::FlushBits()
extern void BitReservoir_FlushBits_m17CD6AAE51599A6C0C30C9D51BCE0D41F5D4FB65 (void);
// 0x000005A3 System.Void NLayer.Decoder.BitReservoir::Reset()
extern void BitReservoir_Reset_mE80EC3ADA04AFD7044A72D817BE506DEFD8E9E7B (void);
// 0x000005A4 System.Void NLayer.Decoder.BitReservoir::.ctor()
extern void BitReservoir__ctor_m1F82DB4407FE42E0646CAB925DC20640B9A2838F (void);
// 0x000005A5 System.Int32 NLayer.Decoder.FrameBase::get_TotalAllocation()
extern void FrameBase_get_TotalAllocation_m69162765AF5EB3937E1A020E1B0BEB112C2BC0B1 (void);
// 0x000005A6 System.Int64 NLayer.Decoder.FrameBase::get_Offset()
extern void FrameBase_get_Offset_m54677B6390075870E56C050F5742F9F88A70FF97 (void);
// 0x000005A7 System.Void NLayer.Decoder.FrameBase::set_Offset(System.Int64)
extern void FrameBase_set_Offset_m8C60A5D36824695685A6B02D43DF37887AF4D540 (void);
// 0x000005A8 System.Int32 NLayer.Decoder.FrameBase::get_Length()
extern void FrameBase_get_Length_mD2AC76CBBCD49FC2501B8B6FBB2F15D4D500511F (void);
// 0x000005A9 System.Void NLayer.Decoder.FrameBase::set_Length(System.Int32)
extern void FrameBase_set_Length_mE9B332C5BD797F4C51D98A02E8EBAA1E78312E53 (void);
// 0x000005AA System.Void NLayer.Decoder.FrameBase::.ctor()
extern void FrameBase__ctor_m5456D68301AFD7E15DD8D3E78BCE0C2BC1544589 (void);
// 0x000005AB System.Boolean NLayer.Decoder.FrameBase::Validate(System.Int64,NLayer.Decoder.MpegStreamReader)
extern void FrameBase_Validate_m5A6B9CDE4E29D240D72C78275BB09EA561061655 (void);
// 0x000005AC System.Int32 NLayer.Decoder.FrameBase::Read(System.Int32,System.Byte[])
extern void FrameBase_Read_mC048B35C2BFFA1015521CE6858AE7CBEE68B26C4 (void);
// 0x000005AD System.Int32 NLayer.Decoder.FrameBase::Read(System.Int32,System.Byte[],System.Int32,System.Int32)
extern void FrameBase_Read_m84D84692DAC1C64E2517ADB8F2B4D31F16ADACBE (void);
// 0x000005AE System.Int32 NLayer.Decoder.FrameBase::ReadByte(System.Int32)
extern void FrameBase_ReadByte_m28F5B6B19F978875FB35AD2B37CAF343F78703B2 (void);
// 0x000005AF System.Int32 NLayer.Decoder.FrameBase::Validate()
// 0x000005B0 System.Void NLayer.Decoder.FrameBase::SaveBuffer()
extern void FrameBase_SaveBuffer_m3E6525C8A71075D51F59199E01DBEB9A3B194710 (void);
// 0x000005B1 System.Void NLayer.Decoder.FrameBase::ClearBuffer()
extern void FrameBase_ClearBuffer_m3BB469BEE3915D2C11D7D8E963F34E5E03FFACBB (void);
// 0x000005B2 System.Void NLayer.Decoder.FrameBase::Parse()
extern void FrameBase_Parse_m7E4B608486875C9FF3E43DD18D1C55DA5F1D2E2A (void);
// 0x000005B3 System.Void NLayer.Decoder.FrameBase::.cctor()
extern void FrameBase__cctor_m5F6F31D86F9500C934F72C8859C9E1FD30642DD1 (void);
// 0x000005B4 System.Void NLayer.Decoder.Huffman::.cctor()
extern void Huffman__cctor_m7B8DADA4ADC76C59176BBFAA1A1CBBB33AE7CB7C (void);
// 0x000005B5 System.Void NLayer.Decoder.Huffman::Decode(NLayer.Decoder.BitReservoir,System.Int32,System.Single&,System.Single&)
extern void Huffman_Decode_m2623F7274FC62D2E4697917389CC6BB285CF0040 (void);
// 0x000005B6 System.Void NLayer.Decoder.Huffman::Decode(NLayer.Decoder.BitReservoir,System.Int32,System.Single&,System.Single&,System.Single&,System.Single&)
extern void Huffman_Decode_m5028306C222D2728292C149B377A236AEE4C0D71 (void);
// 0x000005B7 System.Byte NLayer.Decoder.Huffman::DecodeSymbol(NLayer.Decoder.BitReservoir,System.Int32)
extern void Huffman_DecodeSymbol_mA8CD271AC7CBC35DEE2D932F7536242A1CACAC8D (void);
// 0x000005B8 NLayer.Decoder.Huffman/HuffmanListNode NLayer.Decoder.Huffman::GetNode(System.Int32,System.Int32&)
extern void Huffman_GetNode_m6D51CFE61384D7E01D76DFAC14E3B665B084A1C1 (void);
// 0x000005B9 NLayer.Decoder.Huffman/HuffmanListNode NLayer.Decoder.Huffman::InitTable(System.Byte[0...,0...],System.Int32&)
extern void Huffman_InitTable_mC68008DE3BEEBE1CE09244DD311ADB28F18ED8FF (void);
// 0x000005BA System.Int32 NLayer.Decoder.Huffman::FindPreviousNode(System.Byte[0...,0...],System.Int32,System.Int32&)
extern void Huffman_FindPreviousNode_m3FB37AC184D34CD1B5C8BD421D6169C3CDFBF80E (void);
// 0x000005BB NLayer.Decoder.Huffman/HuffmanListNode NLayer.Decoder.Huffman::BuildLinkedList(System.Collections.Generic.List`1<System.Byte>,System.Collections.Generic.List`1<System.Int32>,System.Collections.Generic.List`1<System.Int32>,System.Int32&)
extern void Huffman_BuildLinkedList_mBF31511EB952039A4AC9B104BD9A67DFD8AFFB20 (void);
// 0x000005BC System.Void NLayer.Decoder.Huffman::.ctor()
extern void Huffman__ctor_mF52C1F71E017B1311C66D92BE2A5A80E07DA45B0 (void);
// 0x000005BD NLayer.Decoder.ID3Frame NLayer.Decoder.ID3Frame::TrySync(System.UInt32)
extern void ID3Frame_TrySync_mC379E95EED0416B260AC9D5D922A1CD84ED92C03 (void);
// 0x000005BE System.Void NLayer.Decoder.ID3Frame::.ctor()
extern void ID3Frame__ctor_m174991CB995510152626FF5ABF604B90F90CB744 (void);
// 0x000005BF System.Int32 NLayer.Decoder.ID3Frame::Validate()
extern void ID3Frame_Validate_mD596BDA5C213849B2869070ACD4669A624661201 (void);
// 0x000005C0 System.Void NLayer.Decoder.ID3Frame::Parse()
extern void ID3Frame_Parse_m1D4CCCB256CF9D70418AB69C84D2528E76F49DF2 (void);
// 0x000005C1 System.Void NLayer.Decoder.ID3Frame::ParseV1(System.Int32)
extern void ID3Frame_ParseV1_mD8F1CC072DF3477E0BC9A9D2B206AE4AF8179807 (void);
// 0x000005C2 System.Void NLayer.Decoder.ID3Frame::ParseV1Enh()
extern void ID3Frame_ParseV1Enh_m13894656FD36D16CBB779B5D2C69F9F5D343E2A8 (void);
// 0x000005C3 System.Void NLayer.Decoder.ID3Frame::ParseV2()
extern void ID3Frame_ParseV2_mB3D9933FFEDD21B76F8DD6FE5AEBC4DEA59B7EEF (void);
// 0x000005C4 System.Int32 NLayer.Decoder.ID3Frame::get_Version()
extern void ID3Frame_get_Version_mC1FC4F976CBEDA52A6DE2A793354A8EA24211C72 (void);
// 0x000005C5 System.Void NLayer.Decoder.ID3Frame::Merge(NLayer.Decoder.ID3Frame)
extern void ID3Frame_Merge_mAA10D3DCE6270DEED43AB935674EBF1A99E2D50E (void);
// 0x000005C6 System.Void NLayer.Decoder.LayerDecoderBase::.ctor()
extern void LayerDecoderBase__ctor_m2FB0CF1FAC0F102CE987253F730D2FAAAE7B570D (void);
// 0x000005C7 System.Int32 NLayer.Decoder.LayerDecoderBase::DecodeFrame(NLayer.IMpegFrame,System.Single[],System.Single[])
// 0x000005C8 System.Void NLayer.Decoder.LayerDecoderBase::SetEQ(System.Single[])
extern void LayerDecoderBase_SetEQ_m7F2B584CC71FB80C4B4A692A6A3246978A5B392A (void);
// 0x000005C9 NLayer.StereoMode NLayer.Decoder.LayerDecoderBase::get_StereoMode()
extern void LayerDecoderBase_get_StereoMode_m3AC9AF5A2C4FD8402C2C584B1CE1F1315993E13E (void);
// 0x000005CA System.Void NLayer.Decoder.LayerDecoderBase::set_StereoMode(NLayer.StereoMode)
extern void LayerDecoderBase_set_StereoMode_m7786DF542234CA67311C4B833CD03A17EF5867A7 (void);
// 0x000005CB System.Void NLayer.Decoder.LayerDecoderBase::ResetForSeek()
extern void LayerDecoderBase_ResetForSeek_m166B7A0865B319168B5A4CD2811E5B4DBA9C4EE0 (void);
// 0x000005CC System.Void NLayer.Decoder.LayerDecoderBase::InversePolyPhase(System.Int32,System.Single[])
extern void LayerDecoderBase_InversePolyPhase_mFA06A4667AA178D8288FDAACA437929F0E121431 (void);
// 0x000005CD System.Void NLayer.Decoder.LayerDecoderBase::GetBufAndOffset(System.Int32,System.Single[]&,System.Int32&)
extern void LayerDecoderBase_GetBufAndOffset_m3CD9EFF14B4A31BFBA7DD3ACF7F4B9060EC3F066 (void);
// 0x000005CE System.Void NLayer.Decoder.LayerDecoderBase::DCT32(System.Single[],System.Single[],System.Int32)
extern void LayerDecoderBase_DCT32_m541F06FEEF44FD7B0394BE4C4A00122E5340A4BB (void);
// 0x000005CF System.Void NLayer.Decoder.LayerDecoderBase::DCT16(System.Single[],System.Single[])
extern void LayerDecoderBase_DCT16_m646DCD43DC6C4A1CD226CBBF0A16EEDE19480B0E (void);
// 0x000005D0 System.Void NLayer.Decoder.LayerDecoderBase::DCT8(System.Single[],System.Single[])
extern void LayerDecoderBase_DCT8_m516633F85CFB259835C87C4F206FA2E8EAF642FD (void);
// 0x000005D1 System.Void NLayer.Decoder.LayerDecoderBase::BuildUVec(System.Single[],System.Single[],System.Int32)
extern void LayerDecoderBase_BuildUVec_m0B4A80BFA03758E2A04D1C364EAEAE3219134B65 (void);
// 0x000005D2 System.Void NLayer.Decoder.LayerDecoderBase::DewindowOutput(System.Single[],System.Single[])
extern void LayerDecoderBase_DewindowOutput_m9350FE27C82AC48AD91A37497A174261EE9A8AED (void);
// 0x000005D3 System.Void NLayer.Decoder.LayerDecoderBase::.cctor()
extern void LayerDecoderBase__cctor_mC13C7CC12A58AFDC8B31BDB87E25C85AD0E8696D (void);
// 0x000005D4 System.Boolean NLayer.Decoder.LayerIDecoder::GetCRC(NLayer.Decoder.MpegFrame,System.UInt32&)
extern void LayerIDecoder_GetCRC_m80B901A65C78F8C5768918D0942BA839FC96F4AA (void);
// 0x000005D5 System.Void NLayer.Decoder.LayerIDecoder::.ctor()
extern void LayerIDecoder__ctor_mD05629CE4CFDC7A23A8691FDA86096C69F99ABCB (void);
// 0x000005D6 System.Int32[] NLayer.Decoder.LayerIDecoder::GetRateTable(NLayer.IMpegFrame)
extern void LayerIDecoder_GetRateTable_mC290B3CDBB4592A95EC9A5CF7AC5AC2D9C9E345A (void);
// 0x000005D7 System.Void NLayer.Decoder.LayerIDecoder::ReadScaleFactorSelection(NLayer.IMpegFrame,System.Int32[][],System.Int32)
extern void LayerIDecoder_ReadScaleFactorSelection_m6ABB789F4243BB767B919F595AF11C2211B08EF7 (void);
// 0x000005D8 System.Void NLayer.Decoder.LayerIDecoder::.cctor()
extern void LayerIDecoder__cctor_m55151DC556480CFE8702C5F7BA80CED2A6B164D2 (void);
// 0x000005D9 System.Boolean NLayer.Decoder.LayerIIDecoder::GetCRC(NLayer.Decoder.MpegFrame,System.UInt32&)
extern void LayerIIDecoder_GetCRC_m7972A57A5A9A8D3ABFAD20CBAFD9737075DE66A4 (void);
// 0x000005DA System.Int32[] NLayer.Decoder.LayerIIDecoder::SelectTable(NLayer.IMpegFrame)
extern void LayerIIDecoder_SelectTable_m1D9D5BB876FB10F6B439F9988AF1D40BE6CD82E2 (void);
// 0x000005DB System.Void NLayer.Decoder.LayerIIDecoder::.ctor()
extern void LayerIIDecoder__ctor_m1C4763F3A5737B2E169E27F2ADFEDD2EBCD8687A (void);
// 0x000005DC System.Int32[] NLayer.Decoder.LayerIIDecoder::GetRateTable(NLayer.IMpegFrame)
extern void LayerIIDecoder_GetRateTable_m36AA219FB08EC9E85481597E390B155EC8D354B7 (void);
// 0x000005DD System.Void NLayer.Decoder.LayerIIDecoder::ReadScaleFactorSelection(NLayer.IMpegFrame,System.Int32[][],System.Int32)
extern void LayerIIDecoder_ReadScaleFactorSelection_mB0218F443E1C6ADF336B7AD4BC7EAD8E875EFB05 (void);
// 0x000005DE System.Void NLayer.Decoder.LayerIIDecoder::.cctor()
extern void LayerIIDecoder__cctor_mAFF960A22F720E245BFD414131B8532C8E7FBAD6 (void);
// 0x000005DF System.Boolean NLayer.Decoder.LayerIIDecoderBase::GetCRC(NLayer.Decoder.MpegFrame,System.Int32[],System.Int32[][],System.Boolean,System.UInt32&)
extern void LayerIIDecoderBase_GetCRC_m3A637A3508AD6415B0B12BDAC4B87103AC8A9A4B (void);
// 0x000005E0 System.Void NLayer.Decoder.LayerIIDecoderBase::.ctor(System.Int32[][],System.Int32)
extern void LayerIIDecoderBase__ctor_m2557B21C6E19BCA8558BA3C49DF4C891DB78CED2 (void);
// 0x000005E1 System.Int32 NLayer.Decoder.LayerIIDecoderBase::DecodeFrame(NLayer.IMpegFrame,System.Single[],System.Single[])
extern void LayerIIDecoderBase_DecodeFrame_m5DEF4ADB1631DD9D1BB317730BC871CA0B86AFE7 (void);
// 0x000005E2 System.Void NLayer.Decoder.LayerIIDecoderBase::InitFrame(NLayer.IMpegFrame)
extern void LayerIIDecoderBase_InitFrame_m01F24B652652AE4C68EDEA5D6CCAA68A028442E3 (void);
// 0x000005E3 System.Int32[] NLayer.Decoder.LayerIIDecoderBase::GetRateTable(NLayer.IMpegFrame)
// 0x000005E4 System.Void NLayer.Decoder.LayerIIDecoderBase::ReadAllocation(NLayer.IMpegFrame,System.Int32[])
extern void LayerIIDecoderBase_ReadAllocation_mE6056ABA874F51F23D8AD23E47CCF95C5E78A178 (void);
// 0x000005E5 System.Void NLayer.Decoder.LayerIIDecoderBase::ReadScaleFactorSelection(NLayer.IMpegFrame,System.Int32[][],System.Int32)
// 0x000005E6 System.Void NLayer.Decoder.LayerIIDecoderBase::ReadScaleFactors(NLayer.IMpegFrame)
extern void LayerIIDecoderBase_ReadScaleFactors_m14C55D5ADC0013B53BC9A029C076A926EC580B97 (void);
// 0x000005E7 System.Void NLayer.Decoder.LayerIIDecoderBase::ReadSamples(NLayer.IMpegFrame)
extern void LayerIIDecoderBase_ReadSamples_mE5B1D05DD0739E17E19F1ADE005A310CC8C0DF44 (void);
// 0x000005E8 System.Int32 NLayer.Decoder.LayerIIDecoderBase::DecodeSamples(System.Single[],System.Single[])
extern void LayerIIDecoderBase_DecodeSamples_mCF93F19B426D2B7E627C49697899A412DF161E03 (void);
// 0x000005E9 System.Void NLayer.Decoder.LayerIIDecoderBase::.cctor()
extern void LayerIIDecoderBase__cctor_m33058A88A2ECE907E44CABCB97CCC2CDBE9C9C42 (void);
// 0x000005EA System.Boolean NLayer.Decoder.LayerIIIDecoder::GetCRC(NLayer.Decoder.MpegFrame,System.UInt32&)
extern void LayerIIIDecoder_GetCRC_mA762E60BB5BB46E82333EB7F6E30B0EC7A114125 (void);
// 0x000005EB System.Void NLayer.Decoder.LayerIIIDecoder::.ctor()
extern void LayerIIIDecoder__ctor_m59D94A35985438D67A24703D720E269BBFF34DF0 (void);
// 0x000005EC System.Int32 NLayer.Decoder.LayerIIIDecoder::DecodeFrame(NLayer.IMpegFrame,System.Single[],System.Single[])
extern void LayerIIIDecoder_DecodeFrame_m8CD38BDA09E14D557D86E9C67ECFB02763959DB3 (void);
// 0x000005ED System.Void NLayer.Decoder.LayerIIIDecoder::ResetForSeek()
extern void LayerIIIDecoder_ResetForSeek_m7C3A815B46F765E41AD3212F352BEE3592CCEB28 (void);
// 0x000005EE System.Void NLayer.Decoder.LayerIIIDecoder::ReadSideInfo(NLayer.IMpegFrame)
extern void LayerIIIDecoder_ReadSideInfo_mAAD0E009FA8C4DE5B90F8E76504BDEFF98F8FE5E (void);
// 0x000005EF System.Void NLayer.Decoder.LayerIIIDecoder::PrepTables(NLayer.IMpegFrame)
extern void LayerIIIDecoder_PrepTables_m6767FA3E6D63C3FCE93242E95D167971555FE3C5 (void);
// 0x000005F0 System.Int32 NLayer.Decoder.LayerIIIDecoder::ReadScalefactors(System.Int32,System.Int32)
extern void LayerIIIDecoder_ReadScalefactors_m9BBD0DBC9C553F2688F08CC8FF43952CC521601F (void);
// 0x000005F1 System.Int32 NLayer.Decoder.LayerIIIDecoder::ReadLsfScalefactors(System.Int32,System.Int32,System.Int32)
extern void LayerIIIDecoder_ReadLsfScalefactors_mB5BD006E2794CE1DC0806F98F3BC36CDE0F609CF (void);
// 0x000005F2 System.Void NLayer.Decoder.LayerIIIDecoder::ReadSamples(System.Int32,System.Int32,System.Int32)
extern void LayerIIIDecoder_ReadSamples_m56543FC403FEED6DD08A164DD75EB77ABEA02AF4 (void);
// 0x000005F3 System.Single NLayer.Decoder.LayerIIIDecoder::Dequantize(System.Int32,System.Single,System.Int32,System.Int32)
extern void LayerIIIDecoder_Dequantize_mF11D342C277919587F80731398F5CA8DA422453E (void);
// 0x000005F4 System.Void NLayer.Decoder.LayerIIIDecoder::Stereo(NLayer.MpegChannelMode,System.Int32,System.Int32,System.Boolean)
extern void LayerIIIDecoder_Stereo_m9CA43FC7865CC4A4CF4FEFFC97530DA2E322EE43 (void);
// 0x000005F5 System.Void NLayer.Decoder.LayerIIIDecoder::ApplyIStereo(System.Int32,System.Int32,System.Int32)
extern void LayerIIIDecoder_ApplyIStereo_mCB1BDCBE6F3F0D5375E2597041B2820A9F22A163 (void);
// 0x000005F6 System.Void NLayer.Decoder.LayerIIIDecoder::ApplyLsfIStereo(System.Int32,System.Int32,System.Int32,System.Int32)
extern void LayerIIIDecoder_ApplyLsfIStereo_mCE7AB8867EF652C4EBD566199B357A80C6A7A90F (void);
// 0x000005F7 System.Void NLayer.Decoder.LayerIIIDecoder::ApplyMidSide(System.Int32,System.Int32)
extern void LayerIIIDecoder_ApplyMidSide_m4D50C6868AE76ED51A00F6B50697B6926E2B76F1 (void);
// 0x000005F8 System.Void NLayer.Decoder.LayerIIIDecoder::ApplyFullStereo(System.Int32,System.Int32)
extern void LayerIIIDecoder_ApplyFullStereo_m826B48DAB2E1346FC04C137E1BE6D233C3F85FBD (void);
// 0x000005F9 System.Void NLayer.Decoder.LayerIIIDecoder::Reorder(System.Single[],System.Boolean)
extern void LayerIIIDecoder_Reorder_mB166BBEF760D6B3094C2A77F9A2629FD0A216F37 (void);
// 0x000005FA System.Void NLayer.Decoder.LayerIIIDecoder::AntiAlias(System.Single[],System.Boolean)
extern void LayerIIIDecoder_AntiAlias_mC07D386CA37EA15BF91851B850E2F04D10E03214 (void);
// 0x000005FB System.Void NLayer.Decoder.LayerIIIDecoder::FrequencyInversion(System.Single[])
extern void LayerIIIDecoder_FrequencyInversion_mFAFC6B768E0DE2D49323F5BA526A00213A534F9F (void);
// 0x000005FC System.Void NLayer.Decoder.LayerIIIDecoder::InversePolyphase(System.Single[],System.Int32,System.Int32,System.Single[])
extern void LayerIIIDecoder_InversePolyphase_mE56EF75768AA7F1BC11DA76D4ADC5F12D6987B35 (void);
// 0x000005FD System.Void NLayer.Decoder.LayerIIIDecoder::.cctor()
extern void LayerIIIDecoder__cctor_m79FC606E25AFD6182ED42B1A4A83282DBC7F8FF8 (void);
// 0x000005FE NLayer.Decoder.MpegFrame NLayer.Decoder.MpegFrame::TrySync(System.UInt32)
extern void MpegFrame_TrySync_mA2F9E0B424917911C69D2611B772A36160EF617E (void);
// 0x000005FF System.Void NLayer.Decoder.MpegFrame::.ctor()
extern void MpegFrame__ctor_mB33368BBE38295360623B4BF7E532A44789B2DB1 (void);
// 0x00000600 System.Int32 NLayer.Decoder.MpegFrame::Validate()
extern void MpegFrame_Validate_mAB148AF751D3EBEB8DE9D88EDBB713A7268DF857 (void);
// 0x00000601 System.Int32 NLayer.Decoder.MpegFrame::GetSideDataSize()
extern void MpegFrame_GetSideDataSize_m7C3469AF367C78BD4DAC969BAE613331286BCF07 (void);
// 0x00000602 System.Boolean NLayer.Decoder.MpegFrame::ValidateCRC()
extern void MpegFrame_ValidateCRC_m7A0C132B8537DD26A1DBECC6D9FBDF02BB5B9C90 (void);
// 0x00000603 System.Void NLayer.Decoder.MpegFrame::UpdateCRC(System.Int32,System.Int32,System.UInt32&)
extern void MpegFrame_UpdateCRC_m39C4D5997725E8EA7519D9FC05CD7D58448A8F2F (void);
// 0x00000604 NLayer.Decoder.VBRInfo NLayer.Decoder.MpegFrame::ParseVBR()
extern void MpegFrame_ParseVBR_m9D53FB11B6CACEAF60931D38B5ABCF59E2E722F3 (void);
// 0x00000605 NLayer.Decoder.VBRInfo NLayer.Decoder.MpegFrame::ParseXing(System.Int32)
extern void MpegFrame_ParseXing_m137F01E11379BABA6AADBF7066E2AEE8EA420556 (void);
// 0x00000606 NLayer.Decoder.VBRInfo NLayer.Decoder.MpegFrame::ParseVBRI()
extern void MpegFrame_ParseVBRI_mDB620A8FFCF302DEA8C1E4D4A583062F0E18FD8E (void);
// 0x00000607 System.Int32 NLayer.Decoder.MpegFrame::get_FrameLength()
extern void MpegFrame_get_FrameLength_m8FCFF91C7E9773C2698C16F3E14457FACAA3CC75 (void);
// 0x00000608 NLayer.MpegVersion NLayer.Decoder.MpegFrame::get_Version()
extern void MpegFrame_get_Version_mECE3628C4BB434D01FDC41452F8BD96FFDDA3BAD (void);
// 0x00000609 NLayer.MpegLayer NLayer.Decoder.MpegFrame::get_Layer()
extern void MpegFrame_get_Layer_mAD5ADF3F8661CD1D96F40B81CB85A5E45FBF67A0 (void);
// 0x0000060A System.Boolean NLayer.Decoder.MpegFrame::get_HasCrc()
extern void MpegFrame_get_HasCrc_m08A3018F2B7BD63C982C8B7BE155FFFB1B425F08 (void);
// 0x0000060B System.Int32 NLayer.Decoder.MpegFrame::get_BitRate()
extern void MpegFrame_get_BitRate_mF34C7D6A4B02D779AE4019D9A625D136C26BDF5F (void);
// 0x0000060C System.Int32 NLayer.Decoder.MpegFrame::get_BitRateIndex()
extern void MpegFrame_get_BitRateIndex_m2005274EDD6777B29276B40C5B1653DF42024460 (void);
// 0x0000060D System.Int32 NLayer.Decoder.MpegFrame::get_SampleRate()
extern void MpegFrame_get_SampleRate_m9859E1E23F2415F99F49242D7A66A6864CB383E0 (void);
// 0x0000060E System.Int32 NLayer.Decoder.MpegFrame::get_SampleRateIndex()
extern void MpegFrame_get_SampleRateIndex_mC0A05DBAE0D69343BB88CAE7699ECA70292EAF41 (void);
// 0x0000060F System.Int32 NLayer.Decoder.MpegFrame::get_Padding()
extern void MpegFrame_get_Padding_mEF4771A1091820578F89528045835D8236F3ADB5 (void);
// 0x00000610 NLayer.MpegChannelMode NLayer.Decoder.MpegFrame::get_ChannelMode()
extern void MpegFrame_get_ChannelMode_m5B768659D14EF410CA3F4628A279DE67F5DC8A89 (void);
// 0x00000611 System.Int32 NLayer.Decoder.MpegFrame::get_ChannelModeExtension()
extern void MpegFrame_get_ChannelModeExtension_mC839FF648C9FB2F59C508FF3E0D43144DBF96F87 (void);
// 0x00000612 System.Int32 NLayer.Decoder.MpegFrame::get_Channels()
extern void MpegFrame_get_Channels_mB418127518D4303F6D2165ABF37D2D313D690694 (void);
// 0x00000613 System.Boolean NLayer.Decoder.MpegFrame::get_IsCopyrighted()
extern void MpegFrame_get_IsCopyrighted_m708F6FC8EC8B568A497CDF6CA2723A981B51C432 (void);
// 0x00000614 System.Boolean NLayer.Decoder.MpegFrame::get_IsOriginal()
extern void MpegFrame_get_IsOriginal_mCA21CC98194187A0A5F5963EAB85F7BC0F0455A7 (void);
// 0x00000615 System.Int32 NLayer.Decoder.MpegFrame::get_EmphasisMode()
extern void MpegFrame_get_EmphasisMode_m9CCF830D93B62FE7C91C24A0BFA33F0D33B5098E (void);
// 0x00000616 System.Boolean NLayer.Decoder.MpegFrame::get_IsCorrupted()
extern void MpegFrame_get_IsCorrupted_m4764C3C3F99CA4783DBEA62487A3ECA5F1C25864 (void);
// 0x00000617 System.Int32 NLayer.Decoder.MpegFrame::get_SampleCount()
extern void MpegFrame_get_SampleCount_m52B0FEA0229C858D4FD689F078DAF0FA58BF67D6 (void);
// 0x00000618 System.Int64 NLayer.Decoder.MpegFrame::get_SampleOffset()
extern void MpegFrame_get_SampleOffset_m442BDBD5267BDF53B477EC2988B4FD8F107E4E4B (void);
// 0x00000619 System.Void NLayer.Decoder.MpegFrame::set_SampleOffset(System.Int64)
extern void MpegFrame_set_SampleOffset_mB6F46B4BC78800421B3E5AA86B4FA1800FB0E052 (void);
// 0x0000061A System.Void NLayer.Decoder.MpegFrame::Reset()
extern void MpegFrame_Reset_m1075651C53C86F2AD8109A7F12858CDAFB52601D (void);
// 0x0000061B System.Int32 NLayer.Decoder.MpegFrame::ReadBits(System.Int32)
extern void MpegFrame_ReadBits_m0F1878B5C926592F3E962F05517485D0BE1B8936 (void);
// 0x0000061C System.Void NLayer.Decoder.MpegFrame::.cctor()
extern void MpegFrame__cctor_mBD3A9B4254CDCDE7782647D0E8B4D4B22444CBFF (void);
// 0x0000061D System.Void NLayer.Decoder.MpegStreamReader::.ctor(System.IO.Stream)
extern void MpegStreamReader__ctor_m9F788F233AE95B18E3EAF03832D42A7911D8B75A (void);
// 0x0000061E NLayer.Decoder.FrameBase NLayer.Decoder.MpegStreamReader::FindNextFrame()
extern void MpegStreamReader_FindNextFrame_m45477875ACD3088A1A33EB742CD047A90CB118D8 (void);
// 0x0000061F System.Int32 NLayer.Decoder.MpegStreamReader::Read(System.Int64,System.Byte[],System.Int32,System.Int32)
extern void MpegStreamReader_Read_mF7D592DCAD05FFF63F88C38DBE10B07F3884F520 (void);
// 0x00000620 System.Int32 NLayer.Decoder.MpegStreamReader::ReadByte(System.Int64)
extern void MpegStreamReader_ReadByte_m8C0A2B85ED50851EB8D643CCA0519D507401986B (void);
// 0x00000621 System.Void NLayer.Decoder.MpegStreamReader::DiscardThrough(System.Int64,System.Boolean)
extern void MpegStreamReader_DiscardThrough_m5F66D5C58E38E09E7B94B7A4F6F6415D5A2DC01A (void);
// 0x00000622 System.Void NLayer.Decoder.MpegStreamReader::ReadToEnd()
extern void MpegStreamReader_ReadToEnd_mF9DFC708B355ADA6D86C04109729B6A22B2629DD (void);
// 0x00000623 System.Boolean NLayer.Decoder.MpegStreamReader::get_CanSeek()
extern void MpegStreamReader_get_CanSeek_mB751C1C28960A488B7668D513F425A1E88D3B78A (void);
// 0x00000624 System.Int64 NLayer.Decoder.MpegStreamReader::get_SampleCount()
extern void MpegStreamReader_get_SampleCount_mE8E99390BAE792FA8821B799EFBA900F200B6E28 (void);
// 0x00000625 System.Int32 NLayer.Decoder.MpegStreamReader::get_SampleRate()
extern void MpegStreamReader_get_SampleRate_mBDCC536FE8E5A321A19C53ED39AF6A4CA52641AC (void);
// 0x00000626 System.Int32 NLayer.Decoder.MpegStreamReader::get_Channels()
extern void MpegStreamReader_get_Channels_m89F592253425DC228421E222234E1BE050330C08 (void);
// 0x00000627 System.Int32 NLayer.Decoder.MpegStreamReader::get_FirstFrameSampleCount()
extern void MpegStreamReader_get_FirstFrameSampleCount_mB670A954B42621122E7E322DE227C96767A9EFBA (void);
// 0x00000628 System.Int64 NLayer.Decoder.MpegStreamReader::SeekTo(System.Int64)
extern void MpegStreamReader_SeekTo_m39BF65CB214DA5080C0840D24C5C07A2F7008CE1 (void);
// 0x00000629 NLayer.Decoder.MpegFrame NLayer.Decoder.MpegStreamReader::NextFrame()
extern void MpegStreamReader_NextFrame_m6B7CD3A6B9CFA20A430BD4D594BEF2BC462823A3 (void);
// 0x0000062A NLayer.Decoder.MpegFrame NLayer.Decoder.MpegStreamReader::GetCurrentFrame()
extern void MpegStreamReader_GetCurrentFrame_m363AFFA6986273631A9619843B77E6120C62932D (void);
// 0x0000062B NLayer.Decoder.RiffHeaderFrame NLayer.Decoder.RiffHeaderFrame::TrySync(System.UInt32)
extern void RiffHeaderFrame_TrySync_m2F3F582DBF6B6B4C7CF65FD213ABD855EAD18620 (void);
// 0x0000062C System.Void NLayer.Decoder.RiffHeaderFrame::.ctor()
extern void RiffHeaderFrame__ctor_mCEF63A01A8146F08D9772900E1D0A3BD7BE5B90F (void);
// 0x0000062D System.Int32 NLayer.Decoder.RiffHeaderFrame::Validate()
extern void RiffHeaderFrame_Validate_m3B8EC8819DA617548D50E48BA89C358683E5A860 (void);
// 0x0000062E System.Void NLayer.Decoder.VBRInfo::.ctor()
extern void VBRInfo__ctor_m50231CFDB4DE402338B49BE22E49BCF591ED45D6 (void);
// 0x0000062F System.Int32 NLayer.Decoder.VBRInfo::get_SampleCount()
extern void VBRInfo_get_SampleCount_m245918E68F1A266D07A19249125D2CE3FAAEBBD6 (void);
// 0x00000630 System.Void NLayer.Decoder.VBRInfo::set_SampleCount(System.Int32)
extern void VBRInfo_set_SampleCount_mEFF5C767EB3D9556D7DD9910CEFE401EECD16138 (void);
// 0x00000631 System.Int32 NLayer.Decoder.VBRInfo::get_SampleRate()
extern void VBRInfo_get_SampleRate_mAA22CF1A82EB1DBF5B38C85BC4F2FC4B125D9AA3 (void);
// 0x00000632 System.Void NLayer.Decoder.VBRInfo::set_SampleRate(System.Int32)
extern void VBRInfo_set_SampleRate_mDF5B021E89C6F8BA7647A2F501C6A3AB29C946C8 (void);
// 0x00000633 System.Int32 NLayer.Decoder.VBRInfo::get_Channels()
extern void VBRInfo_get_Channels_m09E9ADC1C92F1AB34B0733BEF57E3BBB74A4D41E (void);
// 0x00000634 System.Void NLayer.Decoder.VBRInfo::set_Channels(System.Int32)
extern void VBRInfo_set_Channels_mEE04F37666F219D8328E2936FFA19786C9EF0002 (void);
// 0x00000635 System.Int32 NLayer.Decoder.VBRInfo::get_VBRFrames()
extern void VBRInfo_get_VBRFrames_m527B07342FDC2E3BA853881F46419BE9C63253F0 (void);
// 0x00000636 System.Void NLayer.Decoder.VBRInfo::set_VBRFrames(System.Int32)
extern void VBRInfo_set_VBRFrames_m05F87FCDA21E0E71211A090BB11350C2A6603E8A (void);
// 0x00000637 System.Int32 NLayer.Decoder.VBRInfo::get_VBRBytes()
extern void VBRInfo_get_VBRBytes_m4F0ED263093E832E177888E3A6EEE2184AFEED67 (void);
// 0x00000638 System.Void NLayer.Decoder.VBRInfo::set_VBRBytes(System.Int32)
extern void VBRInfo_set_VBRBytes_m457BD3FD26B2CD1205E4773F7D5C6593877137E3 (void);
// 0x00000639 System.Int32 NLayer.Decoder.VBRInfo::get_VBRQuality()
extern void VBRInfo_get_VBRQuality_mECCF492A4EABA139C6502B70DCD86A527D1CB552 (void);
// 0x0000063A System.Void NLayer.Decoder.VBRInfo::set_VBRQuality(System.Int32)
extern void VBRInfo_set_VBRQuality_m3A4C4BA379E79452CBC4F1F1FF3BA2D545480C12 (void);
// 0x0000063B System.Int32 NLayer.Decoder.VBRInfo::get_VBRDelay()
extern void VBRInfo_get_VBRDelay_mA6E1991C5D05B28548C2EE19A949ED815A28F98E (void);
// 0x0000063C System.Void NLayer.Decoder.VBRInfo::set_VBRDelay(System.Int32)
extern void VBRInfo_set_VBRDelay_m2C7452245B88D5D90C43CD7FF883C2A9002E84E7 (void);
// 0x0000063D System.Int64 NLayer.Decoder.VBRInfo::get_VBRStreamSampleCount()
extern void VBRInfo_get_VBRStreamSampleCount_m6EFFAB37F244FCD4234624B31DAC4D691718A8B6 (void);
// 0x0000063E System.Int32 NLayer.Decoder.VBRInfo::get_VBRAverageBitrate()
extern void VBRInfo_get_VBRAverageBitrate_m9EE890A67B078C344C59C026897B534670FD9F79 (void);
// 0x0000063F System.Void AudioLoader/<LoadHelper>d__5::.ctor(System.Int32)
extern void U3CLoadHelperU3Ed__5__ctor_m55FA1DD43D6800B17B5DFA9B6A10F61371B9726E (void);
// 0x00000640 System.Void AudioLoader/<LoadHelper>d__5::System.IDisposable.Dispose()
extern void U3CLoadHelperU3Ed__5_System_IDisposable_Dispose_m83328E4399CA378E13126D1F97741D402818C722 (void);
// 0x00000641 System.Boolean AudioLoader/<LoadHelper>d__5::MoveNext()
extern void U3CLoadHelperU3Ed__5_MoveNext_m8E9CFFDA1AA270321A1B8F0DF718B827AF252272 (void);
// 0x00000642 System.Object AudioLoader/<LoadHelper>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadHelperU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD15A5956E5F0B6119FB3FFC980DC14EAC192D1AC (void);
// 0x00000643 System.Void AudioLoader/<LoadHelper>d__5::System.Collections.IEnumerator.Reset()
extern void U3CLoadHelperU3Ed__5_System_Collections_IEnumerator_Reset_m49B71E2A89FBA8012DED6092EC493360B75EB005 (void);
// 0x00000644 System.Object AudioLoader/<LoadHelper>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CLoadHelperU3Ed__5_System_Collections_IEnumerator_get_Current_m6562A56AD68DDD3D9BF6E0983936E972DC40DD3B (void);
// 0x00000645 System.Void TestingPunch/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m063B6F62D61CAA29B3AB1AC3A819D6D18240F9F6 (void);
// 0x00000646 System.Void TestingPunch/<>c__DisplayClass4_0::<Update>b__2(System.Single)
extern void U3CU3Ec__DisplayClass4_0_U3CUpdateU3Eb__2_mE84C0A7EAF064241DAA6E98CF768F59DF1AB4B9E (void);
// 0x00000647 System.Void TestingPunch/<>c__DisplayClass4_1::.ctor()
extern void U3CU3Ec__DisplayClass4_1__ctor_mBB8D9837F5EB6C46A04F399F601533FEDE46756B (void);
// 0x00000648 System.Void TestingPunch/<>c__DisplayClass4_1::<Update>b__6()
extern void U3CU3Ec__DisplayClass4_1_U3CUpdateU3Eb__6_m76EC6A2E3975E8A2E6A031E8B6DA4C4088F2A55E (void);
// 0x00000649 System.Void TestingPunch/<>c__DisplayClass4_2::.ctor()
extern void U3CU3Ec__DisplayClass4_2__ctor_mAD3D1D58AD09A66A3CFCD2A27D3FDA00EAB9A6AB (void);
// 0x0000064A System.Void TestingPunch/<>c__DisplayClass4_2::<Update>b__8(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_2_U3CUpdateU3Eb__8_mB2A8A874B685A024E5058BEED756182B1B13ACD9 (void);
// 0x0000064B System.Void TestingPunch/<>c::.cctor()
extern void U3CU3Ec__cctor_m962DC1D18E6A690D9DAFF83EC393FE4F5ADE18C8 (void);
// 0x0000064C System.Void TestingPunch/<>c::.ctor()
extern void U3CU3Ec__ctor_m63711F73205AC47759F3870E385391FC49C03864 (void);
// 0x0000064D System.Void TestingPunch/<>c::<Update>b__4_1()
extern void U3CU3Ec_U3CUpdateU3Eb__4_1_mD2E518290C8AB4EDAC4F79EEF665A4A3430655E1 (void);
// 0x0000064E System.Void TestingPunch/<>c::<Update>b__4_4(System.Single)
extern void U3CU3Ec_U3CUpdateU3Eb__4_4_m8FA6F3DC6A54EFB3028C4BDCC66F69FC1EFC9A01 (void);
// 0x0000064F System.Void TestingPunch/<>c::<Update>b__4_5()
extern void U3CU3Ec_U3CUpdateU3Eb__4_5_m3952D2A76F4153183CE5361AD763AC2F13EB00A1 (void);
// 0x00000650 System.Void TestingPunch/<>c::<tweenStatically>b__5_0(System.Single)
extern void U3CU3Ec_U3CtweenStaticallyU3Eb__5_0_m9F5117E4B185369A4121FC6E4972F55B802C4F5D (void);
// 0x00000651 System.Void GeneralBasic/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mEBB010A7E42F38BBA0F6526F4898B3793016754A (void);
// 0x00000652 System.Void GeneralBasic/<>c__DisplayClass2_0::<advancedExamples>b__1()
extern void U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__1_m46CD88EA10100768CC8BC78BD206FB34418B38BE (void);
// 0x00000653 System.Void GeneralBasic/<>c__DisplayClass2_0::<advancedExamples>b__2()
extern void U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__2_mD8310B529F6CC844EFAE6B47D4EE69A074102CAC (void);
// 0x00000654 System.Void GeneralBasics2d/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mAE212C3B3074A3A63D0FAFB45B319458DC469379 (void);
// 0x00000655 System.Void GeneralBasics2d/<>c__DisplayClass4_0::<advancedExamples>b__1()
extern void U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__1_m5443364BDD6519C00E41B679475989EC7BFA2A74 (void);
// 0x00000656 System.Void GeneralBasics2d/<>c__DisplayClass4_0::<advancedExamples>b__2()
extern void U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__2_mC7171C077A8800FAA156D255B27FF78E214EAB2D (void);
// 0x00000657 System.Void GeneralCameraShake/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m6E2FDBEF2F7D210950B106AA2AC99FB7C23365FD (void);
// 0x00000658 System.Void GeneralCameraShake/<>c__DisplayClass4_0::<bigGuyJump>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__0_m83B800F5E384DA675243ACF6BE368207914E6568 (void);
// 0x00000659 System.Void GeneralCameraShake/<>c__DisplayClass4_0::<bigGuyJump>b__1()
extern void U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__1_mA58D74BCD32E0467DF4939BF6A9BBEBEB98B6966 (void);
// 0x0000065A System.Void GeneralCameraShake/<>c__DisplayClass4_1::.ctor()
extern void U3CU3Ec__DisplayClass4_1__ctor_m90198E5152DCA6BBCF86C52303D7FF53541123ED (void);
// 0x0000065B System.Void GeneralCameraShake/<>c__DisplayClass4_1::<bigGuyJump>b__2(System.Single)
extern void U3CU3Ec__DisplayClass4_1_U3CbigGuyJumpU3Eb__2_mA7236E71AC8335842B32DED8F5A9BD467BE5E68E (void);
// 0x0000065C System.Void GeneralEasingTypes/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m99076BF1EC2BA4183686FD305FE9257A1FC108D8 (void);
// 0x0000065D System.Void GeneralEasingTypes/<>c__DisplayClass4_0::<demoEaseTypes>b__0(System.Single)
extern void U3CU3Ec__DisplayClass4_0_U3CdemoEaseTypesU3Eb__0_m8895F6DF6D44640415A16CB3166821A05AE7FB13 (void);
// 0x0000065E System.Void GeneralSimpleUI/<>c::.cctor()
extern void U3CU3Ec__cctor_m7E87C117C81452C517C83B26D383ED1C5747A990 (void);
// 0x0000065F System.Void GeneralSimpleUI/<>c::.ctor()
extern void U3CU3Ec__ctor_mE3568F988560BC01C21F210610E8221EC2136358 (void);
// 0x00000660 System.Void GeneralSimpleUI/<>c::<Start>b__1_1(System.Single)
extern void U3CU3Ec_U3CStartU3Eb__1_1_mBDA4A05B6CF190E80BD4AD54FE5A7B843122E047 (void);
// 0x00000661 System.Void GeneralUISpace/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m1545255D46C77C14BEC0F46C3D149CFF14287CBA (void);
// 0x00000662 System.Void GeneralUISpace/<>c__DisplayClass15_0::<Start>b__0(System.Single)
extern void U3CU3Ec__DisplayClass15_0_U3CStartU3Eb__0_m7360056B580EBA5508B4AAF9B8FE47F13869D198 (void);
// 0x00000663 System.Void TestingZLegacy/NextFunc::.ctor(System.Object,System.IntPtr)
extern void NextFunc__ctor_mE79736745346FAD184C90EC9FFB942EDC6273AA5 (void);
// 0x00000664 System.Void TestingZLegacy/NextFunc::Invoke()
extern void NextFunc_Invoke_m5F15E6FAEEE04B0364D5861105F46B8CA1D15392 (void);
// 0x00000665 System.IAsyncResult TestingZLegacy/NextFunc::BeginInvoke(System.AsyncCallback,System.Object)
extern void NextFunc_BeginInvoke_m8FCE73C9609127DA3C01D221A5D74FAD4CA3EB0B (void);
// 0x00000666 System.Void TestingZLegacy/NextFunc::EndInvoke(System.IAsyncResult)
extern void NextFunc_EndInvoke_m5901C99FC6387B6FB299C1C8426F59A415E9C0F9 (void);
// 0x00000667 System.Void TestingZLegacy/<>c::.cctor()
extern void U3CU3Ec__cctor_mF9DA54E17EBF14B22D6DB252952767BE7FD49C9A (void);
// 0x00000668 System.Void TestingZLegacy/<>c::.ctor()
extern void U3CU3Ec__ctor_mE708DC3155F07189D4E3399F4EF7B6C622475119 (void);
// 0x00000669 System.Void TestingZLegacy/<>c::<cycleThroughExamples>b__20_0(System.Single)
extern void U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m84C10D9978F636C29F9ED1E04BA0742A68D1C98A (void);
// 0x0000066A System.Void TestingZLegacyExt/NextFunc::.ctor(System.Object,System.IntPtr)
extern void NextFunc__ctor_mCCFCFD0AB879EBBB46104ADFF9A9024018DCAFD4 (void);
// 0x0000066B System.Void TestingZLegacyExt/NextFunc::Invoke()
extern void NextFunc_Invoke_m84CD7F169D5B749CFE2ED930F9CF0E7B9D41B75F (void);
// 0x0000066C System.IAsyncResult TestingZLegacyExt/NextFunc::BeginInvoke(System.AsyncCallback,System.Object)
extern void NextFunc_BeginInvoke_mCEE4B7EB7615D52E726E9DB1BF918E0ED3B46FBC (void);
// 0x0000066D System.Void TestingZLegacyExt/NextFunc::EndInvoke(System.IAsyncResult)
extern void NextFunc_EndInvoke_mE35B84DD8B0DD2D1124F28CB2B5729F070AE3FEC (void);
// 0x0000066E System.Void TestingZLegacyExt/<>c::.cctor()
extern void U3CU3Ec__cctor_m18DE319729D6EBE31B6DBEEF6AC68B727FEAFC58 (void);
// 0x0000066F System.Void TestingZLegacyExt/<>c::.ctor()
extern void U3CU3Ec__ctor_mF8076F513CB75419CD4AAF5D2CF96EA750C7C64D (void);
// 0x00000670 System.Void TestingZLegacyExt/<>c::<cycleThroughExamples>b__20_0(System.Single)
extern void U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m8A0627F6E7351B2DF8151D125E1E5045000F449D (void);
// 0x00000671 System.Void LTDescr/EaseTypeDelegate::.ctor(System.Object,System.IntPtr)
extern void EaseTypeDelegate__ctor_m479FD2E5E95095E811CD43C107F9F7BEDA5C1722 (void);
// 0x00000672 UnityEngine.Vector3 LTDescr/EaseTypeDelegate::Invoke()
extern void EaseTypeDelegate_Invoke_m6C4DD4B41DD183CD84736AB68488797939157559 (void);
// 0x00000673 System.IAsyncResult LTDescr/EaseTypeDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void EaseTypeDelegate_BeginInvoke_mC4A8F044C73F14833A8F0892A1B1D7EFA5719775 (void);
// 0x00000674 UnityEngine.Vector3 LTDescr/EaseTypeDelegate::EndInvoke(System.IAsyncResult)
extern void EaseTypeDelegate_EndInvoke_mBBC06324172B4D605212E2BF2D7568C91D55371D (void);
// 0x00000675 System.Void LTDescr/ActionMethodDelegate::.ctor(System.Object,System.IntPtr)
extern void ActionMethodDelegate__ctor_m2C8565D6C66397F9327D19C9D47C45587110BC5F (void);
// 0x00000676 System.Void LTDescr/ActionMethodDelegate::Invoke()
extern void ActionMethodDelegate_Invoke_mD731C08D9044E02DE9276EB7269BCA0F540950EA (void);
// 0x00000677 System.IAsyncResult LTDescr/ActionMethodDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void ActionMethodDelegate_BeginInvoke_mF15334FB75A22146CE827C20402393D61B7203C1 (void);
// 0x00000678 System.Void LTDescr/ActionMethodDelegate::EndInvoke(System.IAsyncResult)
extern void ActionMethodDelegate_EndInvoke_m164BF9BEF0C0CB55BC1AC688BECC75811982F9F6 (void);
// 0x00000679 System.Void LTDescr/<>c::.cctor()
extern void U3CU3Ec__cctor_m7B400CF0B15F5A6BB8EDD68761B868209D666561 (void);
// 0x0000067A System.Void LTDescr/<>c::.ctor()
extern void U3CU3Ec__ctor_m3E01BF844F661459DBA93A1A1A2052BB66D8278C (void);
// 0x0000067B System.Void LTDescr/<>c::<setCallback>b__113_0()
extern void U3CU3Ec_U3CsetCallbackU3Eb__113_0_mBAF1F42A0092D36D9EA8B302EAD7E4B64484A698 (void);
// 0x0000067C System.Void LTDescr/<>c::<setValue3>b__114_0()
extern void U3CU3Ec_U3CsetValue3U3Eb__114_0_m2BEB42E3CC61F73F63334E99081144AC817C3A6A (void);
// 0x0000067D System.Void LeanTester/<timeoutCheck>d__2::.ctor(System.Int32)
extern void U3CtimeoutCheckU3Ed__2__ctor_m86660954A201D3F4042917DED992CF67F2967E40 (void);
// 0x0000067E System.Void LeanTester/<timeoutCheck>d__2::System.IDisposable.Dispose()
extern void U3CtimeoutCheckU3Ed__2_System_IDisposable_Dispose_mE54F8B9D9C634E727ADEB82A507BCC5D2D7F6A58 (void);
// 0x0000067F System.Boolean LeanTester/<timeoutCheck>d__2::MoveNext()
extern void U3CtimeoutCheckU3Ed__2_MoveNext_m81A53EB7B0E977685E46FE5D64A808B557EC5EC3 (void);
// 0x00000680 System.Object LeanTester/<timeoutCheck>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtimeoutCheckU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B28917D1D087CC4FFE00C923F13DE0E3C80DBFC (void);
// 0x00000681 System.Void LeanTester/<timeoutCheck>d__2::System.Collections.IEnumerator.Reset()
extern void U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_Reset_m4A3583E3E830B3116E7DC2C562734E6153CAB4EF (void);
// 0x00000682 System.Object LeanTester/<timeoutCheck>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_get_Current_m21261FBD3907579D1BC3B9C4B58CB01EA868DCB9 (void);
// 0x00000683 System.Void LeanTween/<>c__DisplayClass193_0::.ctor()
extern void U3CU3Ec__DisplayClass193_0__ctor_mC0B0125F1AA00B0D67E5471AC4723D7C4DDF745A (void);
// 0x00000684 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__0()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__0_m1C1C28A5106180AA479E783252142933B5B22CB3 (void);
// 0x00000685 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__1()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__1_mA9FBC068978683FE7F41AB0DE6764B37E8F12FCB (void);
// 0x00000686 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__2()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__2_m32EDEE7568C90E8D332BAF204B827BB696F8915F (void);
// 0x00000687 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__3()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__3_mAB9D969B192F8C7F5CD2D8CEFC40A0AB9A1E569C (void);
// 0x00000688 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__4()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__4_mD098FD9EE16741678387663B5E5A20AACDB3C461 (void);
// 0x00000689 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__5()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__5_m2D34FBF1D0050BC36587D4AC4AB768CBBAD16653 (void);
// 0x0000068A System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__6()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__6_m4F2B350D8676AF4E40A9A522AB546E7D64CB5A4F (void);
// 0x0000068B System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__7()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__7_m4794A3DFBD9F60DDB39A1D79B3261D224192C6EA (void);
// 0x0000068C System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__8()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__8_m8F118FB039CC4F97D3B5099511C0ACF4A16794E1 (void);
// 0x0000068D System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__9()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__9_m1E35B42D88BA7A620623D40B97FE0A034E6D51DF (void);
// 0x0000068E System.Void LeanTween/<>c__DisplayClass194_0::.ctor()
extern void U3CU3Ec__DisplayClass194_0__ctor_mDCBB6E03EFED25D38C1645524A9CFB06906D64A0 (void);
// 0x0000068F System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__0()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__0_m0DF3AC96834FE13B309965159F303CF6519EEF50 (void);
// 0x00000690 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__1()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__1_mC60E4D28F90F14EC7259E5FC1F36B0C7BA0587A4 (void);
// 0x00000691 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__2()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__2_mAC5BC0C40BD4FF4F7A321BB0E5C7F9759AC85F0C (void);
// 0x00000692 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__3()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__3_m7C23E5BA1FAF357B1D25B4DC5B0F587074A36440 (void);
// 0x00000693 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__4()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__4_m27AE2F3933DC68B0444E94E4C6C86ED6C1BAE149 (void);
// 0x00000694 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__5()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__5_m95EB2A430E734B7AA2726020BB238DF0374206B6 (void);
// 0x00000695 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__6()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__6_m15ED9AC2709C71E60DA3125DF3A16E7B8B94C3AD (void);
// 0x00000696 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__7()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__7_m4C0A7E27C26DB6DCFBEFE3C357B8A26CC65474D4 (void);
// 0x00000697 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__8()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__8_m763826D5C9CF0FF3D93E27C117BF47A4A2C57BD6 (void);
// 0x00000698 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__9()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__9_mAB337657583472749A5443EF146F301CF2391AB0 (void);
// 0x00000699 System.Void LeanTween/<>c__DisplayClass195_0::.ctor()
extern void U3CU3Ec__DisplayClass195_0__ctor_mE00CB764397209766E590DF5EFEB52053DC0AE26 (void);
// 0x0000069A System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__0()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__0_mAC6C7B8F8DF55A577CE8781796BE15239B566E89 (void);
// 0x0000069B System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__1()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__1_m5184ABA1AB3627B2DDED0C74E2282344378AF3B8 (void);
// 0x0000069C System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__2()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__2_m34A0E6745FC7434BB5A27B25A097C58784226F3C (void);
// 0x0000069D System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__3()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__3_m2D76D667CE87B27FBCAECC55425C3207533F6A75 (void);
// 0x0000069E System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__4()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__4_m205F4F7C5D9A2B137BCB29491B95C3828F378B07 (void);
// 0x0000069F System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__5()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__5_mCDC95E138FF86491A853A74BF172BEEB38BECA09 (void);
// 0x000006A0 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__6()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__6_mDD9833A53793BAE5B0A03C37BA4EF1F60E9930FF (void);
// 0x000006A1 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__7()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__7_m74731F175AF6639677E96328854558399D9FE7CF (void);
// 0x000006A2 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__8()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__8_m8B743DC6870594CE43D0325E2D884FB0CAF98E20 (void);
// 0x000006A3 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__9()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__9_m466A3211A248D8995FB3AE22D016F1EFD623A8E6 (void);
// 0x000006A4 System.Void LeanTween/<>c__DisplayClass196_0::.ctor()
extern void U3CU3Ec__DisplayClass196_0__ctor_m929D534AE634FB58A7C0ECDFB23A1D78351F6F3F (void);
// 0x000006A5 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__0()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__0_mA2E1AF83CA2338CA95BA84B64C912222C7A3D9E6 (void);
// 0x000006A6 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__1()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__1_m23CB51E9EF35556F273C3C0B62C5C06D8A27273E (void);
// 0x000006A7 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__2()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__2_m54430A940C2FEE40A54C3AE14482E1DD43CBF103 (void);
// 0x000006A8 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__3()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__3_mE6D8BB8EFB0E336E59430CA8A2E029CEC51136C8 (void);
// 0x000006A9 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__4()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__4_mD6D1CF2BB54CAF51FFF102ECDAE4BE98C25FE3E0 (void);
// 0x000006AA System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__5()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__5_m4244AA15E1AFBF399BA4B03799FDD52F1893D5BA (void);
// 0x000006AB System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__6()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__6_mB26E4D96497F6A2BBB615D69D4966E6B8FE8BFF7 (void);
// 0x000006AC System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__7()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__7_mCC89DC9F63965ABBC1705F61D48B28D83959F20E (void);
// 0x000006AD System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__8()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__8_mD29A47A9681480C45508BE22F8600B447930FA58 (void);
// 0x000006AE System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__9()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__9_m6F5ACA5A264C5CF1B4A20164725E5103856A9880 (void);
// 0x000006AF System.Void ReactController/ReactMessageData::.ctor()
extern void ReactMessageData__ctor_mE2C748F6062AF1973D3346BF833A07A0B0763883 (void);
// 0x000006B0 System.Void ReactController/<DownloadAnimationRequest>d__19::.ctor(System.Int32)
extern void U3CDownloadAnimationRequestU3Ed__19__ctor_m0D93AF583ABE22105E87D623D95628816C81D720 (void);
// 0x000006B1 System.Void ReactController/<DownloadAnimationRequest>d__19::System.IDisposable.Dispose()
extern void U3CDownloadAnimationRequestU3Ed__19_System_IDisposable_Dispose_m2947A6FAC770C0357F5CBE03AFE0C9CCE7F0F083 (void);
// 0x000006B2 System.Boolean ReactController/<DownloadAnimationRequest>d__19::MoveNext()
extern void U3CDownloadAnimationRequestU3Ed__19_MoveNext_m802960BA99AC130052338DFD818644C420E6B601 (void);
// 0x000006B3 System.Object ReactController/<DownloadAnimationRequest>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadAnimationRequestU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0EAB7B4FCFCCE25D7941991368B8E54D24C6B247 (void);
// 0x000006B4 System.Void ReactController/<DownloadAnimationRequest>d__19::System.Collections.IEnumerator.Reset()
extern void U3CDownloadAnimationRequestU3Ed__19_System_Collections_IEnumerator_Reset_mFD6BBF6C3A0D221ED9CC1E9C71DADC0CF18037E5 (void);
// 0x000006B5 System.Object ReactController/<DownloadAnimationRequest>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadAnimationRequestU3Ed__19_System_Collections_IEnumerator_get_Current_m0316E8724880503F76E4DF5237E3CB66DFEF712F (void);
// 0x000006B6 System.Void UnityMessageManager/MessageDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageDelegate__ctor_mE80DF7F22AC095655F222BA7B808FB750C059479 (void);
// 0x000006B7 System.Void UnityMessageManager/MessageDelegate::Invoke(System.String)
extern void MessageDelegate_Invoke_m71D9C54B8D33C236D4145F5C171F5057C0A08B09 (void);
// 0x000006B8 System.IAsyncResult UnityMessageManager/MessageDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MessageDelegate_BeginInvoke_mAEE45C362BD9197D646B59B1414ABE19C53BF4C1 (void);
// 0x000006B9 System.Void UnityMessageManager/MessageDelegate::EndInvoke(System.IAsyncResult)
extern void MessageDelegate_EndInvoke_m57B0BBB4D9EBF8DC1F6A7D8A6C449B7C1C75796F (void);
// 0x000006BA System.Void UnityMessageManager/MessageHandlerDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageHandlerDelegate__ctor_mB09E60C8134DF32641562ED528E1E0E35B40184A (void);
// 0x000006BB System.Void UnityMessageManager/MessageHandlerDelegate::Invoke(MessageHandler)
extern void MessageHandlerDelegate_Invoke_m82C612B57B22FCF40994CAA70914DD9701E5CCCF (void);
// 0x000006BC System.IAsyncResult UnityMessageManager/MessageHandlerDelegate::BeginInvoke(MessageHandler,System.AsyncCallback,System.Object)
extern void MessageHandlerDelegate_BeginInvoke_m3E0807D2B4E1CB959FEEA14DFB15AD08299EE994 (void);
// 0x000006BD System.Void UnityMessageManager/MessageHandlerDelegate::EndInvoke(System.IAsyncResult)
extern void MessageHandlerDelegate_EndInvoke_mE15D4204B18F5409F5B926454790F915652ED3B5 (void);
// 0x000006BE System.Void RecordManager/<ManageHideObject>d__22::.ctor(System.Int32)
extern void U3CManageHideObjectU3Ed__22__ctor_mF7C41000BBF32CAAB885C13310489E31E20B8C36 (void);
// 0x000006BF System.Void RecordManager/<ManageHideObject>d__22::System.IDisposable.Dispose()
extern void U3CManageHideObjectU3Ed__22_System_IDisposable_Dispose_mA56142C1BA3143F59115FB890396C747280A6E25 (void);
// 0x000006C0 System.Boolean RecordManager/<ManageHideObject>d__22::MoveNext()
extern void U3CManageHideObjectU3Ed__22_MoveNext_mC367B73E612521D103A9C1F328131CF35D90ACAD (void);
// 0x000006C1 System.Object RecordManager/<ManageHideObject>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CManageHideObjectU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCC9E897F076F5DD26101ED951027E888A0FC54BB (void);
// 0x000006C2 System.Void RecordManager/<ManageHideObject>d__22::System.Collections.IEnumerator.Reset()
extern void U3CManageHideObjectU3Ed__22_System_Collections_IEnumerator_Reset_mC2C76B3917135B50EFC9A79E36E5C988B28366F5 (void);
// 0x000006C3 System.Object RecordManager/<ManageHideObject>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CManageHideObjectU3Ed__22_System_Collections_IEnumerator_get_Current_m83C9A9D94BA18BFEB7EC2C5F84FEB12D6ED49A60 (void);
// 0x000006C4 System.Void SoundTester/<StreamAudioOnMobile>d__26::.ctor(System.Int32)
extern void U3CStreamAudioOnMobileU3Ed__26__ctor_mE986FC0ACBE94C6C1723C557A43FAB11308AA35C (void);
// 0x000006C5 System.Void SoundTester/<StreamAudioOnMobile>d__26::System.IDisposable.Dispose()
extern void U3CStreamAudioOnMobileU3Ed__26_System_IDisposable_Dispose_m279FABF2865FC82585B79B4C8C878F6424DB5DFF (void);
// 0x000006C6 System.Boolean SoundTester/<StreamAudioOnMobile>d__26::MoveNext()
extern void U3CStreamAudioOnMobileU3Ed__26_MoveNext_mA4FA3E77E28880656FFDB1959A7F8556EB331D8D (void);
// 0x000006C7 System.Void SoundTester/<StreamAudioOnMobile>d__26::<>m__Finally1()
extern void U3CStreamAudioOnMobileU3Ed__26_U3CU3Em__Finally1_mCD87FEDFBA7C4F242823FC070BF14EF9DC180A77 (void);
// 0x000006C8 System.Object SoundTester/<StreamAudioOnMobile>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStreamAudioOnMobileU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8811986F6EB49C2821283FBF3F63F065F1DD2FA3 (void);
// 0x000006C9 System.Void SoundTester/<StreamAudioOnMobile>d__26::System.Collections.IEnumerator.Reset()
extern void U3CStreamAudioOnMobileU3Ed__26_System_Collections_IEnumerator_Reset_m7DCB573DE1B9346936DEC6BC50B9FA3FB4B5BB2F (void);
// 0x000006CA System.Object SoundTester/<StreamAudioOnMobile>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CStreamAudioOnMobileU3Ed__26_System_Collections_IEnumerator_get_Current_m0973C7F77C5847BA5E151963CE0053626335BE77 (void);
// 0x000006CB System.Void TipManager/<StreamAudioOnWeb>d__33::.ctor(System.Int32)
extern void U3CStreamAudioOnWebU3Ed__33__ctor_m038A0609CC5F9BA5A7F08E0F0E70541B3D3854E9 (void);
// 0x000006CC System.Void TipManager/<StreamAudioOnWeb>d__33::System.IDisposable.Dispose()
extern void U3CStreamAudioOnWebU3Ed__33_System_IDisposable_Dispose_m1D42DF01350A831B6A0EAFE980D3E59FB103B932 (void);
// 0x000006CD System.Boolean TipManager/<StreamAudioOnWeb>d__33::MoveNext()
extern void U3CStreamAudioOnWebU3Ed__33_MoveNext_mA4BBA78C36FAB0DFD277E105A8F8A57FF7A7AE45 (void);
// 0x000006CE System.Object TipManager/<StreamAudioOnWeb>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStreamAudioOnWebU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57B52C4A1778F156702376A347B7BA3CAD549A61 (void);
// 0x000006CF System.Void TipManager/<StreamAudioOnWeb>d__33::System.Collections.IEnumerator.Reset()
extern void U3CStreamAudioOnWebU3Ed__33_System_Collections_IEnumerator_Reset_mAAD13A207912B1911B4083861E89744815391E67 (void);
// 0x000006D0 System.Object TipManager/<StreamAudioOnWeb>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CStreamAudioOnWebU3Ed__33_System_Collections_IEnumerator_get_Current_m03366AAEEA40F1C66B47F983E0D8029AB2C315B2 (void);
// 0x000006D1 System.Void TipManager/<StreamAudioOnMobile>d__34::.ctor(System.Int32)
extern void U3CStreamAudioOnMobileU3Ed__34__ctor_mEDC848B751757398CA20267314B7659DABABB599 (void);
// 0x000006D2 System.Void TipManager/<StreamAudioOnMobile>d__34::System.IDisposable.Dispose()
extern void U3CStreamAudioOnMobileU3Ed__34_System_IDisposable_Dispose_m8EE85F8A75CC349B29D3E4F421F67D987FAE093B (void);
// 0x000006D3 System.Boolean TipManager/<StreamAudioOnMobile>d__34::MoveNext()
extern void U3CStreamAudioOnMobileU3Ed__34_MoveNext_mD206755DEE4AC6A513D6E5F3D8C48400F60EEF3E (void);
// 0x000006D4 System.Void TipManager/<StreamAudioOnMobile>d__34::<>m__Finally1()
extern void U3CStreamAudioOnMobileU3Ed__34_U3CU3Em__Finally1_m663D2C77454977636310A764E231A73D47CD235F (void);
// 0x000006D5 System.Object TipManager/<StreamAudioOnMobile>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStreamAudioOnMobileU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9834FB5DAB489B5061069A5F6711D78660CB594B (void);
// 0x000006D6 System.Void TipManager/<StreamAudioOnMobile>d__34::System.Collections.IEnumerator.Reset()
extern void U3CStreamAudioOnMobileU3Ed__34_System_Collections_IEnumerator_Reset_mCA8B10D7C79800F98E32DF399A51E0A156A88EE1 (void);
// 0x000006D7 System.Object TipManager/<StreamAudioOnMobile>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CStreamAudioOnMobileU3Ed__34_System_Collections_IEnumerator_get_Current_m3A47D9F4D912EA7AD96CFB56E9393107CABCF096 (void);
// 0x000006D8 System.Void LayoutDelay/<SetStatus>d__2::.ctor(System.Int32)
extern void U3CSetStatusU3Ed__2__ctor_m3D9C54D9141467C1ED2F12B9DE8D4F3DC980D4E7 (void);
// 0x000006D9 System.Void LayoutDelay/<SetStatus>d__2::System.IDisposable.Dispose()
extern void U3CSetStatusU3Ed__2_System_IDisposable_Dispose_m2B9874E28B9F68112B85BE9DE98B3BDB1B016F7B (void);
// 0x000006DA System.Boolean LayoutDelay/<SetStatus>d__2::MoveNext()
extern void U3CSetStatusU3Ed__2_MoveNext_m0881E95A92AF445AD808BD18686CB1539D04722C (void);
// 0x000006DB System.Object LayoutDelay/<SetStatus>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetStatusU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0FFB79FA7697DEC46130B1B0A725867C92AAED27 (void);
// 0x000006DC System.Void LayoutDelay/<SetStatus>d__2::System.Collections.IEnumerator.Reset()
extern void U3CSetStatusU3Ed__2_System_Collections_IEnumerator_Reset_m54B373E3110D68B142B3E6EC7789004E7807E538 (void);
// 0x000006DD System.Object LayoutDelay/<SetStatus>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CSetStatusU3Ed__2_System_Collections_IEnumerator_get_Current_m2178ECF04F71EEFFD8717CE64C8EF95D6EF3D9DA (void);
// 0x000006DE System.Void Tester/<LoginToAccount>d__5::.ctor(System.Int32)
extern void U3CLoginToAccountU3Ed__5__ctor_m784C52FB2CFD64F00942F51C28F052DA16FE9FA4 (void);
// 0x000006DF System.Void Tester/<LoginToAccount>d__5::System.IDisposable.Dispose()
extern void U3CLoginToAccountU3Ed__5_System_IDisposable_Dispose_mF22BF22A5C87F3120AB6DAC4302D50E7E56339D0 (void);
// 0x000006E0 System.Boolean Tester/<LoginToAccount>d__5::MoveNext()
extern void U3CLoginToAccountU3Ed__5_MoveNext_mEF8437367A47E29B0EDB9997E5B42E935B1B44D9 (void);
// 0x000006E1 System.Void Tester/<LoginToAccount>d__5::<>m__Finally1()
extern void U3CLoginToAccountU3Ed__5_U3CU3Em__Finally1_m1148CBB42434DEED8D7B617F7D948E9B5864411D (void);
// 0x000006E2 System.Object Tester/<LoginToAccount>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoginToAccountU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF4DB56B46DDFC68D114741260AA63AC41628678 (void);
// 0x000006E3 System.Void Tester/<LoginToAccount>d__5::System.Collections.IEnumerator.Reset()
extern void U3CLoginToAccountU3Ed__5_System_Collections_IEnumerator_Reset_m11DA4B61DFE0DA00B0ED9D09F7DB8A3A5D26ABD5 (void);
// 0x000006E4 System.Object Tester/<LoginToAccount>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CLoginToAccountU3Ed__5_System_Collections_IEnumerator_get_Current_m26D41CC9720D5E00AFF49694B1DB89A72A63E3A3 (void);
// 0x000006E5 System.Void SkillsAnimationsManager/AnimationPack::.ctor(System.Int32,UnityEngine.AnimationClip[])
extern void AnimationPack__ctor_mC2A8CA2F5431793BC97330D9F54AB789FE9CB9E9 (void);
// 0x000006E6 System.Void SkillsDictionary/JSONObject::.ctor()
extern void JSONObject__ctor_m4B4369D3E8DF7C754F3CA8C67852448A9FAC9E7D (void);
// 0x000006E7 System.Void SkillsDictionary/OrderType::.ctor(System.Int32,System.Int32)
extern void OrderType__ctor_m49ED159AA316907EC00272F89CD765CE4884D8F3 (void);
// 0x000006E8 System.Void SkillsDictionary/SkillData::.ctor(System.Int32,System.String,System.Int32,System.String,System.Int32,System.Int32)
extern void SkillData__ctor_m65907EF121D34D195A9F64F4F45E320774F10109 (void);
// 0x000006E9 System.Void SkillsDictionary/ExerciseData::.ctor(System.String,System.Int32,SkillsDictionary/ExerciseDataItem[])
extern void ExerciseData__ctor_m8E82EBC94D98A2545723143B8BA7E603A7785B75 (void);
// 0x000006EA System.Void SkillsDictionary/ExerciseDataItem::.ctor(System.Int32,System.String,System.Int32[],System.Int32[],System.Int32[],System.String[])
extern void ExerciseDataItem__ctor_m740F68FDA5630E51E792666952B5876AFFB257AE (void);
// 0x000006EB System.Void SkillsDictionary/<>c::.cctor()
extern void U3CU3Ec__cctor_m40512A56022041D95B6EE7293569E4B47ABB7EAF (void);
// 0x000006EC System.Void SkillsDictionary/<>c::.ctor()
extern void U3CU3Ec__ctor_m67A576F2922511E2EBFEB19CF5B5EC026F65E23D (void);
// 0x000006ED System.Int32 SkillsDictionary/<>c::<Awake>b__14_0(SkillsDictionary/SkillData)
extern void U3CU3Ec_U3CAwakeU3Eb__14_0_m88D3D41BC6F22A2C7805252F2174B9CD72434CB9 (void);
// 0x000006EE System.Int32 SkillsDictionary/<>c::<Awake>b__14_1(SkillsDictionary/ExerciseData)
extern void U3CU3Ec_U3CAwakeU3Eb__14_1_m26D35119979F078101FFCE3AB980689F08A44DBA (void);
// 0x000006EF System.Void TranslationManager/MyDictionary::.ctor(System.String,System.String)
extern void MyDictionary__ctor_mC5163FA9C3B9A2DA8693EF40B10E32005432F280 (void);
// 0x000006F0 System.Void UIControl/<SetPreviewScale>d__23::.ctor(System.Int32)
extern void U3CSetPreviewScaleU3Ed__23__ctor_m11ECD093F241F6E5230D346807F8C643723FF294 (void);
// 0x000006F1 System.Void UIControl/<SetPreviewScale>d__23::System.IDisposable.Dispose()
extern void U3CSetPreviewScaleU3Ed__23_System_IDisposable_Dispose_m5F36C8C2836BCF7474D4786875C91F65DC8ABBA7 (void);
// 0x000006F2 System.Boolean UIControl/<SetPreviewScale>d__23::MoveNext()
extern void U3CSetPreviewScaleU3Ed__23_MoveNext_mE89D0CCD0A218951BBEF3A082E930670617E8DDB (void);
// 0x000006F3 System.Object UIControl/<SetPreviewScale>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetPreviewScaleU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC23D393E502DFF8CCC88EFEAC037252E97818011 (void);
// 0x000006F4 System.Void UIControl/<SetPreviewScale>d__23::System.Collections.IEnumerator.Reset()
extern void U3CSetPreviewScaleU3Ed__23_System_Collections_IEnumerator_Reset_mA73ED601844C8CBBE092347BC94D05416E76F5C4 (void);
// 0x000006F5 System.Object UIControl/<SetPreviewScale>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CSetPreviewScaleU3Ed__23_System_Collections_IEnumerator_get_Current_m47B4D54AFDC2686DAD96AB3619087BADB393C311 (void);
// 0x000006F6 System.Void UIControl/<CheckARCapacity>d__25::.ctor(System.Int32)
extern void U3CCheckARCapacityU3Ed__25__ctor_mE4096C7DE70B1B0EABEA0E347426BD681B8BB8B2 (void);
// 0x000006F7 System.Void UIControl/<CheckARCapacity>d__25::System.IDisposable.Dispose()
extern void U3CCheckARCapacityU3Ed__25_System_IDisposable_Dispose_m02ED94E1D3D414EAA9E1820A659FADE32ABFA7E8 (void);
// 0x000006F8 System.Boolean UIControl/<CheckARCapacity>d__25::MoveNext()
extern void U3CCheckARCapacityU3Ed__25_MoveNext_mDFA00605910C6C101E8D3DB8442CA04126E30255 (void);
// 0x000006F9 System.Object UIControl/<CheckARCapacity>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckARCapacityU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m606026299A0D44507A8974D1C0B7C1E4046E28ED (void);
// 0x000006FA System.Void UIControl/<CheckARCapacity>d__25::System.Collections.IEnumerator.Reset()
extern void U3CCheckARCapacityU3Ed__25_System_Collections_IEnumerator_Reset_mC6B250324B06949C68A31C2C96F0A0111B386849 (void);
// 0x000006FB System.Object UIControl/<CheckARCapacity>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CCheckARCapacityU3Ed__25_System_Collections_IEnumerator_get_Current_mF138E793AAFBCD3C50C3B1A20496CA23993445FD (void);
// 0x000006FC System.Void UIControl/<SetActiveState>d__33::.ctor(System.Int32)
extern void U3CSetActiveStateU3Ed__33__ctor_m2A8E45242F2D352C5BF0371F8B1E07F2BB4C4412 (void);
// 0x000006FD System.Void UIControl/<SetActiveState>d__33::System.IDisposable.Dispose()
extern void U3CSetActiveStateU3Ed__33_System_IDisposable_Dispose_mE02C1D71495F6949B4959962BD528B2070A65B23 (void);
// 0x000006FE System.Boolean UIControl/<SetActiveState>d__33::MoveNext()
extern void U3CSetActiveStateU3Ed__33_MoveNext_m6E53EC3AE222BA84940F7216AEDDCE02164956A2 (void);
// 0x000006FF System.Object UIControl/<SetActiveState>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetActiveStateU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31A4340FAB52B8C2492A503BF3DC2959D57DB484 (void);
// 0x00000700 System.Void UIControl/<SetActiveState>d__33::System.Collections.IEnumerator.Reset()
extern void U3CSetActiveStateU3Ed__33_System_Collections_IEnumerator_Reset_mA57F87B3C1DDD172A7856BB1B108F11AAF4CA6F4 (void);
// 0x00000701 System.Object UIControl/<SetActiveState>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CSetActiveStateU3Ed__33_System_Collections_IEnumerator_get_Current_mC72FB53882F89929CE794D6F9E0ED49F290C9777 (void);
// 0x00000702 System.Void VideoPlayerController/<PauseOnFirstFrame>d__18::.ctor(System.Int32)
extern void U3CPauseOnFirstFrameU3Ed__18__ctor_m9335E2E164B1C27AD2C24AA75FA2FDF5BD57163E (void);
// 0x00000703 System.Void VideoPlayerController/<PauseOnFirstFrame>d__18::System.IDisposable.Dispose()
extern void U3CPauseOnFirstFrameU3Ed__18_System_IDisposable_Dispose_mF59D38B58661388947A435120215BF161F5391E1 (void);
// 0x00000704 System.Boolean VideoPlayerController/<PauseOnFirstFrame>d__18::MoveNext()
extern void U3CPauseOnFirstFrameU3Ed__18_MoveNext_m0830B41B39B26E1D2B6908DEC2325BA20886DA2D (void);
// 0x00000705 System.Object VideoPlayerController/<PauseOnFirstFrame>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPauseOnFirstFrameU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0461A57EC47B09376DCF1BFA8E69692CC58DC4C (void);
// 0x00000706 System.Void VideoPlayerController/<PauseOnFirstFrame>d__18::System.Collections.IEnumerator.Reset()
extern void U3CPauseOnFirstFrameU3Ed__18_System_Collections_IEnumerator_Reset_mFA86952F218030112250204D109AD958FE9C92D0 (void);
// 0x00000707 System.Object VideoPlayerController/<PauseOnFirstFrame>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CPauseOnFirstFrameU3Ed__18_System_Collections_IEnumerator_get_Current_mFE1A6613B7D15853C06D635D0C1925B344F327F4 (void);
// 0x00000708 System.Void VideoPlayerController/<GetFirstFrame>d__20::.ctor(System.Int32)
extern void U3CGetFirstFrameU3Ed__20__ctor_m943BFB53A761D1EE372BD24D8D5C54A3BCBF3DC0 (void);
// 0x00000709 System.Void VideoPlayerController/<GetFirstFrame>d__20::System.IDisposable.Dispose()
extern void U3CGetFirstFrameU3Ed__20_System_IDisposable_Dispose_m36E9636217D689DCBD1ADE977C500FF6381B97EE (void);
// 0x0000070A System.Boolean VideoPlayerController/<GetFirstFrame>d__20::MoveNext()
extern void U3CGetFirstFrameU3Ed__20_MoveNext_mB35041372D09F75DCDD243510BBA8FC095F13736 (void);
// 0x0000070B System.Object VideoPlayerController/<GetFirstFrame>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetFirstFrameU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB1DCD5308A20FDB657581990F0706E7486B1D12 (void);
// 0x0000070C System.Void VideoPlayerController/<GetFirstFrame>d__20::System.Collections.IEnumerator.Reset()
extern void U3CGetFirstFrameU3Ed__20_System_Collections_IEnumerator_Reset_m825815D739BE2E5FB349C4F3C5D6E0D283D8DC1F (void);
// 0x0000070D System.Object VideoPlayerController/<GetFirstFrame>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CGetFirstFrameU3Ed__20_System_Collections_IEnumerator_get_Current_m626ACAC60872AC36B2A169D76BEE165B934F6253 (void);
// 0x0000070E System.Void VideoUploader/<VideoUploadRequest>d__3::.ctor(System.Int32)
extern void U3CVideoUploadRequestU3Ed__3__ctor_mDD4CCDCD8A946756EFF9B4CAEDB2762AFE1EA955 (void);
// 0x0000070F System.Void VideoUploader/<VideoUploadRequest>d__3::System.IDisposable.Dispose()
extern void U3CVideoUploadRequestU3Ed__3_System_IDisposable_Dispose_mDCC273EE8138DDC7F093A00193545D74F2FAD709 (void);
// 0x00000710 System.Boolean VideoUploader/<VideoUploadRequest>d__3::MoveNext()
extern void U3CVideoUploadRequestU3Ed__3_MoveNext_m77A2FDAEE4D7D32AE81EA34F917E04F85151F263 (void);
// 0x00000711 System.Object VideoUploader/<VideoUploadRequest>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CVideoUploadRequestU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF4B8C0859DA4A240756F655F0FC8F68B5FB44A0 (void);
// 0x00000712 System.Void VideoUploader/<VideoUploadRequest>d__3::System.Collections.IEnumerator.Reset()
extern void U3CVideoUploadRequestU3Ed__3_System_Collections_IEnumerator_Reset_mA153D08AE3CC4BF9563AC2DFA68B79198577310E (void);
// 0x00000713 System.Object VideoUploader/<VideoUploadRequest>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CVideoUploadRequestU3Ed__3_System_Collections_IEnumerator_get_Current_mD231BD177DB5BD9D96BBF13204CF0FDF8FD4E9C2 (void);
// 0x00000714 System.Void NatSuite.Recorders.GIFRecorder/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mFD002E2D6CB7706F0C2243140DCCDDBDABC96B50 (void);
// 0x00000715 System.IntPtr NatSuite.Recorders.GIFRecorder/<>c__DisplayClass2_0::<.ctor>b__0(NatSuite.Recorders.Internal.Bridge/RecordingHandler,System.IntPtr)
extern void U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__0_m4679C34A9AB15241F3C60107A64853393F4F04B6 (void);
// 0x00000716 System.Void NatSuite.Recorders.HEVCRecorder/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mA35A811275420CFDCC8DE9296446043A0E4E3D8A (void);
// 0x00000717 System.IntPtr NatSuite.Recorders.HEVCRecorder/<>c__DisplayClass2_0::<.ctor>b__0(NatSuite.Recorders.Internal.Bridge/RecordingHandler,System.IntPtr)
extern void U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__0_m6E2FC1CC2CAF73DAD4FC4FDE38A32C2DADEC4940 (void);
// 0x00000718 System.Void NatSuite.Recorders.JPGRecorder/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mF58D1E7D665F02C0C31436E1B8B557195F92F6C8 (void);
// 0x00000719 System.Void NatSuite.Recorders.JPGRecorder/<>c__DisplayClass4_0::<CommitFrame>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CCommitFrameU3Eb__0_mBE9015849553918271F75B2FB578C30D8335D010 (void);
// 0x0000071A System.Void NatSuite.Recorders.MP4Recorder/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m5BAF5483B5FB578A785BD9A2A27FEC3A87633632 (void);
// 0x0000071B System.IntPtr NatSuite.Recorders.MP4Recorder/<>c__DisplayClass2_0::<.ctor>b__0(NatSuite.Recorders.Internal.Bridge/RecordingHandler,System.IntPtr)
extern void U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__0_mEC14B21FE04183E4C93DEFD7F4E19FDBB1D132CF (void);
// 0x0000071C System.Void NatSuite.Recorders.Internal.Bridge/RecordingHandler::.ctor(System.Object,System.IntPtr)
extern void RecordingHandler__ctor_mA135DA539B7C8E44322704009B0939EE773738E7 (void);
// 0x0000071D System.Void NatSuite.Recorders.Internal.Bridge/RecordingHandler::Invoke(System.IntPtr,System.IntPtr)
extern void RecordingHandler_Invoke_mE5C8DBD759692DDA10DB1A750964F6149790478F (void);
// 0x0000071E System.IAsyncResult NatSuite.Recorders.Internal.Bridge/RecordingHandler::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern void RecordingHandler_BeginInvoke_m435EF59D2CF85240FBA593859A936F9BBCCD38FE (void);
// 0x0000071F System.Void NatSuite.Recorders.Internal.Bridge/RecordingHandler::EndInvoke(System.IAsyncResult)
extern void RecordingHandler_EndInvoke_m892EC37A4F82A9157D6697B390F5C67554156CE8 (void);
// 0x00000720 System.Void NatSuite.Recorders.Inputs.AudioInput/AudioInputAttachment::OnAudioFilterRead(System.Single[],System.Int32)
extern void AudioInputAttachment_OnAudioFilterRead_m9959187AFE0FD16D44D7138A6BD591E4DC5376D5 (void);
// 0x00000721 System.Void NatSuite.Recorders.Inputs.AudioInput/AudioInputAttachment::.ctor()
extern void AudioInputAttachment__ctor_mF2B120DA5A6E021B8782F32F13E11D232FD2CED2 (void);
// 0x00000722 System.Void NatSuite.Recorders.Inputs.CameraInput/CameraInputAttachment::.ctor()
extern void CameraInputAttachment__ctor_m95357B638EA1498E24AC2BE6B4E6C8DE8361AFF1 (void);
// 0x00000723 System.Void NatSuite.Recorders.Inputs.CameraInput/<>c::.cctor()
extern void U3CU3Ec__cctor_m6C327885C8FE0957F1E950FB848531F22C696C47 (void);
// 0x00000724 System.Void NatSuite.Recorders.Inputs.CameraInput/<>c::.ctor()
extern void U3CU3Ec__ctor_m460E732670F280C47EE0515A2021FCC55FCC83CE (void);
// 0x00000725 System.Int32 NatSuite.Recorders.Inputs.CameraInput/<>c::<.ctor>b__1_0(UnityEngine.Camera,UnityEngine.Camera)
extern void U3CU3Ec_U3C_ctorU3Eb__1_0_m23D9DEE37ED1F9086FB9D9E91DDCA62A5F1EA2F6 (void);
// 0x00000726 System.Void NatSuite.Recorders.Inputs.CameraInput/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mCB3FD547D1D64D6C77759FE0E310D84C9E09E87D (void);
// 0x00000727 System.Void NatSuite.Recorders.Inputs.CameraInput/<>c__DisplayClass11_0::<OnFrame>b__0(UnityEngine.Rendering.AsyncGPUReadbackRequest)
extern void U3CU3Ec__DisplayClass11_0_U3COnFrameU3Eb__0_m82D5F817290BA9A8AB274361686BEB5BAA7E2A72 (void);
// 0x00000728 System.Void NatSuite.Recorders.Inputs.CameraInput/<OnFrame>d__11::.ctor(System.Int32)
extern void U3COnFrameU3Ed__11__ctor_m4F796CD90BC93408B28AC1424D6532226035950E (void);
// 0x00000729 System.Void NatSuite.Recorders.Inputs.CameraInput/<OnFrame>d__11::System.IDisposable.Dispose()
extern void U3COnFrameU3Ed__11_System_IDisposable_Dispose_m69D1A9CADA3B0D39B2106D158D2E3006797563CD (void);
// 0x0000072A System.Boolean NatSuite.Recorders.Inputs.CameraInput/<OnFrame>d__11::MoveNext()
extern void U3COnFrameU3Ed__11_MoveNext_m29EF9278508033762F9720EB763B5C29CF893CDD (void);
// 0x0000072B System.Object NatSuite.Recorders.Inputs.CameraInput/<OnFrame>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnFrameU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B7A9D75D96DF5290BB17CAA11897F6D413DE7C1 (void);
// 0x0000072C System.Void NatSuite.Recorders.Inputs.CameraInput/<OnFrame>d__11::System.Collections.IEnumerator.Reset()
extern void U3COnFrameU3Ed__11_System_Collections_IEnumerator_Reset_mB7D36B1920CC15C2AA7BB6A970D2714A344FE531 (void);
// 0x0000072D System.Object NatSuite.Recorders.Inputs.CameraInput/<OnFrame>d__11::System.Collections.IEnumerator.get_Current()
extern void U3COnFrameU3Ed__11_System_Collections_IEnumerator_get_Current_mFFECDE7919A8BA72064646BA16FA31FC8853C033 (void);
// 0x0000072E System.Void NatSuite.Examples.Giffy/<StopRecording>d__6::MoveNext()
extern void U3CStopRecordingU3Ed__6_MoveNext_m5421155110B11A174B50BEBC3087691E8D4B89D7 (void);
// 0x0000072F System.Void NatSuite.Examples.Giffy/<StopRecording>d__6::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStopRecordingU3Ed__6_SetStateMachine_m185FE7121D76EB4B77B8A59D3998E6394D8CA96F (void);
// 0x00000730 System.Void NatSuite.Examples.ReplayCam/<>c::.cctor()
extern void U3CU3Ec__cctor_mB03E8B77A501D7DE85C365B316B62805A45AB511 (void);
// 0x00000731 System.Void NatSuite.Examples.ReplayCam/<>c::.ctor()
extern void U3CU3Ec__ctor_mFF9CF1F5E6FFEF1B8591D4941C2EF862025BC203 (void);
// 0x00000732 System.Boolean NatSuite.Examples.ReplayCam/<>c::<Start>b__10_0()
extern void U3CU3Ec_U3CStartU3Eb__10_0_m81D20AD85D236A2E29B2F0DD775A41F6C224B639 (void);
// 0x00000733 System.Void NatSuite.Examples.ReplayCam/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m58B6D784AB393E6D1057D0AEDFBC21F4B65E551A (void);
// 0x00000734 System.Void NatSuite.Examples.ReplayCam/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m53052D22FC479EEA5BCC880E69745CDF8DE02012 (void);
// 0x00000735 System.Boolean NatSuite.Examples.ReplayCam/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m1BBA44FA64E0D76BC8D9132E94F7A53FDA8B50D3 (void);
// 0x00000736 System.Object NatSuite.Examples.ReplayCam/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5DEBDDABA8FB1AF52215B5799BCAFD97FA18DF1 (void);
// 0x00000737 System.Void NatSuite.Examples.ReplayCam/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m53EFD74EEF66B0BEC38F35EF5BBB765E9794ABBD (void);
// 0x00000738 System.Object NatSuite.Examples.ReplayCam/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mE85071D76C60B2CAFB401A970DA3B1B9F8940EA5 (void);
// 0x00000739 System.Void NatSuite.Examples.ReplayCam/<FinishRecording>d__14::MoveNext()
extern void U3CFinishRecordingU3Ed__14_MoveNext_mA296C1BD691818D526C93BE6A5F07910C77AD308 (void);
// 0x0000073A System.Void NatSuite.Examples.ReplayCam/<FinishRecording>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CFinishRecordingU3Ed__14_SetStateMachine_mFF4B37B838D43EE0416559A82B18DE4CCBCC8942 (void);
// 0x0000073B System.Void NatSuite.Examples.Components.CameraPreview/<>c::.cctor()
extern void U3CU3Ec__cctor_mAB2EAB9398C8550520109A5C62B1FF8D89D3CC80 (void);
// 0x0000073C System.Void NatSuite.Examples.Components.CameraPreview/<>c::.ctor()
extern void U3CU3Ec__ctor_mE0FCEC3CB4516ADB8AFB9C98ECA8E4F0EC48911E (void);
// 0x0000073D System.Boolean NatSuite.Examples.Components.CameraPreview/<>c::<Start>b__6_0()
extern void U3CU3Ec_U3CStartU3Eb__6_0_m20C1F06DF56426374C75CBAC68226F33A9C5AC7A (void);
// 0x0000073E System.Void NatSuite.Examples.Components.CameraPreview/<Start>d__6::.ctor(System.Int32)
extern void U3CStartU3Ed__6__ctor_m107A1EFF0DF765587AAC9DD86E8243FD411857FA (void);
// 0x0000073F System.Void NatSuite.Examples.Components.CameraPreview/<Start>d__6::System.IDisposable.Dispose()
extern void U3CStartU3Ed__6_System_IDisposable_Dispose_mCA59AE66206B88F2E510877FDF15B83741191050 (void);
// 0x00000740 System.Boolean NatSuite.Examples.Components.CameraPreview/<Start>d__6::MoveNext()
extern void U3CStartU3Ed__6_MoveNext_m6AC2C38509C890C977B66341F07C683D833238DF (void);
// 0x00000741 System.Object NatSuite.Examples.Components.CameraPreview/<Start>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m76D27D2019DBC9FA5EBF4E7012B8C94C370E0B56 (void);
// 0x00000742 System.Void NatSuite.Examples.Components.CameraPreview/<Start>d__6::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__6_System_Collections_IEnumerator_Reset_mF764C1C0CC9BA12136D27689F492A90BED2B8700 (void);
// 0x00000743 System.Object NatSuite.Examples.Components.CameraPreview/<Start>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__6_System_Collections_IEnumerator_get_Current_m2AFB27621E83FEF889556E50A912B0446F98A68D (void);
// 0x00000744 System.Void NatSuite.Examples.Components.RecordButton/<Countdown>d__13::.ctor(System.Int32)
extern void U3CCountdownU3Ed__13__ctor_mA67DC266DB1B26E8837001AA9B118598CEB76D59 (void);
// 0x00000745 System.Void NatSuite.Examples.Components.RecordButton/<Countdown>d__13::System.IDisposable.Dispose()
extern void U3CCountdownU3Ed__13_System_IDisposable_Dispose_m1E08AE8FE20EC4B97459F9C144DD76B8CCFED82F (void);
// 0x00000746 System.Boolean NatSuite.Examples.Components.RecordButton/<Countdown>d__13::MoveNext()
extern void U3CCountdownU3Ed__13_MoveNext_mFB2C7D1634718AA24C48470C7703769DBF8A94F7 (void);
// 0x00000747 System.Object NatSuite.Examples.Components.RecordButton/<Countdown>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCountdownU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE716D2360AEA565ABBEB8BC1B151B1E18F5B40C0 (void);
// 0x00000748 System.Void NatSuite.Examples.Components.RecordButton/<Countdown>d__13::System.Collections.IEnumerator.Reset()
extern void U3CCountdownU3Ed__13_System_Collections_IEnumerator_Reset_mAD55B07B7C84F030D40AA4CA9ED630CB979FCBCD (void);
// 0x00000749 System.Object NatSuite.Examples.Components.RecordButton/<Countdown>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CCountdownU3Ed__13_System_Collections_IEnumerator_get_Current_m608D04DAA7E73F18173AE52014FE8E3B011506C4 (void);
// 0x0000074A System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m09737A2030CD017312EC64C12F90D25E7F6D889C (void);
// 0x0000074B System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__0_mD9304EE0F587C5B18D9DA6809F5D81BCAB92D43B (void);
// 0x0000074C System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__1()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__1_mF26C6F23B1DE3EA3FD39340C393170DBA0B09740 (void);
// 0x0000074D System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__21()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__21_m7A53268725D5D9AD30AFBA3B94C22E4FE40BE8ED (void);
// 0x0000074E System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__2()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__2_mD6EF48C094516FE0626C9DA7EFAB3AEBBB6A2FE7 (void);
// 0x0000074F System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__4()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__4_m2122D73717CF12BDD39D8D0D7C19611B1F21A2C6 (void);
// 0x00000750 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__5()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__5_m749A2A34E2D95A13A8D8E483357FA5D731550CB2 (void);
// 0x00000751 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__6()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__6_m0B499A19BE3F839A9EFC34F6077C38D5D275AF32 (void);
// 0x00000752 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__8()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__8_m4FA25C019E9365E7E37985DC1E7F7907F379A4F2 (void);
// 0x00000753 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__9()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__9_m2455B8C83E029A34C3DBF08ADD33FFFEC7F0D7CC (void);
// 0x00000754 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__10()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__10_m1A7C51B0CC5CE0D268DBB4FDBABB3B3C09B76E8B (void);
// 0x00000755 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__11()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__11_m0C33237140A600D0C563633E550551710271F061 (void);
// 0x00000756 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__13(System.Object)
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__13_m705EEBD4D5DBDE19B8FDFC3A819610EE1D2509CF (void);
// 0x00000757 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__14()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__14_m3A7DB9A92B2D6751BD123943544D8C0DFCB7EBA8 (void);
// 0x00000758 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__15()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__15_m136FDB1FB84D17F0E6052AC8D1482C41E983C7B7 (void);
// 0x00000759 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__16()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__16_m505B89E3C2306856A34C770F72B07D2F36459437 (void);
// 0x0000075A System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__17()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__17_m6C5F087D0C92313A411D887D78A23FC35674C5AE (void);
// 0x0000075B System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__19(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__19_mBF88042ADFBF15D57513B1F7691F9EBB1AD743E4 (void);
// 0x0000075C System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__20()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__20_m53E8A86C0D0731F935717A27DA325C1CF5BA06A9 (void);
// 0x0000075D System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1::.ctor()
extern void U3CU3Ec__DisplayClass22_1__ctor_mF03C28B73060772B45422D83C2CCD891461E7F5A (void);
// 0x0000075E System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1::<Start>b__23()
extern void U3CU3Ec__DisplayClass22_1_U3CStartU3Eb__23_m0A92AF27DA09C241937F80B5E52AC8A467F592C7 (void);
// 0x0000075F System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::.ctor()
extern void U3CU3Ec__DisplayClass22_2__ctor_m0D149B5FF3BAD974378B32F32122517AFE1008F7 (void);
// 0x00000760 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::<Start>b__24(System.Object)
extern void U3CU3Ec__DisplayClass22_2_U3CStartU3Eb__24_m244CFBB7D843FDE489A08F6438F480F77705B733 (void);
// 0x00000761 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::.cctor()
extern void U3CU3Ec__cctor_mBB9151D0770DA4B6C0D5DE0DF4505B0B1473C792 (void);
// 0x00000762 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::.ctor()
extern void U3CU3Ec__ctor_m7292CCD570F1D648747270AA2E9366DC8BEA76AE (void);
// 0x00000763 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_3()
extern void U3CU3Ec_U3CStartU3Eb__22_3_mBE510D88050EF0EAE74485B1380103286F1E2ACF (void);
// 0x00000764 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_22()
extern void U3CU3Ec_U3CStartU3Eb__22_22_mC2B2676DF6376C960A2168BB616C30FAF9572F46 (void);
// 0x00000765 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_7()
extern void U3CU3Ec_U3CStartU3Eb__22_7_m333F34EE7AB7B6BA9AFB42616E6F31AD16648DFA (void);
// 0x00000766 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_12(System.Single)
extern void U3CU3Ec_U3CStartU3Eb__22_12_mE9BCA088D81630DCA6B4A29493F717166DE4BDC2 (void);
// 0x00000767 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_18()
extern void U3CU3Ec_U3CStartU3Eb__22_18_m0E1A87851B8F186E1D037D6019BF2B5F03BE4AE7 (void);
// 0x00000768 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<pauseTimeNow>b__26_0()
extern void U3CU3Ec_U3CpauseTimeNowU3Eb__26_0_m0067B26B1415DF3C65658EF775E43DE444C9F89E (void);
// 0x00000769 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m277D86C124C510C86F124C314062F6CA37F04780 (void);
// 0x0000076A System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__0_m1C7842AC0922C8535A9D996B445C45BD68ABC7CB (void);
// 0x0000076B System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__1()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__1_m3B2F75CA971620227F026AC96214BE0E9F33EE3B (void);
// 0x0000076C System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__2(System.Single)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__2_mA4939E85F8BBD95C7A69CCA149A6F37BC65799FB (void);
// 0x0000076D System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__3()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__3_mA4873B5E9E38B22E691207FB286333F85B549ACB (void);
// 0x0000076E System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__4()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__4_m14C5D280141ADAF244B2213E9CE61686FC470854 (void);
// 0x0000076F System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__5()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__5_m25B454BD3DB25DF33E9961063468DC070C809146 (void);
// 0x00000770 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__6(System.Single)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__6_m33BFB7BC2272AE16E35A6AF6271EAAD019DE7D5B (void);
// 0x00000771 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__7()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__7_m9DE96F4CFC41638644635C5699FDA81811802151 (void);
// 0x00000772 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__13()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__13_mC7412BB7516D3AAAA92794BF489DD6224FDFA38A (void);
// 0x00000773 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__14(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__14_mE19037EF79A1618B0ABFA5EBB8B7C7167BF59D81 (void);
// 0x00000774 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__15(System.Object)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__15_m5B4E117FD86BFF9E7C043AC7AF54886AFE964EC2 (void);
// 0x00000775 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__16()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__16_mD4F03BFC2B675FB03F1475391F156ACFF2D9AEA9 (void);
// 0x00000776 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__8()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__8_mABF6F7F053A21322BA9C371F9FFF930148E42C63 (void);
// 0x00000777 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__9(System.Single)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__9_m0832DF1094C24C2B4E50D843130FD5C09B2F33C5 (void);
// 0x00000778 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__10()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__10_mDA5269BA5F2FB4EB59DFCCF9E6C806EF781EF838 (void);
// 0x00000779 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__11(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__11_m451E7955EC34F408DFAC4365893C10BCCD1F0FFE (void);
// 0x0000077A System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__12()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__12_m58715E24DE0A08CD597D94E0B4DABAA49D93F71C (void);
// 0x0000077B System.Void DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::.ctor(System.Int32)
extern void U3CtimeBasedTestingU3Ed__24__ctor_mCE839055D4DA089ADAB24FBCCA8FCCB9307C948C (void);
// 0x0000077C System.Void DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::System.IDisposable.Dispose()
extern void U3CtimeBasedTestingU3Ed__24_System_IDisposable_Dispose_m57943CFF04FA4711B6B63A93258509F56768483A (void);
// 0x0000077D System.Boolean DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::MoveNext()
extern void U3CtimeBasedTestingU3Ed__24_MoveNext_m40360D1C97BFDAB90C0E02A76CBAE4E3C2EC99B5 (void);
// 0x0000077E System.Object DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtimeBasedTestingU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB206B6F0564F23D404E3E0F14B01E7359C2EAA8B (void);
// 0x0000077F System.Void DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::System.Collections.IEnumerator.Reset()
extern void U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_Reset_m90D3F19EAF2AE82FB852651B1A13B8EC21FA77D0 (void);
// 0x00000780 System.Object DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_get_Current_mBD52D1EB4D613ADB298D9E9A684232F65AC7468F (void);
// 0x00000781 System.Void DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::.ctor(System.Int32)
extern void U3ClotsOfCancelsU3Ed__25__ctor_mB107D0BCF384F8F0CD3DF49E8E91829D271AB34D (void);
// 0x00000782 System.Void DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::System.IDisposable.Dispose()
extern void U3ClotsOfCancelsU3Ed__25_System_IDisposable_Dispose_mC7CD787978EEAB84A938E833CE818B59FC967926 (void);
// 0x00000783 System.Boolean DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::MoveNext()
extern void U3ClotsOfCancelsU3Ed__25_MoveNext_m6FB026E807483B7AA2A7CF23F6940E0E9DD8B1FE (void);
// 0x00000784 System.Object DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ClotsOfCancelsU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D4392AC16098A343CB609FBF9FA41618CC01B3E (void);
// 0x00000785 System.Void DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::System.Collections.IEnumerator.Reset()
extern void U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_Reset_mFF39885CA3EDBEF256FBE637E5C9E4230518A07A (void);
// 0x00000786 System.Object DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::System.Collections.IEnumerator.get_Current()
extern void U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_get_Current_m94A9DE71BD10C268704724FC2B1E588F75D3C78D (void);
// 0x00000787 System.Void DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::.ctor(System.Int32)
extern void U3CpauseTimeNowU3Ed__26__ctor_m5F319CD4B560891783427EB803E77B4A8ACE0FB6 (void);
// 0x00000788 System.Void DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::System.IDisposable.Dispose()
extern void U3CpauseTimeNowU3Ed__26_System_IDisposable_Dispose_mAB85DDD605A77343E6464DA39BF37EDEFB254086 (void);
// 0x00000789 System.Boolean DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::MoveNext()
extern void U3CpauseTimeNowU3Ed__26_MoveNext_m49B3D9FE98B48DE72D1FA96F2C22FFED13EEB9E5 (void);
// 0x0000078A System.Object DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CpauseTimeNowU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m372428EA5DCAD59137F4E5AFC1C5566DDB6E1625 (void);
// 0x0000078B System.Void DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::System.Collections.IEnumerator.Reset()
extern void U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_Reset_m3DE5854E0C5EAB304CF501FD2D42B92798B6233D (void);
// 0x0000078C System.Object DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_get_Current_m1B325E4F8BE919D78719C70EFC817D07842F3C7C (void);
// 0x0000078D System.Void NLayer.Decoder.Huffman/HuffmanListNode::.ctor()
extern void HuffmanListNode__ctor_mFA57D6482AEFA3746176AEB95B78E87DF22E125C (void);
// 0x0000078E System.Void NLayer.Decoder.Huffman/<>c::.cctor()
extern void U3CU3Ec__cctor_m1A95626A9FBD414AE3D446711A5074CC9AD643B7 (void);
// 0x0000078F System.Void NLayer.Decoder.Huffman/<>c::.ctor()
extern void U3CU3Ec__ctor_mCEB0E994D99999EC9AF502819E66B17112EB3B96 (void);
// 0x00000790 System.Int32 NLayer.Decoder.Huffman/<>c::<BuildLinkedList>b__12_0(NLayer.Decoder.Huffman/HuffmanListNode,NLayer.Decoder.Huffman/HuffmanListNode)
extern void U3CU3Ec_U3CBuildLinkedListU3Eb__12_0_m8967CB03485F2CF46E5E3C8BBC15B6374551C7F2 (void);
// 0x00000791 System.Void NLayer.Decoder.LayerIIIDecoder/HybridMDCT::.cctor()
extern void HybridMDCT__cctor_mDC4085A5F7822BD8180F5DF1C1E80F6D58C80B26 (void);
// 0x00000792 System.Void NLayer.Decoder.LayerIIIDecoder/HybridMDCT::.ctor()
extern void HybridMDCT__ctor_m3E39AB50620725FE1DB8BBA240260FFC230727F5 (void);
// 0x00000793 System.Void NLayer.Decoder.LayerIIIDecoder/HybridMDCT::Reset()
extern void HybridMDCT_Reset_mB26DF29E41EE1290B4F0033298E490CE3866805E (void);
// 0x00000794 System.Void NLayer.Decoder.LayerIIIDecoder/HybridMDCT::GetPrevBlock(System.Int32,System.Single[]&,System.Single[]&)
extern void HybridMDCT_GetPrevBlock_m599980295E5B4262E6DCA1178F38830E59943222 (void);
// 0x00000795 System.Void NLayer.Decoder.LayerIIIDecoder/HybridMDCT::Apply(System.Single[],System.Int32,System.Int32,System.Boolean)
extern void HybridMDCT_Apply_mA5D9889EAB3EDB83671022005B78DFA7A706989B (void);
// 0x00000796 System.Void NLayer.Decoder.LayerIIIDecoder/HybridMDCT::LongImpl(System.Single[],System.Int32,System.Int32,System.Single[],System.Int32)
extern void HybridMDCT_LongImpl_m1097ED7B48984B2AE406227F0CC8A886625D373D (void);
// 0x00000797 System.Void NLayer.Decoder.LayerIIIDecoder/HybridMDCT::LongIMDCT(System.Single[],System.Single[])
extern void HybridMDCT_LongIMDCT_m978F17333B0A243907DBF76CFB7C07D5EAC0E607 (void);
// 0x00000798 System.Single NLayer.Decoder.LayerIIIDecoder/HybridMDCT::ICOS72_A(System.Int32)
extern void HybridMDCT_ICOS72_A_mFB43BFEA79143E11B686393C93451235154B970F (void);
// 0x00000799 System.Single NLayer.Decoder.LayerIIIDecoder/HybridMDCT::ICOS36_A(System.Int32)
extern void HybridMDCT_ICOS36_A_mDD6AB5213EE2B5B6C2684852156DE544F68A6DB9 (void);
// 0x0000079A System.Void NLayer.Decoder.LayerIIIDecoder/HybridMDCT::imdct_9pt(System.Single[],System.Single[])
extern void HybridMDCT_imdct_9pt_mA84BF5D726AD2B37BF3ED0498D79C93BE9844745 (void);
// 0x0000079B System.Void NLayer.Decoder.LayerIIIDecoder/HybridMDCT::ShortImpl(System.Single[],System.Int32,System.Single[])
extern void HybridMDCT_ShortImpl_m43980A3E5459745FAF96F17B1BC2DBC4BA370953 (void);
// 0x0000079C System.Void NLayer.Decoder.LayerIIIDecoder/HybridMDCT::ShortIMDCT(System.Single[],System.Int32,System.Single[])
extern void HybridMDCT_ShortIMDCT_mF9B502B858BC0C5A8CF34404522F50FA7A3738E9 (void);
// 0x0000079D System.Void NLayer.Decoder.MpegStreamReader/ReadBuffer::.ctor(System.Int32)
extern void ReadBuffer__ctor_mF3CC4B10337447A5C2101866F9B0415F516FD566 (void);
// 0x0000079E System.Int32 NLayer.Decoder.MpegStreamReader/ReadBuffer::Read(NLayer.Decoder.MpegStreamReader,System.Int64,System.Byte[],System.Int32,System.Int32)
extern void ReadBuffer_Read_m001E9B3F35FC00A4BC454A263964AEF64B6EC44C (void);
// 0x0000079F System.Int32 NLayer.Decoder.MpegStreamReader/ReadBuffer::ReadByte(NLayer.Decoder.MpegStreamReader,System.Int64)
extern void ReadBuffer_ReadByte_m9261A9104B001B17E4FE9CA60D1CB3285C85A3C3 (void);
// 0x000007A0 System.Int32 NLayer.Decoder.MpegStreamReader/ReadBuffer::EnsureFilled(NLayer.Decoder.MpegStreamReader,System.Int64,System.Int32&)
extern void ReadBuffer_EnsureFilled_mAF006D4F770E9B261258FAE5BC8799C19B32E483 (void);
// 0x000007A1 System.Void NLayer.Decoder.MpegStreamReader/ReadBuffer::DiscardThrough(System.Int64)
extern void ReadBuffer_DiscardThrough_mEBA73731401C395EBD4112CAF531706EB16A4D5C (void);
// 0x000007A2 System.Void NLayer.Decoder.MpegStreamReader/ReadBuffer::Truncate()
extern void ReadBuffer_Truncate_mC6F9C14C6AEF88AD07FBDE86612EBCDA716A7C64 (void);
// 0x000007A3 System.Void NLayer.Decoder.MpegStreamReader/ReadBuffer::CommitDiscard()
extern void ReadBuffer_CommitDiscard_m8976CC33D6D7DDC1AB84FC3DA2593942EAF44C84 (void);
static Il2CppMethodPointer s_methodPointers[1955] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CipherTool_Start_m1BC1C61A2276818DBBDA04CAA330964DEF75A708,
	CipherTool_LoadAnimation_mCEA8D22339C99D33C752FE2DA2A1E5543CEF18BE,
	CipherTool_SaveAnimation_m1D1BA906F27739FFECEDD64F63D3914BD93CAEE7,
	CipherTool__ctor_m414219A0986AFE3A20D1784AC1345F7E664F1DB7,
	DataBundle__ctor_mAFACFD9897E0F84D3EE54444119373F23EE89203,
	AudioLoader_Start_m1D06BEC87E773193385644E6D12EB693B4420BE7,
	AudioLoader_LoadAudio_m666D6550C8EDF3AE4420245437966C09BC448856,
	AudioLoader_LoadHelper_m3736883064E9C22D0889185745442FA473D2FEE4,
	AudioLoader_Update_m817CF291BC2BA4E9BE0C351CB1C1ADEED6911DB4,
	AudioLoader__ctor_m578F6C13779E91F8FE265716882F39D91789EBC0,
	OldGUIExamplesCS_Start_m5C679AD607A16C85DDFAE78B996120E58E8989E9,
	OldGUIExamplesCS_catMoved_m844C01EC2655FB6AEA6261F74BE3AFE380B195A7,
	OldGUIExamplesCS_OnGUI_mE08A1CA5C406551B5D66A4A6BB86E00285F97C30,
	OldGUIExamplesCS__ctor_m626C9BA19A539E78953626625960526EDB180F4A,
	TestingPunch_Start_mB88A524025639F5F9A18D9B82E98E30EB3356D06,
	TestingPunch_Update_m9B5977F7EA9B1FC0F7D69A7E202DC2FA38BBEAA8,
	TestingPunch_tweenStatically_m8E1F7A46E2EAAB6AEDD62E0C2428EC4EBAD0C7F3,
	TestingPunch_enterMiniGameStart_m2FAE7161DDC3665930335EABF53A611D1DDA65A1,
	TestingPunch_updateColor_mFA69FF82ECDF6CD2EC12354370879F019E15D477,
	TestingPunch_delayedMethod_mF8E7A6CE3054ACE65057D43047ECAB6E458C4E84,
	TestingPunch_destroyOnComp_m0D7E0E3DA9113B3B5F265EBC69AEB3CC70D63E13,
	TestingPunch_curveToString_mDE53974600DB58372C612CB078B663510C667F61,
	TestingPunch__ctor_m9A8492FDD64928449DB0DB92CD16061D642E4A6A,
	TestingPunch_U3CUpdateU3Eb__4_0_m8FB81D468DBC883BE134CF8314A05D3482BE566C,
	TestingPunch_U3CUpdateU3Eb__4_3_m542BB1341B4AECC188C74DCBE4F49CA2AB8AC4BE,
	TestingPunch_U3CUpdateU3Eb__4_7_m0D94A53B95AF073898CBDEAD58DC696E6E060066,
	TestingRigidbodyCS_Start_m936906C87BBDA6BF06D51D430EF08BB8D14E0376,
	TestingRigidbodyCS_Update_mFA7EB03B37C1B2DC3C7ABA84488484149155828D,
	TestingRigidbodyCS__ctor_mA1DCB32BECE6491D4E8B9F8E06979C6F2B775516,
	Following_Start_mFEEA46D15CD9B280B5E7F418BAB4456C730EF556,
	Following_Update_m0909311BA8C83F7F2448084BAFC093C62C012B76,
	Following_moveArrow_m6B308968FAA0842DE5E36D17DFF4CFADC87BDE64,
	Following__ctor_m47A2E77D28030BA448B3705FBD95D3EF1C0EF5DE,
	GeneralAdvancedTechniques_Start_mA1FF0FADF625671F926CC2E4D0CB77B979301564,
	GeneralAdvancedTechniques__ctor_m54D6A82A08BC2E749DA9363763E710312B79E899,
	GeneralAdvancedTechniques_U3CStartU3Eb__10_0_mBEBB18A1DC341C34F99BEF02138E30A577DC63C0,
	GeneralBasic_Start_m722ABC7DD46AD33613ED9A99550009B1388A2478,
	GeneralBasic_advancedExamples_mC9210C18842768B7D2E99936EC766E18D85C8773,
	GeneralBasic__ctor_mA0A6D418EBB951CF5EF97DCDE5E62CCC6D85D31F,
	GeneralBasic_U3CadvancedExamplesU3Eb__2_0_m6A1F674D4183516100E64513ACC50585EBA731E8,
	GeneralBasics2d_Start_mE3215316598A87E2CF0D9833F686D13FDC86DF2C,
	GeneralBasics2d_createSpriteDude_mBB652175A3D6AE7952BBE8C0E1226864D79928EA,
	GeneralBasics2d_advancedExamples_mAC58CC5F094D9E634ACE4F04656C6A484C6D52C3,
	GeneralBasics2d__ctor_mFEB91E190B0738BC50845B33FAE252A2B41D55C7,
	GeneralBasics2d_U3CadvancedExamplesU3Eb__4_0_m05957C9477E9A908C2EAF3BD0AD001EE3DC0D881,
	GeneralCameraShake_Start_mB1EE5CBBF521F4D697F8EAF2F53600826F54DABA,
	GeneralCameraShake_bigGuyJump_mE82096DC9C47AFAB293E2B8BAD0B7F2153063681,
	GeneralCameraShake__ctor_mA22AEAD53E34A074E7DBC77BC3164C64814270D8,
	GeneralEasingTypes_Start_mBDA1C8799607E8C3D6052D0B88E5E96ED8892184,
	GeneralEasingTypes_demoEaseTypes_mAD63F64427B735F85C75265777B1EA2C6038B422,
	GeneralEasingTypes_resetLines_m46F8B9B5ADFB7348E099993918DB8EAA1108B746,
	GeneralEasingTypes__ctor_mB2B0932931E070FA4BA5C24CFCFBBBF894E534BB,
	GeneralEventsListeners_Awake_mBAFA4AE2A5056FBFED34D12A4776985220D66AF7,
	GeneralEventsListeners_Start_m6B12B5EE52523BBA2008F17CA57A49B0ECCB619A,
	GeneralEventsListeners_jumpUp_mC8BCDFB7169C9F7EBC42A5860DBB0BAF05612E70,
	GeneralEventsListeners_changeColor_m6FBDF19D2457EA40303B510C9B9C4206943A8D09,
	GeneralEventsListeners_OnCollisionEnter_m43F8FDFAC97EC09E53E98849F68469FCA7881F5F,
	GeneralEventsListeners_OnCollisionStay_mC15D1654A132C57A85C90F0A9D1A0489481B20D2,
	GeneralEventsListeners_FixedUpdate_mF30FB01A225EFB50EB4714A0110DD6668F9715D0,
	GeneralEventsListeners_OnMouseDown_m7961A64655D9C0BC7401ACCD7A54741F5440C659,
	GeneralEventsListeners__ctor_mFE5DA13A6BE436986ADB09018AC3C641A1DD5A3A,
	GeneralEventsListeners_U3CchangeColorU3Eb__8_0_mF7603717CE44B39A97A3F80DC96F80E43A2DD33B,
	GeneralSequencer_Start_mE22252603584E9595014C8CEAC0006FD9B55DBE7,
	GeneralSequencer__ctor_mCF1E29ADF0F70414A63D0F950D0FAB09AB1F0C8E,
	GeneralSequencer_U3CStartU3Eb__4_0_mCC4F15FF69FEEA0BFB73E8E2542C8910D2FAA220,
	GeneralSimpleUI_Start_mEEF7BDC999733A706FE7C03F54C8B4B83243C221,
	GeneralSimpleUI__ctor_m9A7A8639A7F2EE5FEAF39E2B6232FA533F58E1CC,
	GeneralSimpleUI_U3CStartU3Eb__1_0_mD28AC7A7223ECEB398951A2AAA561A4B9DC6FB66,
	GeneralSimpleUI_U3CStartU3Eb__1_2_mA798C3D21C7EDE1BF5F133F6946478FDCE1B5CA8,
	GeneralSimpleUI_U3CStartU3Eb__1_3_m759D56CF0CDCA8E27EDEB8BF968FD00AD9932824,
	GeneralUISpace_Start_mA7F9726366918F04A038C11E04A5230F0C4AA41D,
	GeneralUISpace__ctor_mD9DD0C8F2377084F9DC2EA074C24E93DF1A3518B,
	LogoCinematic_Awake_m72CA9A35497E16D50121250F9BCF15F3BDF65138,
	LogoCinematic_Start_m5DB42CBB808A9C41D32B440B65FEE9F35E1FF86A,
	LogoCinematic_playBoom_m1359C13981E4254A26839968D26F0F96E6033E4E,
	LogoCinematic__ctor_m87A3D83E1047FD2818C5319C7462F2475468743E,
	PathBezier2d_Start_m6101865E419D574B55D90AFC87D59C61E2FE6A5A,
	PathBezier2d_OnDrawGizmos_m5AF15DD02CC758B94C6988CA1084C7D5C03FC721,
	PathBezier2d__ctor_m57F2349CACC84E24CFC8D242C81E3DE948CC357E,
	ExampleSpline_Start_mAD9D6848447333E450D9991F68BDD989B6AE6BFC,
	ExampleSpline_Update_m0C06D2C4C359A2AA1E1EEE9C4A7CE76B10A774F7,
	ExampleSpline_OnDrawGizmos_m1EC31ED5019C6CAAF409DE171F1D46631C4FEB4B,
	ExampleSpline__ctor_mA777C76A3F034C55804819D09FAD803C22B6945A,
	PathSpline2d_Start_m322D034A9E1AE896E48FD9C3A83D81E298B101F2,
	PathSpline2d_OnDrawGizmos_m45503986AD7ABD0B14F69C276092F5E99F3508E0,
	PathSpline2d__ctor_m0C4B7813D0D578B5CAAAF5D2963ADF0E699583B8,
	PathSplineEndless_Start_m0D63B5D9F0A5A8953829DBFF1AB80AD1699E5858,
	PathSplineEndless_Update_m2168C1E36EB175C86685B1D6438244B9905B0D55,
	PathSplineEndless_objectQueue_mDF89CDC0D51CA6B9A84F700BFD011EA5D53D0B3C,
	PathSplineEndless_addRandomTrackPoint_m2A6DF9D7DC969FC9E3E269C8F6815BDB1622552E,
	PathSplineEndless_refreshSpline_mE7C260A36EBDE07ED250D752CC2AE13F9640BF84,
	PathSplineEndless_playSwish_m8DF90B3209DEC3201AD432B013B8CE3D665E70F3,
	PathSplineEndless__ctor_m936283AAD1B2ACB8A975A6C5F44E5C49F235D889,
	PathSplineEndless_U3CStartU3Eb__17_0_m6457B9A5A1966F3A5EB50724D4F867D6AB05BDD5,
	PathSplinePerformance_Start_m1DA9D6816079EA25341277C76A317D94B4168EF3,
	PathSplinePerformance_Update_m440225D51242DD97344316E466389BB633099B3F,
	PathSplinePerformance_OnDrawGizmos_m963F417D81A92D1D1260EB172050FEB2C95B9E06,
	PathSplinePerformance_playSwish_mCDD5986D88F473C4C789EBB08013D5C4371B8934,
	PathSplinePerformance__ctor_mF91B2042C6950B108ACFC3CDB4FA0F2F6545377E,
	PathSplineTrack_Start_m281BC1BE6C44BADFA80DA3D5C0679D34FEBADE1B,
	PathSplineTrack_Update_m789CE9A3C91D635B04AE45C77ADC552CEEB0BE3A,
	PathSplineTrack_OnDrawGizmos_mF8A6C592C8E177043453FAFA09F3917D133E82FC,
	PathSplineTrack_playSwish_m166E8F61FFC8D4DE30A55839EE95413669489D99,
	PathSplineTrack__ctor_m3D4ECADCEDAC59010FC4B6576F49BCC3083E8DBA,
	PathSplines_OnEnable_m9A31A2680F7A3A5A786B007758F6B68DEFD47CBF,
	PathSplines_Start_mBB7ABDB218B114C164C2E61B9BD469CB8D5E76D6,
	PathSplines_Update_mFAFAB73B81EA5981B4EEC5C507B72A52D49E593C,
	PathSplines_OnDrawGizmos_mD3B3EE01513F4BBF82A00F87DBA40C283384C5EB,
	PathSplines__ctor_mFFF59F5DECB94D08ECA1765F4AF70F15F1D0F807,
	PathSplines_U3CStartU3Eb__4_0_mCDFF93FF0B96E66451EDB0D037E46B7F6A0B0D04,
	TestingZLegacy_Awake_mBCE69D28EF4B5C8DD9623C93150A62690FF6F560,
	TestingZLegacy_Start_mF7B3C1D0359654FACF1A636ED276DA2D7AC2F238,
	TestingZLegacy_pauseNow_m655EADF071BB3053B444C84C0BE35675AD971405,
	TestingZLegacy_OnGUI_m8736E68E13F34D38E820CBDF166DB67989878F22,
	TestingZLegacy_endlessCallback_m9AEBB3556FEF851B1F6DB08DB8050E8D89D45D17,
	TestingZLegacy_cycleThroughExamples_mC9AF8FB1FED800DF92B00E6C482380B1ED2EF4A3,
	TestingZLegacy_updateValue3Example_mCA94016385AF00299FD43C7778501DB3F51E8786,
	TestingZLegacy_updateValue3ExampleUpdate_mC0566B31227122FC6140F7704C48065E4AC046C3,
	TestingZLegacy_updateValue3ExampleCallback_mEA31AFEE5C74647804BDE02889A38307D8096B4B,
	TestingZLegacy_loopTestClamp_mEC3D64101ACA122E2257ED726DA36D5E7E2EE6DD,
	TestingZLegacy_loopTestPingPong_mC9FC3BFBB6EB66050EB24EA3623777201C131F91,
	TestingZLegacy_colorExample_mDCABB46692619BF22DD41425D03EA835F6024AC2,
	TestingZLegacy_moveOnACurveExample_m643B475C72457BFC0580809D25206A474786A1AA,
	TestingZLegacy_customTweenExample_mE863F3298241D782EBCB333A93B882C572C8F9E6,
	TestingZLegacy_moveExample_m8CB97B6FAD6C66EC740EB31D539F78BC5930F02A,
	TestingZLegacy_rotateExample_m09C24A6D46978C652A6D8F321F9A071247462DD3,
	TestingZLegacy_rotateOnUpdate_mDB551FEC7A3BC50E5BFCB3A4D86EDBD71343FE97,
	TestingZLegacy_rotateFinished_mEE1B541D31B1E9388A575AEF8FECC8D8FDD3605F,
	TestingZLegacy_scaleExample_m8683640627D1DF303D1A55A59C476198A6D5D7D8,
	TestingZLegacy_updateValueExample_mE0A9F76605CEA59FA59357CE4C5313A32801AAF4,
	TestingZLegacy_updateValueExampleCallback_m4CB6A988D0383234B578C9465B40085A962D92EC,
	TestingZLegacy_delayedCallExample_mB11B32B256519D1ECFBB54216CEB46E7719BE5E9,
	TestingZLegacy_delayedCallExampleCallback_mE8E45FAD4B3C8A46BA856C0250AF6A4F6606F20C,
	TestingZLegacy_alphaExample_m7DEE863B73235D62D7FACC183D9B69A9C22C68F8,
	TestingZLegacy_moveLocalExample_mD7173FDBB36229E28B108F6AD5195F253326F3B2,
	TestingZLegacy_rotateAroundExample_m2D08EC29B26084F4B7D22AAD9D8A4453FFB9659B,
	TestingZLegacy_loopPause_m55A880DF0CB7F8737EAB6CB0202121B258759EE7,
	TestingZLegacy_loopResume_mE7636F1996095F5B17B155C5BA0350DDEF222F8D,
	TestingZLegacy_punchTest_mE2E20734BDEE2A30E5F643FDF84FE56BD99ACB49,
	TestingZLegacy__ctor_m54DC8DE5EF69E156FDDF1C34916397E68B0B9F2E,
	TestingZLegacyExt_Awake_m97F85D3DD812AE6BFA4E59E5BDB074A11DEFB74D,
	TestingZLegacyExt_Start_m3FC22E42B4D1C512B8F76260CF90F7584CDE78C9,
	TestingZLegacyExt_pauseNow_m6FA2ACC29A279AFFEAC9C1117C615E692109E47E,
	TestingZLegacyExt_OnGUI_mE335B55E2688BCF03B84575D64CE6905E9EB390C,
	TestingZLegacyExt_endlessCallback_mC91F80F402003198552BB5A47773AE91C1CDF3E4,
	TestingZLegacyExt_cycleThroughExamples_m7ADA1FA87F2373C6CA2F710EE09E07C6E6B48DA7,
	TestingZLegacyExt_updateValue3Example_m56A718E3C7178DB0C59E3E030EF2007D60168950,
	TestingZLegacyExt_updateValue3ExampleUpdate_mBA7E2C03C01B5CDA6C4DF5EE4E5521FF72644235,
	TestingZLegacyExt_updateValue3ExampleCallback_m83AD25E3BEB33C7F57C68FCD9C1EA9E27F762651,
	TestingZLegacyExt_loopTestClamp_m402BD2B45CDF06FA5F47E43EE139E3D593D3837E,
	TestingZLegacyExt_loopTestPingPong_m88A111C6F9F4C97A89F6E141862BD22F1723F1A8,
	TestingZLegacyExt_colorExample_mD487DAAD856F9B4E044869A56A9E83F0E5CE686D,
	TestingZLegacyExt_moveOnACurveExample_m76FBF8744FDD1EDFA144DF0B05F91F341DDE7C86,
	TestingZLegacyExt_customTweenExample_m7FD53EA42C543F5CDE51BE17B7B1714B078A3671,
	TestingZLegacyExt_moveExample_m877A1E1840D36A34C7655F995471BA952B64FC2D,
	TestingZLegacyExt_rotateExample_mB882491A5D47DE36B743560863B366C88CF69A2D,
	TestingZLegacyExt_rotateOnUpdate_m60EEE824C1EBA3FA12C60B3B23B6144739697A25,
	TestingZLegacyExt_rotateFinished_m6885AB18A26592B3844455D756D70BDC20F257E8,
	TestingZLegacyExt_scaleExample_m3D54AA1EEFBA37C30889E34AFBB350316CEAB41B,
	TestingZLegacyExt_updateValueExample_m0A08F0B235AD41CC0C4153B431862904C81799C6,
	TestingZLegacyExt_updateValueExampleCallback_mD07695C332FF263FCFF89F4B305C96A02A786729,
	TestingZLegacyExt_delayedCallExample_m530FFA75198C880599CE65E105D061B133E172F9,
	TestingZLegacyExt_delayedCallExampleCallback_m0F8E4A9EC78F99065D1B519DA641FEB9B4A686A5,
	TestingZLegacyExt_alphaExample_m6FEFC52F975614F51524FB7B91547C791FC1A982,
	TestingZLegacyExt_moveLocalExample_mD509C90ADEA3546144652A770965A5C98F88F3C2,
	TestingZLegacyExt_rotateAroundExample_m4507C9CD41E678846AE918C6D796A4BFC43EF056,
	TestingZLegacyExt_loopPause_mC23D0D95589F74EA510A9E79D35F28C7418F514F,
	TestingZLegacyExt_loopResume_mA3FBE26AE1243FE2178C55BEDB45AC6763C42FF9,
	TestingZLegacyExt_punchTest_mBC5D8A5FFF359CFAFF16BFD89387B56B571DA001,
	TestingZLegacyExt__ctor_m918A02678AF6D9E398E17E98FC792A73E8205542,
	LTDescr_get_from_m8E7FF7FB5FE65C313FE2CD26C8B1BBAC00BFBDC2,
	LTDescr_set_from_m0C7AA03B3869C1A3B6EEB8ECD16FF0311A19B5FD,
	LTDescr_get_to_m842DE271937A3E21367D57D4C1A963E98E0AF5E9,
	LTDescr_set_to_m720DB590EE8641E7091375EE626ED38CCA36C63D,
	LTDescr_get_easeInternal_mEDF89A6F240F4770536F1FBC0FBB79A6A55C7BBD,
	LTDescr_set_easeInternal_m16C7B8635E5D2FB3E31EBE721E954967EFBF30B7,
	LTDescr_get_initInternal_mE336562DA52E756130ADBB07B3CEF2D7E7EB01D4,
	LTDescr_set_initInternal_m0753D24B8455857620589DBBE1EF095A2AFE489B,
	LTDescr_get_toTrans_m2B06D495FA4BC4F44D1EAD32F163D62BCC457B5F,
	LTDescr_ToString_mA312106BDDBB7E356B88C05BF8C767C63964CCC6,
	LTDescr__ctor_m975E0C19B34E8078C70AFAE95A581777B8D6F4D8,
	LTDescr_cancel_m944E66783EA1A1BD7781AFE7C7EF7A6117B56930,
	LTDescr_get_uniqueId_m11CA85564A0B0C9BE7FC551ECA3A05111888C7B2,
	LTDescr_get_id_m76232B8A21BCE44149F9014942BD1C63F5C6B4E7,
	LTDescr_get_optional_mEA6CD7BE31135EB1F2310490F9FAC346767B3020,
	LTDescr_set_optional_mF78587FE46E7E26D9742F3F156DC92DA51AEA2EE,
	LTDescr_reset_m16BED571328E24D5385BDA9E4E478508EEB74664,
	LTDescr_setFollow_mD35310187FD79F81851CD3635C0376BAAC9EF9FB,
	LTDescr_setMoveX_m3B1BB06BE81F8DF47CE145148ADE5D2E63B7D630,
	LTDescr_setMoveY_m62E79AA722E091918379FDA5255DB3F517D447F3,
	LTDescr_setMoveZ_m5846FCA035E0BC391028178FEAEF74B0C31C998B,
	LTDescr_setMoveLocalX_m1DAB1F3CFCF599184B907273B7EEDB58C319177D,
	LTDescr_setMoveLocalY_m3EFB302B1C5A842539CCC7D972836D4AA5BEF9C1,
	LTDescr_setMoveLocalZ_mBCD523EEE6FDE11FBDF9B291FA827EFCAADC055F,
	LTDescr_initFromInternal_mA4604276862B5C16B4D0A1F323C5031D37CBE125,
	LTDescr_setOffset_m29ED0A18B591990BBCC94E36A22426FEFF0B6F5F,
	LTDescr_setMoveCurved_mD2FC79FD2FBC09320F29D253187B0FD5E1E3F242,
	LTDescr_setMoveCurvedLocal_m5B19FE68C93FBE105E3D02DA4AABB28183218DA4,
	LTDescr_setMoveSpline_m483C9060295F816DDB9F363B2B7D3C7EB2BC709A,
	LTDescr_setMoveSplineLocal_mE23B6AEEBD002D6D811758CB415DBB8559505DB2,
	LTDescr_setScaleX_mC50D44B615017A9DEC47B21746DA49E6907C2106,
	LTDescr_setScaleY_m3980093C790F98A85A697A6CB76A08CE258A1831,
	LTDescr_setScaleZ_m1E3D4F892EFB3F884EB006D6314CEAAE2E86B788,
	LTDescr_setRotateX_m9710079249D050C4709F38FF212A8C5A064141A6,
	LTDescr_setRotateY_m0A6206F1BAC1EB1E74373C866BB993A17AC474C7,
	LTDescr_setRotateZ_m88EB19296F3A5A255B0DDAD63F5556854A3AA6D5,
	LTDescr_setRotateAround_m5C916E21674683F454CF66A50F1B52688EC5A72F,
	LTDescr_setRotateAroundLocal_m26621B8F8199CF50CFD97CF32E6DF271543CE142,
	LTDescr_setAlpha_mA82AF444875CEBDC11AF76A6DA33F8832070A7C9,
	LTDescr_setTextAlpha_m140059336BABDBE97F7B9EB26E7B07A6A2FECCCF,
	LTDescr_setAlphaVertex_m4BE3949864A804651A8E884317090AC0BE9BA543,
	LTDescr_setColor_m677D553334BCD65A976646538D2E3FB901FEB2F8,
	LTDescr_setCallbackColor_m8ED344DF8DA4A35A9323090A9705F338CCEFA6BF,
	LTDescr_setTextColor_mB0E1342E1C5604929D621039E74EB5C968E63CA5,
	LTDescr_setCanvasAlpha_m883942EF674A4FCF4C0621D8BB36F1EB78D9465E,
	LTDescr_setCanvasGroupAlpha_mA08B850710279DF7259A9EA457614BABD29EF28C,
	LTDescr_setCanvasColor_m8D0A659A765604171C1ECE12ACB617319E179015,
	LTDescr_setCanvasMoveX_m012C7901BCEC98126783ACDB01B088297D30EB69,
	LTDescr_setCanvasMoveY_m8124C7761145134D07245BB3031857695501B9EB,
	LTDescr_setCanvasMoveZ_mFDFC0D71E22EE1097FDA8A2A6F0A9990C5403D6C,
	LTDescr_initCanvasRotateAround_m1205028161B594D223E69013F8C92B766CE75959,
	LTDescr_setCanvasRotateAround_m7E04711CE56F7BB9D43E27A0102E0C3C7FDCDDF9,
	LTDescr_setCanvasRotateAroundLocal_m7BA851F76908BF978A682D4E49CD2DB770F27F55,
	LTDescr_setCanvasPlaySprite_m3CDD391E0DC1AB5B60FC4A01F1527184062E0202,
	LTDescr_setCanvasMove_m84559A096BC7A140C8CA25DE9DB766D537C9A148,
	LTDescr_setCanvasScale_mF06739A6DC21077E3EB7704B3997CB495302F134,
	LTDescr_setCanvasSizeDelta_m6C41930BF6F585DCDDF31EB4B540F3C3163DF0E3,
	LTDescr_callback_m9B45B1DCD83B9880A61C16C6C21F0D05C7C6AFA7,
	LTDescr_setCallback_m7BD2A7CFAC15C8D9D992E0521B819B41FD9A6CB3,
	LTDescr_setValue3_m553FBFB157A9765053F9FB7CF08B59A1E3D976D7,
	LTDescr_setMove_mDEC044497DD6398395A17A83EDC32ABEB3F8DA88,
	LTDescr_setMoveLocal_m4B75F54B72038AF2971A73A8A6E905DE94AA6346,
	LTDescr_setMoveToTransform_m9C07DB063E64D2F9A91054DFA08B1C2594D22F88,
	LTDescr_setRotate_m37FB3B6088196BB2A0DD48105125989468400A5B,
	LTDescr_setRotateLocal_m94A88F95DB9D3BD078305AB3A25039966CF4147C,
	LTDescr_setScale_m003B5242EE6667C4F5AC57C5C06A9A32007F0FBD,
	LTDescr_setGUIMove_mC25E34C2B27930D956DB92F12FF79F25F7D78E9E,
	LTDescr_setGUIMoveMargin_m96C670A2423CC1E4A2D02021CF0F5CFA51A8E14B,
	LTDescr_setGUIScale_mE8BA06B69FFA9FF14F0C7CD3FB76AE2F12283B0D,
	LTDescr_setGUIAlpha_m964F89CFFC7D20A97EE78E6340C9F8236AC2DE6A,
	LTDescr_setGUIRotate_m9B46A83C43BAFD1DB0C5629D850F1D3424B3AA7B,
	LTDescr_setDelayedSound_m6223261128689E6768A694E089DE42B67A2BC026,
	LTDescr_setTarget_m96F2398CBF82B65C2BD373807C75C0E04A39DD29,
	LTDescr_init_mB964AC9BD3406D19362D8675474F4DC99B33347B,
	LTDescr_initSpeed_m97D3A943AA4B2F25ABC91FC5C5C055611799AE16,
	LTDescr_updateNow_mB7468B5F37D19C21184A2A885C1DF0A55E1F1109,
	LTDescr_updateInternal_m2295EAEC09FDEF9AD71EB618734DE0DF5570D2A5,
	LTDescr_callOnCompletes_mE047FE764250E4071E38197BC1B29221B87B8B4B,
	LTDescr_setFromColor_m84DCC218D18266CA0D5167AE546CF54EBF2D38B0,
	LTDescr_alphaRecursive_mA15152C74BCA8D8CC498BF7A84A0481A35AA29EC,
	LTDescr_colorRecursive_mA38B1CB2A51FECCCF49F1D1F50D98312A2296E0E,
	LTDescr_alphaRecursive_m6BAD882CBD0900B2ED3ACD96B49E1AE50DEA791D,
	LTDescr_alphaRecursiveSprite_m64AA415027FB1434EFB1E006721B206F47CB1ECA,
	LTDescr_colorRecursiveSprite_mFE805A4F826EC3CFF84CB6E86E43E857B934897D,
	LTDescr_colorRecursive_mE714E3CB2CB79B4F6C40A0C0A5CF648A5C1E3F94,
	LTDescr_textAlphaChildrenRecursive_m99FA1EC22F4CF7A7743038CF8369298B1F3ADAF2,
	LTDescr_textAlphaRecursive_m8EC23765F7904CBA0C35734A7D63F58C879C577C,
	LTDescr_textColorRecursive_mE39D07578A5FCAEE8336AE9D1DE9E841B98FD90C,
	LTDescr_tweenColor_mD7F8129343D2677EC17D5740E160A88540685140,
	LTDescr_pause_m516CADEFB418399C0A796883DB35D183F8FBD9D3,
	LTDescr_resume_m1D572322CCB20396E715EED46A1692A6D4F81E80,
	LTDescr_setAxis_m6785FFA919508809C7A0A5FB7A998E46E1CB22D1,
	LTDescr_setDelay_m78E34545BB3EEA43ABF9CD5D29C8FB1CC3D4F464,
	LTDescr_setEase_mCF284E5CA17BB1BECBB5B1CC1013571B108580A3,
	LTDescr_setEaseLinear_mB1944DC5601584DF8F68F2FD1E844526B4B57D0A,
	LTDescr_setEaseSpring_mCD7F60BDFCE5BFF617264B8775C4E80ABED4A465,
	LTDescr_setEaseInQuad_mC03167FBB2CD3CB171D87440DC20604F51248C6E,
	LTDescr_setEaseOutQuad_mC87008E2DA94BFA521BB12A74F4FC0F160ED1291,
	LTDescr_setEaseInOutQuad_m22C85D4E11EA5B77869864113758310F097CFDDD,
	LTDescr_setEaseInCubic_m0D93D024F3F116E6A7887F86F0F5203D74FAF5FF,
	LTDescr_setEaseOutCubic_mEEAA2F960FB30E6B84B3D52BEB173E406A0A8A6F,
	LTDescr_setEaseInOutCubic_mACAC52E920C0602EC0D90E6019B3EAE9034D25F9,
	LTDescr_setEaseInQuart_mEFE37EB2D9BB775BAC45A1D55CABB0D41FA679F0,
	LTDescr_setEaseOutQuart_mEAD18F5546A6BE77FE48294B5BB316769338A461,
	LTDescr_setEaseInOutQuart_mAFC9F8155E7348004D99107EE10C10AD51C526B4,
	LTDescr_setEaseInQuint_m8C116A004899207382401BCADC6F39F4EE3B75B7,
	LTDescr_setEaseOutQuint_mA59F24802CD0E1007E659D34F533F8F1CA7FAD13,
	LTDescr_setEaseInOutQuint_mF815B5687F9162E45C9D1AD031C317840712FE19,
	LTDescr_setEaseInSine_mF481D539F3DA2FF9AE13F373BB79B2CC93AE82B6,
	LTDescr_setEaseOutSine_mD8439D9AD162893667913F82632A087BB7BF66D0,
	LTDescr_setEaseInOutSine_m1647757426F728F0A076E73DCB939C469B77E1A9,
	LTDescr_setEaseInExpo_mE27C2F90670FA473FC8D27DD70D3E4F47A4935C2,
	LTDescr_setEaseOutExpo_m9220EBCED76070AE7B72E997FD5711A68D35371F,
	LTDescr_setEaseInOutExpo_mA4B0D2B2F98FDD2C5864ECA9000CADFBCE38C8CC,
	LTDescr_setEaseInCirc_m75ABE78F120AC863C1BFCE3426ED6940CA9B30BD,
	LTDescr_setEaseOutCirc_m2728C6194974BE6C02932FB34B991EE2110FB14D,
	LTDescr_setEaseInOutCirc_m75B555F1A463C9A3B59D2DA7642B9A7AD36770F2,
	LTDescr_setEaseInBounce_mFC166FCC2D1359368A01D60AD95F87880E89E22B,
	LTDescr_setEaseOutBounce_mF118AA6B5601E83ED52CCDE9A0EA4C92F30A5626,
	LTDescr_setEaseInOutBounce_m1268040122DB6FA1269D298171FADB03DDE8BA9E,
	LTDescr_setEaseInBack_m3220F0CC60207776341D8612292803E58A8D2175,
	LTDescr_setEaseOutBack_mF300DC1C059B5BB6CCD5BFC2CC71B665334AD717,
	LTDescr_setEaseInOutBack_mCEA5FB6CF2A0A6335CF1F9F06177A4B663CE1F47,
	LTDescr_setEaseInElastic_m1FE80F0CD5ABAA68172CE15DB1280788BB53B440,
	LTDescr_setEaseOutElastic_m0AED6CBCC243E3EE10DBCE0973F32BF5DD16A873,
	LTDescr_setEaseInOutElastic_m494EE77E1C98A7356847D0B44E4D088A5ACC1F6A,
	LTDescr_setEasePunch_m7B72691172462D818F06010F316087DBF5EDEB49,
	LTDescr_setEaseShake_m6AE6595FBDF8FFB68838D6D20B29C64EBAC941E9,
	LTDescr_tweenOnCurve_m84B46DFEB19FDB6D772DE2D47B4366E0A706C4DB,
	LTDescr_easeInOutQuad_m36065001F18BC0B29C29EBA965B08989B028234F,
	LTDescr_easeInQuad_m802EB621FB9F8EF34C4DD060972E506C7D6CD1F0,
	LTDescr_easeOutQuad_mA2A1E36372772F90ED90F59E97062FE128B6794D,
	LTDescr_easeLinear_mAD6CECCB459D27BE9B3ACEBDC2F7473F6A688A17,
	LTDescr_easeSpring_m9D0A0D6942FF7A2D9DE855C19487D7E9ED6F26E7,
	LTDescr_easeInCubic_mEF13536017E3D96BA908FA0C37028D3BB7FD7304,
	LTDescr_easeOutCubic_mD5A8610D69B8116750EA64A02DFAB3314EC11C78,
	LTDescr_easeInOutCubic_m2F441E0BDA04412B6746865E4D810E30E830C0E3,
	LTDescr_easeInQuart_m865902CA1856CE060535F26E68FC3E48A1B9EC0C,
	LTDescr_easeOutQuart_mE87CA7D4DF0135B45C8536A8FC74D4CDE8EE1357,
	LTDescr_easeInOutQuart_m6D290AB4B5EDF285E1A4AD1945B87864B810E672,
	LTDescr_easeInQuint_m3A8922E83FB7FD5921895D0C7C180B251E0D8D87,
	LTDescr_easeOutQuint_mA84CCBDB25A1E58D82C7A446850ADDA258592A0D,
	LTDescr_easeInOutQuint_m73C03ABA976E0D1D40E8835BE9A1FE5DBF7F640B,
	LTDescr_easeInSine_m0608A3B0E9C83EC723B9FB55149DB9837F51AB41,
	LTDescr_easeOutSine_mD364D0BBA67752AE94B5752213507D91A41C3CF2,
	LTDescr_easeInOutSine_m5817005F731481A8FB1B8B88E63319B0C08E27AA,
	LTDescr_easeInExpo_m8975809F06D63CBAC26760C7243779393D7FCCC5,
	LTDescr_easeOutExpo_m1793556A051CCF34AD78DCC5FEE153CFEDC857F2,
	LTDescr_easeInOutExpo_mF6009DCEAA7D84D5E219F93448BF38340917DF47,
	LTDescr_easeInCirc_m7F525DEA40803C0025FAC510774C26F86AB100DC,
	LTDescr_easeOutCirc_m78859138DEAF3005D845A924841F0EA8BBEAD585,
	LTDescr_easeInOutCirc_m16338C8496BAE07CDE37BDA8FCF813BFBA7F19FE,
	LTDescr_easeInBounce_m16D1EE3AB1D5E67FEBE2EA6DBE9C95A92F084C3F,
	LTDescr_easeOutBounce_mC4DFF3C3FD156884B92838BBABB56570EFDBADFD,
	LTDescr_easeInOutBounce_mFD18B378E248EB26725EEE7267C20BD3FBED946C,
	LTDescr_easeInBack_mD2D30FEF3F06538124A2AB1775BCA2CED07256A8,
	LTDescr_easeOutBack_m744F4CCCD32F8F556B68C9577E67B0468BEA3DE8,
	LTDescr_easeInOutBack_m57DB03AEDB85C66A5925B8FF6F15B7C97E2DDFC2,
	LTDescr_easeInElastic_mDF8A4B5CDAAA652D432B99F536FDE5F12D29CC13,
	LTDescr_easeOutElastic_m8D73182BD8A48500C5FB89BF4AA84094CCB62F85,
	LTDescr_easeInOutElastic_m3BFEB7C3B52B2C061544C8C2849BB7AD000E341C,
	LTDescr_setOvershoot_m0E935F8EBE21ED62F29A62DD2FC4E7C1378E0544,
	LTDescr_setPeriod_m6D8399405BD7E4C3CE73E90B1DF55991A6EEF6B6,
	LTDescr_setScale_m75F4440E58BAAACBE7205441A2FDB96E61AFE2AB,
	LTDescr_setEase_mBF09981854FEEF635C7CD97C0CBAB2A941193DC6,
	LTDescr_setTo_m829E53D1E4FA8D3C11D35CC8E736AAAC151A8234,
	LTDescr_setTo_mB3C80993607B61B638F5EC5D773CA42AE33CDB3A,
	LTDescr_setFrom_m869544CF9DE9E93780E6ADAED4B99CD3580058AC,
	LTDescr_setFrom_m916CEBF6049D5ACC8C00337FF34DF141461951A2,
	LTDescr_setDiff_m4E9CA4C17BEAC2C76720E33912216FE25D816E5D,
	LTDescr_setHasInitialized_mDCBCCB7087C97B20C58181593EB1C7A8587C84C6,
	LTDescr_setId_m4F5E561450C9DD9297F9C0A4920E7F56A57BA8F6,
	LTDescr_setPassed_m629829107456CBBEA75E743A244D5A8156872ED4,
	LTDescr_setTime_m9DB98DF42D1DE55F45346D9F4F0C0B836EF5BE82,
	LTDescr_setSpeed_mD51B284F6FEC68EACC2216BCC1CAE20588B88596,
	LTDescr_setRepeat_m39C1FFE0F3BE7AEA0AB2D3D09B8C5E93471FD528,
	LTDescr_setLoopType_m432119442AFE9EC9DCA026A196ACB390CB6E490A,
	LTDescr_setUseEstimatedTime_m1DCE69547A9FDA123D41FC468334AAB464A9E27A,
	LTDescr_setIgnoreTimeScale_m74876A4244E7C476990FD9F49A32F2077733D10E,
	LTDescr_setUseFrames_mC5C200981C6D66E22A194C46E4E68F4DEE93A583,
	LTDescr_setUseManualTime_mA583DBD31325B81D677BFDE10848D522F3B13917,
	LTDescr_setLoopCount_m5F4204D6F059EAB72ECF645AFFF1909CE328E9A7,
	LTDescr_setLoopOnce_mC7EF69D620AE0723DBF4F7786DDE722A9CC46914,
	LTDescr_setLoopClamp_m26C0A8ECA2F70ADA1C48A2C92B13B47B58E2B1B0,
	LTDescr_setLoopClamp_m45FC70613BB57B5B72A397242071368CCC539E35,
	LTDescr_setLoopPingPong_m53E03879D46B899429F463427A5EACEE747F9636,
	LTDescr_setLoopPingPong_m21667D1F3685ED55D706C025D48849E61D655B0E,
	LTDescr_setOnComplete_mDCB57CB9AFEC6C5FFD63A9B89962DEE80BD1E72C,
	LTDescr_setOnComplete_m63F5635BC23F15B3B64960290B0D723D36BC1030,
	LTDescr_setOnComplete_m2D493BA5F77149765D519E685C05ADEC2F9EE5E6,
	LTDescr_setOnCompleteParam_m4172EF1D7B63D5B2BA5B4358BB8282FF55A5D9C2,
	LTDescr_setOnUpdate_m437DAC29C60F0FFBE61C926313DF48FBB1BBDDE7,
	LTDescr_setOnUpdateRatio_m004D6332C968E0F0059FF807E34770989AA009F6,
	LTDescr_setOnUpdateObject_m770A74E823CD5DB5B6448F7A66DED37B968893D1,
	LTDescr_setOnUpdateVector2_m7FD189610C25124F725477B4DC948F11A69455B1,
	LTDescr_setOnUpdateVector3_m0DDC2E6AEDC0E0EACE3947E4E67A6A6803EEFABA,
	LTDescr_setOnUpdateColor_mDAB53EF324EB204A7038AC70A88978C5E93D8009,
	LTDescr_setOnUpdateColor_mF0B148249135750B5B772E6FA0EFC59A9925E69D,
	LTDescr_setOnUpdate_mDFE9026BDBF9479028A6D2DF03C3F9E1C391E354,
	LTDescr_setOnUpdate_mA5CB1382CE68571CE5DB619406AC5C6EB22729B9,
	LTDescr_setOnUpdate_mEFD57B3855E8C131BA76DADD3FD13C41E867F58A,
	LTDescr_setOnUpdate_mEFD26172DE9342C6C0FB34248E3EA7F6267A1968,
	LTDescr_setOnUpdate_m73910A7E30781DFDE8149BF06376A3CB98DDF74B,
	LTDescr_setOnUpdate_mAFDE1872327547292A82B3D393F4B7007913EFDB,
	LTDescr_setOnUpdateParam_mB449256AC309484AE7FF7E1E685A8B6B5727D4D0,
	LTDescr_setOrientToPath_m6458FAA1A9AACC5BA13A1C83AEB87A63883CFBE4,
	LTDescr_setOrientToPath2d_m135B485E55660A23D7FE3A51AA1C5D3E38804E27,
	LTDescr_setRect_mDE3387640B652E3851AA62B4483FE0150067BCF6,
	LTDescr_setRect_mFF955D0F4C0518F219B3DF1063035FF8AD8B5A4B,
	LTDescr_setPath_mD75B1338F2117AEDC0E243C925038014EEC86974,
	LTDescr_setPoint_mE041B263E9365DE9D2C8A12537A69E59DE02D311,
	LTDescr_setDestroyOnComplete_mD7468721AACBFC786485DC873912EF6DC442D831,
	LTDescr_setAudio_m5AA3E45C784E79CC21DB55C4789C4C3B8DB5FE9B,
	LTDescr_setOnCompleteOnRepeat_m6BB648BEEBC71B54A8EA1CD68E4C09C1BD0ED414,
	LTDescr_setOnCompleteOnStart_mEF3D29199EA20722C91FD0FA1663A279D46036F0,
	LTDescr_setRect_m24CBF3848B9211ED00193D668308C5849294191D,
	LTDescr_setSprites_mCA611B87878CCED8217DD98E25AC53FB8874B5BA,
	LTDescr_setFrameRate_mA3F0847615F3E5A97C192C548D40BDC1B6BC48F2,
	LTDescr_setOnStart_mAA21647EAE7FBD188227754F8F8E175985CD9379,
	LTDescr_setDirection_m1C23D5D973D7CB4391403DFEFBDF12F15A1E75B4,
	LTDescr_setRecursive_mB6A214753796545145520D40C7AF9C7B85B8DC97,
	LTDescr_U3CsetMoveXU3Eb__73_0_mC1B3A6D25B73B3945BA6BAE2A0B8048B2E7706F4,
	LTDescr_U3CsetMoveXU3Eb__73_1_mB298DE15C613EDB42361E42D3935981846884621,
	LTDescr_U3CsetMoveYU3Eb__74_0_mB45AE75CE0B616D21D397C4600C3FA7E19608E21,
	LTDescr_U3CsetMoveYU3Eb__74_1_mC7C72E8D96740B0F4F476545FF9A9CE6579D773C,
	LTDescr_U3CsetMoveZU3Eb__75_0_m2F90BBD09164CCA80404F98AEBE39E0C7C49E140,
	LTDescr_U3CsetMoveZU3Eb__75_1_mB0FCBC7EDEE189C9FC90E38F51C5F36FFB1C6E75,
	LTDescr_U3CsetMoveLocalXU3Eb__76_0_m2E2D10CEC52049D66A18E61B7A6820F5693D1000,
	LTDescr_U3CsetMoveLocalXU3Eb__76_1_m2B451B55022DFA5A2149080DCF5EFEA51DD1EF15,
	LTDescr_U3CsetMoveLocalYU3Eb__77_0_mD773679AD1B3F289E97B8A97FA3C9816B43B0F97,
	LTDescr_U3CsetMoveLocalYU3Eb__77_1_mD806F3C4BD68BD6A88B7E47DD52B9E8EF67516DA,
	LTDescr_U3CsetMoveLocalZU3Eb__78_0_mDFB2967D5068D7DA7D1FBD20D8F621C8A7E4A5C7,
	LTDescr_U3CsetMoveLocalZU3Eb__78_1_m9FE503FE722E8A8877BF3559FFA4EFF806841A0E,
	LTDescr_U3CsetMoveCurvedU3Eb__81_0_m93E47D28F6BC0CAEF79EC1B39091B66D2ABFF67F,
	LTDescr_U3CsetMoveCurvedLocalU3Eb__82_0_m7AB4E3AF580B52EE389B4264B1C3DC08035631EF,
	LTDescr_U3CsetMoveSplineU3Eb__83_0_mB704A557E71BAE0EEADA2DBDC7C85EBDE842174D,
	LTDescr_U3CsetMoveSplineLocalU3Eb__84_0_m71CA556831D55D2294212AF2E49D1C38ACB38C73,
	LTDescr_U3CsetScaleXU3Eb__85_0_mFF1273FC3FC6E6955FDF2804FB5894109E1E6877,
	LTDescr_U3CsetScaleXU3Eb__85_1_mB8660436928C1ABFE1AAA2988DC2AC0BD6CEDFAB,
	LTDescr_U3CsetScaleYU3Eb__86_0_m86846124370FA864FD9E0A6F84F3ACFFB73946DB,
	LTDescr_U3CsetScaleYU3Eb__86_1_m2A79BFC6964E50B4602F3C4D17A372BA1712F4E2,
	LTDescr_U3CsetScaleZU3Eb__87_0_m5BB4E20E122944640691C42C328283ACB0E779AA,
	LTDescr_U3CsetScaleZU3Eb__87_1_mDF138908ABE364C4CA46DA854F9C1A01F3A115F8,
	LTDescr_U3CsetRotateXU3Eb__88_0_m493F46830D9AE2D265D85AECD9CC07542CB2D164,
	LTDescr_U3CsetRotateXU3Eb__88_1_mC1D2B816B4365D56F639D50DDA16AE00A3E6FF5F,
	LTDescr_U3CsetRotateYU3Eb__89_0_m0C043D7BDF837A3C3FDA107395C85D181B66F13F,
	LTDescr_U3CsetRotateYU3Eb__89_1_m4E38375358C4BC49BD7BF69476F9B0FA5525BACF,
	LTDescr_U3CsetRotateZU3Eb__90_0_mA1C23C492D18C89ED39A5E0B2B24F14930DAB66A,
	LTDescr_U3CsetRotateZU3Eb__90_1_mD77CC83BA388F7F3A8E101D31AC5662901F347B3,
	LTDescr_U3CsetRotateAroundU3Eb__91_0_mC30EB5DCA1B236C2D2BFB1BC8C2F620729399547,
	LTDescr_U3CsetRotateAroundU3Eb__91_1_m54A375FE4EE6BB07CB382CDA4B91D655E30D8177,
	LTDescr_U3CsetRotateAroundLocalU3Eb__92_0_m71580817297D4C28EF66A991FE629314AD3FE249,
	LTDescr_U3CsetRotateAroundLocalU3Eb__92_1_mEA423F4B08C78D3F9319DF0CF2C2EA95CFA40595,
	LTDescr_U3CsetAlphaU3Eb__93_0_m62840E0A1F518EEED15E89CA1B8FE51800CB6ACA,
	LTDescr_U3CsetAlphaU3Eb__93_2_m74F5E55F3656D02C098B8D5F51444822BCBF619E,
	LTDescr_U3CsetAlphaU3Eb__93_1_m5E1833D9D31C62DF5234D7AD9CB657BE909F10C0,
	LTDescr_U3CsetTextAlphaU3Eb__94_0_m8DFC387E69EA4F5D71017CA03EBBB3B865041251,
	LTDescr_U3CsetTextAlphaU3Eb__94_1_mF29634A706AB1DFF06D7ED9D5314C9CC10998676,
	LTDescr_U3CsetAlphaVertexU3Eb__95_0_m38DB9282A06B18A985B4FA2A3051210DB0796FA3,
	LTDescr_U3CsetAlphaVertexU3Eb__95_1_m9B16A2B73E285ABB14E7AECE55CACB8C04BE2ECB,
	LTDescr_U3CsetColorU3Eb__96_0_mC4C335EE7D3765075D4287E9C5C4E185A010B463,
	LTDescr_U3CsetColorU3Eb__96_1_mF45002C64332B080B32EE1056139487352147496,
	LTDescr_U3CsetCallbackColorU3Eb__97_0_m0C6A0800CAAB148669FB87B0E686A60E9E0E1E50,
	LTDescr_U3CsetCallbackColorU3Eb__97_1_mD30A488146A9F8DE2876C79404BF0A69361CFFC8,
	LTDescr_U3CsetTextColorU3Eb__98_0_m3369FE91DC4472C45068C9B348E308991CDB0D92,
	LTDescr_U3CsetTextColorU3Eb__98_1_mAD33FF9EE92E06980CBEB4C7759E32DA69F625EB,
	LTDescr_U3CsetCanvasAlphaU3Eb__99_0_m4AC161231D4D1B57CA69B7BA0323F4E6CF0F4CA2,
	LTDescr_U3CsetCanvasAlphaU3Eb__99_1_m2B6D509A77B2396A304997DC39DCB3455E4D198F,
	LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_0_mF33DE2956D5D901DC51B5D9924EFD4FD3E38ED57,
	LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_1_mBA6DC3BCE7F96E4D726A0AC809397EE35FFFF222,
	LTDescr_U3CsetCanvasColorU3Eb__101_0_m7054A2F4C59932B279ACCE46880C9832B8A9D889,
	LTDescr_U3CsetCanvasColorU3Eb__101_1_m6460741C9E869E7B10CB86D933E4BDCA18D737ED,
	LTDescr_U3CsetCanvasMoveXU3Eb__102_0_mFE234BD5C94E07BCF7414CB6D52994226A53EF0C,
	LTDescr_U3CsetCanvasMoveXU3Eb__102_1_m2DA6E431C160C39E876CD86BFF777B6D8D0C0894,
	LTDescr_U3CsetCanvasMoveYU3Eb__103_0_m54D0560CC7DACB6DF5B711C9DFF85C1A08524104,
	LTDescr_U3CsetCanvasMoveYU3Eb__103_1_m25A736629C0A77D4B89641F916444588968FA12B,
	LTDescr_U3CsetCanvasMoveZU3Eb__104_0_m334C0650BE3F004D3246A37F9703EE58F310AFEF,
	LTDescr_U3CsetCanvasMoveZU3Eb__104_1_m093E0527B343F64B8D424AFFF2FA73EFF4CD23AC,
	LTDescr_U3CsetCanvasRotateAroundU3Eb__106_0_m77AE4A4821E0D9DA232D32B3F85AA6D17B102CE1,
	LTDescr_U3CsetCanvasRotateAroundLocalU3Eb__107_0_m5B22A810186C6A02EEA6C3D1F9E7823A58F48A8A,
	LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_0_mCCC3A883C06689CE1AF4E56F599E17E6A08063D9,
	LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_1_mEEA14A276656406AA550A39A45E0EF0DBCFCAD00,
	LTDescr_U3CsetCanvasMoveU3Eb__109_0_m9B67812E976E616B40413119359E8A358B779DD9,
	LTDescr_U3CsetCanvasMoveU3Eb__109_1_m515B75E2D9C1099D35D29062A747D6881F3FD22A,
	LTDescr_U3CsetCanvasScaleU3Eb__110_0_m66C78DAA27E664E37107383D3F72D9EAE85E93B5,
	LTDescr_U3CsetCanvasScaleU3Eb__110_1_m7DED18C9319531415228CB9DAD9E40A0578BA7D6,
	LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_0_m622448B8EA313708FCB496103613BE9368191CFA,
	LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_1_mEB78ECCD2C73EDA02A10E61A43F44B0522D99FA8,
	LTDescr_U3CsetMoveU3Eb__115_0_mA9FC21145770DFE3CD83595E33E2A3F9CBA088A5,
	LTDescr_U3CsetMoveU3Eb__115_1_m268228A84C3365C7DFD41C66717C908DD0408766,
	LTDescr_U3CsetMoveLocalU3Eb__116_0_m81C2B5508318FB8C79015D00599694265975859F,
	LTDescr_U3CsetMoveLocalU3Eb__116_1_m4FC9E996AA295A46BEAF965E85DC9372A8A1C081,
	LTDescr_U3CsetMoveToTransformU3Eb__117_0_m4A1FC4A154E19A07F743509A9044C9447DB2A24E,
	LTDescr_U3CsetMoveToTransformU3Eb__117_1_m1CED29CFFC5D0BE430F5DCC24367B64CEE4E43F3,
	LTDescr_U3CsetRotateU3Eb__118_0_m23A4A1AD7B915AC5EA95B9AE5231809F4E7A7D97,
	LTDescr_U3CsetRotateU3Eb__118_1_m575493C2DA420D01FEA96EE0AA7E4871430EBD55,
	LTDescr_U3CsetRotateLocalU3Eb__119_0_m8A85181B21112A32CE5CCE169692DEDB501B4E23,
	LTDescr_U3CsetRotateLocalU3Eb__119_1_m41757196871546467230FABFFB37075B85251B1F,
	LTDescr_U3CsetScaleU3Eb__120_0_mC7315F796459D639557D6B0F54514AEED391BA35,
	LTDescr_U3CsetScaleU3Eb__120_1_mE6F7F8EAEC18A9A19D4D698E0FD8034E91F763F6,
	LTDescr_U3CsetGUIMoveU3Eb__121_0_m1224EB2264D882E8A0055B5AE36CEEE740B3F21C,
	LTDescr_U3CsetGUIMoveU3Eb__121_1_m1A995AAE2505372C605DC64A13E5F8D9282307FE,
	LTDescr_U3CsetGUIMoveMarginU3Eb__122_0_mA08F3D978D14B8FE43A13FD6C51C6EA287D70276,
	LTDescr_U3CsetGUIMoveMarginU3Eb__122_1_m6C40E08EF41DE0B32C19DF4483E1D153D1CA04AF,
	LTDescr_U3CsetGUIScaleU3Eb__123_0_m713651C4163C0D725D46C2CE175BEA1E8A541770,
	LTDescr_U3CsetGUIScaleU3Eb__123_1_mD45B431D66ED1E7AA392547A69C2F14698FC31DD,
	LTDescr_U3CsetGUIAlphaU3Eb__124_0_mB1E87936B50CEA5B8066511633A64DC06C437B7D,
	LTDescr_U3CsetGUIAlphaU3Eb__124_1_m8E111699E7BB33552DF1284E5B6E793D53A0E32A,
	LTDescr_U3CsetGUIRotateU3Eb__125_0_m608C2F49CC8E321754F2F2C67186893EC1BCF121,
	LTDescr_U3CsetGUIRotateU3Eb__125_1_m3CF50F3F6A1980ECB3712F1A437DF9A3EE9A7A17,
	LTDescr_U3CsetDelayedSoundU3Eb__126_0_mC6AFAF26799C7A75AE029A2F1B6ACCFC0EC9C714,
	LTDescrOptional_get_toTrans_mB530A713C24C3F06F8B42AF31CA3FF977AB7EE0C,
	LTDescrOptional_set_toTrans_mD1A169A39C828FA5536708E3A19F1A1D24370ADA,
	LTDescrOptional_get_point_m684081D83B6A9895107F41A78E6FB2EF4F871A62,
	LTDescrOptional_set_point_m699F8960E9420CC80E624E3A914294836144D84B,
	LTDescrOptional_get_axis_mB136A693C94957550FB4A493D8055AF2A8498950,
	LTDescrOptional_set_axis_m3B316ED04F0DD39AA46549611DD2A87CB09563A3,
	LTDescrOptional_get_lastVal_mF32405DF5E4967E0526173E12A1D49ED459DEE96,
	LTDescrOptional_set_lastVal_m10DF605FAB025AE13E603C2272486F8A4CB90A92,
	LTDescrOptional_get_origRotation_m07382A5C09A8A47B7D9C9DE129BD7CF56E59FDBF,
	LTDescrOptional_set_origRotation_m72912AB74F93857BB64706F91607E176B7C5B084,
	LTDescrOptional_get_path_m70C0472D3F2618618EA9A0EA152799AD814CBF56,
	LTDescrOptional_set_path_mBA7489CB0E86A582174D0A5E183CC0BE9FF859A0,
	LTDescrOptional_get_spline_m8DF792E3FA917F95827962B53715396B14B58CFE,
	LTDescrOptional_set_spline_mE5EEFABD4DE99672B93F6A46B09B90C272EEE01F,
	LTDescrOptional_get_ltRect_m6906D85E922AD1856AEEDE0E2736AA2CAD13401C,
	LTDescrOptional_set_ltRect_m16894F8919730156E369DB2273FDA7EFF3736C1E,
	LTDescrOptional_get_onUpdateFloat_m22C332D592FA821F4A74034B3F77061500B9728E,
	LTDescrOptional_set_onUpdateFloat_m2396684827436DC9B10053C9AC2C10A53DA8AECD,
	LTDescrOptional_get_onUpdateFloatRatio_m6B22F53C31109E96E7E48E43A9B44651838F7945,
	LTDescrOptional_set_onUpdateFloatRatio_m94CE022ED233DA9D79AA3F18C8E6054C2D8E6573,
	LTDescrOptional_get_onUpdateFloatObject_mBCB3D7865F5F9ABAC4C5A03DD91241BF2B1AB601,
	LTDescrOptional_set_onUpdateFloatObject_m81041094C42F5FF531D4C26135A74444B4EAC4EC,
	LTDescrOptional_get_onUpdateVector2_m310F0582519C2897287E219E5AE5EB4A034F3FD1,
	LTDescrOptional_set_onUpdateVector2_m843CED0BD5F49E0B9CADEE1E0D5C1F418A77F5BB,
	LTDescrOptional_get_onUpdateVector3_m1EAF8967B52F428EF0BB00792489D5F9DBC2C959,
	LTDescrOptional_set_onUpdateVector3_m8AB5DC8AFCF8FBA6132342A8E662D0BAD10B22E0,
	LTDescrOptional_get_onUpdateVector3Object_m1BC1054A2FBE8FABCE6F6DDB453A60685C8B86B8,
	LTDescrOptional_set_onUpdateVector3Object_m98BFC4AA1096210EC4D9374E8AD0DC61E927FBEB,
	LTDescrOptional_get_onUpdateColor_m2ED4389F5DCBCE5D9157F6DB12EEFE229D279006,
	LTDescrOptional_set_onUpdateColor_m71FBD24255ADA5C0CBA8E473AFB71F18D4A958CE,
	LTDescrOptional_get_onUpdateColorObject_m8942669D7FE94243BCF809D11BAF52E289CF3065,
	LTDescrOptional_set_onUpdateColorObject_m9556E2C730376300992A2388E38161C59B7B87DD,
	LTDescrOptional_get_onComplete_mFF2D0179EF228E49F57BE663851C6BFDB45044B6,
	LTDescrOptional_set_onComplete_m5337F3EF337F442B0CBE09C1721EAC212FE306BC,
	LTDescrOptional_get_onCompleteObject_m6CC2CEEEEE1FA541AEC4D71580593EB4A603CDF0,
	LTDescrOptional_set_onCompleteObject_mB19BF5653BE9F370A1AE846055F9B546C4D74D7B,
	LTDescrOptional_get_onCompleteParam_m428D156539D89C6007E0BA22E368A7B8252FD512,
	LTDescrOptional_set_onCompleteParam_mEDC4259A922981661AEAF55E9B845B4514BF0FBD,
	LTDescrOptional_get_onUpdateParam_mBFC0491E0705696A41C4DCB044F7ED0D70AB1BEC,
	LTDescrOptional_set_onUpdateParam_m795BC6412556889BC4503D1E571DC2FF722643E0,
	LTDescrOptional_get_onStart_mC70E09328F589CFB5965B061923AD4FEB6D4B2C5,
	LTDescrOptional_set_onStart_m529CC6B3EE612FF8C1F736BC185A090C1F04C2FA,
	LTDescrOptional_reset_mAE8172FF1FFB9054153167751ACB4A3756853159,
	LTDescrOptional_callOnUpdate_m10CF5AF6AE9BDA730C97538C3E164DF2961274AB,
	LTDescrOptional__ctor_m50C4F69D364DD967ACF09DE82F78E20D3B15AB0C,
	LTSeq_get_id_mF7BDB8790E2BC1B542C0F5A4260AADDF48795D3F,
	LTSeq_reset_m342DA923D9419644BF3A4793A0606AF0A5C133B1,
	LTSeq_init_m5315CCEC0CACF7895E5B1D5F617F589D41741A1B,
	LTSeq_addOn_mD55953C7985C4A08B02742D62A7E010367F68C2A,
	LTSeq_addPreviousDelays_mB6198AF3CABBB0D35D0AC220991530A5DD2155EB,
	LTSeq_append_m0B12C0E6C61C2780F758FD94D903C716E72F68C0,
	LTSeq_append_mC0F7F9A6570CBE8BAA7112CF7217D6E164A413F4,
	LTSeq_append_mB7C3B091C37DBD0042777E6F099687007BAF5D17,
	LTSeq_append_mDF7F824A764228060EFF8BBA69F903D1795D499F,
	LTSeq_append_mD32A66D2490D4B839B6AAA8B0B577895941E61EE,
	LTSeq_append_m76F5F63AB647BB18EF33454A98F105D75E9E9B80,
	LTSeq_insert_m2B024BE54107A8CAF3AE6B271FFB3CE86441DD9A,
	LTSeq_setScale_m896BA18CF4C685EF34A26175CED8C6906043568E,
	LTSeq_setScaleRecursive_m78C901915A35EB641BDBD9FB639199463C49639B,
	LTSeq_reverse_m777F21FCED971A010923D62CD068626ECAAD9DC5,
	LTSeq__ctor_mB6D4797A9F32BE64240BC1D4FB9A0A8B6D824E2C,
	LeanAudioStream__ctor_m531CEA7A157EA4A83F540265CA7160894005CFC1,
	LeanAudioStream_OnAudioRead_m97338215EAA44081B43443314379D323FA30C308,
	LeanAudioStream_OnAudioSetPosition_m5B845BECDEBA740CB23D0725D335BBAE1B2FA4B4,
	LeanAudio_options_mB74094BFE3791F34AD4614CCB425AEE5228AAC83,
	LeanAudio_createAudioStream_m86F3E14A3FF84DC88892A8B7F16D92224BD15D8A,
	LeanAudio_createAudio_mB693792EC007530C3DCA798A57ECCD7BFE67D426,
	LeanAudio_createAudioWave_m3D9DFFC3319332471839B2D76C5266629AAEED04,
	LeanAudio_createAudioFromWave_m8FFF705E1832195B4E0EAA977C9F4CF61FA33058,
	LeanAudio_OnAudioSetPosition_mFF60A18D3230037BCEB08585DDDA7B27F8B71C4F,
	LeanAudio_generateAudioFromCurve_m7E8EFFD1CC78766F82416FD21A47889DC8D31923,
	LeanAudio_play_mB81F9043651130080D3F0CE28033BED5EB39E0CB,
	LeanAudio_play_m32A1254BE2452ED5367119EFFF686D9595E4D57B,
	LeanAudio_play_m3A30E48BBA88B9E8881968633686928B854544F9,
	LeanAudio_play_mCD89719473F04DF4A0CBDE0FACBBD831A8DD2ACF,
	LeanAudio_playClipAt_mC5C4CD778BDCF8D897740C2B3EA8415C631BCF3F,
	LeanAudio_printOutAudioClip_m2783A6C26F9A85D837099B9A4010A3523C9BDA90,
	LeanAudio__ctor_m88410F3D66A6EDDF53D657CF52DBBCBD40E7E8D8,
	LeanAudio__cctor_m5647818341FC7B5272CFCDD6966CA0407EC9D131,
	LeanAudioOptions__ctor_mC4A118B38AA350E5502615E56400F51A43B3B60B,
	LeanAudioOptions_setFrequency_m2534EC3E99ECCB13667B5703931133E97CCB6459,
	LeanAudioOptions_setVibrato_m493C38946EC65F6B1C2E811A1C232CF8C40E8876,
	LeanAudioOptions_setWaveSine_mDADF8446B8A6BB14326F15C6FD010E43A6B5A84B,
	LeanAudioOptions_setWaveSquare_m694508FC09511CF1BF371B7FA456BE92D503322F,
	LeanAudioOptions_setWaveSawtooth_mC60BB12911D96EA79544EBF3780588CCB0858EF7,
	LeanAudioOptions_setWaveNoise_m92B623A5316643EACFC1CCEF761D463EA7F53C15,
	LeanAudioOptions_setWaveStyle_mD446529C32C1D2809CE0BD19558B9B9A1CCC7381,
	LeanAudioOptions_setWaveNoiseScale_mFE6B538697857F28C0077DC7C7F3C34CC00DD5B3,
	LeanAudioOptions_setWaveNoiseInfluence_m321FC18AC3DB87F3F22658CB134088F6AE8B0A5D,
	LeanSmooth_damp_mC64F76E7BEB749AE0C6620500B8F7A124C8B2281,
	LeanSmooth_damp_m2AE19609C34606AAC949DD5737F7C905D6D1FAEC,
	LeanSmooth_damp_m7155F328C1DE25635DFE88A26F308E83D559C084,
	LeanSmooth_spring_m13E4D3125D99326FA237E391E6AFC87321D643A7,
	LeanSmooth_spring_mB9D231C3690876EE5299B0261A5B101C0299E3C6,
	LeanSmooth_spring_m93FC4C89467835E4367BE781881F47A7E7416B36,
	LeanSmooth_linear_mE0319D93C18D8C6230B69BB6914BC2171D9991FC,
	LeanSmooth_linear_mE1E0D6A877174839F0D805E0E23AB7B9869F99BE,
	LeanSmooth_linear_m5F142DE9C4019A6163ED38F547F8E26D6FF4C1DF,
	LeanSmooth_bounceOut_mAF91D979E058FDC80D6E7BB35FDBDF5D1C584930,
	LeanSmooth_bounceOut_m213E8992275BA3F7E5DF6DCB584C2E4EAE055163,
	LeanSmooth_bounceOut_mC0BF320EBCFCF7EAD5CF449073C707968FC27F1C,
	LeanSmooth__ctor_m14E227157D0426A2F47C06DE30CB17982B72BF66,
	LeanTester_Start_m9A9E920B5F046E3372B0366E0BC9DFC84A919F58,
	LeanTester_timeoutCheck_mC313EB95692CB44EBD8BA1A87AA815BD5416B150,
	LeanTester__ctor_mC69E2DD7669788DEA00909ED2351EAAA6CF317C3,
	LeanTest_debug_m6C2E9AD4476177558EF713F252CB2FCEDB14DEE3,
	LeanTest_expect_mD39E3D849F6BB08F68658A3EA543DABC2ECC9B77,
	LeanTest_padRight_mD8E69C06CFE450F91AE8B44FD304BAECDCB25F91,
	LeanTest_printOutLength_m340E6C2B8CE237DD562D42857AA79E56724407C6,
	LeanTest_formatBC_m61FCBB26428987CBC8EAD380847104E662D747CE,
	LeanTest_formatB_m3A16FF8B95ABDFCFEAF5C5B3591F5D84867A7954,
	LeanTest_formatC_mF76064C060FB981E2535F8A487393A849D5DD292,
	LeanTest_overview_m54D7E1E1324A5D63C8834BCBDCD9627490C9FC37,
	LeanTest__ctor_m08E8F412E4EBF00D54D3AB5D8EE7E0015C3A04EF,
	LeanTest__cctor_m589610FAD222BE5A0012463E6AF9D88514317EBF,
	LeanTween_init_m742C83A1FC52BA7DC1A45D77418E2A20C6EA3FA4,
	LeanTween_get_maxSearch_mCBB6EBACEB7810A13B6BFE5E46FF93D9B576F8DB,
	LeanTween_get_maxSimulataneousTweens_m29E755842BECB453B666A30118106AC8FA3ECDA9,
	LeanTween_get_tweensRunning_m7DAE5C7327AF47F20CEAE9B5D7BE7460C53CDC46,
	LeanTween_init_mF9EBB839A07AFBCAEBB72C052B6F8EC4CD48D7BA,
	LeanTween_init_m876B8AA3523AB02C0246A87E9E37F5388FFFB35B,
	LeanTween_reset_m239840212FEA6441ED109FC1E312A2BBE2534AF9,
	LeanTween_Update_mF15DAA93C9C32D4BA1645DB4B03AC67F6C3D8F8D,
	LeanTween_onLevelWasLoaded54_m30E5953BBD22DF6BFAD61B4B62081D26ED1E0046,
	LeanTween_internalOnLevelWasLoaded_mF01B04BD6D4CCB7D5C896B0C9381BACE2A6996F1,
	LeanTween_update_mE2EEEF001DE108CFA101BB549033B6022655C0E8,
	LeanTween_removeTween_m47036DA44D4E0AF9443A2A459B10F51C039BC835,
	LeanTween_removeTween_m5183FCF6228B6149D948D3175B1906EDE9EB5A2F,
	LeanTween_add_m94B30FC92F9702D1C000E53F0481E2547BCEA152,
	LeanTween_closestRot_m425E4814D09E1E33641F79E253706B42A8677DCA,
	LeanTween_cancelAll_m57047A3005FF66C0137640192B7BDB3DA6CD2F27,
	LeanTween_cancelAll_mDFE02D9737D5CFFBBBAB9B7ADEFF4EC8A2EC6BA7,
	LeanTween_cancel_m5FAAE217AED5E47A774964AD4B49CBF06BB4CFAE,
	LeanTween_cancel_m8C912896B48485DEF6C233BDE9515DD9BCB37F4D,
	LeanTween_cancel_m83465350000117665934A8D6556E5759C60E4BA3,
	LeanTween_cancel_mC1789BEE750E0F49BBB9A5427837EAAED0F6B362,
	LeanTween_cancel_mF06045394FD8DB00302C26CBB06F791FC20BE002,
	LeanTween_cancel_mBDB0EE47F9FEA5CDC99DAE5AB071A317D1646E73,
	LeanTween_cancel_mFBB7196E53B68C2337C13F0AA1F6F96A9E652995,
	LeanTween_descr_m673151C22BCD383E070208DC886A48EA5682354E,
	LeanTween_description_m614B276C8B85841618BC11CC53EC3F9F998B12CD,
	LeanTween_descriptions_m15F1EE30DA28ED509BED8B1208D21C6A305D0F7C,
	LeanTween_pause_m544CC2BEF9E44BF1FA9C2B02FC8C8D8D840303F5,
	LeanTween_pause_mD334E7016A1280D8612923E19FE0EAC3A99213C7,
	LeanTween_pause_m34D1984074FEAD898CB5A732E153230C05BC0F8A,
	LeanTween_pauseAll_m9A08262908FA3B89F629A53310A615960E25681F,
	LeanTween_resumeAll_m0CD4B24182F95AB48670CAA51B31356DA0298F02,
	LeanTween_resume_mA7A5417B25090825B935A5A53BBCBC0DD2CA60FA,
	LeanTween_resume_m9A7648F170B3EF8AD8CF448578A1423E5AC57DC0,
	LeanTween_resume_mED2ED2E734874013D07AB1CA160FA30098BB93CA,
	LeanTween_isPaused_mEA3B9A77082F25532A58E01A3B16F0EB6AD3C9D1,
	LeanTween_isPaused_mF0B834CE3C9278DF00216045E11ACE5EE9D66354,
	LeanTween_isPaused_mB1A22DBCB84A1FAD2A2D5E0B7F2307A77D1C630A,
	LeanTween_isTweening_m8565F710EC2593893864BDC02B0F286E58D77928,
	LeanTween_isTweening_mD5E04C094ED73605C74804ADCF9B13FADDAC9470,
	LeanTween_isTweening_m7300B17390DAF00D71AF9837EFAE524A0F067607,
	LeanTween_isTweening_mBEB3331D582A3CF1DFC989B9538934CBC8B29F39,
	LeanTween_drawBezierPath_mFC7D2D0AF1632D71D25BF9B390285DAFB57A1FBA,
	LeanTween_logError_m37F7E5A9E9028A282D3FB22858AE9E751B5B7E17,
	LeanTween_options_mA6EBBE759321A9ED63E09FCA3E19257A5732399C,
	LeanTween_options_m96D5FD6CD10BC38781B78A7ABBAFD40FD6896C10,
	LeanTween_get_tweenEmpty_mAD2C44A742A9DBD681D325C9EC5708FB4B2DDAD9,
	LeanTween_pushNewTween_m5B8A1F2059CF7A5AA5A994254FB6FF2325FDB5E8,
	LeanTween_play_mCEA864283E853E9E7086662D30B2B35229601255,
	LeanTween_sequence_mF0E50A4160E6E03ED0B1B6A57B25552E195F1E60,
	LeanTween_alpha_m03563AAF9FE673DAEAD06EC87B702F6633009459,
	LeanTween_alpha_m8BD4F21C3DA8E6057BA72EB4500336A8AD2D23D0,
	LeanTween_textAlpha_m56966F76D08E1D2F771F68FAC336CD26A3789C4B,
	LeanTween_alphaText_mDF874DA494D7B483C1AD62D2459795583C87EA50,
	LeanTween_alphaCanvas_mD7760E3B438F098F8E52C0E19BE66AB042700482,
	LeanTween_alphaVertex_m9C46F3D3D5156962E5E6DAFD6D3B91D87A86CD77,
	LeanTween_color_m37AE0DBE5DBE82AB6CCF14D76DF1EFC01D95B69F,
	LeanTween_textColor_mEF9FEF414B74FE47A423CF17CA79712A0D1FB658,
	LeanTween_colorText_m15B3D867D6A42D0D7770B97CB2E7E200F0D661B2,
	LeanTween_delayedCall_m04427AEA4FBB41C4A3CCB898D64BB4AC740BF03C,
	LeanTween_delayedCall_m899A2AE8C3C19487CFA5AB41977F67D01F9DD4E6,
	LeanTween_delayedCall_mA05A41E42F1921AB3867C1AA9B43100FC5F57F2E,
	LeanTween_delayedCall_m7A1A97BE2037CF29C9C68D5DC41476904215AAA0,
	LeanTween_destroyAfter_m7641087D50C872EB2A5BEC69287673C0AA63A6C1,
	LeanTween_move_m1191EC5B42C368CBBAA12E7EF8EFC8378980F2A5,
	LeanTween_move_m0AFE5CA5D827F49074E9ACA6C73E2E6D6E78E79E,
	LeanTween_move_m69E063649877CD64A4755003DB27E1608A41C08B,
	LeanTween_move_mEDCFBFB4014A966F52A3DC08FE2E952DCDC53E95,
	LeanTween_move_m8058A34EDCE00BFEE3336DE5698A3927B796A796,
	LeanTween_moveSpline_mA1A07399D0522C220E7C062CCA41CA1A5F07781B,
	LeanTween_moveSpline_mBC72A4210E5EBE1678828E39AA926A53D72C2EEC,
	LeanTween_moveSplineLocal_mC1BF1287479F66BEADB1E325D9DED97A6E69E67A,
	LeanTween_move_m3ABE07EDBD7AD98100B8545E5AAA864F03A4F6A4,
	LeanTween_moveMargin_mCFF341DFEC796404C5DC2979392988C15AA31620,
	LeanTween_moveX_mC528E208B5A163AA4C5400294A2D03C8E5448DED,
	LeanTween_moveY_m9E9F3F2DD9FC9791FB42E952BC71A32858E9D894,
	LeanTween_moveZ_mA8D3635AAD59B698E00B6AD6981BE2B8E2A381D4,
	LeanTween_moveLocal_m4628772CED24062B5B96C041E6C3FE3B64DE8252,
	LeanTween_moveLocal_m6B50D6459C312416C631F3C391C76022ED19C5D8,
	LeanTween_moveLocalX_mA9353C4CDB6A49FC6F7B9568E70D4D3C210033A1,
	LeanTween_moveLocalY_m605F33473A6318A1BC2719D5032D048B2206ABA9,
	LeanTween_moveLocalZ_m0B7FEA9EDE5CDEB065F4187E11F174465D7E8EF6,
	LeanTween_moveLocal_m52BCB9FDEEFBB74988883648CA96FBA390F991A1,
	LeanTween_moveLocal_mE6FF69270695A740FF6950ABB1C389BD87336445,
	LeanTween_move_mB930433417E02C8708DA33ABFDCB478397AA070B,
	LeanTween_rotate_mA9190F8C72DBB9FF8A0998C936208C97D18236AE,
	LeanTween_rotate_m3D39319C8CA729B58884DED8AAB29903BAD76541,
	LeanTween_rotateLocal_mB981D5E76B2EA75987333079E8EEEDE7F78740A7,
	LeanTween_rotateX_m39C9891518C637C4DFEC9C6E455DF5562AAB1050,
	LeanTween_rotateY_mF66A36114A32D24BFE92D94B7944838A19847B90,
	LeanTween_rotateZ_mDBBBFF6BFBE5B724FCE59D3A6EF40F377431C1C8,
	LeanTween_rotateAround_m061DF6B0D1F4FB9C9A635C2B5C8D5F0D0486DCF9,
	LeanTween_rotateAroundLocal_m8498C0D937E0D12B6EF6C43F5F1AA7F4FD721726,
	LeanTween_scale_m4CE3CCD970CF75664CF7FCA1E6286F418FDE2A0F,
	LeanTween_scale_m209552C2E41F5D6B0B1525A25540FE40A33F4ADE,
	LeanTween_scaleX_mAA4702337267BB96770EA128A25EB894E4C7E2AB,
	LeanTween_scaleY_m664993B4D0F2296E8BE7BF8168D17A90EEEBB3D5,
	LeanTween_scaleZ_m4F475E490D515FAF13E36488615B3C9972EAC2F7,
	LeanTween_value_m915028F98333F18A1326931DFAC8106B6301D23F,
	LeanTween_value_m589DDED951D44371EB0589C01EBB679977F47556,
	LeanTween_value_m4887473E05B0379914BD3795CA6697D274C54606,
	LeanTween_value_m3EF8D28045541C4387176018135B82C81F1FD38E,
	LeanTween_value_m3F8D4D5F95CA382EA0D770B07EC74C270B7EC526,
	LeanTween_value_m115667D1977F3E7BF517B9EA3EA3EE161A8D9D39,
	LeanTween_value_m169AE41FDE8360E78E0B637CEA5A114E112BA45A,
	LeanTween_value_m1709E165F0E2491A98561574CE72092EF202C1F5,
	LeanTween_value_m931E3256745BE0E7AF60C79DEE155EC421C55EC6,
	LeanTween_value_m4FEF278D52CEFC694E29730BC229D3C52F7070C5,
	LeanTween_value_m0846E512D96A497507FAA08EC3747DA89CD6FB7D,
	LeanTween_value_m0E16AF4A36148D3831CBEF54C0DFD01CE08475DD,
	LeanTween_delayedSound_mD225DC785D5C479E00DF03A9406C5EF1A145446A,
	LeanTween_delayedSound_m17C87E524E93E54A7AC33CDF95F5560831E0A6CA,
	LeanTween_move_m9EDD3EF2EDA02B6524829C56199400A302CC7BD3,
	LeanTween_moveX_mAE925994CB0FAE76F0768A0595CCEEB5C440031B,
	LeanTween_moveY_m7FC0800EAC2943A5DF5F0652CD254D40EBA15BD1,
	LeanTween_moveZ_m3AF920F4CDB4AE4E359A577F223341EF73CA9F04,
	LeanTween_rotate_m15EB5A2DF0974570B758B596AF032A42A1EB5F6A,
	LeanTween_rotate_m362D5FBBA0847D5E7A4C8D0037355D7192F10ABB,
	LeanTween_rotateAround_m6149AA8E86FFC604EB792197923D69BA3BF46823,
	LeanTween_rotateAroundLocal_m20B862CE01E52B5C4533D2512D718EDA1437B57A,
	LeanTween_scale_m47628FC09C70C5B77089F8AA97B7682F849EC0A9,
	LeanTween_size_mEC9D2E7E59F8EBFC0CA941CADD467CD1A5491B43,
	LeanTween_alpha_m640B7551150854D49EC0BA41B7CA866B7A6CBF6D,
	LeanTween_color_mD14B3C4D1453119045FC7D74FB6746287E9BA037,
	LeanTween_tweenOnCurve_mF41225AC36913842DA74788977F04689BB139920,
	LeanTween_tweenOnCurveVector_m517A4B35EE61BAE6ADB0DF2D84863D280540DA59,
	LeanTween_easeOutQuadOpt_m3C04BFEED580755A923F99E89B4F023F62450868,
	LeanTween_easeInQuadOpt_mCBC13EA35ADC87B69182C6DDAB43904CE2649BC6,
	LeanTween_easeInOutQuadOpt_m430D10EAD4D0A1FEC9AAD1E33F67B477EE72FD31,
	LeanTween_easeInOutQuadOpt_m6496C1F3A68C113F8798961D6F0E357BAA178A34,
	LeanTween_linear_mC1ED9F0ADBDA8939C75B3BC0914E50DC34CB4F6C,
	LeanTween_clerp_mE62F3DDE55042FAFC1484ECAFC2243E82B86AE62,
	LeanTween_spring_mF690BE574938A3A8F8EC8248642F31ACD7785F32,
	LeanTween_easeInQuad_m4D2FD34AC16BDD31B2838E3A5D80916469833DF7,
	LeanTween_easeOutQuad_mB929599F71CEDB7C7C8579B9C8BA97500166584F,
	LeanTween_easeInOutQuad_m584E55A5AF9BF428B143857479D7D790CA38745C,
	LeanTween_easeInOutQuadOpt2_m6F3DE20B7BFF5E0F5B34C1246C40F9AF115F7799,
	LeanTween_easeInCubic_mE4683158D3ECF4DB9FBBDF6187759886DB83F853,
	LeanTween_easeOutCubic_m42408FA585BA84351F5CD74AD1A3ECED10FD8B91,
	LeanTween_easeInOutCubic_mFB2A765901D685FA00D1C16E6133E53047B7FE71,
	LeanTween_easeInQuart_m1C37EFBC471397640865AF91771EA21DA0ED492F,
	LeanTween_easeOutQuart_m1A24432DC39B7ADD6D87960129D2FDE0C6D03C12,
	LeanTween_easeInOutQuart_mBB2A5F122AC70BF79993F1180AC9A0D77D9C139F,
	LeanTween_easeInQuint_m1104B28EFA33850E5CB9BDC867D4C67B4D90457B,
	LeanTween_easeOutQuint_m9900891B73DD3AC15F28155E1EF340B260C8C67F,
	LeanTween_easeInOutQuint_mC67EDFA9A9953B9AC7C073D7211D115B37BC7B41,
	LeanTween_easeInSine_m81F6144AC64B5E5A4ED990C48A36F5A3D275525C,
	LeanTween_easeOutSine_mD6871A62B4F306845FD33F26B9B82E5CC43DDECC,
	LeanTween_easeInOutSine_m74089B5D05EA9EA3FB89A14DC48F1D310939320F,
	LeanTween_easeInExpo_m3B47C6876FA3DDCDBA8E27516A9D5A92AB8A0F96,
	LeanTween_easeOutExpo_m90B7C8EFAC973AFD5B0FBC48AC5277F3ED95BA4D,
	LeanTween_easeInOutExpo_m1818C9E2CA4DB499749B6E701C523FE50C57CEB7,
	LeanTween_easeInCirc_m32983AB18E1A8F9632D5D3350FE72AAC235F9DF9,
	LeanTween_easeOutCirc_m9C4FF7DD334929D7993925BBCBB8FD8D2D198134,
	LeanTween_easeInOutCirc_m4AF54D1824BB627E59856E1EB3FEEE9801A24926,
	LeanTween_easeInBounce_m4D853643882AA66C8BA82904BF06C8FD89172577,
	LeanTween_easeOutBounce_m1790E9DEA28BAE58ED7FD4606E999056A1074136,
	LeanTween_easeInOutBounce_mDDC1155A0C82DE93397F30C80DF6059685229413,
	LeanTween_easeInBack_mF3612B1794D308B659FBF9FF3DFB1BE11C4C5C3E,
	LeanTween_easeOutBack_m7B845AD4DA44D5C0B79DFFB66BFE8D1EA78BB57D,
	LeanTween_easeInOutBack_m1A651107C2734CD9259AE6F3422D1544D9C445E2,
	LeanTween_easeInElastic_mC0C2B9BD7D1ED6B6624F52DC5D8F6D2E1C1C575D,
	LeanTween_easeOutElastic_mDD7A06B6C97A24B25C34A4191312D6F8E64DED7A,
	LeanTween_easeInOutElastic_mC2E44F37BF3623A004AD1B60EE3AF7968DB21D2B,
	LeanTween_followDamp_mA9C52B1FBD62D1491E62714A18E092E939948A12,
	LeanTween_followSpring_m6A4518BC770FBA7041F96B2D93910A874D8DE95D,
	LeanTween_followBounceOut_m30E47CECD44D0928C51E4868BD229F8C3DE5939C,
	LeanTween_followLinear_mEF3E3F81DB95C8FC7512D9EB79B1A29818537333,
	LeanTween_addListener_mEC31B40DB9ED344E2F2BAE152DA564537AB16754,
	LeanTween_addListener_m34C97CB48F8B906351A79F49A6CECC6334BBD3C8,
	LeanTween_removeListener_m982E421FADD5A0991EBDFBD169F5BE77740ECE3A,
	LeanTween_removeListener_m39242CA2654CC5FC27C4D08B45F0F71888164F41,
	LeanTween_removeListener_mE054AB523D46CA58B0C59101C79AB1C9E680072A,
	LeanTween_dispatchEvent_m2996C15CD7A7C91A70A896DACBD85AD50596EE74,
	LeanTween_dispatchEvent_m746D9A2E728E4018D7156A8C05A2B2113151E475,
	LeanTween__ctor_m29640AFA36A452E41D7D7FCE837930805FE524D4,
	LeanTween__cctor_m4A2CF2E2F8D1A20278F8B86DCF870EBE903F9AD5,
	LTUtility_reverse_mFA541E145D5DB2CAD1B20BA78057C7A3495556C0,
	LTUtility__ctor_m04573B783EECEFDD4E2AF1FFD4D7B62E914D0F4D,
	LTBezier__ctor_m97D899A3D06174612D033E42F804FB14684814D9,
	LTBezier_map_m3DD6B1382C93E0204A339F62ACCFAB5C3A29DD2A,
	LTBezier_bezierPoint_mEB0E0EF802E14A7FD9AB49F23936260D8B0AFC45,
	LTBezier_point_m017F4A7A1923793B57C7A8A76682609CCFD0E8D4,
	LTBezierPath__ctor_m3180C1A051F04229804D0BB0F4ECCAE916AA6845,
	LTBezierPath__ctor_m431529CBFCA2BEBB9158B0E80E9AFDBB2F9EF7C4,
	LTBezierPath_setPoints_m6717D57148D9DD5B0B3B9F2C67F5060423CC62BF,
	LTBezierPath_get_distance_mE03E0C6CAC8CB0D3DC5CD4F21CCE6AF6A2B5615E,
	LTBezierPath_point_m7DD32C57F1078C21BFF31EF88109EF188FBBEA89,
	LTBezierPath_place2d_mA08CEFF1862BCD64FBF0D6DD7821822BED4F56A7,
	LTBezierPath_placeLocal2d_mBDE2E3BB035F85D3A500AC0B0E36E5038CF3B27A,
	LTBezierPath_place_m080C91568428A12F6CAA5EA5787D0A1EE78569E3,
	LTBezierPath_place_mA0099B55C6D850148EF3F139A3AAA21115FF6395,
	LTBezierPath_placeLocal_m0D2D81B70292CE5B7D595E8E1FECB865E2617F88,
	LTBezierPath_placeLocal_mE7093E9870F33F6B93BB03F804B14011A7C0542A,
	LTBezierPath_gizmoDraw_m32B996E9870AA9A63DD800B3E406C54F697B23ED,
	LTBezierPath_ratioAtPoint_mDBAB488E78E8993C759918B979CCCCD9DFDAED19,
	LTSpline__ctor_m89DAF8F9A02B744F14DC951BF26998ABE636079E,
	LTSpline__ctor_mB9F6B9D5E8250215A98C318B775D0862716E2F62,
	LTSpline_init_m5F8F0ED8EE3B00DFB2020C9B8E7F0589D764296E,
	LTSpline_map_m8586FE88C55DFE6D7CD897651AEBB8E161D4AAD3,
	LTSpline_interp_m07E5A5D423EC7092EF3CCF7FD65FFD65DEF7704D,
	LTSpline_ratioAtPoint_m6BE44439D18029959983FA5D960C319BAA0F5EC9,
	LTSpline_point_mDCF2C08D8F3F338D5749D4478D652DDE9B2949F9,
	LTSpline_place2d_m7715D476897D521AC679B2007254773606199FAF,
	LTSpline_placeLocal2d_m1264CC1B0A061E5B1D399B46F1E6BEE91BE49926,
	LTSpline_place_mDBCF580928976DB68CFA586B31B88C23C6D9FD52,
	LTSpline_place_m4F75114ECFDE9711D5BC6AA012180139DB21136F,
	LTSpline_placeLocal_m47E0DF17D96F80E0EF9E7B2809828BECF1063660,
	LTSpline_placeLocal_mA60E9587B94103C8BD655ADD566F2218D4D2B1B5,
	LTSpline_gizmoDraw_mA538B509F29ED2A1583D44FDC86935B98364FA88,
	LTSpline_drawGizmo_mD374863782B673CE885A2855910958A2DF60A95E,
	LTSpline_drawGizmo_m3C4516917996D8F6D1B40A5017937C2B2BADB1F1,
	LTSpline_drawLine_mB44BF9E463BE732E1FC8A178E92FE3230FA79588,
	LTSpline_drawLinesGLLines_m7E7A763E4DEF72CFF387EAE1B80A1A6D26905590,
	LTSpline_generateVectors_m85FEDD714CEEAD04670F854D11CED89775F05AB3,
	LTSpline__cctor_m6739314E6052E4C78709F02047E8DC2DC9096AAF,
	LTRect__ctor_mD2DD65C62E8EDAC7B218018C87391F7E2F36FEF9,
	LTRect__ctor_m47F4B741D47E9809C1B03A566F06863E246A2696,
	LTRect__ctor_mBE81F7E8AAD5FB8A7E8AFCEAECCD66DA9A30F345,
	LTRect__ctor_m108E96CEDBA3D65FEFBE787F4B17F844CF3638AE,
	LTRect__ctor_m6A1374116E6DC9E18821247FAEDDD60F74FA3758,
	LTRect_get_hasInitiliazed_m2D51CC6C1FCF712275E919DFA2E44F1E860DF216,
	LTRect_get_id_m6F74EEF30562B278C561253B7591297CD389C30A,
	LTRect_setId_mF1BD1C3E2DB5CA883A47FB9E8F644EB16A5B294F,
	LTRect_reset_m53A78C5EA3E9E78CC06DB2F25D1C71A588B06F2D,
	LTRect_resetForRotation_mA8B3D468DCD205AE911118E6EED6C8A479A674A0,
	LTRect_get_x_mD70B7C3B314D4FCDF6AF3BE29F6397C47426BFC6,
	LTRect_set_x_m640BF975EE3C8DFC498BEAFC41742A094684ACAB,
	LTRect_get_y_m168437019BB9D70389C282B93AF269634C80EB5A,
	LTRect_set_y_mAD963E12C76DC233063AFACED81EAF3D03F06D27,
	LTRect_get_width_m622A9922E21390846F78DA4E37A7EE0675D9F5CE,
	LTRect_set_width_m03AEDBAC9CA96B53E086CDD5DAB57A394B1417B5,
	LTRect_get_height_m1C9CB0CBAEC6888479CDF92240484145730F0E30,
	LTRect_set_height_m34DC6F5C00E4A2C16382539CE21524603F6EAB89,
	LTRect_get_rect_m6C78FB7176F49F9F8FBB80160FDF37C57EEEEA79,
	LTRect_set_rect_m7FBDEB9CFFC6BE529A76996FCA83F69517F0280D,
	LTRect_setStyle_m73FC3A26735BB8712D8B2D92357035D9FB8FA1C5,
	LTRect_setFontScaleToFit_m39043C9072BB81E3DFF6C7697404EFB2C3090984,
	LTRect_setColor_m260F2239C224794A8C49B970E5F5DBF5CA594779,
	LTRect_setAlpha_m2B25F81A596DFA252D4C2D0090FAF8ED224AD2CD,
	LTRect_setLabel_m1FFCB5B75A7AA46B894883AD569AF4ECE8662D5F,
	LTRect_setUseSimpleScale_mFD395CF7FAF8EF1E65A4697817CA41A1E4DF22D5,
	LTRect_setUseSimpleScale_m319F90C955999F948E57D8FEFFB3706E9446EDE4,
	LTRect_setSizeByHeight_m17196A565268B33273FE50DE6A9D89D71B2C7AB3,
	LTRect_ToString_m30F891F0B46B40656CD5BC9265D6D2F0AAADEA33,
	LTEvent__ctor_mF1824A2419396C340A300E02A11BD3C710931494,
	LTGUI_init_m56E76FF99D2B3BA59DC04B0CD796B8672BAD16CB,
	LTGUI_initRectCheck_mD1D857FC17862BBF0171D8B30C2629B9FD84D4D8,
	LTGUI_reset_m7CE773801CF53E0AFCA45D4C470DA38930F518F0,
	LTGUI_update_m00A3E17E130357C2B8F88B29572E767536C3C097,
	LTGUI_checkOnScreen_m0B39FD2B120D5FB49801098E46F8705837445E6A,
	LTGUI_destroy_m59B5A127738FD610340C642E74C5FC5ED8CDF2C0,
	LTGUI_destroyAll_mEE7BC1026BAC0D8132D6AD8CAAE8CD4B5249807F,
	LTGUI_label_mA4BFF45B5BC97C4227CC0E47BC2183470B42C5F4,
	LTGUI_label_m9016D43E1833A398F9DE235F106DB4B83E4A3EF9,
	LTGUI_texture_m4EAA8E004AB27F16AF3F70E001F32086C34DF6A6,
	LTGUI_texture_m6D73FEC3838D61DB4B92333120AF15139EEB2615,
	LTGUI_element_m5933C737FE6DB8B7F4AC87B9EBEBF338BE4DD456,
	LTGUI_hasNoOverlap_m1174D1999AEEC77DAB75CC5456C4A28268D72663,
	LTGUI_pressedWithinRect_m585FA74045ACAFFF9EDAEC8469B6C61F4191C6BF,
	LTGUI_checkWithinRect_mE13CE64B1EA92D1287B6B090A75E99832C45E4E8,
	LTGUI_firstTouch_mF464F4A8A19F65CBC9EA2BF2111236BEBB9E618C,
	LTGUI__ctor_m914864F7BFF6D41D79693EED54116EA572A41429,
	LTGUI__cctor_m01C5B6F47B1C4B45CA1D6B77B5720BDBAFB049AA,
	LeanTweenExt_LeanAlpha_m461C89B7ADA172C00449BE049ACF50F3CE55D766,
	LeanTweenExt_LeanAlphaVertex_m78E0D1E1E6D229BB56EAC9F208119463298D79D7,
	LeanTweenExt_LeanAlpha_m62EE99D970FEDB18E72E1CECC8BF3C3C806B8897,
	LeanTweenExt_LeanAlpha_mDC6296AC255B03B668D291C60FCC3F49788DF8EB,
	LeanTweenExt_LeanAlphaText_m8289E2F78CE20F51FD72D83399F535A6ECE4DCD0,
	LeanTweenExt_LeanCancel_m258065657CE313E3C76F55578F793097140D9F19,
	LeanTweenExt_LeanCancel_m3EAAA4F94BA9D26442BF5503CF1C13F56B136E41,
	LeanTweenExt_LeanCancel_m0EBACDA55B64F6124F9863947D3EDFF41FE5BC91,
	LeanTweenExt_LeanCancel_m16FFCC4C3A923A8261181F5D40AF48369B766112,
	LeanTweenExt_LeanColor_mA1A81C489F98EB1520DE95136AB29B46E5EB5D1F,
	LeanTweenExt_LeanColorText_m23602453BAB11CF15AA4ED5E4F1B3962CF6836C5,
	LeanTweenExt_LeanDelayedCall_m67EE573EEA0027172A59DC8E6810F2643050D72F,
	LeanTweenExt_LeanDelayedCall_mB1FF6EE6E33AEAFE689676F8674FDF2F8BC8D395,
	LeanTweenExt_LeanIsPaused_mE920863734EE45312ECA09EA8B998C524E26BFE1,
	LeanTweenExt_LeanIsPaused_m40DD7C17AFF3F2B488C94B529A62F0EC3773B532,
	LeanTweenExt_LeanIsTweening_m11D59C75569AD0E04DA5CAA351D2F68F38C8E1D6,
	LeanTweenExt_LeanMove_mBCF028720C25916836835CBFDDEFBBAE0F747730,
	LeanTweenExt_LeanMove_m090CF72208FA48F140219041C8062A46EF3F3ADE,
	LeanTweenExt_LeanMove_m3D682F2EABA9624478F95DC984962451F4C72335,
	LeanTweenExt_LeanMove_mDE73F1D58AAC128EBF331B493A9B836851AC1589,
	LeanTweenExt_LeanMove_m38E623C91DB8CBB6FDAB138979719D2D1837EA1E,
	LeanTweenExt_LeanMove_mCB11792AADB6F2D82EF43C90709B1CBD79FDDB83,
	LeanTweenExt_LeanMove_m3CC6348A9E1270D6703F193F8CC9B1C2CE12384B,
	LeanTweenExt_LeanMove_mDD7DB276339BE449389D687CCF4793E275393331,
	LeanTweenExt_LeanMove_m1D02677FF9D2509C2FA77A3BE7C3C69718AA191B,
	LeanTweenExt_LeanMove_m1FC68173632416D0BBB72EFC74D82348B0523D80,
	LeanTweenExt_LeanMove_m87B72A7F9F763586108B9F5B648D48428B1F8FFD,
	LeanTweenExt_LeanMoveLocal_mBD123664B63D8DE32BF36F0ED100E9B87996267E,
	LeanTweenExt_LeanMoveLocal_m3149C4DF54C63DB2384A197A0AC8831EC0845703,
	LeanTweenExt_LeanMoveLocal_m97955CB5D282428BC2D2CBE129F6A401B482A8F8,
	LeanTweenExt_LeanMoveLocal_m616AE133C599BAF61CE3159CF579C3DB0EFE7B66,
	LeanTweenExt_LeanMoveLocal_mFDF99A832F9B857E1B16A2E744F9F383C1F90375,
	LeanTweenExt_LeanMoveLocal_mA80CD3FC03494A1744FB551747A732908D9B2D05,
	LeanTweenExt_LeanMoveLocalX_m5C894E1B3E98732358670D3837F1F8D3C4867AB6,
	LeanTweenExt_LeanMoveLocalY_mC4D2C4DFD2A41D0988F10D78AB782834A2D7AEA3,
	LeanTweenExt_LeanMoveLocalZ_m2523332A42D3C35D0EB0983B50290B260BE971D0,
	LeanTweenExt_LeanMoveLocalX_m8AF36480056D23742446C3D011AB3462FF6E4DE7,
	LeanTweenExt_LeanMoveLocalY_m402602941FDB4A2F4E4F52723678149A63D28742,
	LeanTweenExt_LeanMoveLocalZ_m277E9019194B31C9D2F5FACD5CA5BDB78D14031F,
	LeanTweenExt_LeanMoveSpline_m1146A15795C039B9B2B03ED4C80AD8D232A3C4F9,
	LeanTweenExt_LeanMoveSpline_m0C9AE2BAD998E3BC3CD0C3F1671F47B2F769D955,
	LeanTweenExt_LeanMoveSpline_m65F76F58B8003E56FCB6C2EDF7044D467CE61D4C,
	LeanTweenExt_LeanMoveSpline_m54CFB8DA58EB1DC4557E6456579F9D3DA7F28C8A,
	LeanTweenExt_LeanMoveSplineLocal_mA26C4FC628C8DBE7393FCEFAEABDDE769F3BA36A,
	LeanTweenExt_LeanMoveSplineLocal_m1E899C48EA516A34C9929D7FDF44EF2632AA0D1E,
	LeanTweenExt_LeanMoveX_m82773527741CA6AE6E241C12A1D8634788BEEB4A,
	LeanTweenExt_LeanMoveX_m96FB87B93DDACF291202E912E981B3760B44215D,
	LeanTweenExt_LeanMoveX_m3E49D1A15D798CF0DEE8FB9A1D7F2932D636371D,
	LeanTweenExt_LeanMoveY_m0372D8F81E13E6B32BCBA2DBE800598DCA71866B,
	LeanTweenExt_LeanMoveY_m821E5633762A108AF0D80528ED060EB0E62DA9E5,
	LeanTweenExt_LeanMoveY_m1AA13E442CA147D6B062BE8424D669317537F6F5,
	LeanTweenExt_LeanMoveZ_m9939145DDF1A83F4D8547D53A4538912466C008B,
	LeanTweenExt_LeanMoveZ_m79A272207A6FBEA47E355DE22A106896A0941ECC,
	LeanTweenExt_LeanMoveZ_m94F3AB61DB4EBFE4F39B405E5EFE8CD3EC9F2A36,
	LeanTweenExt_LeanPause_m46A64B42348CB04917830211A8C40548EED616E6,
	LeanTweenExt_LeanPlay_m635E84DBAF3658181BD11102F39B17126695612C,
	LeanTweenExt_LeanResume_m3499A30358E3318BB0F73B0B1A07AC9C00E463A4,
	LeanTweenExt_LeanRotate_m3663A0A9B43101BE83E0113F198EDE1DC7A61A4E,
	LeanTweenExt_LeanRotate_m0050CFBB76AE4B345C300EE00F8F13584DAF39A9,
	LeanTweenExt_LeanRotate_m8B46191A435295E93DB9D3F3F982A468D020E54C,
	LeanTweenExt_LeanRotateAround_m94E627C88BD4C138F9FD7ADE41FA86F614D4F26B,
	LeanTweenExt_LeanRotateAround_m8D2DDA35FF67D85798119A863ED498C97980575A,
	LeanTweenExt_LeanRotateAround_m1C2F12BDAD3FB54F4DB76AD02D759ECBCD15289E,
	LeanTweenExt_LeanRotateAroundLocal_m6939D0BB618449C6DB007BD42F9A4C898BE97618,
	LeanTweenExt_LeanRotateAroundLocal_m3977508F1548419CF11A64412C95D747F1C5BE71,
	LeanTweenExt_LeanRotateAroundLocal_m83B8A190A95128CD31886544515865EE7553C0E0,
	LeanTweenExt_LeanRotateX_m1945A6153A434154D6ADB55FDDECAC9CCC46BDB7,
	LeanTweenExt_LeanRotateX_mD17F3CCE10C947CB82E9EC222B0810EEA727FCFA,
	LeanTweenExt_LeanRotateY_mA27D8888248D81C3B8EB606E0E5DBD47716C3A7A,
	LeanTweenExt_LeanRotateY_m9984A206D938B5D53BAC015F467C919DA595BEDA,
	LeanTweenExt_LeanRotateZ_mA27C015E2FEA84D212B0C3D03D6E511035E133CB,
	LeanTweenExt_LeanRotateZ_m8CAA7A563407C9E73FA9B514AC08A9B2C0225A97,
	LeanTweenExt_LeanScale_mB0084B9F5FC69561F247954CBED4BA70511D1F96,
	LeanTweenExt_LeanScale_mAE75E6B1234A072C02D7354461EAA9EB3D485E76,
	LeanTweenExt_LeanScale_m4ED118613DFC434764355605FD977578EC755910,
	LeanTweenExt_LeanScaleX_mB4F38759A8A24E581E837A8A696D9AB275172103,
	LeanTweenExt_LeanScaleX_mB89FF0BBCE3BE85F916A6A7B1D8E6AE9E9E88A7A,
	LeanTweenExt_LeanScaleY_m64BF74A2E7E6B81DA3DA69F7F29AD5EB6DC894C4,
	LeanTweenExt_LeanScaleY_mBCBD993A50FC95AF46ADD7992EB634D05875AF55,
	LeanTweenExt_LeanScaleZ_m42B1B4A160D482743F667A0E398885BB3C976787,
	LeanTweenExt_LeanScaleZ_m821101BF3F56706E65FE11C856D03C1F4C02393E,
	LeanTweenExt_LeanSize_m99C3564D6FB42626360A9BB25BD965F40F61B376,
	LeanTweenExt_LeanValue_m0ABC90AD97B6278EBAE75887B295D8EB1A4CA6E1,
	LeanTweenExt_LeanValue_m6192DF3CB7E8ABD9827718DF1C26F4892DCCA3FA,
	LeanTweenExt_LeanValue_mC3261ED9FF392F50DD1713982ADD612BBAD5266E,
	LeanTweenExt_LeanValue_m5757F21E978B62AF23CC5E142AC222BB4A081784,
	LeanTweenExt_LeanValue_m49CCF82C65BDB92A503F5C61B0EA0121BEEA646B,
	LeanTweenExt_LeanValue_mD59F8CFB5C81AF1061A856CDB0F42244708BC1D6,
	LeanTweenExt_LeanValue_mB5C28F3E39E0871176731D189EF33CE24E7A6DD3,
	LeanTweenExt_LeanValue_m6062D935C55251AC83C7D7D71706FBB350CD375B,
	LeanTweenExt_LeanValue_m7F3A44B64D4EB8824AF830E9BCEDA6B5463099AB,
	LeanTweenExt_LeanValue_m0663B7A32820E3685F4447EFC55951C507170CF5,
	LeanTweenExt_LeanSetPosX_mB614BC76AA5B0934B2F156A9A781DE7DB2A9AEB2,
	LeanTweenExt_LeanSetPosY_m8935780D650702F87FB47C62CB3BA667F4153BAE,
	LeanTweenExt_LeanSetPosZ_m2D663CFB259143ECA22EEF0957396362A0648A7F,
	LeanTweenExt_LeanSetLocalPosX_mC5B1B50F60706CF6FF542A63287C1B4DB534691E,
	LeanTweenExt_LeanSetLocalPosY_m9D85F225670E5804C0E9E944161F520536DD81C8,
	LeanTweenExt_LeanSetLocalPosZ_mDEF497EF11DC45905E787A89A835EDC0BB24CE40,
	LeanTweenExt_LeanColor_m3C101DA3D605DD4228FD3E6BD67DA7B2C6D61438,
	ReactData__ctor_m17087AC6A13423903F1853349D20C6E9C99303BB,
	ReactController_Awake_m913CBEA8EF07DE6F3670DB4D731313C5D255FEB7,
	ReactController_onDestroy_mAB9768E447FB391AA7E2773706449AA63AEADA9E,
	ReactController_SendMessageToReact_m43250135A3A5994D2255F3037A8272308F8F3DD8,
	ReactController_ShowAR_m25E41FB6D16D381F84877E33E701A35DFE8506C7,
	ReactController_HideAR_mE9D19346DFC619E808F3D79A2A140AB6EC9CD9B4,
	ReactController_Clear3D_mD4FDCCD9C42BDD634CA99C70B56E1A86728E8105,
	ReactController_GetReactMessage_m558C33E7FB302D7E53033F957D2DB243F3F9B480,
	ReactController_EmergencyBreak_m16CFFDD939BDFE747A1FE827F2CADFFFAC1C6584,
	ReactController_DownloadAnimationRequest_m01D3184285B7A75BF0F5BA663D5D045394155DBD,
	ReactController__ctor_m007DE9D24E2057495560F62158F0A8CDD53A2DBE,
	MessageHandler_Deserialize_mE6CDB21EAEA2491B135D4DDC69091312F68ED426,
	NULL,
	MessageHandler__ctor_m9B9239E75ED95C1778288A2C33692FABD066647E,
	MessageHandler_send_m6E4C9DE3BD5C18B8DD958734711E4FA0803B895C,
	UnityMessage__ctor_m418530AF3414A53A37F1433AEE0C8A83206D44A6,
	UnityMessageManager_onUnityMessage_m8A904C01129EA827FDD76B08B34FAF9A6C4B5E36,
	UnityMessageManager_generateId_m105F25DED7848A21DD040716977EF8B8785C3846,
	UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16,
	UnityMessageManager_set_Instance_mFD260C3BC291C3924C61F0724C6969AB0336716F,
	UnityMessageManager_add_OnMessage_m2725B086DA28C9E73814BE3D96E21BC3F3E3BAA4,
	UnityMessageManager_remove_OnMessage_m67B6DA617E39FEA4523E49A8D40590E6F6F19B07,
	UnityMessageManager_add_OnRNMessage_mCE5F29BD032CE5A7F4106AB17E31CF4AD8538EAE,
	UnityMessageManager_remove_OnRNMessage_m511526C94D646292B680A2F6448062D3EFB819C9,
	UnityMessageManager__cctor_mA16D91AB3FC363648DDA304555ECCFFF699591E5,
	UnityMessageManager_Awake_m1C1E9A8AFB3258EB62A404F13644CBB101AC02DA,
	UnityMessageManager_SendMessageToRN_m04A9CB5DD296CC74889F72AF6B1E62664438F728,
	UnityMessageManager_SendMessageToRN_mB4A20BB4D59ED754C40DA2C5CC09FF853E517887,
	UnityMessageManager_onMessage_m6E60A5C3A00B83B070276B32B727847F9CEEF413,
	UnityMessageManager_onRNMessage_m57E23694F75F5223BC44C0CFA568FE2A59118EB2,
	UnityMessageManager__ctor_mE89F6AEB4A75A1E52A8DF59747ACC13D3858C7C2,
	ARObject_Setskill_mE17AD5A903DC9B3039AAE716AED6717D7E8F2B66,
	ARObject_SetCameraStatus_m824678FE34CA38612CAABE52EE6EF1E8B6FBE51B,
	ARObject_Start_mB3DA5B76805B4E7243A80B9359152E88758F4362,
	ARObject_Update_m7AEEB1F52448E77CCE90B1E13335C5F4CA10678E,
	ARObject__ctor_m78CD5BEDC1A8C65B55D53C1E01F52E8016157C50,
	ARObjectManager_Reset_m9AB20BA849B03DC8372BA5B152D626DEE446274C,
	ARObjectManager_SetARControl_m806D32457C69D1F834DBF402C038E2BA63E76B2B,
	ARObjectManager_ResetAnimation_m0471D070157C1E91499E54C57F1A3A6BF7A96001,
	ARObjectManager_SetFoot_m3D09494940D211D4D8BBD1DFE668D20D60FF651E,
	ARObjectManager_SetARSpeed_mE18074E3BB1A679571EB39EBAB0D5279D8F7036A,
	ARObjectManager__ctor_mC207298C17C8F99EE271A398F093E329638301A0,
	ARViewManager_SetSkill_mB511EB85FF255073E75B8AF1F893C6758ADEA69C,
	ARViewManager_ShowMessage_mDDAEDBD866F97E03B175D0252332F29C6D39364F,
	ARViewManager_HideARView_m1AE02B33559C8C76FA3F1A3506C24E65D58A8870,
	ARViewManager__ctor_m509908C0DBB609958F6C6FAB655C5694B2E1EE95,
	RecordManager_SetToRecord_mEE79C055C17D2BDD5CA2239E72B44815E1DF7C21,
	RecordManager_StartRecording_m6056BBCB3076486C933FDDB321B22DC8EF88AE9B,
	RecordManager_StopRecording_m2480416D3FF7F9FB286904B639D869E86B083477,
	RecordManager_ResetInterface_m7FB8517FE4CD674889E4251AD6CE970B18DCA492,
	RecordManager_ManageHideObject_m4645284829BA86723605822892E5339B4113D6BB,
	RecordManager__ctor_m4A4A202FA7FC57936A4944E88F05601238A4F677,
	AnimationGIF_OnEnable_m07A9A38224CD91EE9DD25F7F7FF29C82F060C460,
	AnimationGIF_Change_m4A59ADB7EBA9CB202D38DF6DFBFC8EE7C57F7AFC,
	AnimationGIF_OnDisable_mF28EBA3D1374C41679302930715ECF4C6428BE1E,
	AnimationGIF__ctor_mF21E90099C57C49BC57B739EE573E5F61A62DDB9,
	AnimationManager_AnimationCompleteHandler_m00A096BD951B015BCF3A4DA645D98FADA0506C5D,
	AnimationManager_AnimationStartedHandler_mF3001108FE9B26A85A3BEE23B12C520B2B6686E0,
	AnimationManager_OnDisable_m0957B0989BF2D1C1BBD27A3CDFF7484B6930519F,
	AnimationManager__ctor_mD0BF8A23C4E13B5F7427A1CB25DE31267832D578,
	AnimationSimpleController_Start_mEDA07690FFEB97B7E484A17DD92D2CD94ABE37C1,
	AnimationSimpleController_PlayAnimation_m99196AC322C8C22201A17F199BB2B9C58C34F62D,
	AnimationSimpleController__ctor_m5D13FDE6D9460838A9620FDB3369810BDA559E0D,
	CameraControl_SetCameraStatus_m03156EB1D5F26527D6C70B80BEA477C9C74E0257,
	CameraControl_OnEnable_mF7C9358B55CE44CFA7BB472CC0D0F4668C2F81A3,
	CameraControl_ResetCamera_mD93197DF31358AACA48E4CD4BC3F283F8C7DEFB5,
	CameraControl_ZoomOnPC_m10F73839A3FC2B9F4F858B8E3432E5A46C29D8E4,
	CameraControl_Update_mADA15FA22CFBD70CC2D84EB6270E470B770C253C,
	CameraControl__ctor_m40CDAD0D4D59CE6C6EC7BD8A5EA9048B5DE4C162,
	CameraControllersManager_SetCurrentCameras_mA10220ECC7D63F26C2E68798621033F34F660211,
	CameraControllersManager_SetCameraIndex_m1B4E82952A0509C9DAB842AED6E5F4F7E5C54675,
	CameraControllersManager_ChangeCamera_mFC9042999E5F537B442141F4ED0420A19D400247,
	CameraControllersManager_ActivateCamera_m39415E903B3516F33D7AC2189646BACB1CD032E1,
	CameraControllersManager__ctor_m43DE492CADC50D6097A72ED0740A6493A472A63B,
	SoundTester_ResetAudioPlayer_m6226A766AC08A48ABCF0B7EB61A9A51A2A76F939,
	SoundTester_OnEnable_m74E345E5C1DCB0E55F1D378AA58D6DF2C878FB73,
	SoundTester_SetLinkToSound_mD3F06D31FD237135F0C896E8D5D928D4A532F252,
	SoundTester_HideSoundButton_m112A9ED037AD7D2624032B3182C40D3AF332BD78,
	SoundTester_ChangeLanguage_m1AC6AD1139051B0DA4E6CB53EA67ACECB46443B3,
	SoundTester_SoundButton_m8CFC5E9EAB950C1255D8832F28A3418FEA889C68,
	SoundTester_PlaySound_m4F04A6FCCE7133D1341CF15300808663D4DC9C0F,
	SoundTester_StopSound_m9DAA3B76B2170ACA3B2C3F6FB8CCEF391F78693D,
	SoundTester_StreamAudioOnMobile_m0B08846F432195C0BC09FC4C509AC74848770B45,
	SoundTester_GetLinkToSound_mDC680A8F598DCAA7F3F87AE85040ED5E2D35EC18,
	SoundTester__ctor_m3B4813C3108B39E2F9E416D5905BE281FC4A8542,
	TimeLineControlller_OnEnable_m08A28AE7057C806227062C57209CF40F46EBE3CA,
	TimeLineControlller_MirrorEffect_m854F9D86CC36E07F4523E7FBC1EA9F11B9186A18,
	TimeLineControlller_SetSpeedToAnimator_m46F91DE95AF693E44C6B25740842EDC6B9A00932,
	TimeLineControlller_ResetAnimationTimeLine_mE217205349F484ED2A716AD3F3981CE9790374C2,
	TimeLineControlller_Play_StopAnimation_m10A664842571FD45AA6C3035526D75199A38FCFA,
	TimeLineControlller_SetPlayImage_m7A76F43800C5C5985F5379422B42596BF3275AAF,
	TimeLineControlller_ChangeAnimatorSpeed_m6C677FF71FE1B20A39896557ADDDFEFE440587C0,
	TimeLineControlller_SwitchPlayer_m453D1CFFA3B66FD4BB15F78C58E7AC73AA91F9AF,
	TimeLineControlller_LockTime_mB260E03FF5F97CB811B8E2B25BF7F66E939E98A6,
	TimeLineControlller_SetTime_m99D190D519B557BA777B454DB26C41EA83DE15C0,
	TimeLineControlller_FixedUpdate_mA37A71AEDF4DD9E68ED6CEB9F4ADC696AC282E54,
	TimeLineControlller_BeginStep_m760367395B28B54F4A1DCB5617EAE98827D3521F,
	TimeLineControlller_SkipStep_mCDF5A1B93B75A31843CEF6BDE48A325B68F1EC7A,
	TimeLineControlller_ClearAnimation_m4361933B27352FB53959803ADC818265E335205C,
	TimeLineControlller_SetSkillAnimation_mCE35EFC39A63249CBE40D30F53776C3DC230DAAB,
	TimeLineControlller_SwitchAnimation_mAFD403BBA0BF6A6A2CAAAB85E2CA6671C8863867,
	TimeLineControlller_CheckAnimation_m21E91967BA17764F81938FC35552EDF996773B0A,
	TimeLineControlller_StartAnimation_mB642B4F3476D9082EF678D069D3146C7980235C0,
	TimeLineControlller_StopTimeLine_mA31B81020F5D5A42A7A5FD489676651CFA6DF089,
	TimeLineControlller_SetAnimationToObject_m259DB3D7CA2EE3C2C31D101354295DD90E561D44,
	TimeLineControlller_SetPositionToObjects_m2DC42022267BE715246785E5FCD8CA17047D906E,
	TimeLineControlller__ctor_m9BE6CB570DFDC5850609F49B3F43034E22074D4B,
	TipManager_Awake_m000FB3B4CA0443C8C68DD78A9331160A994DA418,
	TipManager_ResetAudioPlayer_mEFCC944999CBADA77A24E0FF3F0250E231CD3F91,
	TipManager_SetLinkToSound_m496CFEA39BDCDE7F1912547FB5F547DB34E359E7,
	TipManager_HideSoundButton_m819440CFE82E32C6C6B62B0C6AB74748F8A0F960,
	TipManager_ResetAudioPosition_m5E7D7C9D53708211C1666349760F99020C8635CB,
	TipManager_ChangeLanguage_m798AB21D10C430095AD9ED812133E9D7862B584C,
	TipManager_SoundButton_m1257563C8BA365E6DEA723E91334F5C696D69E24,
	TipManager_CheckMirrorChange_mB58EBE19191FB24BD2E13D7575717637E466595F,
	TipManager_PlaySound_mB3830875E147C9C4775F53B20B49089A54AAD8B4,
	TipManager_HideMessage_mBE52A9E50DFB01683658A1EF0DBF79C3829D4E86,
	TipManager_PlayOrPause_m433443D5D965DB13617EBD7B4DDAB27F7C6EA313,
	TipManager_StopSound_mB4A783532642F7AA9185FBAB1CE64A72CDB30B6E,
	TipManager_StreamAudioOnWeb_mDBA347C9693F5398AA9950F75AFAF3CF1CFB35C0,
	TipManager_StreamAudioOnMobile_mC699C40E3B6DBA260C3E3D376754D2186ECA0973,
	TipManager_GetSoundID_m0A32F3BDD0FA1DBAA989ACB1932D17C8F59DD144,
	TipManager_GetLinkToSound_m3959D9148C188F8D88168C57FDF586640EA0B47C,
	TipManager__ctor_mD574B22018D873BDBD9D408C03EF4F662BED4E18,
	BackButton_Manager_Update_mEFD4086ECF498471B9BFEEDCB584942BAE69ED7B,
	BackButton_Manager_DeleteButtonOnList_mC4DDE0069BF9D4DBC02060E182DB9ABB76E79A5C,
	BackButton_Manager_ClickOnReturn_mF0CA908D751FE44F36BB64962EF364FB691F7B44,
	BackButton_Manager_CheckForBackButton_mEA1B2FD7A2DBE6424AEFF64C807A326E474E0CE3,
	BackButton_Manager__ctor_mECB7D3ED4CC60BA22056634381403C1FC5B8D543,
	CanvasScalerUpdate_Start_m3D671E88705C5DC400AA47145325066B344297DD,
	CanvasScalerUpdate__ctor_m283118746556B9A04D6A291ECF690CE5AB64E970,
	CoursePanelManager_Show_m3A8DA9906705EB9DDED1330FACDB3515011AF70A,
	CoursePanelManager_Hide_m424A304923888994DEE24086A3ACB3E10AA6F207,
	CoursePanelManager__ctor_m891889CCBAF85190CDC1A057C0A4048135E1968B,
	FollowingCamera_Start_mB1E40300679345FD55773B7597D937257BCF1509,
	FollowingCamera_OnEnable_mD2E66B6E35D34BF42F511B24406ACEC0EB234B81,
	FollowingCamera_FixedUpdate_mAA1A98F01A343E41B562E8A957B8BFC30D7AC348,
	FollowingCamera__ctor_m1BE8AED0308D1E26C647C6A9990476E90C31B7F6,
	HideViewsAtStart_OnEnable_m5CF173FE4EB509546CA9F22EA5B7D49F1C5A7DDA,
	HideViewsAtStart_ResetContent_m3B3AB2AF4EEAA2548D4AAC9C8377B890BAFBEC9C,
	HideViewsAtStart_HideObjects_m56E1536B0E19197CA92E5F077671F5EBF1B22539,
	HideViewsAtStart__ctor_m956D74DAA9AF42CDA8F4CB8B0E4DF432FD8C8407,
	LayoutDelay_OnDisable_mC58599D98A58687482376BD413616EFFDCBB0A9E,
	LayoutDelay_SetStatus_m7AAC055DFCFD1171F13C93C390E4AFA45ABDBEAA,
	LayoutDelay_OnEnable_mDEAE0EB8DBBFCE6076487DC96DFA56D0367ACE3D,
	LayoutDelay__ctor_m0A4BBB551FF423171F0AD78B5AC266719BCB99B2,
	LogoScaler_Start_m559E4F99342FCB9C518939551CF14AB1BD0E382C,
	LogoScaler__ctor_mC9928E011E1DF1B65071C90FD02D7CED2F66179D,
	PanelSwitch_SetPanelSwitch_mFD70D297431440643B091DE7FB4DA68720370A68,
	PanelSwitch_HidePanel_mCC491BDF1471DABB5954A047459F6E0966A08F88,
	PanelSwitch__ctor_m83D98FB5A26FD31AD5B5E8AF6679D4073663E439,
	PlayerWeekItem_SetDay_m2296AD540E4EBFEB63A11D5330438B676B844AB3,
	PlayerWeekItem_SetBackGround_m6F99A4318D11D7D0FFCD97D237C952046E05A354,
	PlayerWeekItem__ctor_m447F1E9099EA31FD21023F4F82EE3AA2ADDACDD0,
	ScaleToCanvasHeight_Start_m222E7B3EF8D6BCFF15D2E307A9C685B1489D6D5E,
	ScaleToCanvasHeight__ctor_mDABF8CEFAF59856242AA6F85607998E36A9D725C,
	ScreenSpaceAdapter_Start_mE8BBBD581E56E9DB300E05CD226D2C01527BD474,
	ScreenSpaceAdapter__ctor_mD0DE7AAB253B2AF984FB03EA06435BC6AA4450B5,
	Tester_Start_m149A96941BB9C65ED600258DA659F0443B5DDB0F,
	Tester_LoginToAccount_mB7358E36C1ED5FD543B8A6827197E251173B5B89,
	Tester_ComputeHash_m67D993E45D772E00CC8418A2722022E0784F4601,
	Tester__ctor_m893C374AFBD19F9D3738B19D4140A8227FF029E6,
	UICanvasMessages_Awake_mAF30192C1FE23040860F608268118C6BE5CD2373,
	UICanvasMessages_ShowMessage_m78FF2BF77D46AFB54E31068F0F8FAF1299AA2851,
	UICanvasMessages_HideMessage_m01394AF240938014D1EEBC13165D31A879E33A49,
	UICanvasMessages__ctor_m795898ABD17D85F9BB5EA922CBEFB1C65D5EAD53,
	ServerUserData__ctor_m31D14ACE9D3A9126F4F3D423FBC5E7D9F74D4136,
	ServerTeamsData__ctor_m3016A1B4A67C65940168969CB0F28482C6C08ED4,
	ServerTeamData__ctor_mE4537499959C59EDD8E8B1BCA8E699316DBD5308,
	Data__ctor_mCD3AD6E8D195C4D9716A6478E83258C452C89C07,
	UserData__ctor_m17E413FB2279BF05C1465329FF401F61253AFA54,
	User__ctor_mB5B1D42F0980D69CF10E95B3BF591E010A500A53,
	Team__ctor_m3E77144218E6D3D1EC63590741559CD75000ED50,
	CoachSkillsList__ctor_m8B4F78A4CEDEDFFC6BCB9EE6EEFD11272C33D613,
	CoachSkillsItem__ctor_mE72F6373D41E753598E4A93B8B40B72F4E812600,
	PlayerSkillsResponse__ctor_m5E65AE1A8225B9161BFC30B6739F83963AD87529,
	PlayerSkillsData__ctor_mE84299CA58CA5455E1667EC91FC6E2BCD769C178,
	PivotData__ctor_m186643368771B95059BFBB9356D6291FFF29533A,
	PlayerSkillsItem__ctor_mF973C6B8AB4158D4FEE142AA98C7CAE0ED1D2C6E,
	SkillsReviewResponse__ctor_m49478E3F00DE2050E957C6B6F1D90E4748398B2F,
	ReviewsBlock__ctor_m9D8965DF8CF9E140AEDA09CE9ABC0C895E0CAF85,
	PlayerReviewData__ctor_m24F116E9BFCA37D887ECE3B374408FCA217D91FF,
	ListResponse__ctor_mEB4ACDB4111C7848142EDEA70E16B096B241FFD6,
	PlayerData__ctor_m8A621C041AB02CB7E2C83D0DE68FEDF629440AE3,
	BundleData__ctor_mDDCAE043BA4D4DA0D85205F3795AF803BD28FDBB,
	CoachSkillDetails__ctor_m3F882A64C493695E3B9CBE66580FFA9F86017485,
	CoachSkillDetailsData__ctor_m8DEC32A8A603D7F8AC17804CD8E65B1DB8576187,
	RankingResponse__ctor_m86376EA957E23C2C64D3363CA7CC06ECEA4C821C,
	RankingData__ctor_mB215EB5586492DDADB91F0D2E413AC81647A9FAC,
	NotificationsResponse__ctor_mBE83CB743CFAB01067DCD09F58403B52368E718A,
	Notifications__ctor_mF59746E75E6D494784F4CA3E0C59B00C91C11EC8,
	ChatNotification__ctor_mBD750D07BC336211D12DBF38EFC515735F840782,
	SimpleResponse__ctor_m9E0C8C49526B50FB5661488BA97A83EE373F4269,
	OneSignalResponse__ctor_m8E21C7F5B7D45980750C44CC358116215BA9AA9C,
	CoachDataRequest__ctor_mD742AD2B243E69EB295A808F4BB43E963B60A637,
	CoachData__ctor_m7942B8E537E4B13B26A8BBF80A2770235C969703,
	ChatResponse__ctor_mC1F4A089130332367BED09FAEDA70C04A1E3AEAA,
	Chat__ctor_m05D44117EACDBF7B3E6CC07D06D2C2CA5FC46E9F,
	PusherResponse__ctor_m9F510102109179A667947647AB092F88CAC41704,
	PusherMessage__ctor_m87591149AEB9EF2CBAAA2928038CA4ADBF271E21,
	GeneralTeamDataResponse__ctor_mD8BFFA5D1EC73FCF7AC5315A72CC201B00A02E37,
	GeneralTeamData__ctor_m1991DD58A9A0BD27C61B0300A068F16DC5F73FAC,
	EventsDataResponse__ctor_mC5D63B2C2D25F194747F2691918B128A1F534088,
	EventsData__ctor_mDF785F2C16C0AF770CF8131232EB5B6380AB8CD0,
	MessageToDelete__ctor_m4E4209D2A3A421F03A06C4B212B7AF9B75FDA73C,
	PlayersNotVideos__ctor_mF018C5C94D7CCA54E560DC3EBEDFE27DBB4577BC,
	SkillNotVideos__ctor_mF0773660B2B69FDABA5B825007D7E423271FBA03,
	Utils_GetCleanData_mE7063B99E9BC89CAC79A186FE7812AC5F5581A9A,
	Utils_ComputeHash_m3F021E5385B1199A03FF84CE0E38CF559F58C498,
	Utils_GetCurrentPlataform_m2C379C3FCABE1614B436B7C7E9BECB40317443FC,
	Utils_GetTimeAgo_mD443EB772566421784C838FF25B44B62571B2D4B,
	Utils_GetChildsInContainer_mB2A3205244D62C03C1141F29B489B0566673AEA0,
	Utils_ClearChildsInContainer_mBE88354E3CDD6DE3644CCDA0816B2E05A0C904D4,
	Utils_GetCourseID_m25DDEFFB89C417B87B7B4BDA45C716B8982E2AA6,
	Utils_GetWorkingTypeDisplay_mCCB053E0A1E5AC672F9C97D8EFB7681A8C4438D9,
	Utils_UpperCaseFirstLevel_m35D3848467D6F69AF04311DCE12B4D1E6A2A2421,
	Utils__cctor_m3F050F239F46DED13E183D1F52F0E57D46665693,
	SkillAnimationPreviewManager_SetCone_m3E768683BDA02DEAB490C6CF11BDC60199468FFB,
	SkillAnimationPreviewManager_ChangeCone_mC2084EAD2438D267E663861E2A764E4F041AD98A,
	SkillAnimationPreviewManager_SetSkill_mD497872AAE5788680505126EB3D332C993F17527,
	SkillAnimationPreviewManager_SetExercise_mC0528DED9B0E49E6357A7A88EF8056949F43C035,
	SkillAnimationPreviewManager_ActivateCameraControl_m1CB3C6B6677CF5E6A778B6978048F4ED1F957974,
	SkillAnimationPreviewManager_HideAnimationPreview_m5BCC7526280E5FCE3B4766258A9D74BC9CBD61C5,
	SkillAnimationPreviewManager_ResetAnimation_m73A5539DCD802A6A280A677F9F75208013EEFA99,
	SkillAnimationPreviewManager_LoadARCanvas_m821F284EFFC518A99C49176B01D31BB286F29331,
	SkillAnimationPreviewManager__ctor_m631D29CDD3E0714C88BA43A5C00DFF8894341F58,
	SkillsAnimationsManager_ClearList_m804B19E1538CC452B87E7AA3F32522A080688427,
	SkillsAnimationsManager_CheckAnimation_mA2E84388486F36C04A09426B66855DFACCDD91D1,
	SkillsAnimationsManager_GetAnimation_mFA003D9F11B15D3166016B1A36C8D63B873A4797,
	SkillsAnimationsManager_AddAnimationToList_m1696113D86C04B7F3DA4BE07DA420DC01BCB9976,
	SkillsAnimationsManager__ctor_m2D5A4E351D2BAD3FCCE776E09D1F53FBA7740BEB,
	SkillsDictionary_GetName_m579596021C007D4CC5FF5CDDB01AC6AE5F14E85A,
	SkillsDictionary_Awake_m2252A10894656F7990C7F8B312F242FFF879E21C,
	SkillsDictionary_GetExcelCodeByID_mF1CBE106C6E5C7EBE74DE2804FEC7DFD3344410B,
	SkillsDictionary_GetExcercisePlayerCount_m5909209C29BC0F8033B389280858A0B6BCA95F1A,
	SkillsDictionary_GetExcerciseByID_m4C846EE4A7AF7FAE293DE376BC48B45B3F1048C6,
	SkillsDictionary_GetIDByExcelcode_mFA29ED3A9F1C9CA012939D3557E54B14A5BD909F,
	SkillsDictionary_GetExerciseSteps_m5CE551B195588EC7FE6DBAEC625518C6717EBB01,
	SkillsDictionary_GetExercisesName_mC4D88A3AFE6007630BEA750A49505AC456995639,
	SkillsDictionary_GetExercisesNumber_mEED389C5526CFF26499D088AA4EE29BA50557E01,
	SkillsDictionary_GetSkillName_m13E573D637E3F5A3091948643D6CD8DC33DC794D,
	SkillsDictionary_GetPictureSkill_m4D24FCB9795AB756BF4B397432591B2C0B43A37F,
	SkillsDictionary_GetPictureExercise_m1017F7B6CACB12FA8B246CC31267EAFEBDEC9DF5,
	SkillsDictionary_GetSkillCategory_m9E5CD913A7483131039A104F93CD3204D42ABCD0,
	SkillsDictionary_GetAudioFolderName_m01C59C0314D7EF6A15EDAE9249A5E013A2B2919E,
	SkillsDictionary_GetSkillCategoryID_m58E126A0EAFABC7BB95807E92B8677F266D1B0CC,
	SkillsDictionary_GetCategoryIndex_m8AAF38A446E1C042721CE228F6A8D5AB682131AE,
	SkillsDictionary_GetSkillLevel_m6D78D1522F72FF3A649007B73EB79D693818FF2E,
	SkillsDictionary_GetCategorySkillsCount_m4599F3D64FD1F5A0566281EA5726A5CED4A6DD24,
	SkillsDictionary__ctor_mB16D28E4B9F93A949565AF6F98DCA26BD182D4FD,
	TranslationDropDownItem__ctor_mF4E35B3958038DCD0ACC590EFEAB975491F7E3B4,
	TranslationInputItem_Start_mC71BE956F92A2BF96CCAA9BBC086795F5BA7F0FC,
	TranslationInputItem__ctor_m038A24B857584A9CBBB1114B3141A2E63CD710F9,
	TranslationItem_Awake_m6D9135A9FE36371914D0B314BA157AAF49A3D1F8,
	TranslationItem_Start_m41270ABB11EC0D93284BD80988591962CBC73CA7,
	TranslationItem_Check_m7B6206719C37A0FBA33A65DEACB0A097506D851D,
	TranslationItem__ctor_m389D38D92E3249BDFF73FBA2B1D0B16A27508D3B,
	TranslationManager_Awake_m0287A8E51604439F789686A56F940D85820BFF87,
	TranslationManager_GetTranslation_m1228F93C6240D0B4A1C540A59A35345B4A0F36E3,
	TranslationManager_CheckWordInList_m43AAB95197DD6EBED05648DCEE210A988507AC23,
	TranslationManager_SetResultTranslation_mEF01ED25DD903A08F69D75DF27DF81E1E422CE3F,
	TranslationManager_FillChineseDictionary_mB4B1EB420E74479D9C96993AA91A6021D86F5ADA,
	TranslationManager_FillSpanishDictionary_mCB68D538A20F4F27EE36ABA10431CD87511CC9F0,
	TranslationManager__ctor_m36189DCD06C73713B72C42ABFE2579B044A4109A,
	UIControl_Start_m29A668948A3DABDD046DC4CF5F12DFEA8656AA00,
	UIControl_SetPreviewElement_mF829A395F7867A94C4378BBCBF274A99D36B3880,
	UIControl_PreviewElementState_m0DA18655474B520E52288A8092480F5D1C4A602F,
	UIControl_ShowPreviewCanvas_m98C3B51E6E4ABF7BA73CD4DB609F6E9947D86383,
	UIControl_ShowMainCanvas_mA6E4D2A61ADE7F47CB5E78DFA10616D6EE5BCEAF,
	UIControl_SetPreviewScale_mA468EC06328AD0953BD0681374B9D0A78A29B2D7,
	UIControl_HideMainCanvas_mCBE08EA82F760AFA366C953ADC055D06A38830A4,
	UIControl_CheckARCapacity_m76ADF9D6688E9DE7490DA28B9E8CB6424B828023,
	UIControl_HideARCanvas_m44FD87263B0A70629D4987933286C52DE14CDD07,
	UIControl_HasPermissionForAR_m956B0D950D0144FD9102BCA0165625182EEA9190,
	UIControl_CheckAndroidAudio_m979DFEB0EDD1A3E42227C5AFCE6236C4785C21B7,
	UIControl_ShowARCanvas_m8C90624ED0B02DB5FAE62E02F8820D723107ECF1,
	UIControl_ShowCanvas_m1DBFDC7C797C43087F1FC8151AD7319FED172D4E,
	UIControl_HideCanvas_m3CFFD3EEE63588BBE44AF2FF63E24539DC950FDF,
	UIControl_HideCanvasInverse_m55583D87411CDAB3BA086E6012F06EE900B14A39,
	UIControl_SetActiveState_mBECDC194AD2E9498401D5E48EF6B00281CD6EF60,
	UIControl__ctor_m4AAD1A34B5E78D619B2A13D4427B3A3DF54254C2,
	VideoPlayerController_Awake_mF6E1AA703F1554EB66A033C575655370E4493C59,
	VideoPlayerController_StopVideo_m1CDEAB2485DA294C8F8B61694B3C6DBC57103926,
	VideoPlayerController_PlayPauseVideo_m2C4B0C90F5C62F3CD76F713BDD3558CAC6E02D82,
	VideoPlayerController_FixedUpdate_m1D939733CB33C4E4843A2C76D1914726D44033D1,
	VideoPlayerController_ClearVideoPlayer_m4C6508F4F2E52523894B1F80B79A6AAB52D1AB79,
	VideoPlayerController_OnDisable_mA3EAE527C98EC4BACAD95FA0F51BEAC4AE0112FF,
	VideoPlayerController_SetVideoOnPause_m3ACF8F8502CAC3980175FFAD44A5607DADED7624,
	VideoPlayerController_PauseOnFirstFrame_mF9F7006247F442FE443068D65A57E1EF9CAC1FA4,
	VideoPlayerController_SetVideo_mFBCA0C702139D1AD62A3F19B51DFDB798DAC16FB,
	VideoPlayerController_GetFirstFrame_m7E5F7E23EC8A78726B52DBB1FD1D6C284F0F265F,
	VideoPlayerController__ctor_m7F79CC84BE3EAD9D76CBFE8B13AA3711A573244D,
	VideoPlayerView_SetVideoView_m8CE9F2AD1B556D92F92ED01820E6DEC5062E00B5,
	VideoPlayerView__ctor_m93E08BE6AF15C299166586166B0D81234EEAECC5,
	VideoUploader_VideoUploadRequest_m16F30ACD3F84DA549990BABF008AC2E262197F02,
	VideoUploader__ctor_m6728F827B620AAC67D8D57B6A7C876B345B89981,
	CryptoTools_cifrar_mDE04EFF48265C24BEDBA3F0686820E527B1542A3,
	CryptoTools_descifrar1_m4DA6E0B1DF0751B09CD77816E8887BAA439B6B16,
	CryptoTools_descifrar_m92DB500742AA4D25B6A87F22ED6A48028C64261F,
	PlaceOnPlane_get_placedPrefab_mB16E4C48DB7636A8947E69FC174B8ADDE72C9A03,
	PlaceOnPlane_set_placedPrefab_mCDE62C5196B4ED0382F261FA28670EC75A0BFA03,
	PlaceOnPlane_get_spawnedObject_mD0CF235CF8E541887A9FFB2C17E371EFB1EEB566,
	PlaceOnPlane_set_spawnedObject_mF5EF3DC967889CD70BD8745BFA4DD34C14F68AB9,
	PlaceOnPlane_Awake_mADC207DE2F5C12243E95D153CD46C810DA7DB142,
	PlaceOnPlane_OnDisable_m3C9D6C8DB875B36A3C8685E8A7584574F9BE05E3,
	PlaceOnPlane_TryGetTouchPosition_m1FFA9158FDEB61C7BB6319C588CDDD0B390FBFE8,
	PlaceOnPlane_ResetAR_m688C0ADF99DF3722A526D4BAF45DF16F5D8588DB,
	PlaceOnPlane_StopAR_m2B95DE99D8B5213B68F398AE82F7CABBB399C6C0,
	PlaceOnPlane_TestSetARObject_m5EDD424845836D39DCD5C8FF6FDA3C676D1FAA2D,
	PlaceOnPlane_ResetclickDelay_mA5A6951A3A554F988ED02BF1C979D9A9B5E8BA2D,
	PlaceOnPlane_Update_mD220C63CD54DDBAE6C8B6862BEEC4654F956910B,
	PlaceOnPlane__ctor_mF199E5F3E123D8F1C8E111E0773093C8E2E0A2E5,
	PlaceOnPlane__cctor_m797D1E1F09619F492C3330746975202822EC387B,
	GIFRecorder_get_frameSize_mAF322B62FC3A6D89C8A96AF422C33C042DCB5F19,
	GIFRecorder__ctor_m7877546D7323542EE5B85A7BB154F7CF770D0B24,
	NULL,
	GIFRecorder_CommitFrame_m3C8F811EAE575642F6C44D49630D51B40E257968,
	GIFRecorder_CommitSamples_mAE812C381DA1714761F257D00EA25FB766D6A6E3,
	GIFRecorder_FinishWriting_m60E9D529C5E9DD5997D9741CC1F6756338B6EB43,
	HEVCRecorder_get_frameSize_m94F4A45264A73D19410AE8DC0061974307A1F29B,
	HEVCRecorder__ctor_mF6EE5B8778DA065775FBD166ABDFF189429FFA3B,
	NULL,
	HEVCRecorder_CommitFrame_mC8B7C1C1FF5E7B361BCE109240AE04E1C30E2DDD,
	HEVCRecorder_CommitSamples_m6B9585012799677A81CE10DDAF53E3EA08FAE795,
	HEVCRecorder_FinishWriting_mF0DCDA4A632D6CB1FB9B4E78888F367F03EA77F2,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JPGRecorder_get_frameSize_mA2FB0F92242454DC8FF8870CB0B41B639CCD939C,
	JPGRecorder__ctor_m9D6F75AF0202F8747BAA5D53BF56789EF9811F44,
	NULL,
	JPGRecorder_CommitFrame_m5B4D55C02454FF2FED0BB874788F531DA08630DF,
	JPGRecorder_CommitSamples_mCDB906AA2C0DF6D034E8BB7AE6C18622B27140E8,
	JPGRecorder_FinishWriting_m2C76C18768813218838577D6CF0FC36C7EA77AEE,
	JPGRecorder_U3CFinishWritingU3Eb__6_0_m37DE8E330FF45AC97764BC0EACB5CC53DEC85644,
	MP4Recorder_get_frameSize_m19577C451FE6E3F1A654BB11D1F0ABA7DCBF033E,
	MP4Recorder__ctor_m0D64C114C1434E483486F0794E482E9B4FCF431D,
	NULL,
	MP4Recorder_CommitFrame_m9BB983BD9C296E7AB8784B4543ECF884FFDC702D,
	MP4Recorder_CommitSamples_m040DED0BEA13E3257637BE6192868A2EF81153E9,
	MP4Recorder_FinishWriting_m2DF6A9BEBDF38C66E0413084BA873FE68FCB92F9,
	WAVRecorder_get_frameSize_mD98B044EB00E32D8C3A79D00151CF2C0321DFDB5,
	WAVRecorder__ctor_m8FDACDDCDEFEF8EA49DFE4F39A047201D35FB633,
	NULL,
	WAVRecorder_CommitFrame_m4DE08233AA6FD2DF73EF87D5095FF143E613190F,
	WAVRecorder_CommitSamples_mAEBB994CDA4628B4CA680C918FCFF61A0660F881,
	WAVRecorder_FinishWriting_m96367BC5B7D51B42E8793F082317DD714536F6A1,
	Bridge_CreateMP4Recorder_mA4D58809A1CF0F3C399CC88501E6200000FEEA79,
	Bridge_CreateHEVCRecorder_m62B4CEDCFCA39E5549A2FC337F639D886BFF39DA,
	Bridge_CreateGIFRecorder_mD63D11381D872AD072A0C176DD72E9DCD1A18273,
	Bridge_FrameSize_m085AE9D1EB78FBFA20DE33D36866C18195F1EC2D,
	Bridge_CommitFrame_mB0B9821E7984177FD2A536A3F0F63337939C197B,
	Bridge_CommitSamples_mF543FBE6F6681F1B24C176423EEF45E45AE24750,
	Bridge_FinishWriting_m46DEAE40768F2585AA1C9592D09D4FE15E6A2242,
	NativeRecorder_get_frameSize_m6A2EEB68481BA1116A448E01DCC86FB0E52BCD52,
	NativeRecorder__ctor_m5A1D1E64B4134B06E6971A27B02C5A603737D319,
	NULL,
	NativeRecorder_CommitFrame_m24C7A6DFB0BCFE3AE7DEE1DBC501B77917572773,
	NativeRecorder_CommitSamples_m884990214FA2D83654CB2067148128BFAB23F54A,
	NativeRecorder_FinishWriting_m2CC5461B4C949B310AE8B096816E81C13CC0A6A4,
	NativeRecorder_OnRecording_m6E2D200EE07CE507951155CB24CC8107AFE8D13B,
	Utility_GetPath_m9311C5984418D9EEFB0C1ED622A14626FAE58C39,
	AudioInput__ctor_mA3FD59F98FE959EBB21E6C0D579BAB4663805122,
	AudioInput__ctor_mAA11A2C5840D9129AF876501389E584F0D731725,
	AudioInput_Dispose_mE1741B608B54087C1B26B3C12574D7DE4335FC79,
	AudioInput__ctor_m1E7A78E02D9B62BF0A53D7494A11EFA09A0A53DD,
	AudioInput_OnSampleBuffer_m9FC2F8C4DB48899A9542D1938175CC8DDB3DDA3D,
	CameraInput__ctor_m4BEA8A9CD3DC78C65B73FAF449D39D0437B87BDF,
	CameraInput_Dispose_m6CF1EE3A4CB113950E8E15B3A5B4FCAFC84B6E5F,
	CameraInput_OnFrame_mE39BF5A7C297AB119889F937CC639CA79614C3E1,
	FixedIntervalClock_get_interval_m7629C2CDC1961FE469AD24EC047D3150AA6F50BB,
	FixedIntervalClock_set_interval_m4E4B64B782129CB871EF55BDB7023A2D1D15E531,
	FixedIntervalClock_get_timestamp_m950563A0D70E959160C27CCF7F760DE408CE13F4,
	FixedIntervalClock__ctor_m7448F63B6235668B8F4B682239500CA14F5FCE58,
	FixedIntervalClock__ctor_m9D109F10A6215B3E24795FE5BC5F2709E2F703A7,
	FixedIntervalClock_Tick_m17067E2F79EE296B3DADA632973515686450E085,
	NULL,
	RealtimeClock_get_timestamp_mB99369062BEDDF34284318C4A0F3CB49059C0C81,
	RealtimeClock_get_paused_mA2DF19C0093B9113982809B0759FD67C0C531189,
	RealtimeClock_set_paused_m8FFEF530250DF267554F59DA87BF6F576E95AA15,
	RealtimeClock__ctor_m020229BB99250085023A9D2B1354656862C937B5,
	Giffy_StartRecording_m85B30F8AAB7611332CBCDF523445A0785FEBA41F,
	Giffy_StopRecording_m32BFD9C6A6A4BE5345EEF2A9A445A9B9C13F5EC5,
	Giffy__ctor_m72758AF6110585484B5DA7F5C9892B6611D847B0,
	ReplayCam_Start_mFE062B76895DCE7C9E9CBD3CFDE34A33855DEC89,
	ReplayCam_SetToWrite_mE349A2F35FE0E21184A6A9FBBEA078BB8F6EF045,
	ReplayCam_OnDestroy_mA747A2C64F7189EB9696A8ACFCCD2480986EB63E,
	ReplayCam_StartRecording_m0748785F004077E5690F3973DA07ECD80DC9CBA6,
	ReplayCam_FinishRecording_mEFD5EC691EFA02898A4308B8BF480B9A756456FA,
	ReplayCam_StopRecording_m84CAB6D17FF50FA7B730A58B0D95792C7358DAB5,
	ReplayCam__ctor_m6BA6E989AEE2294EFDFBA2C7313441030DEC8019,
	CameraPreview_get_cameraTexture_m9BEDA6FA7C02BCFF7BC2618C4A8DAFC783A93DF2,
	CameraPreview_set_cameraTexture_m58D885DA978EEA3754899E1D750C8CF911778B08,
	CameraPreview_Start_m2DA911FDB8E31958749F15562F46B193E2C5E1EF,
	CameraPreview__ctor_m5F6E72C6C0780A2FD0020884F8DD6642717BABC0,
	CameraPreview_U3CStartU3Eb__6_1_mE72436C867270099B3AE9FCDE9FE400FEA5AB02C,
	RecordButton_Start_mB17D2C0A02D5C7BCA97CED224BA88627FB167572,
	RecordButton_Reset_mE67AA7E8CDF5B27D3938D441DB2986AB68204F1C,
	RecordButton_UnityEngine_EventSystems_IPointerDownHandler_OnPointerDown_m98470DC813D8CE0E525BA5BF94633E7289C1FCEF,
	RecordButton_UnityEngine_EventSystems_IPointerUpHandler_OnPointerUp_m47DDA104F10D93EFA358ABDCE593E42E0A092EF8,
	RecordButton_Countdown_mC4F140B46C883162C87733942011BB476CF151C0,
	RecordButton__ctor_m4B6521954A3A7A08F6E2FD40C55D1990D0BBE9FA,
	LeanDummy__ctor_mAD9437EB3765999702C790EAC2DE9FEB4442E092,
	PathBezier_OnEnable_m56E18FAFF5DFB0ED61856F90EAFEF6BDD116E2DD,
	PathBezier_Start_mDAF11682189F6675D4FDD93D652A898A3347B50F,
	PathBezier_Update_m3CC2AC4CACBB72CE7640DC38DFC4F19689EAB564,
	PathBezier_OnDrawGizmos_m345FFC89DBF05FCF81AA56654D2764EB6199F256,
	PathBezier__ctor_mB9E6AE4BEEE953C926C93944334E5F099FC262CA,
	TestingUnitTests_Awake_m3084D275E0537EAC8B54FE41937CF217BB332504,
	TestingUnitTests_Start_mE8442EF073840E58B91FD7047EC0D8708D70D928,
	TestingUnitTests_cubeNamed_m46762B53A8B1B8EC91E95B1B886E0BEDA7F59270,
	TestingUnitTests_timeBasedTesting_m5510CA65F550F87AAD701A457ADAF778B46B5758,
	TestingUnitTests_lotsOfCancels_mF5CDB314DC950F82FF777A507914DAA972E57D08,
	TestingUnitTests_pauseTimeNow_m038EABEB7FE1D5E01BAD36F26EDAD62253864249,
	TestingUnitTests_rotateRepeatFinished_m75177B46DFB956A9AC0870AC6E1E767A09E41A65,
	TestingUnitTests_rotateRepeatAllFinished_mE2C3707783D21F0E24E390907EC41AAD46537D53,
	TestingUnitTests_eventGameObjectCalled_m696EE506C9313CDC2955B76C394C7D8DBAFA10D6,
	TestingUnitTests_eventGeneralCalled_m2ACEC83AC54AF7D79C1BFB26F9D63F2201886540,
	TestingUnitTests__ctor_m1551B3D3BCD78DA068595A4F9CA1934061711DA2,
	TestingUnitTests_U3ClotsOfCancelsU3Eb__25_0_m22BEE01EA5BDFFF8A1DBF6DB397911D91B74FABA,
	TestingUnitTests_U3CpauseTimeNowU3Eb__26_1_m4F3F229D6D22317DC30FF68B332B22A3CB6CFE68,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MpegFile__ctor_mFB15E79DE265EEC45B668518410A6FEABBC06028,
	MpegFile__ctor_mA74201D73EA2201136F5D7CB1DC439F04448431C,
	MpegFile_Init_m6FBBC8ACD222EC8B750B52059D82824F12B20541,
	MpegFile_Dispose_mFEA66175E3182F36DE298BF10C21953FAC1201EA,
	MpegFile_get_SampleRate_m8B5D45191008ED6EAD554DFC6B2286935468A3F1,
	MpegFile_get_Channels_m585D4CE88E9981B196394E711792A045EFDDF3EE,
	MpegFile_get_CanSeek_m33A9736A5B4A99E6B75965767B1B7682ED8B4C80,
	MpegFile_get_Length_mD157C580874BF121659678AA740ED7669D99AAB3,
	MpegFile_get_Duration_mE98347B59C07FAE86C52F8A30BF73BDACD54C96A,
	MpegFile_get_Position_mF5299BAEFEEEACC7433D2F195357CD1BC7751617,
	MpegFile_set_Position_mFC180DC8A925684483FAC01426BAB2AE441B693E,
	MpegFile_get_Time_mECAE917124E89D00DB2DFDB92D8B551898920450,
	MpegFile_set_Time_mEFB4D4FC0C086D782801BD9532331B277771063B,
	MpegFile_SetEQ_m05FEE93494C2E7F9817A87B764583A50CFDEE72F,
	MpegFile_get_StereoMode_m08794FC310CFB0DA19236B772EF1C4957A19B3E5,
	MpegFile_set_StereoMode_m7AADBDF4CF454A1295AEB0E50D01C50F2B674D3C,
	MpegFile_ReadSamples_mEB5411FDE75024631E0E50BF18B14AB98E756370,
	MpegFile_ReadSamples_m198848186BC5B41FC179B69035B1E8E4D00DBBBE,
	MpegFile_ReadSamplesImpl_mBCCB01C47CF5EA05A5F60F7D662C2EA28F481F2E,
	MpegFrameDecoder__ctor_mBB627C81E59763B314F303391EBFB3B0BEBE9288,
	MpegFrameDecoder_SetEQ_mB5BCD4538E3168ABB562663B24B1CDEAB01C8233,
	MpegFrameDecoder_get_StereoMode_mB5F3AA3369522D108193283455B38361B4CA6D5A,
	MpegFrameDecoder_set_StereoMode_mFF9AEB280D043F79C0CDF71F3CDFD46249833C8D,
	MpegFrameDecoder_DecodeFrame_m3A78C8BCBB40C95CA9B1FEDC0D462AF4651FA1AC,
	MpegFrameDecoder_DecodeFrame_m1EE49D1829C38E005B233CE734218012C6FC3AC7,
	MpegFrameDecoder_DecodeFrameImpl_m105DAD491B3AAAF670334277440254A2268A3A0E,
	MpegFrameDecoder_Reset_m7A3309B349B79B7A91067DA43E5AF23127A95BC2,
	BitReservoir_GetSlots_m032F3C5A05CB2F1F15E3E878358E1C7B0A46F8D4,
	BitReservoir_AddBits_m469E338C830BB8C3010A68DC57423BBD2783CDF7,
	BitReservoir_GetBits_m073222CC5C65DAE676E64DADF70BBEACA3349005,
	BitReservoir_Get1Bit_m436BEBD334302CB6F69955143400FE328C601045,
	BitReservoir_TryPeekBits_m182CF2486C83737707AE35845F7C5B41B5C08D94,
	BitReservoir_get_BitsAvailable_m1B4A1627734B83C5A7C49DE8736B774D71648CCD,
	BitReservoir_get_BitsRead_mF0BE03D1C23C59AE833A9AAFDFF47E708C9B41DA,
	BitReservoir_SkipBits_m065D078B44A93BF0B491D5D0A3185E362ABA9A9B,
	BitReservoir_RewindBits_m8C790EAC08D378EE9CBDBB4CF666A2BB02CE80B8,
	BitReservoir_FlushBits_m17CD6AAE51599A6C0C30C9D51BCE0D41F5D4FB65,
	BitReservoir_Reset_mE80EC3ADA04AFD7044A72D817BE506DEFD8E9E7B,
	BitReservoir__ctor_m1F82DB4407FE42E0646CAB925DC20640B9A2838F,
	FrameBase_get_TotalAllocation_m69162765AF5EB3937E1A020E1B0BEB112C2BC0B1,
	FrameBase_get_Offset_m54677B6390075870E56C050F5742F9F88A70FF97,
	FrameBase_set_Offset_m8C60A5D36824695685A6B02D43DF37887AF4D540,
	FrameBase_get_Length_mD2AC76CBBCD49FC2501B8B6FBB2F15D4D500511F,
	FrameBase_set_Length_mE9B332C5BD797F4C51D98A02E8EBAA1E78312E53,
	FrameBase__ctor_m5456D68301AFD7E15DD8D3E78BCE0C2BC1544589,
	FrameBase_Validate_m5A6B9CDE4E29D240D72C78275BB09EA561061655,
	FrameBase_Read_mC048B35C2BFFA1015521CE6858AE7CBEE68B26C4,
	FrameBase_Read_m84D84692DAC1C64E2517ADB8F2B4D31F16ADACBE,
	FrameBase_ReadByte_m28F5B6B19F978875FB35AD2B37CAF343F78703B2,
	NULL,
	FrameBase_SaveBuffer_m3E6525C8A71075D51F59199E01DBEB9A3B194710,
	FrameBase_ClearBuffer_m3BB469BEE3915D2C11D7D8E963F34E5E03FFACBB,
	FrameBase_Parse_m7E4B608486875C9FF3E43DD18D1C55DA5F1D2E2A,
	FrameBase__cctor_m5F6F31D86F9500C934F72C8859C9E1FD30642DD1,
	Huffman__cctor_m7B8DADA4ADC76C59176BBFAA1A1CBBB33AE7CB7C,
	Huffman_Decode_m2623F7274FC62D2E4697917389CC6BB285CF0040,
	Huffman_Decode_m5028306C222D2728292C149B377A236AEE4C0D71,
	Huffman_DecodeSymbol_mA8CD271AC7CBC35DEE2D932F7536242A1CACAC8D,
	Huffman_GetNode_m6D51CFE61384D7E01D76DFAC14E3B665B084A1C1,
	Huffman_InitTable_mC68008DE3BEEBE1CE09244DD311ADB28F18ED8FF,
	Huffman_FindPreviousNode_m3FB37AC184D34CD1B5C8BD421D6169C3CDFBF80E,
	Huffman_BuildLinkedList_mBF31511EB952039A4AC9B104BD9A67DFD8AFFB20,
	Huffman__ctor_mF52C1F71E017B1311C66D92BE2A5A80E07DA45B0,
	ID3Frame_TrySync_mC379E95EED0416B260AC9D5D922A1CD84ED92C03,
	ID3Frame__ctor_m174991CB995510152626FF5ABF604B90F90CB744,
	ID3Frame_Validate_mD596BDA5C213849B2869070ACD4669A624661201,
	ID3Frame_Parse_m1D4CCCB256CF9D70418AB69C84D2528E76F49DF2,
	ID3Frame_ParseV1_mD8F1CC072DF3477E0BC9A9D2B206AE4AF8179807,
	ID3Frame_ParseV1Enh_m13894656FD36D16CBB779B5D2C69F9F5D343E2A8,
	ID3Frame_ParseV2_mB3D9933FFEDD21B76F8DD6FE5AEBC4DEA59B7EEF,
	ID3Frame_get_Version_mC1FC4F976CBEDA52A6DE2A793354A8EA24211C72,
	ID3Frame_Merge_mAA10D3DCE6270DEED43AB935674EBF1A99E2D50E,
	LayerDecoderBase__ctor_m2FB0CF1FAC0F102CE987253F730D2FAAAE7B570D,
	NULL,
	LayerDecoderBase_SetEQ_m7F2B584CC71FB80C4B4A692A6A3246978A5B392A,
	LayerDecoderBase_get_StereoMode_m3AC9AF5A2C4FD8402C2C584B1CE1F1315993E13E,
	LayerDecoderBase_set_StereoMode_m7786DF542234CA67311C4B833CD03A17EF5867A7,
	LayerDecoderBase_ResetForSeek_m166B7A0865B319168B5A4CD2811E5B4DBA9C4EE0,
	LayerDecoderBase_InversePolyPhase_mFA06A4667AA178D8288FDAACA437929F0E121431,
	LayerDecoderBase_GetBufAndOffset_m3CD9EFF14B4A31BFBA7DD3ACF7F4B9060EC3F066,
	LayerDecoderBase_DCT32_m541F06FEEF44FD7B0394BE4C4A00122E5340A4BB,
	LayerDecoderBase_DCT16_m646DCD43DC6C4A1CD226CBBF0A16EEDE19480B0E,
	LayerDecoderBase_DCT8_m516633F85CFB259835C87C4F206FA2E8EAF642FD,
	LayerDecoderBase_BuildUVec_m0B4A80BFA03758E2A04D1C364EAEAE3219134B65,
	LayerDecoderBase_DewindowOutput_m9350FE27C82AC48AD91A37497A174261EE9A8AED,
	LayerDecoderBase__cctor_mC13C7CC12A58AFDC8B31BDB87E25C85AD0E8696D,
	LayerIDecoder_GetCRC_m80B901A65C78F8C5768918D0942BA839FC96F4AA,
	LayerIDecoder__ctor_mD05629CE4CFDC7A23A8691FDA86096C69F99ABCB,
	LayerIDecoder_GetRateTable_mC290B3CDBB4592A95EC9A5CF7AC5AC2D9C9E345A,
	LayerIDecoder_ReadScaleFactorSelection_m6ABB789F4243BB767B919F595AF11C2211B08EF7,
	LayerIDecoder__cctor_m55151DC556480CFE8702C5F7BA80CED2A6B164D2,
	LayerIIDecoder_GetCRC_m7972A57A5A9A8D3ABFAD20CBAFD9737075DE66A4,
	LayerIIDecoder_SelectTable_m1D9D5BB876FB10F6B439F9988AF1D40BE6CD82E2,
	LayerIIDecoder__ctor_m1C4763F3A5737B2E169E27F2ADFEDD2EBCD8687A,
	LayerIIDecoder_GetRateTable_m36AA219FB08EC9E85481597E390B155EC8D354B7,
	LayerIIDecoder_ReadScaleFactorSelection_mB0218F443E1C6ADF336B7AD4BC7EAD8E875EFB05,
	LayerIIDecoder__cctor_mAFF960A22F720E245BFD414131B8532C8E7FBAD6,
	LayerIIDecoderBase_GetCRC_m3A637A3508AD6415B0B12BDAC4B87103AC8A9A4B,
	LayerIIDecoderBase__ctor_m2557B21C6E19BCA8558BA3C49DF4C891DB78CED2,
	LayerIIDecoderBase_DecodeFrame_m5DEF4ADB1631DD9D1BB317730BC871CA0B86AFE7,
	LayerIIDecoderBase_InitFrame_m01F24B652652AE4C68EDEA5D6CCAA68A028442E3,
	NULL,
	LayerIIDecoderBase_ReadAllocation_mE6056ABA874F51F23D8AD23E47CCF95C5E78A178,
	NULL,
	LayerIIDecoderBase_ReadScaleFactors_m14C55D5ADC0013B53BC9A029C076A926EC580B97,
	LayerIIDecoderBase_ReadSamples_mE5B1D05DD0739E17E19F1ADE005A310CC8C0DF44,
	LayerIIDecoderBase_DecodeSamples_mCF93F19B426D2B7E627C49697899A412DF161E03,
	LayerIIDecoderBase__cctor_m33058A88A2ECE907E44CABCB97CCC2CDBE9C9C42,
	LayerIIIDecoder_GetCRC_mA762E60BB5BB46E82333EB7F6E30B0EC7A114125,
	LayerIIIDecoder__ctor_m59D94A35985438D67A24703D720E269BBFF34DF0,
	LayerIIIDecoder_DecodeFrame_m8CD38BDA09E14D557D86E9C67ECFB02763959DB3,
	LayerIIIDecoder_ResetForSeek_m7C3A815B46F765E41AD3212F352BEE3592CCEB28,
	LayerIIIDecoder_ReadSideInfo_mAAD0E009FA8C4DE5B90F8E76504BDEFF98F8FE5E,
	LayerIIIDecoder_PrepTables_m6767FA3E6D63C3FCE93242E95D167971555FE3C5,
	LayerIIIDecoder_ReadScalefactors_m9BBD0DBC9C553F2688F08CC8FF43952CC521601F,
	LayerIIIDecoder_ReadLsfScalefactors_mB5BD006E2794CE1DC0806F98F3BC36CDE0F609CF,
	LayerIIIDecoder_ReadSamples_m56543FC403FEED6DD08A164DD75EB77ABEA02AF4,
	LayerIIIDecoder_Dequantize_mF11D342C277919587F80731398F5CA8DA422453E,
	LayerIIIDecoder_Stereo_m9CA43FC7865CC4A4CF4FEFFC97530DA2E322EE43,
	LayerIIIDecoder_ApplyIStereo_mCB1BDCBE6F3F0D5375E2597041B2820A9F22A163,
	LayerIIIDecoder_ApplyLsfIStereo_mCE7AB8867EF652C4EBD566199B357A80C6A7A90F,
	LayerIIIDecoder_ApplyMidSide_m4D50C6868AE76ED51A00F6B50697B6926E2B76F1,
	LayerIIIDecoder_ApplyFullStereo_m826B48DAB2E1346FC04C137E1BE6D233C3F85FBD,
	LayerIIIDecoder_Reorder_mB166BBEF760D6B3094C2A77F9A2629FD0A216F37,
	LayerIIIDecoder_AntiAlias_mC07D386CA37EA15BF91851B850E2F04D10E03214,
	LayerIIIDecoder_FrequencyInversion_mFAFC6B768E0DE2D49323F5BA526A00213A534F9F,
	LayerIIIDecoder_InversePolyphase_mE56EF75768AA7F1BC11DA76D4ADC5F12D6987B35,
	LayerIIIDecoder__cctor_m79FC606E25AFD6182ED42B1A4A83282DBC7F8FF8,
	MpegFrame_TrySync_mA2F9E0B424917911C69D2611B772A36160EF617E,
	MpegFrame__ctor_mB33368BBE38295360623B4BF7E532A44789B2DB1,
	MpegFrame_Validate_mAB148AF751D3EBEB8DE9D88EDBB713A7268DF857,
	MpegFrame_GetSideDataSize_m7C3469AF367C78BD4DAC969BAE613331286BCF07,
	MpegFrame_ValidateCRC_m7A0C132B8537DD26A1DBECC6D9FBDF02BB5B9C90,
	MpegFrame_UpdateCRC_m39C4D5997725E8EA7519D9FC05CD7D58448A8F2F,
	MpegFrame_ParseVBR_m9D53FB11B6CACEAF60931D38B5ABCF59E2E722F3,
	MpegFrame_ParseXing_m137F01E11379BABA6AADBF7066E2AEE8EA420556,
	MpegFrame_ParseVBRI_mDB620A8FFCF302DEA8C1E4D4A583062F0E18FD8E,
	MpegFrame_get_FrameLength_m8FCFF91C7E9773C2698C16F3E14457FACAA3CC75,
	MpegFrame_get_Version_mECE3628C4BB434D01FDC41452F8BD96FFDDA3BAD,
	MpegFrame_get_Layer_mAD5ADF3F8661CD1D96F40B81CB85A5E45FBF67A0,
	MpegFrame_get_HasCrc_m08A3018F2B7BD63C982C8B7BE155FFFB1B425F08,
	MpegFrame_get_BitRate_mF34C7D6A4B02D779AE4019D9A625D136C26BDF5F,
	MpegFrame_get_BitRateIndex_m2005274EDD6777B29276B40C5B1653DF42024460,
	MpegFrame_get_SampleRate_m9859E1E23F2415F99F49242D7A66A6864CB383E0,
	MpegFrame_get_SampleRateIndex_mC0A05DBAE0D69343BB88CAE7699ECA70292EAF41,
	MpegFrame_get_Padding_mEF4771A1091820578F89528045835D8236F3ADB5,
	MpegFrame_get_ChannelMode_m5B768659D14EF410CA3F4628A279DE67F5DC8A89,
	MpegFrame_get_ChannelModeExtension_mC839FF648C9FB2F59C508FF3E0D43144DBF96F87,
	MpegFrame_get_Channels_mB418127518D4303F6D2165ABF37D2D313D690694,
	MpegFrame_get_IsCopyrighted_m708F6FC8EC8B568A497CDF6CA2723A981B51C432,
	MpegFrame_get_IsOriginal_mCA21CC98194187A0A5F5963EAB85F7BC0F0455A7,
	MpegFrame_get_EmphasisMode_m9CCF830D93B62FE7C91C24A0BFA33F0D33B5098E,
	MpegFrame_get_IsCorrupted_m4764C3C3F99CA4783DBEA62487A3ECA5F1C25864,
	MpegFrame_get_SampleCount_m52B0FEA0229C858D4FD689F078DAF0FA58BF67D6,
	MpegFrame_get_SampleOffset_m442BDBD5267BDF53B477EC2988B4FD8F107E4E4B,
	MpegFrame_set_SampleOffset_mB6F46B4BC78800421B3E5AA86B4FA1800FB0E052,
	MpegFrame_Reset_m1075651C53C86F2AD8109A7F12858CDAFB52601D,
	MpegFrame_ReadBits_m0F1878B5C926592F3E962F05517485D0BE1B8936,
	MpegFrame__cctor_mBD3A9B4254CDCDE7782647D0E8B4D4B22444CBFF,
	MpegStreamReader__ctor_m9F788F233AE95B18E3EAF03832D42A7911D8B75A,
	MpegStreamReader_FindNextFrame_m45477875ACD3088A1A33EB742CD047A90CB118D8,
	MpegStreamReader_Read_mF7D592DCAD05FFF63F88C38DBE10B07F3884F520,
	MpegStreamReader_ReadByte_m8C0A2B85ED50851EB8D643CCA0519D507401986B,
	MpegStreamReader_DiscardThrough_m5F66D5C58E38E09E7B94B7A4F6F6415D5A2DC01A,
	MpegStreamReader_ReadToEnd_mF9DFC708B355ADA6D86C04109729B6A22B2629DD,
	MpegStreamReader_get_CanSeek_mB751C1C28960A488B7668D513F425A1E88D3B78A,
	MpegStreamReader_get_SampleCount_mE8E99390BAE792FA8821B799EFBA900F200B6E28,
	MpegStreamReader_get_SampleRate_mBDCC536FE8E5A321A19C53ED39AF6A4CA52641AC,
	MpegStreamReader_get_Channels_m89F592253425DC228421E222234E1BE050330C08,
	MpegStreamReader_get_FirstFrameSampleCount_mB670A954B42621122E7E322DE227C96767A9EFBA,
	MpegStreamReader_SeekTo_m39BF65CB214DA5080C0840D24C5C07A2F7008CE1,
	MpegStreamReader_NextFrame_m6B7CD3A6B9CFA20A430BD4D594BEF2BC462823A3,
	MpegStreamReader_GetCurrentFrame_m363AFFA6986273631A9619843B77E6120C62932D,
	RiffHeaderFrame_TrySync_m2F3F582DBF6B6B4C7CF65FD213ABD855EAD18620,
	RiffHeaderFrame__ctor_mCEF63A01A8146F08D9772900E1D0A3BD7BE5B90F,
	RiffHeaderFrame_Validate_m3B8EC8819DA617548D50E48BA89C358683E5A860,
	VBRInfo__ctor_m50231CFDB4DE402338B49BE22E49BCF591ED45D6,
	VBRInfo_get_SampleCount_m245918E68F1A266D07A19249125D2CE3FAAEBBD6,
	VBRInfo_set_SampleCount_mEFF5C767EB3D9556D7DD9910CEFE401EECD16138,
	VBRInfo_get_SampleRate_mAA22CF1A82EB1DBF5B38C85BC4F2FC4B125D9AA3,
	VBRInfo_set_SampleRate_mDF5B021E89C6F8BA7647A2F501C6A3AB29C946C8,
	VBRInfo_get_Channels_m09E9ADC1C92F1AB34B0733BEF57E3BBB74A4D41E,
	VBRInfo_set_Channels_mEE04F37666F219D8328E2936FFA19786C9EF0002,
	VBRInfo_get_VBRFrames_m527B07342FDC2E3BA853881F46419BE9C63253F0,
	VBRInfo_set_VBRFrames_m05F87FCDA21E0E71211A090BB11350C2A6603E8A,
	VBRInfo_get_VBRBytes_m4F0ED263093E832E177888E3A6EEE2184AFEED67,
	VBRInfo_set_VBRBytes_m457BD3FD26B2CD1205E4773F7D5C6593877137E3,
	VBRInfo_get_VBRQuality_mECCF492A4EABA139C6502B70DCD86A527D1CB552,
	VBRInfo_set_VBRQuality_m3A4C4BA379E79452CBC4F1F1FF3BA2D545480C12,
	VBRInfo_get_VBRDelay_mA6E1991C5D05B28548C2EE19A949ED815A28F98E,
	VBRInfo_set_VBRDelay_m2C7452245B88D5D90C43CD7FF883C2A9002E84E7,
	VBRInfo_get_VBRStreamSampleCount_m6EFFAB37F244FCD4234624B31DAC4D691718A8B6,
	VBRInfo_get_VBRAverageBitrate_m9EE890A67B078C344C59C026897B534670FD9F79,
	U3CLoadHelperU3Ed__5__ctor_m55FA1DD43D6800B17B5DFA9B6A10F61371B9726E,
	U3CLoadHelperU3Ed__5_System_IDisposable_Dispose_m83328E4399CA378E13126D1F97741D402818C722,
	U3CLoadHelperU3Ed__5_MoveNext_m8E9CFFDA1AA270321A1B8F0DF718B827AF252272,
	U3CLoadHelperU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD15A5956E5F0B6119FB3FFC980DC14EAC192D1AC,
	U3CLoadHelperU3Ed__5_System_Collections_IEnumerator_Reset_m49B71E2A89FBA8012DED6092EC493360B75EB005,
	U3CLoadHelperU3Ed__5_System_Collections_IEnumerator_get_Current_m6562A56AD68DDD3D9BF6E0983936E972DC40DD3B,
	U3CU3Ec__DisplayClass4_0__ctor_m063B6F62D61CAA29B3AB1AC3A819D6D18240F9F6,
	U3CU3Ec__DisplayClass4_0_U3CUpdateU3Eb__2_mE84C0A7EAF064241DAA6E98CF768F59DF1AB4B9E,
	U3CU3Ec__DisplayClass4_1__ctor_mBB8D9837F5EB6C46A04F399F601533FEDE46756B,
	U3CU3Ec__DisplayClass4_1_U3CUpdateU3Eb__6_m76EC6A2E3975E8A2E6A031E8B6DA4C4088F2A55E,
	U3CU3Ec__DisplayClass4_2__ctor_mAD3D1D58AD09A66A3CFCD2A27D3FDA00EAB9A6AB,
	U3CU3Ec__DisplayClass4_2_U3CUpdateU3Eb__8_mB2A8A874B685A024E5058BEED756182B1B13ACD9,
	U3CU3Ec__cctor_m962DC1D18E6A690D9DAFF83EC393FE4F5ADE18C8,
	U3CU3Ec__ctor_m63711F73205AC47759F3870E385391FC49C03864,
	U3CU3Ec_U3CUpdateU3Eb__4_1_mD2E518290C8AB4EDAC4F79EEF665A4A3430655E1,
	U3CU3Ec_U3CUpdateU3Eb__4_4_m8FA6F3DC6A54EFB3028C4BDCC66F69FC1EFC9A01,
	U3CU3Ec_U3CUpdateU3Eb__4_5_m3952D2A76F4153183CE5361AD763AC2F13EB00A1,
	U3CU3Ec_U3CtweenStaticallyU3Eb__5_0_m9F5117E4B185369A4121FC6E4972F55B802C4F5D,
	U3CU3Ec__DisplayClass2_0__ctor_mEBB010A7E42F38BBA0F6526F4898B3793016754A,
	U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__1_m46CD88EA10100768CC8BC78BD206FB34418B38BE,
	U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__2_mD8310B529F6CC844EFAE6B47D4EE69A074102CAC,
	U3CU3Ec__DisplayClass4_0__ctor_mAE212C3B3074A3A63D0FAFB45B319458DC469379,
	U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__1_m5443364BDD6519C00E41B679475989EC7BFA2A74,
	U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__2_mC7171C077A8800FAA156D255B27FF78E214EAB2D,
	U3CU3Ec__DisplayClass4_0__ctor_m6E2FDBEF2F7D210950B106AA2AC99FB7C23365FD,
	U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__0_m83B800F5E384DA675243ACF6BE368207914E6568,
	U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__1_mA58D74BCD32E0467DF4939BF6A9BBEBEB98B6966,
	U3CU3Ec__DisplayClass4_1__ctor_m90198E5152DCA6BBCF86C52303D7FF53541123ED,
	U3CU3Ec__DisplayClass4_1_U3CbigGuyJumpU3Eb__2_mA7236E71AC8335842B32DED8F5A9BD467BE5E68E,
	U3CU3Ec__DisplayClass4_0__ctor_m99076BF1EC2BA4183686FD305FE9257A1FC108D8,
	U3CU3Ec__DisplayClass4_0_U3CdemoEaseTypesU3Eb__0_m8895F6DF6D44640415A16CB3166821A05AE7FB13,
	U3CU3Ec__cctor_m7E87C117C81452C517C83B26D383ED1C5747A990,
	U3CU3Ec__ctor_mE3568F988560BC01C21F210610E8221EC2136358,
	U3CU3Ec_U3CStartU3Eb__1_1_mBDA4A05B6CF190E80BD4AD54FE5A7B843122E047,
	U3CU3Ec__DisplayClass15_0__ctor_m1545255D46C77C14BEC0F46C3D149CFF14287CBA,
	U3CU3Ec__DisplayClass15_0_U3CStartU3Eb__0_m7360056B580EBA5508B4AAF9B8FE47F13869D198,
	NextFunc__ctor_mE79736745346FAD184C90EC9FFB942EDC6273AA5,
	NextFunc_Invoke_m5F15E6FAEEE04B0364D5861105F46B8CA1D15392,
	NextFunc_BeginInvoke_m8FCE73C9609127DA3C01D221A5D74FAD4CA3EB0B,
	NextFunc_EndInvoke_m5901C99FC6387B6FB299C1C8426F59A415E9C0F9,
	U3CU3Ec__cctor_mF9DA54E17EBF14B22D6DB252952767BE7FD49C9A,
	U3CU3Ec__ctor_mE708DC3155F07189D4E3399F4EF7B6C622475119,
	U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m84C10D9978F636C29F9ED1E04BA0742A68D1C98A,
	NextFunc__ctor_mCCFCFD0AB879EBBB46104ADFF9A9024018DCAFD4,
	NextFunc_Invoke_m84CD7F169D5B749CFE2ED930F9CF0E7B9D41B75F,
	NextFunc_BeginInvoke_mCEE4B7EB7615D52E726E9DB1BF918E0ED3B46FBC,
	NextFunc_EndInvoke_mE35B84DD8B0DD2D1124F28CB2B5729F070AE3FEC,
	U3CU3Ec__cctor_m18DE319729D6EBE31B6DBEEF6AC68B727FEAFC58,
	U3CU3Ec__ctor_mF8076F513CB75419CD4AAF5D2CF96EA750C7C64D,
	U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m8A0627F6E7351B2DF8151D125E1E5045000F449D,
	EaseTypeDelegate__ctor_m479FD2E5E95095E811CD43C107F9F7BEDA5C1722,
	EaseTypeDelegate_Invoke_m6C4DD4B41DD183CD84736AB68488797939157559,
	EaseTypeDelegate_BeginInvoke_mC4A8F044C73F14833A8F0892A1B1D7EFA5719775,
	EaseTypeDelegate_EndInvoke_mBBC06324172B4D605212E2BF2D7568C91D55371D,
	ActionMethodDelegate__ctor_m2C8565D6C66397F9327D19C9D47C45587110BC5F,
	ActionMethodDelegate_Invoke_mD731C08D9044E02DE9276EB7269BCA0F540950EA,
	ActionMethodDelegate_BeginInvoke_mF15334FB75A22146CE827C20402393D61B7203C1,
	ActionMethodDelegate_EndInvoke_m164BF9BEF0C0CB55BC1AC688BECC75811982F9F6,
	U3CU3Ec__cctor_m7B400CF0B15F5A6BB8EDD68761B868209D666561,
	U3CU3Ec__ctor_m3E01BF844F661459DBA93A1A1A2052BB66D8278C,
	U3CU3Ec_U3CsetCallbackU3Eb__113_0_mBAF1F42A0092D36D9EA8B302EAD7E4B64484A698,
	U3CU3Ec_U3CsetValue3U3Eb__114_0_m2BEB42E3CC61F73F63334E99081144AC817C3A6A,
	U3CtimeoutCheckU3Ed__2__ctor_m86660954A201D3F4042917DED992CF67F2967E40,
	U3CtimeoutCheckU3Ed__2_System_IDisposable_Dispose_mE54F8B9D9C634E727ADEB82A507BCC5D2D7F6A58,
	U3CtimeoutCheckU3Ed__2_MoveNext_m81A53EB7B0E977685E46FE5D64A808B557EC5EC3,
	U3CtimeoutCheckU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B28917D1D087CC4FFE00C923F13DE0E3C80DBFC,
	U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_Reset_m4A3583E3E830B3116E7DC2C562734E6153CAB4EF,
	U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_get_Current_m21261FBD3907579D1BC3B9C4B58CB01EA868DCB9,
	U3CU3Ec__DisplayClass193_0__ctor_mC0B0125F1AA00B0D67E5471AC4723D7C4DDF745A,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__0_m1C1C28A5106180AA479E783252142933B5B22CB3,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__1_mA9FBC068978683FE7F41AB0DE6764B37E8F12FCB,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__2_m32EDEE7568C90E8D332BAF204B827BB696F8915F,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__3_mAB9D969B192F8C7F5CD2D8CEFC40A0AB9A1E569C,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__4_mD098FD9EE16741678387663B5E5A20AACDB3C461,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__5_m2D34FBF1D0050BC36587D4AC4AB768CBBAD16653,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__6_m4F2B350D8676AF4E40A9A522AB546E7D64CB5A4F,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__7_m4794A3DFBD9F60DDB39A1D79B3261D224192C6EA,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__8_m8F118FB039CC4F97D3B5099511C0ACF4A16794E1,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__9_m1E35B42D88BA7A620623D40B97FE0A034E6D51DF,
	U3CU3Ec__DisplayClass194_0__ctor_mDCBB6E03EFED25D38C1645524A9CFB06906D64A0,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__0_m0DF3AC96834FE13B309965159F303CF6519EEF50,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__1_mC60E4D28F90F14EC7259E5FC1F36B0C7BA0587A4,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__2_mAC5BC0C40BD4FF4F7A321BB0E5C7F9759AC85F0C,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__3_m7C23E5BA1FAF357B1D25B4DC5B0F587074A36440,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__4_m27AE2F3933DC68B0444E94E4C6C86ED6C1BAE149,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__5_m95EB2A430E734B7AA2726020BB238DF0374206B6,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__6_m15ED9AC2709C71E60DA3125DF3A16E7B8B94C3AD,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__7_m4C0A7E27C26DB6DCFBEFE3C357B8A26CC65474D4,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__8_m763826D5C9CF0FF3D93E27C117BF47A4A2C57BD6,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__9_mAB337657583472749A5443EF146F301CF2391AB0,
	U3CU3Ec__DisplayClass195_0__ctor_mE00CB764397209766E590DF5EFEB52053DC0AE26,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__0_mAC6C7B8F8DF55A577CE8781796BE15239B566E89,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__1_m5184ABA1AB3627B2DDED0C74E2282344378AF3B8,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__2_m34A0E6745FC7434BB5A27B25A097C58784226F3C,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__3_m2D76D667CE87B27FBCAECC55425C3207533F6A75,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__4_m205F4F7C5D9A2B137BCB29491B95C3828F378B07,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__5_mCDC95E138FF86491A853A74BF172BEEB38BECA09,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__6_mDD9833A53793BAE5B0A03C37BA4EF1F60E9930FF,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__7_m74731F175AF6639677E96328854558399D9FE7CF,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__8_m8B743DC6870594CE43D0325E2D884FB0CAF98E20,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__9_m466A3211A248D8995FB3AE22D016F1EFD623A8E6,
	U3CU3Ec__DisplayClass196_0__ctor_m929D534AE634FB58A7C0ECDFB23A1D78351F6F3F,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__0_mA2E1AF83CA2338CA95BA84B64C912222C7A3D9E6,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__1_m23CB51E9EF35556F273C3C0B62C5C06D8A27273E,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__2_m54430A940C2FEE40A54C3AE14482E1DD43CBF103,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__3_mE6D8BB8EFB0E336E59430CA8A2E029CEC51136C8,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__4_mD6D1CF2BB54CAF51FFF102ECDAE4BE98C25FE3E0,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__5_m4244AA15E1AFBF399BA4B03799FDD52F1893D5BA,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__6_mB26E4D96497F6A2BBB615D69D4966E6B8FE8BFF7,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__7_mCC89DC9F63965ABBC1705F61D48B28D83959F20E,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__8_mD29A47A9681480C45508BE22F8600B447930FA58,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__9_m6F5ACA5A264C5CF1B4A20164725E5103856A9880,
	ReactMessageData__ctor_mE2C748F6062AF1973D3346BF833A07A0B0763883,
	U3CDownloadAnimationRequestU3Ed__19__ctor_m0D93AF583ABE22105E87D623D95628816C81D720,
	U3CDownloadAnimationRequestU3Ed__19_System_IDisposable_Dispose_m2947A6FAC770C0357F5CBE03AFE0C9CCE7F0F083,
	U3CDownloadAnimationRequestU3Ed__19_MoveNext_m802960BA99AC130052338DFD818644C420E6B601,
	U3CDownloadAnimationRequestU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0EAB7B4FCFCCE25D7941991368B8E54D24C6B247,
	U3CDownloadAnimationRequestU3Ed__19_System_Collections_IEnumerator_Reset_mFD6BBF6C3A0D221ED9CC1E9C71DADC0CF18037E5,
	U3CDownloadAnimationRequestU3Ed__19_System_Collections_IEnumerator_get_Current_m0316E8724880503F76E4DF5237E3CB66DFEF712F,
	MessageDelegate__ctor_mE80DF7F22AC095655F222BA7B808FB750C059479,
	MessageDelegate_Invoke_m71D9C54B8D33C236D4145F5C171F5057C0A08B09,
	MessageDelegate_BeginInvoke_mAEE45C362BD9197D646B59B1414ABE19C53BF4C1,
	MessageDelegate_EndInvoke_m57B0BBB4D9EBF8DC1F6A7D8A6C449B7C1C75796F,
	MessageHandlerDelegate__ctor_mB09E60C8134DF32641562ED528E1E0E35B40184A,
	MessageHandlerDelegate_Invoke_m82C612B57B22FCF40994CAA70914DD9701E5CCCF,
	MessageHandlerDelegate_BeginInvoke_m3E0807D2B4E1CB959FEEA14DFB15AD08299EE994,
	MessageHandlerDelegate_EndInvoke_mE15D4204B18F5409F5B926454790F915652ED3B5,
	U3CManageHideObjectU3Ed__22__ctor_mF7C41000BBF32CAAB885C13310489E31E20B8C36,
	U3CManageHideObjectU3Ed__22_System_IDisposable_Dispose_mA56142C1BA3143F59115FB890396C747280A6E25,
	U3CManageHideObjectU3Ed__22_MoveNext_mC367B73E612521D103A9C1F328131CF35D90ACAD,
	U3CManageHideObjectU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCC9E897F076F5DD26101ED951027E888A0FC54BB,
	U3CManageHideObjectU3Ed__22_System_Collections_IEnumerator_Reset_mC2C76B3917135B50EFC9A79E36E5C988B28366F5,
	U3CManageHideObjectU3Ed__22_System_Collections_IEnumerator_get_Current_m83C9A9D94BA18BFEB7EC2C5F84FEB12D6ED49A60,
	U3CStreamAudioOnMobileU3Ed__26__ctor_mE986FC0ACBE94C6C1723C557A43FAB11308AA35C,
	U3CStreamAudioOnMobileU3Ed__26_System_IDisposable_Dispose_m279FABF2865FC82585B79B4C8C878F6424DB5DFF,
	U3CStreamAudioOnMobileU3Ed__26_MoveNext_mA4FA3E77E28880656FFDB1959A7F8556EB331D8D,
	U3CStreamAudioOnMobileU3Ed__26_U3CU3Em__Finally1_mCD87FEDFBA7C4F242823FC070BF14EF9DC180A77,
	U3CStreamAudioOnMobileU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8811986F6EB49C2821283FBF3F63F065F1DD2FA3,
	U3CStreamAudioOnMobileU3Ed__26_System_Collections_IEnumerator_Reset_m7DCB573DE1B9346936DEC6BC50B9FA3FB4B5BB2F,
	U3CStreamAudioOnMobileU3Ed__26_System_Collections_IEnumerator_get_Current_m0973C7F77C5847BA5E151963CE0053626335BE77,
	U3CStreamAudioOnWebU3Ed__33__ctor_m038A0609CC5F9BA5A7F08E0F0E70541B3D3854E9,
	U3CStreamAudioOnWebU3Ed__33_System_IDisposable_Dispose_m1D42DF01350A831B6A0EAFE980D3E59FB103B932,
	U3CStreamAudioOnWebU3Ed__33_MoveNext_mA4BBA78C36FAB0DFD277E105A8F8A57FF7A7AE45,
	U3CStreamAudioOnWebU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57B52C4A1778F156702376A347B7BA3CAD549A61,
	U3CStreamAudioOnWebU3Ed__33_System_Collections_IEnumerator_Reset_mAAD13A207912B1911B4083861E89744815391E67,
	U3CStreamAudioOnWebU3Ed__33_System_Collections_IEnumerator_get_Current_m03366AAEEA40F1C66B47F983E0D8029AB2C315B2,
	U3CStreamAudioOnMobileU3Ed__34__ctor_mEDC848B751757398CA20267314B7659DABABB599,
	U3CStreamAudioOnMobileU3Ed__34_System_IDisposable_Dispose_m8EE85F8A75CC349B29D3E4F421F67D987FAE093B,
	U3CStreamAudioOnMobileU3Ed__34_MoveNext_mD206755DEE4AC6A513D6E5F3D8C48400F60EEF3E,
	U3CStreamAudioOnMobileU3Ed__34_U3CU3Em__Finally1_m663D2C77454977636310A764E231A73D47CD235F,
	U3CStreamAudioOnMobileU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9834FB5DAB489B5061069A5F6711D78660CB594B,
	U3CStreamAudioOnMobileU3Ed__34_System_Collections_IEnumerator_Reset_mCA8B10D7C79800F98E32DF399A51E0A156A88EE1,
	U3CStreamAudioOnMobileU3Ed__34_System_Collections_IEnumerator_get_Current_m3A47D9F4D912EA7AD96CFB56E9393107CABCF096,
	U3CSetStatusU3Ed__2__ctor_m3D9C54D9141467C1ED2F12B9DE8D4F3DC980D4E7,
	U3CSetStatusU3Ed__2_System_IDisposable_Dispose_m2B9874E28B9F68112B85BE9DE98B3BDB1B016F7B,
	U3CSetStatusU3Ed__2_MoveNext_m0881E95A92AF445AD808BD18686CB1539D04722C,
	U3CSetStatusU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0FFB79FA7697DEC46130B1B0A725867C92AAED27,
	U3CSetStatusU3Ed__2_System_Collections_IEnumerator_Reset_m54B373E3110D68B142B3E6EC7789004E7807E538,
	U3CSetStatusU3Ed__2_System_Collections_IEnumerator_get_Current_m2178ECF04F71EEFFD8717CE64C8EF95D6EF3D9DA,
	U3CLoginToAccountU3Ed__5__ctor_m784C52FB2CFD64F00942F51C28F052DA16FE9FA4,
	U3CLoginToAccountU3Ed__5_System_IDisposable_Dispose_mF22BF22A5C87F3120AB6DAC4302D50E7E56339D0,
	U3CLoginToAccountU3Ed__5_MoveNext_mEF8437367A47E29B0EDB9997E5B42E935B1B44D9,
	U3CLoginToAccountU3Ed__5_U3CU3Em__Finally1_m1148CBB42434DEED8D7B617F7D948E9B5864411D,
	U3CLoginToAccountU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF4DB56B46DDFC68D114741260AA63AC41628678,
	U3CLoginToAccountU3Ed__5_System_Collections_IEnumerator_Reset_m11DA4B61DFE0DA00B0ED9D09F7DB8A3A5D26ABD5,
	U3CLoginToAccountU3Ed__5_System_Collections_IEnumerator_get_Current_m26D41CC9720D5E00AFF49694B1DB89A72A63E3A3,
	AnimationPack__ctor_mC2A8CA2F5431793BC97330D9F54AB789FE9CB9E9,
	JSONObject__ctor_m4B4369D3E8DF7C754F3CA8C67852448A9FAC9E7D,
	OrderType__ctor_m49ED159AA316907EC00272F89CD765CE4884D8F3,
	SkillData__ctor_m65907EF121D34D195A9F64F4F45E320774F10109,
	ExerciseData__ctor_m8E82EBC94D98A2545723143B8BA7E603A7785B75,
	ExerciseDataItem__ctor_m740F68FDA5630E51E792666952B5876AFFB257AE,
	U3CU3Ec__cctor_m40512A56022041D95B6EE7293569E4B47ABB7EAF,
	U3CU3Ec__ctor_m67A576F2922511E2EBFEB19CF5B5EC026F65E23D,
	U3CU3Ec_U3CAwakeU3Eb__14_0_m88D3D41BC6F22A2C7805252F2174B9CD72434CB9,
	U3CU3Ec_U3CAwakeU3Eb__14_1_m26D35119979F078101FFCE3AB980689F08A44DBA,
	MyDictionary__ctor_mC5163FA9C3B9A2DA8693EF40B10E32005432F280,
	U3CSetPreviewScaleU3Ed__23__ctor_m11ECD093F241F6E5230D346807F8C643723FF294,
	U3CSetPreviewScaleU3Ed__23_System_IDisposable_Dispose_m5F36C8C2836BCF7474D4786875C91F65DC8ABBA7,
	U3CSetPreviewScaleU3Ed__23_MoveNext_mE89D0CCD0A218951BBEF3A082E930670617E8DDB,
	U3CSetPreviewScaleU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC23D393E502DFF8CCC88EFEAC037252E97818011,
	U3CSetPreviewScaleU3Ed__23_System_Collections_IEnumerator_Reset_mA73ED601844C8CBBE092347BC94D05416E76F5C4,
	U3CSetPreviewScaleU3Ed__23_System_Collections_IEnumerator_get_Current_m47B4D54AFDC2686DAD96AB3619087BADB393C311,
	U3CCheckARCapacityU3Ed__25__ctor_mE4096C7DE70B1B0EABEA0E347426BD681B8BB8B2,
	U3CCheckARCapacityU3Ed__25_System_IDisposable_Dispose_m02ED94E1D3D414EAA9E1820A659FADE32ABFA7E8,
	U3CCheckARCapacityU3Ed__25_MoveNext_mDFA00605910C6C101E8D3DB8442CA04126E30255,
	U3CCheckARCapacityU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m606026299A0D44507A8974D1C0B7C1E4046E28ED,
	U3CCheckARCapacityU3Ed__25_System_Collections_IEnumerator_Reset_mC6B250324B06949C68A31C2C96F0A0111B386849,
	U3CCheckARCapacityU3Ed__25_System_Collections_IEnumerator_get_Current_mF138E793AAFBCD3C50C3B1A20496CA23993445FD,
	U3CSetActiveStateU3Ed__33__ctor_m2A8E45242F2D352C5BF0371F8B1E07F2BB4C4412,
	U3CSetActiveStateU3Ed__33_System_IDisposable_Dispose_mE02C1D71495F6949B4959962BD528B2070A65B23,
	U3CSetActiveStateU3Ed__33_MoveNext_m6E53EC3AE222BA84940F7216AEDDCE02164956A2,
	U3CSetActiveStateU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31A4340FAB52B8C2492A503BF3DC2959D57DB484,
	U3CSetActiveStateU3Ed__33_System_Collections_IEnumerator_Reset_mA57F87B3C1DDD172A7856BB1B108F11AAF4CA6F4,
	U3CSetActiveStateU3Ed__33_System_Collections_IEnumerator_get_Current_mC72FB53882F89929CE794D6F9E0ED49F290C9777,
	U3CPauseOnFirstFrameU3Ed__18__ctor_m9335E2E164B1C27AD2C24AA75FA2FDF5BD57163E,
	U3CPauseOnFirstFrameU3Ed__18_System_IDisposable_Dispose_mF59D38B58661388947A435120215BF161F5391E1,
	U3CPauseOnFirstFrameU3Ed__18_MoveNext_m0830B41B39B26E1D2B6908DEC2325BA20886DA2D,
	U3CPauseOnFirstFrameU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0461A57EC47B09376DCF1BFA8E69692CC58DC4C,
	U3CPauseOnFirstFrameU3Ed__18_System_Collections_IEnumerator_Reset_mFA86952F218030112250204D109AD958FE9C92D0,
	U3CPauseOnFirstFrameU3Ed__18_System_Collections_IEnumerator_get_Current_mFE1A6613B7D15853C06D635D0C1925B344F327F4,
	U3CGetFirstFrameU3Ed__20__ctor_m943BFB53A761D1EE372BD24D8D5C54A3BCBF3DC0,
	U3CGetFirstFrameU3Ed__20_System_IDisposable_Dispose_m36E9636217D689DCBD1ADE977C500FF6381B97EE,
	U3CGetFirstFrameU3Ed__20_MoveNext_mB35041372D09F75DCDD243510BBA8FC095F13736,
	U3CGetFirstFrameU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB1DCD5308A20FDB657581990F0706E7486B1D12,
	U3CGetFirstFrameU3Ed__20_System_Collections_IEnumerator_Reset_m825815D739BE2E5FB349C4F3C5D6E0D283D8DC1F,
	U3CGetFirstFrameU3Ed__20_System_Collections_IEnumerator_get_Current_m626ACAC60872AC36B2A169D76BEE165B934F6253,
	U3CVideoUploadRequestU3Ed__3__ctor_mDD4CCDCD8A946756EFF9B4CAEDB2762AFE1EA955,
	U3CVideoUploadRequestU3Ed__3_System_IDisposable_Dispose_mDCC273EE8138DDC7F093A00193545D74F2FAD709,
	U3CVideoUploadRequestU3Ed__3_MoveNext_m77A2FDAEE4D7D32AE81EA34F917E04F85151F263,
	U3CVideoUploadRequestU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF4B8C0859DA4A240756F655F0FC8F68B5FB44A0,
	U3CVideoUploadRequestU3Ed__3_System_Collections_IEnumerator_Reset_mA153D08AE3CC4BF9563AC2DFA68B79198577310E,
	U3CVideoUploadRequestU3Ed__3_System_Collections_IEnumerator_get_Current_mD231BD177DB5BD9D96BBF13204CF0FDF8FD4E9C2,
	U3CU3Ec__DisplayClass2_0__ctor_mFD002E2D6CB7706F0C2243140DCCDDBDABC96B50,
	U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__0_m4679C34A9AB15241F3C60107A64853393F4F04B6,
	U3CU3Ec__DisplayClass2_0__ctor_mA35A811275420CFDCC8DE9296446043A0E4E3D8A,
	U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__0_m6E2FC1CC2CAF73DAD4FC4FDE38A32C2DADEC4940,
	U3CU3Ec__DisplayClass4_0__ctor_mF58D1E7D665F02C0C31436E1B8B557195F92F6C8,
	U3CU3Ec__DisplayClass4_0_U3CCommitFrameU3Eb__0_mBE9015849553918271F75B2FB578C30D8335D010,
	U3CU3Ec__DisplayClass2_0__ctor_m5BAF5483B5FB578A785BD9A2A27FEC3A87633632,
	U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__0_mEC14B21FE04183E4C93DEFD7F4E19FDBB1D132CF,
	RecordingHandler__ctor_mA135DA539B7C8E44322704009B0939EE773738E7,
	RecordingHandler_Invoke_mE5C8DBD759692DDA10DB1A750964F6149790478F,
	RecordingHandler_BeginInvoke_m435EF59D2CF85240FBA593859A936F9BBCCD38FE,
	RecordingHandler_EndInvoke_m892EC37A4F82A9157D6697B390F5C67554156CE8,
	AudioInputAttachment_OnAudioFilterRead_m9959187AFE0FD16D44D7138A6BD591E4DC5376D5,
	AudioInputAttachment__ctor_mF2B120DA5A6E021B8782F32F13E11D232FD2CED2,
	CameraInputAttachment__ctor_m95357B638EA1498E24AC2BE6B4E6C8DE8361AFF1,
	U3CU3Ec__cctor_m6C327885C8FE0957F1E950FB848531F22C696C47,
	U3CU3Ec__ctor_m460E732670F280C47EE0515A2021FCC55FCC83CE,
	U3CU3Ec_U3C_ctorU3Eb__1_0_m23D9DEE37ED1F9086FB9D9E91DDCA62A5F1EA2F6,
	U3CU3Ec__DisplayClass11_0__ctor_mCB3FD547D1D64D6C77759FE0E310D84C9E09E87D,
	U3CU3Ec__DisplayClass11_0_U3COnFrameU3Eb__0_m82D5F817290BA9A8AB274361686BEB5BAA7E2A72,
	U3COnFrameU3Ed__11__ctor_m4F796CD90BC93408B28AC1424D6532226035950E,
	U3COnFrameU3Ed__11_System_IDisposable_Dispose_m69D1A9CADA3B0D39B2106D158D2E3006797563CD,
	U3COnFrameU3Ed__11_MoveNext_m29EF9278508033762F9720EB763B5C29CF893CDD,
	U3COnFrameU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B7A9D75D96DF5290BB17CAA11897F6D413DE7C1,
	U3COnFrameU3Ed__11_System_Collections_IEnumerator_Reset_mB7D36B1920CC15C2AA7BB6A970D2714A344FE531,
	U3COnFrameU3Ed__11_System_Collections_IEnumerator_get_Current_mFFECDE7919A8BA72064646BA16FA31FC8853C033,
	U3CStopRecordingU3Ed__6_MoveNext_m5421155110B11A174B50BEBC3087691E8D4B89D7,
	U3CStopRecordingU3Ed__6_SetStateMachine_m185FE7121D76EB4B77B8A59D3998E6394D8CA96F,
	U3CU3Ec__cctor_mB03E8B77A501D7DE85C365B316B62805A45AB511,
	U3CU3Ec__ctor_mFF9CF1F5E6FFEF1B8591D4941C2EF862025BC203,
	U3CU3Ec_U3CStartU3Eb__10_0_m81D20AD85D236A2E29B2F0DD775A41F6C224B639,
	U3CStartU3Ed__10__ctor_m58B6D784AB393E6D1057D0AEDFBC21F4B65E551A,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m53052D22FC479EEA5BCC880E69745CDF8DE02012,
	U3CStartU3Ed__10_MoveNext_m1BBA44FA64E0D76BC8D9132E94F7A53FDA8B50D3,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5DEBDDABA8FB1AF52215B5799BCAFD97FA18DF1,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m53EFD74EEF66B0BEC38F35EF5BBB765E9794ABBD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mE85071D76C60B2CAFB401A970DA3B1B9F8940EA5,
	U3CFinishRecordingU3Ed__14_MoveNext_mA296C1BD691818D526C93BE6A5F07910C77AD308,
	U3CFinishRecordingU3Ed__14_SetStateMachine_mFF4B37B838D43EE0416559A82B18DE4CCBCC8942,
	U3CU3Ec__cctor_mAB2EAB9398C8550520109A5C62B1FF8D89D3CC80,
	U3CU3Ec__ctor_mE0FCEC3CB4516ADB8AFB9C98ECA8E4F0EC48911E,
	U3CU3Ec_U3CStartU3Eb__6_0_m20C1F06DF56426374C75CBAC68226F33A9C5AC7A,
	U3CStartU3Ed__6__ctor_m107A1EFF0DF765587AAC9DD86E8243FD411857FA,
	U3CStartU3Ed__6_System_IDisposable_Dispose_mCA59AE66206B88F2E510877FDF15B83741191050,
	U3CStartU3Ed__6_MoveNext_m6AC2C38509C890C977B66341F07C683D833238DF,
	U3CStartU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m76D27D2019DBC9FA5EBF4E7012B8C94C370E0B56,
	U3CStartU3Ed__6_System_Collections_IEnumerator_Reset_mF764C1C0CC9BA12136D27689F492A90BED2B8700,
	U3CStartU3Ed__6_System_Collections_IEnumerator_get_Current_m2AFB27621E83FEF889556E50A912B0446F98A68D,
	U3CCountdownU3Ed__13__ctor_mA67DC266DB1B26E8837001AA9B118598CEB76D59,
	U3CCountdownU3Ed__13_System_IDisposable_Dispose_m1E08AE8FE20EC4B97459F9C144DD76B8CCFED82F,
	U3CCountdownU3Ed__13_MoveNext_mFB2C7D1634718AA24C48470C7703769DBF8A94F7,
	U3CCountdownU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE716D2360AEA565ABBEB8BC1B151B1E18F5B40C0,
	U3CCountdownU3Ed__13_System_Collections_IEnumerator_Reset_mAD55B07B7C84F030D40AA4CA9ED630CB979FCBCD,
	U3CCountdownU3Ed__13_System_Collections_IEnumerator_get_Current_m608D04DAA7E73F18173AE52014FE8E3B011506C4,
	U3CU3Ec__DisplayClass22_0__ctor_m09737A2030CD017312EC64C12F90D25E7F6D889C,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__0_mD9304EE0F587C5B18D9DA6809F5D81BCAB92D43B,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__1_mF26C6F23B1DE3EA3FD39340C393170DBA0B09740,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__21_m7A53268725D5D9AD30AFBA3B94C22E4FE40BE8ED,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__2_mD6EF48C094516FE0626C9DA7EFAB3AEBBB6A2FE7,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__4_m2122D73717CF12BDD39D8D0D7C19611B1F21A2C6,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__5_m749A2A34E2D95A13A8D8E483357FA5D731550CB2,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__6_m0B499A19BE3F839A9EFC34F6077C38D5D275AF32,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__8_m4FA25C019E9365E7E37985DC1E7F7907F379A4F2,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__9_m2455B8C83E029A34C3DBF08ADD33FFFEC7F0D7CC,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__10_m1A7C51B0CC5CE0D268DBB4FDBABB3B3C09B76E8B,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__11_m0C33237140A600D0C563633E550551710271F061,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__13_m705EEBD4D5DBDE19B8FDFC3A819610EE1D2509CF,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__14_m3A7DB9A92B2D6751BD123943544D8C0DFCB7EBA8,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__15_m136FDB1FB84D17F0E6052AC8D1482C41E983C7B7,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__16_m505B89E3C2306856A34C770F72B07D2F36459437,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__17_m6C5F087D0C92313A411D887D78A23FC35674C5AE,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__19_mBF88042ADFBF15D57513B1F7691F9EBB1AD743E4,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__20_m53E8A86C0D0731F935717A27DA325C1CF5BA06A9,
	U3CU3Ec__DisplayClass22_1__ctor_mF03C28B73060772B45422D83C2CCD891461E7F5A,
	U3CU3Ec__DisplayClass22_1_U3CStartU3Eb__23_m0A92AF27DA09C241937F80B5E52AC8A467F592C7,
	U3CU3Ec__DisplayClass22_2__ctor_m0D149B5FF3BAD974378B32F32122517AFE1008F7,
	U3CU3Ec__DisplayClass22_2_U3CStartU3Eb__24_m244CFBB7D843FDE489A08F6438F480F77705B733,
	U3CU3Ec__cctor_mBB9151D0770DA4B6C0D5DE0DF4505B0B1473C792,
	U3CU3Ec__ctor_m7292CCD570F1D648747270AA2E9366DC8BEA76AE,
	U3CU3Ec_U3CStartU3Eb__22_3_mBE510D88050EF0EAE74485B1380103286F1E2ACF,
	U3CU3Ec_U3CStartU3Eb__22_22_mC2B2676DF6376C960A2168BB616C30FAF9572F46,
	U3CU3Ec_U3CStartU3Eb__22_7_m333F34EE7AB7B6BA9AFB42616E6F31AD16648DFA,
	U3CU3Ec_U3CStartU3Eb__22_12_mE9BCA088D81630DCA6B4A29493F717166DE4BDC2,
	U3CU3Ec_U3CStartU3Eb__22_18_m0E1A87851B8F186E1D037D6019BF2B5F03BE4AE7,
	U3CU3Ec_U3CpauseTimeNowU3Eb__26_0_m0067B26B1415DF3C65658EF775E43DE444C9F89E,
	U3CU3Ec__DisplayClass24_0__ctor_m277D86C124C510C86F124C314062F6CA37F04780,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__0_m1C7842AC0922C8535A9D996B445C45BD68ABC7CB,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__1_m3B2F75CA971620227F026AC96214BE0E9F33EE3B,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__2_mA4939E85F8BBD95C7A69CCA149A6F37BC65799FB,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__3_mA4873B5E9E38B22E691207FB286333F85B549ACB,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__4_m14C5D280141ADAF244B2213E9CE61686FC470854,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__5_m25B454BD3DB25DF33E9961063468DC070C809146,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__6_m33BFB7BC2272AE16E35A6AF6271EAAD019DE7D5B,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__7_m9DE96F4CFC41638644635C5699FDA81811802151,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__13_mC7412BB7516D3AAAA92794BF489DD6224FDFA38A,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__14_mE19037EF79A1618B0ABFA5EBB8B7C7167BF59D81,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__15_m5B4E117FD86BFF9E7C043AC7AF54886AFE964EC2,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__16_mD4F03BFC2B675FB03F1475391F156ACFF2D9AEA9,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__8_mABF6F7F053A21322BA9C371F9FFF930148E42C63,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__9_m0832DF1094C24C2B4E50D843130FD5C09B2F33C5,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__10_mDA5269BA5F2FB4EB59DFCCF9E6C806EF781EF838,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__11_m451E7955EC34F408DFAC4365893C10BCCD1F0FFE,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__12_m58715E24DE0A08CD597D94E0B4DABAA49D93F71C,
	U3CtimeBasedTestingU3Ed__24__ctor_mCE839055D4DA089ADAB24FBCCA8FCCB9307C948C,
	U3CtimeBasedTestingU3Ed__24_System_IDisposable_Dispose_m57943CFF04FA4711B6B63A93258509F56768483A,
	U3CtimeBasedTestingU3Ed__24_MoveNext_m40360D1C97BFDAB90C0E02A76CBAE4E3C2EC99B5,
	U3CtimeBasedTestingU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB206B6F0564F23D404E3E0F14B01E7359C2EAA8B,
	U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_Reset_m90D3F19EAF2AE82FB852651B1A13B8EC21FA77D0,
	U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_get_Current_mBD52D1EB4D613ADB298D9E9A684232F65AC7468F,
	U3ClotsOfCancelsU3Ed__25__ctor_mB107D0BCF384F8F0CD3DF49E8E91829D271AB34D,
	U3ClotsOfCancelsU3Ed__25_System_IDisposable_Dispose_mC7CD787978EEAB84A938E833CE818B59FC967926,
	U3ClotsOfCancelsU3Ed__25_MoveNext_m6FB026E807483B7AA2A7CF23F6940E0E9DD8B1FE,
	U3ClotsOfCancelsU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D4392AC16098A343CB609FBF9FA41618CC01B3E,
	U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_Reset_mFF39885CA3EDBEF256FBE637E5C9E4230518A07A,
	U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_get_Current_m94A9DE71BD10C268704724FC2B1E588F75D3C78D,
	U3CpauseTimeNowU3Ed__26__ctor_m5F319CD4B560891783427EB803E77B4A8ACE0FB6,
	U3CpauseTimeNowU3Ed__26_System_IDisposable_Dispose_mAB85DDD605A77343E6464DA39BF37EDEFB254086,
	U3CpauseTimeNowU3Ed__26_MoveNext_m49B3D9FE98B48DE72D1FA96F2C22FFED13EEB9E5,
	U3CpauseTimeNowU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m372428EA5DCAD59137F4E5AFC1C5566DDB6E1625,
	U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_Reset_m3DE5854E0C5EAB304CF501FD2D42B92798B6233D,
	U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_get_Current_m1B325E4F8BE919D78719C70EFC817D07842F3C7C,
	HuffmanListNode__ctor_mFA57D6482AEFA3746176AEB95B78E87DF22E125C,
	U3CU3Ec__cctor_m1A95626A9FBD414AE3D446711A5074CC9AD643B7,
	U3CU3Ec__ctor_mCEB0E994D99999EC9AF502819E66B17112EB3B96,
	U3CU3Ec_U3CBuildLinkedListU3Eb__12_0_m8967CB03485F2CF46E5E3C8BBC15B6374551C7F2,
	HybridMDCT__cctor_mDC4085A5F7822BD8180F5DF1C1E80F6D58C80B26,
	HybridMDCT__ctor_m3E39AB50620725FE1DB8BBA240260FFC230727F5,
	HybridMDCT_Reset_mB26DF29E41EE1290B4F0033298E490CE3866805E,
	HybridMDCT_GetPrevBlock_m599980295E5B4262E6DCA1178F38830E59943222,
	HybridMDCT_Apply_mA5D9889EAB3EDB83671022005B78DFA7A706989B,
	HybridMDCT_LongImpl_m1097ED7B48984B2AE406227F0CC8A886625D373D,
	HybridMDCT_LongIMDCT_m978F17333B0A243907DBF76CFB7C07D5EAC0E607,
	HybridMDCT_ICOS72_A_mFB43BFEA79143E11B686393C93451235154B970F,
	HybridMDCT_ICOS36_A_mDD6AB5213EE2B5B6C2684852156DE544F68A6DB9,
	HybridMDCT_imdct_9pt_mA84BF5D726AD2B37BF3ED0498D79C93BE9844745,
	HybridMDCT_ShortImpl_m43980A3E5459745FAF96F17B1BC2DBC4BA370953,
	HybridMDCT_ShortIMDCT_mF9B502B858BC0C5A8CF34404522F50FA7A3738E9,
	ReadBuffer__ctor_mF3CC4B10337447A5C2101866F9B0415F516FD566,
	ReadBuffer_Read_m001E9B3F35FC00A4BC454A263964AEF64B6EC44C,
	ReadBuffer_ReadByte_m9261A9104B001B17E4FE9CA60D1CB3285C85A3C3,
	ReadBuffer_EnsureFilled_mAF006D4F770E9B261258FAE5BC8799C19B32E483,
	ReadBuffer_DiscardThrough_mEBA73731401C395EBD4112CAF531706EB16A4D5C,
	ReadBuffer_Truncate_mC6F9C14C6AEF88AD07FBDE86612EBCDA716A7C64,
	ReadBuffer_CommitDiscard_m8976CC33D6D7DDC1AB84FC3DA2593942EAF44C84,
};
extern void U3CStopRecordingU3Ed__6_MoveNext_m5421155110B11A174B50BEBC3087691E8D4B89D7_AdjustorThunk (void);
extern void U3CStopRecordingU3Ed__6_SetStateMachine_m185FE7121D76EB4B77B8A59D3998E6394D8CA96F_AdjustorThunk (void);
extern void U3CFinishRecordingU3Ed__14_MoveNext_mA296C1BD691818D526C93BE6A5F07910C77AD308_AdjustorThunk (void);
extern void U3CFinishRecordingU3Ed__14_SetStateMachine_mFF4B37B838D43EE0416559A82B18DE4CCBCC8942_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[4] = 
{
	{ 0x0600072E, U3CStopRecordingU3Ed__6_MoveNext_m5421155110B11A174B50BEBC3087691E8D4B89D7_AdjustorThunk },
	{ 0x0600072F, U3CStopRecordingU3Ed__6_SetStateMachine_m185FE7121D76EB4B77B8A59D3998E6394D8CA96F_AdjustorThunk },
	{ 0x06000739, U3CFinishRecordingU3Ed__14_MoveNext_mA296C1BD691818D526C93BE6A5F07910C77AD308_AdjustorThunk },
	{ 0x0600073A, U3CFinishRecordingU3Ed__14_SetStateMachine_mFF4B37B838D43EE0416559A82B18DE4CCBCC8942_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1955] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	154,
	26,
	1570,
	26,
	26,
	28,
	23,
	23,
	23,
	1703,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	334,
	23,
	23,
	23,
	23,
	23,
	2781,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	1570,
	23,
	23,
	23,
	23,
	23,
	1703,
	1540,
	1570,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	484,
	23,
	23,
	23,
	23,
	334,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1540,
	1540,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	334,
	26,
	23,
	23,
	2782,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1540,
	1540,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	334,
	26,
	23,
	23,
	2782,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1539,
	1540,
	1539,
	1540,
	14,
	26,
	14,
	26,
	14,
	14,
	23,
	28,
	10,
	10,
	14,
	26,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	2355,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	28,
	23,
	23,
	14,
	89,
	23,
	2783,
	2784,
	2785,
	2786,
	1688,
	2787,
	2787,
	2784,
	2784,
	2787,
	2788,
	14,
	14,
	2355,
	1207,
	34,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1539,
	1207,
	1207,
	1207,
	28,
	2355,
	28,
	2355,
	1207,
	2355,
	123,
	190,
	1207,
	1207,
	1207,
	34,
	34,
	123,
	123,
	123,
	123,
	34,
	14,
	14,
	34,
	14,
	34,
	28,
	28,
	105,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	105,
	105,
	105,
	105,
	28,
	123,
	123,
	28,
	2789,
	28,
	2355,
	123,
	28,
	123,
	123,
	28,
	28,
	1207,
	28,
	1207,
	123,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	1539,
	1540,
	1539,
	1540,
	726,
	334,
	1704,
	1705,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	1523,
	23,
	10,
	23,
	129,
	14,
	726,
	1207,
	28,
	105,
	105,
	205,
	28,
	28,
	1207,
	2791,
	14,
	23,
	26,
	26,
	32,
	4,
	2,
	2,
	178,
	132,
	164,
	119,
	2792,
	0,
	2793,
	2794,
	2793,
	2795,
	23,
	3,
	23,
	34,
	28,
	14,
	14,
	14,
	14,
	34,
	1207,
	1207,
	1655,
	2796,
	2797,
	2798,
	2799,
	2800,
	2801,
	2802,
	1617,
	2803,
	2804,
	2805,
	23,
	23,
	14,
	23,
	2806,
	2807,
	43,
	1112,
	1,
	0,
	1,
	3,
	23,
	3,
	3,
	106,
	106,
	106,
	164,
	165,
	3,
	23,
	1726,
	164,
	3,
	165,
	164,
	2793,
	445,
	3,
	849,
	154,
	619,
	154,
	1125,
	372,
	164,
	899,
	43,
	43,
	0,
	372,
	164,
	154,
	3,
	3,
	372,
	164,
	154,
	114,
	114,
	46,
	114,
	114,
	46,
	114,
	2808,
	0,
	0,
	4,
	4,
	2809,
	1,
	208,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2811,
	2811,
	2811,
	2812,
	2812,
	613,
	613,
	2792,
	2794,
	2813,
	2814,
	2814,
	2814,
	2814,
	2814,
	2814,
	2813,
	2813,
	2810,
	2810,
	2810,
	2794,
	2814,
	2810,
	2810,
	2810,
	2814,
	2814,
	2814,
	2794,
	2810,
	2794,
	2810,
	2810,
	2810,
	2815,
	2815,
	2794,
	2813,
	2810,
	2810,
	2810,
	2816,
	2817,
	2818,
	2819,
	2820,
	2821,
	2821,
	2822,
	2822,
	2823,
	2824,
	2821,
	2794,
	2825,
	2794,
	2810,
	2810,
	2810,
	2810,
	2794,
	2815,
	2815,
	2794,
	2813,
	2810,
	2811,
	2826,
	2827,
	1653,
	1653,
	1653,
	1632,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	2801,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	1653,
	2801,
	2801,
	2801,
	2828,
	2828,
	2828,
	2829,
	2830,
	2831,
	2832,
	584,
	498,
	1417,
	46,
	230,
	164,
	584,
	23,
	3,
	0,
	23,
	2833,
	1526,
	1546,
	1546,
	23,
	26,
	26,
	726,
	1546,
	930,
	930,
	930,
	2834,
	930,
	2834,
	334,
	2835,
	26,
	459,
	459,
	1546,
	1546,
	2836,
	1546,
	930,
	930,
	930,
	2834,
	930,
	2834,
	334,
	1570,
	2787,
	2837,
	2838,
	14,
	3,
	23,
	2123,
	1524,
	2287,
	2839,
	89,
	10,
	129,
	23,
	23,
	726,
	334,
	726,
	334,
	726,
	334,
	726,
	334,
	1528,
	2123,
	28,
	123,
	2783,
	1207,
	28,
	2840,
	123,
	123,
	14,
	62,
	3,
	3,
	3,
	164,
	2841,
	164,
	164,
	2842,
	559,
	2842,
	559,
	119,
	2843,
	2841,
	2844,
	1663,
	23,
	3,
	2810,
	2810,
	2810,
	2810,
	2810,
	154,
	619,
	1125,
	154,
	2811,
	2811,
	613,
	613,
	114,
	114,
	114,
	2794,
	2794,
	2794,
	2813,
	2813,
	2814,
	2814,
	2814,
	2814,
	2814,
	2814,
	2794,
	2814,
	2814,
	2794,
	2814,
	2814,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2814,
	2814,
	2814,
	2814,
	2814,
	2814,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	154,
	1,
	154,
	2794,
	2794,
	2794,
	2815,
	2815,
	2815,
	2815,
	2815,
	2815,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2794,
	2794,
	2794,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2813,
	2820,
	2816,
	2818,
	2819,
	2821,
	2821,
	2821,
	2822,
	2823,
	2824,
	1688,
	1688,
	1688,
	1688,
	1688,
	1688,
	2845,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	26,
	23,
	136,
	23,
	0,
	-1,
	40,
	26,
	23,
	154,
	106,
	4,
	154,
	26,
	26,
	26,
	26,
	3,
	23,
	26,
	26,
	26,
	26,
	23,
	32,
	31,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	2846,
	23,
	23,
	23,
	31,
	23,
	26,
	31,
	2847,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	31,
	23,
	23,
	32,
	23,
	23,
	26,
	32,
	23,
	26,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	2848,
	2848,
	23,
	23,
	23,
	334,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	23,
	27,
	2849,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	2848,
	2848,
	34,
	2848,
	23,
	23,
	26,
	23,
	459,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	459,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	0,
	23,
	23,
	459,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	0,
	0,
	4,
	0,
	0,
	154,
	94,
	0,
	0,
	3,
	26,
	23,
	2850,
	952,
	23,
	23,
	23,
	23,
	23,
	23,
	130,
	34,
	62,
	23,
	28,
	23,
	37,
	37,
	34,
	37,
	34,
	34,
	37,
	34,
	34,
	34,
	34,
	34,
	37,
	112,
	34,
	509,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	58,
	105,
	58,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	2852,
	23,
	14,
	23,
	89,
	89,
	23,
	26,
	26,
	26,
	617,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	26,
	14,
	23,
	1401,
	23,
	14,
	23,
	2,
	1,
	1,
	14,
	26,
	14,
	26,
	23,
	23,
	842,
	23,
	88,
	23,
	23,
	23,
	23,
	3,
	2853,
	2069,
	-1,
	2854,
	171,
	14,
	2853,
	2856,
	-1,
	2854,
	171,
	14,
	2853,
	-1,
	2854,
	171,
	14,
	2853,
	129,
	-1,
	2854,
	171,
	14,
	28,
	2853,
	2856,
	-1,
	2854,
	171,
	14,
	2853,
	129,
	-1,
	2854,
	171,
	14,
	2857,
	2857,
	2858,
	1562,
	2859,
	2860,
	25,
	2853,
	26,
	-1,
	2854,
	171,
	14,
	2863,
	0,
	197,
	946,
	23,
	946,
	26,
	197,
	23,
	14,
	463,
	335,
	172,
	133,
	2865,
	23,
	172,
	172,
	89,
	31,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	23,
	89,
	23,
	23,
	26,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	14,
	14,
	14,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	89,
	89,
	89,
	23,
	37,
	26,
	26,
	459,
	23,
	10,
	10,
	89,
	172,
	310,
	172,
	200,
	310,
	948,
	26,
	10,
	32,
	510,
	510,
	510,
	23,
	26,
	10,
	32,
	770,
	770,
	770,
	23,
	94,
	141,
	37,
	10,
	2866,
	10,
	172,
	32,
	32,
	23,
	23,
	23,
	106,
	172,
	200,
	10,
	32,
	23,
	2867,
	1152,
	1091,
	37,
	10,
	23,
	23,
	23,
	3,
	3,
	2868,
	2869,
	134,
	2870,
	689,
	764,
	976,
	23,
	43,
	23,
	10,
	23,
	32,
	23,
	23,
	10,
	26,
	23,
	1180,
	26,
	10,
	32,
	23,
	62,
	1145,
	118,
	27,
	27,
	118,
	27,
	3,
	218,
	23,
	28,
	118,
	3,
	218,
	0,
	23,
	28,
	118,
	3,
	2871,
	130,
	1180,
	26,
	28,
	27,
	118,
	26,
	26,
	41,
	3,
	218,
	23,
	1180,
	23,
	26,
	26,
	56,
	57,
	38,
	2872,
	39,
	38,
	338,
	129,
	129,
	459,
	459,
	26,
	1074,
	3,
	43,
	23,
	10,
	10,
	89,
	2874,
	14,
	34,
	14,
	10,
	10,
	10,
	89,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	89,
	89,
	10,
	89,
	10,
	172,
	200,
	23,
	37,
	3,
	26,
	14,
	2875,
	435,
	1090,
	23,
	89,
	172,
	10,
	10,
	10,
	963,
	14,
	14,
	43,
	23,
	10,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	172,
	10,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	334,
	23,
	23,
	23,
	1703,
	3,
	23,
	23,
	334,
	23,
	334,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	334,
	23,
	334,
	3,
	23,
	334,
	23,
	334,
	124,
	23,
	105,
	26,
	3,
	23,
	334,
	124,
	23,
	105,
	26,
	3,
	23,
	334,
	124,
	1539,
	105,
	2790,
	124,
	23,
	105,
	26,
	3,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	124,
	26,
	205,
	26,
	124,
	26,
	205,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	62,
	23,
	129,
	2851,
	107,
	1457,
	3,
	23,
	112,
	112,
	27,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	2855,
	23,
	2855,
	23,
	23,
	23,
	2855,
	124,
	2861,
	2862,
	26,
	130,
	23,
	23,
	3,
	23,
	41,
	23,
	2864,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	3,
	23,
	89,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	3,
	23,
	89,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	1703,
	23,
	23,
	23,
	23,
	26,
	3,
	23,
	23,
	23,
	23,
	334,
	23,
	23,
	23,
	23,
	23,
	334,
	23,
	23,
	23,
	334,
	23,
	23,
	1540,
	26,
	23,
	23,
	334,
	23,
	1540,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	3,
	23,
	41,
	3,
	23,
	23,
	1145,
	2873,
	60,
	137,
	269,
	269,
	137,
	107,
	498,
	32,
	2876,
	2877,
	2878,
	200,
	23,
	23,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[1] = 
{
	{ 0x06000532, 10,  (void**)&NativeRecorder_OnRecording_m6E2D200EE07CE507951155CB24CC8107AFE8D13B_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[5] = 
{
	{ 0x02000002, { 0, 21 } },
	{ 0x060003D4, { 21, 1 } },
	{ 0x06000503, { 22, 1 } },
	{ 0x06000509, { 23, 1 } },
	{ 0x0600051B, { 24, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[25] = 
{
	{ (Il2CppRGCTXDataType)2, 33230 },
	{ (Il2CppRGCTXDataType)3, 21096 },
	{ (Il2CppRGCTXDataType)2, 33231 },
	{ (Il2CppRGCTXDataType)3, 21097 },
	{ (Il2CppRGCTXDataType)3, 21098 },
	{ (Il2CppRGCTXDataType)2, 33232 },
	{ (Il2CppRGCTXDataType)3, 21099 },
	{ (Il2CppRGCTXDataType)3, 21100 },
	{ (Il2CppRGCTXDataType)2, 33233 },
	{ (Il2CppRGCTXDataType)3, 21101 },
	{ (Il2CppRGCTXDataType)3, 21102 },
	{ (Il2CppRGCTXDataType)2, 33234 },
	{ (Il2CppRGCTXDataType)3, 21103 },
	{ (Il2CppRGCTXDataType)3, 21104 },
	{ (Il2CppRGCTXDataType)3, 21105 },
	{ (Il2CppRGCTXDataType)3, 21106 },
	{ (Il2CppRGCTXDataType)3, 21107 },
	{ (Il2CppRGCTXDataType)2, 31936 },
	{ (Il2CppRGCTXDataType)2, 31937 },
	{ (Il2CppRGCTXDataType)2, 31938 },
	{ (Il2CppRGCTXDataType)2, 31939 },
	{ (Il2CppRGCTXDataType)3, 21108 },
	{ (Il2CppRGCTXDataType)3, 21109 },
	{ (Il2CppRGCTXDataType)3, 21110 },
	{ (Il2CppRGCTXDataType)3, 21111 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	1955,
	s_methodPointers,
	4,
	s_adjustorThunks,
	s_InvokerIndices,
	1,
	s_reversePInvokeIndices,
	5,
	s_rgctxIndices,
	25,
	s_rgctxValues,
	NULL,
};
