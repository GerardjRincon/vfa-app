﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.AssetBundle::.ctor()
extern void AssetBundle__ctor_mF8A0A863455A7DF958A966C86BE58E07535CD3A4 (void);
// 0x00000002 System.Void UnityEngine.AssetBundle::ValidateLoadFromStream(System.IO.Stream)
extern void AssetBundle_ValidateLoadFromStream_m7C9CA2596D1A8A9A199BF9566160F10FEA588005 (void);
// 0x00000003 UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromStream(System.IO.Stream)
extern void AssetBundle_LoadFromStream_m7B76086277FD4E00A6E9297E9095EBF5CF126BBD (void);
// 0x00000004 UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromStreamInternal(System.IO.Stream,System.UInt32,System.UInt32)
extern void AssetBundle_LoadFromStreamInternal_m77C1FCA22FBB3F02A7FDC2D48103FEB5F2D1A478 (void);
// 0x00000005 T[] UnityEngine.AssetBundle::ConvertObjects(UnityEngine.Object[])
// 0x00000006 T[] UnityEngine.AssetBundle::LoadAllAssets()
// 0x00000007 UnityEngine.Object[] UnityEngine.AssetBundle::LoadAllAssets(System.Type)
extern void AssetBundle_LoadAllAssets_m1F7E39416EBCC192881058CBDCA490A8019E13EA (void);
// 0x00000008 System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern void AssetBundle_Unload_m5B7DE41516A059545F8C493EC459EA0A9166EBB3 (void);
// 0x00000009 UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)
extern void AssetBundle_LoadAssetWithSubAssets_Internal_m576FBCE3D60FC5E7E4462E6EF55DF8E5F49072B4 (void);
static Il2CppMethodPointer s_methodPointers[9] = 
{
	AssetBundle__ctor_mF8A0A863455A7DF958A966C86BE58E07535CD3A4,
	AssetBundle_ValidateLoadFromStream_m7C9CA2596D1A8A9A199BF9566160F10FEA588005,
	AssetBundle_LoadFromStream_m7B76086277FD4E00A6E9297E9095EBF5CF126BBD,
	AssetBundle_LoadFromStreamInternal_m77C1FCA22FBB3F02A7FDC2D48103FEB5F2D1A478,
	NULL,
	NULL,
	AssetBundle_LoadAllAssets_m1F7E39416EBCC192881058CBDCA490A8019E13EA,
	AssetBundle_Unload_m5B7DE41516A059545F8C493EC459EA0A9166EBB3,
	AssetBundle_LoadAssetWithSubAssets_Internal_m576FBCE3D60FC5E7E4462E6EF55DF8E5F49072B4,
};
static const int32_t s_InvokerIndices[9] = 
{
	23,
	154,
	0,
	193,
	-1,
	-1,
	28,
	31,
	105,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000005, { 0, 2 } },
	{ 0x06000006, { 2, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)2, 31693 },
	{ (Il2CppRGCTXDataType)2, 31694 },
	{ (Il2CppRGCTXDataType)1, 31696 },
	{ (Il2CppRGCTXDataType)3, 21095 },
};
extern const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule = 
{
	"UnityEngine.AssetBundleModule.dll",
	9,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
};
