﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* ARKitSessionSubsystem_OnAsyncConversionComplete_mF9F38F09B38CC00967BD472D09D8584299F1B04F_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.XR.ARKit.Api::UnityARKit_disposeWorldMap(System.Int32)
extern void Api_UnityARKit_disposeWorldMap_mDB310DE6F820E095FB2B2D6ECD586FB7617026B9 (void);
// 0x00000002 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem::Register()
extern void ARKitCameraSubsystem_Register_m0B242BF988E92E626979B845EE749A1794E9E642 (void);
// 0x00000003 UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider UnityEngine.XR.ARKit.ARKitCameraSubsystem::CreateProvider()
extern void ARKitCameraSubsystem_CreateProvider_mD59CEAEDE9A4351E3B6E069004B813F882FB2987 (void);
// 0x00000004 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem::.ctor()
extern void ARKitCameraSubsystem__ctor_mE0B24FB4ED5FC603CB22E220A161A00177B9B8B9 (void);
// 0x00000005 System.String UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::get_shaderName()
extern void Provider_get_shaderName_m7DAA2314A55B8E6B5BAAB3FDE0926DCCD77F11EA (void);
// 0x00000006 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::get_permissionGranted()
extern void Provider_get_permissionGranted_m02DB9BF826B2D20EBA4C6B7644FDB6AB9642C2A9 (void);
// 0x00000007 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::.ctor()
extern void Provider__ctor_m94418749731B416D24DE7243E47BAEFC002D646E (void);
// 0x00000008 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::Start()
extern void Provider_Start_m2946B41F238958FB44AD3B16DBD26844F61BC7D7 (void);
// 0x00000009 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::Stop()
extern void Provider_Stop_mAEA642A3F7A598594580E3D5687239F74C7E1761 (void);
// 0x0000000A System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::Destroy()
extern void Provider_Destroy_m2CC7D21196FDF5A5B0196595C7AB5E2AC2565554 (void);
// 0x0000000B System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::TryGetFrame(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraFrame&)
extern void Provider_TryGetFrame_m778BED16F19EC494A6514EEF672A8482880C7BE6 (void);
// 0x0000000C System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::TrySetFocusMode(UnityEngine.XR.ARSubsystems.CameraFocusMode)
extern void Provider_TrySetFocusMode_m681B30E6232C7B9FA5DD26925FDCCD9A04631FF7 (void);
// 0x0000000D System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::TrySetLightEstimationMode(UnityEngine.XR.ARSubsystems.LightEstimationMode)
extern void Provider_TrySetLightEstimationMode_m99AE9CF47F47FEAB5D2FF28AE89DA3AE5AD2BAA6 (void);
// 0x0000000E System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::TryGetIntrinsics(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics&)
extern void Provider_TryGetIntrinsics_m8242924A73B0EB1AED11A5D500C070E41C14E79B (void);
// 0x0000000F Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::GetConfigurations(UnityEngine.XR.ARSubsystems.XRCameraConfiguration,Unity.Collections.Allocator)
extern void Provider_GetConfigurations_m4D9FC2BA19A3649FD8A38FB6125B77916FD4A71B (void);
// 0x00000010 System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::get_currentConfiguration()
extern void Provider_get_currentConfiguration_mA0CFBA086EFFF0ED6435E6AB7084329F8DAB02D3 (void);
// 0x00000011 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::set_currentConfiguration(System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration>)
extern void Provider_set_currentConfiguration_m9E1BDFAEEA1A00221B3DC967E772F12204458644 (void);
// 0x00000012 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTextureDescriptor> UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::GetTextureDescriptors(UnityEngine.XR.ARSubsystems.XRTextureDescriptor,Unity.Collections.Allocator)
extern void Provider_GetTextureDescriptors_m9CECC57BD95307B36CC3F27C9CFBBACC92836DCC (void);
// 0x00000013 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::TryAcquireLatestImage(UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImageCinfo&)
extern void Provider_TryAcquireLatestImage_mD6A0021BD259DC4599D9CFE86BE154B9AAB8D157 (void);
// 0x00000014 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::DisposeImage(System.Int32)
extern void Provider_DisposeImage_m937B0669326BFF5390E7F553FE0409EF28F0CAE4 (void);
// 0x00000015 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem/Provider::.cctor()
extern void Provider__cctor_m38524FA8A6011F15F82FE1A2500B22425FC52099 (void);
// 0x00000016 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_Construct(System.Int32,System.Int32)
extern void NativeApi_UnityARKit_Camera_Construct_m2DF0AEE8AA88CDF96D248F9CD714961C2738216F (void);
// 0x00000017 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_Destruct()
extern void NativeApi_UnityARKit_Camera_Destruct_m795FFFF7821CE91E8992070BEFAAE0FFBA6AE084 (void);
// 0x00000018 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_Start()
extern void NativeApi_UnityARKit_Camera_Start_m59DA518F90E02C5707FDD049DC3DECD96B65A5A6 (void);
// 0x00000019 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_Stop()
extern void NativeApi_UnityARKit_Camera_Stop_m098821B53BD6527CA00BB02E51D36A7A5C76FF10 (void);
// 0x0000001A System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_TryGetFrame(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraFrame&)
extern void NativeApi_UnityARKit_Camera_TryGetFrame_m6048991661BFB4AEE17C00EDCF7E65995B7C8691 (void);
// 0x0000001B System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_TrySetFocusMode(UnityEngine.XR.ARSubsystems.CameraFocusMode)
extern void NativeApi_UnityARKit_Camera_TrySetFocusMode_m2D71E0C7D2E3A86263BC42FF937BEAC0FFBD8677 (void);
// 0x0000001C System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_TrySetLightEstimationMode(UnityEngine.XR.ARSubsystems.LightEstimationMode)
extern void NativeApi_UnityARKit_Camera_TrySetLightEstimationMode_m2DAF2550DA728382FB0F1B91B02B74FC50D4257E (void);
// 0x0000001D System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_TryGetIntrinsics(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics&)
extern void NativeApi_UnityARKit_Camera_TryGetIntrinsics_m364B7B8DCA914DD0C1878F56859A5C7D9CC3B045 (void);
// 0x0000001E System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_IsCameraPermissionGranted()
extern void NativeApi_UnityARKit_Camera_IsCameraPermissionGranted_m972DB9591AA4E5EB4F483280F44DEF5CC8C88EE3 (void);
// 0x0000001F System.IntPtr UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_AcquireConfigurations(System.Int32&,System.Int32&)
extern void NativeApi_UnityARKit_Camera_AcquireConfigurations_m6A384C73CC0BE9DA50DC6128284E200678D08771 (void);
// 0x00000020 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_ReleaseConfigurations(System.IntPtr)
extern void NativeApi_UnityARKit_Camera_ReleaseConfigurations_mECAE479F28DB8847031DB58670D463D8692C661D (void);
// 0x00000021 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_TryGetCurrentConfiguration(UnityEngine.XR.ARSubsystems.XRCameraConfiguration&)
extern void NativeApi_UnityARKit_Camera_TryGetCurrentConfiguration_m2B5868976AF970142BB6CDBCBB04329C98FBAA88 (void);
// 0x00000022 UnityEngine.XR.ARKit.ARKitCameraSubsystem/CameraConfigurationResult UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_TrySetCurrentConfiguration(UnityEngine.XR.ARSubsystems.XRCameraConfiguration)
extern void NativeApi_UnityARKit_Camera_TrySetCurrentConfiguration_m10852C9A985C98A1BF729D17027BE7B96C9FB673 (void);
// 0x00000023 System.Void* UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_AcquireTextureDescriptors(System.Int32&,System.Int32&)
extern void NativeApi_UnityARKit_Camera_AcquireTextureDescriptors_mEAFD58446AEBEC88F2E3E55B5C8C3C89B2BA0869 (void);
// 0x00000024 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_ReleaseTextureDescriptors(System.Void*)
extern void NativeApi_UnityARKit_Camera_ReleaseTextureDescriptors_m1B1D58FE2C83A8854CD2223602213D302585A0A2 (void);
// 0x00000025 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_TryAcquireLatestImage(UnityEngine.XR.ARSubsystems.XRCameraSubsystem/CameraImageCinfo&)
extern void NativeApi_UnityARKit_Camera_TryAcquireLatestImage_m9EDEF6EF0E4C88B918827C8DFCA3AF3265C04DEE (void);
// 0x00000026 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi::UnityARKit_Camera_DisposeImage(System.Int32)
extern void NativeApi_UnityARKit_Camera_DisposeImage_m696CD790C79BA59B3B69032D9E331D33F0D8506E (void);
// 0x00000027 System.Void UnityEngine.XR.ARKit.ARKitEnvironmentProbeRegistration::Register()
extern void ARKitEnvironmentProbeRegistration_Register_m99E02E59582A0F880422D128FEEF2BEE553EE20E (void);
// 0x00000028 UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/IProvider UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem::CreateProvider()
extern void ARKitEnvironmentProbeSubsystem_CreateProvider_m734B041E08091395C665D21150F3728493C2A746 (void);
// 0x00000029 System.Void UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem::.ctor()
extern void ARKitEnvironmentProbeSubsystem__ctor_m957799C8DE48100EF21E8429209271292EADFA95 (void);
// 0x0000002A System.Void UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem/Provider::.ctor()
extern void Provider__ctor_m01089AA1109114DA6CA9F68C2B691726E7E75745 (void);
// 0x0000002B System.Void UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem/Provider::Start()
extern void Provider_Start_m00CEAF4D906E61A9A4F729F73AE8766A2ED7D7E5 (void);
// 0x0000002C System.Void UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem/Provider::Stop()
extern void Provider_Stop_mC2481922A4D2761BB6057BA06C255CF80EFBD862 (void);
// 0x0000002D System.Void UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem/Provider::Destroy()
extern void Provider_Destroy_mC7E6A4679A9D3565D0AB9F588BF6009D95D64281 (void);
// 0x0000002E System.Void UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem/Provider::SetAutomaticPlacement(System.Boolean)
extern void Provider_SetAutomaticPlacement_m8B87B886E472B8BA1A07C1C69FD95D8295468BAD (void);
// 0x0000002F System.Boolean UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem/Provider::TryAddEnvironmentProbe(UnityEngine.Pose,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.XR.ARSubsystems.XREnvironmentProbe&)
extern void Provider_TryAddEnvironmentProbe_mC080470C1555B1CCC7F29D4DD842650059BD765A (void);
// 0x00000030 System.Boolean UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem/Provider::RemoveEnvironmentProbe(UnityEngine.XR.ARSubsystems.TrackableId)
extern void Provider_RemoveEnvironmentProbe_m93134B4724464908E4BB6EA7BB62A296352D47C3 (void);
// 0x00000031 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XREnvironmentProbe> UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem/Provider::GetChanges(UnityEngine.XR.ARSubsystems.XREnvironmentProbe,Unity.Collections.Allocator)
extern void Provider_GetChanges_m26E082A5D0B91535BD7201C3AFAFA220CC61F006 (void);
// 0x00000032 UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/IProvider UnityEngine.XR.ARKit.ARKitRaycastSubsystem::CreateProvider()
extern void ARKitRaycastSubsystem_CreateProvider_m5047B990F4841868A7014A87971249FE94C867B3 (void);
// 0x00000033 System.Void UnityEngine.XR.ARKit.ARKitRaycastSubsystem::RegisterDescriptor()
extern void ARKitRaycastSubsystem_RegisterDescriptor_m1442393B3D2D07C4E6F1FBD355695BCEB4DCF63E (void);
// 0x00000034 System.Void UnityEngine.XR.ARKit.ARKitRaycastSubsystem::.ctor()
extern void ARKitRaycastSubsystem__ctor_m8FE9A2A756806FD0166A60C2347D88F0BB38307F (void);
// 0x00000035 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARKit.ARKitRaycastSubsystem/Provider::Raycast(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void Provider_Raycast_m1B485E19859E6EB636FB7E7934E316820C5EEEB6 (void);
// 0x00000036 System.Void UnityEngine.XR.ARKit.ARKitRaycastSubsystem/Provider::.ctor()
extern void Provider__ctor_mFAB8D17BFE8D59172A9949DE1F83981251CBBB88 (void);
// 0x00000037 System.Void UnityEngine.XR.ARKit.ARKitRaycastSubsystem/NativeApi::UnityARKit_raycast_acquireHitResults(UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,System.Void*&,System.Int32&)
extern void NativeApi_UnityARKit_raycast_acquireHitResults_mA4AC6C358549A8DC05F394AFD56ED90CD03E9C9F (void);
// 0x00000038 System.Void UnityEngine.XR.ARKit.ARKitRaycastSubsystem/NativeApi::UnityARKit_raycast_copyAndReleaseHitResults(System.Void*,System.Int32,System.Void*,System.Void*)
extern void NativeApi_UnityARKit_raycast_copyAndReleaseHitResults_m1080A044038B5CF33AAB68D1DD8C4018E1B2D12A (void);
// 0x00000039 UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem/IProvider UnityEngine.XR.ARKit.ARKitReferencePointSubsystem::CreateProvider()
extern void ARKitReferencePointSubsystem_CreateProvider_m5D766C2E862F8A4ECA5772FDA6BF63651C129A09 (void);
// 0x0000003A System.Void UnityEngine.XR.ARKit.ARKitReferencePointSubsystem::RegisterDescriptor()
extern void ARKitReferencePointSubsystem_RegisterDescriptor_mE3DCA20DAE72D2C7CF15B150584606C8D0FB8BA4 (void);
// 0x0000003B System.Void UnityEngine.XR.ARKit.ARKitReferencePointSubsystem::.ctor()
extern void ARKitReferencePointSubsystem__ctor_m5BA45BB0ED79227C4E4ECE86D0EFA99A2ABEE06D (void);
// 0x0000003C System.Void UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::Start()
extern void Provider_Start_mE6069260E7307357558BA42120FF26C71DF79D0D (void);
// 0x0000003D System.Void UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::Stop()
extern void Provider_Stop_m064C737A6B63DD370751437D4154DF9ADF3251B9 (void);
// 0x0000003E System.Void UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::Destroy()
extern void Provider_Destroy_mAB4C5670DDFEDB0575821B397792727B85AA11F7 (void);
// 0x0000003F UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint> UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRReferencePoint,Unity.Collections.Allocator)
extern void Provider_GetChanges_m6ACC32D7B8FF8B2AE977BEE37D81AB5D7141B7F1 (void);
// 0x00000040 System.Boolean UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::TryAddReferencePoint(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRReferencePoint&)
extern void Provider_TryAddReferencePoint_m99AEE284D5BBF3AFD568CD3428C3C50D8441EEF0 (void);
// 0x00000041 System.Boolean UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::TryAttachReferencePoint(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRReferencePoint&)
extern void Provider_TryAttachReferencePoint_m8928234A81EDE93A7C83AE2BDA10940D5EB1A3C8 (void);
// 0x00000042 System.Boolean UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::TryRemoveReferencePoint(UnityEngine.XR.ARSubsystems.TrackableId)
extern void Provider_TryRemoveReferencePoint_m1C398858AF6AFD9A6F3822F2E02941D17F4BC7EC (void);
// 0x00000043 System.Void UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::UnityARKit_refPoints_onStart()
extern void Provider_UnityARKit_refPoints_onStart_m7F72101E953D4FA29B77F9B925CD097271D8417E (void);
// 0x00000044 System.Void UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::UnityARKit_refPoints_onStop()
extern void Provider_UnityARKit_refPoints_onStop_m7C09A2E2921F4C8B7460BC4B3B2FC139092E0865 (void);
// 0x00000045 System.Void UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::UnityARKit_refPoints_onDestroy()
extern void Provider_UnityARKit_refPoints_onDestroy_m1D71BC7DBA28E50A83D322C9B735554D47EC67D1 (void);
// 0x00000046 System.Void* UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::UnityARKit_refPoints_acquireChanges(System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Int32&)
extern void Provider_UnityARKit_refPoints_acquireChanges_mA344595E5CD7FE92F42133E6B0E854E124B837EB (void);
// 0x00000047 System.Void UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::UnityARKit_refPoints_releaseChanges(System.Void*)
extern void Provider_UnityARKit_refPoints_releaseChanges_mA2279618DB35A58E4AA4CAD3D51B25BEF9EB4D59 (void);
// 0x00000048 System.Boolean UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::UnityARKit_refPoints_tryAdd(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRReferencePoint&)
extern void Provider_UnityARKit_refPoints_tryAdd_m2F8B38BB56978FFD60602045422B4C0639B7B004 (void);
// 0x00000049 System.Boolean UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::UnityARKit_refPoints_tryAttach(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRReferencePoint&)
extern void Provider_UnityARKit_refPoints_tryAttach_m774D4727945B1B3E4B2E212913D1482147A923AE (void);
// 0x0000004A System.Boolean UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::UnityARKit_refPoints_tryRemove(UnityEngine.XR.ARSubsystems.TrackableId)
extern void Provider_UnityARKit_refPoints_tryRemove_m3A0BEB288BC29179F673735239E3FEFE65A61906 (void);
// 0x0000004B System.Void UnityEngine.XR.ARKit.ARKitReferencePointSubsystem/Provider::.ctor()
extern void Provider__ctor_m7DAC59A12BE6120D2C72B16E02A503C014166901 (void);
// 0x0000004C UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider UnityEngine.XR.ARKit.ARKitSessionSubsystem::CreateProvider()
extern void ARKitSessionSubsystem_CreateProvider_m0519B1E5B244B918EC2E29AA8D46D09696BBF64B (void);
// 0x0000004D System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::.cctor()
extern void ARKitSessionSubsystem__cctor_mF21044D0935B2CA8F1FCD42BF839812255101E2A (void);
// 0x0000004E System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::OnAsyncConversionComplete(UnityEngine.XR.ARKit.ARWorldMapRequestStatus,System.Int32,System.IntPtr)
extern void ARKitSessionSubsystem_OnAsyncConversionComplete_mF9F38F09B38CC00967BD472D09D8584299F1B04F (void);
// 0x0000004F System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::RegisterDescriptor()
extern void ARKitSessionSubsystem_RegisterDescriptor_m359F3EC534DDADAE498376C22BE8BB24813B1CC9 (void);
// 0x00000050 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::.ctor()
extern void ARKitSessionSubsystem__ctor_m551ABE75E099FDA75C75967077DEFE540A41DC19 (void);
// 0x00000051 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::.ctor()
extern void Provider__ctor_mB6A986F9599E6428D3925C19C0A5937C9F79D7B5 (void);
// 0x00000052 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::Resume()
extern void Provider_Resume_m18F254F006F15D8F65D947C21C1C1E97F40C5512 (void);
// 0x00000053 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::Pause()
extern void Provider_Pause_mA9F691CC624412FF6E86F51A8E74514F4A9B7526 (void);
// 0x00000054 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::Update(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void Provider_Update_m50F7B6BA08687FAD6DDBF21FD5678171C1D69ABA (void);
// 0x00000055 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::Destroy()
extern void Provider_Destroy_mF3FD2EA69B2D3C204EFD53EEF41FD389C0F96056 (void);
// 0x00000056 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::Reset()
extern void Provider_Reset_mBE826EAD96AF6680D04C4316809EE2652AD2A4C9 (void);
// 0x00000057 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::OnApplicationPause()
extern void Provider_OnApplicationPause_m71ADF4616F4162E0725A594460EC66EA301966B3 (void);
// 0x00000058 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::OnApplicationResume()
extern void Provider_OnApplicationResume_mB784ED545CC3802670445357B1E5CD942794F532 (void);
// 0x00000059 UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionAvailability> UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::GetAvailabilityAsync()
extern void Provider_GetAvailabilityAsync_m3A9AC0788CC5F34ECD2ADB5D406C8087FBF60991 (void);
// 0x0000005A UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionInstallationStatus> UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::InstallAsync()
extern void Provider_InstallAsync_mC34CC35BE9F77D036C194822AF2C94ADABCEA322 (void);
// 0x0000005B UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::get_trackingState()
extern void Provider_get_trackingState_m5A3E48A828426BF837B4714F24C0A6C3197CDDDF (void);
// 0x0000005C UnityEngine.XR.ARSubsystems.NotTrackingReason UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::get_notTrackingReason()
extern void Provider_get_notTrackingReason_m3C4778EE2EEB55CD1559FFA9A7098D4BCEBF4382 (void);
// 0x0000005D System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::get_matchFrameRate()
extern void Provider_get_matchFrameRate_m44BA90701AD5DF41CCA0FB516DE568448FCE5729 (void);
// 0x0000005E System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::set_matchFrameRate(System.Boolean)
extern void Provider_set_matchFrameRate_mC644C27FF86C3F9030E007ADBEA2AFC86BE766D6 (void);
// 0x0000005F System.Int32 UnityEngine.XR.ARKit.ARKitSessionSubsystem/Provider::get_frameRate()
extern void Provider_get_frameRate_mA1EAF2FB09260A887342D61065F009B2CC2934EB (void);
// 0x00000060 UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/Availability UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi::UnityARKit_session_getAvailability()
extern void NativeApi_UnityARKit_session_getAvailability_m99887277747C2AAD5A630BF058000B82B97F844D (void);
// 0x00000061 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi::UnityARKit_session_update()
extern void NativeApi_UnityARKit_session_update_m8C606BC443E0856C3CE057C45A554EAF1DAA8B74 (void);
// 0x00000062 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi::UnityARKit_session_construct()
extern void NativeApi_UnityARKit_session_construct_mFF6871B50DE774C16170C63CBC1C5A244B6DF7B3 (void);
// 0x00000063 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi::UnityARKit_session_destroy()
extern void NativeApi_UnityARKit_session_destroy_mED1AC9AA6E6D593699FB6A877AA9B7C015D5C7E6 (void);
// 0x00000064 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi::UnityARKit_session_resume()
extern void NativeApi_UnityARKit_session_resume_m65F7BC2F836A0DFD9C4ED2F9D9F7B3D7FD8BB6A3 (void);
// 0x00000065 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi::UnityARKit_session_pause()
extern void NativeApi_UnityARKit_session_pause_m1D55CF790B44AEA02FBBB1D58CE68306AC1ECEA0 (void);
// 0x00000066 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi::UnityARKit_session_reset()
extern void NativeApi_UnityARKit_session_reset_m67A0BCBAF9237CEA676E4AE5AFCADA108D7DA87C (void);
// 0x00000067 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi::UnityARKit_session_getTrackingState()
extern void NativeApi_UnityARKit_session_getTrackingState_mC556510D36A2A09FD80AB564EF433A9740EEEF60 (void);
// 0x00000068 UnityEngine.XR.ARSubsystems.NotTrackingReason UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi::UnityARKit_session_getNotTrackingReason()
extern void NativeApi_UnityARKit_session_getNotTrackingReason_mDD73A7E193548D8D040F9C147C733072EFA6FF25 (void);
// 0x00000069 System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi::UnityARKit_session_getMatchFrameRateEnabled()
extern void NativeApi_UnityARKit_session_getMatchFrameRateEnabled_m8F100FB7D6375D28C817BA1F3BE453A6197905E3 (void);
// 0x0000006A System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi::UnityARKit_session_setMatchFrameRateEnabled(System.Boolean)
extern void NativeApi_UnityARKit_session_setMatchFrameRateEnabled_m9AD09A23F4C7179C29CFDC5C95D2E75DD3DF751E (void);
// 0x0000006B System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/OnAsyncConversionCompleteDelegate::.ctor(System.Object,System.IntPtr)
extern void OnAsyncConversionCompleteDelegate__ctor_m4BE0A755AAC63D37870EB5636686E53F8A3AB114 (void);
// 0x0000006C System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/OnAsyncConversionCompleteDelegate::Invoke(UnityEngine.XR.ARKit.ARWorldMapRequestStatus,System.Int32,System.IntPtr)
extern void OnAsyncConversionCompleteDelegate_Invoke_m02E895D72FDB88BB08F0A654711CE610B2F00816 (void);
// 0x0000006D System.IAsyncResult UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/OnAsyncConversionCompleteDelegate::BeginInvoke(UnityEngine.XR.ARKit.ARWorldMapRequestStatus,System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern void OnAsyncConversionCompleteDelegate_BeginInvoke_m2C33525828F103BD6E8F47B18F3193F6AF8FB56A (void);
// 0x0000006E System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/OnAsyncConversionCompleteDelegate::EndInvoke(System.IAsyncResult)
extern void OnAsyncConversionCompleteDelegate_EndInvoke_mD44E14594FFBDA7691CD4F7D9BE1247CA1EE6AE7 (void);
// 0x0000006F System.Void UnityEngine.XR.ARKit.ARWorldMap::Dispose()
extern void ARWorldMap_Dispose_m79FA6173E594AE9F057F0AEB856A15E2ACB43757 (void);
// 0x00000070 System.Int32 UnityEngine.XR.ARKit.ARWorldMap::GetHashCode()
extern void ARWorldMap_GetHashCode_m8DDE36BC2ED25796844C59C7A098B07E7A7BA573 (void);
// 0x00000071 System.Boolean UnityEngine.XR.ARKit.ARWorldMap::Equals(System.Object)
extern void ARWorldMap_Equals_m8D5C69808F4E3DB20F697D46F85C17A24FDE4688 (void);
// 0x00000072 System.Boolean UnityEngine.XR.ARKit.ARWorldMap::Equals(UnityEngine.XR.ARKit.ARWorldMap)
extern void ARWorldMap_Equals_m50211C5B4349C580EC9E3913814FCE02E72C7D3A (void);
// 0x00000073 System.Void UnityEngine.XR.ARKit.ARWorldMap::.ctor(System.Int32)
extern void ARWorldMap__ctor_m51BA4D411B69385E02F7A49B7BA1ECB0D2AD2FD7 (void);
// 0x00000074 System.Int32 UnityEngine.XR.ARKit.ARWorldMap::get_nativeHandle()
extern void ARWorldMap_get_nativeHandle_m529E0BB03669BBD9370B50C8F6ED90BA05213F22 (void);
// 0x00000075 System.Void UnityEngine.XR.ARKit.ARWorldMap::set_nativeHandle(System.Int32)
extern void ARWorldMap_set_nativeHandle_mBE78617799CC9B825C61B179F1E2F35D310740DE (void);
// 0x00000076 System.Boolean UnityEngine.XR.ARKit.ARWorldMapRequestStatusExtensions::IsError(UnityEngine.XR.ARKit.ARWorldMapRequestStatus)
extern void ARWorldMapRequestStatusExtensions_IsError_m4324D3418C82C5B1C955A72DD0E9BA5805286540 (void);
// 0x00000077 UnityEngine.XR.ARSubsystems.XRDepthSubsystem/IDepthApi UnityEngine.XR.ARKit.ARKitXRDepthSubsystem::GetInterface()
extern void ARKitXRDepthSubsystem_GetInterface_m77800A3C54989EE2563E744F7F75EA670F8D099D (void);
// 0x00000078 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem::RegisterDescriptor()
extern void ARKitXRDepthSubsystem_RegisterDescriptor_mEB1B5F8A8CF06C7E6F00CEE8B8A5D8F2CB86B5E7 (void);
// 0x00000079 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem::.ctor()
extern void ARKitXRDepthSubsystem__ctor_m81EFFCC78EA40E790162C25FD9DC64DF4557BD46 (void);
// 0x0000007A System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/Provider::UnityARKit_depth_destroy()
extern void Provider_UnityARKit_depth_destroy_mC45296F25777188452BB10D79B7DDDE76B66A956 (void);
// 0x0000007B System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/Provider::UnityARKit_depth_start()
extern void Provider_UnityARKit_depth_start_m1A1619B512F9D7C576B4B8242034AFA9AD432F39 (void);
// 0x0000007C System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/Provider::UnityARKit_depth_stop()
extern void Provider_UnityARKit_depth_stop_mAF0980226FA0F11EA03B312DFA3A980D76BD2851 (void);
// 0x0000007D System.Void* UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/Provider::UnityARKit_depth_acquireChanges(System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Int32&)
extern void Provider_UnityARKit_depth_acquireChanges_m9F4E61A5D34FC6464A9EF30949018DD2EA721088 (void);
// 0x0000007E System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/Provider::UnityARKit_depth_releaseChanges(System.Void*)
extern void Provider_UnityARKit_depth_releaseChanges_m4AC138A3F97251CF7825B44894D9E9572504347A (void);
// 0x0000007F System.Void* UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/Provider::UnityARKit_depth_acquirePointCloud(UnityEngine.XR.ARSubsystems.TrackableId,System.Void*&,System.Void*&,System.Int32&)
extern void Provider_UnityARKit_depth_acquirePointCloud_m5920A0FB4730D9EBDBC4124645C39FB4F423AE0A (void);
// 0x00000080 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/Provider::UnityARKit_depth_releasePointCloud(System.Void*)
extern void Provider_UnityARKit_depth_releasePointCloud_mCA0FFA8714996C6F3DEE1EABE5F0427339E78DCD (void);
// 0x00000081 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud> UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRPointCloud,Unity.Collections.Allocator)
extern void Provider_GetChanges_m3D875DA71FE758D7F6D0BBD275DDA6D771475AEE (void);
// 0x00000082 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/Provider::Destroy()
extern void Provider_Destroy_m2417898BF370FF80CE0190ADE67B1015C9A0C2D1 (void);
// 0x00000083 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/Provider::Start()
extern void Provider_Start_mB04F1A6B572E76533956D66F2E861E8CDD738791 (void);
// 0x00000084 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/Provider::Stop()
extern void Provider_Stop_m21FE3E7A5078886E606E91183DDD2C15C7976F00 (void);
// 0x00000085 UnityEngine.XR.ARSubsystems.XRPointCloudData UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/Provider::GetPointCloudData(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator)
extern void Provider_GetPointCloudData_mCCC7D65910FD1989B92913813B111A78CBFA5200 (void);
// 0x00000086 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/Provider::.ctor()
extern void Provider__ctor_m1EC138B2F744CA07C952BF845D934285B5BA498A (void);
// 0x00000087 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/TransformPositionsJob::Execute(System.Int32)
extern void TransformPositionsJob_Execute_mB106298BC698C628A7389A30320D14D7A001ED2C (void);
// 0x00000088 System.Void UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_Construct()
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Construct_m389EF901E5A84DE58098B34F3159AFAA1B83C6E5 (void);
// 0x00000089 System.Void UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_Destruct()
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Destruct_mA74881A49FA8DB131E3613E414E54215205799C2 (void);
// 0x0000008A System.Void UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_Start()
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Start_mD464FCD2D9A5C50791D7B9D38AAD43D97ACA6227 (void);
// 0x0000008B System.Void UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_Stop()
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Stop_m80FF6427ACD878991E6F3649C32A4C948AD834C9 (void);
// 0x0000008C System.Void UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_SetAutomaticPlacementEnabled(System.Boolean)
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_SetAutomaticPlacementEnabled_m58CF7C2D525E820536C7143D42E84C7EFC4805A9 (void);
// 0x0000008D System.Boolean UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_TryAddEnvironmentProbe(UnityEngine.Pose,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.XR.ARSubsystems.XREnvironmentProbe&)
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_TryAddEnvironmentProbe_m7EF9192A23F0E6CBB565BCB89D9D3AC55024F272 (void);
// 0x0000008E System.Boolean UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_TryRemoveEnvironmentProbe(UnityEngine.XR.ARSubsystems.TrackableId)
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_TryRemoveEnvironmentProbe_m823D591063A7E9D9F2186D3AE0FB2AA96C03E1D6 (void);
// 0x0000008F System.IntPtr UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_AcquireChanges(System.Int32&,System.IntPtr&,System.Int32&,System.IntPtr&,System.Int32&,System.IntPtr&,System.Int32&)
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_AcquireChanges_mF3B06C982FBD823D7B03F3DB870E22846FFA834C (void);
// 0x00000090 System.Void UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_ReleaseChanges(System.IntPtr)
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_ReleaseChanges_m7357747C89D2E0CC5380F76F4965A3EECE4D1539 (void);
// 0x00000091 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::UnityARKit_imageTracking_setMaximumNumberOfTrackedImages(System.Int32)
extern void ARKitImageTrackingSubsystem_UnityARKit_imageTracking_setMaximumNumberOfTrackedImages_mBF0FA67787BE559D28A4949D256D5824B8F6F1F4 (void);
// 0x00000092 UnityEngine.XR.ARKit.SetReferenceLibraryResult UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::UnityARKit_imageTracking_trySetReferenceLibrary(System.String,System.Int32,System.Guid)
extern void ARKitImageTrackingSubsystem_UnityARKit_imageTracking_trySetReferenceLibrary_m8D9CDB6B19408E779E8BFAC00B99A9BF62F130B3 (void);
// 0x00000093 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::UnityARKit_imageTracking_stop()
extern void ARKitImageTrackingSubsystem_UnityARKit_imageTracking_stop_mA5E30442ACDDB68089B08B3F6928E3BDC55E85B6 (void);
// 0x00000094 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::UnityARKit_imageTracking_destroy()
extern void ARKitImageTrackingSubsystem_UnityARKit_imageTracking_destroy_m73470876BEDDE85C215EABCD3555D73571E5F772 (void);
// 0x00000095 System.Void* UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::UnityARKit_imageTracking_acquireChanges(System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Int32&)
extern void ARKitImageTrackingSubsystem_UnityARKit_imageTracking_acquireChanges_mB4BFD5D1B49A8BA6443E7F08FE7BA8EE68A0BCFB (void);
// 0x00000096 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::UnityARKit_imageTracking_releaseChanges(System.Void*)
extern void ARKitImageTrackingSubsystem_UnityARKit_imageTracking_releaseChanges_m79BE2C4F28C174B2D27CCA85FF9C6517F1B9F10A (void);
// 0x00000097 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::RegisterDescriptor()
extern void ARKitImageTrackingSubsystem_RegisterDescriptor_mC8DC8F9919766BB3A242BF200922D81DCC59DE20 (void);
// 0x00000098 UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/IProvider UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::CreateProvider()
extern void ARKitImageTrackingSubsystem_CreateProvider_m5411943AF1A08EF9D30E950EA921F0B3855F8217 (void);
// 0x00000099 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::.ctor()
extern void ARKitImageTrackingSubsystem__ctor_m0E214A0CB94F079C236C8B90D0D3ED0DE8BA8CB1 (void);
// 0x0000009A System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem/Provider::set_imageLibrary(UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary)
extern void Provider_set_imageLibrary_m1EBAF4E0DFA8EBC0FE1FEDF6ED7064F7DF91DA13 (void);
// 0x0000009B UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage> UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem/Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRTrackedImage,Unity.Collections.Allocator)
extern void Provider_GetChanges_m1B8C9B35C5FB039958F5161AE9392270D6B4D1C1 (void);
// 0x0000009C System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem/Provider::Destroy()
extern void Provider_Destroy_m2406B1320F91226952D637ADA195CE6F236A0BBE (void);
// 0x0000009D System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem/Provider::set_maxNumberOfMovingImages(System.Int32)
extern void Provider_set_maxNumberOfMovingImages_mB92A28E92BC478D41E6F1B51DF1AEB5D85156DBC (void);
// 0x0000009E System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem/Provider::.ctor()
extern void Provider__ctor_mA9316C0973B75FC3C67DE8C0227F6A0CDBE7F96D (void);
// 0x0000009F System.Int32 UnityEngine.XR.ARKit.OSVersion::get_major()
extern void OSVersion_get_major_m78423586F3DAF62CF3FACA972DF71A0794FF42E6 (void);
// 0x000000A0 System.Void UnityEngine.XR.ARKit.OSVersion::set_major(System.Int32)
extern void OSVersion_set_major_m5DBF19A6CA77CC1DAD43846899A4871548B48A84 (void);
// 0x000000A1 System.Int32 UnityEngine.XR.ARKit.OSVersion::get_minor()
extern void OSVersion_get_minor_mECC11198E3287B306A2A5B9C2DEAA7113263D7B5 (void);
// 0x000000A2 System.Void UnityEngine.XR.ARKit.OSVersion::set_minor(System.Int32)
extern void OSVersion_set_minor_mA54AFDAB79AF99984AD01893AC99866479877CC8 (void);
// 0x000000A3 System.Int32 UnityEngine.XR.ARKit.OSVersion::get_point()
extern void OSVersion_get_point_m15A8472AC862312B14F0FE398C81D538BBF1EF74 (void);
// 0x000000A4 System.Void UnityEngine.XR.ARKit.OSVersion::set_point(System.Int32)
extern void OSVersion_set_point_mE257BF56FF20203DC23672D35C1B20D6A1454405 (void);
// 0x000000A5 System.Void UnityEngine.XR.ARKit.OSVersion::.ctor(System.Int32,System.Int32,System.Int32)
extern void OSVersion__ctor_mEAB035AA8D379D1C6A42A3CFC1B04F24A8AF2AC4 (void);
// 0x000000A6 UnityEngine.XR.ARKit.OSVersion UnityEngine.XR.ARKit.OSVersion::Parse(System.String)
extern void OSVersion_Parse_mDF26500DB38764536C24E82F4659B9B1E0B40AC8 (void);
// 0x000000A7 System.Int32 UnityEngine.XR.ARKit.OSVersion::IndexOfFirstDigit(System.String)
extern void OSVersion_IndexOfFirstDigit_m3527F158639800E2A9884F8D83A5F394B3FED5B4 (void);
// 0x000000A8 System.Int32 UnityEngine.XR.ARKit.OSVersion::ParseNextComponent(System.String,System.Int32&)
extern void OSVersion_ParseNextComponent_mBDCC4A63E1B916D9B1136864639DA42C8762FC82 (void);
// 0x000000A9 System.Int32 UnityEngine.XR.ARKit.OSVersion::GetHashCode()
extern void OSVersion_GetHashCode_mF4F18C6F777E716D07DEF052A90CC64C73C3D01E (void);
// 0x000000AA System.Int32 UnityEngine.XR.ARKit.OSVersion::CompareTo(UnityEngine.XR.ARKit.OSVersion)
extern void OSVersion_CompareTo_m5102D1398077353643B4784512A599CDCB5590A1 (void);
// 0x000000AB System.Boolean UnityEngine.XR.ARKit.OSVersion::Equals(System.Object)
extern void OSVersion_Equals_mA70B1E35BBCD9C206367BD8C66F3E0F2DAF51BF3 (void);
// 0x000000AC System.Boolean UnityEngine.XR.ARKit.OSVersion::Equals(UnityEngine.XR.ARKit.OSVersion)
extern void OSVersion_Equals_m99FCBF7670DF0DCC1D6968EC51EE6EF55611471E (void);
// 0x000000AD System.Boolean UnityEngine.XR.ARKit.OSVersion::op_LessThan(UnityEngine.XR.ARKit.OSVersion,UnityEngine.XR.ARKit.OSVersion)
extern void OSVersion_op_LessThan_m75C71874EFC4F3233EC421034E6599CADB16504F (void);
// 0x000000AE System.Boolean UnityEngine.XR.ARKit.OSVersion::op_GreaterThan(UnityEngine.XR.ARKit.OSVersion,UnityEngine.XR.ARKit.OSVersion)
extern void OSVersion_op_GreaterThan_mD04D07551AE4D4C9D08B63884DF80B255047F2B1 (void);
// 0x000000AF System.Boolean UnityEngine.XR.ARKit.OSVersion::op_Equality(UnityEngine.XR.ARKit.OSVersion,UnityEngine.XR.ARKit.OSVersion)
extern void OSVersion_op_Equality_mC51EEC8017A1A495CD6DB0E046FA1BCF4D656DB4 (void);
// 0x000000B0 System.Boolean UnityEngine.XR.ARKit.OSVersion::op_GreaterThanOrEqual(UnityEngine.XR.ARKit.OSVersion,UnityEngine.XR.ARKit.OSVersion)
extern void OSVersion_op_GreaterThanOrEqual_m5DC241E62216C8897A29CA8BE7F1352528B9F420 (void);
// 0x000000B1 System.String UnityEngine.XR.ARKit.OSVersion::ToString()
extern void OSVersion_ToString_mC2FB1F8F61AAE40B141FE45C3F3B9408701EA00A (void);
// 0x000000B2 UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/IProvider UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem::CreateProvider()
extern void ARKitXRPlaneSubsystem_CreateProvider_m5EE565D6EB589B5C58B47BE5EFB6429AF2B87212 (void);
// 0x000000B3 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem::RegisterDescriptor()
extern void ARKitXRPlaneSubsystem_RegisterDescriptor_m5F27E00E3BBC38D080D558D6B2689AB709ED0123 (void);
// 0x000000B4 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem::.ctor()
extern void ARKitXRPlaneSubsystem__ctor_m1A4464449DBD1423997334CA3D76D0ABB4E88B2B (void);
// 0x000000B5 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::Destroy()
extern void Provider_Destroy_m2A3C831232DD1ED45CFB4EB3292560FEBDDAE92E (void);
// 0x000000B6 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::Start()
extern void Provider_Start_m97C5ED483AB420884B50BC8974146BF2662A412E (void);
// 0x000000B7 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::Stop()
extern void Provider_Stop_mB0C113558F8046BB4F43369D2A09D2E6F1EFA7D8 (void);
// 0x000000B8 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::GetBoundary(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<UnityEngine.Vector2>&)
extern void Provider_GetBoundary_m2A7D4E3FB7985A08B1179A5B60C890843494FFBA (void);
// 0x000000B9 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.BoundedPlane> UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::GetChanges(UnityEngine.XR.ARSubsystems.BoundedPlane,Unity.Collections.Allocator)
extern void Provider_GetChanges_m1180227923E6905E3CA3F9CEF8BA80C58E7B760E (void);
// 0x000000BA System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::set_planeDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void Provider_set_planeDetectionMode_m1406C8A9A2F69D9E9E1B4F396B8D10D0333CD32C (void);
// 0x000000BB System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::UnityARKit_planes_shutdown()
extern void Provider_UnityARKit_planes_shutdown_m87991ECD39569A385E3D85700AF4A06970E30E4C (void);
// 0x000000BC System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::UnityARKit_planes_start()
extern void Provider_UnityARKit_planes_start_m82B8C363308B956D71355779F01C6295321DA514 (void);
// 0x000000BD System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::UnityARKit_planes_stop()
extern void Provider_UnityARKit_planes_stop_mEAF663A81B50E1EF93EC8E75565035687E50B535 (void);
// 0x000000BE System.Void* UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::UnityARKit_planes_acquireChanges(System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Int32&)
extern void Provider_UnityARKit_planes_acquireChanges_mE72922A68EDC98AA2EAFD46F28272FB3A1E5B0A5 (void);
// 0x000000BF System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::UnityARKit_planes_releaseChanges(System.Void*)
extern void Provider_UnityARKit_planes_releaseChanges_m594A8F1CD399305BDC1CCE9D9F425848AA2070F5 (void);
// 0x000000C0 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::UnityARKit_planes_setPlaneDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void Provider_UnityARKit_planes_setPlaneDetectionMode_mD72BD31A29D28A59882B687FF3F97C10103D6799 (void);
// 0x000000C1 System.Void* UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::UnityARKit_planes_acquireBoundary(UnityEngine.XR.ARSubsystems.TrackableId,System.Void*&,System.Int32&)
extern void Provider_UnityARKit_planes_acquireBoundary_mB24A38E074DD6AC49490DCF129C6BD4170F05CC6 (void);
// 0x000000C2 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::UnityARKit_planes_releaseBoundary(System.Void*)
extern void Provider_UnityARKit_planes_releaseBoundary_mB59DE7A5598CFD12C746724D3BFAEB07E0DEE6DF (void);
// 0x000000C3 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider::.ctor()
extern void Provider__ctor_m7CA41720BA2D536A4C58B904A5AC5DE755605E6C (void);
// 0x000000C4 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider/FlipBoundaryWindingJob::Execute()
extern void FlipBoundaryWindingJob_Execute_m50311444035897AB61EE1379BF51C2B8E58FD3D9 (void);
// 0x000000C5 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider/TransformBoundaryPositionsJob::Execute(System.Int32)
extern void TransformBoundaryPositionsJob_Execute_m2C4A63CBC47948E6E9267689C014416A9CE756DF (void);
static Il2CppMethodPointer s_methodPointers[197] = 
{
	Api_UnityARKit_disposeWorldMap_mDB310DE6F820E095FB2B2D6ECD586FB7617026B9,
	ARKitCameraSubsystem_Register_m0B242BF988E92E626979B845EE749A1794E9E642,
	ARKitCameraSubsystem_CreateProvider_mD59CEAEDE9A4351E3B6E069004B813F882FB2987,
	ARKitCameraSubsystem__ctor_mE0B24FB4ED5FC603CB22E220A161A00177B9B8B9,
	Provider_get_shaderName_m7DAA2314A55B8E6B5BAAB3FDE0926DCCD77F11EA,
	Provider_get_permissionGranted_m02DB9BF826B2D20EBA4C6B7644FDB6AB9642C2A9,
	Provider__ctor_m94418749731B416D24DE7243E47BAEFC002D646E,
	Provider_Start_m2946B41F238958FB44AD3B16DBD26844F61BC7D7,
	Provider_Stop_mAEA642A3F7A598594580E3D5687239F74C7E1761,
	Provider_Destroy_m2CC7D21196FDF5A5B0196595C7AB5E2AC2565554,
	Provider_TryGetFrame_m778BED16F19EC494A6514EEF672A8482880C7BE6,
	Provider_TrySetFocusMode_m681B30E6232C7B9FA5DD26925FDCCD9A04631FF7,
	Provider_TrySetLightEstimationMode_m99AE9CF47F47FEAB5D2FF28AE89DA3AE5AD2BAA6,
	Provider_TryGetIntrinsics_m8242924A73B0EB1AED11A5D500C070E41C14E79B,
	Provider_GetConfigurations_m4D9FC2BA19A3649FD8A38FB6125B77916FD4A71B,
	Provider_get_currentConfiguration_mA0CFBA086EFFF0ED6435E6AB7084329F8DAB02D3,
	Provider_set_currentConfiguration_m9E1BDFAEEA1A00221B3DC967E772F12204458644,
	Provider_GetTextureDescriptors_m9CECC57BD95307B36CC3F27C9CFBBACC92836DCC,
	Provider_TryAcquireLatestImage_mD6A0021BD259DC4599D9CFE86BE154B9AAB8D157,
	Provider_DisposeImage_m937B0669326BFF5390E7F553FE0409EF28F0CAE4,
	Provider__cctor_m38524FA8A6011F15F82FE1A2500B22425FC52099,
	NativeApi_UnityARKit_Camera_Construct_m2DF0AEE8AA88CDF96D248F9CD714961C2738216F,
	NativeApi_UnityARKit_Camera_Destruct_m795FFFF7821CE91E8992070BEFAAE0FFBA6AE084,
	NativeApi_UnityARKit_Camera_Start_m59DA518F90E02C5707FDD049DC3DECD96B65A5A6,
	NativeApi_UnityARKit_Camera_Stop_m098821B53BD6527CA00BB02E51D36A7A5C76FF10,
	NativeApi_UnityARKit_Camera_TryGetFrame_m6048991661BFB4AEE17C00EDCF7E65995B7C8691,
	NativeApi_UnityARKit_Camera_TrySetFocusMode_m2D71E0C7D2E3A86263BC42FF937BEAC0FFBD8677,
	NativeApi_UnityARKit_Camera_TrySetLightEstimationMode_m2DAF2550DA728382FB0F1B91B02B74FC50D4257E,
	NativeApi_UnityARKit_Camera_TryGetIntrinsics_m364B7B8DCA914DD0C1878F56859A5C7D9CC3B045,
	NativeApi_UnityARKit_Camera_IsCameraPermissionGranted_m972DB9591AA4E5EB4F483280F44DEF5CC8C88EE3,
	NativeApi_UnityARKit_Camera_AcquireConfigurations_m6A384C73CC0BE9DA50DC6128284E200678D08771,
	NativeApi_UnityARKit_Camera_ReleaseConfigurations_mECAE479F28DB8847031DB58670D463D8692C661D,
	NativeApi_UnityARKit_Camera_TryGetCurrentConfiguration_m2B5868976AF970142BB6CDBCBB04329C98FBAA88,
	NativeApi_UnityARKit_Camera_TrySetCurrentConfiguration_m10852C9A985C98A1BF729D17027BE7B96C9FB673,
	NativeApi_UnityARKit_Camera_AcquireTextureDescriptors_mEAFD58446AEBEC88F2E3E55B5C8C3C89B2BA0869,
	NativeApi_UnityARKit_Camera_ReleaseTextureDescriptors_m1B1D58FE2C83A8854CD2223602213D302585A0A2,
	NativeApi_UnityARKit_Camera_TryAcquireLatestImage_m9EDEF6EF0E4C88B918827C8DFCA3AF3265C04DEE,
	NativeApi_UnityARKit_Camera_DisposeImage_m696CD790C79BA59B3B69032D9E331D33F0D8506E,
	ARKitEnvironmentProbeRegistration_Register_m99E02E59582A0F880422D128FEEF2BEE553EE20E,
	ARKitEnvironmentProbeSubsystem_CreateProvider_m734B041E08091395C665D21150F3728493C2A746,
	ARKitEnvironmentProbeSubsystem__ctor_m957799C8DE48100EF21E8429209271292EADFA95,
	Provider__ctor_m01089AA1109114DA6CA9F68C2B691726E7E75745,
	Provider_Start_m00CEAF4D906E61A9A4F729F73AE8766A2ED7D7E5,
	Provider_Stop_mC2481922A4D2761BB6057BA06C255CF80EFBD862,
	Provider_Destroy_mC7E6A4679A9D3565D0AB9F588BF6009D95D64281,
	Provider_SetAutomaticPlacement_m8B87B886E472B8BA1A07C1C69FD95D8295468BAD,
	Provider_TryAddEnvironmentProbe_mC080470C1555B1CCC7F29D4DD842650059BD765A,
	Provider_RemoveEnvironmentProbe_m93134B4724464908E4BB6EA7BB62A296352D47C3,
	Provider_GetChanges_m26E082A5D0B91535BD7201C3AFAFA220CC61F006,
	ARKitRaycastSubsystem_CreateProvider_m5047B990F4841868A7014A87971249FE94C867B3,
	ARKitRaycastSubsystem_RegisterDescriptor_m1442393B3D2D07C4E6F1FBD355695BCEB4DCF63E,
	ARKitRaycastSubsystem__ctor_m8FE9A2A756806FD0166A60C2347D88F0BB38307F,
	Provider_Raycast_m1B485E19859E6EB636FB7E7934E316820C5EEEB6,
	Provider__ctor_mFAB8D17BFE8D59172A9949DE1F83981251CBBB88,
	NativeApi_UnityARKit_raycast_acquireHitResults_mA4AC6C358549A8DC05F394AFD56ED90CD03E9C9F,
	NativeApi_UnityARKit_raycast_copyAndReleaseHitResults_m1080A044038B5CF33AAB68D1DD8C4018E1B2D12A,
	ARKitReferencePointSubsystem_CreateProvider_m5D766C2E862F8A4ECA5772FDA6BF63651C129A09,
	ARKitReferencePointSubsystem_RegisterDescriptor_mE3DCA20DAE72D2C7CF15B150584606C8D0FB8BA4,
	ARKitReferencePointSubsystem__ctor_m5BA45BB0ED79227C4E4ECE86D0EFA99A2ABEE06D,
	Provider_Start_mE6069260E7307357558BA42120FF26C71DF79D0D,
	Provider_Stop_m064C737A6B63DD370751437D4154DF9ADF3251B9,
	Provider_Destroy_mAB4C5670DDFEDB0575821B397792727B85AA11F7,
	Provider_GetChanges_m6ACC32D7B8FF8B2AE977BEE37D81AB5D7141B7F1,
	Provider_TryAddReferencePoint_m99AEE284D5BBF3AFD568CD3428C3C50D8441EEF0,
	Provider_TryAttachReferencePoint_m8928234A81EDE93A7C83AE2BDA10940D5EB1A3C8,
	Provider_TryRemoveReferencePoint_m1C398858AF6AFD9A6F3822F2E02941D17F4BC7EC,
	Provider_UnityARKit_refPoints_onStart_m7F72101E953D4FA29B77F9B925CD097271D8417E,
	Provider_UnityARKit_refPoints_onStop_m7C09A2E2921F4C8B7460BC4B3B2FC139092E0865,
	Provider_UnityARKit_refPoints_onDestroy_m1D71BC7DBA28E50A83D322C9B735554D47EC67D1,
	Provider_UnityARKit_refPoints_acquireChanges_mA344595E5CD7FE92F42133E6B0E854E124B837EB,
	Provider_UnityARKit_refPoints_releaseChanges_mA2279618DB35A58E4AA4CAD3D51B25BEF9EB4D59,
	Provider_UnityARKit_refPoints_tryAdd_m2F8B38BB56978FFD60602045422B4C0639B7B004,
	Provider_UnityARKit_refPoints_tryAttach_m774D4727945B1B3E4B2E212913D1482147A923AE,
	Provider_UnityARKit_refPoints_tryRemove_m3A0BEB288BC29179F673735239E3FEFE65A61906,
	Provider__ctor_m7DAC59A12BE6120D2C72B16E02A503C014166901,
	ARKitSessionSubsystem_CreateProvider_m0519B1E5B244B918EC2E29AA8D46D09696BBF64B,
	ARKitSessionSubsystem__cctor_mF21044D0935B2CA8F1FCD42BF839812255101E2A,
	ARKitSessionSubsystem_OnAsyncConversionComplete_mF9F38F09B38CC00967BD472D09D8584299F1B04F,
	ARKitSessionSubsystem_RegisterDescriptor_m359F3EC534DDADAE498376C22BE8BB24813B1CC9,
	ARKitSessionSubsystem__ctor_m551ABE75E099FDA75C75967077DEFE540A41DC19,
	Provider__ctor_mB6A986F9599E6428D3925C19C0A5937C9F79D7B5,
	Provider_Resume_m18F254F006F15D8F65D947C21C1C1E97F40C5512,
	Provider_Pause_mA9F691CC624412FF6E86F51A8E74514F4A9B7526,
	Provider_Update_m50F7B6BA08687FAD6DDBF21FD5678171C1D69ABA,
	Provider_Destroy_mF3FD2EA69B2D3C204EFD53EEF41FD389C0F96056,
	Provider_Reset_mBE826EAD96AF6680D04C4316809EE2652AD2A4C9,
	Provider_OnApplicationPause_m71ADF4616F4162E0725A594460EC66EA301966B3,
	Provider_OnApplicationResume_mB784ED545CC3802670445357B1E5CD942794F532,
	Provider_GetAvailabilityAsync_m3A9AC0788CC5F34ECD2ADB5D406C8087FBF60991,
	Provider_InstallAsync_mC34CC35BE9F77D036C194822AF2C94ADABCEA322,
	Provider_get_trackingState_m5A3E48A828426BF837B4714F24C0A6C3197CDDDF,
	Provider_get_notTrackingReason_m3C4778EE2EEB55CD1559FFA9A7098D4BCEBF4382,
	Provider_get_matchFrameRate_m44BA90701AD5DF41CCA0FB516DE568448FCE5729,
	Provider_set_matchFrameRate_mC644C27FF86C3F9030E007ADBEA2AFC86BE766D6,
	Provider_get_frameRate_mA1EAF2FB09260A887342D61065F009B2CC2934EB,
	NativeApi_UnityARKit_session_getAvailability_m99887277747C2AAD5A630BF058000B82B97F844D,
	NativeApi_UnityARKit_session_update_m8C606BC443E0856C3CE057C45A554EAF1DAA8B74,
	NativeApi_UnityARKit_session_construct_mFF6871B50DE774C16170C63CBC1C5A244B6DF7B3,
	NativeApi_UnityARKit_session_destroy_mED1AC9AA6E6D593699FB6A877AA9B7C015D5C7E6,
	NativeApi_UnityARKit_session_resume_m65F7BC2F836A0DFD9C4ED2F9D9F7B3D7FD8BB6A3,
	NativeApi_UnityARKit_session_pause_m1D55CF790B44AEA02FBBB1D58CE68306AC1ECEA0,
	NativeApi_UnityARKit_session_reset_m67A0BCBAF9237CEA676E4AE5AFCADA108D7DA87C,
	NativeApi_UnityARKit_session_getTrackingState_mC556510D36A2A09FD80AB564EF433A9740EEEF60,
	NativeApi_UnityARKit_session_getNotTrackingReason_mDD73A7E193548D8D040F9C147C733072EFA6FF25,
	NativeApi_UnityARKit_session_getMatchFrameRateEnabled_m8F100FB7D6375D28C817BA1F3BE453A6197905E3,
	NativeApi_UnityARKit_session_setMatchFrameRateEnabled_m9AD09A23F4C7179C29CFDC5C95D2E75DD3DF751E,
	OnAsyncConversionCompleteDelegate__ctor_m4BE0A755AAC63D37870EB5636686E53F8A3AB114,
	OnAsyncConversionCompleteDelegate_Invoke_m02E895D72FDB88BB08F0A654711CE610B2F00816,
	OnAsyncConversionCompleteDelegate_BeginInvoke_m2C33525828F103BD6E8F47B18F3193F6AF8FB56A,
	OnAsyncConversionCompleteDelegate_EndInvoke_mD44E14594FFBDA7691CD4F7D9BE1247CA1EE6AE7,
	ARWorldMap_Dispose_m79FA6173E594AE9F057F0AEB856A15E2ACB43757,
	ARWorldMap_GetHashCode_m8DDE36BC2ED25796844C59C7A098B07E7A7BA573,
	ARWorldMap_Equals_m8D5C69808F4E3DB20F697D46F85C17A24FDE4688,
	ARWorldMap_Equals_m50211C5B4349C580EC9E3913814FCE02E72C7D3A,
	ARWorldMap__ctor_m51BA4D411B69385E02F7A49B7BA1ECB0D2AD2FD7,
	ARWorldMap_get_nativeHandle_m529E0BB03669BBD9370B50C8F6ED90BA05213F22,
	ARWorldMap_set_nativeHandle_mBE78617799CC9B825C61B179F1E2F35D310740DE,
	ARWorldMapRequestStatusExtensions_IsError_m4324D3418C82C5B1C955A72DD0E9BA5805286540,
	ARKitXRDepthSubsystem_GetInterface_m77800A3C54989EE2563E744F7F75EA670F8D099D,
	ARKitXRDepthSubsystem_RegisterDescriptor_mEB1B5F8A8CF06C7E6F00CEE8B8A5D8F2CB86B5E7,
	ARKitXRDepthSubsystem__ctor_m81EFFCC78EA40E790162C25FD9DC64DF4557BD46,
	Provider_UnityARKit_depth_destroy_mC45296F25777188452BB10D79B7DDDE76B66A956,
	Provider_UnityARKit_depth_start_m1A1619B512F9D7C576B4B8242034AFA9AD432F39,
	Provider_UnityARKit_depth_stop_mAF0980226FA0F11EA03B312DFA3A980D76BD2851,
	Provider_UnityARKit_depth_acquireChanges_m9F4E61A5D34FC6464A9EF30949018DD2EA721088,
	Provider_UnityARKit_depth_releaseChanges_m4AC138A3F97251CF7825B44894D9E9572504347A,
	Provider_UnityARKit_depth_acquirePointCloud_m5920A0FB4730D9EBDBC4124645C39FB4F423AE0A,
	Provider_UnityARKit_depth_releasePointCloud_mCA0FFA8714996C6F3DEE1EABE5F0427339E78DCD,
	Provider_GetChanges_m3D875DA71FE758D7F6D0BBD275DDA6D771475AEE,
	Provider_Destroy_m2417898BF370FF80CE0190ADE67B1015C9A0C2D1,
	Provider_Start_mB04F1A6B572E76533956D66F2E861E8CDD738791,
	Provider_Stop_m21FE3E7A5078886E606E91183DDD2C15C7976F00,
	Provider_GetPointCloudData_mCCC7D65910FD1989B92913813B111A78CBFA5200,
	Provider__ctor_m1EC138B2F744CA07C952BF845D934285B5BA498A,
	TransformPositionsJob_Execute_mB106298BC698C628A7389A30320D14D7A001ED2C,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Construct_m389EF901E5A84DE58098B34F3159AFAA1B83C6E5,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Destruct_mA74881A49FA8DB131E3613E414E54215205799C2,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Start_mD464FCD2D9A5C50791D7B9D38AAD43D97ACA6227,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Stop_m80FF6427ACD878991E6F3649C32A4C948AD834C9,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_SetAutomaticPlacementEnabled_m58CF7C2D525E820536C7143D42E84C7EFC4805A9,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_TryAddEnvironmentProbe_m7EF9192A23F0E6CBB565BCB89D9D3AC55024F272,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_TryRemoveEnvironmentProbe_m823D591063A7E9D9F2186D3AE0FB2AA96C03E1D6,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_AcquireChanges_mF3B06C982FBD823D7B03F3DB870E22846FFA834C,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_ReleaseChanges_m7357747C89D2E0CC5380F76F4965A3EECE4D1539,
	ARKitImageTrackingSubsystem_UnityARKit_imageTracking_setMaximumNumberOfTrackedImages_mBF0FA67787BE559D28A4949D256D5824B8F6F1F4,
	ARKitImageTrackingSubsystem_UnityARKit_imageTracking_trySetReferenceLibrary_m8D9CDB6B19408E779E8BFAC00B99A9BF62F130B3,
	ARKitImageTrackingSubsystem_UnityARKit_imageTracking_stop_mA5E30442ACDDB68089B08B3F6928E3BDC55E85B6,
	ARKitImageTrackingSubsystem_UnityARKit_imageTracking_destroy_m73470876BEDDE85C215EABCD3555D73571E5F772,
	ARKitImageTrackingSubsystem_UnityARKit_imageTracking_acquireChanges_mB4BFD5D1B49A8BA6443E7F08FE7BA8EE68A0BCFB,
	ARKitImageTrackingSubsystem_UnityARKit_imageTracking_releaseChanges_m79BE2C4F28C174B2D27CCA85FF9C6517F1B9F10A,
	ARKitImageTrackingSubsystem_RegisterDescriptor_mC8DC8F9919766BB3A242BF200922D81DCC59DE20,
	ARKitImageTrackingSubsystem_CreateProvider_m5411943AF1A08EF9D30E950EA921F0B3855F8217,
	ARKitImageTrackingSubsystem__ctor_m0E214A0CB94F079C236C8B90D0D3ED0DE8BA8CB1,
	Provider_set_imageLibrary_m1EBAF4E0DFA8EBC0FE1FEDF6ED7064F7DF91DA13,
	Provider_GetChanges_m1B8C9B35C5FB039958F5161AE9392270D6B4D1C1,
	Provider_Destroy_m2406B1320F91226952D637ADA195CE6F236A0BBE,
	Provider_set_maxNumberOfMovingImages_mB92A28E92BC478D41E6F1B51DF1AEB5D85156DBC,
	Provider__ctor_mA9316C0973B75FC3C67DE8C0227F6A0CDBE7F96D,
	OSVersion_get_major_m78423586F3DAF62CF3FACA972DF71A0794FF42E6,
	OSVersion_set_major_m5DBF19A6CA77CC1DAD43846899A4871548B48A84,
	OSVersion_get_minor_mECC11198E3287B306A2A5B9C2DEAA7113263D7B5,
	OSVersion_set_minor_mA54AFDAB79AF99984AD01893AC99866479877CC8,
	OSVersion_get_point_m15A8472AC862312B14F0FE398C81D538BBF1EF74,
	OSVersion_set_point_mE257BF56FF20203DC23672D35C1B20D6A1454405,
	OSVersion__ctor_mEAB035AA8D379D1C6A42A3CFC1B04F24A8AF2AC4,
	OSVersion_Parse_mDF26500DB38764536C24E82F4659B9B1E0B40AC8,
	OSVersion_IndexOfFirstDigit_m3527F158639800E2A9884F8D83A5F394B3FED5B4,
	OSVersion_ParseNextComponent_mBDCC4A63E1B916D9B1136864639DA42C8762FC82,
	OSVersion_GetHashCode_mF4F18C6F777E716D07DEF052A90CC64C73C3D01E,
	OSVersion_CompareTo_m5102D1398077353643B4784512A599CDCB5590A1,
	OSVersion_Equals_mA70B1E35BBCD9C206367BD8C66F3E0F2DAF51BF3,
	OSVersion_Equals_m99FCBF7670DF0DCC1D6968EC51EE6EF55611471E,
	OSVersion_op_LessThan_m75C71874EFC4F3233EC421034E6599CADB16504F,
	OSVersion_op_GreaterThan_mD04D07551AE4D4C9D08B63884DF80B255047F2B1,
	OSVersion_op_Equality_mC51EEC8017A1A495CD6DB0E046FA1BCF4D656DB4,
	OSVersion_op_GreaterThanOrEqual_m5DC241E62216C8897A29CA8BE7F1352528B9F420,
	OSVersion_ToString_mC2FB1F8F61AAE40B141FE45C3F3B9408701EA00A,
	ARKitXRPlaneSubsystem_CreateProvider_m5EE565D6EB589B5C58B47BE5EFB6429AF2B87212,
	ARKitXRPlaneSubsystem_RegisterDescriptor_m5F27E00E3BBC38D080D558D6B2689AB709ED0123,
	ARKitXRPlaneSubsystem__ctor_m1A4464449DBD1423997334CA3D76D0ABB4E88B2B,
	Provider_Destroy_m2A3C831232DD1ED45CFB4EB3292560FEBDDAE92E,
	Provider_Start_m97C5ED483AB420884B50BC8974146BF2662A412E,
	Provider_Stop_mB0C113558F8046BB4F43369D2A09D2E6F1EFA7D8,
	Provider_GetBoundary_m2A7D4E3FB7985A08B1179A5B60C890843494FFBA,
	Provider_GetChanges_m1180227923E6905E3CA3F9CEF8BA80C58E7B760E,
	Provider_set_planeDetectionMode_m1406C8A9A2F69D9E9E1B4F396B8D10D0333CD32C,
	Provider_UnityARKit_planes_shutdown_m87991ECD39569A385E3D85700AF4A06970E30E4C,
	Provider_UnityARKit_planes_start_m82B8C363308B956D71355779F01C6295321DA514,
	Provider_UnityARKit_planes_stop_mEAF663A81B50E1EF93EC8E75565035687E50B535,
	Provider_UnityARKit_planes_acquireChanges_mE72922A68EDC98AA2EAFD46F28272FB3A1E5B0A5,
	Provider_UnityARKit_planes_releaseChanges_m594A8F1CD399305BDC1CCE9D9F425848AA2070F5,
	Provider_UnityARKit_planes_setPlaneDetectionMode_mD72BD31A29D28A59882B687FF3F97C10103D6799,
	Provider_UnityARKit_planes_acquireBoundary_mB24A38E074DD6AC49490DCF129C6BD4170F05CC6,
	Provider_UnityARKit_planes_releaseBoundary_mB59DE7A5598CFD12C746724D3BFAEB07E0DEE6DF,
	Provider__ctor_m7CA41720BA2D536A4C58B904A5AC5DE755605E6C,
	FlipBoundaryWindingJob_Execute_m50311444035897AB61EE1379BF51C2B8E58FD3D9,
	TransformBoundaryPositionsJob_Execute_m2C4A63CBC47948E6E9267689C014416A9CE756DF,
};
extern void ARWorldMap_Dispose_m79FA6173E594AE9F057F0AEB856A15E2ACB43757_AdjustorThunk (void);
extern void ARWorldMap_GetHashCode_m8DDE36BC2ED25796844C59C7A098B07E7A7BA573_AdjustorThunk (void);
extern void ARWorldMap_Equals_m8D5C69808F4E3DB20F697D46F85C17A24FDE4688_AdjustorThunk (void);
extern void ARWorldMap_Equals_m50211C5B4349C580EC9E3913814FCE02E72C7D3A_AdjustorThunk (void);
extern void ARWorldMap__ctor_m51BA4D411B69385E02F7A49B7BA1ECB0D2AD2FD7_AdjustorThunk (void);
extern void ARWorldMap_get_nativeHandle_m529E0BB03669BBD9370B50C8F6ED90BA05213F22_AdjustorThunk (void);
extern void ARWorldMap_set_nativeHandle_mBE78617799CC9B825C61B179F1E2F35D310740DE_AdjustorThunk (void);
extern void TransformPositionsJob_Execute_mB106298BC698C628A7389A30320D14D7A001ED2C_AdjustorThunk (void);
extern void OSVersion_get_major_m78423586F3DAF62CF3FACA972DF71A0794FF42E6_AdjustorThunk (void);
extern void OSVersion_set_major_m5DBF19A6CA77CC1DAD43846899A4871548B48A84_AdjustorThunk (void);
extern void OSVersion_get_minor_mECC11198E3287B306A2A5B9C2DEAA7113263D7B5_AdjustorThunk (void);
extern void OSVersion_set_minor_mA54AFDAB79AF99984AD01893AC99866479877CC8_AdjustorThunk (void);
extern void OSVersion_get_point_m15A8472AC862312B14F0FE398C81D538BBF1EF74_AdjustorThunk (void);
extern void OSVersion_set_point_mE257BF56FF20203DC23672D35C1B20D6A1454405_AdjustorThunk (void);
extern void OSVersion__ctor_mEAB035AA8D379D1C6A42A3CFC1B04F24A8AF2AC4_AdjustorThunk (void);
extern void OSVersion_GetHashCode_mF4F18C6F777E716D07DEF052A90CC64C73C3D01E_AdjustorThunk (void);
extern void OSVersion_CompareTo_m5102D1398077353643B4784512A599CDCB5590A1_AdjustorThunk (void);
extern void OSVersion_Equals_mA70B1E35BBCD9C206367BD8C66F3E0F2DAF51BF3_AdjustorThunk (void);
extern void OSVersion_Equals_m99FCBF7670DF0DCC1D6968EC51EE6EF55611471E_AdjustorThunk (void);
extern void OSVersion_ToString_mC2FB1F8F61AAE40B141FE45C3F3B9408701EA00A_AdjustorThunk (void);
extern void FlipBoundaryWindingJob_Execute_m50311444035897AB61EE1379BF51C2B8E58FD3D9_AdjustorThunk (void);
extern void TransformBoundaryPositionsJob_Execute_m2C4A63CBC47948E6E9267689C014416A9CE756DF_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[22] = 
{
	{ 0x0600006F, ARWorldMap_Dispose_m79FA6173E594AE9F057F0AEB856A15E2ACB43757_AdjustorThunk },
	{ 0x06000070, ARWorldMap_GetHashCode_m8DDE36BC2ED25796844C59C7A098B07E7A7BA573_AdjustorThunk },
	{ 0x06000071, ARWorldMap_Equals_m8D5C69808F4E3DB20F697D46F85C17A24FDE4688_AdjustorThunk },
	{ 0x06000072, ARWorldMap_Equals_m50211C5B4349C580EC9E3913814FCE02E72C7D3A_AdjustorThunk },
	{ 0x06000073, ARWorldMap__ctor_m51BA4D411B69385E02F7A49B7BA1ECB0D2AD2FD7_AdjustorThunk },
	{ 0x06000074, ARWorldMap_get_nativeHandle_m529E0BB03669BBD9370B50C8F6ED90BA05213F22_AdjustorThunk },
	{ 0x06000075, ARWorldMap_set_nativeHandle_mBE78617799CC9B825C61B179F1E2F35D310740DE_AdjustorThunk },
	{ 0x06000087, TransformPositionsJob_Execute_mB106298BC698C628A7389A30320D14D7A001ED2C_AdjustorThunk },
	{ 0x0600009F, OSVersion_get_major_m78423586F3DAF62CF3FACA972DF71A0794FF42E6_AdjustorThunk },
	{ 0x060000A0, OSVersion_set_major_m5DBF19A6CA77CC1DAD43846899A4871548B48A84_AdjustorThunk },
	{ 0x060000A1, OSVersion_get_minor_mECC11198E3287B306A2A5B9C2DEAA7113263D7B5_AdjustorThunk },
	{ 0x060000A2, OSVersion_set_minor_mA54AFDAB79AF99984AD01893AC99866479877CC8_AdjustorThunk },
	{ 0x060000A3, OSVersion_get_point_m15A8472AC862312B14F0FE398C81D538BBF1EF74_AdjustorThunk },
	{ 0x060000A4, OSVersion_set_point_mE257BF56FF20203DC23672D35C1B20D6A1454405_AdjustorThunk },
	{ 0x060000A5, OSVersion__ctor_mEAB035AA8D379D1C6A42A3CFC1B04F24A8AF2AC4_AdjustorThunk },
	{ 0x060000A9, OSVersion_GetHashCode_mF4F18C6F777E716D07DEF052A90CC64C73C3D01E_AdjustorThunk },
	{ 0x060000AA, OSVersion_CompareTo_m5102D1398077353643B4784512A599CDCB5590A1_AdjustorThunk },
	{ 0x060000AB, OSVersion_Equals_mA70B1E35BBCD9C206367BD8C66F3E0F2DAF51BF3_AdjustorThunk },
	{ 0x060000AC, OSVersion_Equals_m99FCBF7670DF0DCC1D6968EC51EE6EF55611471E_AdjustorThunk },
	{ 0x060000B1, OSVersion_ToString_mC2FB1F8F61AAE40B141FE45C3F3B9408701EA00A_AdjustorThunk },
	{ 0x060000C4, FlipBoundaryWindingJob_Execute_m50311444035897AB61EE1379BF51C2B8E58FD3D9_AdjustorThunk },
	{ 0x060000C5, TransformBoundaryPositionsJob_Execute_m2C4A63CBC47948E6E9267689C014416A9CE756DF_AdjustorThunk },
};
static const int32_t s_InvokerIndices[197] = 
{
	164,
	3,
	14,
	23,
	14,
	89,
	23,
	23,
	23,
	23,
	2169,
	30,
	30,
	842,
	2171,
	2167,
	2168,
	2172,
	842,
	32,
	3,
	165,
	3,
	3,
	3,
	2759,
	46,
	46,
	386,
	49,
	2760,
	25,
	386,
	2761,
	453,
	17,
	386,
	164,
	3,
	14,
	23,
	23,
	23,
	23,
	23,
	31,
	2203,
	2204,
	2206,
	14,
	3,
	23,
	2245,
	23,
	2762,
	2763,
	14,
	3,
	23,
	23,
	23,
	23,
	2254,
	2252,
	2253,
	2204,
	3,
	3,
	3,
	2764,
	17,
	2765,
	2766,
	2767,
	23,
	14,
	3,
	2768,
	3,
	23,
	23,
	23,
	23,
	2259,
	23,
	23,
	23,
	23,
	14,
	14,
	10,
	10,
	89,
	31,
	10,
	106,
	3,
	3,
	3,
	3,
	3,
	3,
	106,
	106,
	49,
	849,
	124,
	2769,
	2770,
	26,
	23,
	10,
	9,
	2771,
	32,
	10,
	32,
	46,
	14,
	3,
	23,
	3,
	3,
	3,
	2764,
	17,
	2772,
	17,
	2182,
	23,
	23,
	23,
	2181,
	23,
	32,
	3,
	3,
	3,
	3,
	849,
	2773,
	2767,
	2774,
	25,
	164,
	2775,
	3,
	3,
	2764,
	17,
	3,
	14,
	23,
	26,
	2220,
	23,
	32,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	38,
	2776,
	94,
	643,
	10,
	2777,
	9,
	2778,
	2779,
	2779,
	2779,
	2779,
	14,
	14,
	3,
	23,
	23,
	23,
	23,
	2216,
	2235,
	32,
	3,
	3,
	3,
	2764,
	17,
	164,
	2780,
	17,
	23,
	23,
	32,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[1] = 
{
	{ 0x0600004E, 9,  (void**)&ARKitSessionSubsystem_OnAsyncConversionComplete_mF9F38F09B38CC00967BD472D09D8584299F1B04F_RuntimeMethod_var, 0 },
};
extern const Il2CppCodeGenModule g_Unity_XR_ARKitCodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_ARKitCodeGenModule = 
{
	"Unity.XR.ARKit.dll",
	197,
	s_methodPointers,
	22,
	s_adjustorThunks,
	s_InvokerIndices,
	1,
	s_reversePInvokeIndices,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
