/**
 * @format
 */
import React, {useEffect} from 'react';
import {AppRegistry, Platform} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import OneSignal from 'react-native-onesignal';
import { setFCMTOKEN } from "./src/storage/user/dataUser";
import { setCustomText } from 'react-native-global-props';
const customTextProps = { 
    style: { 
      fontFamily: 'DINRoundPro'
    }
  }
setCustomText(customTextProps);
OneSignal.setAppId("76bb2a20-cc61-49f0-b469-96d6a42ae255");
if (Platform.OS === "ios") {
    OneSignal.promptForPushNotificationsWithUserResponse(response => {
        console.warn(response);
    });
}

AppRegistry.registerComponent(appName, () => App);
